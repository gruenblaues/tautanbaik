#include "DialogGraphicsSceneResultDisplay.h"
#include "ui_DialogGraphicsSceneResultDisplay.h"

#include <fstream>

#include "TspFacade.h"
#include "DTTree.h"
#include "GraphicsPoint.h"
#include "GraphicsLine.h"
#include "StaticHelper.h"
#include "Parameters.h"

DialogGraphicsSceneResultDisplay::DialogGraphicsSceneResultDisplay(QWidget *parent, TSP::Parameters::AlgorithmType mainAlgo, TSP::Parameters para, size_t width, size_t height) :
    QDialog(parent),
    ui(new Ui::DialogGraphicsSceneResultDisplay)
{
    ui->setupUi(this);

    mScene = new QGraphicsScene(this);
    mScene->setSceneRect( QRectF(0, 0, width, height) );
    ui->graphicsView->setScene(mScene);
    m_para = para;      // TODO [seboeh] can we drop it?
    m_mainAlgo = mainAlgo;
}

DialogGraphicsSceneResultDisplay::~DialogGraphicsSceneResultDisplay()
{
    delete mScene;
    delete ui;
    clearWays();
}

void DialogGraphicsSceneResultDisplay::clearWays()
{
    for (std::list<TSP::Way*>::iterator wayIter = mWays.begin();
         wayIter != mWays.end(); ++wayIter)
    {
        delete *wayIter;
        *wayIter = NULL;
    }
    mWays.clear();
}

void DialogGraphicsSceneResultDisplay::exec(TSP::DT::DTTree* dtTree, bsp::BspTree * bspTree, TSP::Way * resultTrack)
{
    mWayIter = mWays.end();         // prevent next and previous buttons for crashing the application.
    mResultTrack = resultTrack;
    if (dtTree == NULL) {
        if (bspTree != NULL && resultTrack != NULL) {
            StaticHelper::drawBsp( mScene, bspTree, resultTrack);
        }
    } else {
        clearWays();
        fillWays(dtTree->root());
        mWayIter = mWays.begin();
        if (!mWays.empty()) {
            StaticHelper::drawWay( mScene, *mWayIter, TSP::Point(), true );
            setParameters();
        }
    }
    QDialog::exec();
}

void DialogGraphicsSceneResultDisplay::fillWays(TSP::DT::DTNode::NodeType node)
{
    if (node->children()->empty()) {
        /// 1. reached leaf - create way.
        TSP::Way *way = new TSP::Way();
        TSP::DT::DTNode::NodeType prev = node->parent();
        int number=0;
        while (prev != NULL) {
            way->number(++number);
            way->push_front(*(node->value()));
            node = prev;
            prev = node->parent();
        }
        mWays.push_back( way );
        return;
    } else {
        for (TSP::DT::DTNode::ChildType::iterator childI = node->children()->begin();
             childI != node->children()->end(); ++childI)
        {
            fillWays(*childI);
        }
    }
}

TSP::Point DialogGraphicsSceneResultDisplay::tamarExtractPoint(std::string str)
{
    size_t pos = str.find("x");
    bool ok=false;
    auto x = QString::fromStdString(str.substr(0,pos)).toFloat(&ok);
    if (!ok)
        std::cout << "Error: could not parse the debugOutput.txt. 1" << std::endl;
    auto y = QString::fromStdString(str.substr(pos+1,str.length()-pos)).toFloat(&ok);
    if (!ok)
        std::cout << "Error: could not parse the debugOutput.txt. 2" << std::endl;
    return TSP::Point(x, y);
}

void DialogGraphicsSceneResultDisplay::tamarReadDebugFile()
{
    // precondition
    std::ifstream debugFile("debugOutput.txt");
    if (!debugFile.is_open())
        return;     // TODO [seboeh] error message.

    // preparation
    m_tamarDebugData.clear();

    // read the file content
    std::string line;
    TamarDebugDataType::value_type entry;
    bool isHull = false;
    bool isInnerPoints = false;
    while ((debugFile >> line) && line.length() > 0 )
    {
        if (line.find("</entry>") != std::string::npos)
        {
            m_tamarDebugData.push_back(entry);
            entry.clear();
            continue;
        }
        if (line.find("<hull>") != std::string::npos)
        {
            isHull = true;
            continue;
        }
        if (line.find("<innerPoints>") != std::string::npos)
        {
            isInnerPoints = true;
            continue;
        }
        if (line.find("<hullPos>") != std::string::npos)
        {
            debugFile >> line;
            entry.hullPoint = tamarExtractPoint(std::move(line));
            continue;
        }
        if (line.find("<innerPos>") != std::string::npos)
        {
            debugFile >> line;
            entry.innerPoint = tamarExtractPoint(std::move(line));
            continue;
        }
        if (isHull || isInnerPoints)
        {
            size_t pos=0;
            size_t len=line.length();
            while (pos < len)
            {
                // HINT: at least in second char could be a ';'. -> pos+1
                size_t pos2 = line.find(";", pos+1);
                if (pos2 == std::string::npos)
                    pos2 = len;
                if (isHull)
                {
                    entry.hull.push_back( tamarExtractPoint(line.substr(pos, pos2-pos)) );
                } else {
                    entry.innerPoints.push_back( tamarExtractPoint(line.substr(pos, pos2-pos)) );
                }
                pos = pos2+1;       // +1, because omitt ';'
            }
            isHull = false;
            isInnerPoints = false;
        }
    }       // end while(debugFile >> line ...
}

void DialogGraphicsSceneResultDisplay::tamarDisplayEntry(size_t index)
{
    /// precondition
    if (index < 0 || index >= m_tamarDebugData.size())
        return;

    /// init
    mScene->clear();
    auto* data = &(m_tamarDebugData[index]);

    /// paint all points of result track elements
//    for (const auto &point : *mResultTrack->way())
//    {
//        GUI::Point * graphicPoint = new GUI::Point();
//        //mGraphicsItemMap[(x+y)*(x-y)+x+y] = point;    // TODO [seboeh] fix memory leak here and below.
//        graphicPoint->setPos(QPointF(point.x(), point.y()));
//        graphicPoint->setRect(-3,-3,6,6);
//        mScene->addItem(graphicPoint);
//    }

//    /// draw hull circle
//    TSP::Point nextHullPoint = StaticHelper::drawWay(mScene, &data->hull, data->hullPoint, true);

    /// draw inner circle
    TSP::Point nextInnerPoint = StaticHelper::drawWay(mScene, &data->innerPoints, data->innerPoint, true);
    if (nextInnerPoint.state() == TSP::Point::State::ISNULL)
    {       // there exists only one point in innerPoints.
        nextInnerPoint = data->innerPoint;
    }

    /// line to inner circle.
//    GUI::GraphicsLine * line = new GUI::GraphicsLine();
//    //mGraphicsItemMap[(x+y)*(x-y)+(xBefore+yBefore)*(xBefore-yBefore)] = line;     // TODO [seboeh] memory leak
//    line->setLine(data->hullPoint.x(), data->hullPoint.y(), data->innerPoint.x(), data->innerPoint.y());
//    line->setPen( QPen( QBrush(QColor(0,0,0)), 2 ) );
//    mScene->addItem(line);

//    /// line to hull circle
//    line = new GUI::GraphicsLine();
//    line->setLine(nextInnerPoint.x(), nextInnerPoint.y(), nextHullPoint.x(), nextHullPoint.y());
//    line->setPen( QPen( QBrush(QColor(0,0,0)), 2 ) );
//    mScene->addItem(line);

//    /// draw a point at innerPoint city
//    GUI::Point * point = new GUI::Point();
//    point->setPos(QPointF(data->innerPoint.x(), data->innerPoint.y()));
//    point->setRect(-3,-3,6,6);
//    point->setBrush(QBrush(QColor(0,255,0)));
//    mScene->addItem(point);

//    /// draw a point at hullPoint city
//    point = new GUI::Point();
//    //mGraphicsItemMap[(x+y)*(x-y)+x+y] = point;    // TODO [seboeh] fix memory leak here and below.
//    point->setPos(QPointF(data->hullPoint.x(), data->hullPoint.y()));
//    point->setRect(-3,-3,6,6);
//    point->setBrush(QBrush(QColor(0,255,255)));
//    mScene->addItem(point);
}

void DialogGraphicsSceneResultDisplay::on_pbPrev_clicked()
{
    if (false)      //m_mainAlgo == TSP::Parameters::AlgorithmType::Tamar)
    {
        if (!m_tamarIsPrev)
        {
            --m_tamarIndex;
            m_tamarIsPrev = true;
        }
        if (m_tamarIndex > 0)
            --m_tamarIndex;
        tamarDisplayEntry(m_tamarIndex);
    } else {
        if (mWayIter != mWays.begin()) {
            --mWayIter;
        } else {
            return;
        }
        if (mWayIter == mWays.end()) {
            return;
        }

        mScene->clear();
        StaticHelper::drawWay( mScene, *mWayIter, TSP::Point(), true );
        setParameters();
    }
}

void DialogGraphicsSceneResultDisplay::on_pbNext_clicked()
{
    if (false) //m_mainAlgo == TSP::Parameters::AlgorithmType::Tamar)
    {
        if (m_tamarDebugData.empty())
            tamarReadDebugFile();

        if (m_tamarIsPrev)
        {
            ++m_tamarIndex;
            m_tamarIsPrev = false;
        }
        tamarDisplayEntry(m_tamarIndex);
        if (m_tamarIndex < m_tamarDebugData.size())
        {
            ++m_tamarIndex;
        }
    } else {
        if (mWayIter != mWays.end()) {
            ++mWayIter;
        } else {
            return;
        }
        if (mWayIter == mWays.end()) {
            --mWayIter;
            return;
        }
        mScene->clear();
        StaticHelper::drawWay( mScene, *mWayIter, TSP::Point(), true );
        setParameters();
    }

}

void DialogGraphicsSceneResultDisplay::setParameters()
{
    double length = (*mWayIter)->length(m_para);
    length += m_para.distanceFnc(*(*mWayIter)->front(), *(*mWayIter)->back());
    ui->lbLength->setText( QString::number(length) );

}
