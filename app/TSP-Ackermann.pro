#-------------------------------------------------
#
# Project created by QtCreator 2014-04-06T10:37:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets     # necessary for graphicsscene

TARGET = shortcut
TEMPLATE = app

CCFLAG += -std=c++11
CONFIG += c++11

CCFLAG += --stack,1073741824

#linux qt include path
INCLUDEPATH += /opt/Qt/currentVersion/gcc_64/include/
#windows qt include path

#INCLUDEPATH += /usr/include/c++/4.8/ \
#        /usr/include/x86_64-linux-gnu/c++/4.8    # necessary for Bug:http://qt-project.org/forums/viewthread/28316 "moc: Parse error at “std"

INCLUDEPATH += TspSeboeh/

unix {
INCLUDEPATH += $(libpng-config --cflags)
}
windows {
INCLUDEPATH += lib/libpng
}

unix {
LIBS += -L../build-TspSeboeh-Desktop-Debug/ -lTspSeboeh
}
windows {
#LIBS += -L../build-TspSeboeh-Desktop-Debug/debug -lTspSeboeh
}
windows {
LIBS += -L../build-TspSeboeh-Desktop-Release/release -lTspSeboeh
}


unix {
LIBS += -L/usr/lib/x86_64-linux-gnu/ `libpng-config --ldflags`
}
windows {
LIBS += -L../trunk/lib/libpng -lwin_png -lz -lm
}

unix {
LIBS += -L/opt/Qt/currentVersion/gcc_64/
}

SOURCES += main.cpp\
    DiagramScene.cpp \
    TspAckermannMain.cpp \
    GraphicsPoint.cpp \
    GraphicsLine.cpp \
    DialogGraphicsSceneResultDisplay.cpp \
    DialogSaveAsImageSettings.cpp \
    DialogEnterPoint.cpp \
    DialogDebug.cpp \
    StaticHelper.cpp

HEADERS  += TspAckermannMain.h \
    DiagramScene.h \
    GraphicsPoint.h \
    GraphicsLine.h \
    DialogGraphicsSceneResultDisplay.h \
    DialogSaveAsImageSettings.h \
    DialogEnterPoint.h \
    DialogDebug.h \
    StaticHelper.h

FORMS    += tspackermannmain.ui \
    DialogGraphicsSceneResultDisplay.ui \
    DialogSaveAsImageSettings.ui \
    DialogEnterPoint.ui \
    DialogDebug.ui
