#ifndef DIALOGENTERPOINT_H
#define DIALOGENTERPOINT_H

#include <QDialog>

namespace Ui {
class DialogEnterPoint;
}

class DialogEnterPoint : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEnterPoint(QWidget *parent = 0);
    ~DialogEnterPoint();

    float x();
    float y();

private:
    Ui::DialogEnterPoint *ui;
};

#endif // DIALOGENTERPOINT_H
