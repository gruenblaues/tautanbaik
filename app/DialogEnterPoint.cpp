#include "DialogEnterPoint.h"
#include "ui_DialogEnterPoint.h"

DialogEnterPoint::DialogEnterPoint(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogEnterPoint)
{
    ui->setupUi(this);
}

DialogEnterPoint::~DialogEnterPoint()
{
    delete ui;
}

float DialogEnterPoint::x()
{
    return static_cast<float>( ui->sbXAchsis->value() );
}

float DialogEnterPoint::y()
{
    return static_cast<float>( ui->sbYAchsis->value() );
}
