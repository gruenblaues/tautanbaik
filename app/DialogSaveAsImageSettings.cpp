#include "DialogSaveAsImageSettings.h"
#include "ui_DialogSaveAsImageSettings.h"

DialogSaveAsImageSettings::DialogSaveAsImageSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSaveAsImageSettings)
{
    ui->setupUi(this);
}

DialogSaveAsImageSettings::~DialogSaveAsImageSettings()
{
    delete ui;
}

DialogSaveAsImageSettings::SaveAsState DialogSaveAsImageSettings::saveType()
{
    if (ui->rbSaveModeOnlyMetaInfo->isChecked())
    {
        return SaveAsState::OnlyMeta;
    } else if (ui->rbSaveModeOnlyDots->isChecked())
    {
        return SaveAsState::OnlyDots;
    } else {
        return SaveAsState::Graph;
    }
}
