#ifndef STATICHELPER_H
#define STATICHELPER_H

#include <QGraphicsScene>
#include <stdexcept>
#include <iostream>
#include <stdarg.h>
#include <stdio.h>
#include <string>
#include <exception>

#include "Way.h"
#include "bspTreeVersion/BspTree.h"
#include "TspFacade.h"

class StaticHelper
{
public:

    static TSP::Point drawWay(QGraphicsScene * scene, TSP::Way * resultTrack, TSP::Point hullPoint=TSP::Point(), bool withHullVector = false);

    // TODO [seboeh] this is code duplication - also used in SeboehTSP lib. Should be moved to own lib.
    static void logAndThrow(const char * s, ...) throw (std::logic_error)
    {
        va_list args;
        va_start(args, s);
        char * outStr = new char[1000];        // This is a limit of 1000.
        vsnprintf(outStr, 1000, s, args);
        va_end(args);
        outStr[999] = '\0';
        std::string outString = outStr;
        delete [] outStr;
        std::cerr << outString << std::endl;   // logging - better to log more than too less.
        throw std::logic_error(outString.c_str());
    }

    static void drawBsp(QGraphicsScene * scene, bsp::BspTree *bspTree, TSP::Way * resultTrack);

    static void drawBspLines(QGraphicsScene * scene, bsp::BspNode* node, double width, double height);

    TSP::Way *fillWay(TSP::DT::DTNode::NodeType &parent);
};


#endif // STATICHELPER_H
