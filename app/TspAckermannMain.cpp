#include "TspAckermannMain.h"
#include "ui_tspackermannmain.h"
#include "qmutex.h"

#include <iostream>
#include <QFileDialog>
#include <stdexcept>
#include <QRgb>
#include <QMessageBox>
#include <locale>

#include "TspFacade.h"
#include "GraphicsPoint.h"
#include "GraphicsLine.h"
#include "DialogGraphicsSceneResultDisplay.h"
#include "StaticHelper.h"
#include "PngImage.h"
#include "DialogSaveAsImageSettings.h"
#include "DialogEnterPoint.h"
#include "DialogDebug.h"

// for specifying the parameters like distanceFnc.
#include "TspAlgoSeboeh.h"

using namespace GUI;

TSPAckermannMain::TSPAckermannMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TSPAckermannMain),
    mTsp(NULL),
    m_tspNaive(NULL)
{
    ui->setupUi(this);

    createActions();

    /// setup diagramScene
    mDiagramSceneTSPAcker = new DiagramScene(this);
    ui->graphicsView->setScene(mDiagramSceneTSPAcker);
    /// size of the scene will be set to graphicsView.size in method resizeEvent.
    mDiagramSceneTSPNaive = new DiagramScene(this);
    ui->graphicsViewTSPNaive->setScene(mDiagramSceneTSPNaive);

    /// etc.
    setWindowTitle("Travel-Sales-Man Calculator ");

    /// initialize comboBox of Algorithms.
    ui->cbAlgorithmType->addItem("SeboehAckermann");
    ui->cbAlgorithmType->addItem("Tamar");
    ui->cbAlgorithmType->addItem("Tamar Exp(best)");
    ui->cbAlgorithmType->addItem("seboehDistAlg");
    ui->cbAlgorithmType->addItem("VoronoiAreaSort");
    ui->cbAlgorithmType->addItem("VoronoiClusterSort");
    ui->cbAlgorithmType->addItem("VoronoiClusterDescent");
    ui->cbAlgorithmType->addItem("VoronoiAreaPairGrow");
    ui->cbAlgorithmType->addItem("VoronoiConvexAlignment");
    ui->cbAlgorithmType->addItem("MinMaxClusterAlignment");
    ui->cbAlgorithmType->addItem("DetourClusterAlignment");
    ui->cbAlgorithmType->addItem("HierarchicalBreadthSplitting");
    ui->cbAlgorithmType->addItem("HierarchicalClusteredAlignment");
    ui->cbAlgorithmType->setCurrentIndex(12);

    setWindowState(Qt::WindowMaximized);
}

TSPAckermannMain::~TSPAckermannMain()
{
    clearDiagramElements();
    delete mDiagramSceneTSPAcker;
    delete mDiagramSceneTSPNaive;
    delete ui;
    if (mTsp) delete mTsp;
    if (m_tspNaive) delete m_tspNaive;
}

void TSPAckermannMain::showEvent(QShowEvent *)
{
    int width = ui->graphicsView->rect().width();
    int height = ui->graphicsView->rect().height();
    mDiagramSceneTSPAcker->setSceneRect( 0, 0, width, height );
    mDiagramSceneTSPNaive->setSceneRect( 0, 0, width, height );
}

void TSPAckermannMain::resizeEvent(QResizeEvent * event)
{
    QWidget::resizeEvent(event);
    QRect viewRect = ui->graphicsView->rect();
    QRect imageRect;
    if (!m_diagramBackgroundImage.isNull()) {
        imageRect = m_diagramBackgroundImage.rect();
    } else {
        imageRect = QRect(0,0,0,0);
    }
    QRect rectResult = QRect(0,0,
                             (std::max)(imageRect.width(), viewRect.width())
                             , (std::max)(imageRect.height(), viewRect.height()));
    if (mDiagramSceneTSPAcker->items().size() > 0)
    {
        // HINT: a rect of null in setSceneRect() will cause the graphicsView to resize according to inserted points.
        mDiagramSceneTSPAcker->setSceneRect( QRect() );
        mDiagramSceneTSPNaive->setSceneRect( QRect() );
    } else {
        mDiagramSceneTSPAcker->setSceneRect( rectResult );
        mDiagramSceneTSPNaive->setSceneRect( rectResult );
    }
}

void TSPAckermannMain::createActions() {

    connect(ui->pushButtonRun, SIGNAL(clicked()), this, SLOT(onRunClicked()));
    connect(ui->pbShowWays, SIGNAL(clicked()), this, SLOT(onShowWaysClicked()));
    connect(ui->actionLoad_image, SIGNAL(triggered()), this, SLOT(onLoadImageTriggered()));
    connect(ui->action_clear_scene, SIGNAL(triggered()), this, SLOT(onClearTriggered()));
    connect(ui->actionClear_last_point, SIGNAL(triggered()), this, SLOT(onClearPointTriggered()));
    connect(ui->action_save_as_image, SIGNAL(triggered()), this, SLOT(onSaveImageTriggered()));
    connect(ui->action_add_new_point, SIGNAL(triggered()), this, SLOT(onAddNewPointTriggered()));
}

void TSPAckermannMain::includeTimeDuration(QString &logText, const std::chrono::system_clock::time_point &startTime)
{
    std::chrono::seconds timeDiffSec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - startTime);
    long secs = timeDiffSec.count();
    logText += " " + QString::number(secs/(60*60*24));  // days
    secs %= (60*60*24);
    logText += " " + QString::number(secs/(60*60));     // hours
    secs %= (60*60);
    logText += ":" + QString::number(secs/60);          // mins
    secs %= 60;
    logText += ":" + QString::number(secs);             // secs
}

void TSPAckermannMain::drawPartialResultsInDTTree()
{
    if (mDTTree == nullptr
            || mDTTree->root() == nullptr
            || mDTTree->root()->children() == nullptr
            || mDTTree->root()->children()->empty())
        return;
    auto *ways = mDTTree->root()->children();
    if (mLastDTTreeSize + 2 <= ways->size())
    {
        // * clear whole canvas
        mDiagramSceneTSPAcker->clear();

        // * paint main task points again
        for (TSP::TSPFacade::PointListType::iterator cityIter= mCities.begin();
             cityIter != mCities.end(); ++cityIter)
        {
            GUI::Point *gPoint = new GUI::Point();
            qreal x = cityIter->first;
            qreal y = cityIter->second;
            gPoint->setX(x);
            gPoint->setY(y);
            gPoint->setRect(-5,-5,10,10);
            mDiagramSceneTSPAcker->addItem(gPoint);
            QGraphicsTextItem * text = new QGraphicsTextItem(QString("%1x%2").arg(x).arg(y));
            text->setFont(QFont("Courier News", 8));
            text->setPos(x-20, y-20);
            mDiagramSceneTSPAcker->addItem(text);
        }

        // * paint pre-last and last way from children

    }

}

void TSPAckermannMain::onRunClicked()
{
    if (m_isFilledWithResultWay == true)
    {
        QMessageBox msg;
        msg.setWindowTitle(QString("Usage-Information"));
        msg.setText(QString("Please clear the area with CTR+C and load the file again, before rerun.\n"
                             "This is because the dots in the area will be used for calculation.\n"
                             "After a run, there are new dots and lines in this area, which should not include again.")
                    );
        msg.exec();
        return;
    }

    ui->progressBar->setMaximum(1000);
    ui->labelStateTSPAckermann->setText("running ...");

    /// 1. collect points from graphicsScene.
    foreach (QGraphicsItem * item, mDiagramSceneTSPAcker->items())
    {
        GUI::Point *gPoint = dynamic_cast<GUI::Point*>(item);
        if (gPoint) {
            std::pair<float, float> tspPoint = std::pair<float,float>( gPoint->x(), gPoint->y() );
            mCities.push_front(tspPoint);        /// push_front because QGraphicsScene::addItem is push_front.
        }
    }

    /// 2. run the selected algorithm
    if (mTsp) {
        delete mTsp;
    }
    if (ui->cbAlgorithmType->currentText().compare("SeboehAckermann") == 0) {
        // run the algorithm seboeh
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::Ackermann);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
        m_para.findNextPointFnc = TSP::TSPAlgoSeboeh::findNextPointFnc;
        m_para.decissionTreeChildCountFnc = TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc;
        m_para.decissionTreeWeightFnc = TSP::TSPAlgoSeboeh::decissionTreeWeightFnc;
    } else if (ui->cbAlgorithmType->currentText().compare("Tamar") == 0) {
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::Tamar);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("Tamar Exp(best)") == 0) {
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::TamarExp);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("seboehDistAlg") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::SeboehDistAlg);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("VoronoiAreaSort") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::VoronoiAreaSort);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("VoronoiClusterSort") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::VoronoiClusterSort);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("VoronoiClusterDescent") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::VoronoiClusterDescent);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("VoronoiAreaPairGrow") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::VoronoiAreaPairGrow);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("VoronoiConvexAlignment") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::VoronoiConvexAlignment);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("MinMaxClusterAlignment") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::MinMaxClusterAlignment);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("DetourClusterAlignment") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::DetourClusterAlignment);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("BellmannHeldKarp") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::BellmannHeldKarp);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("BellmannHeldKarpInternet") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::BellmannHeldKarpInternet);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("HierarchicalBreadthSplitting") == 0){
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::HierarchicalBreadthSplitting);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    } else if (ui->cbAlgorithmType->currentText().compare("HierarchicalClusteredAlignment") == 0) {
        mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::HierarchicalClusteredAlignment);
        mTsp->addInput(mCities);
        mTsp->setDefaultParameters(&m_para);
    }

    /// save dots into last.png
    saveAsImage("last.png", DialogSaveAsImageSettings::SaveAsState::OnlyDots);
    mTsp->startRun(m_para);

    if (ui->cbWithNaiveAlgo->isChecked()) {
        /// 4. run naive version.
        ui->labelStateTSPNaive->setText("running ...");
        if (m_tspNaive) {
            delete m_tspNaive;
        }
        m_tspNaive = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::BellmannHeldKarpInternet);
        m_tspNaive->addInput(mCities);
        m_para.m_naiveRunMinutes = m_naiveRunMinutes;
        m_tspNaive->setDefaultParameters(&m_para);
        m_tspNaive->startRun(m_para);
    } else {
       ui->labelStateTSPNaive->setText("not running!");
    }

    /// 5. wait for seboeh Algo
    /// and wait for naive Algo
    bool isNaive = ui->cbWithNaiveAlgo->isChecked() && m_tspNaive;
    TSP::Indicators indi;
    TSP::Indicators indi_naive;
    bool finished = false;
    bool finished_naive = false;
    bool isDrawn = false;
    bool isDrawn_naive = false;
    mLastDTTreeSize = 0;
    if (mDTTree != nullptr
            && mDTTree->root() != nullptr
            && mDTTree->root()->children() != nullptr)
    {
        mDTTree->root()->children()->clear();
    }
    std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
    while ((!finished || !finished_naive) && (!isDrawn || !isDrawn_naive)) {
        indi = mTsp->status();
        finished = (indi.state()
                          & (TSP::Indicators::States::ISFINISHED | TSP::Indicators::States::ISCANCELED));
        if (isNaive)
        {
            indi_naive = m_tspNaive->status();
            finished_naive = (indi_naive.state()
                              & (TSP::Indicators::States::ISFINISHED | TSP::Indicators::States::ISCANCELED));
            ui->progressBar->setValue((indi.progress()+indi_naive.progress()) * 50);        // 50 = 100/2
        } else {
            finished_naive = true;
            ui->progressBar->setValue(indi.progress()*100);
        }
        drawPartialResultsInDTTree();
        QApplication::processEvents();
        QThread::msleep(100);

        if (finished && !isDrawn)
        {
            /// 6. draw the result of the algorithm
            mTsp->wait();
            // later used for debugging reasons.
            mDTTree = mTsp->getDtTree();
            auto res = mTsp->getResults();
            StaticHelper::drawWay( mDiagramSceneTSPAcker, res->way() );
            QString logText;
            logText = "length: " + QString::number(res->wayLength());
            logText += " time:";
            includeTimeDuration(logText, startTime);
            ui->labelStateTSPAckermann->setText(logText);
            QApplication::processEvents();
            isDrawn = true;
        }

        if (finished_naive && !isDrawn_naive)
        {
            /// 7. draw the naive result.
            if (isNaive) {
                m_tspNaive->wait();
                /// HINT: It is not possible to demonstrate the whole decission treee for naive version.
                /// The tree is too big. (mDTTree = m_tspNaive->getDtTree();)
                StaticHelper::drawWay( mDiagramSceneTSPNaive, m_tspNaive->getResultTypeWay() );
                QString logText;
                if (indi_naive.state() & TSP::Indicators::States::ISCANCELED)
                    logText = "interrupted - result took longer than time-limit : ";
                logText += "length: " + QString::number(m_tspNaive->getResults()->wayLength());
                logText += " time:";
                includeTimeDuration(logText, startTime);
                ui->labelStateTSPNaive->setText(logText);
                QApplication::processEvents();
            }
            isDrawn_naive = true;
        }
    }

    // calculation and drawing finished.
    m_isFilledWithResultWay = true;

    ui->progressBar->setValue(ui->progressBar->maximum());
}


void TSPAckermannMain::onShowWaysClicked()
{
    if (mTsp == nullptr)
        return;

    TSP::Parameters::AlgorithmType algoType = TSP::Parameters::AlgorithmType::Ackermann;        // default
    if ((ui->cbAlgorithmType->currentText().compare("Tamar") == 0)
            || (ui->cbAlgorithmType->currentText().compare("Tamar Exp(best)") == 0))
    {       // TODO [seboeh] if is hack.
        algoType = TSP::Parameters::AlgorithmType::Tamar;
    }

    DialogGraphicsSceneResultDisplay * dlg = new DialogGraphicsSceneResultDisplay(this, algoType, m_para
                                                                                  , mDiagramSceneTSPAcker->width()
                                                                                  , mDiagramSceneTSPAcker->height());
    TSP::Result * results = mTsp->getResults();
    dlg->exec(mDTTree, results->bspTree(), results->way());
    delete dlg;
}

void TSPAckermannMain::onLoadImageTriggered()
{
    /// 1. open dialog for selecting the file.
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.png_toobig)"));
    setWindowTitle(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    /// 2. set the background of graphicsScene with the new image
    QImage image(fileName);
    mDiagramSceneTSPAcker->setSceneRect(image.rect());
    mDiagramSceneTSPNaive->setSceneRect(image.rect());
    mDiagramSceneTSPAcker->setBackgroundBrush(QBrush(image));
    m_diagramBackgroundImage = image;

    /// 3. load the saved TSP-Way from the png image.
    PngImage pngImg(1,1);
    try {
        pngImg.read_png_file(fileName.toStdString());
    } catch (std::invalid_argument &e) {
        QMessageBox::information(this, "Information",
                              QString("Could not load meta data from the image\nReason:\n%1")
                              .arg(e.what()));
        return;
    }

    PngImage::PngText pngTxt;
	pngTxt.text = nullptr;		// basic initialization later used.
    int num;
    PngImage::PngText * pngTxtP = NULL;
    try {
        size_t s = strlen(PNG_TEXT_KEY_TSPWAY);
        char * key = new char[s+1];
        strncpy(key, PNG_TEXT_KEY_TSPWAY, s);
        key[s] = '\0';
        pngTxtP = pngImg.get_text_chunk(key, &num);
    } catch (std::invalid_argument &e) {
        QMessageBox::information( this, "Information"
                                 , QString("Could not load the meta-information.\n\nReason:%1")
                                 .arg(e.what()) );
        return;
    }
    if (num == 0 || pngTxtP == nullptr)
    {
        /// Read points if available.
        try {
            size_t s = strlen(PNG_TEXT_KEY_DOTS);
            char * key = new char[s+1];
            strncpy(key, PNG_TEXT_KEY_DOTS, s);
            key[s] = '\0';
            pngTxtP = pngImg.get_text_chunk(key, &num);
        } catch (std::invalid_argument &e) {
            QMessageBox::information( this, "Information"
                                     , QString("Could not load the dots-information.\n\nReason:%1")
                                     .arg(e.what()) );
            return;
        }
        if (num > 0 && pngTxtP != nullptr) {
            for (int i=0; i < num; ++i) {
                if ( strncmp(pngTxtP[i].key, PNG_TEXT_KEY_DOTS, strlen(PNG_TEXT_KEY_DOTS)) == 0 ) {
                    // FIXME: Because of unclear reasons pngTxtP contains all texts and not only TSPWay, although get_text_chunk().
                    pngTxt = pngTxtP[i];
                }
            }
        }
    } else {
        for (int i=0; i < num; ++i) {
            if ( strncmp(pngTxtP[i].key, PNG_TEXT_KEY_TSPWAY, strlen(PNG_TEXT_KEY_TSPWAY)) == 0 ) {
                pngTxt = pngTxtP[i];
                break;
            }
            // HINT: here are both PNG_TEXT_KEY compares, because this is the main path. The above if is only for security, if pnglib would change/improve.
            if ( strncmp(pngTxtP[i].key, PNG_TEXT_KEY_DOTS, strlen(PNG_TEXT_KEY_DOTS)) == 0 ) {
                pngTxt = pngTxtP[i];
                break;
            }
        }
    }

    if (num != 0 && pngTxtP != nullptr) {
        std::string pngCitiesStr = pngTxt.text;
        QString pngCitiesString(pngCitiesStr.c_str());
        QStringList pngCities = pngCitiesString.split(QRegExp("[,;]"));
        if (pngCities.size() % 2 != 0) {
            QMessageBox::critical(this, "Error",
                QString("Could not read the TSPWay. List of Points corrupted. Size of coordinates is not odd (size =%1)").arg(pngCities.size()) );
            return;
        }
        for (QStringList::iterator pngCityIter= pngCities.begin();
             pngCityIter != pngCities.end(); ++pngCityIter)
        {
            bool ok=true;
            int x = pngCityIter->toFloat(&ok);
            ++pngCityIter;      // omit the point y - this is possible due to check on % 2 above.
            if (!ok) {
                continue;
            }
            int y = pngCityIter->toFloat(&ok);
            if (!ok) {
                continue;
            }
            GUI::Point *gPoint = new GUI::Point();
            gPoint->setX(x);
            gPoint->setY(y);
            gPoint->setRect(-5,-5,10,10);
            mDiagramSceneTSPAcker->addItem(gPoint);
            QGraphicsTextItem * text = new QGraphicsTextItem(QString("%1x%2").arg(x).arg(y));
            text->setFont(QFont("Courier News", 8));
            text->setPos(x-20, y-20);
            mDiagramSceneTSPAcker->addItem(text);
        }

    }
}

void TSPAckermannMain::onClearTriggered()
{
    /// 1. clear items from memory-buffer mGraphicsItem - set in \see StaticHelper::drawWay().
    clearDiagramElements();
    mDiagramSceneTSPAcker->clear();
    mDiagramSceneTSPNaive->clear();
    mDiagramSceneTSPAcker->backgroundBrush().setTextureImage( QImage() );
    m_isFilledWithResultWay = false;
}

void TSPAckermannMain::clearDiagramElements()
{
    for (std::map<float,QGraphicsItem*>::iterator itemIter = mGraphicsItemMap.begin();
         itemIter != mGraphicsItemMap.end(); ++itemIter)
    {
        delete itemIter->second;
        itemIter->second = NULL;
    }
    mGraphicsItemMap.clear();
}

void TSPAckermannMain::onClearPointTriggered()
{
    if (! mDiagramSceneTSPAcker->items().isEmpty() ) {
        mDiagramSceneTSPAcker->removeItem( mDiagramSceneTSPAcker->items().front() );
    }
}

void TSPAckermannMain::saveAsImage(std::string filename, DialogSaveAsImageSettings::SaveAsState saveType)
{
    /// 1. paint graphics scene to image
    QRectF rect = mDiagramSceneTSPAcker->sceneRect();
    QImage image(static_cast<size_t>(rect.width()), static_cast<size_t>(rect.height()), QImage::Format_ARGB32);
    size_t width = image.width();
    size_t height = image.height();

    /// 1.5 save background only, if is only meta info selected.
    if (saveType == DialogSaveAsImageSettings::SaveAsState::Graph) {
        QImage backgrImg = mDiagramSceneTSPAcker->backgroundBrush().textureImage();
        if (!backgrImg.isNull() && backgrImg.width() > 0 && backgrImg.height() > 0) {
            image = mDiagramSceneTSPAcker->backgroundBrush().textureImage().copy();
        }
        QPainter painter(&image);
        mDiagramSceneTSPAcker->render(&painter);
    } else {
        // write white to the QImage.
        for (size_t y=0; y<height; y++) {
            for (size_t x=0; x < width; x++) {
                image.setPixel(x, y, qRgb( 255, 255, 255));
            }
        }
    }

    /// 2. copy that image to png-image of libpng
    PngImage pngImage(width,height);
    try {
        pngImage.checkPixelFormat();
    } catch (std::invalid_argument &e) {
        QMessageBox::critical(this, "Error",
                              QString("Could not save the image.\n\nReason:\n%1")
                              .arg(e.what()));
        return;
    }

    for (size_t y=0; y<height; y++) {
            PngImage::PngByte* row = pngImage.row_pointers_at(y);
            for (size_t x=0; x<width; x++) {
                QRgb pixel = image.pixel(x, y);
                PngImage::PngByte* ptr = &(row[x*4]);
                ptr[0] = qRed(pixel);
                ptr[1] = qGreen(pixel);
                ptr[2] = qBlue(pixel);
                ptr[3] = qAlpha(pixel);
            }
    }

    /// 3. write the text chunks (the calculated TSP-Way) to png image.

    PngImage::PngText pngTxt;

    pngTxt.compression = PNG_TEXT_COMPRESSION_NONE;
    char keyTitle[] = "Title";
    size_t s = strlen(keyTitle);
    pngTxt.key = new char[s+1];
    strncpy(pngTxt.key, keyTitle, s);
    pngTxt.key[s] = '\0';
    char textTitle[] = "TSP-Way by Sebastian Boehmer";
    s = strlen(textTitle);
    pngTxt.text = new char[s+1];
    strncpy(pngTxt.text, textTitle, s);
    pngTxt.text[s] = '\0';
    pngTxt.text_length = s;
    pngImage.set_text_chunk(&pngTxt);

    char keyAuthor[] = "Author";
    s = strlen(keyAuthor);
    pngTxt.key = new char[s+1];
    strncpy(pngTxt.key, keyAuthor, s);
    pngTxt.key[s] = '\0';
    char textAuthor[] = "created by Sebastian Boehmer";
    s = strlen(textAuthor);
    pngTxt.text = new char[s+1];
    strncpy(pngTxt.text, textAuthor, s);
    pngTxt.text[s] = '\0';
    pngTxt.text_length = s;
    pngImage.set_text_chunk(&pngTxt);

    setlocale(LC_ALL, "C");

    char * pngWayStr = nullptr;
    if (saveType == DialogSaveAsImageSettings::SaveAsState::Graph
            || saveType == DialogSaveAsImageSettings::SaveAsState::OnlyMeta)
    {
        char keyWay[] = PNG_TEXT_KEY_TSPWAY;
        size_t s = strlen(keyWay);
        pngTxt.key = new char[s+1];
        strncpy(pngTxt.key, keyWay, s);
        pngTxt.key[s] = '\0';
        std::string wayStr("");
        char number[51];
        if (mTsp)
        {
            const TSP::Way::WayType * way = mTsp->getResultTypeWay()->way();
            if (way && !way->empty()) {
                for (TSP::Way::WayType::const_iterator cityIter=way->begin();
                     cityIter != way->end(); ++cityIter)
                {
                    snprintf(&number[0], 50, "%10.10f,%10.10f;", static_cast<float>(cityIter->x()), static_cast<float>(cityIter->y()));
                    wayStr += number;
                }
            }
        }
        pngWayStr = new char[wayStr.length()+1];
        strncpy(pngWayStr, wayStr.c_str(), wayStr.length());
        pngWayStr[wayStr.length()-1] = '\0';        // deletes the last ';'
        pngTxt.text = pngWayStr;
        pngTxt.text_length = wayStr.length()-1;
        pngImage.set_text_chunk(&pngTxt);
    } else if (saveType == DialogSaveAsImageSettings::SaveAsState::OnlyDots)
    {
        char keyDots[] = PNG_TEXT_KEY_DOTS;
        size_t s = strlen(keyDots);
        pngTxt.key = new char[s+1];
        strncpy(pngTxt.key, keyDots, s);
        pngTxt.key[s] = '\0';
        std::string dotsStr;
        char number[51];
        foreach (QGraphicsItem * item, mDiagramSceneTSPAcker->items())
        {
            GUI::Point *gPoint = dynamic_cast<GUI::Point*>(item);
            if (gPoint) {
                snprintf( &number[0], 50, "%10.10f,%10.10f;", static_cast<float>(gPoint->x()), static_cast<float>(gPoint->y()) );
                dotsStr += number;
            }
        }
        pngWayStr = new char[dotsStr.length()+1];
        strncpy(pngWayStr, dotsStr.c_str(), dotsStr.length());
        pngWayStr[dotsStr.length()-1] = '\0';       // deletes the last ';'
        pngTxt.text = pngWayStr;
        pngTxt.text_length = dotsStr.length()-1;
        pngImage.set_text_chunk(&pngTxt);
    }

    /// 4. write the image .
    pngImage.write_png_file(filename);

    delete [] pngWayStr;
}

void TSPAckermannMain::onSaveImageTriggered()
{
    DialogSaveAsImageSettings settings;
    int result = settings.exec();
    if (result != QDialog::Accepted) {
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save Image"), "", tr("Image Files (*.png)"));
    if (!fileName.isEmpty()) {
        saveAsImage(fileName.toStdString(), settings.saveType());
    }
}

void TSPAckermannMain::onAddNewPointTriggered()
{
    DialogEnterPoint enterPointDlg;
    if (enterPointDlg.exec() != QDialog::Accepted) {
        return;
    } else {
        GUI::Point *gPoint = new GUI::Point();
        auto x = enterPointDlg.x();
        auto y = enterPointDlg.y();
        gPoint->setX( x );
        gPoint->setY( y );
        gPoint->setRect(-5,-5,10,10);
        mDiagramSceneTSPAcker->addItem(gPoint);
        QGraphicsTextItem * text = new QGraphicsTextItem(QString("%1x%2").arg(x).arg(y));
        text->setFont(QFont("Courier News", 8));
        text->setPos(x-20, y-20);
        mDiagramSceneTSPAcker->addItem(text);
    }
}
