#ifndef DIALOGGRAPHICSSCENERESULTDISPLAY_H
#define DIALOGGRAPHICSSCENERESULTDISPLAY_H

#include <QDialog>
#include <QGraphicsScene>

#include <vector>

#include "Way.h"
#include "DTTree.h"
#include "TspFacade.h"
#include "Parameters.h"
#include "bspTreeVersion/BspTree.h"

namespace Ui {
class DialogGraphicsSceneResultDisplay;
}

class DialogGraphicsSceneResultDisplay : public QDialog
{
    Q_OBJECT

public:
    explicit DialogGraphicsSceneResultDisplay(QWidget *parent, TSP::Parameters::AlgorithmType mainAlgo, TSP::Parameters para, size_t width, size_t height);
    ~DialogGraphicsSceneResultDisplay();

    void exec(TSP::DT::DTTree* dtRoot, bsp::BspTree* bspTree, TSP::Way * resultTrack);

private slots:
    void on_pbPrev_clicked();

    void on_pbNext_clicked();

private:
    void fillWays(TSP::DT::DTTree::NodeType* node);
    void setParameters();
    void clearWays();

private:
    Ui::DialogGraphicsSceneResultDisplay *ui;
    std::list<TSP::Way*> mWays;
    std::list<TSP::Way*>::iterator mWayIter;
    QGraphicsScene * mScene;
    TSP::Parameters     m_para;
    TSP::Way            *mResultTrack;
    TSP::Parameters::AlgorithmType m_mainAlgo;

    size_t              m_tamarIndex=0;
    bool                m_tamarIsPrev=false;
    struct TamarDebugEntry
    {
        TSP::Way hull;
        TSP::Way innerPoints;
        TSP::Point hullPoint;
        TSP::Point innerPoint;
        void clear()
        {
            hull.clear();
            innerPoints.clear();
            hullPoint.state(TSP::Point::State::ISNULL);
            innerPoint.state(TSP::Point::State::ISNULL);
        }
    };
    using TamarDebugDataType = std::vector<TamarDebugEntry>;
    TamarDebugDataType m_tamarDebugData;
    TSP::Point tamarExtractPoint(std::string str);
    void tamarReadDebugFile();
    void tamarDisplayEntry(size_t index);
};

#endif // DIALOGGRAPHICSSCENERESULTDISPLAY_H
