#include "DialogDebug.h"
#include "ui_DialogDebug.h"

DialogDebug::DialogDebug(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDebug)
{
    ui->setupUi(this);
}

DialogDebug::~DialogDebug()
{
    delete ui;
}

QLabel * DialogDebug::getGraphicsDevice1()
{
    return ui->lbImage;
}

QLabel * DialogDebug::getGraphicsDevice2()
{
    return ui->lbImage2;
}
