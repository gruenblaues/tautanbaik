#include "StaticHelper.h"
#include <QImage>

#include "GraphicsLine.h"
#include "GraphicsPoint.h"


TSP::Point StaticHelper::drawWay(QGraphicsScene * scene, TSP::Way * resultTrack, TSP::Point hullPoint, bool withHullVector)
{
    Q_ASSERT(scene != NULL);
    if (resultTrack->state() & TSP::Way::State::ISNULL) {
        return TSP::Point();
    }
    if (resultTrack->way()->empty()) {
        return TSP::Point();
    }

    TSP::Point returnPoint;

    /// 1. walk through all cities in the resultTrack
    TSP::Point cityBefore = resultTrack->way()->front();
    for (TSP::Way::WayType::const_iterator cityIter = resultTrack->way()->begin();
         cityIter != resultTrack->way()->end(); ++cityIter)
    {
        if (cityBefore == hullPoint)
        {
            returnPoint = cityBefore = *cityIter;
            continue;
        }

        /// 1.1 draw a point at this city
        float x = cityIter->x();
        float y = cityIter->y();
        float xBefore = cityBefore.x();
        float yBefore = cityBefore.y();

        GUI::Point * point = new GUI::Point();
        //mGraphicsItemMap[(x+y)*(x-y)+x+y] = point;
        point->setPos(QPointF(x,y));
        point->setRect(-3,-3,6,6);
        scene->addItem(point);

        /// 1.2 and draw a line to next city.
        GUI::GraphicsLine * line = new GUI::GraphicsLine();
        //mGraphicsItemMap[(x+y)*(x-y)+(xBefore+yBefore)*(xBefore-yBefore)] = line;
        line->setLine(xBefore, yBefore, x, y);
        switch (0) // cityIter->number())
        {
        case 0 :
            line->setPen( QPen( QBrush(QColor(0,0,128)), 2 ) );
            break;
        case 1 :
            line->setPen( QPen( QBrush(QColor(0,0,255)), 2 ) );
            break;
        case 2 :
            line->setPen( QPen( QBrush(QColor(0,128,128)), 2 ) );
            break;
        case 3 :
            line->setPen( QPen( QBrush(QColor(0,255,128)), 2 ) );
            break;
        case 4 :
            line->setPen( QPen( QBrush(QColor(0,128,0)), 2 ) );
            break;
        case 5 :
            line->setPen( QPen( QBrush(QColor(0,255,0)), 2 ) );
            break;
        case 6 :
            line->setPen( QPen( QBrush(QColor(128,128,0)), 2 ) );
            break;
        case 7 :
            line->setPen( QPen( QBrush(QColor(255,128,0)), 2 ) );
            break;
        case 8 :
            line->setPen( QPen( QBrush(QColor(128,0,0)), 2 ) );
            break;
        default :
            line->setPen( QPen( QBrush(QColor(255,0,0)), 2 ) );
            break;
        }
        scene->addItem(line);

        QGraphicsTextItem * text = new QGraphicsTextItem(QString("%1x%2").arg(x).arg(y));
        text->setFont(QFont("Courier News", 8));
        text->setPos(x-20, y-20);
        scene->addItem(text);

        if (withHullVector && cityIter->hullVector != nullptr)
        {
            line = new GUI::GraphicsLine();
            double hx = cityIter->hullVector->x();
            double hy = cityIter->hullVector->y();
            double length = std::sqrt(hx*hx + hy*hy);
            line->setLine(x, y, x + hx*30/length, y + hy*30/length);
            line->setPen( QPen( QBrush(QColor(128,128,128)), 1 ) );
            scene->addItem(line);

            if (cityIter->hullVectorSecond != nullptr)
            {
                line = new GUI::GraphicsLine();
                hx = cityIter->hullVectorSecond->x();
                hy = cityIter->hullVectorSecond->y();
                length = std::sqrt(hx*hx + hy*hy);
                line->setLine(x, y, x + hx*20/length, y + hy*20/length);
                line->setPen( QPen( QBrush(QColor(128,128,128)), 1 ) );
                scene->addItem(line);
            }

            if (cityIter->hullVectorThird != nullptr)
            {
                line = new GUI::GraphicsLine();
                hx = cityIter->hullVectorThird->x();
                hy = cityIter->hullVectorThird->y();
                length = std::sqrt(hx*hx + hy*hy);
                line->setLine(x, y, x + hx*10/length, y + hy*10/length);
                line->setPen( QPen( QBrush(QColor(128,128,128)), 1 ) );
                scene->addItem(line);
            }
        }

        cityBefore = *cityIter;
    }
    // if hullpoint for return is last point in hull - for loop above would exclude this if.
    if (cityBefore == hullPoint)
    {
        // the starting point is the next point after the end.
        returnPoint = *resultTrack->front();
    }
    if (! (*resultTrack->back() == hullPoint) )
    {
        /// 2. draw a line to the starting city.
        float x = resultTrack->way()->front().x();
        float y = resultTrack->way()->front().y();
        float xBefore = cityBefore.x();
        float yBefore = cityBefore.y();
        GUI::GraphicsLine * line = new GUI::GraphicsLine();
        //mGraphicsItemMap[(x+y)*(x-y)+(xBefore+yBefore)*(xBefore-yBefore)+x+y] = line;
        line->setLine(xBefore, yBefore, x, y);
        line->setPen( QPen( QBrush(QColor(0,0,128)), 2 ) );
        //line->setPen( QPen( QBrush(QColor(255,0,0)), 3 ) );
        scene->addItem(line);
        /// 3. draw big point at startingCity
        GUI::Point * point = new GUI::Point();
        point->setBrush( QBrush(QColor(255,0,0)) );
        point->setPos( x, y );
        point->setRect(-5,-5, 10, 10);
        scene->addItem(point);
    }

    return returnPoint;
}

void StaticHelper::drawBsp(QGraphicsScene * scene, bsp::BspTree *bspTree, TSP::Way * resultTrack)
{
    Q_ASSERT(scene != NULL);
    if (resultTrack->state() & TSP::Way::State::ISNULL) {
        return;
    }
    if (resultTrack->way()->empty()) {
        return;
    }

    /// 1. draw the spacing lines recursively
    drawBspLines(scene, bspTree->root(), scene->width(), scene->height());
    
    /// 2. draw the points from result track
    drawWay(scene, resultTrack);
}

void StaticHelper::drawBspLines(QGraphicsScene * scene, bsp::BspNode* node, double width, double height)
{
    Q_ASSERT(scene!=NULL);
    if (node == NULL || node->m_center.state() & TSP::Point::State::ISNULL) {
        return;
    }
    /// 1. draw two lines crossed by node->m_center
    QBrush brush = QBrush( QColor(64,64,64) );
    GUI::GraphicsLine * line = new GUI::GraphicsLine();
    float w2 = width/3.0;
    float h2 = height/3.0;
    float x = node->m_center.x();
    float y = node->m_center.y();
    line->setLine(x-w2, y, x+w2, y);
    line->setPen( QPen(brush, 1, Qt::DashLine) );
    scene->addItem(line);
    line = new GUI::GraphicsLine();
    line->setLine(x, y-h2, x, y+h2);
    line->setPen( QPen(brush, 1, Qt::DashLine) );
    scene->addItem(line);

    /// 2. call recursive
    drawBspLines(scene, node->m_upperLeft, w2, h2);
    drawBspLines(scene, node->m_upperRight, w2, h2);
    drawBspLines(scene, node->m_lowerRight, w2, h2);
    drawBspLines(scene, node->m_lowerLeft, w2, h2);
}

