#include "TspAckermannMain.h"
#include <QApplication>
#include <iostream>
#include <numeric>
#include <string>

int main(int argc, char *argv[])
{
    QCoreApplication::addLibraryPath(".");
    QApplication a(argc, argv);
    GUI::TSPAckermannMain w;
    if (argc > 1 && strncmp(argv[1], "--help", 6) == 0)
    {
        std::cout << "Usage: " << argv[0] << " [--cancel [minutes]]" << std::endl;
        std::cout << "Options:\n";
        std::cout << " --cancel <minutes>   : does cancel the naive version after <minutes>"
                   << "(default " << std::to_string(TSP::Parameters::naiveRunTimeDefault) << "min).\n"
                   << "Tamar version will ever run. If <minutes>=inf naive version runs infinite." << std::endl;
        return 0;
    }
    if (argc > 1 && strncmp(argv[1], "--cancel", 11) == 0)
    {
        if (argc > 2 && strncmp(argv[2], "inf", 3) == 0)
        {
            w.m_naiveRunMinutes = std::numeric_limits<int>::max();
        } else {
            w.m_naiveRunMinutes = atoi(argv[2]);
        }
    } else {
        // default parameters
        w.m_naiveRunMinutes = TSP::Parameters::naiveRunTimeDefault;
    }
    w.show();

    return a.exec();
}
