#ifndef GRAPHICSLINE_H
#define GRAPHICSLINE_H

#include <QGraphicsLineItem>

namespace GUI {

class GraphicsLine : public QGraphicsLineItem
{
public:
    GraphicsLine();
};

}

#endif // GRAPHICSLINE_H
