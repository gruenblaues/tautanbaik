#ifndef DIAGRAMSCENE_H
#define DIAGRAMSCENE_H

#include "qgraphicsscene.h"

namespace GUI {

class DiagramScene : public QGraphicsScene
{
public:
    DiagramScene(QObject* parent);

protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

};

}

#endif // DIAGRAMSCENE_H
