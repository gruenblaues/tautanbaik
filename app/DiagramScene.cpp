#include "DiagramScene.h"
#include "GraphicsPoint.h"
#include "QGraphicsSceneMouseEvent"

using namespace GUI;

DiagramScene::DiagramScene(QObject *parent)
    : QGraphicsScene(parent)
{
}


void DiagramScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    Point * point = new Point();
    addItem(point);
    point->setPos(mouseEvent->scenePos());
    point->setRect(-5,-5,10.0,10.0);

    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}
