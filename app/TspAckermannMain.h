#ifndef TSPACKERMANNMAIN_H
#define TSPACKERMANNMAIN_H

#include <QMainWindow>
#include <string>

#include "DiagramScene.h"
#include <TspFacade.h>
#include "DialogSaveAsImageSettings.h"

namespace Ui {
class TSPAckermannMain;
}

namespace GUI {

class TSPAckermannMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit TSPAckermannMain(QWidget *parent = 0);
    ~TSPAckermannMain();

    // QWidget interface
protected:
    void showEvent(QShowEvent *);
    void resizeEvent(QResizeEvent * event);

protected slots:
    void onRunClicked();
    void onShowWaysClicked();
    void onLoadImageTriggered();
    void onClearTriggered();
    void onClearPointTriggered();
    void onSaveImageTriggered();
    void onAddNewPointTriggered();

private:
    void createActions();
    void saveAsImage(std::string filename, DialogSaveAsImageSettings::SaveAsState saveType);
    void clearDiagramElements();

public:
    int            m_naiveRunMinutes;        ///< console parameter --cancel

private:
    TSP::TSPFacade::PointListType mCities;
    Ui::TSPAckermannMain *ui = nullptr;
    DiagramScene * mDiagramSceneTSPNaive = nullptr;
    DiagramScene * mDiagramSceneTSPAcker = nullptr;
    std::map<float, QGraphicsItem*> mGraphicsItemMap;
    TSP::DT::DTTree * mDTTree = nullptr;
    TSP::TSPFacade  * mTsp = nullptr;
    QImage          m_diagramBackgroundImage;
    TSP::Parameters     m_para;
    TSP::TSPFacade      * m_tspNaive = nullptr;
    bool                m_isFilledWithResultWay = false;
    size_t mLastDTTreeSize = 0;

    void includeTimeDuration(QString &logText, const std::chrono::system_clock::time_point &startTime);
    void drawPartialResultsInDTTree();
};

}

#endif // TSPACKERMANNMAIN_H
