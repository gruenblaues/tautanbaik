#ifndef DIALOGSAVEASIMAGESETTINGS_H
#define DIALOGSAVEASIMAGESETTINGS_H

#include <QDialog>

namespace Ui {
class DialogSaveAsImageSettings;
}

class DialogSaveAsImageSettings : public QDialog
{
    Q_OBJECT

public:
    enum class SaveAsState {
        OnlyMeta, Graph, OnlyDots
    };

    explicit DialogSaveAsImageSettings(QWidget *parent = 0);
    ~DialogSaveAsImageSettings();

    SaveAsState saveType();

private:
    Ui::DialogSaveAsImageSettings *ui;
};

#endif // DIALOGSAVEASIMAGESETTINGS_H
