#ifndef DIALOGDEBUG_H
#define DIALOGDEBUG_H

#include <QDialog>
#include <QLabel>

namespace Ui {
class DialogDebug;
}

class DialogDebug : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDebug(QWidget *parent = 0);
    ~DialogDebug();

    QLabel * getGraphicsDevice1();
    QLabel * getGraphicsDevice2();

private:
    Ui::DialogDebug *ui;
};

#endif // DIALOGDEBUG_H
