std::list<HierarchicalClusteredAlignment::LinkBST2WayNodeTuple>  HierarchicalClusteredAlignment::_insertAtBestAlignmentAllVariations(WayL& way, std::list<BSTNode*>& points)
{

    // * init maxVar Var bestD bestV and insertions
    size_t maxCombi = pow(2, points.size() - 1);
    size_t maxNum = pow(2, points.size()) - 1;
    size_t maxVar = 0;
    double bestD = std::numeric_limits<double>::max();
    list<list<BSTNode*>::iterator> bestV;

    // c++-standard impl
    list<size_t> nums;
    for (size_t i = 0; i < points.size(); ++i)
        nums.push_back(i);

    // * do while maxVar != maxCombi
    do {
        list<list<BSTNode*>::iterator> pointsPos;
        size_t alreadySet = 0;
        double varDist = 0;
        list<list<Node*>::iterator> insertions;
        size_t count = 0;
        //   * do while var != maxCombi
        do {
            //      * increase maxVar
            ++maxVar;
            //      * insert the element which is 1 in maxVar and 0 in var
            size_t pos = 0;
            auto pointsI = points.begin();
            size_t bits = 1;
            while (pos < maxCombi 
                && (((maxVar & bits) == 0) 
                    || ((alreadySet & bits) == bits))
                && (pos < count)
            ) {
                ++pos;
                bits = (1u << pos);
                if (pointsI != points.end()) ++pointsI;
            }


            // c++-standard impl
            pos = 0;
            pointsI = points.begin();





            double dist = 0;
            WayL::iterator wayI;
            tie(dist, wayI) = _findBestAlignment(way, (*pointsI)->point);
            insertions.push_back(
                way.insert(++wayI, (*pointsI)->point)
            );
            pointsPos.push_back(pointsI);
            varDist += dist;
            //      * set var to 1 at the inserted element position
            alreadySet |= (1u << pos);
            ++count;
        } while (alreadySet < maxNum);
        //   * if dist < bestD  update bestV
        if (varDist < bestD)
        {
            bestD = varDist;
            bestV = pointsPos;
        }
        //   * clear insertions
        for (const auto& wayI : insertions)
        {
            way.erase(wayI);
        }
    } //while (maxVar < maxCombi);
    while (next_permutation(nums.begin(), nums.end()));

    // * insert bestV
    list<LinkBST2WayNodeTuple> ret;
    for (const auto& pointsI : bestV)
    {
        auto res = _findBestAlignment(way, (*pointsI)->point);
        ret.push_back({ 
            get<0>(res), 
            way.insert(get<1>(res), (*pointsI)->point), 
            *pointsI 
            });
    }

    return ret;
}
