** tautanBaik **


See the **.pptx** for more details about  -  energy-minimization problem

Proof that TSP is not going better than O(2^n).


## Important information

tspsolved.blogspot.com

[researchgate Geometry-Approach by Seboeh](https://www.researchgate.net/project/Travelling-Salesman-Problem-with-Dynamic-Programming-and-Computational-Geometry-Approach)

[ibm TSP is not NP-complete](https://www.ibm.com/developerworks/community/blogs/jfp/entry/no_the_tsp_isn_t_np_complete?lang=en)


---

## how to compile on linux
0. clone https://gruenblaues@bitbucket.org/gruenblaues/tspseboeh.git to C:\Development\tsp\trunk
1. start QT
2. open TSP-Ackermann.pro
3. use Desktop Qt Kit
4. use build directories C:\Development\tsp\build-TSP-Ackermann-Desktop-Debug and build-TSP-Ackermann-Desktop-Release
5. open additionally project TspSeboeh
6. use Desktop Qt Kit
7. use build directories (C:\Development\tsp\build-TspSeboeh-Desktop-Debug) build-TspSeboeh-Desktop-Debug and Release
8. add dependency in qt-configuration: TSP-Ackermann use TspSeboeh
9. comment out LIBS += -L../build-TspSeboeh-Desktop-Debug/debug -lTspSeboeh or Release version in TSP-Ackermann.pro
10. compile
11. start deploy/compile-win.bat

---

## how to compile on windows
use CMake



