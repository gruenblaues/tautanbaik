/*
 *
 * Definitions - can be configured by hand here, till now!
 */

#define TEST_POINT_LIMIT 13

#include <iostream>
#include <list>
#include <csignal>
#include <thread>
#include <chrono>

#include <QDir>
#include <QFileInfo>
#include <QString>

#include "PngImage.h"
#include "TspAlgoSeboehThread.h"
#include "TspAlgoNaiveThread.h"
#include "TspFacade.h"

using namespace std;

TSP::TSPFacade::PointListType loadFile(const std::string &filename)
{
    PngImage pngImg(0,0);
    TSP::TSPFacade::PointListType points;
    try {
        pngImg.read_png_file(filename);
    } catch (std::invalid_argument &e) {
        cerr << "Information: Could not load meta data from the image\nReason: " << e.what() << endl;
        return points;
    }

    PngImage::PngText pngTxt;
    pngTxt.text = nullptr;		// basic initialization later used.
    int num;
    PngImage::PngText * pngTxtP = NULL;
    try {
        size_t s = strlen(PNG_TEXT_KEY_TSPWAY);
        char * key = new char[s+1];
        strncpy(key, PNG_TEXT_KEY_TSPWAY, s);
        key[s] = '\0';
        pngTxtP = pngImg.get_text_chunk(key, &num);
    } catch (std::invalid_argument &e) {
        cerr << "Information: Could not load the meta-information.\nReason: " << e.what() << endl;
        return points;
    }
    if (num == 0 || pngTxtP == nullptr)
    {
        /// Read points if available.
        try {
            size_t s = strlen(PNG_TEXT_KEY_DOTS);
            char * key = new char[s+1];
            strncpy(key, PNG_TEXT_KEY_DOTS, s);
            key[s] = '\0';
            pngTxtP = pngImg.get_text_chunk(key, &num);
        } catch (std::invalid_argument &e) {
            cerr << "Information: Could not load the dots-information.\nReason: " << e.what() << endl;
            return points;
        }
        if (num > 0 && pngTxtP != nullptr) {
            for (int i=0; i < num; ++i) {
                if ( strncmp(pngTxtP[i].key, PNG_TEXT_KEY_DOTS, strlen(PNG_TEXT_KEY_DOTS)) == 0 ) {
                    // FIXME: Because of unclear reasons pngTxtP contains all texts and not only TSPWay, although get_text_chunk().
                    pngTxt = pngTxtP[i];
                }
            }
        }
    } else {
        for (int i=0; i < num; ++i) {
            if ( strncmp(pngTxtP[i].key, PNG_TEXT_KEY_TSPWAY, strlen(PNG_TEXT_KEY_TSPWAY)) == 0 ) {
                pngTxt = pngTxtP[i];
                break;
            }
            // HINT: here are both PNG_TEXT_KEY compares, because this is the main path. The above if is only for security, if pnglib would change/improve.
            if ( strncmp(pngTxtP[i].key, PNG_TEXT_KEY_DOTS, strlen(PNG_TEXT_KEY_DOTS)) == 0 ) {
                pngTxt = pngTxtP[i];
                break;
            }
        }
    }

    if (num != 0 && pngTxtP != nullptr) {
        std::string pngCitiesStr = pngTxt.text;
        QString pngCitiesString(pngCitiesStr.c_str());
        QStringList pngCities = pngCitiesString.split(QRegExp("[,;]"));
        if (pngCities.size() % 2 != 0) {
            cerr << "Error: Could not read the TSPWay. List of Points corrupted. Size of coordinates is not odd (size =" << pngCities.size() << ")" << endl;
            return points;
        }
        for (QStringList::iterator pngCityIter= pngCities.begin();
             pngCityIter != pngCities.end(); ++pngCityIter)
        {
            bool ok=true;
            int x = pngCityIter->toFloat(&ok);
            ++pngCityIter;      // omit the point y - this is possible due to check on % 2 above.
            if (!ok) {
                continue;
            }
            int y = pngCityIter->toFloat(&ok);
            if (!ok) {
                continue;
            }
            points.push_back(TSP::TSPFacade::PointListType::value_type(x,y));
        }

    }

    return points;
}


static TspAlgoSeboehThread *gSeboehThread;
static TspAlgoNaiveThread *gNaiveThread;

static void signal_handler(int)
{
    if (gSeboehThread != nullptr)
    {
        gSeboehThread->cancel();
        for (int i=0; gSeboehThread->isRunning() && i<1000; ++i)
        {
            gSeboehThread->requestInterruption();
            std::this_thread::sleep_for(std::chrono::microseconds(500));
        }
    }

    if (gNaiveThread != nullptr)
    {
        gNaiveThread->cancel();
        for (int i=0; gNaiveThread->isRunning() && i<1000; ++i)
        {
            gNaiveThread->requestInterruption();
            std::this_thread::sleep_for(std::chrono::microseconds(500));
        }
    }

    // because of last output interrupted by SIG_INT.
    cout << endl;
    if (!gSeboehThread->isRunning())
    {
        cout << "SeboehThread with DetourClusterAlignment algorithm cancled." << endl;
    } else {
        cerr << "Error: Could not cancel SeboehThread with DetourClusterAlignment algorithm. Please kill the process 'testTspSeboeh...' by hand." << endl;
    }
    if (!gNaiveThread->isRunning())
    {
        cout << "NaiveThread with Naive Algorithm cancled." << endl;
    } else {
        cerr << "Error: Could not cancel NaiveThread with Naive Algorithm. Please kill the process 'testTspSeboeh...' by hand." << endl;
    }
    exit(1);
}


///////////////////////////////////////////////////////////////////////////////////////
/// main
///////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char*argv[])
{


    // Install a signal handler
    std::signal(SIGINT, signal_handler);

    if (argc > 1)
    {
        cout << "Usage: ./" << argv[0] << endl;
        cout << "  The test program runs through all .png files," << endl;
        cout << "  and try to detect TSP-Point Tags in .png." << endl;
        cout << "  If there are TSP-Point Tags, it will run the MinMaxClusterAlignment Algorithm" << endl;
        cout << "  and the naive Algorithm. Although, the naive algorithm is only run," << endl;
        cout << "  if the count of points is smaller than " << TEST_POINT_LIMIT << ". Because else," << endl;
        cout << "  it would take a exhaustive amount of time." << endl;
        cout << "  If you want to run the strategy of your choices, " << endl;
        cout << "  start the TSP-Ackerman GUI." << endl;
        cout << "  Regards, Sebastian." << endl;
        cout << "  - This program need no arguments. Please run it in the working-directory." << endl;
        return 0;
    }

    try {
        char timeStr[201];
        std::time_t t = std::time(NULL);
        std::strftime(timeStr, 200, "%F %H:%M:%S %Z", std::localtime(&t));

        cout << "Start: " << timeStr << endl;

        auto startT = std::chrono::system_clock::now();

        /// collect all filenames of ".png" file sin the current directory.
        QDir currentPath(QDir::currentPath());
        std::list<std::string> files;
        for (const QFileInfo &entry : currentPath.entryInfoList())
        {
            if (entry.isFile())
            {
                if (entry.fileName().endsWith("png")
                        && entry.fileName().startsWith("test"))
                {
                    files.push_back(entry.fileName().toStdString());
                }
            }
        }

        bool isFailureFound = false;
        /// run through all files
        for (const auto &filename : files)
        {
            cout << filename << "...";
            cout.flush();

            /// open file and read the points
            TSP::TSPFacade::PointListType points = loadFile(filename);

            /// start thread MinMaxClusterAlignment Algorithm
            gSeboehThread = new TspAlgoSeboehThread();
            gSeboehThread->inputCities( &points );
            gSeboehThread->start();           /// used a thread for the algorithm to clean up the memory.

            /// start thread NaiveAlgo.
            gNaiveThread = new TspAlgoNaiveThread();
            if (points.size() < TEST_POINT_LIMIT)
            {
                gNaiveThread->inputCities( &points );
                gNaiveThread->start();
            }

            gSeboehThread->wait();
            double lengthSeboeh = gSeboehThread->result()->wayLength();

            double lengthNaive;
            if (points.size() < TEST_POINT_LIMIT)
            {
                gNaiveThread->wait();
                lengthNaive = gNaiveThread->result()->wayLength();
            } else {
                lengthNaive = lengthSeboeh;
            }

            if (lengthSeboeh != lengthNaive)
            {
                cout << "TEST FAILED FOR FILE: " << filename << endl;
                isFailureFound = true;
            } else {
                cout << "\b\b\b" << " succeeded" << endl;
            }

            /// clear
            if (gSeboehThread != nullptr)
                delete gSeboehThread;
            if (gNaiveThread != nullptr)
                delete gNaiveThread;
        }

        cout << " Needed time is: "
             << std::chrono::duration_cast<std::chrono::seconds>(
                    std::chrono::system_clock::now() - startT).count()
             << " seconds" << endl;

        t = std::time(NULL);
        std::strftime(timeStr, 200, "%F %H:%M:%S %Z", std::localtime(&t));
        if (isFailureFound)
        {
            cout << "!!!!!!!!!!! TESTS FIALED !!!!!!!!!!!!! " << timeStr << endl;
        } else {
            cout << "  SUCCESSFULLY PASSED " << timeStr << endl;
        }

        return isFailureFound? 1 : 0;
    } catch (const std::exception &e) {
        return 2;
    }
    catch (...) {
        return 3;
    }
}

