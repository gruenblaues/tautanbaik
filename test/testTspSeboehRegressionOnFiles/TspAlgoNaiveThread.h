#ifndef TSPALGONAIVETHREAD_H
#define TSPALGONAIVETHREAD_H

#include <QThread>

#include "TspFacade.h"
#include "Result.h"

class TspAlgoNaiveThread : public QThread
{
public:
    TspAlgoNaiveThread();

    ~TspAlgoNaiveThread();

    void run();

    void inputCities(TSP::TSPFacade::PointListType *cities) { m_cities = cities; }
    TSP::Result * result() { return m_tspFacade->getResults(); }
    TSP::TSPFacade * tspFacade() { return m_tspFacade; }
    void cancel() { m_isCanceled = true; }

protected:
    TSP::TSPFacade      *m_tspFacade = nullptr;
    TSP::TSPFacade::PointListType   *m_cities = nullptr;
    bool m_isCanceled = false;
};

#endif // TSPALGONAIVETHREAD_H
