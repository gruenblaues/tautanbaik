#-------------------------------------------------
#
# Project created by QtCreator 2016-09-24T10:13:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = testTspSeboehRegressionOnFiles
CONFIG   += console
CONFIG   -= app_bundle

CCFLAG += -std=c++11
CONFIG += c++11

TEMPLATE = app

#linux qt include path
INCLUDEPATH += /opt/Qt/currentVersion/gcc_64/include/
#windows qt include path
#INCLUDEPATH +=

INCLUDEPATH += ../../TspSeboeh
INCLUDEPATH += $(libpng-config --cflags)

LIBS += -L../build-TspSeboeh-Desktop-Debug/ -lTspSeboeh
LIBS += -L/usr/lib/x86_64-linux-gnu/ `libpng-config --ldflags`

LIBS += -L/opt/Qt/currentVersion/gcc_64/lib

SOURCES += main.cpp \
	TspAlgoSeboehThread.cpp \
	TspAlgoNaiveThread.cpp

HEADERS += \
	TspAlgoSeboehThread.h \
	TspAlgoNaiveThread.h \

DISTFILES += \
    regressionTest.sh \
    testTspSeboehRegressionOnFiles.output.txt
