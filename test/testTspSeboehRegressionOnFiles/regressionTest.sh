#!/bin/bash

export LD_LIBRARY_PATH=../build-TspSeboeh-Desktop-Debug/:$LD_LIBRARY_PATH
# update regression test exe to working dir.
cp ../../../build-testTspSeboehRegressionOnFiles-Desktop-Debug/testTspSeboehRegressionOnFiles ../../../build-TSP-Ackermann-Desktop-Debug/
# directory we want to execute the file search of test.
cd ../../../build-TSP-Ackermann-Desktop-Debug/
# because last.png could be an example of bad performance.
rm last.png
# execute the test
./testTspSeboehRegressionOnFiles > ../src/test/testTspSeboehRegressionOnFiles/testTspSeboehRegressionOnFiles.output.txt & tail -f ../src/test/testTspSeboehRegressionOnFiles/testTspSeboehRegressionOnFiles.output.txt
exit $#
