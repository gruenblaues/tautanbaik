#include "TspAlgoSeboehThread.h"

#include "TspAlgoSeboeh.h"
#include "Parameters.h"

TspAlgoSeboehThread::TspAlgoSeboehThread()
{
    m_tspFacade = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::HierarchicalBreadthSplitting);
}

TspAlgoSeboehThread::~TspAlgoSeboehThread()
{
    // TODO [seboeh] check if used outside. shared_ptr? - delete m_tspFacade;
}

void TspAlgoSeboehThread::run()
{
    TSP::Parameters para;
    m_tspFacade->setDefaultParameters(&para);

    m_tspFacade->addInput(*m_cities);

    m_tspFacade->startRun(para);

    while (!m_isCanceled && !m_tspFacade->isFinished())
    {
        m_tspFacade->wait(2000);        /// This is in another thread, so we don't need a thread inside a thread. Therefore we wait.
    }

}
