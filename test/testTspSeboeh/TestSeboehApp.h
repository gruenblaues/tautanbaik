#ifndef TESTSEBOEHAPP_H
#define TESTSEBOEHAPP_H

/*
 * configuration/defines by hand.
 */
struct TspTest {
    static const int COUNT_OF_SAMPLES = 1;
    static const int SCENE_WIDTH = 640;
    static const int SCENE_HEIGHT = 500;
    static const int MIN_POINT_COUNT = 15;
    static const int MAX_POINT_COUNT = 16;
};

#include <QCoreApplication>
#include <string>

#include <TspFacade.h>

class TestSeboehApp : public QCoreApplication
{
public:
    TestSeboehApp(int argc, char **argv)
        : QCoreApplication(argc, argv) {}

    int exec();
    void saveAsImage(TSP::TSPFacade *tsp, std::string filename, bool isOnlyMetaInfo);
};

#endif // TESTSEBOEHAPP_H
