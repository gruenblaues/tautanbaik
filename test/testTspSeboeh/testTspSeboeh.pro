#-------------------------------------------------
#
# Project created by QtCreator 2014-05-09T22:03:49
#
#-------------------------------------------------

QT       += core

TARGET = testTspSeboeh
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

CCFLAG += -std=c++11
CONFIG += c++11

#linux qt include path
INCLUDEPATH += /opt/Qt/currentVersion/gcc_64/include/
#windows qt include path
#INCLUDEPATH +=

INCLUDEPATH += ../../TspSeboeh
INCLUDEPATH += $(libpng-config --cflags)

LIBS += -L../build-TspSeboeh-Desktop-Debug/ -lTspSeboeh
LIBS += -L/usr/lib/x86_64-linux-gnu/ `libpng-config --ldflags`


SOURCES += main.cpp \
    TestSeboehApp.cpp \
    TspAlgoSeboehThread.cpp \
    TspAlgoNaiveThread.cpp

HEADERS += \
    TestSeboehApp.h \
    TspAlgoSeboehThread.h \
    TspAlgoNaiveThread.h \
