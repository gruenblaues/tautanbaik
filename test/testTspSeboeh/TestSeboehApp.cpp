#include "TestSeboehApp.h"

#include "TspFacade.h"
#include "TspAlgoSeboehThread.h"
#include "TspAlgoNaiveThread.h"
#include "Way.h"
#include "png.h"
#include "PngImage.h"

#include <stdlib.h>
#include <iostream>
#include <chrono>
#include <cstdio>
#include <locale>

using namespace std;

int TestSeboehApp::exec()
{
    //srand(1);       /// I don't really need a random number generator. Better is the same chaos at new start again.
    srand(std::chrono::system_clock::now().time_since_epoch().count());
    setlocale(LC_ALL, "C");     // necessary - for float with point, instead of comma.
    std::locale::global(std::locale("en_US.UTF8"));

    auto timeStart = std::chrono::system_clock::now();
    /// 1. for 1 to 11 pointsCount start each thread one another.
    for (int pointCount=TspTest::MIN_POINT_COUNT; pointCount <= TspTest::MAX_POINT_COUNT; ++pointCount)
    {
        std::cout << "Points:" << pointCount << std::endl;
        std::string sPointCount = std::to_string(pointCount);
        /// 1.1 for n different scenes,
        for (int sceneNum=1; sceneNum <= TspTest::COUNT_OF_SAMPLES; ++sceneNum)
        {
            std::string sSceneNum = std::to_string(sceneNum);
            /// 1.1.1 generate pointsCount random points.
            TSP::TSPFacade::PointListType points;
            for (int pointNum=0; pointNum < pointCount; ++pointNum)
            {
                int x = 20 + static_cast<int>((TspTest::SCENE_WIDTH-80)*(static_cast<double>(rand())/RAND_MAX)/10)*10;
                int y = 20 + static_cast<int>((TspTest::SCENE_HEIGHT-80)*(static_cast<double>(rand())/RAND_MAX)/10)*10;
                points.push_back( TSP::TSPFacade::PointListValueType(x,y) );
            }

            /// 1.1.2 start thread SeboehAlgo
            TspAlgoSeboehThread seboehThread;
            // save current points into temp file last.png.
            // WORKAROUND: use tspFacade()->getResults(), better other intreface for saveAsImage()...
            auto *way = seboehThread.tspFacade()->getResults()->way()->wayVolatile();
            for (const auto &el : points)
            {
                way->push_back(TSP::Point(el.first, el.second));
            }
            saveAsImage(seboehThread.tspFacade(), "last.png", true);
            way->clear();

            // start Tamar Algorithm
            seboehThread.inputCities( &points );
            seboehThread.start();           /// used a thread for the algorithm to clean up the memory.

            /// 1.1.4 start thread NaiveAlgo.
            TspAlgoNaiveThread naiveThread;
            naiveThread.inputCities( &points );
            naiveThread.start();

            auto seboehStartTime = std::chrono::system_clock::now();
            while (!seboehThread.isFinished() && !naiveThread.isFinished())
            {
                seboehThread.wait(500);
                naiveThread.wait(500);
            }
            double lengthSeboeh = 0;
            double lengthNaive = 0;
            if (seboehThread.isFinished())
            {
                lengthSeboeh = seboehThread.result()->wayLength();
                std::cout << sPointCount << "p/" << sSceneNum << "n Length for Seboeh:" << lengthSeboeh
                          << " time:" << std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - seboehStartTime).count() << "s"
                          << " - seboeh-thread is the winner." << endl;
            }
            else
            {
                lengthNaive = naiveThread.result()->wayLength();
                std::cout << sPointCount << "p/" << sSceneNum << "n Length for Naive:" << lengthNaive
                          << " time:" << std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - seboehStartTime).count() << "s"
                          << " - naive-thread is the winner." << std::endl;

            }
            while (!seboehThread.isFinished())
                seboehThread.wait(500);
            while (!naiveThread.isFinished())
                naiveThread.wait(500);

            if (lengthSeboeh > 0)
            {
                lengthNaive = naiveThread.result()->wayLength();
                std::cout << sPointCount << "p/" << sSceneNum << "n Length for Naive:" << lengthNaive
                          << " time:" << std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - seboehStartTime).count() << "s"
                          << std::endl;

            } else {
                lengthSeboeh = seboehThread.result()->wayLength();
                std::cout << sPointCount << "p/" << sSceneNum << "n Length for Seboeh:" << lengthSeboeh
                          << " time:" << std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - seboehStartTime).count() << "s"
                          << endl;
            }


            /// 1.1.5 if way length differ, store also the points in a png image.
            int lengthNaiveRounded = static_cast<int>(lengthNaive*10);
            int lengthSeboehRounded = static_cast<int>(lengthSeboeh*10);
            if (lengthSeboehRounded != lengthNaiveRounded)
            {
                std::cout << "----------" << std::endl;
                std::string filename;
                filename = "tspSample";
                filename += std::to_string(pointCount) + "-" + std::to_string(sceneNum) + "-";
                filename += std::to_string(lengthNaiveRounded);
                filename += ".png";
                saveAsImage(naiveThread.tspFacade(), filename, true);
            }

            points.clear();
        }
    }

    std::cout << " Time needed: "
              << std::chrono::duration_cast<std::chrono::seconds>(
                     std::chrono::system_clock::now() - timeStart).count()
              << " seconds." << std::endl;

    return 0;
}

void TestSeboehApp::saveAsImage(TSP::TSPFacade *tsp, std::string filename, bool isOnlyMetaInfo)
{
    /// 2. copy that image to png-image of libpng
    size_t width = static_cast<size_t>(TspTest::SCENE_WIDTH);
    size_t height = static_cast<size_t>(TspTest::SCENE_HEIGHT);
    PngImage pngImage(width,height);
    try {
        pngImage.checkPixelFormat();
    } catch (std::invalid_argument &e) {
        std::cout << "Error: " <<
                              QString("Could not save the image.\n\nReason:\n%1")
                              .arg(e.what()).toStdString();
        return;
    }

    for (size_t y=0; y<height; y++) {
            PngImage::PngByte* row = pngImage.row_pointers_at(y);
            for (size_t x=0; x<width; x++) {
                PngImage::PngByte* ptr = &(row[x*4]);
                ptr[0] = 255;       // red
                ptr[1] = 255;       // green
                ptr[2] = 255;       // blue
                ptr[3] = 255;       // alpha
            }
    }

    /// 3. write the text chunks (the calculated TSP-Way) to png image.

    PngImage::PngText pngTxt;
    char * text;

    pngTxt.compression = PNG_TEXT_COMPRESSION_NONE;
    char keyTitle[] = "Title";
    size_t s = strlen(keyTitle);
    pngTxt.key = new char[s+1];
    strncpy(pngTxt.key, keyTitle, s);
    pngTxt.key[s] = '\0';
    char titleText[] = "TSP-Way by Tamar function";
    s = strlen(titleText);
    text = new char[s+1];       // TODO [seboeh] delete
    strncpy(text, titleText, s);
    text[s] = '\0';
    pngTxt.text = text;
    pngTxt.text_length = s;
    pngImage.set_text_chunk(&pngTxt);

    char keyAut[] = "Author";
    s = strlen(keyAut);
    pngTxt.key = new char[s+1];
    strncpy(pngTxt.key, keyAut, s);
    pngTxt.key[s] = '\0';
    char authorText[] = "created by Sebastian Boehmer";
    s = strlen(authorText);
    text = new char[s+1];       // TODO [seboeh] delete
    strncpy(text, authorText, s);
    text[s] = '\0';
    pngTxt.text = text;
    pngTxt.text_length = strlen(text);
    pngImage.set_text_chunk(&pngTxt);

    s = strlen(PNG_TEXT_KEY_DOTS);
    pngTxt.key = new char[s+1];
    strncpy(pngTxt.key, PNG_TEXT_KEY_DOTS, s);
    pngTxt.key[s] = '\0';
    std::string wayStr("");
    char number[51];
    const TSP::Way::WayType * way = tsp->getResults()->way()->way();
    if (!way->empty()) {
        for (TSP::Way::WayType::const_iterator cityIter=way->begin();
             cityIter != way->end(); ++cityIter)
        {
            snprintf(&number[0], 50, "%10.10f,%10.10f;", static_cast<float>(cityIter->x()), static_cast<float>(cityIter->y()));
            wayStr += number;
        }
    }
    char * pngWayStr = new char[wayStr.length()+1];
    strncpy(pngWayStr, wayStr.c_str(), wayStr.length());
    pngWayStr[wayStr.length()-1] = '\0';        // -1 deletes last ';'
    pngTxt.text = pngWayStr;
    pngTxt.text_length = wayStr.length()-1;
    pngImage.set_text_chunk(&pngTxt);

    /// 4. write the image .
    pngImage.write_png_file(filename);

    delete [] pngWayStr;
}

