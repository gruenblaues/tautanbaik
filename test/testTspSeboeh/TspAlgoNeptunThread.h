#ifndef TSPALGONEPTUNTHREAD_H
#define TSPALGONEPTUNTHREAD_H

#include <QThread>

#include "TspFacade.h"
#include "Result.h"

class TspAlgoNeptunThread : public QThread
{
public:
    TspAlgoNeptunThread();
    ~TspAlgoNeptunThread();

    void run();

    void inputCities(TSP::TSPFacade::PointListType *cities) { m_cities = cities; }
    TSP::Result * result() { return m_tspFacade->getResults(); }
    TSP::TSPFacade * tspFacade() { return m_tspFacade; }

protected:
    TSP::TSPFacade                  *m_tspFacade;
    TSP::TSPFacade::PointListType   *m_cities;


};

#endif // TSPALGONEPTUNTHREAD_H
