#ifndef TSPALGOPLUTOTHREAD_H
#define TSPALGOPLUTOTHREAD_H

#include <QThread>

#include "TspFacade.h"
#include "Result.h"

class TspAlgoPlutoThread : public QThread
{
public:
    TspAlgoPlutoThread();
    ~TspAlgoPlutoThread();

    void run();

    void inputCities(TSP::TSPFacade::PointListType *cities) { m_cities = cities; }
    TSP::Result * result() { return m_tspFacade->getResults(); }
    TSP::TSPFacade * tspFacade() { return m_tspFacade; }

protected:
    TSP::TSPFacade                  *m_tspFacade;
    TSP::TSPFacade::PointListType   *m_cities;

};

#endif // TSPALGOPLUTOTHREAD_H
