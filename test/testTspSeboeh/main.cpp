#include <QCoreApplication>

#include "TestSeboehApp.h"

int main(int argc, char *argv[])
{
    TestSeboehApp a(argc, argv);

    return a.exec();
}
