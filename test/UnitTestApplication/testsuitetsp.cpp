#include "testsuitetsp.h"

#include "TspFacade.h"
#include "BspTreeSarah.h"

#include <iostream>

TestSuiteTSP::TestSuiteTSP()
{
}

void TestSuiteTSP::testInitialization()
{
    TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Naive);

    TSP::Parameters para;
    QVERIFY(para.decissionTreeChildCountFnc == NULL);
    QVERIFY(para.decissionTreeWeightFnc == NULL);

    tsp.setDefaultParameters(&para);
    QVERIFY(para.decissionTreeChildCountFnc != NULL);
    QVERIFY(para.decissionTreeWeightFnc != NULL);
    QVERIFY(para.distanceFnc != NULL);
    QVERIFY(para.findNextPointFnc != NULL);
}


void TestSuiteTSP::testFirstRunOfSeboehAlgo()
{
    TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Ackermann);

    TSP::Parameters para;
    tsp.setDefaultParameters(&para);

    std::list<std::pair<float, float> > points;
    points.push_back(std::pair<float, float>(1,2));
    points.push_back(std::pair<float, float>(2,3));
    points.push_back(std::pair<float, float>(1.5,2));
    points.push_back(std::pair<float, float>(3,2.5));
    tsp.addInput(points);

    tsp.startRun(para);
    bool finished = false;
    while (!finished) {
        TSP::Indicators indi = tsp.status();
        finished = indi.state() == TSP::Indicators::States::ISFINISHED;
        QThread::msleep(500);
    }
    tsp.wait();

    QVERIFY(tsp.getResults() != NULL);
    QVERIFY(tsp.getResults()->wayLength() > 0);
}

void TestSuiteTSP::testFiveNodesCrossed()
{
    TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Ackermann);
    TSP::TSPFacade::PointListType cities;
    TSP::TSPFacade::PointListValueType point1 = TSP::TSPFacade::PointListValueType(100, 50.0);
    TSP::TSPFacade::PointListValueType point2 = TSP::TSPFacade::PointListValueType(150, 50.0);
    TSP::TSPFacade::PointListValueType point3 = TSP::TSPFacade::PointListValueType(200, 100.0);
    TSP::TSPFacade::PointListValueType point4 = TSP::TSPFacade::PointListValueType(180, 150.0);
    TSP::TSPFacade::PointListValueType point5 = TSP::TSPFacade::PointListValueType(80, 150.0);
    cities.push_back(point3);      //3
    cities.push_back(point2);     //2
    cities.push_back(point1);     // 1
    cities.push_back(point4);    //4
    cities.push_back(point5);     //5
    tsp.addInput(cities);
    TSP::Parameters para;
    tsp.setDefaultParameters(&para);
    para.findNextPointFnc = &TSP::TSPAlgoSeboeh::findNextPointFnc;
    para.decissionTreeChildCountFnc = &TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc;
    para.decissionTreeWeightFnc = &TSP::TSPAlgoSeboeh::decissionTreeWeightFnc;
    tsp.startRun(para);
    bool finished = false;
    while (!finished) {
        TSP::Indicators indi = tsp.status();
        finished = indi.state() == TSP::Indicators::States::ISFINISHED;
        QThread::msleep(500);
    }
    tsp.wait();

    TSP::Result * result = tsp.getResults();
    QVERIFY(result != NULL);
    QVERIFY(result->way()->way()->size() == 5);

    TSP::Way::WayType::const_iterator resI = result->way()->way()->begin();
    // closest points order starting form point3.
    std::cout << "coords:" << resI->x() << "x" << resI->y() << std::endl;

    QVERIFY(*resI++ == point3);
    QVERIFY(*resI++ == point4);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point1);
    QVERIFY(*resI++ == point2);
}


void TestSuiteTSP::testAckermannFnc()
{
    std::cout << "0 = " << TSP::TSPAlgoSeboeh::ackermann(0/2,0) << std::endl;
    std::cout << "1 = " << TSP::TSPAlgoSeboeh::ackermann(1/2,1*2) << std::endl;
    std::cout << "2 = " << TSP::TSPAlgoSeboeh::ackermann(2/2,2*2) << std::endl;
    std::cout << "3 = " << TSP::TSPAlgoSeboeh::ackermann(3/2,3*2) << std::endl;
    std::cout << "4 = " << TSP::TSPAlgoSeboeh::ackermann(4/2,4*2) << std::endl;
    std::cout << "5 = " << TSP::TSPAlgoSeboeh::ackermann(5/2,5*2) << std::endl;
    std::cout << "max = " << TSP::TSPAlgoSeboeh::ackermann(2,15) << std::endl;
    QVERIFY(true);
}


void TestSuiteTSP::testSeboehFiveNodesCrossed()
{
    TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Ackermann);
    TSP::TSPFacade::PointListType cities;
    TSP::TSPFacade::PointListValueType point1 = TSP::TSPFacade::PointListValueType(100, 50.0);
    TSP::TSPFacade::PointListValueType point2 = TSP::TSPFacade::PointListValueType(150, 50.0);
    TSP::TSPFacade::PointListValueType point3 = TSP::TSPFacade::PointListValueType(200, 100.0);
    TSP::TSPFacade::PointListValueType point4 = TSP::TSPFacade::PointListValueType(180, 150.0);
    TSP::TSPFacade::PointListValueType point5 = TSP::TSPFacade::PointListValueType(80, 150.0);
    cities.push_back(point3);      //3
    cities.push_back(point2);     //2
    cities.push_back(point1);     // 1
    cities.push_back(point4);    //4
    cities.push_back(point5);     //5
    tsp.addInput(cities);
    TSP::Parameters para;
    tsp.setDefaultParameters(&para);

    para.findNextPointFnc = &TSP::TSPAlgoSeboeh::findNextPointFnc;
    para.decissionTreeChildCountFnc = &TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc;
    para.decissionTreeWeightFnc = &TSP::TSPAlgoSeboeh::decissionTreeWeightFnc;

    tsp.startRun(para);
    bool finished = false;
    while (!finished) {
        TSP::Indicators indi = tsp.status();
        finished = indi.state() == TSP::Indicators::States::ISFINISHED;
        QThread::msleep(500);
    }
    tsp.wait();

    TSP::Result * result = tsp.getResults();
    QVERIFY(result != NULL);
    QVERIFY(result->way()->way()->size() == 5);

    TSP::Way::WayType::const_iterator resI = result->way()->way()->begin();
    // closest points order starting form point3.
    QVERIFY(*resI++ == point3);
    QVERIFY(*resI++ == point4);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point1);
    QVERIFY(*resI++ == point2);
}


void TestSuiteTSP::testSeboehCrossOverByNearestNeighbour()
{
    TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Naive);
    TSP::TSPFacade::PointListType cities;
    TSP::TSPFacade::PointListValueType point1 = TSP::TSPFacade::PointListValueType(100, 50);
    TSP::TSPFacade::PointListValueType point2 = TSP::TSPFacade::PointListValueType(140, 50);
    TSP::TSPFacade::PointListValueType point3 = TSP::TSPFacade::PointListValueType(140, 130);
    TSP::TSPFacade::PointListValueType point4 = TSP::TSPFacade::PointListValueType(140, 150);
    TSP::TSPFacade::PointListValueType point5 = TSP::TSPFacade::PointListValueType(160, 150);
    TSP::TSPFacade::PointListValueType point6 = TSP::TSPFacade::PointListValueType(100, 130);
    TSP::TSPFacade::PointListValueType point7 = TSP::TSPFacade::PointListValueType(40, 130);
    TSP::TSPFacade::PointListValueType point8 = TSP::TSPFacade::PointListValueType(40, 90);
    TSP::TSPFacade::PointListValueType point9 = TSP::TSPFacade::PointListValueType(40, 50);

    cities.push_back(point1);
    cities.push_back(point2);
    cities.push_back(point3);
    cities.push_back(point4);
    cities.push_back(point5);
    cities.push_back(point6);
    cities.push_back(point7);
    cities.push_back(point8);
    cities.push_back(point9);
    tsp.addInput(cities);
    TSP::Parameters para;
    tsp.setDefaultParameters(&para);

    para.findNextPointFnc = &TSP::TSPAlgoSeboeh::findNextPointFnc;
    para.decissionTreeChildCountFnc = &TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc;
    para.decissionTreeWeightFnc = &TSP::TSPAlgoSeboeh::decissionTreeWeightFnc;

    tsp.startRun(para);
    bool finished = false;
    while (!finished) {
        TSP::Indicators indi = tsp.status();
        finished = indi.state() == TSP::Indicators::States::ISFINISHED;
        QThread::msleep(500);
    }
    tsp.wait();

    TSP::Result * result = tsp.getResults();
    QVERIFY(result != NULL);
    QVERIFY(result->way()->way()->size() == 9);

    TSP::Way::WayType::const_iterator resI = result->way()->way()->begin();
    // closest points order starting form point3.
    QVERIFY(*resI++ == point1);
    QVERIFY(*resI++ == point2);
    QVERIFY(*resI++ == point3);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point4);
    QVERIFY(*resI++ == point6);
    QVERIFY(*resI++ == point7);
    QVERIFY(*resI++ == point8);
    QVERIFY(*resI++ == point9);
}


void TestSuiteTSP::testSeboehCrossOverByHeadingThreeNearestNeighbour()
{
    return;
    TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Ackermann);
    TSP::TSPFacade::PointListType cities;

    TSP::TSPFacade::PointListValueType point1 = TSP::TSPFacade::PointListValueType(100, 50);
    TSP::TSPFacade::PointListValueType point2 = TSP::TSPFacade::PointListValueType(140, 50);
    TSP::TSPFacade::PointListValueType point3 = TSP::TSPFacade::PointListValueType(140, 130);
    TSP::TSPFacade::PointListValueType point4 = TSP::TSPFacade::PointListValueType(140, 150);
    TSP::TSPFacade::PointListValueType point5 = TSP::TSPFacade::PointListValueType(160, 150);
    TSP::TSPFacade::PointListValueType point6 = TSP::TSPFacade::PointListValueType(100, 130);
    TSP::TSPFacade::PointListValueType point7 = TSP::TSPFacade::PointListValueType(60, 130);
    TSP::TSPFacade::PointListValueType point8 = TSP::TSPFacade::PointListValueType(60, 90);
    TSP::TSPFacade::PointListValueType point9 = TSP::TSPFacade::PointListValueType(60, 50);

    TSP::TSPFacade::PointListValueType point10 = TSP::TSPFacade::PointListValueType(40, 150);
    TSP::TSPFacade::PointListValueType point11 = TSP::TSPFacade::PointListValueType(60, 150);
    TSP::TSPFacade::PointListValueType point12 = TSP::TSPFacade::PointListValueType(40, 30);
    TSP::TSPFacade::PointListValueType point13 = TSP::TSPFacade::PointListValueType(60, 30);
    TSP::TSPFacade::PointListValueType point14 = TSP::TSPFacade::PointListValueType(140, 30);
    TSP::TSPFacade::PointListValueType point15 = TSP::TSPFacade::PointListValueType(160, 30);
    TSP::TSPFacade::PointListValueType point16 = TSP::TSPFacade::PointListValueType(140, 90);

    cities.push_back(point1);
    cities.push_back(point2);
    cities.push_back(point3);
    cities.push_back(point4);
    cities.push_back(point5);
    cities.push_back(point6);
    cities.push_back(point7);
    cities.push_back(point8);
    cities.push_back(point9);
    cities.push_back(point10);
    cities.push_back(point11);
    cities.push_back(point12);
    cities.push_back(point13);
    cities.push_back(point14);
    cities.push_back(point15);
    cities.push_back(point16);

    tsp.addInput(cities);
    TSP::Parameters para;
    tsp.setDefaultParameters(&para);

    para.findNextPointFnc = &TSP::TSPAlgoSeboeh::findNextPointFnc;
    para.decissionTreeChildCountFnc = &TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc;
    para.decissionTreeWeightFnc = &TSP::TSPAlgoSeboeh::decissionTreeWeightFnc;

    tsp.startRun(para);
    bool finished = false;
    while (!finished) {
        TSP::Indicators indi = tsp.status();
        finished = indi.state() == TSP::Indicators::States::ISFINISHED;
        QThread::msleep(500);
    }
    tsp.wait();

    TSP::Result * result = tsp.getResults();
    QVERIFY(result != NULL);
    QVERIFY(result->way()->way()->size() == 16);

    TSP::Way::WayType::const_iterator resI = result->way()->way()->begin();
    // closest points order starting form point3.
    QVERIFY(*resI++ == point1);
    QVERIFY(*resI++ == point2);
    QVERIFY(*resI++ == point3);
    QVERIFY(*resI++ == point4);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);
    QVERIFY(*resI++ == point5);

}


void TestSuiteTSP::testStaticCast()
{
    double df = 123.456789;
    int i = static_cast<int>(df);
    QVERIFY(i == 123);
}

void TestSuiteTSP::testSeboehChildCountFnc()
{
    float num;
    std::cout << "decissionTreeChildCountFnc:" << std::endl;
    num=0; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=1; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=2; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=3; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=4; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=5; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=6; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;
    num=7; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeChildCountFnc(num) << std::endl;

}

void TestSuiteTSP::testSeboehWeightFnc()
{
    float num;
    std::cout << "decissionTreeWeightFnc:" << std::endl;
    num=0; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=1; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=2; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=3; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=4; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=5; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=6; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;
    num=7; std::cout << num << " = " << TSP::TSPAlgoSeboeh::decissionTreeWeightFnc(num) << std::endl;

}


// test nearest point
/**
 * @brief The TestTamar class is friend of TSP::TamarAlgorithm.
 */
class TestTamar : private TSP::TamarAlgorithm
{
public:
    TestTamar() : TSP::TamarAlgorithm()
    {
        m_bspTreeRoot = new bsp::BspNode();
    }

    bsp::BspNode* m_bspTreeRoot;

    /**
     * @brief testClosestEdge only in this class.
     * @param point used to find closest edge.
     * @return first node of closest edge.
     */
    bsp::Edge * testClosestEdge(TSP::Point &point)
    {
        TSP::TSPFacade tsp(TSP::Parameters::AlgorithmType::Tamar);
        TSP::Parameters para;
        tsp.setDefaultParameters(&para);
        startRun(nullptr, para);
        // test
        auto closest = selectFirstClosestEdge(m_bspTreeRoot, point);
        return std::get<0>(closest);
    }

    void start()
    {
        // prevent start of algorithm. Necessary for testClosestEdge
    }

};

void TestSuiteTSP::testNearestEdge()
{
    // Test initialization of Tamar.

    // Create data of some edges.
    TestTamar testData;
    testData.m_bspTreeRoot->insertToEdgeLists(new oko::Node(TSP::Point(10,10)), new oko::Node(TSP::Point(10,5)));   // better
    testData.m_bspTreeRoot->insertToEdgeLists(new oko::Node(TSP::Point(10,7)), new oko::Node(TSP::Point(8,5)));       // more bad

    TSP::Point test1(12,7);
    QVERIFY(testData.testClosestEdge(test1)->startPoint->point == TSP::Point(10,10));

    TSP::Point test2(9,6);
    QVERIFY(testData.testClosestEdge(test2)->startPoint->point == TSP::Point(10,7));

}
