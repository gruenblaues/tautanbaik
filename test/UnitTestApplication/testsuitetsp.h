#ifndef TESTSUITETSP_H
#define TESTSUITETSP_H

#include <QObject>
#include <QtTest/QtTest>


class TestSuiteTSP : public QObject
{
    Q_OBJECT
public:
    TestSuiteTSP();

private slots:
    void testInitialization();
    void testFirstRunOfSeboehAlgo();
    void testFiveNodesCrossed();
    void testAckermannFnc();
    void testSeboehFiveNodesCrossed();
    void testSeboehCrossOverByNearestNeighbour();
    void testSeboehCrossOverByHeadingThreeNearestNeighbour();
    void testStaticCast();
    void testSeboehChildCountFnc();
    void testSeboehWeightFnc();
    void testNearestEdge();

};

#endif // TESTSUITETSP_H
