#pragma once

#include <QCoreApplication>


class TspLibReader : public QCoreApplication
{
public:
    TspLibReader(int argc, char **argv)
        : QCoreApplication(argc, argv) {}

    int exec();
};
