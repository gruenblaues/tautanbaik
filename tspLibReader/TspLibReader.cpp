#include "TspLibReader.h"

#include <fstream>
#include <string>
#include <tuple>
#include <chrono>

#include "TspAlgoSeboeh.h"
#include "Parameters.h"
#include "TspFacade.h"

using namespace std;

int TspLibReader::exec()
{
    if (arguments().size() < 2)
    {
        cout << "Missing TSPLib filename as parameter." << endl;
        return 1;
    }

    string fileName = arguments().at(1).toStdString();
    // read file and format
    ifstream f;
    f.open(fileName.c_str(), ios_base::in);
    if (!f.is_open())
    {
        cerr << "Could not read the file '" << fileName << "'." << endl;
        return 2;
    }

    string word;
    bool isCoordSectionFound = false;
    while (f >> word)
    {
        if (word == "EDGE_WEIGHT_TYPE:")
        {
            f >> word;
            if (word != "EUC_2D")
            {
                cerr << "File format '" << word << "' is not as expected EUC_2D." << endl;
                return 3;
            }
        }

        if (word == "NODE_COORD_SECTION")
        {
            isCoordSectionFound = true;
            break;
        }
    }
    if (!isCoordSectionFound)
    {
        cerr << "Section NODE_COORD_SECTION not found in file." << endl;
        return 4;
    }

    TSP::TSPFacade::PointListType points;
    string xs, ys;
    while (f >> word)
    {
        if (word == "EOF")
            break;
        // ignore first sign - which is the number of the point
        f >> xs;
        f >> ys;

        try {
            points.push_back({stof(xs), stof(ys)});
        } catch (const exception &e)
        {
            cerr << "Could not read x,y coordinate. '" << word << "'." << endl;
        }
    }


    // start Tamar EXP
    auto timeStart = std::chrono::system_clock::now();

    TSP::TSPFacade tspFacade(TSP::Parameters::AlgorithmType::TamarExp);

    TSP::Parameters para;
    tspFacade.setDefaultParameters(&para);

    tspFacade.addInput(points);

    cout << "calculating ... (cancel with CTR^C)";
    cout.flush();
    tspFacade.startRun(para);
    tspFacade.wait();        /// This is in another thread, so we don't need a thread inside a thread. Therefore we wait.

    cout << "\nFINISHED" << endl;
    auto timeEnd = std::chrono::system_clock::now();
    cout << "Time needed: " << chrono::duration_cast<chrono::seconds>(timeEnd - timeStart).count() << " seconds." << endl;

    cout << "Way length: " << tspFacade.getResults()->wayLength() << endl;

    return 0;

}
