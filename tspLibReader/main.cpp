#include "TspLibReader.h"

int main(int argc, char *argv[])
{
    TspLibReader a(argc, argv);

    return a.exec();
}
