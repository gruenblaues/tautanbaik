echo %1
if "%1" == "debug" (
  set buildType=debug
) else (
  set buildType=release
)
echo ===== Using ====
echo %buildType%

copy ..\build-TspSeboeh-Desktop-%buildType%\%buildType%\TspSeboeh.dll %buildType%\
	
	
if "%buildType%" == "debug" (
	if not exist debug\Qt5Cored.dll 	copy C:\Qt\5.5\mingw492_32\bin\Qt5Cored.dll %buildType%\
	if not exist debug\Qt5Guid.dll 	copy C:\Qt\5.5\mingw492_32\bin\Qt5Guid.dll %buildType%\
	if not exist debug\Qt5Widgetsd.dll 	copy C:\Qt\5.5\mingw492_32\bin\Qt5Widgetsd.dll %buildType%\
) else (
	if not exist release\Qt5Core.dll 	copy C:\Qt\5.5\mingw492_32\bin\Qt5Core.dll %buildType%\
	if not exist release\Qt5Gui.dll 	copy C:\Qt\5.5\mingw492_32\bin\Qt5Gui.dll %buildType%\
	if not exist release\Qt5Widgets.dll 	copy C:\Qt\5.5\mingw492_32\bin\Qt5Widgets.dll %buildType%\
)

if not exist %buildType%\libwinpthread-1.dll 	copy C:\Qt\5.5\mingw492_32\bin\libwinpthread-1.dll %buildType%\
if not exist %buildType%\libstdc++-6.dll 	copy "C:\Qt\5.5\mingw492_32\bin\libstdc++-6.dll" %buildType%\

if not exist %buildType%\zlib1.dll 	copy C:\MinGW\bin\zlib1.dll %buildType%\
if not exist %buildType%\libgcc_s_dw2-1.dll 	copy C:\MinGW\bin\libgcc_s_dw2-1.dll %buildType%\

