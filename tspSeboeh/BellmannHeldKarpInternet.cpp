#include "BellmannHeldKarpInternet.h"

#include <vector>
#include <math.h>

using namespace std;

using namespace TSP;

void TSP::BellmannHeldKarpInternet::buildTree(TSP::DT::DTNode::NodeType node, float, TSP::TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL input;
    size_t num = 1;
    for (auto * &point : *openPointsBasic)
    {
        input.push_back(new Node(*point));
        input.back()->number = ++num;
    }

    _input = std::move(input);

    NodeL tspRes = bellmannHeldKarpAlgo(_input);

    assert(m_resultWay != nullptr);
    for (Node *n : tspRes)
        m_resultWay->push_back(n->point);
}

BellmannHeldKarpInternet::Node *BellmannHeldKarpInternet::getNodeAt(NodeL &liste, size_t pos)
{
    Node *ret = nullptr;
    if (liste.size() > pos)
    {
        auto nodeI = liste.begin();
        for (size_t i=0; i < pos; ++i) ++nodeI;
        ret = *nodeI;
    }
    return ret;
}

BellmannHeldKarpInternet::NodeL TSP::BellmannHeldKarpInternet::bellmannHeldKarpAlgo(NodeL &input)
{
    size_t N = input.size();

    // distnace matrix
    vector< vector<double> > distance( 1<<(N-1), vector<double>( N, numeric_limits<double>::max() ) );
    auto inI = input.begin();
    for (size_t i = 0; i < N; ++i)
    {
        auto inJ = input.begin();
        for (size_t j = 0; j < N; ++j)
        {
            distance[i][j] = m_para.distanceFnc((*inI)->point, (*inJ)->point);
            ++inJ;
        }
        ++inI;
    }

    // initialize the DP table
    // best[visited][last] = the cheapest way to reach the state where:
    // - "visited" encodes the set of visited vertices other than N-1
    // - "last" is the number of the vertex visited last

    vector< vector<double> > best( 1<<(N-1), vector<double>( N, numeric_limits<double>::max() ) );

    vector< vector<size_t> > prevs( 1<<(N-1), vector<size_t>( N, 0 ) );

    // use DP to solve all states
    // note that whenever we solve a particular state,
    // the smaller states we need have already been solved
    for (size_t visited = 1; visited < (1<<(N-1)); ++visited) {
        for (size_t last = 0; last < (N-1); ++last) {

            // last visited vertex must be one of the visited vertices
            if (!(visited & 1<<last)) continue;

            // try all possibilities for the previous vertex,
            // pick the best among them
            if (visited == 1 << last) {
                // previous vertex must have been N-1
                best[visited][last] = distance[N-1][last];
                prevs[visited][last] = N-1;
            } else {
                // previous vertex was one of the other ones in "visited"
                size_t prev_visited = visited ^ 1<<last;
                for (size_t prev = 0; prev < N-1; ++prev) {
                    if (!(prev_visited & 1<<prev)) continue;
                    if (best[visited][last] >  distance[last][prev] + best[prev_visited][prev])
                    {
                        best[visited][last] = distance[last][prev] + best[prev_visited][prev];
                        prevs[visited][last] = prev;
                    }
                }
            }
        }
    }


    double answer = numeric_limits<double>::max();
    size_t startInd = N;
    for (size_t last=0; last < N-1; ++last) {
        if (answer >  distance[last][N-1] + best[ (1<<(N-1))-1 ][last])
        {
            answer = distance[last][N-1] + best[ (1<<(N-1))-1 ][last];
            startInd = last;
        }
    }

    // find by backtracking to result
    NodeL res;
    res.push_back(getNodeAt(input, startInd));
    size_t visited = (1<<(N-1))-1;
    size_t last = startInd;
    while (visited > 0)
    {
        size_t prev_visited = visited ^ 1<<last;
        last = prevs[visited][last];

        res.push_back(getNodeAt(input, last));

        visited = prev_visited;
    }

    return res;
}
