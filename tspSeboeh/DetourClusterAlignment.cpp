#include "DetourClusterAlignment.h"

#include <algorithm>
#include <tuple>
#include <math.h>

#ifndef M_PI
#define M_PI       3.14159265358979323846   // pi
#endif

#include "bspTreeVersion/BspTreeSarah.h"
#include "Way.h"

#define SMALL_DOUBLE_STEP 0.0001

using TSPWay = TSP::Way;

using namespace TSP;
using namespace std;
using namespace TSPHelper;

size_t DetourClusterAlignment::tspSize = 0;
double DetourClusterAlignment::tspWidth = 0;
double DetourClusterAlignment::tspHeight = 0;

DetourClusterAlignment::DetourClusterAlignment()
    : TspAlgoCommon()
{

}

DetourClusterAlignment::NodeL DetourClusterAlignment::createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL ret;

    double tspMaxWidth = 0;
    double tspMaxHeight = 0;
    double tspMinWidth = numeric_limits<double>::max();
    double tspMinHeight = numeric_limits<double>::max();
    for (const auto &point : *openPointsBasic)
    {
        ret.push_back(std::make_shared<Node>(*point));
        if (tspMaxWidth < point->x())
            tspMaxWidth = point->x();
        if (tspMinWidth > point->x())
            tspMinWidth = point->x();
        if (tspMaxHeight < point->y())
            tspMaxHeight = point->y();
        if (tspMinHeight > point->y())
            tspMinHeight = point->y();
    }
    tspWidth = tspMaxWidth - tspMinWidth;
    tspHeight = tspMaxHeight - tspMinHeight;

    return ret;
}

bsp::BspTreeSarah DetourClusterAlignment::_createBSPTree(NodeL &input)
{
    InputCitiesPointerType cities;
    for (const auto &node : input)
        cities.push_back(new TSP::Point(node->point));
    // HINT: above. better to use the nodes from input than new points.
    // because later search of hull-node necessary - see mark MARK_hullNodeSearch

    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cities);

    return bspTree;
}

std::tuple<DetourClusterAlignment::NodeL, DetourClusterAlignment::Way*> DetourClusterAlignment::determineHull(NodeL &input)
{
    NodeL ret;
    // * split input into quad-BSP-tree
    // * on returning from recursion merge to cyclic way (convex hull).

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = _createBSPTree(input);

    /// 2. create points order - merge
    //////////////////////////
    Way * way = nullptr;
    auto root = bspTree.root();
    if (root != nullptr) {
        way = _newConqueredWay(root);
        if (way != nullptr)
        {
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (Way::iterator valI=way->begin();
                 !valI.end(); )
            {
                Node *node = valI.nextChild();
                node->isHull = true;
                ret.push_back(std::shared_ptr<Node>(node));
            }
// TODO [seboeh] check if possible
//            way->clear();
//            delete way;
        }
    }

    // update input with new nodes from way
    Way::iterator wayI = way->begin();
    while (!wayI.end())
    {
        Node *node = wayI.nextChild();
        auto inputI = find_if(input.begin(), input.end(),
                              [node] (const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        assert(inputI != input.end());
        *inputI = shared_ptr<Node>(node, no_op_delete());
    }

    // * create resulting list
    return make_tuple(ret, way);
}


bool DetourClusterAlignment::_isClockwise(TSP::Point pCenter, Way *way)
{
    Way::iterator pI = way->begin();
    Way::iterator pbackupI = pI;
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0 && ((dot/90) != m_para.distanceFnc(*pbackupI,*pI));
}

void DetourClusterAlignment::_flipWay(Way *way, TSP::Point pCenter)
{
    if(!_isClockwise(pCenter, way))
        way->flipDirection();
}

Point DetourClusterAlignment::_correctNodeCenter(Way* wayBiggerVals, Way* waySmallerVals, bsp::BspNode *node, MergeCallState state)
{
    Point::value_type valA = 0;
    auto wI = waySmallerVals->begin();
    while (!wI.end())
    {
        valA = max(valA, state == MergeCallState::leftRight? (*wI).x() : (*wI).y());
        ++wI;
    }
    float valB = numeric_limits<double>::max();
    wI = wayBiggerVals->begin();
    while (!wI.end())
    {
        valB = min(valB, state == MergeCallState::leftRight? (*wI).x() : (*wI).y());
        ++wI;
    }
    Point mainCenter = node->m_center;
    if (state == MergeCallState::leftRight)
        mainCenter.set(mainCenter.x() + ((valB - valA)/2.0), mainCenter.y());
    else
        mainCenter.set(mainCenter.x(), mainCenter.y() + ((valB - valA)/2.0));

    return mainCenter;
}

DetourClusterAlignment::Way *DetourClusterAlignment::_newConqueredWay(bsp::BspNode *node)
{
    if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        Way *way = new Way();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
//        way->setOrigin(node);
        return way;
    }
    Way * wayUpper;
    Way * wayLower;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        Way * wayUpperLeft = _newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            Way * wayUpperRight = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x()/2.0, pUpperCenter.y()/2.0);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            // see argument below Point mainCenter = _correctNodeCenter(wayUpperRight, wayUpperLeft, node, MergeCallState::leftRight);
            _mergeHull(wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        } else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    } else {
        if (node->m_upperRight) {
            wayUpper = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        } else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        Way * wayLowerLeft = _newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            Way * wayLowerRight = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x()/2.0, pLowerCenter.y()/2.0);

            // HINT: it is faster not to do this: Point mainCenter = _correctNodeCenter(wayLowerRight, wayLowerLeft, node, MergeCallState::leftRight);
            // instead using node->m_center
            _mergeHull(wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        } else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    } else {
        if (node->m_lowerRight) {
            wayLower = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        } else {
            wayLower = nullptr;
        }
    }

    //incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {
        // HINT: test37 needs master center node->m_center.
        // HINT: test2 needs flipWay
        _flipWay(wayLower, pLowerCenter);
        _flipWay(wayUpper, pUpperCenter);
        // Point mainCenter = _correctNodeCenter(wayLower, wayUpper, node, MergeCallState::topDown);
        _mergeHull(wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    } else if (wayUpper) {
        return wayUpper;
    } else if (wayLower) {
        return wayLower;
    } else {
        return nullptr;
    }
}

float DetourClusterAlignment::sign (const Point &p1, const Point &p2, const Point &p3)
{
    return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
}

bool DetourClusterAlignment::_isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    if (p1 == inside || p2 == inside || p3 == inside)
        return false;

    // [https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle]
    Point pt = inside;
    Point v1 = p1;
    Point v2 = p2;
    Point v3 = p3;
    float d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(pt, v1, v2);
    d2 = sign(pt, v2, v3);
    d3 = sign(pt, v3, v1);

    has_neg = (d1 <= 0) || (d2 <= 0) || (d3 <= 0);      // also =0 --> point on line --> not inside (false)
    has_pos = (d1 >= 0) || (d2 >= 0) || (d3 >= 0);      // =0 --> on one line of triangle

    bool isInsideV1 = !(has_neg && has_pos);


    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    // barycentric coordinates.
    bool isInsideV2 = false;
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        isInsideV2 = false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        isInsideV2 = false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        isInsideV2 = false;

    // see test12 - V2 was wrong assert(isInsideV1 == isInsideV2);
    return isInsideV1;
}

void DetourClusterAlignment::_triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = _isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = _isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
                && pb == *lpbI
                && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
                && pb == *hpbI
                && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

DetourClusterAlignment::CrossedState DetourClusterAlignment::_isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint)
{
    /// Crossed between a and b?
    double x1 = pa1I.x();
    double y1 = pa1I.y();
    double x2 = pa2I.x();
    double y2 = pa2I.y();
    double x3 = pb1I.x();
    double y3 = pb1I.y();
    double x4 = pb2I.x();
    double y4 = pb2I.y();

    /// HINT: [http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection]
    double denominator = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (denominator == 0) {
        // lines are parallel not guarantied to lay on each other.
        // We want to integrate the considered node as inner node -> so it should not be covered -> parallel
        return CrossedState::isParallel;
    }
    double pX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denominator;
    double pY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denominator;
    if (crossPoint != nullptr)
        *crossPoint = TSP::Point(pX,pY);

    if ((std::max)(std::min(x1,x2),std::min(x3,x4)) <= pX && pX <= std::min((std::max)(x1,x2),(std::max)(x3,x4))
            && (std::max)(std::min(y1,y2),std::min(y3,y4)) <= pY && pY <= std::min((std::max)(y1,y2),(std::max)(y3,y4)))
    {
        if ((pX == x1 && pY == y1)          // FIX: Could be a speed-up, if we check if this crossing on exact one outer point could happen with the input.
            || (pX == x2 && pY == y2)
            || (pX == x3 && pY == y3)
            || (pX == x4 && pY == y4))
        {
            return CrossedState::isOnPoint;
        } else {
            return CrossedState::isCrossed;
        }
    } else {
        return CrossedState::isOutOfRange;
    }
}

DetourClusterAlignment::DirectAccessState DetourClusterAlignment::_isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI)
{
    Way::iterator prevAI = pAI; --prevAI;
    Way::iterator nextAI = pAI; ++nextAI;
    Way::iterator prevBI = pBI; --prevBI;
    Way::iterator nextBI = pBI; ++nextBI;
    CrossedState state = _isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = _isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = _isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = _isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool DetourClusterAlignment::_isNormalVectorLeft(const Point &from, const Point &to, const Point &insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x())/2
                    , (from.y() + to.y())/2);

    Point normalVectLeft(meanPoint.x() - straight.y()
                         , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
                          , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}


bool DetourClusterAlignment::_isCovered(Way * way, Way::iterator &w1I, Way::iterator &fixI)
{
    Way::iterator wI = way->begin();
    Way::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = _isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed
                || state == CrossedState::isCovered
                || state == CrossedState::isOnPoint)
            return true;
    }
    return false;
}

bool DetourClusterAlignment::_createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI)
{
    if (_isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        Way::iterator w1NewI = way1->push_back(&way2I);
        Way::iterator w1NNewI = way1->push_back(&way2NI);

        Node *nodePoint = (&w1Inside);
        if (dist1 < dist2 && dist1 < dist3)
        {
            nodePoint->prev = &w1Alt;
            nodePoint->next = (&w1Alt)->next;
            way1->insertChild(w1Alt, nodePoint);      // TODO [seboeh] is new really necessary?
        } else if (dist2 < dist1 && dist2 < dist3)
        {
            nodePoint->prev = &w1NNewI;
            nodePoint->next = (&w1NNewI)->next;
            way1->insertChild(w1NNewI, nodePoint);
        } else {
            nodePoint->prev = &w1NewI;
            nodePoint->next = (&w1NewI)->next;
            way1->insertChild(w1NewI, nodePoint);
        }
        return true;
    }
    return false;
}

void DetourClusterAlignment::_createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    Way::iterator w1I = way1->begin();
    Way::iterator w1NI = w1I; ++w1NI;
    Way::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    Node *nodePoint = (&insideI);
    if (dist1 < dist2 && dist1 < dist3)
    {
        nodePoint->prev = &w1I;
        nodePoint->next = (&w1I)->next;
        way1->insertChild(w1I, nodePoint);
    } else if (dist2 < dist1 && dist2 < dist3)
    {
        nodePoint->prev = &w1NI;
        nodePoint->next = (&w1NI)->next;
        way1->insertChild(w1NI, nodePoint);
    } else {
        nodePoint->prev = &w1NNI;
        nodePoint->next = (&w1NNI)->next;
        way1->insertChild(w1NNI, nodePoint);
    }
}

void DetourClusterAlignment::_mergeHull(Way * &way1, const TSP::Point &pWay1Center, Way * &way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        _flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        Way::iterator w1I = way1->begin();
        Way::iterator fixI = way2->begin();
        if (_isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            _flipWay(way1, pCenter);
            return;
        } else {
            ++w1I;
            if (_isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                _flipWay(way1, pCenter);
                return;
            } else {
                ++w1I;
                if (_isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    _flipWay(way1, pCenter);
                    return;
                } else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    Way::iterator w1NI = w1I; ++w1NI;
                    Way::iterator w1NNI = w1NI; ++w1NNI;
                    if (_isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        _createTriangle3x1(way1, w1NNI, fixI);
                    } else if (_isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        _createTriangle3x1(way1, w1NI, fixI);
                    } else {
                        _createTriangle3x1(way1, w1I, fixI);
                    }
                    _flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        Way::iterator way1I = way1->begin();
        Way::iterator way2I = way2->begin();
        Way::iterator way1NI = way1I;
        ++way1NI;
        Way::iterator way2NI = way2I;
        ++way2NI;
        Way::iterator way1Nearest;
        Way::iterator way2Nearest;
        double maxDist=std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        Way::iterator way1Next = way1Nearest; ++way1Next;
        Way::iterator way2Next = way2Nearest; ++way2Next;
        Point crossedPoint;
        auto crossedVal = _isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next, &crossedPoint);
        if (crossedVal == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            _flipWay(way1, pCenter);
            return;
        } else if (crossedVal == CrossedState::isOnPoint)
        {       // triangle with inside point exactly on one triangle-side
            /// - determine way1 or 2 = wayX, contains crossedPoint
            auto wayI = way1->begin();
            Way *wayX = nullptr;
            Way *wayY = nullptr;
            if (*wayI == crossedPoint || *(++wayI) == crossedPoint)
            {
                wayX = way1;
                wayY = way2;
            }
            else
            {
                wayI = way2->begin();
                if (*wayI == crossedPoint || *(++wayI) == crossedPoint)
                {
                    wayX = way2;
                    wayY = way1;
                }
            }
            assert( wayX != nullptr && wayY != nullptr);
            auto wayCrossedI = wayI;
            auto wayNotCrossedI = ++wayI;
            /// - determine p_i from wayY is on one line with crossedPoint and other point (=xa) of wayX
            wayI = wayY->begin();
            auto wayNI = wayI; ++wayNI;
            Point meanP = (*wayI + *wayNI)/2.0;
            Way::iterator wayPI;
            if (_isCrossed(*wayNotCrossedI, *wayI, *wayCrossedI, meanP) == CrossedState::isOnPoint)
            {       // pi = wayI
                wayPI = wayI;
            } else
            {       // pi = ++wayI
                wayPI = ++wayI;
            }
            auto wayPNI = wayPI; ++wayPNI;
            /// - insert wayY before xa and after crossedPoint.
            (&wayNotCrossedI)->next = &wayCrossedI;
            (&wayNotCrossedI)->prev = &wayPNI;
            (&wayPNI)->next = &wayNotCrossedI;
            (&wayPNI)->prev = &wayPI;
            (&wayPI)->next = &wayPNI;
            (&wayPI)->prev = &wayCrossedI;
            (&wayCrossedI)->next = &wayPI;
            (&wayCrossedI)->prev = &wayNotCrossedI;

            if (way1 != wayX)
                swap(way1, way2);
            _flipWay(way1, pCenter);
            return;
        }
        else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (_createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            } else  if (_createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI ))
            {
            } else if (_createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else if (_createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            _flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2

    /// beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (Way::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (Way::iterator wBI=way2->begin();
             !wBI.end(); ++wBI)
        {
            if ((*wAI).x() == 470)
                int i=5;
            if ((*wBI).x() == 470)
                int i=5;

            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (_isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    Way::iterator haI, hbI, laI, lbI;

    Point realCenter = (pWay1Center + pWay2Center)/2.0;

    smallestDist /= 2;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    CrossedState oldState = CrossedState::isOutOfRange;
    Point oldCrossedPoint;
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = _isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x())-10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x())+10, pCenter.y()), &crossedPoint);
        else
            state = _isCrossed(pa, pb, TSP::Point(pCenter.x(),std::min(pa.y(), pb.y())-10), TSP::Point(pCenter.x(),(std::max)(pa.y(), pb.y())+10), &crossedPoint);

        //bool isLowNotHigh = _isNormalVectorLeft(pb, pa, realCenter);

        double value;

        if (pa.x() == 270)
            if (pb.x() == 550)
                int i=5;
        if (pb.x() == 270)
            int i=5;

        bool isGoodLow = false;
        bool isGoodHigh = false;
        if (state == CrossedState::isOnPoint)
        {       // see test82, test76
            // see test89, test64

            // test if all points are on one side.
            int countSides = 0;
            Point vect = (pb - pa) / m_para.distanceFnc(pa, pb);
            double l = (max(tspWidth, tspHeight)/2);
            Point startP = pa - vect * l;
            Point endP = pb + vect * l;

            for (Way::iterator wAI=way1->begin();
                 !wAI.end(); ++wAI)         // iterate over convex hull
            {
                try {
                    if (_isLeftSideExEqual(startP, endP, *wAI))
                    {
                        if (countSides == 0)
                        {
                            countSides = 1;
                        }
                        else if (countSides == 2)
                        {
                            countSides = 3;
                            break;
                        }
                    } else {
                        if (countSides == 0)
                        {
                            countSides = 2;
                        }
                        else if (countSides == 1)
                        {
                            countSides = 3;
                            break;
                        }
                    }
                } catch (const exception &)
                {
                    // ignore, all points on the line are not disrupting the tangent
                }
            }
            if (countSides != 3)
            {
                for (Way::iterator wBI=way2->begin();
                     !wBI.end(); ++wBI)
                {
                    try {
                        if (_isLeftSideExEqual(startP, endP, *wBI))
                        {
                            if (countSides == 2)
                            {
                                countSides = 3;
                                break;
                            }
                        } else {
                            if (countSides == 1)
                            {
                                countSides = 3;
                                break;
                            }
                        }
                    } catch (const exception &)
                    {
                        // ignore, because tangente still valid.
                    }
                }
            }
            if (countSides != 3)
            {
                state = CrossedState::isCrossed;
            }
        }

        if (state == CrossedState::isCrossed)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();

            if ((callState == MergeCallState::topDown
                    && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGoodLow = true;
            }
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                    && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGoodHigh = true;
            }

            // running both if-s parallel, because to guaranty below the h.. and l.. valid
            if (isGoodLow)
            {
                laI = edgeI->aNodeI;
                lbI = edgeI->bNodeI;
            }
            if (isGoodHigh)
            {
                haI = edgeI->aNodeI;
                hbI = edgeI->bNodeI;
            }
        }
    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, haI, laI, hbI, lbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hbI, lbI, haI, laI);

    /// correct the hull in way1
    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    /// clear childs - see test7
    (&haI)->isChildNode = false;
    (&haI)->child = nullptr;
    (&hbI)->isChildNode = false;
    (&hbI)->child = nullptr;
    (&laI)->isChildNode = false;
    (&laI)->child = nullptr;
    (&lbI)->isChildNode = false;
    (&lbI)->child = nullptr;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in _mergeHull().

    _flipWay(way1, pCenter);  // necessary, because code before does not keep the clock-wise order.
}

double DetourClusterAlignment::_calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double DetourClusterAlignment::_area(const PointL &polygon)
{
    double area = 0;
    for (auto pointI = polygon.begin();
         pointI != polygon.end(); ++pointI)
    {
        auto pointNextI = pointI; ++pointNextI;
        if (pointNextI == polygon.end()) pointNextI = polygon.begin();
        area += pointI->x() * pointNextI->y();
        area -= pointI->y() * pointNextI->x();
    }
    return abs(area);
}

double DetourClusterAlignment::_triangleAltitudeOnC(Point c1, Point c2, Point perpendicular)
{
    double c = m_para.distanceFnc(c1, c2);
    double a = m_para.distanceFnc(c2, perpendicular);
    double b = m_para.distanceFnc(perpendicular, c1);
    double s = (a + b + c)/2.0;
    return 2.0*sqrt(s*(s-a)*(s-b)*(s-c)) / c;
}

bool DetourClusterAlignment::_isLeftSideExEqual(const Point &p1Line, const Point &p2Line, const Point &check)
{
    Point p1 = p2Line - p1Line;
    Point p2 = check - p1Line;

    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    dot = static_cast<int>(dot * 100)/100;
    double degree90strait = m_para.distanceFnc(p2Line,check);
    if (dot == 0 || ((dot/90 -0.01) < degree90strait && degree90strait < (dot/90 +0.01)))
        throw logic_error("Point is on line, and not left or right.");
    return dot < 0;
}


bool DetourClusterAlignment::_isRightSide(const Point &p1, const Point &p2)
{
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

double DetourClusterAlignment::_calcDegree(Node *node, Node *nodeTested)
{
    return _calcDegree(nodeTested->point, node->point);
}

double DetourClusterAlignment::_calcDegree(Point &node, Point &nodeTested)
{
    Point pointVect = nodeTested - node;

    double degree = (pointVect.x() + pointVect.y())
            / (m_para.distanceFnc(Point(0,0), pointVect)
               * m_para.distanceFnc(Point(0,0), Point(1,1)));

    double degreeArc = acos(degree);
    // clock-wise
    if (_isRightSide(Point(1,1), pointVect))
        return  degreeArc;
    else
        return  2*acos(-1) - degreeArc;
}

bool DetourClusterAlignment::_checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{       // calc clock-wise
    double degO1 = _calcDegree(nodePoint, cluPointOut);     // typical smaller
    double degO2 = _calcDegree(nodePoint, cluPointOut2);
    double degC = _calcDegree(nodePoint, closestP);
    if (degO1 <= degO2)
    {
        if (degO2-degO1 == M_PI)
            return true;                // see test25
        else if (degO2-degO1 > M_PI)
            return degO2 <= degC || degC <= degO1;
        else
            return degO1 <= degC && degC <= degO2;
    } else {
        if (degO2-degO1 == M_PI)
            return true;               // see test25
        return degO1 <= degC || degC <= degO2;      // shall include points although at the outer winkel! see test25
    }
}

double DetourClusterAlignment::_correlation(Point a, Point b)
{
    return ((a.x()*b.x()) + (a.y()*b.y())) / (m_para.distanceFnc(Point(0,0), a) * m_para.distanceFnc(Point(0,0),b));
}


bool DetourClusterAlignment::_checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{
    double dist1 = m_para.distanceFnc(cluPointOut, nodePoint);
    double dist2 = m_para.distanceFnc(cluPointOut2, nodePoint);
    double distC = m_para.distanceFnc(closestP, nodePoint);
    double corr1 = _correlation(cluPointOut - nodePoint, closestP - nodePoint);
    //double degree0to1For1 = (corr1 + 1) / 2.0;
    double corr2 = _correlation(cluPointOut2 - nodePoint, closestP - nodePoint);
    //double degree0to1For2 = (corr2 + 1) / 2.0;
    //double corr = degree0to1For1 + degree0to1For2;
    //double corr = corr1 + corr2;
    double corr3 = _correlation(cluPointOut - nodePoint, cluPointOut2 - nodePoint);
    // distC, dist1,2 are tradeofs not correct decisions
    return ((distC/2 < dist1 || distC/2 < dist2) && corr3 < corr1 && corr3 < corr2);
}


void DetourClusterAlignment::_insertClosestTestedPointsSorted(std::map<double, Node*> &closestTestedPointsSorted, Node *node, Node* closest)
{
    double degree = _calcDegree(node, closest);
    auto iter = closestTestedPointsSorted.find(degree);
    if (iter != closestTestedPointsSorted.end())
    {
        degree += SMALL_DOUBLE_STEP;
    }
    closestTestedPointsSorted.insert({degree, closest});
}

double DetourClusterAlignment::_calcCeListDist(CeList &ceList)
{
    if (ceList.ceList.empty())
        return 0;
    double distC = m_para.distanceFnc(ceList.ceList.front().fromNode->point, ceList.ceList.back().toNode->point);
    double distS = 0;
    Node *prev = ceList.ceList.front().fromNode;
    for (const auto &ce : ceList.ceList)
    {
        distS += m_para.distanceFnc(prev->point, ce.insertNode->point);
        prev = ce.insertNode;
    }
    distS += m_para.distanceFnc(ceList.ceList.back().insertNode->point, ceList.ceList.back().toNode->point);

    return distS - distC;
}


DetourClusterAlignment::CeList::CeListType::iterator  DetourClusterAlignment::_insertToCeList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert)
{
    auto ceListResI = ceList.ceList.end();

    if (!ceList.ceList.empty())
    {
        double distDirect = seboehDist(ceList.ceList.front().fromNode, ceList.ceList.back().toNode, insert);
        auto insertPosI = ceList.ceListOrder.insert({distDirect, insert}).first;
        if (++insertPosI != ceList.ceListOrder.end())
        {
            ceList.ceList.clear();
            for (auto &orderEl : ceList.ceListOrder)
            {
                auto iter = __insertToCeListAlongList(ceList, rootFrom, rootTo, orderEl.second);
                if (orderEl.second == insert)
                    ceListResI = iter;
            }
        } else {
            ceListResI = __insertToCeListAlongList(ceList, rootFrom, rootTo, insert);
        }
    }
    else
    {
        ceListResI = __insertToCeListAlongList(ceList, rootFrom, rootTo, insert);
        double distDirect = seboehDist(ceList.ceList.front().fromNode, ceList.ceList.back().toNode, insert);
        ceList.ceListOrder.insert({distDirect, insert});
    }

    return ceListResI;
}

DetourClusterAlignment::CeList::CeListType::iterator  DetourClusterAlignment::__insertToCeListAlongList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert)
{
    ClosestEdges ce(insert);
    CeList::CeListType::iterator ceI = ceList.ceList.end();

    if (ceList.ceList.empty())
    {
        ce.fromNode = rootFrom;
        ce.toNode = rootTo;
    }
    else
    {
        double bestDist = numeric_limits<double>::max();
        auto bestI = ceList.ceList.end();
        Node *bestPrev = nullptr;
        Node *prev = ceList.ceList.front().fromNode;
        for (auto foundCeI = ceList.ceList.begin();
             foundCeI != ceList.ceList.end(); ++foundCeI)
        {
            double dist = seboehDist(prev, foundCeI->insertNode, ce.insertNode);
            if (dist < bestDist)
            {
                bestDist = dist;
                bestI = foundCeI;
                bestPrev = prev;
            }
            prev = foundCeI->insertNode;
        }
        // check end
        double dist = seboehDist(ceList.ceList.back().insertNode, ceList.ceList.back().toNode, ce.insertNode);
        if (dist < bestDist)
        {
            bestDist = dist;
            ceI = ceList.ceList.end();
        } else {
            ceI = bestI;
        }

        // set from to
        if (ceI != ceList.ceList.end())
        {
            ce.fromNode = bestPrev;
            ce.toNode = ceI->insertNode;
        } else {
            ce.fromNode = ceList.ceList.back().insertNode;
            ce.toNode = ceList.ceList.back().toNode;
        }
    }

    ce.dist = seboehDist(ce.fromNode, ce.toNode, ce.insertNode);
    return ceList.ceList.insert(ceI, ce);
}

/**
 * @brief DetourClusterAlignment::_isAtEdge is for test12
 * but not at one already guarantied edge - see test12, because, then it would be grown
 * by evultionary algorithm and is not guarantied. - see test45 for &&
 * there should be no point inside those triangle
 */
bool DetourClusterAlignment::_isAtEdge(std::list<Node*> &openNodes, Node *from, Node *to, std::list<Node*> &selectedNodes)
{
    bool hasNoNodeInside = true;
    for (Node* &innerNode : openNodes)
    {
        auto findI = find(selectedNodes.begin(), selectedNodes.end(), innerNode);
        if (findI != selectedNodes.end())
            continue;

        for (Node* &selectedNode : selectedNodes)
        {
            if (_isInsideTriangle(from->point, to->point, selectedNode->point, innerNode->point))
            {
                hasNoNodeInside = false;
                break;
            }
        }
        if (!hasNoNodeInside)
            break;
    }

    return hasNoNodeInside;
}

void DetourClusterAlignment::_determineGroupAlignmentRec(std::list<Node*> selectedNodes, std::list<Node*> openNodes)
{
    /// * select the nodes
    while (!openNodes.empty())
    {
        Node *openNode = openNodes.front();
        selectedNodes.push_back(openNode);
        openNodes.pop_front();

        /// * find best edge for selected alignment
        double bestDist = numeric_limits<double>::max();
        auto bestToI = m_edgesGuarantied.end();
        CeList bestCeList;
        Node *prev = *(--m_edgesGuarantied.end());
        for (auto nextI = m_edgesGuarantied.begin();
             nextI != m_edgesGuarantied.end(); ++nextI)
        {
            Node *next = *nextI;

            ///     * check if close to hull-edge
            if (_isAtEdge(openNodes, prev, next, selectedNodes))
            {
               CeList ceList;
               // TODO [seb] speedup - presorting of directAlign of selectedNodes
                for (auto &selectedN : selectedNodes)
                    _insertToCeList(ceList, prev, next, selectedN);

                double dist = _calcCeListDist(ceList);

                if (dist < bestDist)
                {
                    bestDist = dist;
                    bestToI = nextI;
                    bestCeList = ceList;
                }
            }

            prev = next;
        }

        double currentDist = 0;
        for (Node * &node : selectedNodes)
            currentDist += node->guarantiedBasicDist;

        auto selectI = selectedNodes.begin();
        if (selectedNodes.size() == 3
                && ((*selectI)->point.x() == 310 || (*selectI)->point.x() == 340)
                && ((*(++selectI))->point.x() == 310 || (*selectI)->point.x() == 340)
                && ((*(++selectI))->point.x() == 310 || (*selectI)->point.x() == 340)
           )
            int i=5;

        ///  * if bestDist < (bestA + bestB) change both to alignment to that edge
        if (!bestCeList.ceList.empty()
              && bestDist < currentDist)
        {
            auto bestFromI = bestToI;
            if (bestFromI != m_edgesGuarantied.begin())
                --bestFromI;
            else
                bestFromI = --m_edgesGuarantied.end();

            ///     * set the bigger dist
            for (auto &ce : bestCeList.ceList)
            {
                double distAligned = seboehDist(ce.fromNode, ce.toNode, ce.insertNode);

                // it is necessary to enlarge the dist, because it is used in evolutinoary-grow later.
                if (distAligned > ce.insertNode->guarantiedDist)
                {       // see test10-2 and test12
                    ce.insertNode->guarantiedDist = distAligned;
                    ce.insertNode->guarantiedFrom = ce.fromNode;
                    ce.insertNode->guarantiedTo = ce.toNode;
                }
                // TODO [seb] riscy? - reduced also if not applied (upgraded)
                ce.insertNode->guarantiedBasicDist = min(ce.insertNode->guarantiedBasicDist
                                                         , distAligned);
            }
        }

        /// * recursive call for bigger groups
        _determineGroupAlignmentRec(selectedNodes, openNodes);

        selectedNodes.pop_back();
    }

}

void DetourClusterAlignment::determineGroupAlignment(std::list<Node*> &_openNodes)
{
    std::list<Node*> selectedNodes;
    auto openNodes = _openNodes;
    while (!openNodes.empty())
    {
        Node *openNode = openNodes.front();
        selectedNodes.push_back(openNode);
        openNodes.pop_front();

        _determineGroupAlignmentRec(selectedNodes, openNodes);

        selectedNodes.pop_back();
    }
}

void DetourClusterAlignment::determineGuarantiedAlignment(NodeL &input, NodeL &hull)
{
    /// * Collect the maximal alignment, of the guarantied alignments generated from each hull-edge.
    ///   Write it into a temporary tree.
    ///   First alignment in first group of hirachical-clustering with cutting at the biggest dist,
    ///   would be the best edge. But for now, there shall be used the maximum of those edges.
    ///
    for (const auto &node : hull)
    {
        node->isHullNodeProcessed = false;      // for test12
        m_edgesGuarantied.push_back(node.get());
    }

    // collect rest of alignment
    list<Node*> openNodes;
    for (auto &node : input)
    {
        auto findI = std::find_if(hull.begin(), hull.end(),
                                  [node](const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        if (findI == hull.end())
            openNodes.push_back(node.get());
    }

    // find smallest direct alignment, if more edges than points
    int sshift = static_cast<int>(m_edgesGuarantied.size()) - openNodes.size();
    if (sshift >= 0)
    {   // test12, test60 and test10-2?
        map<double, tuple<Node*,Node*,Node*> > sortedEdgesWithNodes;    // node, from, to
        // for test12?
        for (auto openNI = openNodes.begin();
             openNI != openNodes.end(); ++openNI)
        {
            Node *insertNode = *openNI;

            Node *prev = *(--m_edgesGuarantied.end());
            for (auto nextI = m_edgesGuarantied.begin();
                 nextI != m_edgesGuarantied.end(); ++nextI)
            {
                Node *next = *nextI;
                _insertToMap(seboehDist(prev, next, insertNode), sortedEdgesWithNodes, make_tuple(insertNode, prev, next));
                prev = next;
            }
        }

        // for test12
        Node *node, *from, *to;
        size_t setElC = 0;
        auto sortedI = sortedEdgesWithNodes.begin();
        while (setElC < openNodes.size()
               && sortedI != sortedEdgesWithNodes.end())
        {
            tie(node,from,to) = sortedI->second;
            if (node->guarantiedFrom == nullptr
                    && ! from->isHullNodeProcessed)
            {
                node->guarantiedDist = sortedI->first;
                node->guarantiedBasicDist = sortedI->first;
                node->guarantiedFrom = from;
                node->guarantiedTo = to;
                from->isHullNodeProcessed = true;
                ++setElC;
            }
            ++sortedI;
        }

    }
    else
    {
        // for test60 and test65?

        // general alignment to edge
        DirectAlignments aligns;
        for (auto &node : openNodes)
        {
            ClosestEdges diAl;
            Node *prev = m_edgesGuarantied.back();
            for (Node * &next : m_edgesGuarantied)
            {
                double distDirect = seboehDist(prev, next, node);
                node->isInWay = false;
                diAl.insertNode = node;
                diAl.fromNode = prev;
                diAl.toNode = next;

                aligns.insert({distDirect, diAl});

                prev = next;
            }
        }

        // calc max dist
        list<ClosestEdges> bestAlignments;
        for (DirectAlignments::value_type &alignEl : aligns)
        {
            ClosestEdges alignment = alignEl.second;

            if (!alignment.insertNode->isInWay)
            {
                Node *from = alignment.fromNode;
                Node *to = alignment.toNode;

                double c = m_para.distanceFnc(from->point, to->point);
                Point vect = (to->point - from->point) / c;
                // solved:
                // a1 + c = a2 + 2*c1;  a2 + c = a1 + 2*c2
                // c1 = (a1 + c - a2)/2 ; c2 = (a2 + c - a1)/2
                double a_1 = m_para.distanceFnc(from->point, alignment.insertNode->point);
                double a_2 = m_para.distanceFnc(to->point, alignment.insertNode->point);
                double c_1 = (a_1 + c - a_2)/2.0;

                Point perpC = from->point + vect * c_1;

                alignment.insertNode->guarantiedMaxDist = seboehDist(perpC, from->point, alignment.insertNode->point);

                /// enlarge guarantiedMax by neighbours - only theoretically for test108 (but not the issue)
                // search second closest neighbour x, for point y = node
/*                Node *bestNode = nullptr;
                Node *secondBestNode;
                double bestDist = numeric_limits<double>::max();
                for (Node *open : openNodes)
                {
                    if (open == alignment.insertNode)
                        continue;
                    double dist = m_para.distanceFnc(alignment.insertNode->point, open->point);
                    if (dist < bestDist)
                    {
                        if (bestNode == nullptr)
                            secondBestNode = open;
                        else
                            secondBestNode = bestNode;
                        bestNode = open;
                        bestDist = dist;
                    }
                }

                // dist a = between x and farest edge-point of y = z
                double a = max(m_para.distanceFnc(secondBestNode->point, from->point)
                               , m_para.distanceFnc(secondBestNode->point, to->point));

                if (bestDist < a)
                {       // else, to far away to help at this edge, because the second would help more or is used as base.
                    // assume:
                    // a^2 + a^2 = b^2 -> b
                    double b = sqrt(a*a + a*a);
                    // 2b - 2a = searched dist - a is ortho to b and b should be an approx for the edge from-to
                    double alt = (2*b - 2*a);
                    alignment.insertNode->guarantiedMaxDist = max(
                                alignment.insertNode->guarantiedMaxDist
                                , alt);
                }
*/
                bestAlignments.push_back(alignment);
                alignment.insertNode->isInWay = true;
            }
        }

        // set guarantiedDist - by degree, because test109
        for (ClosestEdges &alignment : bestAlignments)
        {
            Node *node = alignment.insertNode;
            Node *prev = m_edgesGuarantied.back();
            for (Node * &next : m_edgesGuarantied)
            {
                double dist = seboehDist(prev, next, node);
                if (dist < node->guarantiedMaxDist)
                {       // because not all edges are relevant - save some time
                    /// outer alignments
                    map<double, Node*> inbetweenPointsOrdered;
                    for (Node* &nodeBetween : openNodes)
                    {
                        if (_checkAngelBetween(prev->point, next->point, node->point, nodeBetween->point))
                        {
                            inbetweenPointsOrdered.insert({_calcDegree(node, nodeBetween), nodeBetween});
                        }
                    }

                    Node *from = prev;
                    double bestDistBetween = numeric_limits<double>::max(); // search minimum, because of test65
                    Node *bestFrom, *bestTo;
                    for (auto &betweenEl : inbetweenPointsOrdered)
                    {
                        Node *to = betweenEl.second;

                        double distBetween = seboehDist(from, to, node);

                        if (distBetween < bestDistBetween)      // necessary to minimize, because test98 420x280 and in determineCluster no point between triangle at foundS.
                        {
                            bestDistBetween = distBetween;
                            bestFrom = from;
                            bestTo = to;
                        }

                        from = to;
                    }
                    Node *to = next;
                    double distBetween = seboehDist(from, to, node);
                    /// or at least direct alignment
                    if (distBetween < bestDistBetween)
                    {
                        bestDistBetween = distBetween;
                        bestFrom = from;
                        bestTo = to;
                    }

                    // global maiximize over all hull-edges
                    if (node->guarantiedDist < bestDistBetween)
                    {
                        node->guarantiedDist = bestDistBetween;
                        node->guarantiedFrom = bestFrom;
                        node->guarantiedTo = bestTo;
                    }
                    // TODO [seb] riscy?
                    node->guarantiedBasicDist = min(node->guarantiedBasicDist, bestDistBetween);

                }

                prev = next;
            }
        }

    }       // else, more innerPoint than hull-edges

    // multiple alignments together - for test60 (2group) and test45 (2group)
    determineGroupAlignment(openNodes);
}

// because test38 -> HINT: in the whole algorithm it is not allowed to have map.insert with collision.
template <typename T, typename V>
std::pair<typename T::iterator, bool> DetourClusterAlignment::_insertToMap(double dist, T &mapContainer, V &&value)
{
    auto mI = mapContainer.end();
    if ((mI = mapContainer.find(dist)) != mapContainer.end())
    {
        if (mI->second == value) return make_pair(mI, false);

        auto mIprev = mI;
        double diffPrev = 0;
        if (mIprev == mapContainer.begin())
        {
            diffPrev = dist-1;
        } else {
            --mIprev;
            diffPrev = mI->first - mIprev->first;
        }

        auto mInext = mI; ++mInext;
        double diffNext = 0;
        if (mInext == mapContainer.end())
        {
            diffNext = dist+1;
        } else {
            diffNext = mInext->first - mI->first;
        }

        dist = min(min(diffPrev, diffNext), SMALL_DOUBLE_STEP) / 2.0 + mI->first;
    }
    return mapContainer.insert({dist, value});
}


bool DetourClusterAlignment::_isNotInsideTriangle(Node *point1, Node *point2, Node *point3, std::list<Node*> &pointsInside)
{
    for (auto &insideNode : pointsInside)
    {
        if (insideNode != point1 && insideNode != point2 && insideNode != point3)
        {
            if (_isInsideTriangle(point1->point, point2->point, point3->point, insideNode->point))
            {
                return false;
            }
        }
    }
    return true;
}

DetourClusterAlignment::ClusterGrow DetourClusterAlignment::determineClusters(NodeL &input, NodeL &hull)
{
    ClusterGrow clusters;
    double coordMax = max(tspWidth, tspHeight) * 12;
    //  TODO [seboeh] * 12 - still a problem, because the intersection below can be far away.

    /// * walk through all inputs except hull-points
    for (const auto &node : input)
    {
        Cluster clu;
        clu.point = node;
        auto fI = std::find_if(hull.begin(), hull.end(),
                               [node](const shared_ptr<Node> &val)
        {
            return val->point == node->point;
        });
        if (fI != hull.end())
            continue;

        // HINT: TECHNIQUE: _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());

        /// * find closest three points including the cluster-point p.
        ///   HINT: the closest point are allways the prefered points.
        ///   Because, if the alignment point is more far away, then the alignment would cross the other point,
        ///   and then it is not clear, what the other point aligns to.
        ///   --> the closest three points are enough to see - for 'cutting'
        ///
        map<double, shared_ptr<Node>> closestPoints;
        for (const auto &node2 : input)
        {
            if (node == node2)
                continue;
            _insertToMap(m_para.distanceFnc(node->point, node2->point), closestPoints, node2);
        }
        if (closestPoints.size() < 3)
            return clusters;
        for (const auto &closE : closestPoints)
        {
            clu.closestN.push_back(closE.second);
        }

        /// * check if x is inside P_1,
        auto closestP1I = closestPoints.begin();
        auto closestP2I = closestP1I; ++closestP2I;
        auto closestP3I = closestP2I; ++closestP3I;
        list<Node*> closestTestedP;
        closestTestedP.push_back(closestP1I->second.get());
        closestTestedP.push_back(closestP2I->second.get());
        closestTestedP.push_back(closestP3I->second.get());
        Node* foundP1 = closestP1I->second.get();
        Node* foundP2 = closestP2I->second.get();
        Node* foundP3 = closestP3I->second.get();
        if ( ! _isInsideTriangle(closestP1I->second->point, closestP2I->second->point, closestP3I->second->point, node->point))
        {
            ///     * if yes, continue
            ///     * else determine next closest p_2
            ///     * check in two of P_1 and p_2, if x is inside this triangle
            ///         * if yes, continue
            ///         * else, repeat until end of inputs. -> unexpected-error, if not found
            auto closestPnextI = ++closestP3I;
            while (closestPnextI != closestPoints.end())
            {
                // 2 over n
                // TODO [seb] hint: keep closest fixed
                bool stopWhile = false;
                for (auto closestTestedP1I = closestTestedP.begin();
                     closestTestedP1I != closestTestedP.end(); ++closestTestedP1I)
                {
                    for (auto closestTestedP2I = closestTestedP.begin();
                         closestTestedP2I != closestTestedP.end()
                         && closestTestedP1I != closestTestedP2I; ++closestTestedP2I)
                    {
                        if (_isInsideTriangle((*closestTestedP1I)->point, (*closestTestedP2I)->point, closestPnextI->second->point, node->point))
                        {
                            foundP1 = *closestTestedP1I;
                            foundP2 = *closestTestedP2I;
                            foundP3 = closestPnextI->second.get();
                            closestTestedP.push_back(closestPnextI->second.get());
                            stopWhile = true;
                            break;
                        }
                    }
                    if (stopWhile) break;
                }
                if (stopWhile) break;
                closestTestedP.push_back(closestPnextI->second.get());
                ++closestPnextI;
            }
            if (closestPnextI == closestPoints.end())
                return clusters;
        }

        /// * sort closestTestedP, according to degree, because there is no order than circular around the point
        // cross product a*d - b*c, with point = (a,b) and (c,d) = (1,1)
        map<double, Node*> closestTestedPointsSorted;
        _insertToMap(_calcDegree(node.get(), foundP1), closestTestedPointsSorted, foundP1);
        _insertToMap(_calcDegree(node.get(), foundP2), closestTestedPointsSorted, foundP2);
        _insertToMap(_calcDegree(node.get(), foundP3), closestTestedPointsSorted, foundP3);

        for (const auto &cloPointsEl : closestTestedPointsSorted)
        {
            const Node *node = cloPointsEl.second;
            auto findI = std::find_if(closestPoints.begin(), closestPoints.end(),
                                   [node] (const map<double, shared_ptr<Node>>::value_type &val)
            {
               return val.second.get() == node;
            });
            if (findI != closestPoints.end())
                closestPoints.erase(findI);
        }

        // because of test114
        double d1 = seboehDist(foundP1, node->guarantiedFrom, node.get());
        d1 = max(d1, seboehDist(foundP1, node->guarantiedTo, node.get()));
        double d2 = seboehDist(foundP2, node->guarantiedFrom, node.get());
        d2 = max(d2, seboehDist(foundP2, node->guarantiedTo, node.get()));
        double d3 = seboehDist(foundP3, node->guarantiedFrom, node.get());
        d3 = max(d3, seboehDist(foundP3, node->guarantiedTo, node.get()));
        node->guarantiedMaxDist = max(d1, max(d2, d3));

        foundP1->foundDist = numeric_limits<double>::max();
        foundP2->foundDist = numeric_limits<double>::max();
        foundP3->foundDist = numeric_limits<double>::max();

        /// * scan the next closest points between already detected points
        ///   If the point between the already detected points is, with his best alignment to the cluster-point,
        ///   better than the guarantied alignment, add this point and repeat the scan.
        ///
        clu.verts.clear();
        bool isChanged = true;
        while (isChanged)
        {
            isChanged = false;
            auto closestPI = closestPoints.begin();
            while (closestPI != closestPoints.end())
            {
                // search the pair of degree between closestPI.
                bool isIterated = false;
                if (node->point.x() == 310)
                    if (closestPI->second->point.x() == 240)
                        int i=5;
                for (auto cluPointOutI = closestTestedPointsSorted.begin();
                 cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
                {
                    auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
                    if (cluPointOut2I == closestTestedPointsSorted.end())
                        cluPointOut2I = closestTestedPointsSorted.begin();

                    if (_checkAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                    {
                        /// calculations of alignment

                        double dist1 = seboehDist(cluPointOutI->second, closestPI->second.get(), node.get());
                        double dist2 = seboehDist(cluPointOut2I->second, closestPI->second.get(), node.get());

// debug
if (node->point.x() == 310)
    if (closestPI->second->point.x() == 240)
        int i=5;



        // TODO [seb] use angle instead of min(dist1,dist2)

                        // because test114
                        // before: if (std::min(dist1, dist2) <= node->guarantiedDist)
                        // calc min dist to three closest nodes.
                        double d1 = seboehDist(foundP1, closestPI->second.get(), node.get());
                        double d2 = seboehDist(foundP2, closestPI->second.get(), node.get());
                        double d3 = seboehDist(foundP3, closestPI->second.get(), node.get());
                        double bestD = min(min(d1, d2), d3);

                        // =, because it can be the guarantied alignment at the hull, which was found here
                        bool doIter = true;
                        if (bestD <= node->guarantiedDist)
                        {
                            closestPI->second->foundDist = bestD;
                            // guarantie point is not too far away, so another point is better.
                            if (closestPI->second->isHull
                                 || (cluPointOutI->second->foundDist >= bestD
                                    && cluPointOut2I->second->foundDist >= bestD))
                            {
                                // standard insert
                                _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                                isChanged = true;
                                closestPI = closestPoints.erase(closestPI);
                                doIter = false;
                            } else {
                                int i=5;
                            }
                        }

                        if (doIter)
                            ++closestPI;
                        isIterated = true;
                        break;
                    }
                }       // degree search finished
                assert(isIterated); // if (!isIterated) ++closestPI;
            }       // continue with next closest point, because they are typically equal distributed around cluster-node.
        }       // there was no change anymore, means the closest and relevant nodes are all found.

        // clear foundDist
        for (auto &el : closestTestedPointsSorted)
            el.second->foundDist = numeric_limits<double>::max();

        /// * create cluster
        for (auto &el : closestTestedPointsSorted)
            clu.closestTestedPointsSorted.push_back(el.second);

        // TODO [seb] still necessary at DetourClusterAlignment?
        // because test5 and test12 - areaOsquare solve the problem of test12 and test5
        // but here because of safety for test5 and similar, the closest edges including first.
        map<double, Node*> sortedByDegree;
        _insertToMap(_calcDegree(node.get(), foundP1), sortedByDegree, foundP1);
        _insertToMap(_calcDegree(node.get(), foundP2), sortedByDegree, foundP2);
        _insertToMap(_calcDegree(node.get(), foundP3), sortedByDegree, foundP3);
        auto sortedI = --sortedByDegree.end();
        auto sortedIn = sortedByDegree.begin();
        foundP1 = sortedIn->second;
        _insertToMap(seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , clu.neighbourEdges, make_tuple(sortedI->second, sortedIn->second));
        sortedI = sortedByDegree.begin();
        ++sortedIn;
        foundP2 = sortedIn->second;
        _insertToMap(seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , clu.neighbourEdges, make_tuple(sortedI->second, sortedIn->second));
        ++sortedI;
        ++sortedIn;
        foundP3 = sortedIn->second;
        _insertToMap(seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , clu.neighbourEdges, make_tuple(sortedI->second, sortedIn->second));


        // because test20, test100 - there can be two dots behind each other in the ray from the cluster-center
/*        double degreeMin = numeric_limits<double>::max();
        for (shared_ptr<Cluster> &clu : clusters)
        {
            Point cluP = clu->point.get()->point;
            Node *cluN = clu->point.get();
            auto iterPcurr = clu->closestTestedPointsSorted.begin();
            auto iterPprev = --clu->closestTestedPointsSorted.end();
            for (;iterPcurr != clu->closestTestedPointsSorted.end(); ++iterPcurr)
            {
                double x = fabs(_calcDegree(cluN, *iterPcurr) - _calcDegree(cluN, *iterPprev));
                double degree = fabs(std::fmod(x, 3.14158) - static_cast<int>(x/3.14158)*3.14158);
                if (degree < degreeMin)
                    degreeMin = degree;
                if (degree < 0.001)     // because there can be three points in a line - TODO improve the hard-coded number by variable number
                {
                    auto iterPrevPrev = iterPprev;
                    if (iterPrevPrev == clu->closestTestedPointsSorted.begin())
                        iterPrevPrev = --clu->closestTestedPointsSorted.end();
                    else
                        --iterPrevPrev;
                    auto iterNext = iterPcurr;
                    ++iterNext;
                    if (iterNext == clu->closestTestedPointsSorted.end())
                        iterNext = clu->closestTestedPointsSorted.begin();

                    double distPrev1 = seboehDist((*iterPrevPrev)->point, (*iterPprev)->point, cluP);
                    double distPrev2 = seboehDist((*iterPrevPrev)->point, (*iterPcurr)->point, cluP);
                    double distNext1 = seboehDist((*iterNext)->point, (*iterPprev)->point, cluP);
                    double distNext2 = seboehDist((*iterNext)->point, (*iterPcurr)->point, cluP);

                    if (min(distPrev1, distPrev2) < min(distNext1, distNext2))
                    {
                        if (distPrev1 > distPrev2)
                            swap(*iterPcurr, *iterPprev);
                           // else nothing, because all already correct
                    } else {
                        if (distNext1 < distNext2)
                            swap(*iterPcurr, *iterPprev);
                    }
                }

                iterPprev = iterPcurr;
            }
        }
*/

        /// collect all child-triangles below the main triangle
        // add old edge, because test9 and test19
        map<double, Node*>::iterator beginI = closestTestedPointsSorted.begin();
        while (beginI != closestTestedPointsSorted.end()
               && beginI->second != foundP1
               && beginI->second != foundP2
               && beginI->second != foundP3
               )
            ++beginI;

        auto foundP = foundP1;
        auto foundPN = foundP2;
        auto foundS = foundP3;              // because test10-2
        int foundI = 0;
        if (beginI->second == foundP2)
        {
            foundP = foundP2;
            foundPN = foundP3;
            foundS = foundP1;
            foundI = 1;
        }
        else if (beginI->second == foundP3)
        {
            foundP = foundP3;
            foundPN = foundP1;
            foundS = foundP2;
            foundI = 2;
        }
        assert(closestTestedPointsSorted.size() > 2);

        // toMap
        list<Node*> closestTestedNodesSorted(closestTestedPointsSorted.size());
        transform(closestTestedPointsSorted.begin(), closestTestedPointsSorted.end(), closestTestedNodesSorted.begin(),
                  [](const pair<double, Node*> &val) -> Node*
        {
            return val.second;
        });

        map<double, Node*>::iterator pointI = beginI; ++pointI;
        while (pointI != beginI)
        {
            if (foundPN == pointI->second)
            {
                foundI = ++foundI % 3;
                if (foundI == 0)
                {
                    foundP = foundP1;
                    foundPN = foundP2;
                    foundS = foundP3;
                } else if (foundI == 1)
                {
                    foundP = foundP2;
                    foundPN = foundP3;
                    foundS = foundP1;
                } else {
                    foundP = foundP3;
                    foundPN = foundP1;
                    foundS = foundP2;
                }
            } else {
                if (node->point.x() == 420)
                {
                    if (foundS->point.x() == 560)
                        int i=5;
                }
                // create edge for test9 and test19
                if (_isNotInsideTriangle(node.get(), pointI->second, foundP, closestTestedNodesSorted))     // check, because test98
                    _insertToMap(seboehDist(foundP, pointI->second, node.get())
                                               , clu.neighbourEdges, make_tuple(foundP, pointI->second));

                if (_isNotInsideTriangle(node.get(), pointI->second, foundPN, closestTestedNodesSorted))
                    _insertToMap(seboehDist(pointI->second, foundPN, node.get())
                                               , clu.neighbourEdges, make_tuple(pointI->second, foundPN));

                // because test10-2
                if (_isNotInsideTriangle(node.get(), pointI->second, foundS, closestTestedNodesSorted))
                    _insertToMap(seboehDist(pointI->second, foundS, node.get())
                                               , clu.neighbourEdges, make_tuple(pointI->second, foundS));
            }

            ++pointI;
            if (pointI == closestTestedPointsSorted.end())
                pointI = closestTestedPointsSorted.begin();
        }


        // debug
        if (node->point.x() == 310)
            int i=5;

        for (auto closEl : clu.neighbourEdges)
        {
            Node *from = std::get<0>(closEl.second);
            Node *to = std::get<1>(closEl.second);
            if ((from->point.x() == 290 || to->point.x() == 290)
                    && (from->point.x() == 320 || to->point.x() == 320))
                int i=5;
        }

        /// repeat from beginning, until all inputs are handled.
        clusters.push_back(shared_ptr<Cluster>(new Cluster(clu),no_op_delete()));
    }

    return clusters;
}

// VoronoiClusterAlignment and DetourClusterAlignment
void DetourClusterAlignment::_collectClusterEdges(map<double, Cluster*> &clustersM)
{
    m_edgeToClusters.clear();

    // TODO [seb] there are more than 2clu best 3, or more
    // add clusters
    for (auto &cluEl : clustersM)
    {
        Cluster* clu = cluEl.second;

        if (clu->point->point.x() == 654)
            int i=5;

        // add the way circular and closeing triangle - because test5.
        for (auto neiEl : clu->neighbourEdges)
        {
            std::tuple<Node*,Node*> neigh = neiEl.second;
            Node *prev = std::get<0>(neigh);
            Node *next = std::get<1>(neigh);
            if (prev->point.x() == 603 && next->point.x() == 869)
                int i=5;
            if (m_edgeToClusters[prev][next].attachments.empty())
            {       // init
                m_edgeToClusters[prev][next].edge.from = prev;
                m_edgeToClusters[prev][next].edge.to = next;
                m_edgeToClusters[next][prev].edge.from = next;
                m_edgeToClusters[next][prev].edge.to = prev;
            }
            _insertToMap(neiEl.first, m_edgeToClusters[prev][next].attachments, clu);
            _insertToMap(neiEl.first, m_edgeToClusters[next][prev].attachments, clu);       // see test10-2
        }
    }
}


DetourClusterAlignment::ClusterEdge * DetourClusterAlignment::_findClusterToEdge(Node *from, Node *to)
{
    int i=0;
    for (; i < 2; ++i)
    {
        auto iterEdge = m_edgeToClusters.find(from);
        if (iterEdge != m_edgeToClusters.end())
        {
            auto iterEdge2 = iterEdge->second.find(to);
            if (iterEdge2 != iterEdge->second.end())
            {
                return &iterEdge2->second;
            }
        }
        swap(from,to);
    }
    return nullptr;
}


// VoronoiClusterAlignment and DetourClusterAlignment
std::list<DetourClusterAlignment::OpenClustersAtEdges::iterator>
DetourClusterAlignment::_insertCluEIfPossible(ClusterEdge *cluE, OpenClustersAtEdges &openN)
{
    std::list<OpenClustersAtEdges::iterator> ret;
    if (cluE != nullptr)
    {
        ///         * if not visited, insert to openN, each
        for (auto attEl : cluE->attachments)
        {
            Cluster *clu = attEl.second;
            if (clu->state != 1)
            {
                OpenClusterEdge oClu;
                oClu.edge = cluE->edge;
                oClu.clu = clu;
                ret.push_back(_insertToMap(attEl.first, openN, oClu).first);       // M20190111
            }
        }
    }
    return ret;
}

// HINT: TODO [seb]: it is possible to improve search of p. By using index for open - map<Node*, open::iterator>
// because nodes, which have state=1 can not be used as next p, and the open map is sorted by dist and not by Node*.
bool DetourClusterAlignment::_searchBestPathByConvexHullAlignment(OpenClustersAtEdges &open)
{
    // Version 2
    bool isLast = false;
//    ///     * copy map to openN and delete the element p
//    OpenClustersAtEdges openN = open;
//    auto openEI = openN.begin();
//    Cluster *p = nullptr;
//    ClusterEdge *cluEdge = nullptr;
//    while (p == nullptr)
//    {
//        cluEdge = openEI->second;

//        if (cluEdge->clu1->state != 1)
//            p = cluEdge->clu1;
//        else if (cluEdge->clu2 != nullptr && cluEdge->clu2->state != 1)
//            p = cluEdge->clu2;
//        // TODO [seb] what is with n-closest point?

//        openEI = openN.erase(openEI);       // first is unique because M20190111
//        if (openEI == openN.end())
//            break;
//    }
//    if (p == nullptr)
//        _searchBestPathByConvexHullAlignment(openN);        // because, final cancel-criteria

    /// * go through all elements p of open
    // version 1
    double lengthChange;
    bool isFound = false;
    for (auto openEI = open.begin();
         openEI != open.end(); )
//    auto openEI = open.begin();
//    for (int i=0; i < 2 && openEI != open.end(); ++i)
    {
        ///     * copy map to openN and delete the element p
        OpenClusterEdge oCluEdge;
        Cluster *p = nullptr;
        while (p == nullptr)
        {
            oCluEdge = openEI->second;

            if (oCluEdge.clu->state != 1)
                p = oCluEdge.clu;
//            else if (cluEdge->clu2 != nullptr && cluEdge->clu2->state != 1)
//                p = cluEdge->clu2;
            // TODO [seb] what is with n-closest point?

            if (p == nullptr)
            {
//                openEI = open.erase(openEI);       // first is unique because M20190111
                ++openEI;
                if (openEI == open.end())
                    break;
            }
            else
                ++openEI;
        }
        if (p == nullptr)
        {
//            _searchBestPathByConvexHullAlignment(open);        // because, final cancel-criteria
            if (!isFound)
            {
                /// * cancel criteria
                ///     * add p to way
                ///     * check way-length and update way
                double length = 0;
                Node *prev = m_currentWay.back();
                for (Node *next : m_currentWay)
                {
                    length += m_para.distanceFnc(prev->point, next->point);
                    prev = next;
                }
                if (length < m_bestLength)
                {
                    m_bestLength = length;
                    m_bestWay = m_currentWay;
                }
            }
            return true;
        }
        else
        {
            ///     * mark p as visited

    // version 2
    //        for (auto neighbourEl : p->neighbourEdges)
    //        {
//                Node *from = std::get<0>(neighbourEl.second);
//                Node *to = std::get<1>(neighbourEl.second);
            Node *from = oCluEdge.edge.from;
            Node *to = oCluEdge.edge.to;

                auto wayInsertedI = m_currentWay.end();
                ///     * add p to way
                if (p->point->point.x() == 340)
                    int i=5;
                if (p->point->point.x() == 310)
                {
                    if ((from->point.x() == 160 && to->point.x() == 340)|| (from->point.x() == 340 && to->point.x() == 160))
                        int i=5;
                }
                if (p->point->point.x() == 340)
                {
                    if ((from->point.x() == 160 && to->point.x() == 340)|| (from->point.x() == 340 && to->point.x() == 160))
                        int i=5;
                }
                if (p->point->point.x() == 340)
                {
                    if ((from->point.x() == 160 && to->point.x() == 320)|| (from->point.x() == 320 && to->point.x() == 160))
                        int i=5;
                }

                auto wayI = --m_currentWay.end();       // trick because edge from 'last' to 'first' node.
                if (*wayI != from)
                {
                    wayI = m_currentWay.begin();
                    for (; wayI != m_currentWay.end(); ++wayI)
                    {
                            // both edges form/to below, because safety.
                        if (*wayI == from)
                        {
                            ++wayI;
                            if (wayI == m_currentWay.end())
                                wayI = m_currentWay.begin();
                            if (*wayI == to)
                                break;
                        }
                    }
                }
                else
                    wayI = m_currentWay.begin();
                if (wayI != m_currentWay.end())
                {
                    auto prevWayI = wayI;
                    if (prevWayI == m_currentWay.begin())
                        prevWayI = m_currentWay.end();
                    --prevWayI;
                    lengthChange = - m_para.distanceFnc((*prevWayI)->point, (*wayI)->point);
                    lengthChange += m_para.distanceFnc((*prevWayI)->point, p->point->point);
                    lengthChange += m_para.distanceFnc(p->point->point, (*wayI)->point);

                    // pruning?
                    if (m_currentLength < m_bestLength)
                    {
                        wayInsertedI = m_currentWay.insert(wayI, p->point.get());
                        m_currentLength += lengthChange;
                    } else
                        continue;       // pruning
                } else
                    continue;       // because the cluster can have opposit directions
                                    // see test4 clu-center 80 with edge 154 to 215.


                isFound = true;
                p->state = 1;

                ///     * find clu1 clu2 of the two edges between cluEdge.edge and p.point
                ///     * check if clu1 or clu2 ar not visited already
                // TODO [seb] there are more than two clu possible at one edge
                auto newClu1AtEdgeIters = _insertCluEIfPossible(
                            _findClusterToEdge(
                                from, p->point.get())
                            , open);
                auto newClu2AtEdgeIters = _insertCluEIfPossible(
                            _findClusterToEdge(
                                p->point.get(), to)
                            , open);

                ///     * start recursion again.
                isLast = _searchBestPathByConvexHullAlignment(open);

                ///     * clear for recursion
                for (const auto &iter : newClu1AtEdgeIters)
                    if (iter != open.end())
                        open.erase(iter);
                for (const auto &iter : newClu2AtEdgeIters)
                    if (iter != open.end())
                        open.erase(iter);
                if (wayInsertedI != m_currentWay.end())
                {
                    m_currentLength -= lengthChange;
                    m_currentWay.erase(wayInsertedI);
                }
//                if (isLast)      // last is always aligned at best edge
//                    break;
//            }
            p->state = 0;
        }
    }

    return false; //openEI == openN.end();
}


void DetourClusterAlignment::_runEvolutionaryGrow(NodeL &hull)
{
    m_bestLength = std::numeric_limits<double>::max();
    m_bestWay.clear();
    m_currentLength = 0;
    m_currentWay.clear();

    // * create openEdges list with alignments to hull
    OpenClustersAtEdges open;

    Node *prev = hull.back().get();
    for (auto sp : hull)
    {
        Node *next = sp.get();
        m_currentWay.push_back(next);
        if (m_currentWay.size() > 1)
            m_currentLength += m_para.distanceFnc((*(--(--m_currentWay.end())))->point, m_currentWay.back()->point);

        _insertCluEIfPossible(_findClusterToEdge(prev, next), open);        // see test46

        prev = next;
    }
    m_currentLength += m_para.distanceFnc(m_currentWay.back()->point, m_currentWay.front()->point);

    // * start recursive search (similar to A*-search)
    _searchBestPathByConvexHullAlignment(open);
}

tuple<map<double, Cluster *>, Cluster*> DetourClusterAlignment::determineStructure(Way *way, NodeL &hull, ClusterGrow &clustersA)
{
    map<double, Cluster*> clustersM;
    Cluster* root;
    int i = 0;
    for (auto &cluE : clustersA)
    {
        double area = 0;
        Cluster *clu = cluE.get();

        /// * pre-calc cluster alignments at neighbours
        auto neighNextI = clu->closestTestedPointsSorted.begin();
        for (Node* &neigh : clu->closestTestedPointsSorted)
        {
            ++neighNextI;
            if (neighNextI == clu->closestTestedPointsSorted.end())
                neighNextI = clu->closestTestedPointsSorted.begin();
//            double dist = seboehDist(neigh, *neighNextI, clu->point.get());
//            _insertToMap(dist, clu->neighbourEdges
//                         , std::tuple<Node*,Node*>(neigh, *neighNextI));
            area += m_para.distanceFnc(clu->point->point, neigh->point);
        }
        area /= clu->closestTestedPointsSorted.size();
        clu->area = area;

        //clustersM.insert({clu->area, clu});
        clustersM.insert({area, clu});
    }

    NodeL input;

    // reduce cluster-neighbours
    bool isNewClusters = false;
    for (auto cluI = clustersM.begin(); cluI != clustersM.end(); )
    {
        Cluster *clu = cluI->second;
        if (clu->point->algoState != Node::AlgoState::RESERVED)
        {
            ++cluI;
            auto neighI = clu->closestTestedPointsSorted.begin();
            for (; neighI != clu->closestTestedPointsSorted.end();)
            {
                if ((*neighI)->algoState != Node::AlgoState::RESERVED)
                {
                    (*neighI)->algoState = Node::AlgoState::RESERVED;
                    ++neighI;
                }
                else
                    neighI = clu->closestTestedPointsSorted.erase(neighI);
            }
            if (!clu->closestTestedPointsSorted.empty())
                isNewClusters = true;
            input.push_back(clu->point);
        }
        else
            cluI = clustersM.erase(cluI);
    }

    if (input.size() > 1)
    {
        // * set pruning criterias to Node
        determineGuarantiedAlignment(input, hull);

        // * determine Voronoi cluster
        auto clusters = determineClusters(input, hull);

        map<double, Cluster*> clustersParents;
        tie(clustersParents, root) = determineStructure(way, hull, clusters);
        // insert parents
        for (auto cluE : clustersM)
        {
            Cluster *clu = cluE.second;
            for (auto cluPE : clustersParents)
            {
                Cluster *par = cluPE.second;
                auto cluChildI = find(par->closestTestedPointsSorted.begin(), par->closestTestedPointsSorted.end()
                                      , clu->point.get());
                if (cluChildI != par->closestTestedPointsSorted.end())
                {
                    clu->parent = make_shared<Cluster>(*par);
                    clu->childs.push_back(make_shared<Cluster>(*clu));
                }
            }
        }
    }
    else
        root = clustersM.begin()->second;

    return make_tuple(clustersM, root);
}


void DetourClusterAlignment::_insertNodes(Way *way, std::list<Node*> &nodes)
{
    if (way->size() < 3) return;

    // preordered, to guaranty better alignment
    map<double, Node*> sorted;
    for (Node *node : nodes)
    {
        auto wayI = way->begin();
        while (!wayI.end())
        {
            Node *wn = wayI.nextChild();
            Node *wnn = wayI.nextChild();
            sorted.insert({seboehDist(wn, wnn, node), node });
        }
    }

    for (auto &el : sorted)
    {
        Node *node = el.second;

        auto wayI = way->begin();
        auto bestI = wayI;
        double bestDist = numeric_limits<double>::max();
        while (!wayI.end())
        {
            Node *wn = wayI.nextChild();
            auto tmpI = wayI;
            Node *wnn = wayI.nextChild();
            double dist = seboehDist(wn, wnn, node);
            if (dist < bestDist)
            {
                bestDist = dist;
                bestI = tmpI;
            }
        }
        way->insertChild(bestI, node);
    }


}


void DetourClusterAlignment::_recursiveWayCreationByHierarchy(Way *way, NodeL &hull, Cluster *currentClu)
{
    /// * walk depth-first
    if (currentClu->childs.empty())
    {       // leaf



    }

// .....

    /// * align to best edges starting from hull
//    if (!clustersM.empty())
//    {
//        _runEvolutionaryGrow(hull);
//    } else {
//        for (auto sp : hull)
//            m_bestWay.push_back(sp.get());
//    }


}


DetourClusterAlignment::NodeL DetourClusterAlignment::determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way)
{
    assert(way->size() > 1);

    /// * create structure
    map<double, Cluster*> clustersM;
    Cluster *root = nullptr;
    tie(clustersM, root) = determineStructure(way, hull, clustersA);

    /// align to hull
    _recursiveWayCreationByHierarchy(way, hull, root);

    /// * create result-struct
    NodeL ret;
    for (Node *node : m_bestWay)
        ret.push_back(make_shared<Node>(*node));

    return ret;
}

void DetourClusterAlignment::buildTree(DT::DTNode::NodeType node, float level, TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    m_resultLength = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    incProgress(progressPercentage);

    // * intern Input representation
    auto input = createInputRepresentation(openPointsBasic);
    m_tspSize = input.size();

    // * determine hull
    auto res = determineHull(input);
    auto hull = get<0>(res);
    Way *way = get<1>(res);

    // * set pruning criterias to Node
    determineGuarantiedAlignment(input, hull);

    // * determine Voronoi cluster
    auto clusters = determineClusters(input, hull);

    // * build final path
    auto finalWay = determineWay(clusters, hull, way);

//    if (m_isCanceled)
//    {
        // set any way - like the input, because we want valid m_resultLength values at further processing.
//        m_shortestWay.clear();
//        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
//             openNodeIter != openPointsBasic->end(); ++openNodeIter)
//        {
//            m_shortestWay.push_back(**openNodeIter);
//        }
//        m_mutex.lock();
//        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
//        m_mutex.unlock();
//    }

    /// copy result to final output
    for (auto pointI = finalWay.begin();
         pointI != finalWay.end(); ++pointI)
    {
        TSPWay w;
        w.push_back((*pointI)->point);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial ways - drop this tree of ways.
        (*newChildIter)->parent(node);
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}
