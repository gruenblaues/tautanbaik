#ifndef TAMARALGORITHM_H
#define TAMARALGORITHM_H

#include "dll.h"
#include <fstream>
#include <chrono>

#include <TspAlgoCommon.h>
#include <bspTreeVersion/BspNode.h>
#include <dataStructures/OlavList.h>
#include <bspTreeVersion/BspTreeSarah.h>

namespace TSP {

class EXTERN TamarAlgorithm : public TspAlgoCommon
{
public:
    enum class TamarState {
      DirectionW2Forward, DirectionW2Backward
    };
    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };
    enum class MergeCallState {
        leftRight, topDown
    };
    enum class DirectAccessState {
        isDirectAccess, isNoDirectAccess
    };

    TamarAlgorithm();
    virtual ~TamarAlgorithm();

    /**
     * @brief setBlockedEdges sets the edges according to
     * @param edges they shall be omited
     */
    void setBlockedEdges(std::list<std::pair<Point,Point>> &&edges)
    {
        m_blockeEdgesInput = std::move(edges);
    }

protected:
    void run() override;

    friend class TamarDebugging;

    std::chrono::system_clock::time_point m_startTime;

protected:      // protected, cause of tests.
    typedef oko::OlavList Way;
    typedef oko::NodeChild NodeChild;
    typedef oko::Node Node;
    typedef oko::ClosestEdges ClosestEdges;

    struct EdgeIter {
        EdgeIter(Way::iterator &aNodeIter, Way::iterator &bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
        Way::iterator aNodeI;
        Way::iterator bNodeI;
    };

    class HirarchicalNode
    {
    public:
        struct Edge {
            Point a;
            Point b;
        };
        Edge points;
        double value = std::numeric_limits<double>::max();

        HirarchicalNode *childA = nullptr;
        HirarchicalNode *childB = nullptr;
        HirarchicalNode *parent = nullptr;
    };

    typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;

    Way * newConqueredWay(bsp::BspNode *node);
    float m_progressMax = 0;
    typedef std::list<ClosestEdges> NavelsZippedType;
    typedef std::map<Node*, NavelsZippedType> NavelsMapType;

    struct Navel {
        Navel(NavelsZippedType *aNavelRef, Node *aAnchorNode, double aDist)
            : navelRef(aNavelRef), anchorNode(aAnchorNode), dist(aDist) {}
        NavelsZippedType *navelRef = nullptr;
        NavelsZippedType navelCopy;
        Node *anchorNode = nullptr;
        double dist = std::numeric_limits<double>::max();
    };
    typedef std::list<Navel> NavelsType;
    NavelsType m_navels;
    struct SortNavelEl {
        SortNavelEl(Navel *aNavel, const ClosestEdges &aCe)
            : navelFrom(aNavel), ce(std::move(aCe)) {}
        Navel *navelFrom = nullptr;
        Navel *navelTo = nullptr;
        double gapOpp = std::numeric_limits<double>::max();
        ClosestEdges ce;
    };
    typedef std::multimap<double, SortNavelEl> SortNavelType;
    SortNavelType m_navelSorted;

    // candidates and backtracking
    SortNavelType m_oppositeNavelSorted;     // gS
    SortNavelType m_returnersNavelSorted;

    // TODO [seboeh] ...improve - merge SortNavelEl and ClosestRootNavel.
    struct ClosestNavel
    {
        Navel *navel = nullptr;     ///<< if navel set, it is an navel, else it is an root.
        double dist = std::numeric_limits<double>::max();
        double gapOpp = 0;      // 0 is neutral in gaps

        void clear()
        {
            navel = nullptr;
            dist = std::numeric_limits<double>::max();
        }
    };

    // fix edges
    std::list<std::pair<Point,Point>> m_blockeEdgesInput;
    using BlockedEdgesType = std::map<long long, std::list<Point>>;
    BlockedEdgesType m_blockedEdges;

    // expSearch
    double m_maxdistAB = 0;


    /**
     * @brief mergeWays - conqueror step
     * @param way1 - will be enlarged
     * @param way2 - can be deleted afterwards, is moved to way1.
     * @param callState - left-right-merge, top-bottom-merge
     */
    void mergeWays(bsp::BspNode *origin, Way *&way1, const Point &pWay1Center, Way *&way2, const Point &pWay2Center, const MergeCallState &callState, const Point &pCenter);

    double seboehDist(const Point &p1, const Point &p2, const Point &pNew);
    double seboehDist(const ClosestEdges &ce);
    double seboehDist(const Node *from, const Node *to, const Node *insert);
    double correctDist(NavelsZippedType &navelRoot);

    double calcGravity(NavelsZippedType &navel, Node *fromNode, Node *toNode, Node *innerPoint);
    double gravity(Node *p1, Node *p2, Node *pInsert);
    void updateGravity(NavelsMapType &navels, std::list<Node *> &originalInnerNodes, Node *backwardFromNode, Node *middleFromNode, Node *forwardFromNode);

    TamarAlgorithm::CrossedState isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint = nullptr);
    /// [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    bool isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);
    bool createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI);
    void createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI);
    bool isCovered(Way *way, Way::iterator &w1I, Way::iterator &fixI);
    DirectAccessState isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI);
    /// [http://www.wolframalpha.com/input/?i=area+of+triangle]
    void flipWay(Way *way, Point pCenter);
    Point calculateMeanPointOfWayCenter(Way *way1, Way *way2);
    bool isClockwise(TSP::Point pCenter, Way *way);

    /// This method creates childnodes for insertNode.
    void insertNewNodeToEdge(Way* way1, ClosestEdges &cx);
    void mergeCascada(Way::iterator haI, Way::iterator laI, Way::iterator hbI, Way::iterator lbI, Way *way1, Way *way2, Point pCenter);
    void collectAllCloseInnerNodes(std::list<std::shared_ptr<ClosestEdges> > &innerPoints, Way &way);

    void findClosestEdgeDepthFirstInHull(bsp::Edge* &resultEdge, double &reachedDist, bsp::BspNode *hullNode, const TSP::Point &point);
    void selectClosestEdgeFromSpace(bsp::Edge* &closestEdge, double &reachedDist, std::list<const bsp::Edge *> &hullEdgeList, const Point &point);
    std::tuple<bsp::Edge*, double> selectFirstClosestEdge(bsp::BspNode *origin, const Point &point);

    void navelZipoIgelMerge(Way *hull, ClosestEdgesListInputType &innerPoints);

    void removeNodeFromPolygon(Way *hull, Node *node, int stateWithoutNewEdge = 0);

    void wipeFoldingOpenAndInitiateClose(std::list<Node *> &originalInnerNodes, NavelsMapType &navels, std::list<Node *> &froms);
    void wipeFoldingIncIter(std::list<Node*> &froms
                              , std::list<Node*>::iterator &backwardCascadeFromI
                              , std::list<Node*>::iterator &forwardCascadeFromI
                              , std::list<Node*>::iterator &middleCascadeFromI);

    std::tuple<std::list<ClosestEdges>::iterator, double> findBestSingleSearch(NavelsMapType::mapped_type &navelRoot, const ClosestEdges &ce);

    bool getNextNavel(NavelsMapType &navels, NavelsMapType::iterator &navelI, std::list<Node*> &fromNodes, NavelsMapType::mapped_type *&nextNavelRoot, std::list<Node*>::iterator fromNodeI);

    bool wipe(Node *currentNavelFrom, Node *currentNavelTo, Node *nextNavelFrom, Node *nextNavelTo, NavelsMapType::mapped_type *nextNavelRoot, NavelsMapType::mapped_type *currentNavelRoot, bool isForward);
    std::tuple<double, std::list<ClosestEdges>::iterator, std::list<ClosestEdges>, std::list<ClosestEdges>> calculateWipe(Node *currentNavelFrom, Node *currentNavelTo, Node *nextNavelFrom, Node *nextNavelTo, std::list<ClosestEdges> *currentNavelRoot, std::list<ClosestEdges> *nextNavelRoot);

    void reorderNavels(NavelsMapType::iterator &mapI, Node *startNode, bool isFrom);
    void reorderCurrentNavel(const std::list<Node *> &innerNodes, std::list<ClosestEdges> &currentNavelOrdered, const NavelsMapType::mapped_type *currentNavelRoot);

    void adaptPrevNextCeAndErase(std::list<ClosestEdges> &newNodes, std::list<std::list<ClosestEdges>::iterator>::iterator &ceII);

    /// This method exists, because of second-closest-edge problem, See Test5.
    void improveNavels(NavelsMapType &navels);
    void wipeAll(NavelsMapType &navels, std::list<Node *> &fromNodes);

    std::tuple<double, std::list<ClosestEdges>, std::list<ClosestEdges> > calculateWipeOpen(Node *currentNavelFrom, Node *currentNavelTo, Node *nextNavelFrom, Node *nextNavelRootMainListNode, std::list<ClosestEdges> *currentNavelOrdered, std::list<ClosestEdges> *nextNavelRoot, std::list<ClosestEdges> *middleNavelOrdered);
    NavelsZippedType::iterator eraseFromCeList(std::list<ClosestEdges>::iterator &iter, std::list<ClosestEdges> &ceList);

    /// Find the closest inner point in @a innerNodesOrdered in seboehDist to currentNavelFrom and currentNavelTo.
    std::tuple<double, TamarAlgorithm::NavelsZippedType::iterator, TamarAlgorithm::NavelsZippedType::iterator, bool>
        findClosestAndSecondClosestOfHigherOrders(NavelsZippedType *innerNodesOrdered, Node *currentNavelFrom, Node *currentNavelTo);

    /// Search over all Navels and their innerNodes the best gravity of an alternative Navel, instead of forward/middle/backward.
    std::tuple<double, TamarAlgorithm::NavelsMapType::iterator> wipeFoldingCloseAlternative(NavelsMapType &navels, Node *forwardFromNode, Node *middleFromNode, Node *backwardFromNode, Node *insertNode);

    /// for test29.png
    /// Alignment according to two closest points on navel, and decision over back, middle and front navel.
    std::tuple<int, NavelsZippedType, NavelsZippedType, NavelsZippedType, std::list<NavelsMapType::iterator>, std::list<double> >
    wipeFoldingCloseGravity(NavelsZippedType &relevantNodesOrdered, Node *middleFromNode, Node *middleToNode, Node *forwardFromNode, Node *forwardToNode, Node *backwardFromNode, Node *backwardToNode, NavelsMapType &navels, std::list<Node *> &originalInnerNodes);

    void updateWipeCloseResult(NavelsMapType &navels, std::list<NavelsMapType::iterator> &navelIs, NavelsZippedType &frontListCloseRes
                               , std::list<ClosestEdges> *middleList, NavelsZippedType &backListCloseRes, Node *middleFromNode
                               , NavelsZippedType &middleListCloseRes, std::list<ClosestEdges> *frontList, std::list<ClosestEdges> *backList
                               , bool &isChanged, Node *backwardFromNode, Node *forwardFromNode, bool closeCombiImproved);

    bsp::BspTreeSarah createBSPTree(InputCitiesPointerType &cityPoints);

    int iterateBackOrForward(const NavelsZippedType::iterator &closestBestIter, bool isBackward, NavelsZippedType::iterator &ceI, NavelsZippedType &listOrdered);

    void searchClosestFirstHullEdges(std::list<Node*> &originalInnerNodes, Way *hull, std::list<Node *> &fromNodes, NavelsMapType &navels);
    void wipeForwardBackward(NavelsMapType &navels, std::list<Node *> &fromNodes);
    void removeFromPolygon(ClosestEdges &ce, Way* innerPointWay);

    /// Try to combine nodes from back and front to middle list.
    std::tuple<bool, NavelsZippedType, NavelsZippedType, NavelsZippedType> wipeFoldingCloseCombination(NavelsZippedType &backList, NavelsZippedType &middleList, NavelsZippedType &frontList, Node *middleFromNode, Node *middleToNode, Node *forwardFromNode, Node *forwardToNode);

    std::tuple<Node *, Node *> determineClosestNodesInCeList(NavelsZippedType &navel, NavelsZippedType::iterator &ceI);

    /// Just for do step of wipeFoldingCloseeCombination() on original nodes, and add result to newCloseRes...
    void wipeFoldingCloseCombinationResultOnOriginalLists(std::list<ClosestEdges> *middleList, bool &closeCombiImproved, std::list<ClosestEdges> *backList, Node *forwardNextNode, NavelsZippedType &backListCloseRes, NavelsZippedType &middleListCloseRes, Node *forwardFromNode, std::list<ClosestEdges> *frontList, Node *middleFromNode, NavelsZippedType &frontListCloseRes, Node *middleNextNode);
    double correctDistTMP_DELETE(const NavelsZippedType &navelRoot);
    bool wipeMultiple(NavelsType::iterator &currentNavelI);

    // candidates and backtracking

    bool wipeCandidateWithBacktracking(NavelsType::iterator &currentNavelI, NavelsType::iterator &oppositeNavelI, double &distCurrentWholeNavel);
    void sortAlignedOnEdge(SortNavelType &navelSorted, NavelsType::iterator &currentNavelI, NavelsType::iterator &oppositeNavelI);
    void sortAlingedOnEdgeBackward(SortNavelType &navelSorted, std::list<SortNavelEl> &returnCandidates, NavelsType::iterator &originalNavelI);
    void activateCandidateInCeList(Node *insertNode, std::list<ClosestEdges> &navelCeList);
    void wipeAllCandidatesWithBacktracking();
    std::tuple<TamarAlgorithm::ClosestNavel, TamarAlgorithm::ClosestNavel, TamarAlgorithm::ClosestNavel, TamarAlgorithm::ClosestNavel> findClosestEdgesFour(Node *anchorNode, Node *oppAnchorNode, Node *node);
    /// @return the costs of leaving of applicant the oldNavel. Negative values, means the costs are greater than before, positive means, there is an benefit at leaving.
    /// If leavingApplicaitonI is not in oldNavel, nextEmplayOldNavelI have to be in oldNavel, else nextEmployOldNavelI can be nullptr.
    double calcGapInNavel(NavelsZippedType::iterator *leavingApplicantI, NavelsZippedType::iterator *nextEmployOldNavelI, NavelsZippedType &oldNavel, Node *fromNode = nullptr);
    double calcGapInNavel(Node *insertNode, NavelsZippedType::iterator *nextEmployOldNavel, NavelsZippedType &oldNavel, bool &isOk);
    void rejectCandidateInCeList(Node *insertNode, NavelsZippedType &navelCeList);
    double calcNewLocalCosts2(NavelsZippedType::iterator *nextEmployOldNavelI, NavelsZippedType::iterator *leavingApplicantI, NavelsZippedType &oldNavel, Node *fromNode);
    void debugFindWrongOrder();
    bool wipeDivergenzeCloseSingle(const NavelsType::iterator &navelCurrentI);
    void wipeCandidateAll(size_t type);
    std::tuple<NavelsZippedType::iterator, NavelsZippedType::iterator> findFromTo(Node *insert, NavelsZippedType &ceList);
    ClosestEdges updateBestResort(Node *fromNode, Node *toNode, Node *insert, NavelsZippedType::iterator &iter, NavelsZippedType &ceList, ClosestEdges *newCeP = nullptr);
    bool isNormalVectorLeft(const Point &from, const Point &to, const Point &insert);
    std::tuple<std::list<TamarAlgorithm::ClosestEdges>::iterator, double> findBestSingleCeAndAdd(Node *toNode, Node *fromNode, const ClosestEdges &element, std::list<ClosestEdges> &ceList, ClosestEdges *newCeP = nullptr, bool isOldMethod = false, bool isGroupAlign = false);
    double correctDistTMP_DELETE_WITH_CANDIDATE(const NavelsZippedType &navelRoot);
    /// inserts the @a insert node finally. Is applied in wipeDivergenzeCloseSingle(). TODO [seboeh] use also in wipeDivergenzeClose().
    TamarAlgorithm::NavelsZippedType::iterator insertBestResortedPos(Node *from, Node *to, Node *insert, NavelsZippedType &ceList, int level = 0);
    /// after erase from ceList, the iterator @a iter is not longer valid. It has to be iterated over ceList again.
    void eraseFoundBestPos(Node *insert, NavelsZippedType &ceList);
    bool wipeDivergenzeClose(const NavelsType::iterator &navelCurrentI, double &bestDistFound);
    TamarAlgorithm::NavelsZippedType::iterator findBestSingleSearchGroupAlign(NavelsZippedType &ceList, Node *insert);
    TamarAlgorithm::NavelsZippedType::iterator eraseFromCeList(Node *el, std::list<ClosestEdges> &ceList);
    double calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3);
    double seboehDistWithGravity(Node *from, Node *to, Node *insert, Point &center);
    double seboehDistWithGravity(const ClosestEdges &ce, Point &center);
    Point calcCenter(const NavelsZippedType &ceList);
    double localDecision(double area, double detour, double distCenter);
    Point testPointsInOrder(NavelsZippedType::iterator thirdPointI, Node *secondPointN, Node *firstPointN);
    double calcLengthOfPolygonFromNavelsAndHull(NavelsType &navelsKnapSack, Way *hull);
    void previousInserts(Way *way, Way::iterator &hullI);
    long long blockedEdgesCRC(const Point &p1);
    void removeDoubleEntries(ClosestEdgesListInputType &innerNodes);
    std::tuple<double, bool> triangleHeightC(const TSP::Point &ap, const TSP::Point &bp, const TSP::Point &cp);
    std::list<Node *> convexSorting(std::list<Node *> &innerPoints, Way &hull);
    void _normalizeByRange(std::map<double, Node *> &in);
    void _triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI);
    void _savePoints(std::list<Node *> points);
    std::list<Point> hirachicalSorting(const std::list<Point> &points);
    std::list<Node *> rotatedSorting(const std::list<Node *> &nodes, Way &hull);
    void sortedMerge_resortIntoHull(Way &hull);
    void _setHullVector(Node *node, Way &hull);
    std::list<Node *> selectNavels(Way *hull, std::list<Node *> &originalInnerNodes);
};

}

#endif // TAMARALGORITHM_H
