#ifndef TSPALGOSEBOEH_H
#define TSPALGOSEBOEH_H

#include <assert.h>

#include "TspAlgoCommon.h"
#include "Parameters.h"
#include "Indicators.h"
#include "Result.h"
#include "Way.h"

//using namespace __gnu_cxx;          // necessary for hash_map

namespace TSP {

class TSPAlgoSeboeh : public TspAlgoCommon
{
public:
    TSPAlgoSeboeh();

    ~TSPAlgoSeboeh();

    /// Parameterized functions assigned to obj Parameters.
    static std::list<Point*>::iterator findNextPointFnc(Point &startPoint, std::list<Point*> * openPoints, Parameters &para);

    static int decissionTreeChildCountFnc(float &);

    static float decissionTreeWeightFnc(float &);

    /**
     * @brief Only test-suite wants access to it. TODO SB: shift it to private.
     * @param n - degree
     * @param m - repeat
     * @return
     */
    static long ackermann(long n, long m);

protected:

    Way createWay(Point &startPoint, InputCitiesPointerType * openPoints, float wayLength);

    static std::list<Point*>::iterator nearestNeighbour(Point &startPoint, std::list<Point*> * openPoints, Parameters &para);

    static std::list<Point*>::iterator findIter(std::list<Point*> * openPoints, Point &pointToFind);

    static std::list<Point*> * newOpenPointsStatic(std::list<Point*> * openPoints, Point &point);

    /**
     * @brief arctangens returns the arcus tangens between vect(p2 to p1) and vect(p2 to p3)
     * @param p1
     * @param p2
     * @param p3
     * @return
     */
    static double arcTangens(Point &p1, Point &p2, Point &p3);


private:
    static long  ackermannMaximum;
};

}

#endif // TSPALGOSEBOEH_H
