/*
 * Copyright 2002-2010 Guillaume Cottenceau.
 *
 * This software may be freely redistributed under the terms
 * of the X11 license.
 *
 */
/*
 * Edited by Sebastian Bohmer under the licence of LGPL V1. 2014
 * The change is the object oriented programming style.
 */

#ifndef PNGIMAGE_H
#define PNGIMAGE_H

#define PNG_DEBUG 3
#include <lpng/png.h>
#include <stdexcept>
#include <string>

#define PNG_TEXT_KEY_TSPWAY "TSPWay"
#define PNG_TEXT_KEY_DOTS "Dots"

class PngImage
{
public:

    typedef png_byte PngByte;
    typedef png_text PngText;

    struct State { enum StateType { ISNULL=1, ISREADPTR=2, ISWRITEPTR=4 }; };

    PngImage(size_t w, size_t h);

    ~PngImage();

    void read_png_file(std::string file_name);

    void write_png_file(std::string file_name);

    void checkPixelFormat() throw (std::logic_error);

    png_byte* row_pointers_at(size_t y);

    inline png_byte* pixel(size_t x, size_t y) { return &(row_pointers_at(y)[x*4]); }

    /**
     * @brief set_text_chunk sets a text chunk.
     * Don't delete the allocated memory of \a txt.text because it is used while image writing.
     */
    void set_text_chunk(PngText * txtArray);

    PngText * get_text_chunk(char * key, int * num) throw (std::logic_error);

private:
    void freeRowPointers();
    void allocRowPointers();

private:
    int width, height;
    png_byte color_type;
    png_byte bit_depth;

    png_structp png_ptr;
    png_infop info_ptr;
    int number_of_passes;
    png_bytep * row_pointers;

    int m_state;

    png_text * m_textChunk;

};

#endif // PNGIMAGE_H
