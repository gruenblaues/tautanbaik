#include "KnapSackSolver.h"

#include <vector>
#include <string>
#include <iostream>
#include <tuple>
#include <set>
#include <map>
#include <list>
#include <limits>

using namespace TSP;

#include "TamarAlgorithmHelper.h"

void KnapSackSolver::setInput(const InputContainerType &input)
{
    if (input.empty())
        return;
    m_preparedItems.clear();

    InputContainerSortedType inputSorted;
    // necessary, because the algorithm findPos needs the first item with weight 0 and value 0
    inputSorted.insert({-1.0, std::make_tuple(GroupMapperEntry(), -1)});
    // fill elements
    size_t i=0;     // for index, have to be 0, because mapping to parameter input.
    for (const GroupMapperEntry &el : input)
    {
        // HINT: here is the calculation of mean weight - necessary for the KnapSack weight.
        //inputSorted.insert({std::get<0>(el).size()/(std::get<1>(el)+1), std::make_tuple(el, i++)});
        inputSorted.insert({i, std::make_tuple(el, i)});
        i++;
    }

    i = 0;
    for (auto elI = inputSorted.begin();
         elI != inputSorted.end(); ++elI, ++i)
    {
        const auto inputRepres = std::get<0>(elI->second).group;
        m_preparedItems.push_back(std::make_tuple(inputRepres
                                                  , inputRepres.size()
                                                  , std::get<0>(elI->second).froms));
    }
}


KnapSackSolver::IndizesType KnapSackSolver::findBestPack(const int countOfNodes) {
   //dynamic programming approach sacrificing storage space for execution
   //time , creating a table of optimal values for every weight and a
   //second table of sets with the items collected so far in the knapsack
   //the best value is in the bottom right corner of the values table,
   //the set of items in the bottom right corner of the sets' table.
   const int n = m_preparedItems.size();
   m_countOfNodes = countOfNodes;
   const int weightSize = countOfNodes + 1;
   m_calc.resize(n, CalcAreaType::value_type(weightSize));
   for ( int i = 1; i < n; i++ ) {
        int itemweight = _getCosts((m_preparedItems.begin( ) + i));
        for ( int weight = 0; weight < weightSize; weight++ ) {
            if ( weight < itemweight )
            {
                m_calc[ i ][ weight ] = m_calc[ i - 1 ][ weight ];
            } else { // weight >= itemweight
                bool isInsert;
                CalcAreaElement calcEl;
                std::tie(isInsert, calcEl) = _calcAndcompareValue(i, weight, itemweight);
                if ( isInsert )
                {
                    m_calc[ i ][ weight ] = calcEl;
                } else {
                    m_calc[ i ][ weight ] = m_calc[ i - 1 ][ weight ];
                }
            }
        }
    }

    // to get the correct index, compared to the input vector.
    IndizesType output;
    std::list<Node*> countSet;
    for (const GroupIndex &gInd : m_calc[ n - 1][ weightSize - 1 ].indizes)
    {
        if (gInd.index > 0)
        {
            IndizesEntry entry;
            entry.index = gInd.index-1;
            entry.from = gInd.from;
            output.push_back(entry);
#ifndef NDEBUG
            for (const auto &repIndex : std::get<0>(m_preparedItems[gInd.index]))
            {
                countSet.push_back(repIndex);
            }
#endif
        }
    }
#ifndef NDEBUG
    assert(countOfNodes == countSet.size());
#endif

    return output;
}

double KnapSackSolver::_getValue(CalcAreaElement &calcEl)
{
    if (calcEl.fromsOrderedSmall.empty())
        return - std::numeric_limits<double>::max() / m_countOfNodes;

    double value = 0.0;
    for (const auto from : calcEl.fromsOrderedSmall)
    {
        value -= std::get<1>(from.second);
    }
    return calcEl.value = value;
}

std::tuple<KnapSackSolver::GroupIndex, bool> KnapSackSolver::_makeStdGroupIndex(CalcAreaElement &calc, size_t index)
{
    auto froms = _getFromNodes(m_preparedItems.begin()+index);
    GroupIndex gInd;
    gInd.from = froms->front().from;
    gInd.fromsOrigin = froms;
    gInd.index = index;

    calc.indizes.push_back(gInd);

    _addToOrderedFroms(calc, index, froms);
    bool ret = _reorderFroms(calc, froms, index);

    return std::make_tuple(calc.indizes.back(), ret);     // because calc is updated, gInd not.
}

std::tuple<bool, KnapSackSolver::CalcAreaElement> KnapSackSolver::_calcAndcompareValue(size_t index, size_t weight, size_t itemWeight)
{
    // pre-condition
    GroupRepresentationType currentItems = std::get<0>(m_preparedItems[index]);

    // debug
//    for (Node *node : currentItems)
//    {
//        if (node->point.x() == 130 && node->point.y() == 150)
//            int i=5;
//    }

    // calculate value initial
    if (currentItems.size() == 1)
    {
        CalcAreaElement calc = m_calc[index - 1][weight];
        GroupIndex gInd;
        bool isSeperateFromEdge = false;
        std::tie(gInd, isSeperateFromEdge) = _makeStdGroupIndex(calc, index);
        double valueBest;
        if (!isSeperateFromEdge)
        {
            valueBest = - std::numeric_limits<double>::max() / m_countOfNodes;
        }
        else
        {
            valueBest = _getValue(calc);
        }
        bool isBestOk = -std::numeric_limits<double>::max() == valueBest || valueBest > - std::numeric_limits<double>::max()/m_calc.size();

        // HINT: here at size 1 it has to be added every time, because this is the basic and guaranty, that the matrix will work with all nodes.
        //        if (!isBestOk)
        //            return std::make_tuple(false, CalcAreaElement());
        calc.value = valueBest;
        return std::make_tuple(true, calc);
    }

    // copy, because the old values in tree should further exists, due to using again in another context in future.
    CalcAreaElement calcBest;
    size_t calcBestAreaInd;
    std::tie(calcBest, calcBestAreaInd) = _findNewestCoveredGroup(index, index, weight, currentItems);
    CalcAreaElement calcPast;
    size_t calcPastAreaInd;
    std::tie(calcPast, calcPastAreaInd) = _findNewestCoveredGroup(index, index, weight - itemWeight, currentItems);

    // new value, calculate current common values for best and past.
    double groupValueBestOld, groupValuePastOld;
    groupValueBestOld = m_calc[index - 1][weight].value;
    groupValuePastOld = m_calc[index - 1][weight - itemWeight].value;

    if (calcBest.indizes.empty() && calcPast.indizes.empty())
    {
        if (groupValueBestOld >= groupValuePastOld)
        {
            return std::make_tuple(false, CalcAreaElement());
        }
        else
        {
            return std::make_tuple(true, m_calc[index - 1][weight - itemWeight]);
        }
    }


    // because we need to insert new elements.
    // HINT: It is assumed, that all kind of pairs from starting point are inserted.
    // Because all kind of pairs are inserted, the best can be found.

    double badVal = - std::numeric_limits<double>::max()/m_countOfNodes;
    bool isBestOk = -std::numeric_limits<double>::max() == calcBest.value || calcBest.value > badVal;
    double groupValueCurrentBest = badVal;
    if (isBestOk)
        groupValueCurrentBest = _getValue(calcBest);

    bool isPastOk = -std::numeric_limits<double>::max() == calcPast.value || calcPast.value > badVal;
    double groupValueCurrentPast = badVal;
    if (isPastOk)
        groupValueCurrentPast = _getValue(calcPast);

    std::map<double, size_t> argmax;
    argmax.insert({groupValueBestOld, 1});
    argmax.insert({groupValuePastOld, 2});
    if (isBestOk)
        argmax.insert({groupValueCurrentBest, 3});
    if (isPastOk)
        argmax.insert({groupValueCurrentPast, 4});

    size_t bestArg = (--argmax.end())->second;      // end, because all negative results.

    // decision
    switch (bestArg)
    {
    case 1:
        return std::make_tuple(false, CalcAreaElement());
    case 2:
        return std::make_tuple(true, m_calc[index - 1][weight - itemWeight]);
    case 3:
        return std::make_tuple(true, calcBest);
    case 4:
        return std::make_tuple(true, calcPast);
    }
}

std::tuple<GroupRepresentationType,bool, bool> KnapSackSolver::_substract(const GroupRepresentationType & currentItems, const GroupRepresentationType & disjointItemsArg)
{
    GroupRepresentationType itemsNotSubstracted;
    GroupRepresentationType disjointItems = disjointItemsArg;
    for (const auto curEl : currentItems)
    {
        if (disjointItems.find(curEl) == disjointItems.end())
        {
            itemsNotSubstracted.insert(curEl);
        } else {
            disjointItems.erase(curEl);
        }
    }
    return std::make_tuple(itemsNotSubstracted, disjointItems.empty(), disjointItems.size() == disjointItemsArg.size());
}

std::tuple<KnapSackSolver::CalcAreaElement, size_t> KnapSackSolver::_findNewestCoveredGroup(size_t index, size_t newIndex, size_t weight, GroupRepresentationType &currentItems)
{
    size_t i = index-1;
    CalcAreaElement calcNew;
    if (m_calc[i][weight].indizes.size() == 1
        && std::get<0>(m_preparedItems[m_calc[i][weight].indizes.begin()->index]).size() == 1)
        return std::make_tuple(CalcAreaElement(), i);
    GroupRepresentationType currentSubstractGroupDisjoint = currentItems;
    bool isDisjoint, isUntouched;
    for (const auto &gInd : m_calc[i][weight].indizes)
    {
        std::tie(currentSubstractGroupDisjoint, isDisjoint, isUntouched) = _substract(currentSubstractGroupDisjoint, std::get<0>(m_preparedItems[gInd.index]));
        if (!isDisjoint && !isUntouched)
            // restart the in past, because past have to contain all currentItems.
            return _findNewestCoveredGroup(i, newIndex, weight, currentItems);
        // HINT: given up, because only need of not disjoint sets. See test54.
        //        if (currentSubstractGroupDisjoint.empty())
        //            return m_calc[i][weight];
        if (isUntouched)
            calcNew.indizes.push_back(gInd);
    }
    for (const auto &from : m_calc[i][weight].fromsOrdered)
    {
        auto giI = std::find_if(calcNew.indizes.begin(), calcNew.indizes.end(),
                                [from](const GroupIndex &val)
        {
                return from.groupIndex == val.index;
        });
        if (giI != calcNew.indizes.end())
            calcNew.fromsOrdered.push_back(from);
    }
    // add element
    GroupFromNodeTypePointer currentFroms = _getFromNodes(m_preparedItems.begin() + newIndex);
    GroupIndex gInd;
    gInd.from = currentFroms->front().from;
    gInd.fromsOrigin = currentFroms;
    gInd.index = newIndex;
    calcNew.indizes.push_back(gInd);
    // check count of elements
    size_t countEl = 0;
    for (const auto &groupIndex : calcNew.indizes)
    {
        countEl += _getRepresentation(m_preparedItems.begin()+groupIndex.index).size();
    }
    assert(countEl <=  m_countOfNodes);
    if (countEl < m_countOfNodes)
        return std::make_tuple(CalcAreaElement(), i);       // because, the result will not be relevant, the new group goes not together with the existing ones.
    // and this shall be ignored as next. --> constructor contains negative max value.

    // add new element
    _addToOrderedFroms(calcNew, newIndex, currentFroms);
    if (!_reorderFroms(calcNew, currentFroms, newIndex))
        calcNew.value = - std::numeric_limits<double>::max()/m_countOfNodes;
    return std::make_tuple(calcNew, i);
}

void KnapSackSolver::_addToOrderedFroms(CalcAreaElement &calcy, size_t currentIndex, GroupFromNodeTypePointer currentFroms)
{
    for (GroupFromNodeType::iterator fromI = currentFroms->begin();
         fromI != currentFroms->end(); ++fromI)
    {
        GroupFromOrderContainer::value_type value;
        value.dist = fromI->dist;
        value.from = fromI->from;
        value.fromI = fromI;
        value.groupIndex = currentIndex;
        insert_sort(calcy.fromsOrdered, value,
                    [](const GroupFromOrderContainer::value_type &left, const GroupFromOrderContainer::value_type &right)
        {
            return left.dist <= right.dist;
        });
    }
}

bool KnapSackSolver::_reorderFroms(CalcAreaElement &calcy, GroupFromNodeTypePointer currentFroms, size_t currentIndex)
{
    if (calcy.fromsOrdered.empty())
        return false;
    // repeat search of froms
    calcy.fromsOrderedSmall.clear();
    std::map<Node*, bool> isNodeUsedAlready;
    for (const GroupFromOrderContainer::value_type &fromOrdered : calcy.fromsOrdered)
    {
        if (calcy.fromsOrderedSmall.size() == calcy.indizes.size())
            break;
        if (calcy.fromsOrderedSmall.find(fromOrdered.groupIndex) == calcy.fromsOrderedSmall.end())
        {
            if (isNodeUsedAlready.find(fromOrdered.from) == isNodeUsedAlready.end())
            {
                calcy.fromsOrderedSmall.insert({fromOrdered.groupIndex, std::make_tuple(fromOrdered.from, fromOrdered.dist)});
                isNodeUsedAlready.insert({fromOrdered.from, true});
            }
        }
    }

    bool ret;
    if (calcy.fromsOrderedSmall.size() != calcy.indizes.size())
    {       // because later it is necessary to weight this wrong state
            // of doubled used hull-edge as bad.
        ret = false;
    }
    else
    {
        ret = true;
    }

    // update groupIndex
    for (GroupIndex &group : calcy.indizes)
    {
        auto iter = calcy.fromsOrderedSmall.find(group.index);
        if (iter != calcy.fromsOrderedSmall.end())
        {
            group.from = std::get<0>(iter->second);
        }
        else
        {
            group.from = group.fromsOrigin->front().from;
        }
    }
    return ret;
}

std::shared_ptr<GroupMapBuilder::TamarGroupMapperType> GroupMapBuilder::createMapper(std::list<oko::Node*> fromNodes, Way *innerPointWay)
{
    /// for size of n, the group size
    // TODO [seboeh] remove on refactoring - guaranty that it will be
    size_t pointsCount = innerPointWay->recalcSize();
    if (pointsCount == 0)
        return nullptr;

    std::list<Edge> edges;
    for (const Node* el : fromNodes)
    {
        Edge edge;
        edge.fromNode = const_cast<Node*>(el);
        edge.toNode = const_cast<Node*>(Way::nextChild(el));
        edges.push_back(edge);
    }

    for (size_t n=1; n <= pointsCount; ++n)
    {
        /// set initial iterators
        Sample s;
        Way::iterator innerLastI = innerPointWay->begin();
        for (size_t i=0; !innerLastI.end() && i < n; ++i)
            s.innerNodes.push_back(innerLastI.nextChild());

        /// walk through nodes
        Node *innerPoint = nullptr;
        Node *startPoint = s.innerNodes.back();
        do {
            /// walk through hull edges
            s.froms = std::make_shared<GroupFromNodeType>();
            for (const Edge &edge : edges)
            {
                std::list<ClosestEdges> ceList;
                for (const Node *node : s.innerNodes)
                {
                    ClosestEdges ce(const_cast<Node*>(node));
                    TSPHelper::findBestSingleCeAndAdd(edge.toNode, edge.fromNode, ce, ceList);
                }
                double dist = TSPHelper::correctDistHelper(ceList);
                GroupFromNodeType::value_type value;
                value.from = edge.fromNode;
                value.dist = dist;
                insert_sort(*s.froms, value,
                            [](const GroupFromNodeType::value_type &left, const GroupFromNodeType::value_type &right)
                {
                    return left.dist <= right.dist;
                });
            }
            /// insert
            m_samples.push_back(s);

            /// iterate
            innerPoint = innerLastI.nextChild();
            s.innerNodes.pop_front();
            s.innerNodes.push_back(innerPoint);
        } while (innerPoint != startPoint && s.innerNodes.size() < pointsCount);

    }

    m_mapper = std::make_shared<TamarGroupMapperType>();
    for (Sample &s : m_samples)
    {
        m_mapper->insert(s.innerNodes, &s);
    }

    return m_mapper;
}

