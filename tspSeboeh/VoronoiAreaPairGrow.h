#pragma once

#include <memory>
#include <tuple>

#include "TspAlgoCommon.h"
#include "dataStructures/OlavList.h"
#include "dataStructures/commonstructures.h"
#include "bspTreeVersion/BspTreeSarah.h"

namespace TSP {

class VoronoiAreaPairGrow : public TspAlgoCommon
{
public:
    using Node = comdat::Node;
    using NodeL = std::list<std::shared_ptr<comdat::Node>>;
    using PointL = std::list<TSP::Point>;
    using Way = oko::OlavList;

    struct no_op_delete
    {
        void operator()(void*) { }
    };

    enum class MergeCallState {
        leftRight, topDown
    };
    enum class DirectAccessState {
        isDirectAccess, isNoDirectAccess
    };
    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };
    typedef oko::ClosestEdges ClosestEdges;
    typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;
    struct EdgeIter {
        EdgeIter(Way::iterator &aNodeIter, Way::iterator &bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
        Way::iterator aNodeI;
        Way::iterator bNodeI;
    };
    struct Cluster;
    struct NeighbourRef {
        Cluster *neighClu = nullptr;
    };
    struct Cluster {
        PointL verts;
        std::shared_ptr<Node> point;
        double area = 0.0;
        std::vector<std::shared_ptr<Node>> closestN;
        std::list<Node*> closestTestedPointsSorted;
        std::list<Cluster*> closestSortedNeighbours;
        std::map<double, std::tuple<Node*,Node*>> neighbourEdges;
        int state = 0;      // 1 = isAligned
        int iterationC = 0;
        size_t growPosition = 0;
        std::map<double, std::map<Node*, NeighbourRef>> neighbourRefs;       // first index = dist for common edge pairEdge, second index = from-node
    };
    typedef std::list<std::shared_ptr<Cluster>> ClusterGrow;

    struct PartnerEl
    {
        Node* from = nullptr;
        Node* to = nullptr;
        Cluster* partner1 = nullptr;
        Cluster* partner2 = nullptr;
        double commonDist = std::numeric_limits<double>::max();
    };
    using PartnerM = std::map<Node*, std::map<Node*, PartnerEl>>;
    PartnerM m_partners;

    PartnerM m_commonDist;      // for fnc _commonDistCalc()


    size_t m_tspSize = 0;

    /** globals **/
    static size_t tspSize;
    static double tspWidth;
    static double tspHeight;

    /** Ctor **/
    VoronoiAreaPairGrow();

protected:
    /** Impl **/
    void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic);

private:
    NodeL createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic);
    std::tuple<VoronoiAreaPairGrow::NodeL, VoronoiAreaPairGrow::Way *> determineHull(NodeL &input);
    ClusterGrow determineClusters(NodeL &input, NodeL &hull);
    void alignToHull(std::map<double, Cluster *> &clusters, NodeL &hull);
    VoronoiAreaPairGrow::NodeL determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way);

    void _triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI);
    VoronoiAreaPairGrow::Way *_newConqueredWay(bsp::BspNode *node);
    bsp::BspTreeSarah _createBSPTree(NodeL &input);
    void _mergeHull(Way *&way1, const TSP::Point &pWay1Center, Way *&way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter);
    bool _isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);
    DirectAccessState _isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI);
    CrossedState _isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint = nullptr);
    bool _isNormalVectorLeft(const Point &from, const Point &to, const Point &insert);
    double _calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3);
    double _area(const PointL &polygon);
    bool _isCovered(Way *way, Way::iterator &w1I, Way::iterator &fixI);
    void _createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI);
    bool _createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI);
    void _flipWay(Way *way, TSP::Point pCenter);
    bool _isClockwise(TSP::Point pCenter, Way *way);
    VoronoiAreaPairGrow::Node *_nextNode(Node *curr, Node * &prev);
    bool _updateHullOrUpdateSimple(std::shared_ptr<Node> hullNodeA, std::shared_ptr<Node> updateNode, NodeL &hull);
    bool _updateAllowed(std::shared_ptr<Node> closest, std::shared_ptr<Node> node, bool isTestOnClique = true);
    void _updateNodes(std::map<double, Cluster *> &clustersSorted, NodeL &hull);
    void _updateNodes2(std::map<double, Cluster *> &clustersSorted, NodeL &hull);
    double _triangleAltitudeOnC(Point A, Point B, Point C);
    void _insertNewNodeToEdge(Way *way1, Node *fromNode, Node *insertNode);
    bool _isRightSide(const Point &p1, const Point &p2);
    double _calcDegree(Node *node, Node *nodeTested);
    void _runEvolutionaryGrow(std::map<double, Cluster *> &clustersM, Way *way);
    void _insertAsNeighbour(Node *node, Cluster *clu);
    double _correlation(Point a, Point b);
    bool _checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);
    void _collectPairsEdges(std::map<double, Cluster *> &clustersM);
    void _collectPairsEdgesCalc(std::map<double, std::tuple<Node *, Node *> > &neighbourEdges, Cluster *clu, Cluster *neigh);
    double _getBestAlignEdge(Node *partnerN, Node *cluN, Cluster *clu);
    void _alignPartners(Way *way, Node *from, Node *to);
    void _addPartnersToHull(Way *way);
};

}
