#pragma once

#include <memory>
#include <tuple>

#include "TspAlgoCommon.h"
#include "dataStructures/OlavList.h"
#include "dataStructures/commonstructures.h"
#include "bspTreeVersion/BspTreeSarah.h"

namespace TSP {

class VoronoiSort : public TspAlgoCommon
{
public:
    using Node = comdat::Node;
    using NodeL = std::list<std::shared_ptr<comdat::Node>>;
    using PointL = std::list<TSP::Point>;
    using Way = oko::OlavList;
    enum class MergeCallState {
        leftRight, topDown
    };
    enum class DirectAccessState {
        isDirectAccess, isNoDirectAccess
    };
    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };
    typedef oko::ClosestEdges ClosestEdges;
    typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;
    struct EdgeIter {
        EdgeIter(Way::iterator &aNodeIter, Way::iterator &bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
        Way::iterator aNodeI;
        Way::iterator bNodeI;
    };
    struct Cluster {
        PointL verts;
        std::shared_ptr<Node> point;
        double area = 0.0;
        std::vector<std::shared_ptr<Node>> closestN;
        std::list<Node*> closestTestedPointsSorted;
        std::list<Cluster*> closestSortedNeighbours;
        std::map<double, std::tuple<Node*,Node*>> neighbourEdges;
        int state = 0;      // 1 = isAligned
        int iterationC = 0;
    };
    typedef std::map<double, std::map<double, Cluster>> ClusterM;       ///< x-coor, y-coor -> Cluster.point

    size_t m_tspSize = 0;

    /** Ctor **/
    VoronoiSort();

protected:
    /** Impl **/
    void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic);

private:
    NodeL createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic);
    std::tuple<VoronoiSort::NodeL, VoronoiSort::Way *> determineHull(NodeL &input);
    ClusterM determineClusters(NodeL &input, NodeL &hull);
    void alignToHull(std::map<double, Cluster *> &clusters, NodeL &hull);
    VoronoiSort::NodeL determineWay(ClusterM &clusters, NodeL &hull);
    VoronoiSort::NodeL determineWay2(ClusterM &clusters, NodeL &hull, Way *way);
    VoronoiSort::NodeL determineWay3(ClusterM &clustersA, NodeL &hull, Way *way);

    void _triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI);
    VoronoiSort::Way *_newConqueredWay(bsp::BspNode *node);
    bsp::BspTreeSarah _createBSPTree(NodeL &input);
    void _mergeHull(Way *&way1, const TSP::Point &pWay1Center, Way *&way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter);
    bool _isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);
    DirectAccessState _isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI);
    CrossedState _isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint = nullptr);
    bool _isNormalVectorLeft(const Point &from, const Point &to, const Point &insert);
    double _calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3);
    double _area(const PointL &polygon);
    bool _isCovered(Way *way, Way::iterator &w1I, Way::iterator &fixI);
    void _createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI);
    bool _createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI);
    void _flipWay(Way *way, TSP::Point pCenter);
    bool _isClockwise(TSP::Point pCenter, Way *way);
    VoronoiSort::Node *_nextNode(Node *curr, Node * &prev);
    bool _updateHullOrUpdateSimple(std::shared_ptr<Node> hullNodeA, std::shared_ptr<Node> updateNode, NodeL &hull);
    bool _updateAllowed(std::shared_ptr<Node> closest, std::shared_ptr<Node> node, bool isTestOnClique = true);
    void _updateNodes(std::map<double, Cluster *> &clustersSorted, NodeL &hull);
    void _updateNodes2(std::map<double, Cluster *> &clustersSorted, NodeL &hull);
    double _triangleAltitudeOnC(Point A, Point B, Point C);
    void _insertNewNodeToEdge(Way *way1, Node *fromNode, Node *insertNode);
    bool _isRightSide(const Point &p1, const Point &p2);
    double _calcDegree(Node *node, Node *nodeTested);
    void _runEvolutionaryGrow(std::map<double, Cluster *> clustersM, Way *way);
};

}
