#include "MinMaxClusterAlignment.h"

#include <algorithm>
#include <tuple>
#include <math.h>
#ifndef M_PI
#define M_PI       3.14159265358979323846   // pi
#endif

#include "bspTreeVersion/BspTreeSarah.h"
#include "Way.h"

#define SMALL_DOUBLE_STEP_MAX 0.0001
#define SMALL_DOUBLE_STEP_MIN 0.000001

using TSPWay = TSP::Way;

using namespace TSP;
using namespace std;
using namespace TSPHelper;

size_t MinMaxClusterAlignment::tspSize = 0;
double MinMaxClusterAlignment::tspWidth = 0;
double MinMaxClusterAlignment::tspHeight = 0;

MinMaxClusterAlignment::MinMaxClusterAlignment()
    : TspAlgoCommon()
{

}

MinMaxClusterAlignment::NodeL MinMaxClusterAlignment::createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL ret;

    double tspMaxWidth = 0;
    double tspMaxHeight = 0;
    double tspMinWidth = numeric_limits<double>::max();
    double tspMinHeight = numeric_limits<double>::max();
    for (const auto &point : *openPointsBasic)
    {
        ret.push_back(std::make_shared<Node>(*point));
        if (tspMaxWidth < point->x())
            tspMaxWidth = point->x();
        if (tspMinWidth > point->x())
            tspMinWidth = point->x();
        if (tspMaxHeight < point->y())
            tspMaxHeight = point->y();
        if (tspMinHeight > point->y())
            tspMinHeight = point->y();
    }
    tspWidth = tspMaxWidth - tspMinWidth;
    tspHeight = tspMaxHeight - tspMinHeight;

    return ret;
}

bsp::BspTreeSarah MinMaxClusterAlignment::_createBSPTree(NodeL &input)
{
    InputCitiesPointerType cities;
    for (const auto &node : input)
        cities.push_back(new TSP::Point(node->point));
    // HINT: above. better to use the nodes from input than new points.
    // because later search of hull-node necessary - see mark MARK_hullNodeSearch

    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cities);

    return bspTree;
}

std::tuple<MinMaxClusterAlignment::NodeL, MinMaxClusterAlignment::Way*> MinMaxClusterAlignment::determineHull(NodeL &input)
{
    NodeL ret;
    // * split input into quad-BSP-tree
    // * on returning from recursion merge to cyclic way (convex hull).

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = _createBSPTree(input);

    /// 2. create points order - merge
    //////////////////////////
    Way * way = nullptr;
    auto root = bspTree.root();
    if (root != nullptr) {
        way = _newConqueredWay(root);
        if (way != nullptr)
        {
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (Way::iterator valI=way->begin();
                 !valI.end(); )
            {
                Node *node = valI.nextChild();
                node->isHull = true;
                ret.push_back(std::shared_ptr<Node>(node));
            }
// TODO [seboeh] check if possible
//            way->clear();
//            delete way;
        }
    }

    // update input with new nodes from way
    Way::iterator wayI = way->begin();
    while (!wayI.end())
    {
        Node *node = wayI.nextChild();
        auto inputI = find_if(input.begin(), input.end(),
                              [node] (const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        assert(inputI != input.end());
        *inputI = shared_ptr<Node>(node, no_op_delete());
    }

    // * create resulting list
    return make_tuple(ret, way);
}


bool MinMaxClusterAlignment::_isClockwise(TSP::Point pCenter, Way *way)
{
    Way::iterator pI = way->begin();
    Way::iterator pbackupI = pI;
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0 && ((dot/90) != m_para.distanceFnc(*pbackupI,*pI));
}

void MinMaxClusterAlignment::_flipWay(Way *way, TSP::Point pCenter)
{
    if(!_isClockwise(pCenter, way))
        way->flipDirection();
}

Point MinMaxClusterAlignment::_correctNodeCenter(Way* wayBiggerVals, Way* waySmallerVals, bsp::BspNode *node, MergeCallState state)
{
    Point::value_type valA = 0;
    auto wI = waySmallerVals->begin();
    while (!wI.end())
    {
        valA = max(valA, state == MergeCallState::leftRight? (*wI).x() : (*wI).y());
        ++wI;
    }
    float valB = numeric_limits<double>::max();
    wI = wayBiggerVals->begin();
    while (!wI.end())
    {
        valB = min(valB, state == MergeCallState::leftRight? (*wI).x() : (*wI).y());
        ++wI;
    }
    Point mainCenter = node->m_center;
    if (state == MergeCallState::leftRight)
        mainCenter.set(mainCenter.x() + ((valB - valA)/2.0), mainCenter.y());
    else
        mainCenter.set(mainCenter.x(), mainCenter.y() + ((valB - valA)/2.0));

    return mainCenter;
}

MinMaxClusterAlignment::Way *MinMaxClusterAlignment::_newConqueredWay(bsp::BspNode *node)
{
    if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        Way *way = new Way();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
//        way->setOrigin(node);
        return way;
    }
    Way * wayUpper;
    Way * wayLower;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        Way * wayUpperLeft = _newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            Way * wayUpperRight = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x()/2.0, pUpperCenter.y()/2.0);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            // see argument below Point mainCenter = _correctNodeCenter(wayUpperRight, wayUpperLeft, node, MergeCallState::leftRight);
            _mergeHull(wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        } else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    } else {
        if (node->m_upperRight) {
            wayUpper = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        } else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        Way * wayLowerLeft = _newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            Way * wayLowerRight = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x()/2.0, pLowerCenter.y()/2.0);

            // HINT: it is faster not to do this: Point mainCenter = _correctNodeCenter(wayLowerRight, wayLowerLeft, node, MergeCallState::leftRight);
            // instead using node->m_center
            _mergeHull(wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        } else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    } else {
        if (node->m_lowerRight) {
            wayLower = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        } else {
            wayLower = nullptr;
        }
    }

    //incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {
        // HINT: test37 needs master center node->m_center.
        // HINT: test2 needs flipWay
        _flipWay(wayLower, pLowerCenter);
        _flipWay(wayUpper, pUpperCenter);
        // Point mainCenter = _correctNodeCenter(wayLower, wayUpper, node, MergeCallState::topDown);
        _mergeHull(wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    } else if (wayUpper) {
        return wayUpper;
    } else if (wayLower) {
        return wayLower;
    } else {
        return nullptr;
    }
}

float MinMaxClusterAlignment::sign (const Point &p1, const Point &p2, const Point &p3)
{
    return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
}

bool MinMaxClusterAlignment::_isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    if (p1 == inside || p2 == inside || p3 == inside)
        return false;

    // [https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle]
    Point pt = inside;
    Point v1 = p1;
    Point v2 = p2;
    Point v3 = p3;
    float d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(pt, v1, v2);
    d2 = sign(pt, v2, v3);
    d3 = sign(pt, v3, v1);

    has_neg = (d1 <= 0) || (d2 <= 0) || (d3 <= 0);      // also =0 --> point on line --> not inside (false)
    has_pos = (d1 >= 0) || (d2 >= 0) || (d3 >= 0);      // =0 --> on one line of triangle

    bool isInsideV1 = !(has_neg && has_pos);


    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    // barycentric coordinates.
    bool isInsideV2 = false;
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        isInsideV2 = false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        isInsideV2 = false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        isInsideV2 = false;

    // see test12 - V2 was wrong assert(isInsideV1 == isInsideV2);
    return isInsideV1;
}

void MinMaxClusterAlignment::_triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = _isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = _isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
                && pb == *lpbI
                && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
                && pb == *hpbI
                && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

MinMaxClusterAlignment::CrossedState MinMaxClusterAlignment::_isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint)
{
    /// Crossed between a and b?
    double x1 = pa1I.x();
    double y1 = pa1I.y();
    double x2 = pa2I.x();
    double y2 = pa2I.y();
    double x3 = pb1I.x();
    double y3 = pb1I.y();
    double x4 = pb2I.x();
    double y4 = pb2I.y();

    /// HINT: [http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection]
    double denominator = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (denominator == 0) {
        // lines are parallel not guarantied to lay on each other.
        // We want to integrate the considered node as inner node -> so it should not be covered -> parallel
        return CrossedState::isParallel;
    }
    double pX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denominator;
    double pY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denominator;
    if (crossPoint != nullptr)
        *crossPoint = TSP::Point(pX,pY);

    if ((std::max)(std::min(x1,x2),std::min(x3,x4)) <= pX && pX <= std::min((std::max)(x1,x2),(std::max)(x3,x4))
            && (std::max)(std::min(y1,y2),std::min(y3,y4)) <= pY && pY <= std::min((std::max)(y1,y2),(std::max)(y3,y4)))
    {
        if ((pX == x1 && pY == y1)          // FIX: Could be a speed-up, if we check if this crossing on exact one outer point could happen with the input.
            || (pX == x2 && pY == y2)
            || (pX == x3 && pY == y3)
            || (pX == x4 && pY == y4))
        {
            return CrossedState::isOnPoint;
        } else {
            return CrossedState::isCrossed;
        }
    } else {
        return CrossedState::isOutOfRange;
    }
}

MinMaxClusterAlignment::DirectAccessState MinMaxClusterAlignment::_isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI)
{
    Way::iterator prevAI = pAI; --prevAI;
    Way::iterator nextAI = pAI; ++nextAI;
    Way::iterator prevBI = pBI; --prevBI;
    Way::iterator nextBI = pBI; ++nextBI;
    CrossedState state = _isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = _isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = _isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = _isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool MinMaxClusterAlignment::_isNormalVectorLeft(const Point &from, const Point &to, const Point &insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x())/2
                    , (from.y() + to.y())/2);

    Point normalVectLeft(meanPoint.x() - straight.y()
                         , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
                          , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}


bool MinMaxClusterAlignment::_isCovered(Way * way, Way::iterator &w1I, Way::iterator &fixI)
{
    Way::iterator wI = way->begin();
    Way::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = _isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed
                || state == CrossedState::isCovered
                || state == CrossedState::isOnPoint)
            return true;
    }
    return false;
}

bool MinMaxClusterAlignment::_createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI)
{
    if (_isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        Way::iterator w1NewI = way1->push_back(&way2I);
        Way::iterator w1NNewI = way1->push_back(&way2NI);

        Node *nodePoint = (&w1Inside);
        if (dist1 < dist2 && dist1 < dist3)
        {
            nodePoint->prev = &w1Alt;
            nodePoint->next = (&w1Alt)->next;
            way1->insertChild(w1Alt, nodePoint);      // TODO [seboeh] is new really necessary?
        } else if (dist2 < dist1 && dist2 < dist3)
        {
            nodePoint->prev = &w1NNewI;
            nodePoint->next = (&w1NNewI)->next;
            way1->insertChild(w1NNewI, nodePoint);
        } else {
            nodePoint->prev = &w1NewI;
            nodePoint->next = (&w1NewI)->next;
            way1->insertChild(w1NewI, nodePoint);
        }
        return true;
    }
    return false;
}

void MinMaxClusterAlignment::_createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    Way::iterator w1I = way1->begin();
    Way::iterator w1NI = w1I; ++w1NI;
    Way::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    Node *nodePoint = (&insideI);
    if (dist1 < dist2 && dist1 < dist3)
    {
        nodePoint->prev = &w1I;
        nodePoint->next = (&w1I)->next;
        way1->insertChild(w1I, nodePoint);
    } else if (dist2 < dist1 && dist2 < dist3)
    {
        nodePoint->prev = &w1NI;
        nodePoint->next = (&w1NI)->next;
        way1->insertChild(w1NI, nodePoint);
    } else {
        nodePoint->prev = &w1NNI;
        nodePoint->next = (&w1NNI)->next;
        way1->insertChild(w1NNI, nodePoint);
    }
}

void MinMaxClusterAlignment::_mergeHull(Way * &way1, const TSP::Point &pWay1Center, Way * &way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        _flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        Way::iterator w1I = way1->begin();
        Way::iterator fixI = way2->begin();
        if (_isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            _flipWay(way1, pCenter);
            return;
        } else {
            ++w1I;
            if (_isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                _flipWay(way1, pCenter);
                return;
            } else {
                ++w1I;
                if (_isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    _flipWay(way1, pCenter);
                    return;
                } else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    Way::iterator w1NI = w1I; ++w1NI;
                    Way::iterator w1NNI = w1NI; ++w1NNI;
                    if (_isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        _createTriangle3x1(way1, w1NNI, fixI);
                    } else if (_isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        _createTriangle3x1(way1, w1NI, fixI);
                    } else {
                        _createTriangle3x1(way1, w1I, fixI);
                    }
                    _flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        Way::iterator way1I = way1->begin();
        Way::iterator way2I = way2->begin();
        Way::iterator way1NI = way1I;
        ++way1NI;
        Way::iterator way2NI = way2I;
        ++way2NI;
        Way::iterator way1Nearest;
        Way::iterator way2Nearest;
        double maxDist=std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        Way::iterator way1Next = way1Nearest; ++way1Next;
        Way::iterator way2Next = way2Nearest; ++way2Next;
        Point crossedPoint;
        auto crossedVal = _isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next, &crossedPoint);
        if (crossedVal == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            _flipWay(way1, pCenter);
            return;
        } else if (crossedVal == CrossedState::isOnPoint)
        {       // triangle with inside point exactly on one triangle-side
            /// - determine way1 or 2 = wayX, contains crossedPoint
            auto wayI = way1->begin();
            Way *wayX = nullptr;
            Way *wayY = nullptr;
            if (*wayI == crossedPoint || *(++wayI) == crossedPoint)
            {
                wayX = way1;
                wayY = way2;
            }
            else
            {
                wayI = way2->begin();
                if (*wayI == crossedPoint || *(++wayI) == crossedPoint)
                {
                    wayX = way2;
                    wayY = way1;
                }
            }
            assert( wayX != nullptr && wayY != nullptr);
            auto wayCrossedI = wayI;
            auto wayNotCrossedI = ++wayI;
            /// - determine p_i from wayY is on one line with crossedPoint and other point (=xa) of wayX
            wayI = wayY->begin();
            auto wayNI = wayI; ++wayNI;
            Point meanP = (*wayI + *wayNI)/2.0;
            Way::iterator wayPI;
            if (_isCrossed(*wayNotCrossedI, *wayI, *wayCrossedI, meanP) == CrossedState::isOnPoint)
            {       // pi = wayI
                wayPI = wayI;
            } else
            {       // pi = ++wayI
                wayPI = ++wayI;
            }
            auto wayPNI = wayPI; ++wayPNI;
            /// - insert wayY before xa and after crossedPoint.
            (&wayNotCrossedI)->next = &wayCrossedI;
            (&wayNotCrossedI)->prev = &wayPNI;
            (&wayPNI)->next = &wayNotCrossedI;
            (&wayPNI)->prev = &wayPI;
            (&wayPI)->next = &wayPNI;
            (&wayPI)->prev = &wayCrossedI;
            (&wayCrossedI)->next = &wayPI;
            (&wayCrossedI)->prev = &wayNotCrossedI;

            if (way1 != wayX)
                swap(way1, way2);
            _flipWay(way1, pCenter);
            return;
        }
        else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (_createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            } else  if (_createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI ))
            {
            } else if (_createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else if (_createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            _flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2

    /// beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (Way::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (Way::iterator wBI=way2->begin();
             !wBI.end(); ++wBI)
        {
            if ((*wAI).x() == 470)
                int i=5;
            if ((*wBI).x() == 470)
                int i=5;

            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (_isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    Way::iterator haI, hbI, laI, lbI;

    Point realCenter = (pWay1Center + pWay2Center)/2.0;

    smallestDist /= 2;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    CrossedState oldState = CrossedState::isOutOfRange;
    Point oldCrossedPoint;
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = _isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x())-10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x())+10, pCenter.y()), &crossedPoint);
        else
            state = _isCrossed(pa, pb, TSP::Point(pCenter.x(),std::min(pa.y(), pb.y())-10), TSP::Point(pCenter.x(),(std::max)(pa.y(), pb.y())+10), &crossedPoint);

        //bool isLowNotHigh = _isNormalVectorLeft(pb, pa, realCenter);

        double value;

        if (pa.x() == 270)
            if (pb.x() == 550)
                int i=5;
        if (pb.x() == 270)
            int i=5;

        bool isGoodLow = false;
        bool isGoodHigh = false;
        if (state == CrossedState::isOnPoint)
        {       // see test82, test76
            // see test89, test64

            // test if all points are on one side.
            int countSides = 0;
            Point vect = (pb - pa) / m_para.distanceFnc(pa, pb);
            double l = (max(tspWidth, tspHeight)/2);
            Point startP = pa - vect * l;
            Point endP = pb + vect * l;

            for (Way::iterator wAI=way1->begin();
                 !wAI.end(); ++wAI)         // iterate over convex hull
            {
                try {
                    if (_isLeftSideExEqual(startP, endP, *wAI))
                    {
                        if (countSides == 0)
                        {
                            countSides = 1;
                        }
                        else if (countSides == 2)
                        {
                            countSides = 3;
                            break;
                        }
                    } else {
                        if (countSides == 0)
                        {
                            countSides = 2;
                        }
                        else if (countSides == 1)
                        {
                            countSides = 3;
                            break;
                        }
                    }
                } catch (const exception &)
                {
                    // ignore, all points on the line are not disrupting the tangent
                }
            }
            if (countSides != 3)
            {
                for (Way::iterator wBI=way2->begin();
                     !wBI.end(); ++wBI)
                {
                    try {
                        if (_isLeftSideExEqual(startP, endP, *wBI))
                        {
                            if (countSides == 2)
                            {
                                countSides = 3;
                                break;
                            }
                        } else {
                            if (countSides == 1)
                            {
                                countSides = 3;
                                break;
                            }
                        }
                    } catch (const exception &)
                    {
                        // ignore, because tangente still valid.
                    }
                }
            }
            if (countSides != 3)
            {
                state = CrossedState::isCrossed;
            }
        }

        if (state == CrossedState::isCrossed)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();

            if ((callState == MergeCallState::topDown
                    && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGoodLow = true;
            }
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                    && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGoodHigh = true;
            }

            // running both if-s parallel, because to guaranty below the h.. and l.. valid
            if (isGoodLow)
            {
                laI = edgeI->aNodeI;
                lbI = edgeI->bNodeI;
            }
            if (isGoodHigh)
            {
                haI = edgeI->aNodeI;
                hbI = edgeI->bNodeI;
            }
        }
    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, haI, laI, hbI, lbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hbI, lbI, haI, laI);

    /// correct the hull in way1
    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    /// clear childs - see test7
    (&haI)->isChildNode = false;
    (&haI)->child = nullptr;
    (&hbI)->isChildNode = false;
    (&hbI)->child = nullptr;
    (&laI)->isChildNode = false;
    (&laI)->child = nullptr;
    (&lbI)->isChildNode = false;
    (&lbI)->child = nullptr;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in _mergeHull().

    _flipWay(way1, pCenter);  // necessary, because code before does not keep the clock-wise order.
}

double MinMaxClusterAlignment::_calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double MinMaxClusterAlignment::_area(const PointL &polygon)
{
    double area = 0;
    for (auto pointI = polygon.begin();
         pointI != polygon.end(); ++pointI)
    {
        auto pointNextI = pointI; ++pointNextI;
        if (pointNextI == polygon.end()) pointNextI = polygon.begin();
        area += pointI->x() * pointNextI->y();
        area -= pointI->y() * pointNextI->x();
    }
    return abs(area);
}

double MinMaxClusterAlignment::_triangleAltitudeOnC(Point c1, Point c2, Point perpendicular)
{
    double c = m_para.distanceFnc(c1, c2);
    double a = m_para.distanceFnc(c2, perpendicular);
    double b = m_para.distanceFnc(perpendicular, c1);
    double s = (a + b + c)/2.0;
    return 2.0*sqrt(s*(s-a)*(s-b)*(s-c)) / c;
}

bool MinMaxClusterAlignment::_isLeftSideExEqual(const Point &p1Line, const Point &p2Line, const Point &check)
{
    Point p1 = p2Line - p1Line;
    Point p2 = check - p1Line;

    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    dot = static_cast<int>(dot * 100)/100;
    double degree90strait = m_para.distanceFnc(p2Line,check);
    if (dot == 0 || ((dot/90 -0.01) < degree90strait && degree90strait < (dot/90 +0.01)))
        throw logic_error("Point is on line, and not left or right.");
    return dot < 0;
}


bool MinMaxClusterAlignment::_isRightSide(const Point &p1, const Point &p2)
{
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

double MinMaxClusterAlignment::_calcDegree(Node *node, Node *nodeTested)
{
    return _calcDegree(nodeTested->point, node->point);
}

double MinMaxClusterAlignment::_calcDegree(Point &node, Point &nodeTested)
{
    Point pointVect = nodeTested - node;

    double degree = (pointVect.x() + pointVect.y())
            / (m_para.distanceFnc(Point(0,0), pointVect)
               * m_para.distanceFnc(Point(0,0), Point(1,1)));

    double degreeArc = acos(degree);
    // clock-wise
    if (_isRightSide(Point(1,1), pointVect))
        return  degreeArc;
    else
        return  2*acos(-1) - degreeArc;
}

bool MinMaxClusterAlignment::_checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{       // calc clock-wise
    double degO1 = _calcDegree(nodePoint, cluPointOut);     // typical smaller
    double degO2 = _calcDegree(nodePoint, cluPointOut2);
    double degC = _calcDegree(nodePoint, closestP);
    if (degO1 <= degO2)
    {
        if (degO2-degO1 == M_PI)
            return true;                // see test25
        else if (degO2-degO1 > M_PI)
            return degO2 <= degC || degC <= degO1;
        else
            return degO1 <= degC && degC <= degO2;
    } else {
        if (degO2-degO1 == M_PI)
            return true;               // see test25
        return degO1 <= degC || degC <= degO2;      // shall include points although at the outer winkel! see test25
    }
}

double MinMaxClusterAlignment::_correlation(Point a, Point b)
{
    return ((a.x()*b.x()) + (a.y()*b.y())) / (m_para.distanceFnc(Point(0,0), a) * m_para.distanceFnc(Point(0,0),b));
}


bool MinMaxClusterAlignment::_checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{
    double dist1 = m_para.distanceFnc(cluPointOut, nodePoint);
    double dist2 = m_para.distanceFnc(cluPointOut2, nodePoint);
    double distC = m_para.distanceFnc(closestP, nodePoint);
    double corr1 = _correlation(cluPointOut - nodePoint, closestP - nodePoint);
    //double degree0to1For1 = (corr1 + 1) / 2.0;
    double corr2 = _correlation(cluPointOut2 - nodePoint, closestP - nodePoint);
    //double degree0to1For2 = (corr2 + 1) / 2.0;
    //double corr = degree0to1For1 + degree0to1For2;
    //double corr = corr1 + corr2;
    double corr3 = _correlation(cluPointOut - nodePoint, cluPointOut2 - nodePoint);
    // distC, dist1,2 are tradeofs not correct decisions
    return ((distC/2 < dist1 || distC/2 < dist2) && corr3 < corr1 && corr3 < corr2);
}


void MinMaxClusterAlignment::_insertClosestTestedPointsSorted(std::map<double, Node*> &closestTestedPointsSorted, Node *node, Node* closest)
{
    double degree = _calcDegree(node, closest);
    auto iter = closestTestedPointsSorted.find(degree);
    if (iter != closestTestedPointsSorted.end())
    {
        degree += SMALL_DOUBLE_STEP_MAX;
    }
    closestTestedPointsSorted.insert({degree, closest});
}

double MinMaxClusterAlignment::_calcCeListDist(CeList &ceList)
{
    if (ceList.ceList.empty())
        return 0;
    double distC = m_para.distanceFnc(ceList.ceList.front().fromNode->point, ceList.ceList.back().toNode->point);
    double distS = 0;
    Node *prev = ceList.ceList.front().fromNode;
    for (const auto &ce : ceList.ceList)
    {
        distS += m_para.distanceFnc(prev->point, ce.insertNode->point);
        prev = ce.insertNode;
    }
    distS += m_para.distanceFnc(ceList.ceList.back().insertNode->point, ceList.ceList.back().toNode->point);

    return distS - distC;
}


MinMaxClusterAlignment::CeList::CeListType::iterator  MinMaxClusterAlignment::_insertToCeList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert)
{
    auto ceListResI = ceList.ceList.end();

    if (!ceList.ceList.empty())
    {
        double distDirect = seboehDist(ceList.ceList.front().fromNode, ceList.ceList.back().toNode, insert);
        auto insertPosI = ceList.ceListOrder.insert({distDirect, insert}).first;
        if (++insertPosI != ceList.ceListOrder.end())
        {
            ceList.ceList.clear();
            for (auto &orderEl : ceList.ceListOrder)
            {
                auto iter = __insertToCeListAlongList(ceList, rootFrom, rootTo, orderEl.second);
                if (orderEl.second == insert)
                    ceListResI = iter;
            }
        } else {
            ceListResI = __insertToCeListAlongList(ceList, rootFrom, rootTo, insert);
        }
    }
    else
    {
        ceListResI = __insertToCeListAlongList(ceList, rootFrom, rootTo, insert);
        double distDirect = seboehDist(ceList.ceList.front().fromNode, ceList.ceList.back().toNode, insert);
        ceList.ceListOrder.insert({distDirect, insert});
    }

    return ceListResI;
}

MinMaxClusterAlignment::CeList::CeListType::iterator  MinMaxClusterAlignment::__insertToCeListAlongList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert)
{
    ClosestEdges ce(insert);
    CeList::CeListType::iterator ceI = ceList.ceList.end();

    if (ceList.ceList.empty())
    {
        ce.fromNode = rootFrom;
        ce.toNode = rootTo;
    }
    else
    {
        double bestDist = numeric_limits<double>::max();
        auto bestI = ceList.ceList.end();
        Node *bestPrev = nullptr;
        Node *prev = ceList.ceList.front().fromNode;
        for (auto foundCeI = ceList.ceList.begin();
             foundCeI != ceList.ceList.end(); ++foundCeI)
        {
            double dist = seboehDist(prev, foundCeI->insertNode, ce.insertNode);
            if (dist < bestDist)
            {
                bestDist = dist;
                bestI = foundCeI;
                bestPrev = prev;
            }
            prev = foundCeI->insertNode;
        }
        // check end
        double dist = seboehDist(ceList.ceList.back().insertNode, ceList.ceList.back().toNode, ce.insertNode);
        if (dist < bestDist)
        {
            bestDist = dist;
            ceI = ceList.ceList.end();
        } else {
            ceI = bestI;
        }

        // set from to
        if (ceI != ceList.ceList.end())
        {
            ce.fromNode = bestPrev;
            ce.toNode = ceI->insertNode;
        } else {
            ce.fromNode = ceList.ceList.back().insertNode;
            ce.toNode = ceList.ceList.back().toNode;
        }
    }

    ce.dist = seboehDist(ce.fromNode, ce.toNode, ce.insertNode);
    return ceList.ceList.insert(ceI, ce);
}

/**
 * @brief MinMaxClusterAlignment::_isAtEdge is for test12
 * but not at one already guarantied edge - see test12, because, then it would be grown
 * by evultionary algorithm and is not guarantied. - see test45 for &&
 * there should be no point inside those triangle
 */
bool MinMaxClusterAlignment::_isAtEdge(std::list<Node*> &openNodes, Node *from, Node *to, std::list<Node*> &selectedNodes)
{
    bool hasNoNodeInside = true;
    for (Node* &innerNode : openNodes)
    {
        auto findI = find(selectedNodes.begin(), selectedNodes.end(), innerNode);
        if (findI != selectedNodes.end())
            continue;

        for (Node* &selectedNode : selectedNodes)
        {
            if (_isInsideTriangle(from->point, to->point, selectedNode->point, innerNode->point))
            {
                hasNoNodeInside = false;
                break;
            }
        }
        if (!hasNoNodeInside)
            break;
    }

    return hasNoNodeInside;
}

void MinMaxClusterAlignment::_determineGroupAlignmentRec(std::list<Node*> selectedNodes, std::list<Node*> openNodes)
{
    /// * select the nodes
    while (!openNodes.empty())
    {
        Node *openNode = openNodes.front();
        selectedNodes.push_back(openNode);
        openNodes.pop_front();

        /// * find best edge for selected alignment
        double bestDist = numeric_limits<double>::max();
        auto bestToI = m_edgesGuarantied.end();
        CeList bestCeList;
        Node *prev = *(--m_edgesGuarantied.end());
        for (auto nextI = m_edgesGuarantied.begin();
             nextI != m_edgesGuarantied.end(); ++nextI)
        {
            Node *next = *nextI;

            ///     * check if close to hull-edge
            if (_isAtEdge(openNodes, prev, next, selectedNodes))
            {
               CeList ceList;
               // TODO [seb] speedup - presorting of directAlign of selectedNodes
                for (auto &selectedN : selectedNodes)
                    _insertToCeList(ceList, prev, next, selectedN);

                double dist = _calcCeListDist(ceList);

                if (dist < bestDist)
                {
                    bestDist = dist;
                    bestToI = nextI;
                    bestCeList = ceList;
                }
            }

            prev = next;
        }

        double currentDist = 0;
        for (Node * &node : selectedNodes)
            currentDist += node->guarantiedBasicDist;

        auto selectI = selectedNodes.begin();
        if (selectedNodes.size() == 3
                && ((*selectI)->point.x() == 310 || (*selectI)->point.x() == 340)
                && ((*(++selectI))->point.x() == 310 || (*selectI)->point.x() == 340)
                && ((*(++selectI))->point.x() == 310 || (*selectI)->point.x() == 340)
           )
            int i=5;

        ///  * if bestDist < (bestA + bestB) change both to alignment to that edge
        if (!bestCeList.ceList.empty()
              && bestDist < currentDist)
        {
            auto bestFromI = bestToI;
            if (bestFromI != m_edgesGuarantied.begin())
                --bestFromI;
            else
                bestFromI = --m_edgesGuarantied.end();

            ///     * set the bigger dist
            for (auto &ce : bestCeList.ceList)
            {
                double distAligned = seboehDist(ce.fromNode, ce.toNode, ce.insertNode);

                // it is necessary to enlarge the dist, because it is used in evolutinoary-grow later.
                if (distAligned > ce.insertNode->guarantiedDist)
                {       // see test10-2 and test12
                    ce.insertNode->guarantiedDist = distAligned;
                    ce.insertNode->guarantiedFrom = ce.fromNode;
                    ce.insertNode->guarantiedTo = ce.toNode;
                }
                // TODO [seb] riscy? - reduced also if not applied (upgraded)
                ce.insertNode->guarantiedBasicDist = min(ce.insertNode->guarantiedBasicDist
                                                         , distAligned);
            }
        }

        /// * recursive call for bigger groups
        _determineGroupAlignmentRec(selectedNodes, openNodes);

        selectedNodes.pop_back();
    }

}

void MinMaxClusterAlignment::determineGroupAlignment(std::list<Node*> &_openNodes)
{
    std::list<Node*> selectedNodes;
    auto openNodes = _openNodes;
    while (!openNodes.empty())
    {
        Node *openNode = openNodes.front();
        selectedNodes.push_back(openNode);
        openNodes.pop_front();

        _determineGroupAlignmentRec(selectedNodes, openNodes);

        selectedNodes.pop_back();
    }
}

TSP::Point MinMaxClusterAlignment::_calcPerpendicularPoint(Node *from, Node *to, Node *normal)
{
    double alt = _triangleAltitudeOnC(from->point, to->point, normal->point);
    double c = m_para.distanceFnc(from->point, normal->point);
    double b = sqrt(c*c - alt*alt);

    return from->point + ((to->point - from->point) * (b/ m_para.distanceFnc(from->point, to->point)));
}


void MinMaxClusterAlignment::determineGuarantiedAlignment(NodeL &input, NodeL &hull)
{
    /// * Collect the maximal alignment, of the guarantied alignments generated from each hull-edge.
    ///   Write it into a temporary tree.
    ///   First alignment in first group of hirachical-clustering with cutting at the biggest dist,
    ///   would be the best edge. But for now, there shall be used the maximum of those edges.
    ///
    for (const auto &node : hull)
    {
        node->isHullNodeProcessed = false;      // for test12
        m_edgesGuarantied.push_back(node.get());
    }

    // collect rest of alignment
    list<Node*> openNodes;
    for (auto &node : input)
    {
        auto findI = std::find_if(hull.begin(), hull.end(),
                                  [node](const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        if (findI == hull.end())
            openNodes.push_back(node.get());
    }

    // find smallest direct alignment, if more edges than points
    int sshift = static_cast<int>(m_edgesGuarantied.size()) - openNodes.size();
    if (sshift >= 0)
    {   // test12, test60 and test10-2?
        map<double, tuple<Node*,Node*,Node*> > sortedEdgesWithNodes;    // node, from, to
        // for test12?
        for (auto openNI = openNodes.begin();
             openNI != openNodes.end(); ++openNI)
        {
            Node *insertNode = *openNI;

            Node *prev = *(--m_edgesGuarantied.end());
            for (auto nextI = m_edgesGuarantied.begin();
                 nextI != m_edgesGuarantied.end(); ++nextI)
            {
                Node *next = *nextI;
                _insertToMap(seboehDist(prev, next, insertNode), sortedEdgesWithNodes, make_tuple(insertNode, prev, next));
                prev = next;
            }
        }

        // for test12
        Node *node, *from, *to;
        size_t setElC = 0;
        auto sortedI = sortedEdgesWithNodes.begin();
        while (setElC < openNodes.size()
               && sortedI != sortedEdgesWithNodes.end())
        {
            tie(node,from,to) = sortedI->second;
            if (node->guarantiedFrom == nullptr
                    && ! from->isHullNodeProcessed)
            {
                node->guarantiedDist = sortedI->first;
                node->guarantiedBasicDist = sortedI->first;
                node->guarantiedFrom = from;
                node->guarantiedTo = to;
                from->isHullNodeProcessed = true;
                ++setElC;
            }
            ++sortedI;
        }

    }
    else
    {
        // for test60 and test65?

        // general alignment to edge
        DirectAlignments aligns;
        for (auto &node : openNodes)
        {
            ClosestEdges diAl;
            Node *prev = m_edgesGuarantied.back();
            for (Node * &next : m_edgesGuarantied)
            {
                double distDirect = seboehDist(prev, next, node);
                node->isInWay = false;
                diAl.insertNode = node;
                diAl.fromNode = prev;
                diAl.toNode = next;

                aligns.insert({distDirect, diAl});

                prev = next;
            }
        }

        // calc max dist
        list<ClosestEdges> bestAlignments;
        for (DirectAlignments::value_type &alignEl : aligns)
        {
            ClosestEdges alignment = alignEl.second;

            if (!alignment.insertNode->isInWay)
            {
                Node *from = alignment.fromNode;
                Node *to = alignment.toNode;
                //  Point perp = _calcPerpendicularPoint(from, to, ce.insertNode);

                double c = m_para.distanceFnc(from->point, to->point);
                Point vect = (to->point - from->point) / c;
                // solved:
                // a1 + c = a2 + 2*c1;  a2 + c = a1 + 2*c2
                // c1 = (a1 + c - a2)/2 ; c2 = (a2 + c - a1)/2
                double a_1 = m_para.distanceFnc(from->point, alignment.insertNode->point);
                double a_2 = m_para.distanceFnc(to->point, alignment.insertNode->point);
                double c_1 = (a_1 + c - a_2)/2.0;

                Point perpC = from->point + vect * c_1;

                alignment.insertNode->guarantiedMaxDist = seboehDist(perpC, from->point, alignment.insertNode->point);

                /// enlarge guarantiedMax by neighbours - only theoretically for test108 (but not the issue)
                // search second closest neighbour x, for point y = node
/*                Node *bestNode = nullptr;
                Node *secondBestNode;
                double bestDist = numeric_limits<double>::max();
                for (Node *open : openNodes)
                {
                    if (open == alignment.insertNode)
                        continue;
                    double dist = m_para.distanceFnc(alignment.insertNode->point, open->point);
                    if (dist < bestDist)
                    {
                        if (bestNode == nullptr)
                            secondBestNode = open;
                        else
                            secondBestNode = bestNode;
                        bestNode = open;
                        bestDist = dist;
                    }
                }

                // dist a = between x and farest edge-point of y = z
                double a = max(m_para.distanceFnc(secondBestNode->point, from->point)
                               , m_para.distanceFnc(secondBestNode->point, to->point));

                if (bestDist < a)
                {       // else, to far away to help at this edge, because the second would help more or is used as base.
                    // assume:
                    // a^2 + a^2 = b^2 -> b
                    double b = sqrt(a*a + a*a);
                    // 2b - 2a = searched dist - a is ortho to b and b should be an approx for the edge from-to
                    double alt = (2*b - 2*a);
                    alignment.insertNode->guarantiedMaxDist = max(
                                alignment.insertNode->guarantiedMaxDist
                                , alt);
                }
*/
                bestAlignments.push_back(alignment);
                alignment.insertNode->isInWay = true;
            }
        }

        // set guarantiedDist - by degree, because test109
        for (ClosestEdges &alignment : bestAlignments)
        {
            Node *node = alignment.insertNode;
            Node *prev = m_edgesGuarantied.back();
            for (Node * &next : m_edgesGuarantied)
            {
                double dist = seboehDist(prev, next, node);
                if (dist < node->guarantiedMaxDist)
                {       // because not all edges are relevant - save some time
                    /// outer alignments
                    map<double, Node*> inbetweenPointsOrdered;
                    for (Node* &nodeBetween : openNodes)
                    {
                        if (_checkAngelBetween(prev->point, next->point, node->point, nodeBetween->point))
                        {
                            inbetweenPointsOrdered.insert({_calcDegree(node, nodeBetween), nodeBetween});
                        }
                    }

                    Node *from = prev;
                    double bestDistBetween = numeric_limits<double>::max(); // search minimum, because of test65
                    Node *bestFrom, *bestTo;
                    for (auto &betweenEl : inbetweenPointsOrdered)
                    {
                        Node *to = betweenEl.second;

                        double distBetween = seboehDist(from, to, node);

                        if (distBetween < bestDistBetween)      // necessary to minimize, because test98 420x280 and in determineCluster no point between triangle at foundS.
                        {
                            bestDistBetween = distBetween;
                            bestFrom = from;
                            bestTo = to;
                        }

                        from = to;
                    }
                    Node *to = next;
                    double distBetween = seboehDist(from, to, node);
                    /// or at least direct alignment
                    if (distBetween < bestDistBetween)
                    {
                        bestDistBetween = distBetween;
                        bestFrom = from;
                        bestTo = to;
                    }

                    // global maiximize over all hull-edges
                    if (node->guarantiedDist < bestDistBetween)
                    {
                        node->guarantiedDist = bestDistBetween;
                        node->guarantiedFrom = bestFrom;
                        node->guarantiedTo = bestTo;
                    }
                    // TODO [seb] riscy?
                    node->guarantiedBasicDist = min(node->guarantiedBasicDist, bestDistBetween);

                }

                prev = next;
            }
        }

    }       // else, more innerPoint than hull-edges

    // multiple alignments together - for test60 (2group) and test45 (2group)
    determineGroupAlignment(openNodes);
}

// because test38 -> HINT: in the whole algorithm it is not allowed to have map.insert with collision.
template <typename T, typename V>
std::pair<typename T::iterator, bool> MinMaxClusterAlignment::_insertToMap(double dist, T &mapContainer, V &&value)
{
    assert(dist > -SMALL_DOUBLE_STEP_MIN);
    auto mI = mapContainer.end();
    if ((mI = mapContainer.find(dist)) != mapContainer.end())
    {
        if (mI->second == value) return make_pair(mI, false);

        // HINT: can happen at neighboursEdge - MARK:collision1
        if (!m_isLogCollisionDetected)
        {
            cerr << "\nCollision detected - is handled!" << endl;
            m_isLogCollisionDetected = true;
        }

        auto mInext = mI; ++mInext;
        double diffNext = 0;
        if (mInext == mapContainer.end())
        {
            diffNext = SMALL_DOUBLE_STEP_MIN;
        } else {
            diffNext = (mInext->first - mI->first) / 2.0;
        }

        // overflow
        if ((mI->first + SMALL_DOUBLE_STEP_MIN) < mI->first)
        {
            if (!m_isLogOverflowDetected)
            {
                cerr << "\nOverflow detected - please shrink your inputs!" << endl;
                m_isLogOverflowDetected = true;
            }
            dist = std::numeric_limits<double>::min();
        } else {
            dist = max(min(diffNext, SMALL_DOUBLE_STEP_MAX), SMALL_DOUBLE_STEP_MIN) + mI->first;
        }
    }
    return mapContainer.insert({dist, value});
}


bool MinMaxClusterAlignment::_isNotInsideTriangle(Node *point1, Node *point2, Node *point3, std::list<Node*> &pointsInside)
{
    for (auto &insideNode : pointsInside)
    {
        if (insideNode != point1 && insideNode != point2 && insideNode != point3)
        {
            if (_isInsideTriangle(point1->point, point2->point, point3->point, insideNode->point))
            {
                return false;
            }
        }
    }
    return true;
}

MinMaxClusterAlignment::ClusterGrow MinMaxClusterAlignment::determineClusters(NodeL &input, NodeL &hull)
{
    ClusterGrow clusters;
    double coordMax = max(tspWidth, tspHeight) * 12;
    //  TODO [seboeh] * 12 - still a problem, because the intersection below can be far away.

    /// * walk through all inputs except hull-points
    for (const auto &node : input)
    {
        Cluster clu;
        clu.point = node;
        auto fI = std::find_if(hull.begin(), hull.end(),
                               [node](const shared_ptr<Node> &val)
        {
            return val->point == node->point;
        });
        if (fI != hull.end())
            continue;

        // HINT: TECHNIQUE: _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());

        /// * find closest three points including the cluster-point p.
        ///   HINT: the closest point are allways the prefered points.
        ///   Because, if the alignment point is more far away, then the alignment would cross the other point,
        ///   and then it is not clear, what the other point aligns to.
        ///   --> the closest three points are enough to see - for 'cutting'
        ///
        map<double, shared_ptr<Node>> closestPoints;
        for (const auto &node2 : input)
        {
            if (node == node2)
                continue;
            _insertToMap(m_para.distanceFnc(node->point, node2->point), closestPoints, node2);
        }
        if (closestPoints.size() < 3)
            return clusters;
        for (const auto &closE : closestPoints)
        {
            clu.closestN.push_back(closE.second);
        }

        /// * check if x is inside P_1,
        auto closestP1I = closestPoints.begin();
        auto closestP2I = closestP1I; ++closestP2I;
        auto closestP3I = closestP2I; ++closestP3I;
        list<Node*> closestTestedP;
        closestTestedP.push_back(closestP1I->second.get());
        closestTestedP.push_back(closestP2I->second.get());
        closestTestedP.push_back(closestP3I->second.get());
        Node* foundP1 = closestP1I->second.get();
        Node* foundP2 = closestP2I->second.get();
        Node* foundP3 = closestP3I->second.get();
        if ( ! _isInsideTriangle(closestP1I->second->point, closestP2I->second->point, closestP3I->second->point, node->point))
        {
            ///     * if yes, continue
            ///     * else determine next closest p_2
            ///     * check in two of P_1 and p_2, if x is inside this triangle
            ///         * if yes, continue
            ///         * else, repeat until end of inputs. -> unexpected-error, if not found
            auto closestPnextI = ++closestP3I;
            while (closestPnextI != closestPoints.end())
            {
                // 2 over n
                // TODO [seb] hint: keep closest fixed
                bool stopWhile = false;
                for (auto closestTestedP1I = closestTestedP.begin();
                     closestTestedP1I != closestTestedP.end(); ++closestTestedP1I)
                {
                    for (auto closestTestedP2I = closestTestedP.begin();
                         closestTestedP2I != closestTestedP.end()
                         && closestTestedP1I != closestTestedP2I; ++closestTestedP2I)
                    {
                        if (_isInsideTriangle((*closestTestedP1I)->point, (*closestTestedP2I)->point, closestPnextI->second->point, node->point))
                        {
                            foundP1 = *closestTestedP1I;
                            foundP2 = *closestTestedP2I;
                            foundP3 = closestPnextI->second.get();
                            closestTestedP.push_back(closestPnextI->second.get());
                            stopWhile = true;
                            break;
                        }
                    }
                    if (stopWhile) break;
                }
                if (stopWhile) break;
                closestTestedP.push_back(closestPnextI->second.get());
                ++closestPnextI;
            }
            if (closestPnextI == closestPoints.end())
                return clusters;
        }

        /// * sort closestTestedP, according to degree, because there is no order than circular around the point
        // cross product a*d - b*c, with point = (a,b) and (c,d) = (1,1)
        map<double, Node*> closestTestedPointsSorted;
        _insertToMap(_calcDegree(node.get(), foundP1), closestTestedPointsSorted, foundP1);
        _insertToMap(_calcDegree(node.get(), foundP2), closestTestedPointsSorted, foundP2);
        _insertToMap(_calcDegree(node.get(), foundP3), closestTestedPointsSorted, foundP3);

        for (const auto &cloPointsEl : closestTestedPointsSorted)
        {
            const Node *node = cloPointsEl.second;
            auto findI = std::find_if(closestPoints.begin(), closestPoints.end(),
                                   [node] (const map<double, shared_ptr<Node>>::value_type &val)
            {
               return val.second.get() == node;
            });
            if (findI != closestPoints.end())
                closestPoints.erase(findI);
        }

        // because of test114
        double d1 = seboehDist(foundP1, node->guarantiedFrom, node.get());
        d1 = max(d1, seboehDist(foundP1, node->guarantiedTo, node.get()));
        double d2 = seboehDist(foundP2, node->guarantiedFrom, node.get());
        d2 = max(d2, seboehDist(foundP2, node->guarantiedTo, node.get()));
        double d3 = seboehDist(foundP3, node->guarantiedFrom, node.get());
        d3 = max(d3, seboehDist(foundP3, node->guarantiedTo, node.get()));
        node->guarantiedMaxDist = max(d1, max(d2, d3));

        foundP1->foundDist = numeric_limits<double>::max();
        foundP2->foundDist = numeric_limits<double>::max();
        foundP3->foundDist = numeric_limits<double>::max();

        /// * scan the next closest points between already detected points
        ///   If the point between the already detected points is, with his best alignment to the cluster-point,
        ///   better than the guarantied alignment, add this point and repeat the scan.
        ///
        clu.verts.clear();
        bool isChanged = true;
        while (isChanged)
        {
            isChanged = false;
            auto closestPI = closestPoints.begin();
            while (closestPI != closestPoints.end())
            {
                // search the pair of degree between closestPI.
                bool isIterated = false;
                if (node->point.x() == 310)
                    if (closestPI->second->point.x() == 240)
                        int i=5;
                for (auto cluPointOutI = closestTestedPointsSorted.begin();
                 cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
                {
                    auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
                    if (cluPointOut2I == closestTestedPointsSorted.end())
                        cluPointOut2I = closestTestedPointsSorted.begin();

                    if (_checkAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                    {
                        /// calculations of alignment

                        double dist1 = seboehDist(cluPointOutI->second, closestPI->second.get(), node.get());
                        double dist2 = seboehDist(cluPointOut2I->second, closestPI->second.get(), node.get());

// debug
if (node->point.x() == 310)
    if (closestPI->second->point.x() == 240)
        int i=5;



        // TODO [seb] use angle instead of min(dist1,dist2)

                        // because test114
                        // before: if (std::min(dist1, dist2) <= node->guarantiedDist)
                        // calc min dist to three closest nodes.
                        double d1 = seboehDist(foundP1, closestPI->second.get(), node.get());
                        double d2 = seboehDist(foundP2, closestPI->second.get(), node.get());
                        double d3 = seboehDist(foundP3, closestPI->second.get(), node.get());
                        double bestD = min(min(d1, d2), d3);

                        // =, because it can be the guarantied alignment at the hull, which was found here
                        bool doIter = true;
                        if (bestD <= node->guarantiedDist)
                        {
                            closestPI->second->foundDist = bestD;
                            // guarantie point is not too far away, so another point is better.
                           if (closestPI->second->isHull
                                 || (cluPointOutI->second->foundDist >= bestD
                                    && cluPointOut2I->second->foundDist >= bestD))
                            {
                                // standard insert
                                _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                                isChanged = true;
                                closestPI = closestPoints.erase(closestPI);
                                doIter = false;
                            } else {
                                int i=5;
                            }
                        }

                        if (doIter)
                            ++closestPI;
                        isIterated = true;
                        break;
                    }
                }       // degree search finished
                assert(isIterated); // if (!isIterated) ++closestPI;
            }       // continue with next closest point, because they are typically equal distributed around cluster-node.
        }       // there was no change anymore, means the closest and relevant nodes are all found.

        // clear foundDist
        for (auto &el : closestTestedPointsSorted)
            el.second->foundDist = numeric_limits<double>::max();

        /// * create cluster
        for (auto &el : closestTestedPointsSorted)
            clu.closestTestedPointsSorted.push_back(el.second);

        // TODO [seb] still necessary at MinMaxClusterAlignment?
        // because test5 and test12 - areaOsquare solve the problem of test12 and test5
        // but here because of safety for test5 and similar, the closest edges including first.
        map<double, Node*> sortedByDegree;
        _insertToMap(_calcDegree(node.get(), foundP1), sortedByDegree, foundP1);
        _insertToMap(_calcDegree(node.get(), foundP2), sortedByDegree, foundP2);
        _insertToMap(_calcDegree(node.get(), foundP3), sortedByDegree, foundP3);
        auto sortedI = --sortedByDegree.end();
        auto sortedIn = sortedByDegree.begin();
        foundP1 = sortedIn->second;
        _insertToMap(seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , clu.neighbourEdges, make_tuple(sortedI->second, sortedIn->second));
        sortedI = sortedByDegree.begin();
        ++sortedIn;
        foundP2 = sortedIn->second;
        _insertToMap(seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , clu.neighbourEdges, make_tuple(sortedI->second, sortedIn->second));
        ++sortedI;
        ++sortedIn;
        foundP3 = sortedIn->second;
        _insertToMap(seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , clu.neighbourEdges, make_tuple(sortedI->second, sortedIn->second));


        // because test20, test100 - there can be two dots behind each other in the ray from the cluster-center
/*        double degreeMin = numeric_limits<double>::max();
        for (shared_ptr<Cluster> &clu : clusters)
        {
            Point cluP = clu->point.get()->point;
            Node *cluN = clu->point.get();
            auto iterPcurr = clu->closestTestedPointsSorted.begin();
            auto iterPprev = --clu->closestTestedPointsSorted.end();
            for (;iterPcurr != clu->closestTestedPointsSorted.end(); ++iterPcurr)
            {
                double x = fabs(_calcDegree(cluN, *iterPcurr) - _calcDegree(cluN, *iterPprev));
                double degree = fabs(std::fmod(x, 3.14158) - static_cast<int>(x/3.14158)*3.14158);
                if (degree < degreeMin)
                    degreeMin = degree;
                if (degree < 0.001)     // because there can be three points in a line - TODO improve the hard-coded number by variable number
                {
                    auto iterPrevPrev = iterPprev;
                    if (iterPrevPrev == clu->closestTestedPointsSorted.begin())
                        iterPrevPrev = --clu->closestTestedPointsSorted.end();
                    else
                        --iterPrevPrev;
                    auto iterNext = iterPcurr;
                    ++iterNext;
                    if (iterNext == clu->closestTestedPointsSorted.end())
                        iterNext = clu->closestTestedPointsSorted.begin();

                    double distPrev1 = seboehDist((*iterPrevPrev)->point, (*iterPprev)->point, cluP);
                    double distPrev2 = seboehDist((*iterPrevPrev)->point, (*iterPcurr)->point, cluP);
                    double distNext1 = seboehDist((*iterNext)->point, (*iterPprev)->point, cluP);
                    double distNext2 = seboehDist((*iterNext)->point, (*iterPcurr)->point, cluP);

                    if (min(distPrev1, distPrev2) < min(distNext1, distNext2))
                    {
                        if (distPrev1 > distPrev2)
                            swap(*iterPcurr, *iterPprev);
                           // else nothing, because all already correct
                    } else {
                        if (distNext1 < distNext2)
                            swap(*iterPcurr, *iterPprev);
                    }
                }

                iterPprev = iterPcurr;
            }
        }
*/

        /// collect all child-triangles below the main triangle
        // add old edge, because test9 and test19
        map<double, Node*>::iterator beginI = closestTestedPointsSorted.begin();
        while (beginI != closestTestedPointsSorted.end()
               && beginI->second != foundP1
               && beginI->second != foundP2
               && beginI->second != foundP3
               )
            ++beginI;

        auto foundP = foundP1;
        auto foundPN = foundP2;
        auto foundS = foundP3;              // because test10-2
        int foundI = 0;
        if (beginI->second == foundP2)
        {
            foundP = foundP2;
            foundPN = foundP3;
            foundS = foundP1;
            foundI = 1;
        }
        else if (beginI->second == foundP3)
        {
            foundP = foundP3;
            foundPN = foundP1;
            foundS = foundP2;
            foundI = 2;
        }
        assert(closestTestedPointsSorted.size() > 2);

        // toMap
        list<Node*> closestTestedNodesSorted(closestTestedPointsSorted.size());
        transform(closestTestedPointsSorted.begin(), closestTestedPointsSorted.end(), closestTestedNodesSorted.begin(),
                  [](const pair<double, Node*> &val) -> Node*
        {
            return val.second;
        });

        map<double, Node*>::iterator pointI = beginI; ++pointI;
        while (pointI != beginI)
        {
            if (foundPN == pointI->second)
            {
                foundI = ++foundI % 3;
                if (foundI == 0)
                {
                    foundP = foundP1;
                    foundPN = foundP2;
                    foundS = foundP3;
                } else if (foundI == 1)
                {
                    foundP = foundP2;
                    foundPN = foundP3;
                    foundS = foundP1;
                } else {
                    foundP = foundP3;
                    foundPN = foundP1;
                    foundS = foundP2;
                }
            } else {
                if (node->point.x() == 420)
                {
                    if (foundS->point.x() == 560)
                        int i=5;
                }
                // create edge for test9 and test19
                if (_isNotInsideTriangle(node.get(), pointI->second, foundP, closestTestedNodesSorted))     // check, because test98
                    _insertToMap(seboehDist(foundP, pointI->second, node.get())
                                               , clu.neighbourEdges, make_tuple(foundP, pointI->second));

                if (_isNotInsideTriangle(node.get(), pointI->second, foundPN, closestTestedNodesSorted))
                    _insertToMap(seboehDist(pointI->second, foundPN, node.get())
                                               , clu.neighbourEdges, make_tuple(pointI->second, foundPN));

                // because test10-2
                if (_isNotInsideTriangle(node.get(), pointI->second, foundS, closestTestedNodesSorted))
                    _insertToMap(seboehDist(pointI->second, foundS, node.get())
                                               , clu.neighbourEdges, make_tuple(pointI->second, foundS));
            }

            ++pointI;
            if (pointI == closestTestedPointsSorted.end())
                pointI = closestTestedPointsSorted.begin();
        }


        // debug
        if (node->point.x() == 310)
            int i=5;

        for (auto closEl : clu.neighbourEdges)
        {
            Node *from = std::get<0>(closEl.second);
            Node *to = std::get<1>(closEl.second);
            if ((from->point.x() == 290 || to->point.x() == 290)
                    && (from->point.x() == 320 || to->point.x() == 320))
                int i=5;
        }

        /// repeat from beginning, until all inputs are handled.
        clusters.push_back(shared_ptr<Cluster>(new Cluster(clu),no_op_delete()));
    }

    return clusters;
}

MinMaxClusterAlignment::Node* MinMaxClusterAlignment::_nextNode(Node *curr, Node * &prev)
{
    if (curr->edge1 == prev)
    {
        prev = curr;
        return curr->edge2;
    }
    else
    {
        prev = curr;
        return curr->edge1;
    }
}

bool MinMaxClusterAlignment::_updateHullOrUpdateSimple(shared_ptr<Node> hullNodeA, shared_ptr<Node> updateNode, NodeL &hull)
{
// MARK_hullNodeSearch
    auto hullI = find_if(hull.begin(), hull.end(),
                         [hullNodeA] (const NodeL::value_type &val)
    {
        return hullNodeA->point == val->point;
    });
    shared_ptr<Node> hullNode;
    if (hullI != hull.end())
        hullNode = *hullI;
    else
        hullNode = hullNodeA;

    // check valid updateNode
    if (updateNode.get() == hullNode->edge1
            || updateNode.get() == hullNode->edge2)
        return false;
    if (hullNode->edge1 != nullptr
            && hullNode->edge2 != nullptr)
        return false;
    if (updateNode->edge1 != nullptr
            && updateNode->edge2 != nullptr)
        return true;

    // update
    if (hullNode->edge1 == nullptr)
    {
        hullNode->edge1 = updateNode.get();
        hullNodeA->edge1 = updateNode.get();
    }
    else if (hullNode->edge2 == nullptr)
    {
        hullNode->edge2 = updateNode.get();
        hullNodeA->edge2 = updateNode.get();
    }    else return true;
    if (updateNode->edge1 == nullptr)
        updateNode->edge1 = hullNode.get();
    else if (updateNode->edge2 == nullptr)
        updateNode->edge2 = hullNode.get();
    else return true;

    // check
    assert(updateNode->edge1 != updateNode.get());
    assert(updateNode->edge2 != updateNode.get());

    return true;
}

bool MinMaxClusterAlignment::_updateAllowed(shared_ptr<Node> closest, shared_ptr<Node> node, bool isTestOnClique)
{
    // check valid nodes
    if (node.get() == closest->edge1
            || node.get() == closest->edge2)
        return false;
    if (closest->edge1 != nullptr
            && closest->edge2 != nullptr)
        return false;
    if (node->edge1 != nullptr
            && node->edge2 != nullptr)
        return true;        // because this calls intern if, which calls this criteria again, and breaks the loop.

    // check on clique - graph theory
    // walk through the edges1/2 starting from closest:
    //  if ending at node, there is a clique - edge shall not be supported
    //  only if the cound of vertices = n.
    if (isTestOnClique)
    {
        Node *nod = closest.get();
        Node *prev = nullptr;
        size_t tspSizeI = 0;
        bool isClique = false;
        while (nod != nullptr)
        {
            ++tspSizeI;
            if (nod == node.get() && tspSizeI < m_tspSize)
            {
                isClique = true;
                break;
            }
            if (nod->edge1 != nullptr && nod->edge1 != prev)
            {
                prev = nod;
                nod = nod->edge1;
            } else if (nod->edge2 != nullptr && nod->edge2 != prev)
            {
                prev = nod;
                nod = nod->edge2;
            }
            else break;
        }
        return !isClique;
    }

    return true;
}


void MinMaxClusterAlignment::_updateNodes2(map<double, Cluster*> &clustersSorted, NodeL &hull)
{
    for (auto &cluE : clustersSorted)
    {
        Cluster *clu = cluE.second;
        Node* from;
        Node* to;
        double distBest = std::numeric_limits<double>::max();
        for (auto nodeI = clu->closestN.begin();
             nodeI != clu->closestN.end(); ++nodeI)
        {
            // update
            auto nodeNextI = nodeI; ++nodeNextI;
            if (nodeNextI == clu->closestN.end())
                nodeNextI = clu->closestN.begin();

            // case no edges
            int clearedEdgesNode1C = 0;
            int clearedEdgesNode2C = 0;
            if ((*nodeI)->edge1 != nullptr)
                ++clearedEdgesNode1C;
            if ((*nodeNextI)->edge1 != nullptr)
                ++clearedEdgesNode2C;
            if ((*nodeI)->edge2 != nullptr)
                 ++clearedEdgesNode1C;
            if ((*nodeNextI)->edge2 != nullptr)
                 ++clearedEdgesNode2C;

            double dist = TSPHelper::seboehDist((*nodeI).get(), (*nodeNextI).get(), clu->point.get());
            if (clearedEdgesNode1C == 2 && clearedEdgesNode2C == 2)
            {
                // replace
                if ((*nodeI)->edge1 == (*nodeNextI)->edge1
                        || (*nodeI)->edge2 == (*nodeNextI)->edge2
                        || (*nodeI)->edge1 == (*nodeNextI)->edge2
                        || (*nodeI)->edge2 == (*nodeNextI)->edge1)
                {
                    if (dist < distBest)
                    {
                        distBest = dist;
                        from = (*nodeI).get();
                        to = (*nodeNextI).get();
                    }
                }
            }
            else
            {
                // check
                if (dist < distBest)
                {
                    distBest = dist;
                    from = (*nodeI).get();
                    to = (*nodeNextI).get();
                }
            }
        }

        ///     * connect c.point to the closest node pn
        if (clu->point->edge1 != nullptr
                || clu->point->edge2 != nullptr)
        {
            // check if redirection is necessary.
            Node* altN = (std::max)(clu->point->edge1, clu->point->edge2);
            Node* startN = altN;
            if (startN->edge1 != clu->point.get())
                startN = startN->edge1;
            else if (startN->edge2 != clu->point.get())
                startN = startN->edge2;
            assert(startN != nullptr);
            Node* closestN;
            Node* closestOppoN;
            if (m_para.distanceFnc(from->point, altN->point) <
                    m_para.distanceFnc(to->point, altN->point))
            {
                closestN = from;
                closestOppoN = to;
            }
            else
            {
                closestN = to;
                closestOppoN = from;
            }
            double distWay1 = m_para.distanceFnc(startN->point, altN->point)
                    + m_para.distanceFnc(altN->point, clu->point->point)
                    + m_para.distanceFnc(clu->point->point, closestN->point)
                    + m_para.distanceFnc(closestN->point, closestOppoN->point);
            double distWay2 = m_para.distanceFnc(startN->point, altN->point)
                    + m_para.distanceFnc(altN->point, closestN->point)
                    + m_para.distanceFnc(closestN->point, clu->point->point)
                    + m_para.distanceFnc(clu->point->point, closestOppoN->point);
            if (distWay1 < distWay2)
            {
                from = altN;
                to = closestN;
            }
        }

        shared_ptr<Node> fromSP(from, no_op_delete());
        shared_ptr<Node> toSP(to, no_op_delete());

        _updateHullOrUpdateSimple(fromSP, clu->point, hull);
        _updateHullOrUpdateSimple(toSP, clu->point, hull);
    }
}


void MinMaxClusterAlignment::_updateNodes(map<double, Cluster*> &clustersSorted, NodeL &hull)
{
    for (auto &cluE : clustersSorted)
    {
        Cluster *clu = cluE.second;
        ///     * connect c.point to the closest node pn, if pn does not have already two edge1, edge2 nodes.
        for (auto &clN : clu->closestN)
        {
            // check if update allowed
            if (_updateAllowed(clN, clu->point))
            {
                // update
                if (_updateHullOrUpdateSimple(clN, clu->point, hull))
                    break;
            }
        }
    }
}

void MinMaxClusterAlignment::alignToHull(std::map<double, Cluster*> &clusters, NodeL &hull)
{
    for (const auto &cluE : clusters)
    {
        Cluster* clu = const_cast<Cluster*>(cluE.second);
        if (!clu->point->isConnectedToHull)
        {
            // search hull or end
            Node *node = clu->point.get();
            Node *prev = node->edge2;
            Node *foundNode = nullptr;
            auto foundHullI = hull.end();
            auto hullI = hull.end();
            while (true)
            {
                while (node != nullptr)
                {
                    node->isConnectedToHull = true;
                    hullI = find_if(hull.begin(), hull.end(),
                            [node] (const NodeL::value_type &val)
                    {
                        return val->point == node->point;
                    });
                    if (hullI != hull.end())
                    {
                        break;
                    }
                    node = _nextNode(node, prev);
                }
                if (node == nullptr)
                {       // end without hull node
                    if (prev != nullptr)
                    {       // because there is only one missing edge to the hull for each navel.
                        foundNode = prev;
                        if (foundHullI != hull.end())
                        {
                            // search closest hull
                            auto hullPrevI = foundHullI;
                            if (hullPrevI == hull.begin())
                                hullPrevI = hull.end();
                            --hullPrevI;
                            double distPrev = m_para.distanceFnc(foundNode->point, (*hullPrevI)->point);
                            auto hullNextI = foundHullI;
                            ++hullNextI;
                            if (hullNextI == hull.end())
                                hullNextI = hull.begin();
                            double distNext = m_para.distanceFnc(foundNode->point, (*hullNextI)->point);
                            Node *hullN = nullptr;
                            if (distPrev < distNext)
                            {
                                hullN = (*hullPrevI).get();
                            }
                            else
                            {
                                hullN = (*hullNextI).get();
                            }

                            // update
                            if (foundNode->edge1 == nullptr)
                                foundNode->edge1 = hullN;
                            else if (foundNode->edge2 == nullptr)
                                foundNode->edge2 = hullN;
                            else assert(false);

                            if (hullN->edge1 == nullptr)
                                hullN->edge1 = foundNode;
                            else if (hullN->edge2 == nullptr)
                                hullN->edge2 = foundNode;
                            else assert(false);

                            break;
                        }
                        else
                        {
                            node = clu->point.get();
                            prev = node->edge1;
                        }
                    } else assert(false);
                }
                else
                {       // end with hull node
                    // test into other direction
                    if (foundHullI != hull.end())
                        break;      // because both ends are already connected to hull.
                    foundHullI = hullI;
                    node = clu->point.get();
                    prev = node->edge1;
                }
            }
        }
    }
}

void MinMaxClusterAlignment::_insertAsNeighbour(Node *node, Cluster *clu)
{
    Node *next = const_cast<Node*>(Way::nextChild(node));
    _insertToMap(seboehDist(next, node, clu->point.get())
                , clu->neighbourEdges, tuple<Node*,Node*>(next, node));
    Node *prev = const_cast<Node*>(Way::prevChild(node));
    _insertToMap(seboehDist(prev, node, clu->point.get())
                , clu->neighbourEdges, tuple<Node*,Node*>(prev, node));
}


// dead code
double MinMaxClusterAlignment::_getBestAlignEdge(Node *partnerN, Node *cluN, Cluster *clu)
{
    double bestAlign = 0;
    for (auto &edge : clu->neighbourEdges)
    {
        Node *from = get<0>(edge.second);
        Node *to = get<1>(edge.second);
        if (from != cluN && to != cluN
            && from != partnerN && to != partnerN)
        {
            bestAlign = edge.first;
            break;
        }
    }
    return bestAlign;
}


// VoronoiClusterAlignment and MinMaxClusterAlignment
void MinMaxClusterAlignment::_collectClusterEdges(map<double, Cluster*> &clustersM)
{
//    // add hull // TODO [seb] problably _not_ necessary to be guarantied, because of _not_ missing at cluster-edges?
//    Node *prev = hull.back().get();
//    for (auto &sp : hull)
//    {
//        Node *next = sp.get();
//        double bestDist = numeric_limits<double>::max();
//        Cluster *bestClu = nullptr;
//        for (auto &cluEl : clustersM)
//        {
//            Cluster *clu = cluEl.second;
//            double dist = seboehDist(prev, next, clu->point.get());
//            if (dist < bestDist)
//            {
//                bestDist = dist;
//                bestClu = clu;
//            }
//        }
//        m_edgeToClusters[prev][next].clu1 = bestClu;
//        m_edgeToClusters[prev][next].dist1 = bestDist;
//        m_edgeToClusters[prev][next].edge.from = prev;
//        m_edgeToClusters[prev][next].edge.to = next;
//        prev = next;
//    }

    // TODO [seb] there are more than 2clu best 3, or more
    // add clusters
    for (auto &cluEl : clustersM)
    {
        Cluster* clu = cluEl.second;
//        Node *prev = clu->closestTestedPointsSorted.back();
//        for (Node *next : clu->closestTestedPointsSorted)
//        {
//            double dist = seboehDist(prev, next, clu->point.get());
//            if (dist < m_edgeToClusters[prev][next].dist1)
//            {
//                m_edgeToClusters[prev][next].clu1 = clu;
//                if (m_edgeToClusters[prev][next].dist1 == std::numeric_limits<double>::max())
//                {       // init
//                    m_edgeToClusters[prev][next].edge.from = prev;
//                    m_edgeToClusters[prev][next].edge.to = next;
//                }
//                m_edgeToClusters[prev][next].dist1 = dist;
//            }
//            else if (dist < m_edgeToClusters[prev][next].dist2)
//            {
//                m_edgeToClusters[prev][next].clu2 = clu;
//                if (m_edgeToClusters[prev][next].dist2 == std::numeric_limits<double>::max())
//                {       // init
//                    m_edgeToClusters[prev][next].edge.from = prev;
//                    m_edgeToClusters[prev][next].edge.to = next;
//                }
//                m_edgeToClusters[prev][next].dist2 = dist;
//            }

//            prev = next;
//        }

        if (clu->point->point.x() == 654)
            int i=5;

        // add the way circular and closeing triangle - because test5.
        for (auto neiEl : clu->neighbourEdges)
        {
            std::tuple<Node*,Node*> neigh = neiEl.second;
            Node *prev = std::get<0>(neigh);
            Node *next = std::get<1>(neigh);
            if (prev->point.x() == 603 && next->point.x() == 869)
                int i=5;
            if (m_edgeToClusters[prev][next].attachments.empty())
            {       // init
                m_edgeToClusters[prev][next].edge.from = prev;
                m_edgeToClusters[prev][next].edge.to = next;
                m_edgeToClusters[next][prev].edge.from = next;
                m_edgeToClusters[next][prev].edge.to = prev;
            }
            _insertToMap(neiEl.first, m_edgeToClusters[prev][next].attachments, clu);
            _insertToMap(neiEl.first, m_edgeToClusters[next][prev].attachments, clu);       // see test10-2
        }
    }
}


MinMaxClusterAlignment::ClusterEdge * MinMaxClusterAlignment::_findClusterToEdge(Node *from, Node *to)
{
    int i=0;
    for (; i < 2; ++i)
    {
        auto iterEdge = m_edgeToClusters.find(from);
        if (iterEdge != m_edgeToClusters.end())
        {
            auto iterEdge2 = iterEdge->second.find(to);
            if (iterEdge2 != iterEdge->second.end())
            {
                return &iterEdge2->second;
            }
        }
        swap(from,to);
    }
    return nullptr;
}



// VoronoiClusterAlignment and MinMaxClusterAlignment
void MinMaxClusterAlignment::_clearOpenClustersAtEdgesList(std::list<double> &ret, OpenClustersAtEdges &open, size_t s)
{
    for (const auto &key : ret)
    {
        size_t c = open.count(key);
        if (c != 1)
            int i=5;
        auto iter = open.find(key);
        if (iter != open.end())
        {
            // TODO check exact insert/drop assert(!iter->second.clu->currentRelativeMeasure.empty());
            if (!iter->second.clu->currentRelativeMeasure.empty())
                iter->second.clu->currentRelativeMeasure.erase(iter->second.clu->currentRelativeMeasure.begin());
            if (open.size() == 29)
                int i=5;
            if (open.size() == s)
                int i=5;
            open.erase(iter);
        }
    }
    ret.clear();
}

std::list<double>
MinMaxClusterAlignment::_insertCluEIfPossible(ClusterEdge *cluE, OpenClustersAtEdges &openN, Node *from, Node *to, Node *middle)
{
    std::list<double> ret;
    if (cluE != nullptr)
    {
        ///         * if not visited, insert to openN, each
        //size_t maxCount = 0;        // reduce complexity to O(2^n) HINT: is not working see test119
        // ...               ++maxCount;
        //                if (maxCount > 2) break;
        for (auto attEl : cluE->attachments)
        {
            Cluster *clu = attEl.second;
            if (clu->state != 1)
            {
                OpenClusterEdge oClu;
                oClu.edge = cluE->edge;
                oClu.clu = clu;
                // MARK:collision1 assert(seboehDist(cluE->edge.from, cluE->edge.to, clu->point.get()) == attEl.first);
                clu->currentRelativeMeasure.insert({attEl.first, true});
                ret.push_back(_insertToMap(attEl.first, openN, oClu).first->first);       // M20190111
            } else {
                // check if cancel is possible
//                if (false)
                if (attEl.first < clu->currentRelativeMeasure.begin()->first
                        && get<0>(clu->lastAssignedEdges) != from
                        && get<1>(clu->lastAssignedEdges) != from
                        && get<0>(clu->lastAssignedEdges) != to
                        && get<1>(clu->lastAssignedEdges) != to
                        && get<0>(clu->lastAssignedEdges) != middle
                        && get<1>(clu->lastAssignedEdges) != middle
                   )
                {
                    // HINT: test54
                    // it is not possible to jump back over several iterations, because
                    // it could be that the reached 'wrong' node (clu) is correct,
                    // but the current node (middle) is not correct.
                    // therefore no throw till element : throw ClusterAlignmentException("better alignment found", clu);
                    // Only throw to one level deeper.
                    throw ClusterAlignmentException("better alignment found", ret);     // throw catched in next level
                }
            }
        }
    }
    return ret;
}

// HINT: TODO [seb]: it is possible to improve search of p. By using index for open - map<Node*, open::iterator>
// because nodes, which have state=1 can not be used as next p, and the open map is sorted by dist and not by Node*.
void MinMaxClusterAlignment::_finalize()
{
    double length = 0;
    Node *prev = m_currentWay.back();
    for (Node *next : m_currentWay)
    {
        length += m_para.distanceFnc(prev->point, next->point);
        prev = next;
    }
    if (length < m_bestLength)
    {
        m_bestLength = length;
        m_bestWay = m_currentWay;
    }
}

void MinMaxClusterAlignment::_clearEvolutionaryGrowRecursion(std::list<double> &newClu1AtEdgeIters, std::list<double> &newClu2AtEdgeIters, OpenClustersAtEdges &open, WayL::iterator &wayInsertedI, Cluster *p, double &lengthChange, size_t level, size_t iterN, size_t s)
{
//    if (m_lastLevel != level)
//    {
//        m_lastIterN = 0;
//        m_lastLevel = level;
//    }
//    if (m_lastIterN < iterN)
//    {
        _clearOpenClustersAtEdgesList(newClu1AtEdgeIters, open, s);
        _clearOpenClustersAtEdgesList(newClu2AtEdgeIters, open, s);

        if (wayInsertedI != m_currentWay.end())
        {
            m_currentLength -= lengthChange;
            m_currentWay.erase(wayInsertedI);
        }
        p->lastAssignedEdges = make_tuple(nullptr, nullptr);
        p->state = 0;

        m_lastIterN = iterN;
//    }
}

bool MinMaxClusterAlignment::_searchBestPathByConvexHullAlignment(OpenClustersAtEdges &open, size_t level)
{
    // Version 2
//    ///     * copy map to openN and delete the element p
//    OpenClustersAtEdges openN = open;
//    auto openEI = openN.begin();
//    Cluster *p = nullptr;
//    ClusterEdge *cluEdge = nullptr;
//    while (p == nullptr)
//    {
//        cluEdge = openEI->second;

//        if (cluEdge->clu1->state != 1)
//            p = cluEdge->clu1;
//        else if (cluEdge->clu2 != nullptr && cluEdge->clu2->state != 1)
//            p = cluEdge->clu2;
//        // TODO [seb] what is with n-closest point?

//        openEI = openN.erase(openEI);       // first is unique because M20190111
//        if (openEI == openN.end())
//            break;
//    }
//    if (p == nullptr)
//        _searchBestPathByConvexHullAlignment(openN);        // because, final cancel-criteria

    /// * go through all elements p of open
    // version 1
    double lengthChange;
    size_t iterN = 0;
    size_t openS = open.size();
    double openKey = open.begin()->first;
    for (auto openEI = open.begin();
         openEI != open.end();
         openEI = ++(open.find(openKey)) )
    {
        assert(open.size() >= openS);

        ///     * copy map to openN and delete the element p
        OpenClusterEdge oCluEdge;
        Cluster *p = nullptr;
        while (p == nullptr)
        {
            oCluEdge = openEI->second;

            if (oCluEdge.clu->state != 1)
            {
                p = oCluEdge.clu;
                // HINT [seb] what is with n-closest point?
                // We have to check all, because test54
            } else
            {
                // HINT: openEI = open.erase(openEI);       // first is unique because M20190111
                ++openEI;
                if (openEI == open.end())
                    break;
            }
        }

        if (openEI != open.end())
        {
            ///     * mark p as visited
            openKey = openEI->first;
            Node *from = oCluEdge.edge.from;
            Node *to = oCluEdge.edge.to;

            if (level == 3 && p->point->point.x() == 430)
                int i=5;
            if (level == 3)
                int i=5;

            auto wayInsertedI = m_currentWay.end();
            ///     * add p to way
            if (p->point->point.x() == 240)
                int i=5;
            if (p->point->point.x() == 510)
            {
                if ((from->point.x() == 240 && to->point.x() == 80)|| (from->point.x() == 80 && to->point.x() == 240))
                    int i=5;
            }
            if (p->point->point.x() == 240)
            {
                if ((from->point.x() == 40 && to->point.x() == 560)|| (from->point.x() == 560 && to->point.x() == 40))
                    int i=5;
            }
            if (p->point->point.x() == 340)
            {
                if ((from->point.x() == 160 && to->point.x() == 320)|| (from->point.x() == 320 && to->point.x() == 160))
                    int i=5;
            }

            auto wayI = --m_currentWay.end();       // trick because edge from 'last' to 'first' node.
            if (*wayI != from)
            {
                wayI = m_currentWay.begin();
                for (; wayI != m_currentWay.end(); ++wayI)
                {
                        // both edges form/to below, because safety.
                    if (*wayI == from)
                    {
                        ++wayI;
                        if (wayI == m_currentWay.end())
                            wayI = m_currentWay.begin();
                        if (*wayI == to)
                            break;
                    }
                }
            }
            else
                wayI = m_currentWay.begin();
            if (wayI != m_currentWay.end())
            {
                auto prevWayI = wayI;
                if (prevWayI == m_currentWay.begin())
                    prevWayI = m_currentWay.end();
                --prevWayI;
                lengthChange = - m_para.distanceFnc((*prevWayI)->point, (*wayI)->point);
                lengthChange += m_para.distanceFnc((*prevWayI)->point, p->point->point);
                lengthChange += m_para.distanceFnc(p->point->point, (*wayI)->point);

                // pruning?
                if (m_currentLength < m_bestLength)
                {
                    wayInsertedI = m_currentWay.insert(wayI, p->point.get());
                    m_currentLength += lengthChange;
                } else
                    continue;       // pruning
            } else
                continue;       // because the cluster can have opposit directions
                                // see test4 clu-center 80 with edge 154 to 215.

            /// recursion
            list<double> newClu1AtEdgeIters, newClu2AtEdgeIters;
            ++iterN;

            if (m_currentWay.size() == m_tspSize)
            {   /// stop recursion
                /// * cancel criteria
                ///     * add p to way
                ///     * check way-length and update way
                _finalize();
            }
            else
            {
                p->state = 1;
                p->lastAssignedEdges = make_tuple(from, to);

                ///     * find clu1 clu2 of the two edges between cluEdge.edge and p.point
                ///     * check if clu1 or clu2 ar not visited already
                // TODO [seb] there are more than two clu possible at one edge

                if (p->point->point.x() == 270
                        && p->point->point.y() == 220)
                    int i=5;

                try {
                    newClu1AtEdgeIters = _insertCluEIfPossible(
                                _findClusterToEdge(
                                    from, p->point.get())
                                , open, from, to, p->point.get());
                    newClu2AtEdgeIters = _insertCluEIfPossible(
                                _findClusterToEdge(
                                    p->point.get(), to)
                                , open, from, to, p->point.get());

                    ///     * start recursion again.
                    _searchBestPathByConvexHullAlignment(open, level +1);

                } catch (const ClusterAlignmentException &e)
                {
                    if (!m_isLogFaultDetected)
                    {
                        m_isLogFaultDetected = true;
                        cerr << "\nAlternative detected" << endl;
                    }
                    assert(newClu1AtEdgeIters.size() == 0 || newClu2AtEdgeIters.size() == 0);
                    if (newClu1AtEdgeIters.size() > 0)
                        newClu2AtEdgeIters = e._ret;
                    else
                        newClu1AtEdgeIters = e._ret;
                }
            }

            ///     * clear for recursion
            _clearEvolutionaryGrowRecursion(newClu1AtEdgeIters, newClu2AtEdgeIters, open, wayInsertedI, p, lengthChange, level, iterN, openS);
        }
        else
            break;      // end reached

    }

    return false; //openEI == openN.end();
}

void MinMaxClusterAlignment::_runEvolutionaryGrow(NodeL &hull)
{
    m_bestLength = std::numeric_limits<double>::max();
    m_bestWay.clear();
    m_currentLength = 0;
    m_currentWay.clear();

    // * create openEdges list with alignments to hull
    OpenClustersAtEdges open;

    Node *prev = hull.back().get();
    for (auto sp : hull)
    {
        Node *next = sp.get();
        m_currentWay.push_back(next);
        if (m_currentWay.size() > 1)
            m_currentLength += m_para.distanceFnc((*(--(--m_currentWay.end())))->point, m_currentWay.back()->point);

        _insertCluEIfPossible(_findClusterToEdge(prev, next), open, nullptr, nullptr, nullptr);        // see test46

        prev = next;
    }
    m_currentLength += m_para.distanceFnc(m_currentWay.back()->point, m_currentWay.front()->point);

    // * start recursive search (similar to A*-search)
    try {
        _searchBestPathByConvexHullAlignment(open, 1);
    } catch (...)
    {
        std::cerr << "Process canceled" << std::endl;
    }
}



MinMaxClusterAlignment::NodeL MinMaxClusterAlignment::determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way)
{
    assert(way->size() > 1);

    /// * create structure
    map<double, Cluster*> clustersM;
    int i = 0;
    for (auto &cluE : clustersA)
    {
        Cluster *clu = cluE.get();

        /// * pre-calc cluster alignments at neighbours
        auto neighNextI = clu->closestTestedPointsSorted.begin();
        for (Node* &neigh : clu->closestTestedPointsSorted)
        {
            ++neighNextI;
            if (neighNextI == clu->closestTestedPointsSorted.end())
                neighNextI = clu->closestTestedPointsSorted.begin();
            double dist = seboehDist(neigh, *neighNextI, clu->point.get());
            _insertToMap(dist, clu->neighbourEdges
                         , std::tuple<Node*,Node*>(neigh, *neighNextI));
        }

        //clustersM.insert({clu->area, clu});
        clustersM.insert({++i, clu});
    }
    size_t growPos = 0;
    for (auto &cluE : clustersM)
    {
        cluE.second->growPosition = ++growPos;
    }

    for (auto &cluE : clustersM)
    {
        Cluster *clu = cluE.second;
        for (const Node * node : clu->closestTestedPointsSorted)
        {
            auto cluI = find_if(clustersM.begin(), clustersM.end(),
                                [node] (const map<double, Cluster*>::value_type &val)
            {
                return node == val.second->point.get();
            });
            if (cluI != clustersM.end())
                clu->closestSortedNeighbours.push_back(cluI->second);
        }
    }

    /// * create structure
    _collectClusterEdges(clustersM);

//    for (const auto &el : m_edgeToClusters)
//    {
//        if (el.first->point.x() == 240)
//        {
//            for (const auto &el2 : el.second)
//            {
//                if (el2.first->point.x() == 560)
//                {
//                    for (const auto &el3 : el2.second.attachments)
//                    {
//                        if (el3.second->point->point.x() == 510)
//                        {
//                            int i=5;
//                        }
//                    }

//                }
//            }
//        }
//    }

    /// * align to best edges starting from hull
    if (!clustersM.empty())
    {
        _runEvolutionaryGrow(hull);
    } else {
        for (auto sp : hull)
            m_bestWay.push_back(sp.get());
    }

    /// * create result-struct
    NodeL ret;
    for (Node *node : m_bestWay)
        ret.push_back(make_shared<Node>(*node));

    return ret;
}

void MinMaxClusterAlignment::buildTree(DT::DTNode::NodeType node, float level, TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    m_resultLength = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    incProgress(progressPercentage);

    // * intern Input representation
    auto input = createInputRepresentation(openPointsBasic);
    m_tspSize = input.size();

    // * determine hull
    auto res = determineHull(input);
    auto hull = get<0>(res);
    Way *way = get<1>(res);

    // * set pruning criterias to Node
    determineGuarantiedAlignment(input, hull);

    // * determine Voronoi cluster
    auto clusters = determineClusters(input, hull);

    // * build final path
    auto finalWay = determineWay(clusters, hull, way);

//    if (m_isCanceled)
//    {
        // set any way - like the input, because we want valid m_resultLength values at further processing.
//        m_shortestWay.clear();
//        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
//             openNodeIter != openPointsBasic->end(); ++openNodeIter)
//        {
//            m_shortestWay.push_back(**openNodeIter);
//        }
//        m_mutex.lock();
//        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
//        m_mutex.unlock();
//    }

    /// copy result to final output
    for (auto pointI = finalWay.begin();
         pointI != finalWay.end(); ++pointI)
    {
        TSPWay w;
        w.push_back((*pointI)->point);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial ways - drop this tree of ways.
        (*newChildIter)->parent(node);
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}
