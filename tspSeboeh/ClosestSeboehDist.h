#pragma once

#include "dll.h"

#include <TspAlgoCommon.h>

namespace TSP {

class EXTERN ClosestSeboehDistAlgorithm : public TspAlgoCommon
{
public:
    ClosestSeboehDistAlgorithm();
    virtual ~ClosestSeboehDistAlgorithm();

protected:
    void run() override;

protected:      // protected, cause of tests.
    struct ClosestEdges {
        Point from;
        Point to;
        Point insert;
    };
    typedef std::list<ClosestEdges> ClosestSeboehDistsType;


    double seboehDist(const Point &p1, const Point &p2, const Point &pNew);

    ClosestSeboehDistsType determineClosestNeighbours(const InputCitiesType &citiesInput);
    void writeResult(ClosestSeboehDistsType &closestDistEdges);
};

}
