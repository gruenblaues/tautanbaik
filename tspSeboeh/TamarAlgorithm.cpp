#include "TamarAlgorithm.h"

#include <bspTreeVersion/BspTreeSarah.h>
#include <limits>
#include <iostream>
#include <assert.h>
#include <vector>
#include <memory>
//#include <boost/crc.hpp>
#include <algorithm>
#include <map>
#include <tuple>
//#include <math.h>       // because of PI

#include "TamarDebugging.h"
#include "TamarAlgorithmHelper.h"
#include "SortedMerge.h"
#include "exponentsearch.h"
#include "ExcludingMerge.h"

using namespace TSP;

using namespace std;

TamarAlgorithm::TamarAlgorithm() : TspAlgoCommon()
{
}

TamarAlgorithm::~TamarAlgorithm()
{
}

bsp::BspTreeSarah TamarAlgorithm::createBSPTree(InputCitiesPointerType &cityPoints)
{
    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cityPoints);
    m_progressMax += bspTree.getCountOfNodes();
    bspTree.root()->nextChildFnc = oko::OlavList::nextChild;
    bspTree.root()->prevChildFnc = oko::OlavList::prevChild;

    return bspTree;
}

void TamarAlgorithm::run()
{
    if (m_cities == nullptr || m_resultWay == nullptr)
        return;     // method init() should be called before run().

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;

    m_mutex.lock();
    int state = m_indicators.state();
    state &= ~Indicators::States::ISNULL;
    state |= Indicators::States::ISRUNNING;
    m_indicators.state(state);
    m_mutex.unlock();

    /// 0. some figures
    ///////////////////////////
    // it is not forseen to have multiple same points
    std::map<double, double> reduceSamePointsMap;
    auto iter = reduceSamePointsMap.end();

    InputCitiesPointerType cityPoints;
    for (InputCitiesType::iterator cityI=m_cities->begin();
         cityI != m_cities->end(); ++cityI)
    {
        if ((iter = reduceSamePointsMap.find(cityI->x())) != reduceSamePointsMap.end())
        {
            if (iter->second == cityI->y())
                continue;
        }
        cityPoints.push_back(new Point(*cityI));
        reduceSamePointsMap.insert({cityI->x(), cityI->y()});
    }

    for (const auto &points : m_blockeEdgesInput)
    {
        m_blockedEdges[blockedEdgesCRC(points.first)].push_back(points.second);
    }

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = createBSPTree(cityPoints);

    /// 2. create points order - merge
    //////////////////////////
    auto root = bspTree.root();
    if (root != nullptr) {
        Way * way = newConqueredWay(root);
        if (way != nullptr)
        {
            m_resultWay->clear();
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (Way::iterator valI=way->begin();
                 !valI.end(); )
            {
                m_resultWay->push_back(Point(valI.nextChild()->point));
            }
// TODO [seboeh] check if possible
//            way->clear();
//            delete way;
        }
    }

    // clear cityPoints.
// TODO [seboeh] check if possible
    //    for (const auto &point : cityPoints)
//        delete point;
//    cityPoints.clear();

    m_mutex.lock();
    state = m_indicators.state();
    state &= ~Indicators::States::ISRUNNING;
    state |= Indicators::States::ISFINISHED;
    m_indicators.state(state);
    m_indicators.progress(1.0f);
    m_mutex.unlock();

    /// some postprocess debugging
    debugBspPrint();
}

TSP::Point TamarAlgorithm::calculateMeanPointOfWayCenter(Way *way1, Way *way2)
{
    double x = 0;
    double y = 0;
    size_t count = 0;
    auto iter = way1->begin();
    while (!iter.end())
    {
        auto node = iter.nextChild();
        x += node->point.x();
        y += node->point.y();
        ++count;
    }
    iter = way2->begin();
    while (!iter.end())
    {
        auto node = iter.nextChild();
        x += node->point.x();
        y += node->point.y();
        ++count;
    }
    return TSP::Point(x/count, y/count);
}

TamarAlgorithm::Way *TamarAlgorithm::newConqueredWay(bsp::BspNode *node)
{
	if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        Way *way = new Way();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
//        way->setOrigin(node);
        return way;
    }
    Way * wayUpper;
    Way * wayLower;
    bsp::BspNode *tmpUpperLeftOrigin = nullptr;
    bsp::BspNode *tmpUpperRightOrigin = nullptr;
    bsp::BspNode::BspEdgesType tmpUpperLeftEdges;
    bsp::BspNode::BspEdgesType tmpUpperRightEdges;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        Way * wayUpperLeft = newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            Way * wayUpperRight = newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x()/2.0, pUpperCenter.y()/2.0);

            wayUpperRight->setOrigin(node);
            wayUpperLeft->setOrigin(node);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            mergeWays(node, wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        } else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    } else {
        if (node->m_upperRight) {
            wayUpper = newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        } else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        Way * wayLowerLeft = newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            Way * wayLowerRight = newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x()/2.0, pLowerCenter.y()/2.0);

            /// HINT: the setting of origin have to be after mergeWays, because the node, as origin, should not have
            /// the actual edges in it, because we use different BspNodes in upper... and lower...
            /// See test51.png and basis2.png
            // switch off upper's in BspNode = origin.
            tmpUpperLeftOrigin = node->m_upperLeft;
            // node->m_upperLeft, should not lead to seg-fault, because it should not taken into account in mergeWays.
            // Because mergeWays is doing action only below center.
            node->m_upperLeft = nullptr;
            tmpUpperRightOrigin = node->m_upperRight;
            node->m_upperRight = nullptr;
            tmpUpperLeftEdges = node->m_edgesUpperLeft;
            node->m_edgesUpperLeft.clear();
            tmpUpperRightEdges = node->m_edgesUpperRight;
            node->m_edgesUpperRight.clear();

            wayLowerRight->setOrigin(node);
            wayLowerLeft->setOrigin(node);

            mergeWays(node, wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        } else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    } else {
        if (node->m_lowerRight) {
            wayLower = newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        } else {
            wayLower = nullptr;
        }
    }

    incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {

        // reinsert tmpUpper...
        node->merge(tmpUpperLeftOrigin);
        // emplace_back, for the case, that there was edges inserted in edges list at mergeWay for lowerWays
        // , like the convex hull-edges at merge!
        node->m_edgesUpperLeft.insert(node->m_edgesUpperLeft.end()
                                      , tmpUpperLeftEdges.begin()
                                      , tmpUpperLeftEdges.end());
        node->merge(tmpUpperRightOrigin);
        node->m_edgesUpperRight.insert(node->m_edgesUpperRight.end()
                                       , tmpUpperRightEdges.begin()
                                       , tmpUpperRightEdges.end());

        wayUpper->setOrigin(node);      // necessary for case, quader has only one leaf.
        wayLower->setOrigin(node);

        // TODO [seboeh] delete node from parameter list below, because node already in way.
        // HINT: test37 needs master center node->m_center.
        mergeWays(node, wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    } else if (wayUpper) {
        return wayUpper;
    } else if (wayLower) {
        return wayLower;
    } else {
        return nullptr;
    }
}

void TamarAlgorithm::insertNewNodeToEdge(Way* way1, ClosestEdges &cx)
{
    /// HINT: prev and next nodes of xN are in nodeList.
    /// TODO [xeboeh] delete childNode - memory leak here.
    // we detect cases, where it is no child node - selectNavel insert innerNodes size 1.
//    TODO .... delete if (cx.insertNode->isChildNode)
//        cx.insertNode->child->node = cx.insertNode;     // update old node --> delete node pointer.
    cx.insertNode->child = nullptr;     // HINT: insertChild expect child == nullptr if new. Else we assume not new!
    cx.insertNode->isChildNode = false;            // will be re-set on insertChild().

    /// 2. Insert xN;
    if (cx.fromNode->child == nullptr || cx.fromNode->isChildNode == false)     // isChildNode == false, for test61
    {       // convex-hull node
        way1->insertChild(Way::iterator(cx.fromNode), cx.insertNode);
    } else {
        way1->insertChildInNext(cx.fromNode, cx.insertNode);
    }
    cx.insertNode->isDeleted = false;
}

void TamarAlgorithm::collectAllCloseInnerNodes(std::list<std::shared_ptr<ClosestEdges>> &innerPoints, Way &way)
{
    if (innerPoints.empty())
        return;

    std::list<Node*> removeNodeList;

    // HINT: because we want only iterate through the current innerPoints and append at innerPoints additionally,
    // we copy innerPoints here!
    auto currentInnerPoints = innerPoints;
    const Node * prevNodeA = Way::prevChild(currentInnerPoints.front()->insertNode);
    const Node * nextNodeA = nullptr;
    const Node *insertNode = nullptr;
    for (std::list<std::shared_ptr<ClosestEdges>>::iterator ipI = currentInnerPoints.begin();
         ipI != currentInnerPoints.end(); ++ipI)
    {
        insertNode = (*ipI)->insertNode;
        nextNodeA = Way::nextChild(insertNode);
        if (nextNodeA == nullptr || prevNodeA == nullptr)
        {
            std::cerr << "Warning: The structure of the way at collectAllCloseInnerNodes() is not correct/stabil." << std::endl
                      << " running further without include of closer nodes. Could lead to misresults." << std::endl;
            continue;       // just omit this include, because this structure shall not happen.
        }
        double distPrev = m_para.distanceFnc(prevNodeA->point, insertNode->point);
        double distNext = m_para.distanceFnc(nextNodeA->point, insertNode->point);
        // radius is max, because of test80 - TODO [seboeh] improve, because very slow - using min & nothing else, would be better.
        double radiusMax = std::min(distPrev, distNext);
        // TODO [seb]: find better radius methods; double radiusMax = (std::max)(distPrev, distNext) - 0.25 * std::min(distPrev, distNext);

        Way::iterator xI = way.begin();
        Node* currentNode = nullptr;
        const Node *prevNodeB = Way::prevChild(&xI);
        while (!xI.end())
        {
            currentNode = xI.nextChild();
            if (!currentNode->isChildNode)
                continue;       // only childNodes are new innerPoints.
            auto removeI = std::find(removeNodeList.begin(), removeNodeList.end()
                                    , currentNode);
            if (removeI != removeNodeList.end())
                continue;       // because already deleted.
            auto findI = std::find_if(innerPoints.begin(), innerPoints.end(),
                                      [currentNode](const std::shared_ptr<ClosestEdges> &el)
            {
                return el->insertNode == currentNode;
            });
            if (findI != innerPoints.end())
               continue;

            double dist = m_para.distanceFnc(insertNode->point, currentNode->point);

            // see test27
            auto xTemp = xI;
            double distPrev;
            if (prevNodeB == nullptr)
                distPrev = std::numeric_limits<double>::max();
            else
                distPrev = m_para.distanceFnc(currentNode->point, prevNodeB->point);

            xTemp = xI;
            const Node *nextNodeB = xTemp.nextChild();
            double distNext;
            if (nextNodeB == nullptr)
                distNext = std::numeric_limits<double>::max();
            else
                distNext = m_para.distanceFnc(currentNode->point, nextNodeB->point);

            // max() because test73 - the old inner-node on the way, can implicite new alignment
            // the new inserted inner-node is already on best position.
            // Only for the old nodes, there can be the possibility to align better.
            double distCurr = (std::max)(distPrev, distNext);

            // test80 - alignment of points together with option on a new edge - this edge shall be better than current edge, as assumtion
            double distSebB1 = seboehDist(insertNode, nextNodeA, currentNode);
            double distSebB2 = seboehDist(insertNode, prevNodeA, currentNode);
            double distSebBNew = std::min(distSebB1, distSebB2);
            if (distSebBNew == 0)
                distSebBNew = std::numeric_limits<double>::max();
            double distSebB = seboehDist(prevNodeB, nextNodeB, currentNode);

            if (dist < (radiusMax + distCurr)
                    || distSebBNew < distSebB)
            {       // include to innerPoint list.
                // HINT: currentNode have to be child node only - see if-continue above.
                // Therefore eraseNode() in next loop outside.

                auto insertNodeNewI = std::find_if(innerPoints.begin(), innerPoints.end(),
                                                   [ipI] (const std::shared_ptr<ClosestEdges> &val)
                {
                    return (*ipI)->insertNode == val->insertNode;
                });
                if (insertNodeNewI == innerPoints.end())
                {
                    std::cerr << "ERROR: corresponding innerPoint not found in collectAllCloseInnerPoints()." << std::endl;
                    continue;
                }
                // HINT: it is necessary to include the new point
                // here inside the correct position already.
                // see test68
                auto iterNext = innerPoints.begin();
                double distBestIn = std::numeric_limits<double>::max();
                auto iterBestIn = innerPoints.begin();
                for (auto iter = innerPoints.begin();
                     iter != innerPoints.end(); ++iter)
                {
                    ++iterNext;
                    if (iterNext == innerPoints.end())
                        iterNext = innerPoints.begin();
                    double distIn = seboehDist((*iter)->insertNode, (*iterNext)->insertNode, currentNode);
                    if (distIn < distBestIn)
                    {
                        distBestIn = distIn;
                        iterBestIn = iter;
                    }
                }
                ++iterBestIn;
                innerPoints.insert(iterBestIn, std::make_shared<ClosestEdges>(currentNode));

                // save for later remove
                removeNodeList.push_back(currentNode);
            }
            prevNodeB = currentNode;
        }
        prevNodeB = insertNode;
    }       // currentInnerPoints - the original ones

    // HINT: Similar to erase in mergeCascada(), although it is for test10-2, test12 and test31 necessary.
    //way.removeEdge(ceP->insertNode);
    removeNodeList.reverse();
    for (const auto &node : removeNodeList)
        removeNodeFromPolygon(&way, node);

}


double TamarAlgorithm::localDecision(double area, double detour, double distCenter)
{
//    return (std::pow(area, 0.5)  * std::pow(detour,2) * std::pow(distCenter,1))
//        +  (std::pow(detour,0) * std::pow(distCenter,0))
//        +  std::pow(detour,2);
//    return detour * std::pow(distCenter,1);
    return detour;      // for test63
}

long long TamarAlgorithm::blockedEdgesCRC(const Point &p1)
{
    float buffer[2];
    buffer[0] = p1.x();
    buffer[1] = p1.y();
    return static_cast<long long>(buffer[0]);
}

double TamarAlgorithm::seboehDist(const Point &p1, const Point &p2, const Point &pNew)
{
    auto blockedIter = m_blockedEdges.find(blockedEdgesCRC(p1));
    if (blockedIter != m_blockedEdges.end())
    {
        if (blockedIter->second.front() == p2)
        {
            return std::numeric_limits<double>::max();
        }
        if (blockedIter->second.back() == p2)
        {
            return std::numeric_limits<double>::max();
        }
    }

	double detour = m_para.distanceFnc(p2, pNew)
            + m_para.distanceFnc(p1, pNew)
            - m_para.distanceFnc(p1, p2);

    return detour;
//    Point center((p1.x()+p2.x())/2.0, (p1.y()+p2.y())/2.0);
//    double m = m_para.distanceFnc(center, pNew);
//    return localDecision(calcAreaOfTriangle(p1, p2, pNew), detour, m);
//    return deviation * m;
}

double TamarAlgorithm::seboehDist(const ClosestEdges &ce)
{
    return seboehDist(ce.fromNode->point, ce.toNode->point, ce.insertNode->point);
}

double TamarAlgorithm::seboehDist(const Node *from, const Node *to, const Node *insert)
{
    return seboehDist(from->point, to->point, insert->point);
}

double TamarAlgorithm::calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double TamarAlgorithm::seboehDistWithGravity(Node *from, Node* to, Node *insert, Point &center)
{
	ClosestEdges ce(insert);
	ce.fromNode = from;
	ce.toNode = to;
	return seboehDistWithGravity(ce, center);
}

double TamarAlgorithm::seboehDistWithGravity(const ClosestEdges &ce, Point &center)
{
	double m = m_para.distanceFnc(ce.insertNode->point, center);
	double detour= m_para.distanceFnc(ce.fromNode->point, ce.insertNode->point)
			+ m_para.distanceFnc(ce.insertNode->point, ce.toNode->point)
			- m_para.distanceFnc(ce.fromNode->point, ce.toNode->point);


    return localDecision(calcAreaOfTriangle(ce.fromNode->point, ce.toNode->point, ce.insertNode->point)
                         , detour, m);
}

Point TamarAlgorithm::calcCenter(const NavelsZippedType &ceList)
{
    /* new
    auto start = ceList.front().fromNode->point;
    auto end = ceList.front().toNode->point;
    Point center((start.x()+end.x())/2, (start.y()+end.y())/2);

    return center;
*/
// /* old
    Point center(ceList.front().fromNode->point);
	center += ceList.back().toNode->point;
	for (const auto &ce : ceList)
	{
		center += ce.insertNode->point;
	}
	size_t n = ceList.size() + 2;
	return Point(center.x()/n, center.y()/n);
 // */
}

// FIXME: do not calculate the distance again, but use new variable m_forwardNavelsDist and m_backwardNavelsDist to adjust.
double TamarAlgorithm::correctDist(NavelsZippedType &navelRoot)
{
	double dist = 0;
	for (const auto &ce : navelRoot)
	{
//        dist += seboehDist(ce);     // TODO [seboeh] use ce.dist - it's faster!
		if (ce.insertNode->candidateState != Node::CandidateState::CANDIDATE)
			dist += ce.dist;            // TODO ...improve move NavelsZippedType into container and keep totalDist updated.
	}
	return dist;
}

double TamarAlgorithm::correctDistTMP_DELETE(const NavelsZippedType &navelRoot)
{
	if (navelRoot.empty())
		return 0;
	Point center = calcCenter(navelRoot);
	double dist = 0;
	for (const auto &ce : navelRoot)
	{
		if (ce.insertNode->candidateState != Node::CandidateState::CANDIDATE)
			dist += seboehDistWithGravity(ce, center);     // TODO [seboeh] use ce.dist - it's faster!
	}
	return dist;
}

double TamarAlgorithm::correctDistTMP_DELETE_WITH_CANDIDATE(const NavelsZippedType &navelRoot)
{
	if (navelRoot.empty())
		return 0;
	Point center = calcCenter(navelRoot);
	double dist = 0;
	Node *prevNode = navelRoot.front().fromNode;
	for (const auto &ce : navelRoot)
	{
		// HINT: it is using candidate here also.
		dist += seboehDistWithGravity(prevNode, ce.toNode, ce.insertNode, center);
		prevNode = ce.insertNode;
	}
	return dist;
}


// swift next inner nodes backward. Ce should not be part of navelRoot.
// return navelRoot.end() and dist 0 if navelRoot is empty.
/// @return iterator after best edge for ce.insertNode.
// TODO [seboeh] improve: not only check to fromNode.
std::tuple<TamarAlgorithm::NavelsZippedType::iterator, double>
TamarAlgorithm::findBestSingleSearch(NavelsMapType::mapped_type &navelRoot, const ClosestEdges &ce)
{
	if (navelRoot.empty())
		return std::tuple<TamarAlgorithm::NavelsZippedType::iterator, double>(navelRoot.end(), std::numeric_limits<double>::max());

	double bestDist = std::numeric_limits<double>::max();
	Node *prevNode = navelRoot.front().fromNode;
	// HINT: One edge (last prevNode to navelRoot.front().toNode) is missing
	// because we do not need to check, it is on the opposite site of ce.insertNode.
    Point center = calcCenter(navelRoot);
    size_t n = navelRoot.size()+2;
    center = Point(center.x()*n, center.y()*n);
    center += ce.insertNode->point;
    ++n;
    center = Point(center.x()/n, center.y()/n);
	NavelsZippedType::iterator bestI = navelRoot.end();
	for (auto nextCeI = navelRoot.begin();
		 nextCeI != navelRoot.end(); ++nextCeI)
	{
		if (nextCeI->insertNode->candidateState == Node::CandidateState::CANDIDATE)
			continue;
        assert(nextCeI->insertNode != ce.insertNode);

        /// HINT: Due to test63, it is necessary to calculate the area of triangular also into seboehDist.
//		double distNext = seboehDist(prevNode, nextCeI->insertNode, ce.insertNode);
		double distNext = seboehDistWithGravity(prevNode, nextCeI->insertNode, ce.insertNode, center);
        if (distNext < bestDist)
		{
            bestDist = distNext;
			bestI = nextCeI;
		}
		prevNode = nextCeI->insertNode;
	}
//	double dist = seboehDist(prevNode, navelRoot.back().toNode, ce.insertNode);
	double dist = seboehDistWithGravity(prevNode, navelRoot.back().toNode, ce.insertNode, center);
    if (dist < bestDist)
	{
        return std::tuple<TamarAlgorithm::NavelsZippedType::iterator, double>(navelRoot.end(), dist);
	} else {
		return std::tuple<TamarAlgorithm::NavelsZippedType::iterator, double>(bestI, bestDist);
	}
}

Point
TamarAlgorithm::testPointsInOrder(NavelsZippedType::iterator thirdPointI, Node *secondPointN, Node *firstPointN)
{
    auto pointBase = firstPointN->point;
    auto pointNext = secondPointN->point;
    auto pointNNext = thirdPointI->insertNode->point;
    auto vect1 = pointNext - pointBase;
    auto vect2 = pointNNext - pointBase;
    auto vectRes = vect2 - vect1;

    return vectRes;
}

std::tuple<TamarAlgorithm::NavelsZippedType::iterator
, TamarAlgorithm::NavelsZippedType::iterator>
TamarAlgorithm::findFromTo(Node *insert, NavelsZippedType &ceList)
{
    if (ceList.empty())
        return std::make_tuple(ceList.end(), ceList.end());

    Node *rootFrom = ceList.front().fromNode;
    Node *rootTo = ceList.back().toNode;
    double startDist = seboehDist(rootFrom, rootTo, insert);

    // find to
    NavelsZippedType::iterator toI = ceList.end();
    double bestDist = startDist;
    for (auto ceI = ceList.begin();
         ceI != ceList.end(); ++ceI)
    {
        double dist = seboehDist(rootFrom, ceI->insertNode, insert);
        if (dist < bestDist)
        {
            bestDist = dist;
            toI = ceI;
        }
    }

    // find from
    bool isCrossed = false;
    bool bestIsCrossed = false;
    NavelsZippedType::iterator fromI = ceList.end();
    bestDist = startDist;
    for (auto ceI = ceList.begin();
         ceI != ceList.end(); ++ceI)
    {
        if (ceI == toI)
            isCrossed = true;
        double dist = seboehDist(ceI->insertNode, rootTo, insert);
        if (dist < bestDist)
        {
            bestDist = dist;
            fromI = ceI;
            bestIsCrossed = isCrossed;
        }
    }

    /// secure check on point before toI. See test63.
    if (toI != ceList.end())
    {
        auto nextI = toI; ++nextI;
        Node *nextN;
        if (nextI == ceList.end())
        {
            nextN = toI->toNode;
        } else {
            nextN = nextI->insertNode;
        }

        // check if toI is before
        auto vectRes = testPointsInOrder(toI, insert, rootFrom);
        if (
                vectRes.x() < 0
                ||
                vectRes.y() < 0
            )
        {
            if (fromI != ceList.end())
            {
                auto vectResFrom = testPointsInOrder(fromI, insert, rootFrom);
                if (
                        vectRes.x() > 0
                        ||
                        vectRes.y() > 0
                    )
                {
                    bestIsCrossed = true;
                } else {
                    auto vectResFromTo = testPointsInOrder(fromI, toI->insertNode, rootFrom);
                    if (
                            vectResFromTo.x() < 0
                            ||
                            vectResFromTo.y() < 0
                        )
                    {
                        toI = nextI;
                    } else {
                        bestIsCrossed = true;       // because, fromI is after toI.
                    }
                }
            } else {
                fromI = toI;
                toI = ceList.end();
            }
        }
    }
    // TODO [seboeh] same as above could be helpful for fromI too.

    if (bestIsCrossed)
    {
        std::swap(fromI, toI);
    }

    /// check if points are in correct order according to ceList.
    if (fromI != ceList.end() && toI != ceList.end())
    {
        bool isFromReached = false;
        for (const auto &ce : ceList)
        {
            if (ce.insertNode == fromI->insertNode)
                isFromReached = true;
            if (ce.insertNode == toI->insertNode)
                break;
        }
        if (!isFromReached)
            std::swap(fromI, toI);
    }

    return std::make_tuple(fromI, toI);
}

TamarAlgorithm::ClosestEdges TamarAlgorithm::updateBestResort(Node *fromNode, Node *toNode, Node *insert, NavelsZippedType::iterator &iter, NavelsZippedType &ceList, ClosestEdges *newCeP)
{
	ClosestEdges ce(insert);
    if (iter == ceList.end())
    {
        if (ceList.empty())
        {
            ce.fromNode = fromNode;
            ce.toNode = toNode;		// TODO [seboeh] replace toNode with getNextChild().
        } else {
            ce.fromNode = ceList.back().insertNode;
            ce.toNode = toNode;
        }
    } else {		// normal
        ce.fromNode = iter->fromNode;
        ce.toNode = iter->toNode;        // toNode is Root-Anchor-toNode everytime.
        if (newCeP == nullptr
				&& insert->candidateState == Node::CandidateState::ELECTED)
            iter->fromNode = ce.insertNode;      // insert Candidate in correct list, but does not set the fromNode.
        if (newCeP == nullptr
                && insert->candidateState == Node::CandidateState::ELECTED)
			iter->dist = seboehDist(*iter);
    }
    ce.dist = seboehDist(ce);
    if (newCeP != nullptr)
        *newCeP = ce;
    return ce;
}

bool TamarAlgorithm::isNormalVectorLeft(const Point &from, const Point &to, const Point &insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x())/2
                    , (from.y() + to.y())/2);

    Point normalVectLeft(meanPoint.x() - straight.y()
                         , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
                          , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}

TamarAlgorithm::NavelsZippedType::iterator TamarAlgorithm::insertBestResortedPos(Node *from, Node *to, Node *insert, NavelsZippedType &ceList, int level)
{
    return TSPHelper::insertBestResortedPos(from, to, insert, ceList, level);
}

TamarAlgorithm::NavelsZippedType::iterator
TamarAlgorithm::findBestSingleSearchGroupAlign(NavelsZippedType &ceList, Node *insert)
{
    if (ceList.empty())
        return ceList.end();
// HINT: It is not allowed to compare with root pos, if current front.dist < new cist to root.
// DECISION: drop grout align, and use standard align further on.
    Node *prev = ceList.front().fromNode;
    Node *to = ceList.back().toNode;
    Point center = calcCenter(ceList);
    double bestDist = seboehDistWithGravity(prev, to, insert, center);
    NavelsZippedType::iterator bestI = ceList.end();
    for (auto ceI = ceList.begin();
         ceI != ceList.end(); ++ceI)
    {
        double dist = seboehDistWithGravity(ceI->insertNode, to, insert, center);
        if (dist < bestDist)
        {
            bestDist = dist;
            bestI = ceI; ++bestI;
        }
    }
    return bestI;
}

std::tuple<std::list<TamarAlgorithm::ClosestEdges>::iterator, double> 
TamarAlgorithm::findBestSingleCeAndAdd(Node *toNode, Node *fromNode, const ClosestEdges &element, std::list<ClosestEdges> &ceList, ClosestEdges *newCeP, bool isOldMethod, bool isGroupAlign)
{
    if (isOldMethod)
    {
        NavelsZippedType::iterator iter;
        ClosestEdges ce(element.insertNode);
        if (!isGroupAlign)
        {
            auto bestRes = findBestSingleSearch(ceList, element);
            iter = std::get<0>(bestRes);
            ce = updateBestResort(fromNode, toNode, element.insertNode, iter, ceList, newCeP);
        } else {
            // decision droped of this align, because findBestSingleSearchGroupAlign is not suitable for align to hull-edge.
            assert(false);
            iter = findBestSingleSearchGroupAlign(ceList, element.insertNode);
            // following code differs from update in updateBestResort().
            if (iter == ceList.end())
            {
                ce.fromNode = fromNode;
                ce.toNode = toNode;
                iter = ceList.begin();
                if (!ceList.empty()
                    && newCeP == nullptr
                    && element.insertNode->candidateState == Node::CandidateState::ELECTED)
                {
                    iter->fromNode = ce.insertNode;
                    iter->dist = seboehDist(*iter);
                }
            } else {
                ce.fromNode = iter->fromNode;
                ce.toNode = toNode;
                if (newCeP == nullptr
                        && element.insertNode->candidateState == Node::CandidateState::ELECTED)
                {
                    iter->fromNode = ce.insertNode;
                    iter->dist = seboehDist(*iter);
                }
            }
            ce.dist = seboehDist(ce);
        }
        if (newCeP == nullptr)
        {
            iter = ceList.insert(iter, ce);
        } else {
            *newCeP = ce;
        }
        return std::make_tuple(iter, 0);
    }

    auto ceListCopy = ceList;
    double distAdditional = correctDistTMP_DELETE_WITH_CANDIDATE(ceList);

    NavelsZippedType::iterator iter = insertBestResortedPos(fromNode, toNode, element.insertNode, ceList);

    distAdditional = correctDistTMP_DELETE_WITH_CANDIDATE(ceList) - distAdditional;
    distAdditional -= iter->dist;

    if (newCeP != nullptr)
    {
        *newCeP = *iter;
        ceList = ceListCopy;        // delete inserted element, iter not longer valid.
        return std::make_tuple(NavelsZippedType::iterator(), distAdditional);
    } else {
        return std::make_tuple(iter, distAdditional);
    }
}

void TamarAlgorithm::eraseFoundBestPos(Node *insert, NavelsZippedType &ceList)
{
    assert(!ceList.empty());
    // TODO [seboeh] improve by marking resorted elements and only reinsert the resorted elements. Resorted by insertBestResortedPos
    NavelsZippedType ceListNew;
    Node *from = ceList.front().fromNode;
    Node *to = ceList.back().toNode;
    for (const auto &ce : ceList)
    {
        if (insert == ce.insertNode)
            continue;
        insertBestResortedPos(from, to, ce.insertNode, ceListNew);
    }
    ceList = std::move(ceListNew);
}

TamarAlgorithm::NavelsZippedType::iterator TamarAlgorithm::eraseFromCeList(Node *el, std::list<ClosestEdges> &ceList)
{
    auto findI = std::find_if(ceList.begin(), ceList.end()
                              , [el] (const ClosestEdges &val)
    {
        return el == val.insertNode;
    });
    assert(findI != ceList.end());
    return eraseFromCeList(findI, ceList);
}

TamarAlgorithm::NavelsZippedType::iterator TamarAlgorithm::eraseFromCeList(std::list<ClosestEdges>::iterator &iter, std::list<ClosestEdges> &ceList)
{
    assert(iter != ceList.end());
    if (iter == --ceList.end())
    {
        ceList.pop_back();
        return ceList.end();
    } else {
        auto nextI = iter; ++nextI;
        nextI->fromNode = iter->fromNode;
        nextI->dist = seboehDist(*nextI);
		return ceList.erase(iter);
    }
}

std::tuple<double
   , TamarAlgorithm::NavelsZippedType::iterator
    , TamarAlgorithm::NavelsZippedType::iterator
    , bool>
    TamarAlgorithm::findClosestAndSecondClosestOfHigherOrders(NavelsZippedType *innerNodesOrdered
                                                , Node *currentNavelFrom
                                                , Node *currentNavelTo)
{
    double distClosest = std::numeric_limits<double>::max();
    auto closestBestIter = innerNodesOrdered->end();
    for (auto ceI = innerNodesOrdered->begin();
         ceI != innerNodesOrdered->end(); ++ceI)
    {
        double dist = seboehDist(currentNavelFrom, currentNavelTo, ceI->insertNode);
        if (dist < distClosest)
        {
            distClosest = dist;
            closestBestIter = ceI;
        }
    }
    assert(closestBestIter != innerNodesOrdered->end());
    auto iterPrev = closestBestIter;
    if (iterPrev == innerNodesOrdered->begin())
    {
        iterPrev = --innerNodesOrdered->end();
    } else {
        --iterPrev;
    }
    auto iterNext = closestBestIter;
    if (iterNext == --innerNodesOrdered->end())
    {
        iterNext = innerNodesOrdered->begin();
    } else {
        ++iterNext;
    }

    double distPrev = seboehDist(currentNavelFrom, currentNavelTo, iterPrev->insertNode);
    double distNext = seboehDist(currentNavelFrom, currentNavelTo, iterNext->insertNode);
    bool isBackward = (distPrev < distNext);
    NavelsZippedType::iterator closestSecondI;
    if (isBackward)
    {
        closestSecondI = iterPrev;
    } else {
        closestSecondI = iterNext;
    }
    return std::tuple<double
            , TamarAlgorithm::NavelsZippedType::iterator
            , TamarAlgorithm::NavelsZippedType::iterator
            , bool>
            (
                distClosest
                , closestBestIter
                , closestSecondI
                , isBackward
                );

}

std::tuple<double
    , std::list<TamarAlgorithm::ClosestEdges>, std::list<TamarAlgorithm::ClosestEdges>>
TamarAlgorithm::calculateWipeOpen(Node *currentNavelFrom, Node* currentNavelTo
                                  , Node *nextNavelFrom, Node *nextNavelRootMainListNode
                                  , std::list<ClosestEdges> *currentNavelRoot
                                  , std::list<ClosestEdges> *nextNavelRoot
                                  , std::list<ClosestEdges> *middleNavelOrdered
                              )
{
    /// assumption
    if (middleNavelOrdered->empty())
        return std::tuple<double
                , std::list<ClosestEdges>
                , std::list<ClosestEdges>>(
                                              std::numeric_limits<double>::max()
                                              , std::move(std::list<ClosestEdges>())
                                              , std::move(std::list<ClosestEdges>()));

    /// preparation - move all nextNavelRoot elements to newForwardedNodes.
    std::list<ClosestEdges> newForwardedNodes = *nextNavelRoot;
    for (auto ceI = --middleNavelOrdered->end();
         true; --ceI)
    {
        findBestSingleCeAndAdd(nextNavelRootMainListNode, nextNavelFrom, *ceI, newForwardedNodes
                               , nullptr, true);
        assert(newForwardedNodes.front().fromNode != ceI->insertNode);
        if (ceI == middleNavelOrdered->begin())
            break;
    }

    /// detect closest and second closest point to alternative edge.
    auto findClosestRes = findClosestAndSecondClosestOfHigherOrders(middleNavelOrdered, currentNavelFrom, currentNavelTo);
    bool isBackward = std::get<3>(findClosestRes);
    auto closestBestIter = std::get<1>(findClosestRes);

    /// calculation - minimization
    // HINT: DECIDE: every node of current and next root have to be intaken.
    // Song : max Giesinger 80 millionen
    // HINT: Not allowed: std::list<ClosestEdges> newBackwardedNodes = *currentNavelOrder;
    // Because currentNavelRoot is the already improved order. But we walk over ordered nodes starting with closestBestIter.
    std::list<ClosestEdges> newBackwardedNodes = *currentNavelRoot;
    double bestDist = correctDist(newForwardedNodes);       // newForwardeNodes contains all point by start.
    auto backI = closestBestIter;
    while (true)
    {
        /// delete forward node to shift it to backward list.
        assert(!newForwardedNodes.empty());
        // find equivalent entry in newForwardedNodes
        // TODO [seboeh] improve by map.
        Node *node = backI->insertNode;
        auto iter = std::find_if(newForwardedNodes.begin(), newForwardedNodes.end(),
                              [node](const ClosestEdges &ce)
        {
           return ce.insertNode == node;
        });
        assert(iter != newForwardedNodes.end());
        eraseFromCeList(iter, newForwardedNodes);

        /// recalc dist for forward - todo improve
        double distTotalForw = correctDist(newForwardedNodes);

        // Song : The strumbellas - spirits
        // HINT: We need to insert, calculate the distance and delete it again, if not suitable.
        // Because the single distance is not comparable, because it could be bad and other points could get better.
        auto res = findBestSingleCeAndAdd(currentNavelTo, currentNavelFrom, *backI, newBackwardedNodes
                                          , nullptr, true);
        auto ceNewI = std::get<0>(res);

        double distTotalBack = correctDist(newBackwardedNodes);

        if (distTotalBack + distTotalForw < bestDist)
        {
            bestDist = distTotalBack + distTotalForw;
        } else {
            // delete it from backward
            eraseFromCeList(ceNewI, newBackwardedNodes);
            // insert to newForwardedNodes.
            findBestSingleCeAndAdd(nextNavelRootMainListNode, nextNavelFrom, *backI, newForwardedNodes
                                   , nullptr, true);
        }
        // Song: frans - if i were sorry

        /// iterate
        if (iterateBackOrForward(closestBestIter, isBackward, backI, *middleNavelOrdered) == 0)
            break;
    }

    /// insert/delete to new lists
    // TODO [seboeh]: Improve - we don not need newBackwardedNodes.erase
    // , because navelBackwardInsertions is not full with nodes which should not align to. see isOmit above.

    /// postprocessing & return
    return std::tuple<double
            , std::list<ClosestEdges>
            , std::list<ClosestEdges>>(
                bestDist
                , std::move(newForwardedNodes)
                , std::move(newBackwardedNodes));
                //, newForwardedNodes, newBackwardedNodes);
}

void TamarAlgorithm::updateWipeCloseResult(NavelsMapType &navels
                                           , std::list<NavelsMapType::iterator> &navelIs
                                           , NavelsZippedType &frontListCloseRes
                                           , std::list<ClosestEdges> *middleList
                                           , NavelsZippedType &backListCloseRes
                                           , Node *middleFromNode
                                           , NavelsZippedType &middleListCloseRes
                                           , std::list<ClosestEdges> *frontList
                                           , std::list<ClosestEdges> *backList
                                           , bool &isChanged
                                           , Node *backwardFromNode
										   , Node *forwardFromNode
										   , bool closeCombiImproved)
{
    *backList = backListCloseRes;
    *middleList = middleListCloseRes;
    *frontList = frontListCloseRes;
    isChanged = true;
    backwardFromNode->gravityIsUpdated = false;
    forwardFromNode->gravityIsUpdated = false;
    middleFromNode->gravityIsUpdated = false;
	if (!closeCombiImproved)
	{
		for (auto &navelI : navelIs)
		{
			auto findI = navels.find(navelI->first);
			assert(findI != navels.end());
			findI->second = std::move(navelI->second);
		}
	}		// else, it can throw away the result of wipeFoldingClose().
}

void TamarAlgorithm::wipeFoldingIncIter(std::list<Node*> &froms
                                          , std::list<Node*>::iterator &backwardCascadeFromI
                                          , std::list<Node*>::iterator &forwardCascadeFromI
                                          , std::list<Node*>::iterator &middleCascadeFromI)
{
    ++backwardCascadeFromI;
    ++middleCascadeFromI;
    ++forwardCascadeFromI;
    if (forwardCascadeFromI == froms.end())
        forwardCascadeFromI = froms.begin();
    if (middleCascadeFromI == froms.end())
        middleCascadeFromI = froms.begin();
    if (backwardCascadeFromI == froms.end())
        backwardCascadeFromI = froms.begin();
}

void TamarAlgorithm::wipeFoldingCloseCombinationResultOnOriginalLists(std::list<ClosestEdges> *middleList, bool &closeCombiImproved
                                                                      , std::list<ClosestEdges> *backList
                                                                      , Node *forwardNextNode, NavelsZippedType &backListCloseRes
                                                                      , NavelsZippedType &middleListCloseRes
                                                                      , Node *forwardFromNode, std::list<ClosestEdges> *frontList
                                                                      , Node *middleFromNode, NavelsZippedType &frontListCloseRes
                                                                      , Node *middleNextNode)
{
    auto bestResCloseCombi = wipeFoldingCloseCombination(*backList, *middleList, *frontList
                                                         , middleFromNode, middleNextNode
                                                         , forwardFromNode, forwardNextNode);

    if (std::get<0>(bestResCloseCombi))
    {       // lists are changed - result is improved
        backListCloseRes = std::move(std::get<1>(bestResCloseCombi));
        middleListCloseRes = std::move(std::get<2>(bestResCloseCombi));
        frontListCloseRes = std::move(std::get<3>(bestResCloseCombi));
        closeCombiImproved = true;
    }
}

void TamarAlgorithm::wipeFoldingOpenAndInitiateClose(std::list<Node*> &originalInnerNodes, NavelsMapType &navels, std::list<Node*> &froms)
{
    /// pre-condition
	if (froms.size() < 3)
	{		// all shifting swift should be done by forward and backward before this method call.
		return;
	}

    /// calculate
	// run through froms - hull cascades.
	auto backwardCascadeFromI = froms.begin();
	auto middleCascadeFromI = ++(froms.begin());
	auto forwardCascadeFromI = ++(++(froms.begin()));
    auto firstForwardFromI = forwardCascadeFromI;
    bool isChanged;
    do {
        isChanged = false;
		// move half back elements to backwardcascade and half front elements to forwardcascade. And calculate cascade consts.
		auto findMiddleListI = navels.find(*middleCascadeFromI);
		assert(findMiddleListI != navels.end());
        std::list<ClosestEdges> *middleList = &(findMiddleListI->second);
		Node *middleFromNode = *middleCascadeFromI;
        Node *middleNextNode = middleFromNode->next;        // equal to : const_cast<Node*>(Way::nextChild(middleFromNode));

		NavelsMapType::iterator backwardToNodeFindI = navels.find(*backwardCascadeFromI);
		assert(backwardToNodeFindI != navels.end());
		std::list<ClosestEdges> *backList = &(backwardToNodeFindI->second);
		Node *backwardFromNode = *backwardCascadeFromI;
        Node *backwardNextNode = backwardFromNode->next;        // equal to: const_cast<Node*>(Way::nextChild(backwardFromNode));

		NavelsMapType::iterator forwardFromNodeFindI = navels.find(*forwardCascadeFromI);
		assert(forwardFromNodeFindI != navels.end());
		std::list<ClosestEdges> *frontList = &(forwardFromNodeFindI->second);
		Node *forwardFromNode = *forwardCascadeFromI;
        Node *forwardNextNode = forwardFromNode->next;       // equal to: const_cast<Node*>(Way::nextChild(forwardFromNode));

        /// cancel criteria
		if ((middleList->empty() && frontList->empty() && backList->empty())
				|| ((middleList->size() + frontList->size() + backList->size()) < 2))
        {
            // HINT: We need three points in the ways at least. Because two or one point is solved by wipeAll() before.
            // next iteration
            wipeFoldingIncIter(froms, backwardCascadeFromI, forwardCascadeFromI, middleCascadeFromI);
            if (forwardCascadeFromI != firstForwardFromI)
            {
                continue;
            } else {
                if (!isChanged)
                {
                    break;
                } else {
                    continue;
                }
            }
        }

		// HINT: At least two elements at those hull edge.
		// check to wipe all to left or right and go along the middle hull edge.
		// search best alignment.
        double distOld =
                correctDist(*backList)
                + correctDist(*middleList)
                + correctDist(*frontList);

        double distNewOpen;
        NavelsZippedType backListOpenRes;
        NavelsZippedType frontListOpenRes;
        if (middleList->size() > 1)
        {
            // HINT: we do opening only from 2 and more points, because one point was correct already through wiping forward/backward.
            /// find best alignment between next and prev edge
            auto bestRes = calculateWipeOpen(backwardFromNode, backwardNextNode, forwardFromNode, forwardNextNode
                                             , backList, frontList, middleList);

            /// detect order
            backListOpenRes = std::move(std::get<2>(bestRes));
            frontListOpenRes = std::move(std::get<1>(bestRes));
            distNewOpen =
                    correctDist(backListOpenRes)		// TODO [seboeh] std::get<0>(bestRes) is not correct!
                    + correctDist(frontListOpenRes);
            // HINT: we calculate only on detour, because length is dependend from other edges.
            // so we do not need this: + m_para.distanceFnc(middleFromNode->point, middleList->front().toNode->point);

        } else {
            /// could be, that a closing is possible and suitable, although no opening was possible.
            distNewOpen = std::numeric_limits<double>::max();
			backListOpenRes = *backList;
            frontListOpenRes = *frontList;
			if (!middleList->empty())
			{			// HINT: there is only one element in middleList, because of test size() > 1, above this line.
				findBestSingleCeAndAdd(forwardNextNode, forwardFromNode, middleList->front(), frontListOpenRes
                                       , nullptr, true);
            }
        }

        /// initiate close
        auto combinedList = frontListOpenRes;
        combinedList.insert( combinedList.end(), backListOpenRes.begin(), backListOpenRes.end());

        NavelsMapType navelsClose = navels;
        NavelsZippedType backListCloseRes, middleListCloseRes, frontListCloseRes;
        std::list<NavelsMapType::iterator> navelIs;
        std::list<double> altDists;
        auto bestResClose = wipeFoldingCloseGravity(combinedList
                                              , middleFromNode, middleNextNode
                                              , forwardFromNode, forwardNextNode
                                              , backwardFromNode, backwardNextNode
                                              , navelsClose
                                              , originalInnerNodes);

        double distNewClose;
		// HINT: we assume open folding was always good before close!
		bool closeCombiImproved = false;
		if (std::get<0>(bestResClose) == 1)
		{
			backListCloseRes = std::move(std::get<1>(bestResClose));
			middleListCloseRes = std::move(std::get<2>(bestResClose));
			frontListCloseRes = std::move(std::get<3>(bestResClose));

            distNewClose =
                    correctDist(backListCloseRes)
                    + correctDist(middleListCloseRes)
                    + correctDist(frontListCloseRes);

            // add alternative distances
            navelIs = std::move(std::get<4>(bestResClose));
            if (!navelIs.empty())
            {
                altDists = std::move(std::get<5>(bestResClose));
                double altDistSum = 0;
                for (double dist : altDists)
                    altDistSum += dist;
                distOld += altDistSum;
                distNewOpen += altDistSum;

                for (auto &navelI : navelIs)
                {
                    distNewClose += correctDist(navelI->second);
                }
                // HINT: Not true always: assert(distOld > distNewClose);
                // HINT: Not true always: assert(distNewOpen > distNewClose);
            }

            // HINT: we assume open folding was always good before close!
            bool closeCombiImproved = false;
            if (distNewClose < distNewOpen)
            {
                if (distNewClose < distOld)
                {
                    // TODO [seboeh] check if the gravity result with alternative edges, makes
                    // following closCombination useless.
                    /// close with combination of front and back list - will improve the lists only.
                    auto bestResCloseCombi = wipeFoldingCloseCombination(backListCloseRes, middleListCloseRes, frontListCloseRes
                                                                         , middleFromNode, middleNextNode
                                                                         , forwardFromNode, forwardNextNode);

                    if (std::get<0>(bestResCloseCombi))
                    {       // lists are changed - result is improved
                        backListCloseRes = std::move(std::get<1>(bestResCloseCombi));
                        middleListCloseRes = std::move(std::get<2>(bestResCloseCombi));
                        frontListCloseRes = std::move(std::get<3>(bestResCloseCombi));
                        closeCombiImproved = true;
                    }
                } else {
                    // calculate on original constellation.
                    wipeFoldingCloseCombinationResultOnOriginalLists(middleList, closeCombiImproved, backList, forwardNextNode, backListCloseRes, middleListCloseRes, forwardFromNode, frontList, middleFromNode, frontListCloseRes, middleNextNode);
                }
            } else if (distNewOpen < distOld)
            {
                NavelsZippedType middleFakeList;
                auto bestResCloseCombi = wipeFoldingCloseCombination(backListOpenRes, middleFakeList, frontListOpenRes
                                                                     , middleFromNode, middleNextNode
                                                                     , forwardFromNode, forwardNextNode);

                if (std::get<0>(bestResCloseCombi))
                {       // lists are changed - result is improved
                    backListCloseRes = std::move(std::get<1>(bestResCloseCombi));
                    middleListCloseRes = std::move(std::get<2>(bestResCloseCombi));
                    frontListCloseRes = std::move(std::get<3>(bestResCloseCombi));
                    closeCombiImproved = true;
                }
            } else {
                        // calculate on original constellation.
                wipeFoldingCloseCombinationResultOnOriginalLists(middleList, closeCombiImproved, backList, forwardNextNode, backListCloseRes, middleListCloseRes, forwardFromNode, frontList, middleFromNode, frontListCloseRes, middleNextNode);
            }

            if (closeCombiImproved)
            {
                distNewClose =
                        correctDist(backListCloseRes)
                        + correctDist(middleListCloseRes)
                        + correctDist(frontListCloseRes);
            }

        } else if (distNewOpen < distOld){

            /// close with combination of front and back list - will improve the lists only.
            NavelsZippedType middleFakeList;
            auto bestResCloseCombi = wipeFoldingCloseCombination(backListOpenRes, middleFakeList, frontListOpenRes
                                                                 , middleFromNode, middleNextNode
                                                                 , forwardFromNode, forwardNextNode);

            if (std::get<0>(bestResCloseCombi))
            {       // lists are changed
                backListCloseRes = std::move(std::get<1>(bestResCloseCombi));
                middleListCloseRes = std::move(std::get<2>(bestResCloseCombi));
                frontListCloseRes = std::move(std::get<3>(bestResCloseCombi));
                distNewClose =
                        correctDist(backListCloseRes)
                        + correctDist(middleListCloseRes)
                        + correctDist(frontListCloseRes);
				closeCombiImproved = true;
            } else {
                distNewClose = std::numeric_limits<double>::max();
            }
        } else {
            auto bestResCloseCombi = wipeFoldingCloseCombination(*backList, *middleList, *frontList
                                                                 , middleFromNode, middleNextNode
                                                                 , forwardFromNode, forwardNextNode);

            if (std::get<0>(bestResCloseCombi))
            {       // lists are changed
                backListCloseRes = std::move(std::get<1>(bestResCloseCombi));
                middleListCloseRes = std::move(std::get<2>(bestResCloseCombi));
                frontListCloseRes = std::move(std::get<3>(bestResCloseCombi));
                distNewClose =
                        correctDist(backListCloseRes)
                        + correctDist(middleListCloseRes)
                        + correctDist(frontListCloseRes);
				closeCombiImproved = true;
            } else {
                distNewClose = std::numeric_limits<double>::max();
            }
        }

        /// update - decision
        if (distNewOpen < distOld)
		{
            if (distNewOpen < distNewClose)
            {
                *backList = backListOpenRes;
                *frontList = frontListOpenRes;
                middleList->clear();
                isChanged = true;
                backwardFromNode->gravityIsUpdated = false;
                forwardFromNode->gravityIsUpdated = false;
                middleFromNode->gravityIsUpdated = false;
            } else if (distNewClose < distNewOpen)
            {
				updateWipeCloseResult(navels, navelIs, frontListCloseRes, middleList, backListCloseRes, middleFromNode, middleListCloseRes, frontList, backList, isChanged, backwardFromNode, forwardFromNode, closeCombiImproved);
            }   // else, old - do nothing
        } else if (distNewClose < distOld)
        {
			updateWipeCloseResult(navels, navelIs, frontListCloseRes, middleList, backListCloseRes, middleFromNode, middleListCloseRes, frontList, backList, isChanged, backwardFromNode, forwardFromNode, closeCombiImproved);
        } // else, old - do nothing

		/// next iteration
        wipeFoldingIncIter(froms, backwardCascadeFromI, forwardCascadeFromI, middleCascadeFromI);

    } while (forwardCascadeFromI != firstForwardFromI || isChanged);

}

double TamarAlgorithm::gravity(Node *p1, Node *p2, Node *pInsert)
{
	// [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double grav = 0;
    // HINT: for-loop should represent the area of gravity.
    // stringPointsCount is a kind of precision of gravity area.
    // word string is in relation to string-theory of physics.
    int stringPointsCount = 7;
    double step = 1.0/(m_para.distanceFnc(p1->point, p2->point)/stringPointsCount);
    step = sqrt(step);		// if step is small, do bigger steps.
    for (double pos=0; pos < 1.0; pos += step)
    {
        double gravityInterpolated = (1.0-pos)*p1->gravityWeight + pos*p2->gravityWeight;
        TSP::Point pInterpolated = Point((1.0-pos)*p1->point.x()+pos*p2->point.x()
                                       , (1.0-pos)*p1->point.y()+pos*p2->point.y());
        double stringDist = m_para.distanceFnc(pInterpolated, pInsert->point);
        grav += gravityInterpolated/(pow(stringDist,2));
    }

	return grav;
}

double TamarAlgorithm::calcGravity(NavelsZippedType &navel, Node *fromNode, Node *toNode, Node *innerPoint)
{
	double gravityWeight = 1;
	if (navel.empty())
	{
		// if first point, calc gravity based on weight 1 for from/to node.
		fromNode->gravityWeight = 1;
		toNode->gravityWeight = 1;
		gravityWeight += gravity(fromNode, toNode, innerPoint);
	} else if (++navel.begin() == navel.end())
	{
		// if second point, calc gravity based on weigth of first point and on closest from/to Node, with weight 1.
		double distFrom = m_para.distanceFnc(fromNode->point, innerPoint->point);
		double distTo = m_para.distanceFnc(toNode->point, innerPoint->point);
		if (distFrom < distTo)
		{
			gravityWeight += gravity(fromNode, navel.begin()->insertNode, innerPoint);
		} else {
			gravityWeight += gravity(toNode, navel.begin()->insertNode, innerPoint);
		}
	} else {
		// if more than two points, calc closest points, including from/to node, and calc gravity based on those points.
		Node *closestFirst = nullptr;
		Node *closestSecond = nullptr;
		double closestDist = std::numeric_limits<double>::max();
		for (const auto &ce : navel)
		{
			double dist = m_para.distanceFnc(ce.insertNode->point, innerPoint->point);
			if (dist < closestDist)
			{
				closestSecond = closestFirst;
				closestFirst = ce.insertNode;
				closestDist = dist;
			} else {
				if (closestSecond == nullptr)		// guaranty that second != null, if navel only first entry was smallest dist.
					closestSecond = ce.insertNode;
			}
		}
		assert(closestFirst != nullptr);
		assert(closestSecond != nullptr);
		gravityWeight += gravity(closestFirst, closestSecond, innerPoint);
	}

	return gravityWeight;
}

void TamarAlgorithm::updateGravity(NavelsMapType &navels, std::list<Node*> &originalInnerNodes
                                   , Node *backwardFromNode
                                   , Node *middleFromNode
                                   , Node *forwardFromNode)
{
    for (auto navelI = navels.begin();
         navelI != navels.end(); ++navelI)
    {
        /// assumptions
        Node *fromNode = navelI->first;
        if (navelI->second.empty()
            || fromNode == backwardFromNode
            || fromNode == middleFromNode
            || fromNode == forwardFromNode
            || fromNode->gravityIsUpdated)
        continue;

        /// prepare
        Node *toNode = const_cast<Node*>(Way::nextChild(fromNode));
        auto findClosestRes = findClosestAndSecondClosestOfHigherOrders(&navelI->second, fromNode, toNode);
        bool isBackward = std::get<3>(findClosestRes);
        auto closestBestIter = std::get<1>(findClosestRes);

        /// calculate
        NavelsZippedType newNavel;
        for (NavelsZippedType::iterator ceI = closestBestIter;
             true; )
        {
            ceI->insertNode->gravityWeight = calcGravity(newNavel, fromNode, toNode, ceI->insertNode);
            findBestSingleCeAndAdd(toNode, fromNode, *ceI, newNavel
                                   , nullptr, true);

            /// iterate
            if (iterateBackOrForward(closestBestIter, isBackward, ceI, navelI->second) == 0)
                break;
        }

        /// decision/update
        fromNode->gravityIsUpdated = true;
        navelI->second = std::move(newNavel);
    }
}

int TamarAlgorithm::iterateBackOrForward(const NavelsZippedType::iterator &closestBestIter, bool isBackward, NavelsZippedType::iterator &ceI, NavelsZippedType &listOrdered)
{
	if (isBackward)
    {
		if (listOrdered.size() == 1)
			return 0;		// because, only one iteration.
		if (ceI == listOrdered.begin())
        {
            ceI = --listOrdered.end();
        } else {
            --ceI;
        }
	} else {
        ++ceI;
        if (ceI == listOrdered.end())
        {
            ceI = listOrdered.begin();
        }
    }
	if (ceI == closestBestIter)
	{
		return 0;
	} else {
		return 1;
	}
}

std::tuple<double, TamarAlgorithm::NavelsMapType::iterator>
TamarAlgorithm::wipeFoldingCloseAlternative(NavelsMapType &navels
                                                  , Node *forwardFromNode
                                                  , Node *middleFromNode
                                                  , Node *backwardFromNode
                                                  , Node *insertNode)
{
    auto bestNavelI = navels.end();
    double bestGrav = 0;
    for (auto navelI = navels.begin();
         navelI != navels.end(); ++navelI)
    {
        Node *fromNode = navelI->first;
        if (fromNode == forwardFromNode
            || fromNode == middleFromNode
            || fromNode == backwardFromNode)
            continue;

        Node *toNode = const_cast<Node*>(Way::nextChild(navelI->first));

        double grav = calcGravity(navelI->second, fromNode, toNode, insertNode);
        if (grav > bestGrav)
        {
            bestNavelI = navelI;
            bestGrav = grav;
        }
    }

    return std::tuple<double, TamarAlgorithm::NavelsMapType::iterator>
            (bestGrav, bestNavelI);
}

std::tuple<bool, TamarAlgorithm::NavelsZippedType, TamarAlgorithm::NavelsZippedType,
   TamarAlgorithm::NavelsZippedType>
TamarAlgorithm::wipeFoldingCloseCombination(NavelsZippedType &backList, NavelsZippedType &middleList, NavelsZippedType &frontList
                                            , Node *middleFromNode, Node *middleToNode
                                            , Node *forwardFromNode, Node *forwardToNode)
{
    if (backList.empty() || frontList.empty())
        return std::tuple<bool, TamarAlgorithm::NavelsZippedType, TamarAlgorithm::NavelsZippedType,
                TamarAlgorithm::NavelsZippedType>(
                    false
                    , backList
                    , middleList
                    , frontList);


    /// Calculate old dist for back,front and middleList.
    double distOld =
            correctDist(backList)
            + correctDist(middleList)
            + correctDist(frontList);

    /// Copy middleList into backup (lastMiddle) for iterative loop over back and front list.
    NavelsZippedType backCurrent;
    auto frontCurrent = frontList;

    /// Initiate bestIter's. One for frontList, which is walked forward, one for backList, which is walked backward.
    Node *bestBackNode = nullptr;            // backList instead of backCurrent, because we use this as sign.
    Node *bestFrontNode = nullptr;
    NavelsZippedType bestMiddle = middleList;
    NavelsZippedType bestBack = backList;
    NavelsZippedType middleLast = middleList;

    /// bestDist = current dist, because we have to improve the given constellation.
    double distBest = distOld;
    /// loop over front list - forward
    for (auto frontI = frontCurrent.begin();
         frontI != frontCurrent.end(); )
    {
        auto middleCurrent = middleLast;
        /// insert frontI to currentMiddleList
        findBestSingleCeAndAdd(middleToNode, middleFromNode, *frontI, middleCurrent
                               , nullptr, true);
        Node *frontNode = frontI->insertNode;
        middleLast = middleCurrent;
        /// erase frontI from front list
        frontI = eraseFromCeList(frontI, frontCurrent);
        /// copy lastMiddleList = currentMiddleList
        /// loop over back list - backward, starting from bestBackI
        backCurrent = bestBack;
        for (auto backI = --backCurrent.end();
             backI != backCurrent.end(); )
        {
            /// insert backI in middle list
            findBestSingleCeAndAdd(middleToNode, middleFromNode, *backI, middleCurrent
                                   , nullptr, true);
            /// erase backI from back list
            Node *backNode = backI->insertNode;
            backI = eraseFromCeList(backI, backCurrent);
            // because backI walks backward, after eeraseFCL we are at end always.
            if (!backCurrent.empty())
                --backI;
            /// recalc dist of back,middle,front list
            double distCur =
                    correctDist(backCurrent)
                    + correctDist(middleCurrent)
                    + correctDist(frontCurrent);

            /// check if dist < bestDist -> save iterators, bestMiddle = currentMiddle and lastMiddle = currentMiddle.
            if (distCur < distBest)
            {
                /// new frontList and backList.
                bestBackNode = backNode;
                bestFrontNode = frontNode;
                distBest = distCur;
                bestMiddle = middleCurrent;
                bestBack = backCurrent;
            }
        }       // end backward backList
    }       // end forward frontList

    /// move all Nodes from frontList starting from exclusive bestFrontNode, to newFrontList.
    NavelsZippedType newForward;
    if (bestFrontNode != nullptr)
    {
        bool isInsert = false;
        for (auto frontI = frontList.begin();
             frontI != frontList.end(); ++frontI)
        {
            if (isInsert)
                findBestSingleCeAndAdd(forwardToNode, forwardFromNode, *frontI, newForward
                                       , nullptr, true);
            if (frontI->insertNode == bestFrontNode)
                isInsert = true;        // next frontI will be inserted, till end().
        }
    } else {
            // we did not find any improvement
        newForward = frontList;
    }
    // HINT: bestBack is the correct list - nothing to do for backward case.

    /// Return - isChanged, newBackList, bestMiddleList, newFrontList
    return std::tuple<bool, TamarAlgorithm::NavelsZippedType, TamarAlgorithm::NavelsZippedType,
            TamarAlgorithm::NavelsZippedType>(
                bestFrontNode != nullptr
                , std::move(bestBack)
                , std::move(bestMiddle)
                , std::move(newForward));
}

std::tuple<int, TamarAlgorithm::NavelsZippedType, TamarAlgorithm::NavelsZippedType,
   TamarAlgorithm::NavelsZippedType
, std::list<TamarAlgorithm::NavelsMapType::iterator>
, std::list<double>>
TamarAlgorithm::wipeFoldingCloseGravity(NavelsZippedType &relevantNodesOrdered
								  , Node *middleFromNode, Node *middleToNode
								  , Node *forwardFromNode, Node *forwardToNode
                                  , Node *backwardFromNode, Node *backwardToNode
                                  , NavelsMapType &navels
                                  , std::list<Node*> &originalInnerNodes)
{
	// TODO [seboeh] originally, the middle list shall be constructed together with front and back points in combination. Because together, they get an advantage.
	// currently, the algorithm should do this by hierachical order.
    assert(!relevantNodesOrdered.empty());
    assert(++relevantNodesOrdered.begin() != relevantNodesOrdered.end());

    // necessary because open and new order of navels elements according to originalInnerNodes.
    updateGravity(navels, originalInnerNodes, backwardFromNode, middleFromNode, forwardFromNode);

	/// preparation
	// detect closest and second closest point to alternative edge.
    auto findClosestRes = findClosestAndSecondClosestOfHigherOrders(&relevantNodesOrdered, middleFromNode, middleToNode);
	bool isBackward = std::get<3>(findClosestRes);
	auto closestBestIter = std::get<1>(findClosestRes);

	NavelsZippedType middleNew;
	NavelsZippedType frontNew;
	NavelsZippedType backNew;
    std::list<NavelsMapType::iterator> navelsIters;
    std::list<NavelsZippedType> oldNavels;
    std::list<double> oldDists;
    bool isChangedByAlternative = false;

	/// run
    for (NavelsZippedType::iterator ceI = closestBestIter;
		 true; )
	{
        // TODO [seboeh] .... calc together with two points in the middle.
        // TODO [seboeh] use four hull-edges instead of three.
		/// calculate
		double gravForw = calcGravity(frontNew, forwardFromNode, forwardToNode, ceI->insertNode);

		double gravBack = calcGravity(backNew, backwardFromNode, backwardToNode, ceI->insertNode);

		double gravMiddle = calcGravity(middleNew, middleFromNode, middleToNode, ceI->insertNode);

        auto alternativeRes = wipeFoldingCloseAlternative(navels
                                                          , forwardFromNode
                                                          , middleFromNode
                                                          , backwardFromNode
                                                          , ceI->insertNode);
        double gravAlt = std::get<0>(alternativeRes);
        auto altNavelI = std::get<1>(alternativeRes);

		/// insert
        if (gravAlt > gravBack && gravAlt > gravMiddle && gravAlt > gravForw && altNavelI != navels.end())
        {
            isChangedByAlternative = true;
			auto findI = std::find_if(navelsIters.begin(), navelsIters.end(),
									  [altNavelI] (const NavelsMapType::iterator &val)
			{
				return altNavelI->first == val->first;
			});
			if (findI == navelsIters.end())
            {       // new alternative navel found.
				navelsIters.push_back(altNavelI);
                // HINT: old navels entry was copied before this method call.
                oldDists.push_back( correctDist(altNavelI->second) );
            }

			// find ceI in navels to delete
			// HINT: navels is copy of original navel, before this method call, see navelClose.
			bool isNavelCeFound = false;		// TODO [seboeh] check if possible without delete?
			for (auto &navel : navels)
			{
				for (auto ceNavelI = navel.second.begin();
					 ceNavelI != navel.second.end(); ++ceNavelI)
				{
					if (ceI->insertNode == ceNavelI->insertNode)
					{
						eraseFromCeList(ceNavelI, navel.second);
						isNavelCeFound = true;
						break;
					}
				}
				if (isNavelCeFound)
					break;
			}

			// insert new point
			// HINT: gravAlt is specified on the two closest nodes, already. So we need not to determine the two closest too.
            // We can use gravAlt as it is also.
            ceI->insertNode->gravityWeight = gravAlt;
            // HINT: with setting gravity above, we need no setting of: altNavelI->first->gravityIsUpdated = false;
            // insert ceI to alternative navel.
            Node *toNode = const_cast<Node*>(Way::nextChild(altNavelI->first));
            findBestSingleCeAndAdd(toNode, altNavelI->first, *ceI, altNavelI->second
                                   , nullptr, true);
        } else {
            if (gravBack > gravMiddle)
            {
                if (gravBack > gravForw)
                {
                    ceI->insertNode->gravityWeight = gravBack;
                    findBestSingleCeAndAdd(backwardToNode, backwardFromNode, *ceI, backNew
                                           , nullptr, true);
                } else {
                    ceI->insertNode->gravityWeight = gravForw;
                    findBestSingleCeAndAdd(forwardToNode, forwardFromNode, *ceI, frontNew
                                           , nullptr, true);
                }
            } else {
                if (gravMiddle > gravForw)
                {
                    ceI->insertNode->gravityWeight = gravMiddle;
                    findBestSingleCeAndAdd(middleToNode, middleFromNode, *ceI, middleNew
                                           , nullptr, true);
                } else {
                    ceI->insertNode->gravityWeight = gravForw;
                    findBestSingleCeAndAdd(forwardToNode, forwardFromNode, *ceI, frontNew
                                           , nullptr, true);
                }
            }
        }

        /// iterate
        if (iterateBackOrForward(closestBestIter, isBackward, ceI, relevantNodesOrdered) == 0)
            break;
    }       // end of finding alignment

	/// return
	return
			std::tuple<int, TamarAlgorithm::NavelsZippedType
			, TamarAlgorithm::NavelsZippedType
            , TamarAlgorithm::NavelsZippedType
            , std::list<TamarAlgorithm::NavelsMapType::iterator>
            , std::list<double>>(
                (!middleNew.empty() || isChangedByAlternative) ? 1 : 0
				, std::move(backNew)
				, std::move(middleNew)
                , std::move(frontNew)
                , std::move(navelsIters)
                , std::move(oldDists));

}




// HINT: If a navel get empty, it keeps empty, because other navels seem to be better.
bool TamarAlgorithm::getNextNavel(NavelsMapType &navels, NavelsMapType::iterator &navelI, std::list<Node*> &fromNodes, NavelsMapType::mapped_type * &nextNavelRoot, std::list<Node*>::iterator fromNodeI)
{
    if (fromNodes.empty())
        return false;
    auto fromNodeIStart = fromNodeI;
    ++fromNodeI;
    if (fromNodeI == fromNodes.end())
    {
        fromNodeI = fromNodes.begin();
        if (fromNodeIStart == fromNodeI)
            return false;
    }
    navelI = navels.find(*fromNodeI);
    if (navelI == navels.end())
    {
        std::cerr << "Error: millicious error occured. fromNode not in map. Error occur pos 2" << std::endl;
        return false;
    }
    nextNavelRoot = &navelI->second;

    return true;
}

// INFO: HINT: TODO: never calculate over length, (see test29) because it will not help to find best way.
// INFO: HINT: TODO: always calculate with seboehDist.
void TamarAlgorithm::adaptPrevNextCeAndErase(std::list<ClosestEdges> &newNodes, std::list<std::list<ClosestEdges>::iterator>::iterator &ceII)
{		// TODO ....delete this meth - old code
    auto iter = *ceII;
    if (iter == --newNodes.end())
    {
        auto prevI = iter;
        if (prevI != newNodes.begin())
        {
            --prevI;
            prevI->toNode = iter->toNode;
        }
    } else if (iter == newNodes.begin())
    {
        auto nextI = iter; ++nextI;
        if (nextI != newNodes.end())
        {
            nextI->fromNode = iter->fromNode;
        }
    } else {
        auto prevI = iter; --prevI;
        auto nextI = iter; ++nextI;
        prevI->toNode = iter->toNode;
        nextI->fromNode = iter->fromNode;
    }
    newNodes.erase(iter);
}

// TODO [seboeh] reduce the list of return values. Not all needed. Dead code.
std::tuple<double, std::list<TamarAlgorithm::ClosestEdges>::iterator
, std::list<TamarAlgorithm::ClosestEdges>, std::list<TamarAlgorithm::ClosestEdges>>
TamarAlgorithm::calculateWipe(Node *currentNavelFrom, Node* currentNavelTo,
                                     Node *nextNavelFrom, Node *nextNavelRootMainListNode,
                                     std::list<ClosestEdges> *currentNavelRoot,
									 std::list<ClosestEdges> *nextNavelRoot
                              )
{
    /// assumption
    if (currentNavelRoot->empty())
		return std::tuple<double, std::list<TamarAlgorithm::ClosestEdges>::iterator
                ,std::list<ClosestEdges>, std::list<ClosestEdges>>(std::numeric_limits<double>::max(), currentNavelRoot->end()
			,std::move(std::list<ClosestEdges>()), std::move(std::list<ClosestEdges>()));

    /// sort according first closest on nextNavelRoot
    std::multimap<double, NavelsZippedType::value_type> currentNavelSorted;
    for (auto ceI = currentNavelRoot->begin();
         ceI != currentNavelRoot->end(); ++ceI)
    {
        auto res = findBestSingleSearch(*nextNavelRoot, *ceI);
        auto iter = std::get<0>(res);
        ClosestEdges ceNew(ceI->insertNode);
        if (iter == nextNavelRoot->end())
        {
            if (nextNavelRoot->empty())
            {
                ceNew.fromNode = nextNavelFrom;
                ceNew.toNode = nextNavelRootMainListNode;		// TODO [seboeh] replace toNode with getNextChild().
                ceNew.dist = seboehDist(ceNew);
            } else {
                ceNew.fromNode = nextNavelRoot->back().insertNode;
                ceNew.toNode = nextNavelRootMainListNode;
                ceNew.dist = std::get<1>(res);
            }
        } else {		// normal
            ceNew.toNode = iter->toNode;
            ceNew.fromNode = iter->fromNode;
            ceNew.dist = std::get<1>(res);
        }
        currentNavelSorted.insert({ceNew.dist, ceNew});
    }
    NavelsZippedType currentNavelOrdered;
    for (auto sortedI = currentNavelSorted.begin();
         sortedI != currentNavelSorted.end(); ++sortedI)
    {
        currentNavelOrdered.push_back(sortedI->second);
    }

    /// move all to next navel
    std::list<ClosestEdges> newForwardedNodes;
    std::list<ClosestEdges> navelForwardInsertions;
    for (auto &ce : *nextNavelRoot)
    {
        auto res = findBestSingleCeAndAdd(nextNavelRootMainListNode, nextNavelFrom, ce, newForwardedNodes
                                          , nullptr, true);
        auto ceNewI = std::get<0>(res);
        /// HINT: the implementation of std::list does not use pointers as isterator, as it seems - see test54
        navelForwardInsertions.push_back(*ceNewI);
        assert(ceNewI->insertNode == ce.insertNode);
    }
    // HINT: we walk forward, because the ordered is already with closest/smallest starting.
    for (auto ceI = currentNavelOrdered.begin();
         ceI != currentNavelOrdered.end(); ++ceI)
    {
        auto res = findBestSingleCeAndAdd(nextNavelRootMainListNode, nextNavelFrom, *ceI, newForwardedNodes
                                          , nullptr, true);
        auto ceNewI = std::get<0>(res);
		assert(newForwardedNodes.front().fromNode != ceI->insertNode);
        navelForwardInsertions.push_back(*ceNewI);
        assert(ceNewI->insertNode == ceI->insertNode);
    }

    /// calculation - minimization - move back to current navel, and measure the result. In currentNavelOrdered order.
	// HINT: DECIDE: every node of current and next root have to be intaken.
    // Song : max Giesinger 80 millionen
    // HINT: Not allowed: std::list<ClosestEdges> newBackwardedNodes = *currentNavelOrder;
    std::list<ClosestEdges> newBackwardedNodes;
    NavelsZippedType bestBackwardedNodes;
    NavelsZippedType bestForwardedNodes = newForwardedNodes;
    double bestDist = correctDistTMP_DELETE(newForwardedNodes);
    assert(!navelForwardInsertions.empty());
    assert(navelForwardInsertions.size() == newForwardedNodes.size());
    for (auto ceI = --navelForwardInsertions.end();
         true; --ceI)
    {
        /// delete forward node to shift it to backward list.
        auto iter = std::find_if(newForwardedNodes.begin()
                                 , newForwardedNodes.end()
                                 , [ceI] (const ClosestEdges &ce)
        {
           return ceI->insertNode == ce.insertNode;
        });
        eraseFromCeList(iter, newForwardedNodes);
        /// recalc dist for forward
        double distTotalForw = correctDistTMP_DELETE(newForwardedNodes);

        // Song : The strumbellas - spirits
        findBestSingleCeAndAdd(currentNavelTo, currentNavelFrom, *ceI, newBackwardedNodes
                               , nullptr, true);
        double distTotalBack = correctDistTMP_DELETE(newBackwardedNodes);

        if (distTotalBack + distTotalForw < bestDist)
        {
            bestDist = distTotalBack + distTotalForw;
            bestForwardedNodes = newForwardedNodes;
            bestBackwardedNodes = newBackwardedNodes;
        }
        // Song: frans - if i were sorry

        /// iterate backward - necessary, because it is moving points form forwarded edge to backwarded edge.
        if (ceI == navelForwardInsertions.begin())
            break;
    }

    /// postprocessing & return
	return std::tuple<double, std::list<ClosestEdges>::iterator
			, std::list<ClosestEdges>, std::list<ClosestEdges>>(
                bestDist
                , currentNavelRoot->end() // unused para
                , std::move(bestForwardedNodes), std::move(bestBackwardedNodes));
}

bool TamarAlgorithm::wipe(Node *currentNavelFrom, Node *currentNavelTo, Node *nextNavelFrom, Node *nextNavelTo, NavelsMapType::mapped_type *nextNavelRoot, NavelsMapType::mapped_type *currentNavelRoot, bool isForward)
{
    /// precondition
    if ((isForward && currentNavelRoot->empty())
            || (!isForward && nextNavelRoot->empty()))
        return false;

    // debug output
    TamarDebugging::debugIsInNavels(*currentNavelRoot);
    TamarDebugging::debugIsInNavels(*nextNavelRoot);

    /// preparation

    if (!isForward)
	{
        //currentNavelRoot->reverse();
		nextNavelRoot->reverse();
		std::swap(currentNavelRoot, nextNavelRoot);
		std::swap(currentNavelFrom, nextNavelFrom);
		std::swap(currentNavelTo, nextNavelTo);
    }

    // debug output
    TamarDebugging::debugIsInNavels(*currentNavelRoot);
    TamarDebugging::debugIsInNavels(*nextNavelRoot);

    /// calculate
    auto bestResForward = calculateWipe(currentNavelFrom, currentNavelTo, nextNavelFrom, nextNavelTo,
                                        currentNavelRoot, nextNavelRoot);

    /// check if equal
    bool isEqual = true;
    double distOld =
            correctDist(*currentNavelRoot)
            + correctDist(*nextNavelRoot);
    double distNew =
            correctDist(std::get<3>(bestResForward))
            + correctDist(std::get<2>(bestResForward));
    if (distNew < distOld)
    {
        for (const auto &ce : std::get<2>(bestResForward))
        {
            auto iter = std::find_if(nextNavelRoot->begin(), nextNavelRoot->end()
                                     , [ce](const ClosestEdges &ceEl)
            {
                return ce.insertNode == ceEl.insertNode;
            });
            if (iter == nextNavelRoot->end())
            {
                isEqual = false;
                break;
            }
        }
        if (isEqual)
        {
            for (const auto &ce : std::get<3>(bestResForward))
            {
                auto iter = std::find_if(currentNavelRoot->begin(), currentNavelRoot->end()
                                         , [ce](const ClosestEdges &ceEl)
                {
                    return ce.insertNode == ceEl.insertNode;
                });
                if (iter == currentNavelRoot->end())
                {
                    isEqual = false;
                    break;
                }
            }
        }
    } else {
        isEqual = true;
    }

    /// insert
    if (!isEqual)
    {
        // next line is valid for forward and backward, because current == next if swapped - if (!isForward)
        *nextNavelRoot = std::move(std::get<2>(bestResForward));
        *currentNavelRoot = std::move(std::get<3>(bestResForward));
    }

    /// post processing
	if (!isForward)
	{
		std::swap(currentNavelRoot, nextNavelRoot);
        //currentNavelRoot->reverse();  HINT: turning is already done in calculateWipe().
        //nextNavelRoot->reverse();
	}
    return !isEqual;		// list changed.
}

// TODO [seboeh] is this method still necessary wipeDivergenceClose() could do the job.
bool TamarAlgorithm::wipeMultiple(NavelsType::iterator &currentNavelI)
{
    /// assumption
    // HINT: test47 needs also alignement to empty navels.
    //    if (currentNavelI->navelRef->empty())
    //        return false;

    /// sort according first closest on nextNavelRoot
    m_navelSorted.clear();
    for (auto &navel : m_navels)
    {
        if (navel.anchorNode == currentNavelI->anchorNode)
            continue;

        for (auto &ce : navel.navelCopy)
        {
            auto res = findBestSingleSearch(currentNavelI->navelCopy, ce);
            auto iter = std::get<0>(res);
            ClosestEdges ceNew(ce.insertNode);
            if (iter == currentNavelI->navelCopy.end())
            {
                if (currentNavelI->navelCopy.empty())
                {
                    ceNew.fromNode = currentNavelI->anchorNode;
                    ceNew.toNode = currentNavelI->anchorNode->next;		// TODO [seboeh] replace toNode with getNextChild().
                    ceNew.dist = seboehDist(ceNew);
                } else {
                    ceNew.fromNode = currentNavelI->navelCopy.back().insertNode;
                    ceNew.toNode = currentNavelI->anchorNode->next;
                    ceNew.dist = std::get<1>(res);
                }
            } else {		// normal
                ceNew.toNode = iter->toNode;
                ceNew.fromNode = iter->fromNode;
                ceNew.dist = std::get<1>(res);
            }
            m_navelSorted.insert({ceNew.dist, SortNavelEl(&navel, ceNew)});
        }
    }

    /// move step by step to current navel - minimization
    // HINT: Analog zu Adam Smith gilt: Wenn von allen genommen wird, was geht, und dies alle machen, dann ist es auch so,
    // das einem genommen wir, was einem nicht zusteht.


    std::list<SortNavelEl> bestSortedNavels;
    NavelsZippedType bestCurrentNavel;
    double bestDist = 0;
    for (auto &elS : m_navelSorted)
    {
        bestDist += elS.second.navelFrom->dist;
    }
    double currentDist = bestDist;
    double currentOldDist = correctDistTMP_DELETE(currentNavelI->navelCopy);
    bool isImproved = false;
    for (auto &elS : m_navelSorted)     // elS = element sorted.
    {
        auto elSN = elS.second.navelFrom;

        /// remove old dists
        currentDist -= currentOldDist;
        currentDist -= elSN->dist;

        /// calculate - insert
        findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                                             , elS.second.ce
                                             , currentNavelI->navelCopy
                               , nullptr, true);

        /// prepare - remove
        auto iter = std::find_if(elSN->navelCopy.begin(), elSN->navelCopy.end()
                                 , [elS](const NavelsZippedType::value_type &val)
        {
            return elS.second.ce.insertNode == val.insertNode;
        });
        assert(iter != elSN->navelCopy.end());

        /// calculate - remove
        eraseFromCeList(iter, elSN->navelCopy);

        /// calculate - add new dists
        elSN->dist = correctDistTMP_DELETE(elSN->navelCopy);
        currentDist += elSN->dist;
        currentOldDist = correctDistTMP_DELETE(currentNavelI->navelCopy);
        currentDist += currentOldDist;

        /// decision
        if (currentDist < bestDist)
        {
            // copy m_navelSorted
            for (auto &el : bestSortedNavels)
            {
                delete el.navelFrom;
            }
            bestSortedNavels.clear();
            for (auto &elS : m_navelSorted)
            {
                bestSortedNavels.push_back(elS.second);
                auto nav = new Navel(nullptr, nullptr, 0);      // nav is only used for navelCopy element.
                bestSortedNavels.back().navelFrom = nav;
                for (auto &ceEl : elS.second.navelFrom->navelCopy)
                    nav->navelCopy.push_back(ceEl);
            }
            bestCurrentNavel = currentNavelI->navelCopy;
            bestDist = currentDist;
            isImproved = true;
        }
    }       // end navelSorted

    /// return
    if (isImproved)
    {
        auto bestI = bestSortedNavels.begin();
        for (auto elSI = m_navelSorted.begin();
             elSI != m_navelSorted.end(); ++elSI, ++bestI)
        {
            elSI->second.navelFrom->navelCopy = std::move(bestI->navelFrom->navelCopy);
            delete bestI->navelFrom;
        }
        currentNavelI->navelCopy = bestCurrentNavel;
    }

    return isImproved;
}

void TamarAlgorithm::searchClosestFirstHullEdges(std::list<Node*> &originalInnerNodes, Way *hull, std::list<Node*> &fromNodes, NavelsMapType &navels)
{
    Node *innerLastNode = nullptr;
    Node *innerToNode = nullptr;
    Node *neighbourRootFromNodeLast = nullptr;
    NavelsZippedType *startRoot = nullptr;
    for (Node *innerNode : originalInnerNodes)
    {
        /// find the closest edge
        ClosestEdges ce = closestEdge(hull, innerNode);

        /// insert second and third closest to fromNodes.
        int count = 0;
        for (auto &edgeT : hull->getOrigin()->getBestFoundEdges())
        {
            ++count;
            if (count > 1 && count < 4)
            {
                bsp::Edge *ed = const_cast<bsp::Edge*>(edgeT.second);
                Node *node = ed->startPoint;
                node = const_cast<Node*>(Way::nextChild(node));
                if (node == ed->endPoint)
                {
                    fromNodes.push_back(ed->startPoint);
                } else {
                    fromNodes.push_back(ed->endPoint);
                }
            } else if (count == 1)
            {
                fromNodes.push_back(ce.fromNode);
            } else {
                break;
            }
        }

        /// insert closest edge to navel.
        double backDist;
        double neighbourDist;
        if (innerLastNode != nullptr && startRoot != nullptr)
        {
            neighbourDist = seboehDist(innerLastNode, innerToNode, innerNode);
            // DECIDE: we check all startI nodes, because we care not the current pointer.
            // Additionally it is helpful, if points are aligned correctly already.
            // HINT: Decide: we walk in clock-wise. So innerLastNode will be from and innerToNode will be to.
            auto backRes = findBestSingleSearch(*startRoot, ce);
            // HINT: backDist can be double::max in next line, but startRoot is never empty here!
            backDist = std::get<1>(backRes);
        } else {
            neighbourDist = std::numeric_limits<double>::max();
            neighbourRootFromNodeLast = ce.fromNode;
            backDist = std::numeric_limits<double>::max();
        }

        /// decide
        bool isBack = false;
        Node *rootFromNode = ce.fromNode;
        if (neighbourDist < ce.dist)
        {
            if (neighbourDist < backDist)
            {
                ce.fromNode = innerLastNode;
                ce.toNode = innerToNode;
                ce.dist = neighbourDist;
                rootFromNode = neighbourRootFromNodeLast;
            } else {
                isBack = true;
            }
        } else if (backDist < ce.dist)
        {
            isBack = true;
        }		// else ce.dist
        if (startRoot != nullptr && rootFromNode == startRoot->front().fromNode)
        {
            isBack = true;
        }

        /// insert
        auto findI = navels.find(rootFromNode);
        if (findI == navels.end())
        {       // no navel found -> create new navel
            NavelsZippedType navelStrang;
            assert(rootFromNode == ce.fromNode);
            navelStrang.push_back(ce);
            navels.emplace( NavelsMapType::value_type(ce.fromNode, std::move(navelStrang)) );
            neighbourRootFromNodeLast = ce.fromNode;
            if (startRoot == nullptr)
            {
                startRoot = &(navels.begin()->second);
            }
        }  else if (neighbourDist < backDist)
        {
            if (neighbourDist < ce.dist)
            {
                ce.fromNode = innerLastNode;
                if (ce.fromNode->point.x() == 360)
                    int i=5;
                ce.toNode = innerToNode;
                ce.dist = neighbourDist;
                assert(rootFromNode == ce.fromNode);
                findI->second.push_back(ce);
            } else {        // ce.dist
                findBestSingleCeAndAdd(findI->second.back().toNode, findI->second.front().fromNode, ce, findI->second
                                       , nullptr, true);
                assert(rootFromNode == findI->second.front().fromNode);
            }
        } else if (!isBack) {
            // ce.dist
            assert(rootFromNode == findI->second.front().fromNode);
            findBestSingleCeAndAdd(findI->second.back().toNode, findI->second.front().fromNode, ce, findI->second
                                   , nullptr, true);
            // HINT: findBestSingleCeAndAdd(findI->second.front().toNode, findI->second.front().fromNode, ce, findI->second);
            // also possible
        } else {
            assert(startRoot != nullptr);
            // TODO [seboeh] is it necessary to improve the findBest, not to use the old method?
            findBestSingleCeAndAdd(startRoot->back().toNode, startRoot->front().fromNode, ce, *startRoot
                                   , nullptr, true);
            // back=true - has no rootFromNode set! assert(rootFromNode == startRoot->front().fromNode);
        }

        TamarDebugging::debugIsInNavels(navels);

        /// iteration
        if (!isBack)
        {
            innerLastNode = innerNode;
            innerToNode = ce.toNode;
        }
    }       // end for walk over originalInnerNodes.

    /// reorder from nodes.
    /// delete doubled entries from fromNodes.
    ///    HINT: do not sort on Node* !!! This will completely destroy the sorting algorithm.
    std::map<Node*,bool> foundNodes;
    for (auto elI = fromNodes.begin();
         elI != fromNodes.end(); )
    {
        auto iter = foundNodes.find(*elI);
        if (iter != foundNodes.end())
        {
            elI = fromNodes.erase(elI);
        } else {
            foundNodes.insert({*elI, true});
            ++elI;
        }
    }
    foundNodes.clear();

    /// order on hull
    std::list<Node*> fromNodesOrdered;
    // find start
    const Node *startNode = &hull->begin();     // TODO [seboeh] here we can walk with iterator on hull. instead of nextChild...
    auto currentNavelFirstNode = fromNodes.front();
    while (startNode != currentNavelFirstNode)
        startNode = Way::nextChild(startNode);
    // order entries
    auto node = startNode;
    do
    {
        auto iter = std::find(fromNodes.begin(), fromNodes.end(), node);
        if (iter != fromNodes.end())
        {
            fromNodesOrdered.push_back(*iter);
            fromNodes.erase(iter);
        }
        node = Way::nextChild(node);
    } while (!fromNodes.empty() && node != startNode);

    fromNodes = std::move(fromNodesOrdered);

    /// update empty navels
    for (const auto &nodeP : fromNodes)
    {
        auto iter = navels.find(nodeP);
        if (iter == navels.end())
        {       // insert empty navel strang.
            NavelsZippedType navelStrang;
            navels.emplace( NavelsMapType::value_type(nodeP, std::move(navelStrang)) );
        }
    }
}

void TamarAlgorithm::wipeForwardBackward(NavelsMapType &navels, std::list<Node*> &fromNodes)
{
    bool isChanged = true;
    while (isChanged)
    {
        isChanged = false;

        for (auto fromNodeI = fromNodes.begin();
             fromNodeI != fromNodes.end(); ++fromNodeI)
        {
            NavelsMapType::iterator navelI = navels.find(*fromNodeI);
            //if (navelI == navels.end() || navelI->second.empty())
            if (navelI == navels.end())
            {
                std::cerr << "Error: millicious error occured. fromNode not in map. continuing ..." << std::endl;
                continue;
            }
            // get current hull-navel and next hull-navel.
            NavelsMapType::mapped_type *currentNavelRoot = &navelI->second;
            Node *currentNavelFrom = navelI->first;
            // next line is for the case, that currentNavelRoot is empty.
            Node *currentNavelTo = currentNavelFrom->next;      // equal to .., because from on hull. const_cast<Node*>(Way::nextChild(currentNavelFrom));

            NavelsMapType::mapped_type *nextNavelRoot = nullptr;
            if (!getNextNavel(navels, navelI, fromNodes, nextNavelRoot, fromNodeI))
            {
                break;
            }
            Node *nextNavelFrom = navelI->first;
            Node *nextNavelTo = nextNavelFrom->next;        // equal to: const_cast<Node*>(Way::nextChild(nextNavelFrom));

            if (currentNavelRoot->empty() && nextNavelRoot->empty())
                continue;

            /// correct order of currentNavelRoot and nextNavelRoot for new calculation

            std::list<ClosestEdges> newNextNavelRoot = *nextNavelRoot;
            std::list<ClosestEdges> newCurrentNavelRoot = *currentNavelRoot;

            /// calculate forward wipe
            if (wipe(currentNavelFrom, currentNavelTo, nextNavelFrom, nextNavelTo, &newNextNavelRoot, &newCurrentNavelRoot, true))
            {
                *currentNavelRoot = std::move(newCurrentNavelRoot);
                *nextNavelRoot = std::move(newNextNavelRoot);
                isChanged = true;
			}

			/// output the navels - debug
			TamarDebugging::debugApp2016_print(navels, m_para, m_indicators);       // else, it is not the debug app 2016


			newNextNavelRoot = *nextNavelRoot;
			newCurrentNavelRoot = *currentNavelRoot;

			/// calculate backward wipe
            if (wipe(currentNavelFrom, currentNavelTo, nextNavelFrom, nextNavelTo, &newNextNavelRoot, &newCurrentNavelRoot, false))
            {
                *currentNavelRoot = std::move(newCurrentNavelRoot);
                *nextNavelRoot = std::move(newNextNavelRoot);
                isChanged = true;
			}

			/// output the navels - debug
			TamarDebugging::debugApp2016_print(navels, m_para, m_indicators);       // else, it is not the debug app 2016
        }

    }
}

std::list<TamarAlgorithm::Node*>
TamarAlgorithm::selectNavels(Way *hull, std::list<Node*> &originalInnerNodes)
{
	NavelsMapType navels;
    // HINT: formNodes are necessary to keep an order on hull-edges. The navels map does not keep the order, because of the kind of map.
    std::list<Node*> fromNodes;
    // go through inner nodes and find navels, which has closest dist to hull.

    /// collect relevant fromNodes
    long hullLevel = hull->getOrigin()->m_level;
    auto wayIter = hull->begin();
    while (!wayIter.end())
    {
        Node *fromNode = wayIter.nextChild();
        if (std::abs(static_cast<long>(fromNode->level) - hullLevel) < 3)
        {
            fromNodes.push_back(fromNode);
        }
    }

    /// wipe forward, backward until no change
//    wipeForwardBackward(navels, fromNodes);

	/// wipe close and open.
//    wipeFoldingOpenAndInitiateClose(originalInnerNodes, navels, fromNodes);

    return std::move(fromNodes);
}


void TamarAlgorithm::removeNodeFromPolygon(Way *hull, Node *node, int stateWithoutNewEdge)
{
    hull->eraseNode(node, stateWithoutNewEdge);
    node->next = nullptr;
    node->debugInfo = 2;
	node->prev = nullptr;
	if (node->child != nullptr)
	{
		node->child->next = nullptr;
		node->child->prev = nullptr;
		node->isChildNode = false;
		node->child = nullptr;
	}
}

void TamarAlgorithm::removeFromPolygon(ClosestEdges &ce, Way* innerPointWay)
{
    assert(ce.insertNode != nullptr);
    // fromNode is old edge in hull, and not in innerPointWay.
    if (ce.fromNode != nullptr && ce.fromNode->origin != nullptr)
    {
        ce.fromNode->origin->removeFromEdgeLists(ce.fromNode);
    } else {
            // try to remove prevNode to insertNode edge, by prevChild
            // see test59 - not the issue longer; solved by OlavList::removeEdge()
            // for savety still in.
        auto prevNode = const_cast<Node*>(Way::prevChild(ce.insertNode));
        if (prevNode != nullptr)
        {
            prevNode->origin->removeFromEdgeLists(prevNode, ce.insertNode);
        }
    }
    if (ce.insertNode->origin != nullptr)
    {
        ce.insertNode->origin->removeFromEdgeLists(ce.insertNode);
        if (ce.fromNode != nullptr)
        {
            const Node *nextN = Way::nextChild(ce.fromNode);
            if (nextN != nullptr)
                ce.insertNode->origin->removeFromEdgeLists(ce.insertNode, nextN);
        }
        innerPointWay->getOrigin()->removeFromEdgeLists(ce.insertNode);     // TODO [seboeh] is it the same as one line above?
    }
    if (ce.insertNode->isChildNode)
    {
        removeNodeFromPolygon(innerPointWay, ce.insertNode, 1);     // 1, because only on innerPointWay - TODO [seboeh] fix this method,
        // ... it is necessary to delete all edges here and insert a new one, or it is not necessary to call removeNodeFromPolygon();
    } else {
        // for the case, that we use removeFromPolygon also for convex-hull nodes in insertNode.
        // then fromNode and toNode would be nullptr's.
        auto iter = Way::iterator(ce.insertNode);
        innerPointWay->erase( iter, 1 );
    }
}

double TamarAlgorithm::calcLengthOfPolygonFromNavelsAndHull(NavelsType &navels, Way *hull)
{
    auto hullI = hull->begin();
    double length = 0.0;
    const Node *current = nullptr;
    while (!hullI.end())
    {
        if (current == nullptr)
            current = hullI.nextChild();
        const NavelsZippedType *ceList = nullptr;
        for (const Navel &navel : navels)
        {
            if (navel.anchorNode == current)
            {
                ceList = navel.navelRef;
                break;
            }
        }
        const Node *next = hullI.nextChild();
        // walk through ceList
        if (ceList != nullptr && !ceList->empty())
        {
            for (const auto &ce : *ceList)
            {
                const Node *next = ce.insertNode;
                length += m_para.distanceFnc(current->point, next->point);
                current = next;
            }
            length += m_para.distanceFnc(current->point, ceList->back().toNode->point);
        } else {
            length += m_para.distanceFnc(current->point, next->point);
        }
        // iterate
        current = next;
    }
    length += m_para.distanceFnc(current->point, hullI.nextChild()->point);
    return length;
}

void TamarAlgorithm::_normalizeByRange(std::map<double, Node*> &in)
{
    double min = std::numeric_limits<double>::max();
    double max = 0;
    for (const auto &tuple : in)
    {
        if (min > tuple.first)
            min = tuple.first;
        if (max < tuple.first)
            max = tuple.first;
    }
    double range = max - min;
    if (range == 0) range = 0.000001;
    std::map<double, Node*> newOrder;
    for (auto &tuple : in)
    {
        double weightNormalized = (tuple.first - min) / range;
        while (newOrder.find(weightNormalized) != newOrder.end())
            weightNormalized += 0.000001;
        newOrder.insert({weightNormalized
                         , tuple.second});
    }

    in = std::move(newOrder);
}

std::list<Node*> TamarAlgorithm::convexSorting(std::list<Node*> &innerPoints, Way &hull)
{
    assert(hull.size() > 1);
    if (innerPoints.size() < 3)
        return innerPoints;

    // description
    // 1. assumption weight of outer points
    std::map<double, Node*> newInnerPointsOrder;
    for (Node* node : innerPoints)
    {
        double alignDenom = 0;
        double alignSum = 0;

        Way::iterator xI = hull.begin();
        Node* currentNode = xI.nextChild();
        Node* startNode = currentNode;
        Node* next = nullptr;
        size_t edgePos = 0;
        while (next != startNode)
        {
            ++edgePos;
            next = xI.nextChild();
            double dist = seboehDist(currentNode, next, node);
            if (dist == 0)
                dist = 0.000001;

            double fact = std::log2(1 + 1/dist);
            alignSum += edgePos * fact;
            alignDenom += fact;

            currentNode = next;
        }

        double weightAlign = alignSum/alignDenom;
        while (newInnerPointsOrder.find(weightAlign) != newInnerPointsOrder.end())
            weightAlign += 0.000001;
        newInnerPointsOrder.insert({weightAlign
                                    , node});
    }
    _normalizeByRange(newInnerPointsOrder);


    // 2. assumption weight of inner points
    std::map<double, Node*> newInnerPointsOrderIntern;
    for (Node *currentNode : innerPoints)
    {
        double alignSum = 0;

        for (const auto &node : innerPoints)
        {
            if (currentNode == node)
                continue;
            for (const auto &node2 : innerPoints)
            {
                if (node == node2
                        || currentNode == node2)
                    continue;
                double dist = seboehDist(node, node2, currentNode);
                if (dist == 0)
                    dist = 0.000001;

                alignSum += std::log2(1 + 1/dist);
            }
        }

        while (newInnerPointsOrderIntern.find(alignSum) != newInnerPointsOrderIntern.end())
            alignSum += 0.000001;
        newInnerPointsOrderIntern.insert({alignSum
                                         , currentNode});
    }
    _normalizeByRange(newInnerPointsOrderIntern);


    // 3. correction of position
    Point center(0,0);
    for (const auto &node : innerPoints)
    {
        center += node->point;
    }
    size_t s = innerPoints.size();
    center = Point(center.x()/s, center.y()/s);

    double minLen = std::numeric_limits<double>::max();
    double maxLen = 0;
    for (const auto &node : innerPoints)
    {
        double dist = m_para.distanceFnc(center, node->point);
        if (minLen > dist)
            minLen = dist;
        if (maxLen < dist)
            maxLen = dist;
    }
    double rangeLen = maxLen - minLen;
    if (rangeLen == 0) rangeLen = 0.000001;

    // becaus 0 is not natural, minWeight. 4.0, because an half avg weight is enough to prevent zero.
    double minWeight_4 = 1.0 / innerPoints.size() / 4.0;
    std::map<double, Node*> newOrderMixed;
    for (const auto &order : newInnerPointsOrder)
    {
        auto newIPIiter = std::find_if(newInnerPointsOrderIntern.begin(), newInnerPointsOrderIntern.end()
                                       , [order](const std::pair<double, Node*> val)
        {
            return val.second == order.second;
        });

        double weight = (m_para.distanceFnc(center, order.second->point) - minLen) / rangeLen;
        // becaus 0 is not natural
        weight += minWeight_4;
        weight /= (1 + 2*minWeight_4);

        // weight and 1-weight used as opposite probability
        // -1 at hull , +1 at center
        double weightCombined = -weight * order.first  +  (1-weight) * newIPIiter->first;
        // because close to 0 many values, due to average, the cos() below
        // not necessary in sorting - weightCombined = std::cos(weightCombined * M_PI);
        while (newOrderMixed.find(weightCombined) != newOrderMixed.end())
            weightCombined += 0.000001;
        newOrderMixed.insert({ weightCombined
                             , newIPIiter->second});
    }

    std::list<Node*> newInnerPoints;
    for (auto &pair : newOrderMixed)
        newInnerPoints.push_back(pair.second);

    _savePoints(newInnerPoints);

    return newInnerPoints;
}

void TamarAlgorithm::_savePoints(std::list<Node*> points)
{
    std::string filename = "savedPoints.csv";
    std::ofstream out;
    out.open(filename, std::ios_base::out);
    for (const auto &node : points)
    {
        out << node->point.x() << "x" << node->point.y() << ";";
    }
    out << std::endl;
    Node *prev = points.front();
    for (const auto &node : points)
    {
        if (node == prev)
            continue;
        out << std::fixed << m_para.distanceFnc(prev->point, node->point) << ";";
        prev = node;
    }
    out << std::fixed << m_para.distanceFnc(points.back()->point, points.front()->point) << ";" << std::endl;
    out.flush();
    out.close();
}


std::list<Point> TamarAlgorithm::hirachicalSorting(const std::list<Point> &points)
{

    // create tree backward
    std::map<double, HirarchicalNode::Edge> leafs;
    auto pointsBeginI = points.begin();
    for (; pointsBeginI != points.end(); ++pointsBeginI)
    {
        auto pointI = pointsBeginI;
        ++pointI;
        for (; pointI != points.end(); ++pointI)
        {
            leafs.insert({  m_para.distanceFnc(*pointsBeginI, *pointI)
                             , {*pointsBeginI, *pointI}});
        }
    }

    // create leaf childs
    std::list<HirarchicalNode*> firstLevel;
    for (const auto &leaf : leafs)
    {
        HirarchicalNode *node = new HirarchicalNode();
        node->value= leaf.first;
        node->points = leaf.second;
        firstLevel.emplace_back(std::move(node));
    }

    // ... will not work, because swapping from one side to another, already at first level

    return std::list<Point>();
}

std::list<Node*> TamarAlgorithm::rotatedSorting(const std::list<Node*> &nodes, Way &hull)
{
    // needed by test90, but failed test87
    // and test90, test91 fixed by sortedMerge_resortIntoHull()
    Point centerHull(0,0);
    Way::iterator xI = hull.begin();
    Node* currentNode = xI.nextChild();
    Node* startNode = currentNode;
    Node* next = nullptr;
    size_t elN = 0;
    while (next != startNode)
    {
        centerHull += currentNode->point;
        next = xI.nextChild();
        ++elN;

        currentNode = next;
    }
    centerHull /= elN;
    // TODO improve, by using existing center of previous ways.
    double maxLengthHull = 0;
    xI = hull.begin();
    while (!xI.end())
    {
        double dist = m_para.distanceFnc(centerHull, xI.nextChild()->point);
        if (dist > maxLengthHull)
            maxLengthHull = dist;
    }

    // because guaranties better derivation along the circle?
    // not working, due to test90
    Point centerInner(0,0);
    for (const auto &node : nodes)
        centerInner += node->point;
    centerInner /= nodes.size();
    double maxLengthInner = 0;
    for (const auto &node : nodes)
    {
        double dist = m_para.distanceFnc(centerInner, node->point);
        if (dist > maxLengthInner)
            maxLengthInner = dist;
    }

    // see test91, test90, but still failed test87
    Point center = (centerInner*maxLengthHull + centerHull*maxLengthInner) / (maxLengthHull + maxLengthInner);
    //center = centerHull;     // for test90
    //center = centerInner;     // for test91

    std::map<double, Node*> sortedNodes;
    for (const auto &node : nodes)
    {
        Point relP = node->point - center;
        double key = std::atan2(static_cast<double>(relP.x()), relP.y());
        while (sortedNodes.find(key) != sortedNodes.end())
            key += 0.00001;
        sortedNodes.insert({
                                key
                                , node});
    }

    std::list<Node*> newNodes;
    for (const auto &el : sortedNodes)
        newNodes.push_front(el.second);     // make counter-clockwise to clockwise

    _savePoints(newNodes);

    TSP::Way w;
    for (const auto &node : newNodes)
    {
        w.push_back(node->point);
    }

    m_tree.root()->children()->clear();
    m_tree.root()->addChild(w);

    return newNodes;
}

void TamarAlgorithm::_setHullVector(Node *node, Way &hull)
{
    auto hullI = hull.begin();
    Node *prevHull = &hullI;     // HINT: hull.begin() is always not a NEW_SORTED_MERGE_ELEMENT
                                        // HINT: because it is always a convex-hull-point.

    Point hullVectorMax(0,0);
    Point hullVectorSecond(0,0);
    Point hullVectorThird(0,0);
    double maxFactor = 0;
    double secFactor = 0;
    double thirdFactor = 0;
    while (!hullI.end())
    {
        Node *nodeHull = &(++hullI);
        if (prevHull == nodeHull)
            continue;
        Point hullCenter = (nodeHull->point + prevHull->point) / 2.0;
        double vectorLength = m_para.distanceFnc(hullCenter, node->point);
        double vectorWeight = m_para.distanceFnc(prevHull->point, nodeHull->point);
        // HINT: vectorAreaSide is only an approximation
        double vectorAreaSide = std::min(m_para.distanceFnc(prevHull->point, node->point)
                                         , m_para.distanceFnc(nodeHull->point, node->point));

        // node = c prevHull = a nodeHull = b
        double c = m_para.distanceFnc(prevHull->point, nodeHull->point);
        double a = m_para.distanceFnc(prevHull->point, node->point);
        double b = m_para.distanceFnc(nodeHull->point, node->point);
        double s = 0.5 * (a+b+c);
        double h = 2.0/c * std::sqrt(s*(s-a)*(s-b)*(s-c));

        double c_1 = std::sqrt( a*a - h*h );
        Point delta = nodeHull->point - prevHull->point;
        Point c1p ( prevHull->point.x() + delta.x()*c_1/c
                   , prevHull->point.y() + delta.y()*c_1/c);

        double ap_c1 = m_para.distanceFnc(prevHull->point, c1p);
        double bp_c1 = m_para.distanceFnc(nodeHull->point, c1p);

        // area * average-distance-squar
//        double gravityArea = bp_c1*h*0.5 * (((a*a) + (h*h))/2.0)
//                           + ap_c1*h*0.5 * (((b*b) + (h*h))/2.0);
        double x = c1p.x();
        double y = c1p.y();
        double x2 = node->point.x();
        double y2 = node->point.y();
        double gravityArea = std::sqrt((x-x2)*(x-x2) + (y-y2)*(y-y2));

        double factor = (c*c / (gravityArea * gravityArea));

        if (factor > maxFactor)
        {
            hullVectorThird = hullVectorSecond;
            thirdFactor = secFactor;
            hullVectorSecond = hullVectorMax;
            secFactor = maxFactor;
            hullVectorMax = (c1p - node->point);
            maxFactor = factor;
        }
        if (factor > secFactor && maxFactor != factor)
        {
            hullVectorThird = hullVectorSecond;
            thirdFactor = secFactor;
            hullVectorSecond = (c1p - node->point);
            secFactor = factor;
        }
        if (factor > thirdFactor && maxFactor != factor && secFactor != factor)
        {
            hullVectorThird = (c1p - node->point);
            thirdFactor = factor;
        }

        // iterate
        prevHull = nodeHull;
    }

    node->hullVector = hullVectorMax;       // max, because test99
    node->hullVectorSecond = hullVectorSecond;      // second achse of main achse transformation (MAT)
    node->hullVectorThird = hullVectorThird;
}

void TamarAlgorithm::sortedMerge_resortIntoHull(Way &hull)
{
    bool end = false;
    while (!end)
    {
        auto hullI = hull.begin();
        Node *prevHull = &hull.begin();     // HINT: hull.begin() is always not a NEW_SORTED_MERGE_ELEMENT
                                            // HINT: because it is always a convex-hull-point.
        bool wasFound = false;
        while (!hullI.end() && !wasFound)
        {
            Node *node = hullI.nextChild();
            if (node->candidateState == Node::CandidateState::NEW_SORTED_MERGE_ELEMENT)
            {       // insert in alternative hull.
                auto hullNewI = hull.begin();
                Node *curr = hullNewI.nextChild();
                Node *startingNode = curr;
                Node *next = nullptr;
                double distBest = std::numeric_limits<double>::max();
                Node *fromBest = nullptr;
                do
                {
                    next = hullNewI.nextChild();
                    if (//curr->candidateState != Node::CandidateState::NEW_SORTED_MERGE_ELEMENT - omitted, because test87
                        // HINT: next->candidateState != ... is omitted, because this gives more possibilities. TODO check if necessary.
                            curr != node
                            && next != node
                        )
                    {
                        double dist = seboehDist(curr, next, node);
                        if (dist < distBest)
                        {
                            fromBest = curr;
                            distBest = dist;
                        }
                    }

                    // iterate
                    curr = next;
                } while (startingNode != curr);

                if (fromBest != nullptr)
                {
                    auto nextI = hullI;
                    Node *next = nextI.nextChild();
                    double distBefore = seboehDist(prevHull, next, node);

                    if (distBest < distBefore)
                    {
                        hull.eraseNode(node);

                        ClosestEdges ce(node);
                        ce.fromNode = fromBest;
                        ce.toNode = const_cast<Node*>(Way::nextChild(ce.fromNode));
                        insertNewNodeToEdge(&hull, ce);
                        wasFound = true;
                        // next setting is a possiblity to change this algorithm to running
                        // multiple times over a hull, till no change happens.
                        node->candidateState = Node::CandidateState::NONE;     // means checked.
                    }
                    else
                    {
                        wasFound = false;
                    }
                }
                else
                {
                    wasFound = false;
                }

            }       // if, is NEW_SORTED_MERGE_ELEMENT

            // iterate - while hull ...
            if (!wasFound)
                prevHull = node;
        }       // while hull...
        if (!wasFound)
            end = true;
    }   // while n^2 loop
}

void TamarAlgorithm::navelZipoIgelMerge(Way *hull, ClosestEdgesListInputType &innerPoints)
{
    /// recursive call of TSP-Algo for innerPoints.
    /// This is the main feature. It enlarge the devide and conquer approach.
    /// enlarge the complexity to log(n)^2.

    // prepare - clean up           -  TODO [seboeh] do we need this?
//    InputCitiesPointerType innerPointsAsCityPoints;
//    for (const auto &innerPoint : innerPoints)
//    {
//        innerPointsAsCityPoints.push_back(&(innerPoint->insertNode->point));
//    }

    /**
     * devide and conquer
     */
    // create bsp tree - divide step
//    bsp::BspTreeSarah bspInnerPointTree = createBSPTree(innerPointsAsCityPoints);

//    // create points order - merge
//    auto root = bspInnerPointTree.root();
//    if (root == nullptr) {
//        std::cout << "Error: could not generate a new root for the innerPoints. Result not 100% correct. Running without some points." << std::endl;
//        return;
//    }

    // conquer
    // TODO [seboeh] check test19 -
//    Way * innerPointWay = newConqueredWay(root);
/*    if (innerPointWay == nullptr)
    {
        std::cout << "Error: innerPoint way not created. Merge cancelded. Result is not 100% correct. Running futher with missing points." << std::endl;
        return;
    }
*/

    Way * innerPointWay = new Way();
    innerPointWay->setOrigin(new bsp::BspNode());
    // alternative to newConqueredWay(root)
    Point pCenter(0,0);
    for (const auto &sharedCE : innerPoints)
    {
        innerPointWay->push_back( new Node(sharedCE->insertNode->point) );
        pCenter += sharedCE->insertNode->point;
    }
    pCenter = Point(pCenter.x()/innerPoints.size()
                  , pCenter.y()/innerPoints.size());
      // does not work for test19 - flipWay(innerPointWay, pCenter);
//    innerPointWay->flipDirection();     // essential for test19 - due to insertBestReorderedPos() works only clock-wise.

    /// apply navel, zipping and igel merge here..
    /// This is the general heart part of TSP Merge.

    // debugging
    // TamarDebugging::getInstance()->printDebugFile(hull, innerPointWay, hullPosition->point, navelPosition->point);

    if (innerPoints.size() == 1)
    {
        /// choose embrio
        /// integrate one point if innner points only size == 1.
        // find best position by seboehDist
        Node *from, *to;
        double distBest = std::numeric_limits<double>::max();
        Node *node = innerPoints.front()->insertNode;
        Way::iterator xI = hull->begin();
        Node* currentNode = xI.nextChild();
        Node* startNode = currentNode;
        Node* next = nullptr;
        while (next != startNode)
        {
            next = xI.nextChild();
            double dist = seboehDist(currentNode, next, node);
            if (dist < distBest)
            {
                distBest = dist;
                from = currentNode;
                to = next;
            }
            currentNode = next;
        }

        ClosestEdges ce(node);
        ce.fromNode = from;
        ce.toNode = to;
        insertNewNodeToEdge(hull, ce);

        return;

    } else {
		/// find navel length to limit merge of ziping and igel (igel is an animal) to that length

        // find best navel position. Sort with edge-quad-tree. Find in n*log(n) steps.
        Node* hullPosition;
        Node* navelPosition = getNavelPosition(innerPointWay, hull, hullPosition);
        assert(hullPosition != nullptr);
        Node *nextHullPosition = const_cast<Node*>(Way::nextChild(hullPosition));

/*
        Node *currentInnerNode = navelPosition;
		Node *left = hullPosition;
		Node *right = nextHullPosition;
		Node *last = currentInnerNode;

        /// calculate embrio length
        double embrioLength = m_para.distanceFnc(hullPosition->point, currentInnerNode->point);
		embrioLength += m_para.distanceFnc(currentInnerNode->point, nextHullPosition->point);
        // don't do this, because it is not added.: embrioLength -= m_para.distanceFnc(hullPosition->point, nextHullPosition->point);

        std::list<ClosestEdges> embrio;
		ClosestEdges ce(currentInnerNode);
		ce.fromNode = hullPosition;
		ce.toNode = nextHullPosition;
		ce.dist = seboehDist(ce);
		embrio.push_back(ce);
		while ((currentInnerNode=const_cast<Node*>(Way::nextChild(currentInnerNode))) != navelPosition)
		{
			double distleft =  seboehDist(left, last, currentInnerNode);
			double distright = seboehDist(right, last, currentInnerNode);
			ClosestEdges ce(currentInnerNode);
			if (distleft < distright)
			{
				ce.fromNode = left;
				ce.toNode = last;
				ce.dist = distleft;
				right = last;
			} else {
				ce.fromNode = last;
				ce.toNode = right;
				ce.dist = distright;
				left = last;
			}
            embrio.push_back(ce);
            // HINT: seboehDist is here aquivalent with a+b-c,
            // and because above we calculate a way with c, which is dropped now,
            // a+b-c is correct regarding the total length.
            embrioLength += seboehDist(ce);
			last = currentInnerNode;
		}

        /// calculate rest way on hull. Necessary to compair with wiping later.
        /// Because wipe inlcudes all close other hull-edges.
        // FIXME: Is it possible to save the current total way length in hull/way object?
		Node *lastHull = nextHullPosition;
		while ((nextHullPosition=const_cast<Node*>(Way::nextChild(nextHullPosition)))!=hullPosition)
		{
			embrioLength += m_para.distanceFnc(lastHull->point, nextHullPosition->point);
			lastHull = nextHullPosition;
		}
		embrioLength += m_para.distanceFnc(lastHull->point, hullPosition->point);

*/
		/// copy inner polygon, for later reuse in embrio creation.
        /// Because, the new created nodes have to be deleted, if we use the embrio version.
        std::list<Node*> innerPolygon;
        // find first navelPosition
        Node *startInnerNode = nullptr;
        Way::iterator innerPointI;
        for (innerPointI = innerPointWay->begin();
                 !innerPointI.end(); )
        {
                if ((startInnerNode = innerPointI.nextChild()) == navelPosition)
                {
                        break;
                }
        }
        Node *currentInnerCopyNode = startInnerNode;

        /**
         * zipo / wiping
         */
        /// preparation for rest of solutions - all innerPolygon nodes are innerNodes.
        do {
            Node *nNew = new Node(currentInnerCopyNode->point);
            hull->createNodeChild(nNew);
            innerPolygon.push_back(nNew);
            currentInnerCopyNode = innerPointI.nextChild();
        } while (currentInnerCopyNode != startInnerNode);

        /// calculate the zipping and igel merge.
        // The word zipo is here in conjunction to a zipo at clothes.
        // The close nodes forms a triangle. And those triangles do align each other.
        auto fromNodes = selectNavels(hull, innerPolygon);
/*'
        /// wipe all fromNodes-edges. All, because in sorting, we have to walk through all n-elements to find best order.
        /// HINT: Wiping all in wipeAll() guaranties best 100% matching/sorting in merge of border-elements.
//        wipeAll(navels, fromNodes);

        /// Necessary for test55.
        /// copy current navelsMap to NavelsType. navelsMap will be updated by &el.second pointer.
        m_navels.clear();
        for (auto &el : navels)
        {
            // HINT: need to be a push_back, because else, el.second would be moved.
            m_navels.push_back(Navel(&el.second, el.first, correctDist(el.second)));
            m_navels.back().navelCopy = *m_navels.back().navelRef;
        }
/*
        // TODO [seboeh] check if necessary?
        wipeCandidateAll(1);

        /// Candidate and Backtracking algorithm. See test57.png
        // TODO [seboeh] this method has to be clarified with logic, because of there candidate measurements.
        // TODO [seboeh] check if necessary?
        wipeAllCandidatesWithBacktracking();

        /// divergate with wipeDivergenzeCloseSingle. See test58.png
        // HINT: wipeDivergenzeCloseSingle is kind of postprocessing.
        wipeCandidateAll(2);

        /// divergate with wipeCandidateCloseAll. See test60.png and test63.png
        wipeCandidateAll(3);
*/
        /// calculate zipo length
//        double zipoLength = calcLengthOfPolygonFromNavelsAndHull(m_navels, hull);
        double zipoLength = std::numeric_limits<double>::max();


        // HINT: direct insert of innerPoints, because test19.
        SortedMerge sm;
        SortedMerge::output_value outputSM;
        double sortedMergeLength = std::numeric_limits<double>::max();
        comdat::Result outputES;
        double expSearchLength = std::numeric_limits<double>::max();
        if (!m_isCanceled)
        {
            if (m_para.algoType == Parameters::AlgorithmType::Tamar)
            {
                /**
                 * tsp 2d sorted_merge
                 */

                /// solve inner TSP
                InputCitiesPointerType innerPointsAsCityPoints;
                for (const auto &innerPoint : innerPoints)
                {
                    innerPointsAsCityPoints.push_back(&(innerPoint->insertNode->point));
                }
                // create bsp tree - divide step
                bsp::BspTreeSarah bspInnerPointTree = createBSPTree(innerPointsAsCityPoints);

                // create points order - merge
                auto root = bspInnerPointTree.root();
                if (root == nullptr) {
                    std::cerr << "Error: could not generate a new root for the innerPoints. Result not 100% correct. Running without some points." << std::endl;
                    return;
                }

                Way * innerPointWay = newConqueredWay(root);
                if (innerPointWay == nullptr)
                {
                    std::cerr << "Error: innerPoint way not created. Merge cancelded. Result is not 100% correct. Running futher with missing points." << std::endl;
                    return;
                }

                std::list<Node*> innerHullNodes;
                std::list<Node*> innerNodes;
                double innerLength = 0;
                auto wayI = innerPointWay->begin();
                while (!wayI.end())
                {
                    Node *node = wayI.nextChild();
                    if (!node->isChildNode)
                        innerHullNodes.push_back(node);
                    innerNodes.push_back(node);
                }
                Node* prev = innerHullNodes.front();
                for (const auto &node : innerHullNodes)
                {
                    innerLength += m_para.distanceFnc(prev->point, node->point);
                    prev = node;
                }
                innerLength += m_para.distanceFnc(prev->point, innerHullNodes.front()->point);

                /// set the hull-vector to nodes
                for (const auto &node : innerNodes)
                {
                    _setHullVector(node, *hull);
                }

                /// debug show way window
                m_tree.root()->children()->clear();
                TSP::Way w;
                for (const auto &node : innerNodes)
                {
                    node->point.hullVector = new Point(node->hullVector);
                    node->point.hullVectorSecond = new Point(node->hullVectorSecond);
                    node->point.hullVectorThird = new Point(node->hullVectorThird);
                    w.push_back(node->point);
                }
                m_tree.root()->addChild(w);

                // move innerNodes to innerPoints, used in Sorted-Merge-Search
                assert(innerNodes.size() == innerPoints.size());
                auto nodeI = innerNodes.begin();
                for (const auto &ce : innerPoints)
                {
                    ce->insertNode = *nodeI;
                    ++nodeI;
                }

                innerPoints.front()->insertNode->origin = hull->getOrigin();        // used inside sm.search(), see test25
                sm.m_isCanceled = m_isCanceled;
                sm.m_fimHullNodes = innerHullNodes;
                sm.m_fimCrossedMax = static_cast<size_t>(std::ceil((innerPoints.size()-innerHullNodes.size()) / 2.0));        // round up
                sm.m_fimMaxInnerLength = 2 * innerLength;
                sm.m_fimSimpleAlignment = innerPoints.size() > 7 && (innerPoints.size()-innerHullNodes.size()) > 1;
                sm.m_startTime = m_startTime;
                std::tie(outputSM, sortedMergeLength) = sm.search(fromNodes, innerPoints);
            }
            else if (m_para.algoType == Parameters::AlgorithmType::TamarExp)
            {
                std::list<Node*> innerNodes;
                for (const auto pce : innerPoints)
                {
                    innerNodes.push_back(pce->insertNode);
                }

                expsea::ExponentSearch es;
                //std::tie(outputES, expSearchLength) = es.search(es.findHullEdges(*hull, innerNodes, m_maxdistAB * std::sqrt(2.0)), innerNodes);
                std::tie(outputES, expSearchLength) = es.search(fromNodes, innerNodes);
            }

            m_isCanceled = sm.m_isCanceled;
        }
        else
        {
            // write simplest sorting, which is no sorting
            ClosestEdges ce;
            ce.fromNode = fromNodes.front();
            ce.toNode = const_cast<Node*>(Way::nextChild(ce.fromNode));
            for (const auto &pce : innerPoints)       //with rotate..() newInnerNodes
            {
                ce.insertNode = pce->insertNode;
                insertNewNodeToEdge(hull, ce);
                ce.fromNode = ce.insertNode;
            }
            return;
        }

        /**
         * Decision between zipo, embrio and knapSack solution.
         * HINT: Test19 shows that knapSack get other solution than zipo.
         */
        /// decide to choose the method with shortest path.
        /// This will lead to choosing navel, or zip-igel method.
        // because of innerPointWay->size can be smaller than real, outputKS.size() could be bigger.
//        if (sortedMergeLength < embrioLength && sortedMergeLength < zipoLength)
//        {
        if (sortedMergeLength < expSearchLength)
        {
            /// insert nodes
            for (const auto &smEl : outputSM)
            {
                ClosestEdges ce;
                ce.fromNode = smEl.hullEdgeFrom;
                ce.toNode = const_cast<Node*>(Way::nextChild(ce.fromNode));
                for (Node* node : smEl.innerNodes)
                {
                    ce.insertNode = node;
                    ce.insertNode->candidateState = Node::CandidateState::NEW_SORTED_MERGE_ELEMENT;
                    insertNewNodeToEdge(hull, ce);
                    ce.fromNode = ce.insertNode;
                }

                /// debug show way window
                TSP::Way w;
                for (const auto &node : smEl.innerNodes)
                {
                    w.push_back(node->point);
                }
                m_tree.root()->addChild(w);

            }

            sortedMerge_resortIntoHull(*hull);
        }
        else
        {
            /// insert nodes
            for (const auto &outEl : outputES)
            {
                ClosestEdges ce;
                ce.fromNode = outEl.hullEdgeFrom;
                ce.toNode = const_cast<Node*>(Way::nextChild(ce.fromNode));
                for (Node* node : outEl.innerNodes)
                {
                    ce.insertNode = node;
                    insertNewNodeToEdge(hull, ce);
                    ce.fromNode = ce.insertNode;
                }

                /// debug show way window
                TSP::Way w;
                for (const auto &node : outEl.innerNodes)
                {
                    w.push_back(node->point);
                }
                m_tree.root()->addChild(w);
            }

        }
/*
        } else if (zipoLength <= embrioLength && !navels.empty())
//        if (zipoLength <= embrioLength && !navels.empty())
        {
            /// insert nodes
            TamarDebugging::debugFindWrongOrder(m_navels);
            for (auto &navelRoot : m_navels)
            {
                for (ClosestEdges &ce : *navelRoot.navelRef)
                {
                    // HINT: not ce.insertNode->child != nullptr; because the node is further a childNode.
                    // TODO [seboeh] necessary? removeFromPolygon(ce, innerPointWay);

                    insertNewNodeToEdge(hull, ce);
                }
            }
        } else {
                // link navel - embrio
            /// insert/create new embrio.
            // HINT: Don't do this here, is done in insertNewNodeToEdge: hullPosition->origin->removeFromEdgeLists(hullPosition);
            ClosestEdges ce(*innerPolygon.begin());
            ce.fromNode = hullPosition;
            ce.toNode = nextHullPosition;
            ce.dist = seboehDist(ce);
            insertNewNodeToEdge(hull, ce);
            // not necessary to do: innerPointWay->eraseNode(navelPosition);
            // because we use copies of Point and delete innerPointWay below.
            // HINT: On using embrio, every node is aligned on edge (hullPosition,nextChild).
            if (innerPolygon.size() > 1)
            {
                // HINT: walk backwards, because innerPolygon in clockwise.
                // And we need the other way around inside the convex-hull.
                auto innerNodeI = --innerPolygon.end();
                for (; innerNodeI != innerPolygon.begin(); --innerNodeI)
                {
                    ce.fromNode = ce.insertNode;
                    ce.insertNode = *innerNodeI;
                   ce.dist = seboehDist(ce);
                    // toNode is still nextHullPosition.
                    insertNewNodeToEdge(hull, ce);
                }
            }
        }   // end else length check between embrio and zipo
*/

        delete innerPointWay;
    }   // end else innerPointWay count == 1

    /// clean the way, because the way is integrated now in hull.
// TODO [seboeh]  ....delete, check if possible
    //    innerPointWay->clear();
//    delete innerPointWay;
// TODO check if delete is possible
//    innerPointWay->clear();
//    delete innerPointWay;
}

/// HINT: because of structure of fromNodes and navles are seperate, we can only wipe between hull-edges, which are not empty.
void TamarAlgorithm::wipeAll(NavelsMapType &navelsMap, std::list<Node*> &fromNodes)
{
    /// wipe every close(fromNode) edge until no change
    bool isChanged = true;
    while (isChanged)
    {
        isChanged = false;

        for (auto fromNodeI = fromNodes.begin();
             fromNodeI != fromNodes.end(); ++fromNodeI)
        {
            Node *fromNode = *fromNodeI;
            auto iter = navelsMap.find(fromNode);
            if (iter == navelsMap.end())
                continue;

            NavelsZippedType *backList = &(iter->second);

            /// HINT: This loop is necessary for test54
            for (auto fromNodeOther : fromNodes)
            {
            // It is only necessary to look on next edge for wiping.
            // Essential in wipeAll is the repeat of isChanged.
//            auto fromNodeOtherI = fromNodeI;
//            if (fromNodeI == --fromNodes.end())
//            {
//                fromNodeOtherI = fromNodes.begin();
//            } else {
//                ++fromNodeOtherI;
//            }
//            Node *fromNodeOther = *fromNodeOtherI;
                if (fromNodeOther == fromNode)
                    continue;

            // find other navel, corresponding to fromNodeOther.
            auto iterOther = navelsMap.find(fromNodeOther);
            if (iterOther == navelsMap.end())
                continue;

            NavelsZippedType *frontList = &(iterOther->second);
            // TODO [seboeh] check if necessary?
            if (frontList->empty())
                continue;

            // get current hull-navel and next hull-navel.
            NavelsMapType::mapped_type *currentNavelRoot = backList;
            Node *currentNavelFrom = fromNode;
            // if currentNavelRoot is empty, we have although a toNode with nextChild().
            Node *currentNavelTo = const_cast<Node*>(Way::nextChild(fromNode));

            NavelsMapType::mapped_type *nextNavelRoot = frontList;
            Node *nextNavelFrom = fromNodeOther;
            Node *nextNavelTo = const_cast<Node*>(Way::nextChild(fromNodeOther));

            /// We have to copy the navels, because we want to undo the wipe().
            std::list<ClosestEdges> newNextNavelRoot = *nextNavelRoot;
            std::list<ClosestEdges> newCurrentNavelRoot = *currentNavelRoot;

            /// calculate forward wipe
            if (!newCurrentNavelRoot.empty() && wipe(currentNavelFrom, currentNavelTo, nextNavelFrom, nextNavelTo, &newNextNavelRoot, &newCurrentNavelRoot, true))
            {
                *currentNavelRoot = std::move(newCurrentNavelRoot);
                *nextNavelRoot = std::move(newNextNavelRoot);

                TamarDebugging::debugIsInNavels(newCurrentNavelRoot);
                TamarDebugging::debugIsInNavels(newNextNavelRoot);

                isChanged = true;
            } else {
                // undo wipe().
                newCurrentNavelRoot.clear();
                newNextNavelRoot.clear();
            }

//            /// output the navels - debug
//            TamarDebugging::debugApp2016_printWait(navelsMap, m_para, m_indicators);       // else, it is not the debug app 2016

            newNextNavelRoot = *nextNavelRoot;
            newCurrentNavelRoot = *currentNavelRoot;

            /// calculate backward wipe - necessary for test48
            if (!newNextNavelRoot.empty() && wipe(currentNavelFrom, currentNavelTo, nextNavelFrom, nextNavelTo, &newNextNavelRoot, &newCurrentNavelRoot, false))
            {
                *currentNavelRoot = std::move(newCurrentNavelRoot);
                *nextNavelRoot = std::move(newNextNavelRoot);

                TamarDebugging::debugIsInNavels(newCurrentNavelRoot);
                TamarDebugging::debugIsInNavels(newNextNavelRoot);

                isChanged = true;
            } else {
                // undo wipe().
                newCurrentNavelRoot.clear();
                newNextNavelRoot.clear();
            }

//            /// output the navels - debug
//            TamarDebugging::debugApp2016_printWait(navelsMap, m_para, m_indicators);       // else, it is not the debug app 2016

            }       // fromNodeOther

        }       // end first fromNode
    }       // end until no change

}



/*********** CANDIDATES BACKTRACKING start *********************/

std::tuple<TamarAlgorithm::ClosestNavel
, TamarAlgorithm::ClosestNavel
, TamarAlgorithm::ClosestNavel
, TamarAlgorithm::ClosestNavel>
TamarAlgorithm::findClosestEdgesFour(Node *anchorNode, Node *oppAnchorNode, Node *node)
{
    std::multimap<double, ClosestNavel> findNodes;
    ClosestEdges elCe(node);

    /// find closest in navels
    ClosestNavel foundPos;
	for (auto &otherNavel : m_navels)
    {
		if (otherNavel.anchorNode == anchorNode)
            continue;
		if (otherNavel.anchorNode == oppAnchorNode)
			continue;

        ClosestEdges newCe;
        auto res = findBestSingleCeAndAdd(
					otherNavel.anchorNode->next, otherNavel.anchorNode
                    , elCe
					, otherNavel.navelCopy
                    , &newCe
                    , true);
//        auto nextCollegeI = std::get<0>(res);

        // HINT: with ->next, it takes also one hull-edge from last navelNode into account. Which does not matter here.
		//double dist = newCe.dist + std::get<1>(res);
		double dist = newCe.dist;
//        bool isOk;
//        // TODO [seboeh] is it necessary to improve the gap calculation, doe to the point, that the sorting of ceList is different
//        // if the emplyee is deleted. It is not only dependend on nextCollegeI.
//        foundPos.gapOpp = calcGapInNavel(node, &nextCollegeI, otherNavel.navelCopy, isOk);
//		if (!isOk)
//		{
//            foundPos.gapOpp = dist;		// TODO ....improve here better 0?
//		}
		foundPos.dist = dist;
		foundPos.navel = &otherNavel;
		findNodes.insert({dist, foundPos});
    }

    /// select best ClosestRootNavel
	const size_t countMax = 4;
	ClosestNavel navels[countMax];
    int count = 0;
    for (auto &el : findNodes)
    {
        navels[count] = std::move(el.second);
        ++count;
		if (count == countMax)
            break;
    }

    return std::make_tuple(navels[0], navels[1], navels[2], navels[3]);
}


void TamarAlgorithm::sortAlignedOnEdge(SortNavelType &navelSorted, NavelsType::iterator &currentNavelI, NavelsType::iterator &oppositeNavelI)
{
	ClosestEdges newCe;
    for (auto &ce : currentNavelI->navelCopy)
    {
        // HINT: in oldMethod of findBestSingleCeAndAdd(), distAdditional is 0.
        auto res = findBestSingleCeAndAdd(oppositeNavelI->anchorNode->next, oppositeNavelI->anchorNode
                                          , ce, oppositeNavelI->navelCopy
                                          , &newCe, true);
        auto nextCollegeI = std::get<0>(res);
		auto el = SortNavelEl(&(*currentNavelI), newCe);
        el.navelTo = &(*oppositeNavelI);
        bool isOk = false;
//		el.gapOpp = calcGapInNavel(ce.insertNode, &nextCollegeI, oppositeNavelI->navelCopy, isOk);
		if (!isOk)
			el.gapOpp = newCe.dist;
        navelSorted.insert({newCe.dist, el});
    }
}

void TamarAlgorithm::sortAlingedOnEdgeBackward(SortNavelType &navelSorted, std::list<SortNavelEl> &returnCandidates, NavelsType::iterator &originalNavelI)
{
    ClosestEdges newCeP;
    for (SortNavelEl &retCandSNE : returnCandidates)
    {
        if (retCandSNE.ce.insertNode->candidateState != Node::CandidateState::CANDIDATE)
            continue;       // only candidates in view.
        findBestSingleCeAndAdd(originalNavelI->anchorNode->next, originalNavelI->anchorNode
                                          , retCandSNE.ce, originalNavelI->navelCopy
                               , &newCeP, true);
        auto el = SortNavelEl(retCandSNE.navelFrom, newCeP);
		auto *navelTo = retCandSNE.navelTo;
		auto retCandPosI = std::find_if(navelTo->navelCopy.begin(), navelTo->navelCopy.end()
										, [retCandSNE] (const ClosestEdges &val)
		{
			return retCandSNE.ce.insertNode == val.insertNode;
		});
		assert(retCandPosI != navelTo->navelCopy.end());
		el.gapOpp = calcGapInNavel(&retCandPosI, nullptr, navelTo->navelCopy);
		el.navelTo = retCandSNE.navelTo;
		// HINT: it is necessary to keep the old distance to navelTo - el.ce.dist = newCeP->dist;
		el.ce.dist = retCandSNE.ce.dist;
        navelSorted.insert({newCeP.dist, el});
    }
}

void TamarAlgorithm::rejectCandidateInCeList(Node *insertNode, NavelsZippedType &navelCeList)
{
	for (auto iter = navelCeList.begin();
		 iter != navelCeList.end(); ++iter)
	{
		if (iter->insertNode == insertNode)
		{
            //eraseFoundBestPos(iter, navelCeList);
            eraseFromCeList(iter, navelCeList);
			return;
		}
	}
}

void TamarAlgorithm::activateCandidateInCeList(Node *insertNode, std::list<ClosestEdges> &navelCeList)
{
    for (auto iter = navelCeList.begin();
         iter != navelCeList.end(); ++iter)
    {
        if (iter->insertNode == insertNode)
        {       // candidate found
            auto nextI = iter; ++nextI;
            if (nextI != navelCeList.end())
            {
                nextI->fromNode = iter->insertNode;
                nextI->dist = seboehDist(*nextI);
            }
            if (iter != navelCeList.begin())
            {       // HINT: necessary, because there are candidates before iter, and this leads to a gap.
                // see test12-2
                auto prevI = iter; --prevI;
                iter->fromNode = prevI->insertNode;
                iter->dist = seboehDist(*iter);
            }
            // HINT: prev --iter is correct, because toNode always point to Navels-Edge-toNode.
            iter->insertNode->candidateState = Node::CandidateState::ELECTED;
            break;      // There should be, only one candidate with unique insertNode in the list!
        }
    }
}

double TamarAlgorithm::calcGapInNavel(Node *insertNode, NavelsZippedType::iterator *nextEmployOldNavel, NavelsZippedType &oldNavel, bool &isOk)
{
	assert(nextEmployOldNavel != nullptr);		// it needs access to the oldNavel position.
	isOk = true;
	std::list<ClosestEdges> currentNavelPlaceholderList;
	ClosestEdges ce(insertNode);
	Node *fromNode = nullptr;
	if (*nextEmployOldNavel == oldNavel.end())
	{
		if (oldNavel.empty())
		{
			isOk = false;
			return 0;
		}
		ce.fromNode = oldNavel.back().insertNode;
		ce.toNode = oldNavel.back().toNode;
		if (ce.fromNode == insertNode)		// it is already in list
		{
			if (oldNavel.size() == 1)
			{		// only insertNode in list
				ce.fromNode = oldNavel.back().fromNode;
			} else {
				auto prevI = oldNavel.end(); --prevI; --prevI;
				ce.fromNode = prevI->insertNode;
			}
		}
        fromNode = ce.fromNode;
	} else {
        if ((*nextEmployOldNavel)->fromNode == insertNode)  // insertNode is in list already
		{
			if (*nextEmployOldNavel == oldNavel.begin())
			{
				isOk = false;
				return 0;
			} else {
				auto prevI = *nextEmployOldNavel; --prevI;
				fromNode = prevI->fromNode;
			}
        } else {
            fromNode = (*nextEmployOldNavel)->fromNode;
        }
	}

	// else, we need only insertNode from currentNavelPlaceholderList::iterator.
	currentNavelPlaceholderList.push_back(ce);
	auto iter = currentNavelPlaceholderList.begin();
	return calcGapInNavel(&iter, nextEmployOldNavel, oldNavel, fromNode);
}

static int debugCount = 0;


double TamarAlgorithm::calcNewLocalCosts2(NavelsZippedType::iterator *nextEmployOldNavelI, NavelsZippedType::iterator *leavingApplicantI, NavelsZippedType &oldNavel, Node *fromNode)
{
	if (*nextEmployOldNavelI != oldNavel.begin())
	{
		auto prevI = *nextEmployOldNavelI; --prevI;
		if (prevI->insertNode == (*leavingApplicantI)->insertNode)
		{
			if (prevI != oldNavel.begin())
			{
				--prevI;
			} else {
                assert(fromNode != nullptr);
                // HINT: nextEmployOldNavelI could be end(), therefore prevI->toNode
                return seboehDist(fromNode, prevI->toNode, (*leavingApplicantI)->insertNode);
			}
		}
		return seboehDist(prevI->insertNode, prevI->toNode, (*leavingApplicantI)->insertNode);
	} else {
        assert(fromNode != nullptr);
		return seboehDist(fromNode, (*nextEmployOldNavelI)->toNode, (*leavingApplicantI)->insertNode);
	}
}

double TamarAlgorithm::calcGapInNavel(NavelsZippedType::iterator *leavingApplicantI, NavelsZippedType::iterator *nextEmployOldNavelI
									  , NavelsZippedType &oldNavel, Node *fromNode)
{
	// Im Rahmen der Konvergenz des Candidaten-Systems, ist zu beachten, dass eine Lücke
	// im originalen Navel/(auch Unternehmen) entsteht, welche nicht durch die Gesamtkosten
	// betrachtet werden kann, sondern nur durch den zusätzlichen Aufwand des nächsten
	// Mitarbeiters/Points. Es kann nicht in den Gesamtkosten betrachtet werden, da die
	// Gesamtkosten immer sinken. Jedoch ist das Sinken mit zusätzlichem Aufwand des nächsten
	// Mitarbeiters zum Erhalt des Unternehmens einhergehend. So, dass der zusätzliche Aufwand
	// für die Konvergenz nicht ausgelassen werden kann.

	++debugCount;

	assert(leavingApplicantI != nullptr);
	assert(*leavingApplicantI != oldNavel.end());
	double gap = 0;
	if (nextEmployOldNavelI != nullptr && *nextEmployOldNavelI != oldNavel.end())
	{
        if (fromNode == nullptr && (*leavingApplicantI)->insertNode->candidateState == Node::CandidateState::CANDIDATE)
			fromNode = (*nextEmployOldNavelI)->fromNode;
		// HINT: because of (*nextEmployOldNavel)->toNode, leavingApplicationI, must not be in oldNavel.
		// HINT: nextEmployOldNavelI->fromNode, because leavingApplicantI is not inside the list.
		double newLocalCosts1 = seboehDist((*leavingApplicantI)->insertNode, (*nextEmployOldNavelI)->toNode, (*nextEmployOldNavelI)->insertNode);
		double newLocalCosts2 = calcNewLocalCosts2(nextEmployOldNavelI, leavingApplicantI, oldNavel, fromNode);
		gap = std::min(newLocalCosts1, newLocalCosts2);
	} else {
		// HINT: here, leavingApplicationI must not be in oldNavel!
        NavelsZippedType::iterator nextI;       // outside next if, because need valid in *nextEmployOldNavelI != oldNavel.end() compare
		if (nextEmployOldNavelI == nullptr)
		{
			// HINT: leavingApplicationI have to be in oldNavel now!
            nextI = *leavingApplicantI; ++nextI;
			nextEmployOldNavelI = &(nextI);
		}
        if (fromNode == nullptr)
            fromNode = oldNavel.front().fromNode;
		if (*nextEmployOldNavelI != oldNavel.end())
		{
			double newLocalCosts1 = seboehDist((*leavingApplicantI)->insertNode, (*nextEmployOldNavelI)->toNode, (*nextEmployOldNavelI)->insertNode);
			double newLocalCosts2 = calcNewLocalCosts2(nextEmployOldNavelI, leavingApplicantI, oldNavel, fromNode);
			gap = std::min(newLocalCosts1, newLocalCosts2);
		} else {
            double newLocalCost1 = seboehDist((*leavingApplicantI)->fromNode, (*leavingApplicantI)->toNode, (*leavingApplicantI)->insertNode);		// else, no costs for oldNavel at leaving in the moment.
            double newLocalCosts2 = calcNewLocalCosts2(nextEmployOldNavelI, leavingApplicantI, oldNavel, fromNode);
            gap = std::min(newLocalCost1, newLocalCosts2);
		}
	}
	return std::abs(gap);
}

bool TamarAlgorithm::wipeCandidateWithBacktracking(NavelsType::iterator &currentNavelI, NavelsType::iterator &oppositeNavelI, double &distCurrentWholeNavel)
{
    /// assumption
	if (currentNavelI->navelCopy.empty())
		return false;

    // HINT: TODO [seboeh] we have to programm on Node* instead of list::iterator, because findBestSingle reorders the list.

	/// output the navels - debug
	TamarDebugging::debugApp2016_print2(m_navels, m_para, m_indicators);

    // HINT: It is better to calculate with one opposite and one enemy, because the TSP is faster solved,
    // instead of fighting against all simultanious. Fightings are always one against one!

    /// sort according first closest on nextNavelRoot   GENERATE gS
    // gR := oppositeNavelI, uR := currentNavelI, Alginment := ceNewP->dist.
    // Die Punkte P von uR (hier 370x30) werden bezüglich ihres Abstandes (seboehDist) zu gR sortiert.
    // Dabei wird nur eine grobe Sortierung (gS) gemacht, welche nur das Alignement von p in P berücksichtigt,
    // welches aufgrund der aktuellen Situation an gR existiert.
    // D.h. es werden keine vorherigen p_i an gR aligned, um das nächste Alignement des nächsten p_i+1 zu bestimmen.
    m_oppositeNavelSorted.clear();
	sortAlignedOnEdge(m_oppositeNavelSorted, currentNavelI, oppositeNavelI);

	/// For all elements of gS      FORWARDTRACKING
    // HINT: Analog zu Adam Smith gilt: Wenn von allen genommen wird, was geht, und dies alle machen, dann ist es auch so,
    // das einem genommen wir, was einem nicht zusteht.

    TamarDebugging::debugFindWrongOrder(m_navels);
    // gA :=  , K(p) :=
    // Entlang der gS wird nun p_s aus gS gewählt, welches am nächsten zu gR liegt, und das Alignement an gR (gA),
    // sowie das Alignment an die altenativen vier Kanten K(p) bestimmt.
    // Das Alignement an K(p) wird hier aA bezeichnet. Mit aA_i(p) als die Kante mit i'ten Abstand zu p.
    std::list<SortNavelEl> candidates;
    for (auto &elS : m_oppositeNavelSorted)     // elS = element sorted := p
    {
		/// update opposite applicant dist
		auto *applicantCeAtNewNavel = &elS.second.ce;
		// HINT: applicantCeAtNewNavel is not inserted to navelTo. Therefore, the gapOpp is used.
		// HINT: it is recalculated the dist, because here we take already inserted opposite nodes into account.
		ClosestEdges newCe;
        auto res = findBestSingleCeAndAdd(oppositeNavelI->anchorNode->next, oppositeNavelI->anchorNode
                               , elS.second.ce, oppositeNavelI->navelCopy
                               , &newCe, true);
        auto appNewNavelNextCollegeIter = std::get<0>(res);
        double distAppNewNavel = newCe.dist;

		/// calculate gaps
		bool isOk = false;
//        double gapAppNewNavel = calcGapInNavel(newCe.insertNode, &appNewNavelNextCollegeIter, oppositeNavelI->navelCopy, isOk);
//		if (!isOk)
//			gapAppNewNavel = seboehDist(applicantCeAtNewNavel->fromNode, applicantCeAtNewNavel->toNode
//										, applicantCeAtNewNavel->insertNode);

		auto elSNFrom = elS.second.navelFrom;
		Node *applicant = elS.second.ce.insertNode;
		auto applicantPosI = std::find_if(elSNFrom->navelCopy.begin(), elSNFrom->navelCopy.end()
								 , [applicant](const NavelsZippedType::value_type &val)
		{
			return applicant == val.insertNode;
		});
		assert(applicantPosI != elSNFrom->navelCopy.end());
//		double gapAppOldNavel = calcGapInNavel(&applicantPosI, nullptr, elSNFrom->navelCopy);

        /// find four closest edges bA
		auto closestNavelsRes = findClosestEdgesFour(
					currentNavelI->anchorNode
					, oppositeNavelI->anchorNode
					, applicantCeAtNewNavel->insertNode);
        ClosestNavel closestEdges[4];       // := p_s
        std::tie(closestEdges[0], closestEdges[1], closestEdges[2], closestEdges[3]) = closestNavelsRes;

        /// select best alignment.
		std::vector<double> dists{
					distAppNewNavel
					, closestEdges[0].dist
					, closestEdges[1].dist
					, closestEdges[2].dist
					, closestEdges[3].dist };
		auto distI = std::min_element(dists.begin(), dists.end());
        size_t bestAIndex = std::distance(dists.begin(), distI);

        // Herrscht nun eine lokale Verbesserung, so wird p als "auserwählt" an aA aligned.
        // Herrscht keine lokale Verbesserung, so wird p als "Kandidat" an aA aligned.
        // Kandidaten werden in weiteren Alignements nicht berücksichtigt. Sie sind für später.

        /// remove from old position
		double distOldPos = applicantPosI->dist;
		eraseFromCeList(applicantPosI, elSNFrom->navelCopy);

		/// detect local improvement?
		// HINT: it is necessary to calc with candidates, because it is not immediatelly an improvement
		// of whole seboehDist over all navels, because there are only candidates, which are possibilities.
		// And they are not immediatelly an improvement of whole dist.
		if (bestAIndex == 0)
        {       // gA
            // HINT: Principle: posDist + gapAtPos - gapAtOpp
			if ((distAppNewNavel)
					<
				distOldPos)
			{       // local improvement -> elected
				applicant->candidateState = oko::Node::CandidateState::ELECTED;
			} else {
					// candidate
				applicant->candidateState = oko::Node::CandidateState::CANDIDATE;
				candidates.push_back(elS.second);
                candidates.back().ce.dist = distAppNewNavel;      // update used dist for new position
                candidates.back().gapOpp = 0;       // gapAppNewNavel;
			}
			/// insert to new position
			findBestSingleCeAndAdd(elS.second.navelTo->anchorNode->next, elS.second.navelTo->anchorNode
                                                 , elS.second.ce
                                                 , elS.second.navelTo->navelCopy
                                   , nullptr, true);
            TamarDebugging::debugFindWrongOrder(m_navels);
        } else {
                // one edge of bA is relevant.
			ClosestNavel bestA = closestEdges[bestAIndex-1];
            assert(bestA.navel != nullptr);
            elS.second.navelTo = bestA.navel;
			// HINT: distOldPos != elS.second.dist, because the dist is specified on navelTo.
			if ((bestA.dist)
					<
				distOldPos)
			{       // local improvement
				applicant->candidateState = Node::CandidateState::ELECTED;
			} else {
				applicant->candidateState = Node::CandidateState::CANDIDATE;
				candidates.push_back(elS.second);
                candidates.back().ce.dist = bestA.dist;
                candidates.back().gapOpp = 0;       // bestA.gapOpp;
			}
			/// insert to new position
            findBestSingleCeAndAdd(bestA.navel->anchorNode->next, bestA.navel->anchorNode
                                                 , elS.second.ce
                                                 , bestA.navel->navelCopy
                                   , nullptr, true);
            TamarDebugging::debugFindWrongOrder(m_navels);
        }

        // Es wird mit dem nächsten p aus gS fortgefahren, bis keine Punkte mehr in gS liegen.
    }       // loop gS

    TamarDebugging::debugFindWrongOrder(m_navels);
	TamarDebugging::debugApp2016_print2(m_navels, m_para, m_indicators);

	// Nun ist es so, dass wir Auserwählte haben und Kandidaten haben.
    // Es wird versucht per Backtracking die Kandidaten wieder an uR zu algin-en.
    /// backtracking
    // Dabei werden die Kandidaten C grob an die ursprüngliche Hull-Edge-Kante uR
    // (ursprünglicher Root ohne Punkte) sortiert cS.
    m_returnersNavelSorted.clear();
    sortAlingedOnEdgeBackward(m_returnersNavelSorted, candidates, currentNavelI);

    // Für jeden c aus cS wird nun ein bester Partner bP für das Alignment an uR gesucht.
    // Der Partner ist wiederum ein Punkt aus cS, welcher noch nicht verarbeitet wurde.
    for (auto &retCandSNE : m_returnersNavelSorted)     // m_returners... := cS
    {
        assert(retCandSNE.second.navelTo != nullptr);
		// if retCand is elected, it was previous a candidate for another retCand before.
		if (retCandSNE.second.ce.insertNode->candidateState == Node::CandidateState::ELECTED)
			continue;

		/// calculate leavingDist in opposite navels.
		/// find partner
        SortNavelEl *bestPartnerSNE = nullptr;
        double bestDistCommon = std::numeric_limits<double>::max();
        double distCommon = std::numeric_limits<double>::max();
		double bestGapPartnerCurr = std::numeric_limits<double>::min();
		for (auto &partnerSNE : m_returnersNavelSorted)
        {
            if (partnerSNE.second.ce.insertNode == retCandSNE.second.ce.insertNode)
                continue;
			// if the partnerSNE is already elected, it was a candidate for previous retCand. It is not in focus anymore.
			if (partnerSNE.second.ce.insertNode->candidateState == Node::CandidateState::ELECTED)
				continue;

            // try to insert first partner fix.
            auto res = findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                                   , partnerSNE.second.ce, currentNavelI->navelCopy
                                   , nullptr, true);
            auto partnerCeI = std::get<0>(res);
			// calc gap current
            // TODO [seboeh] is the gap calculation really necessary? Would it be better to resort with insertBestResortedPos()?
			double gapPartnerCurr = calcGapInNavel(&partnerCeI, nullptr, currentNavelI->navelCopy);

			// determine the position of applicant in original Navel.
            ClosestEdges newCe;
            findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                                   , retCandSNE.second.ce, currentNavelI->navelCopy
                                   , &newCe, true);

            // decision
            distCommon = partnerCeI->dist + newCe.dist;
            if (distCommon < bestDistCommon)
            {
                bestDistCommon = distCommon;
                bestPartnerSNE = &partnerSNE.second;
				bestGapPartnerCurr = gapPartnerCurr;
            }
            // delete first insert of partner
            // simple eraseFromCeList() is enough, because we use oldMethod above in findBestSingleCeAndAdd().
            eraseFromCeList(partnerCeI, currentNavelI->navelCopy);
        }

        /// correlation/covariance between returnCandidate and bestPartner according to originalNavel(=currentNavelI).
        // Es wird die Korrelation zwischen c und bP bezüglich uR berechnet.
        // D.h. es gibt drei Möglichkeiten c geht alleine an uR, c geht nicht an uR oder c geht mit bP an uR.

        // only c aligned on original Navel
        ClosestEdges newCe;
        auto res = findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                               , retCandSNE.second.ce, currentNavelI->navelCopy
                               , &newCe, true);
        auto retCandNextCollegePosI = std::get<0>(res);
        double distRetCandOnly = newCe.dist;

		/// calc gaps retCand current
		bool isOk;
        double gapRetCandCurr = calcGapInNavel(retCandSNE.second.ce.insertNode, &retCandNextCollegePosI, currentNavelI->navelCopy, isOk);
		if (!isOk)
			gapRetCandCurr = seboehDist(currentNavelI->anchorNode, currentNavelI->anchorNode->next, retCandSNE.second.ce.insertNode);

        // Die beste Möglichkeit wird gewählt.
        double distAppOppPos = retCandSNE.second.ce.dist;
        auto *navelTo = retCandSNE.second.navelTo;
        auto ce = retCandSNE.second.ce;
        auto appOppIter = std::find_if(navelTo->navelCopy.begin(), navelTo->navelCopy.end()
                                       , [ce] (const ClosestEdges &val)
        {
            return ce.insertNode == val.insertNode;
        });
        double gapAppOppNavel = calcGapInNavel(&appOppIter, nullptr, navelTo->navelCopy);

        /// Test on returning yes/no.
		if ( bestPartnerSNE != nullptr
				&&
			// check return common better than only retCand return?
             ((bestDistCommon + gapRetCandCurr + bestGapPartnerCurr)/2
				<
			 (distRetCandOnly + gapRetCandCurr))
				&&
			// check return common better than staying at opposite?
             ((bestDistCommon + gapRetCandCurr + bestGapPartnerCurr)/2       //test57: doubRet for 220x250 opp 370 curr 30
				<
              (distAppOppPos + gapAppOppNavel))     // + bestPartnerSNE->gapOpp))
		)
        {       // common return to original navel
            // delete from old opponent navels
            rejectCandidateInCeList(bestPartnerSNE->ce.insertNode, bestPartnerSNE->navelTo->navelCopy);
            // delete return candidate from potential opponents
            rejectCandidateInCeList(retCandSNE.second.ce.insertNode, retCandSNE.second.navelTo->navelCopy);
            // Der Kandidat c und evtl auch bP werden
            // nun als "auserwählt" markiert und an ihren besten Root aligned.
            // Here are the candidates elected back to original navel.
            bestPartnerSNE->ce.insertNode->candidateState = Node::CandidateState::ELECTED;
            // HINT: This is not improved, because both elements go back. - isImproved = true;
            retCandSNE.second.ce.insertNode->candidateState = Node::CandidateState::ELECTED;
            findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                                   , bestPartnerSNE->ce, currentNavelI->navelCopy
                                   , nullptr, true);
            findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                                   , retCandSNE.second.ce, currentNavelI->navelCopy
                                   , nullptr, true);
            TamarDebugging::debugFindWrongOrder(m_navels);
        } else if (
				   // only retCand return better than stay at opposite?
				   (distRetCandOnly + gapRetCandCurr)
					  <
                   (distAppOppPos + gapAppOppNavel) //test57:nothing,+->220 not at 140; + gapAppOldNavel)	// test57:+, 220x250 at 370
		)
        {       // single return to original navel
            rejectCandidateInCeList(retCandSNE.second.ce.insertNode, retCandSNE.second.navelTo->navelCopy);
            retCandSNE.second.ce.insertNode->candidateState = Node::CandidateState::ELECTED;
            findBestSingleCeAndAdd(currentNavelI->anchorNode->next, currentNavelI->anchorNode
                                   , retCandSNE.second.ce, currentNavelI->navelCopy
                                   , nullptr, true);
            TamarDebugging::debugFindWrongOrder(m_navels);
            // HINT: This is not improved, because the element under question goes back. - isImproved = true;
        } else {
                // no return to original navel -> insert as elected.
			activateCandidateInCeList(retCandSNE.second.ce.insertNode, retCandSNE.second.navelTo->navelCopy);
            TamarDebugging::debugFindWrongOrder(m_navels);
        }

        // Diese Schritte werden für c aus cS wiederholt.
    }       // returners navel order sorted

    // output the navels - debug
	TamarDebugging::debugApp2016_print2(m_navels, m_para, m_indicators);
    TamarDebugging::debugFindWrongOrder(m_navels);

    /// test on improved
    double distNewWhole = 0;
    for (const auto &elNav : m_navels)
    {
        distNewWhole += correctDistTMP_DELETE(elNav.navelCopy);
    }
    bool isImproved = distNewWhole < distCurrentWholeNavel;
    if (isImproved)
        distCurrentWholeNavel = distNewWhole;

    return isImproved;
}

void TamarAlgorithm::wipeAllCandidatesWithBacktracking()
{
    /// copy current navelsMap to NavelsType. navelsMap will be updated by &el.second pointer.
    double distCurrentWholeNavel = 0;
    for (auto &el : m_navels)
    {
        // HINT: need to be a push_back, because else, el.second would be moved.
        double dist = correctDistTMP_DELETE(el.navelCopy);
        distCurrentWholeNavel += dist;
    }

    /// wipe every close(fromNode) edge until no change
    bool isChanged = true;
    while (isChanged)
    {
        isChanged = false;

        for (auto navelI = m_navels.begin();
             navelI != m_navels.end(); ++navelI)
        {
            for (auto navelOpponentI = m_navels.begin();
                 navelOpponentI != m_navels.end(); ++navelOpponentI)
            {
                if (navelI->anchorNode == navelOpponentI->anchorNode)
                    continue;       // no position switch between same navel.

                /// calculate forward wipe
                if (wipeCandidateWithBacktracking(navelI, navelOpponentI, distCurrentWholeNavel))
				{
                    /// update with new results, including it's own fromNode-navel.
                    for (auto &el : m_navels)
                    {
                        *el.navelRef = el.navelCopy;
                        el.dist = correctDistTMP_DELETE(*el.navelRef);
                    }
                    isChanged = true;
                } else {
                    /// undo wipe().
                    for (auto &el : m_navels)
                    {
                        el.navelCopy = *el.navelRef;
                        el.dist = correctDistTMP_DELETE(el.navelCopy);
                    }
                }
            }       // second fromNode - fromNodeOpponentI

        }       // end first fromNode - fromNodeI
    }       // end until no change

}



/*********** CANDIDATES BACKTRACKING end *********************/

/*********** CANDIDATES OPEN ALL start **********************/

void TamarAlgorithm::wipeCandidateAll(size_t type)
{
    double bestDistFound = 0;
    for (auto navel : m_navels)
    {
        bestDistFound += correctDistTMP_DELETE_WITH_CANDIDATE(navel.navelCopy);
        // TODO [seboeh] or bestDistFound += navel.dist;
    }

    bool isChanged = true;
    while (isChanged)
    {
        isChanged = false;
        /// select current navel
        for (auto navelI = m_navels.begin();
             navelI != m_navels.end(); ++navelI)
        {
            /// close other to current navel
            if ( (type == 1 && wipeMultiple(navelI))
                 || (type == 2 && wipeDivergenzeCloseSingle(navelI))
                 || (type == 3 && wipeDivergenzeClose(navelI, bestDistFound))
                )
            {
                /// update
                for (auto &el : m_navels)
                {
                    *el.navelRef = el.navelCopy;
                    el.dist = correctDistTMP_DELETE(*el.navelRef);
                }
                isChanged = true;
            } else {
                /// else, restore
                for (auto &el : m_navels)
                {
                    el.navelCopy = *el.navelRef;
                    el.dist = correctDistTMP_DELETE(el.navelCopy);
                }
            }
        }
    }
}

bool TamarAlgorithm::wipeDivergenzeCloseSingle(const NavelsType::iterator &navelCurrentI)
{
    /// precondition
    if (navelCurrentI->navelCopy.empty())
        return false;

    /// over all points, search closest and align.
    bool isChanged = false;
    double distCurrOld = correctDistTMP_DELETE(navelCurrentI->navelCopy);
    for (auto &navel : m_navels)
    {
        if (navel.anchorNode == navelCurrentI->anchorNode)
            continue;
        if (navel.navelCopy.empty())
            continue;
        double distOppOld = correctDistTMP_DELETE(navel.navelCopy);
        // copy necessary for iteration in loop below
        auto navelCopyX = navel.navelCopy;
        for (auto ceXI = navelCopyX.begin();
             ceXI != navelCopyX.end(); ++ceXI)
        {
            auto ceI = std::find_if(navel.navelCopy.begin(), navel.navelCopy.end()
                                    , [ceXI] (const ClosestEdges &val)
            {
                return ceXI->insertNode == val.insertNode;
            });
            assert(ceI != navel.navelCopy.end());
            if (ceXI->insertNode->point.x() == 330
                    && navelCurrentI->anchorNode->point.x() == 30)
                int i=5;
            ClosestEdges ceNew(ceI->insertNode);
            auto res = findBestSingleCeAndAdd(navelCurrentI->anchorNode->next, navelCurrentI->anchorNode
                                                    , ceNew, navelCurrentI->navelCopy
                                              , nullptr, false);
            auto insertI = std::get<0>(res);
            eraseFoundBestPos(ceI->insertNode, navel.navelCopy);

            /// test on improvement
            // tmp_delete here necessary, because findBestSingle did change the size.
            double distOpp = correctDistTMP_DELETE(navel.navelCopy);
            double distCurr = correctDistTMP_DELETE(navelCurrentI->navelCopy);
            if ((distOpp + distCurr)
                    <
                (distOppOld + distCurrOld))
            {       // improved
                // repeat, iterated already.
                distOppOld = distOpp;
                distCurrOld = distCurr;
                isChanged = true;
            } else {
                /// else, reject
                findBestSingleCeAndAdd(navel.anchorNode->next, navel.anchorNode
                                       , ceNew, navel.navelCopy
                                       , nullptr, false);
                eraseFoundBestPos(insertI->insertNode, navelCurrentI->navelCopy);
            }
        }
    }
    return isChanged;
}

/*********** CANDIDATES OPEN ALL end ************************/


/*********** CANDIDATES CLOSE ALL START ************************/

bool TamarAlgorithm::wipeDivergenzeClose(const NavelsType::iterator &navelCurrentI, double &bestDistFound)
{
    /// assumptions / preconditions

    /// find alternative distance to navelCurrent of all nodes in all other navels.
    m_navelSorted.clear();

    Node *from = navelCurrentI->anchorNode;
    Node *to = navelCurrentI->anchorNode->next;
    Point currMiddle((from->point.x()+to->point.x())/2.0, (from->point.y()+to->point.y())/2.0);
    NavelsZippedType *navelCurrCopy = &navelCurrentI->navelCopy;
    for (Navel &navel : m_navels)
    {
        if (navel.navelCopy.empty()
            || navel.anchorNode == navelCurrentI->anchorNode)
            continue;
        for (auto ceI = navel.navelCopy.begin();
             ceI != navel.navelCopy.end(); )
        {
            /// insert as candidate to navelCurrent and calc better/correct dist in ceInserted.
            auto res = findBestSingleCeAndAdd(to, from, *ceI, *navelCurrCopy, nullptr);     // old with group align.
            SortNavelEl snEl(&navel, *std::get<0>(res));
            snEl.ce.insertNode->isCurrNode = true;
            m_navelSorted.insert({m_para.distanceFnc(ceI->insertNode->point, currMiddle)
                                  , snEl});
            ceI = navel.navelCopy.erase(ceI);
        }
    }

    if (m_navelSorted.empty())
        return false;

    /// reject candidate according to sorted candidate list.
    // calc dist with all inserted elements
    NavelsZippedType navelCurrRun = *navelCurrCopy;
    double bestDistCurr = correctDistTMP_DELETE_WITH_CANDIDATE(navelCurrRun);
    NavelsZippedType bestNavel = navelCurrRun;
    for (SortNavelType::iterator snMapElI = --m_navelSorted.end();
          true; --snMapElI)
    {
        SortNavelEl *snEl = &(snMapElI->second);
        // reject candidate snI
        // eraseFoundBestPos(snEl->ce.insertNode, navelCurrRun);
        eraseFoundBestPos(snEl->ce.insertNode, navelCurrRun);

        Navel *navelCandFrom = snEl->navelFrom;
        double distFromOld = correctDistTMP_DELETE_WITH_CANDIDATE(navelCandFrom->navelCopy);
        findBestSingleCeAndAdd(navelCandFrom->anchorNode->next, navelCandFrom->anchorNode
                               , snEl->ce, navelCandFrom->navelCopy, nullptr);      // old group align

        double distCurr = correctDistTMP_DELETE_WITH_CANDIDATE(navelCurrRun);
        double distFrom = correctDistTMP_DELETE_WITH_CANDIDATE(navelCandFrom->navelCopy);

        /// estimate improvement.
        if ((distCurr + distFrom)
                <
            (bestDistCurr + distFromOld))
        {
            for (auto &ce : bestNavel)
            {
                auto findI = std::find_if(navelCurrRun.begin(), navelCurrRun.end()
                                          , [ce] (const ClosestEdges &val)
                {
                    return ce.insertNode == val.insertNode;
                });
                if (findI == navelCurrRun.end())
                {
                    ce.insertNode->isCurrNode = false;
                }
            }
            bestDistCurr = distCurr;
            bestNavel = navelCurrRun;
        } else {
                // reinsert
            // old style - navelCurrRun.insert(posCurrOldI, updateBestResort(from, to, snEl->ce.insertNode, posCurrOldI, navelCurrRun));
            findBestSingleCeAndAdd(to, from, snEl->ce, navelCurrRun);
        }

        // iterate
        if (snMapElI == m_navelSorted.begin())
            break;
    }

    /// select as elected or reject to navelFrom.
    *navelCurrCopy = bestNavel;
    double distAllOtherNavels = 0;
    for (auto &navel : m_navels)
    {
        if (navel.navelCopy.empty()
            || navel.anchorNode == navelCurrentI->anchorNode)
            continue;
        // because, the navel.navelCopy will be resorted, while iterating, it is necessary to copy before iteration.
        NavelsZippedType navelX = navel.navelCopy;
        for (auto &ce : navelX)
        {
            if (ce.insertNode->isCurrNode)
            {
                // TODO [seboeh] speed-up - eraseFoundBestPos() is very slow.
                eraseFoundBestPos(ce.insertNode, navel.navelCopy);
                // old style - eraseFromCeList(ce.insertNode, navel.navelCopy);
            }
        }
        distAllOtherNavels += correctDistTMP_DELETE_WITH_CANDIDATE(navel.navelCopy);
    }

    /// fix test39 - fix correct fromNode for candidates, not already elected.
    // TODO [seboeh] test, if still necessary?
    Node *prevNode = from;
    for (ClosestEdges &ce : *navelCurrCopy)
    {
        ce.fromNode = prevNode;
        ce.insertNode->candidateState = Node::CandidateState::ELECTED;
        ce.dist = seboehDist(ce);
        prevNode = ce.insertNode;
    }

    /// return changed state
    double distCurrEnd = correctDistTMP_DELETE_WITH_CANDIDATE(*navelCurrCopy);
    if ((distCurrEnd + distAllOtherNavels) < bestDistFound)
    {
        bestDistFound = distCurrEnd + distAllOtherNavels;
        return true;
    } else {
        return false;
    }
}


/*********** CANDIDATES CLOSE ALL end ************************/


// TODO [seboeh] improvment, because current impl does not work. We need wipe on second-closest edes.!!!
std::tuple<TamarAlgorithm::Node*, TamarAlgorithm::Node*>
TamarAlgorithm::determineClosestNodesInCeList(NavelsZippedType &navel, NavelsZippedType::iterator &ceI)
{
    Node *from;
    if (ceI == navel.begin())
    {
        from = ceI->fromNode;
    } else {
        auto prevI = ceI; --prevI;
        from = prevI->insertNode;
    }
    Node *to;
    if (ceI == --navel.end())
    {
        to = ceI->toNode;
    } else {
        auto nextI = ceI; ++nextI;
        to = nextI->insertNode;
    }

    return std::tuple<TamarAlgorithm::Node*, TamarAlgorithm::Node*>(from, to);
}

void TamarAlgorithm::previousInserts(Way *way, Way::iterator &hullI)
{
    // See test
    // 1. look on prev edge for childs, fitting better to new hull edge hullI - hullI->next.
    // 2. look on next edge for childs, fitting better to new hull edge.
    // 3. move collected childs to hull with findBestSingleCeAndAdd()
}

void TamarAlgorithm::removeDoubleEntries(ClosestEdgesListInputType &innerNodes)
{
    ClosestEdgesListInputType newInnerNodes;
    for (const auto &n : innerNodes)
    {
        bool isMet = false;
        for (const auto &p : innerNodes)
        {
            if (p->insertNode == n->insertNode
                    || p->insertNode->point == n->insertNode->point)
            {
                if (isMet)
                {
                    isMet = false;      // error - point double
                    break;
                }
                isMet = true;
            }
        }
        if (isMet)
            newInnerNodes.push_back(n);
    }
    innerNodes = std::move(newInnerNodes);
}

void TamarAlgorithm::mergeCascada(Way::iterator haI, Way::iterator laI, Way::iterator hbI, Way::iterator lbI, Way* way1, Way *way2, Point pCenter)
{
    ClosestEdgesListInputType innerNodes;

    /// collect all nodes to realign again.
    // collect nodes, which are close to innerPoints. Checked with test28.
//    collectAllCloseInnerNodes(innerNodes, *way1);
    previousInserts(way1, haI);     // TODO [seboeh] correct code for test54 59 and 62

    Way::iterator xaI = haI;
    Node* endAC = &laI;
    // omit convex hull point
    // HINT: Guaranty at pyramid that all points will be inner points.
    xaI.nextChild();
    Node* currentNode;
    while ((currentNode = xaI.nextChild()) != endAC) {
        innerNodes.emplace_front(
                    std::make_shared<ClosestEdges>(currentNode));       // emplace_front, because it have to insert the innerpoint clock-wise.
        innerNodes.front()->insertNode->wayNum = 1;
    }

    previousInserts(way1, lbI);

    ClosestEdgesListInputType::iterator way2InsertIter = innerNodes.end();
    Way::iterator xbI = lbI;
    Node* endBC = &hbI;
    xbI.nextChild();
    // omit convex hull point
    // HINT: First convex hull point will not be realigned.
    while ((currentNode = xbI.nextChild()) != endBC) {
        way2InsertIter = innerNodes.emplace( way2InsertIter,
                    std::make_shared<ClosestEdges>(currentNode));
        (*way2InsertIter)->insertNode->wayNum = 2;
    }

    /// clear edges of convex-hull - have to be before redirect.
    if ((&haI)->next != (&haI)->prev)       // size > 2
    {
        (&haI)->origin->removeFromEdgeLists(&haI);
        Node *prevLaNode = const_cast<Node*>(Way::prevChild(&laI));
        if (prevLaNode != &haI)
        {
            prevLaNode->origin->removeFromEdgeLists(prevLaNode);
        }
    } else if (haI == laI && (&haI)->next != &haI)      // &haI->next because way should be bigger than one.
    {       // special case if way.size == 2 is orthogonal to other way -> point inside. See test12.png.
        (&haI)->origin->removeFromEdgeLists(&haI);
    }
    if ((&lbI)->next != (&lbI)->prev)
    {
        (&lbI)->origin->removeFromEdgeLists(&lbI);
        Node *prevHbNode = const_cast<Node*>(Way::prevChild(&hbI));
        if (prevHbNode != &lbI)
        {
            prevHbNode->origin->removeFromEdgeLists(prevHbNode);
        }
    } else if (hbI == lbI && (&hbI)->next != &hbI)
    {       // See test12.png and test10.png.
        (&hbI)->origin->removeFromEdgeLists(&hbI);
    }

    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    assert(!(&haI)->isChildNode);
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in mergeWays().

    if (innerNodes.empty())
    {
        // create edges for convex-hull merged points (haI,...)
        // HINT: This is inside innerNodes.empty() if, because
        // collectAllCloseInnerNodes will base on edge-tree.
        // Those lines are repeated alos below for other cases than innerNodes.empty().
        way1->getOrigin()->insertToEdgeLists(&haI, &hbI);
        way1->getOrigin()->insertToEdgeLists(&lbI, &laI);
        // see test79, see below, and principal of new level for merged points.
        size_t level = way1->getOrigin()->m_level;
        (&haI)->level = level;
        (&laI)->level = level;
        (&hbI)->level = level;
        (&lbI)->level = level;
        return;     // rest of code base only on innerNodes.
    }

    /// prepare merge
    way1->calculateCountOfConvexHullEdges();

    /// correct from and to's; and create new hull.
    int loopCount = 0;
    for (auto &ce : innerNodes)
    {
        // clear edges of innerNodes. Prev and next edge are in two directions.
        /// HINT: below is not necessary, because innerNodes are side-by-side in one line.
        /// ce->insertNode->prev->origin->removeFromEdgeLists(ce->insertNode->prev);
        // first remove, then createNodeChild(), because below createNodeChild deletes next pointer.
        // We risk to work simplier herer than that: const Node * nc = Way::nextChild(ce->insertNode);
        // HINT: because next pointer is not on next child, therefore on next hullnode, we need
        // to use nextChild() fnc ???
        const Node * nc = ce->insertNode->next;
        const Node * pc = ce->insertNode->prev;
        // TODO [seboeh] use other signals instead of next,prev == nullptr.
        if (nc != nullptr && pc != nullptr)     // not already deleted by collectAllCloseInner...()
        {
            // HINT: deletes insertNode to next node.
            ce->insertNode->origin->removeFromEdgeLists(ce->insertNode);
            // Therefore, we need delete pc also.
            // necessary for test39
            if (pc != &haI && pc != &lbI)
            {
                const auto prevN = Way::prevChild(ce->insertNode);
                if (prevN != nullptr)
                    prevN->origin->removeFromEdgeLists(prevN);
            }
            // necessary for test33
            // TODO ....delete TODO [seboeh] fix WORKAROUND test44, because the prev->next is not correct.
            if (loopCount == 1)
            {
                way1->getOrigin()->removeFromEdgeLists(pc, ce->insertNode);
            }
            ++loopCount;
            // HINT: it could be, that new inner polygons are created with nodes, which are not marked as childNode.
            // TODO [seboeh] improve in using only childNodes internally? - Although embrio navel?
            if (nc != &laI && nc != &hbI
                && pc != &haI && pc != &lbI)
            {       // check on nc and pc, because we changed the next and prev of pc->next, because pc was hull node.
                if (ce->insertNode->isChildNode)
                {
                    way1->eraseNode(ce->insertNode, 1);
                    // HINT: way1 is enough, way2 not necessary!
                } else {
                    auto iter = Way::iterator(ce->insertNode);
                    way1->erase( iter, 1 );
                }
            }
            // DONT DO THIS: This keeps the most right pointer: ce->insertNode->next = nullptr;
            // DONT DO THIS: This keeps the most left pointer: ce->insertNode->prev = nullptr;
            ce->insertNode->debugInfo = 3;
        }
        else
        {
            removeNodeFromPolygon(way1, ce->insertNode);
        }

        // create childNodes for innerNodes, to use them inside convex-hull.
        if (!ce->insertNode->isChildNode)
        {
            ce->insertNode->isChildNode = true;
            way1->createNodeChild(ce->insertNode);
            ce->insertNode->debugInfo = 7;
        }

        // used to detect inner nodes in display gui hmi
        ce->insertNode->point.number(ce->insertNode->point.number() + 1);
    }

    // create edges for convex-hull merged points (haI,...)
    if (!way1->getOrigin()->hasEdge(&haI, &hbI))        // TODO [seboeh] drop if, always include edge. somethhing must be wrong here.
        way1->getOrigin()->insertToEdgeLists(&haI, &hbI);
    if (!way1->getOrigin()->hasEdge(&lbI, &laI))
        way1->getOrigin()->insertToEdgeLists(&lbI, &laI);

    /// include close edges, which are already aligned - see test28
    // TODO [seboeh] deleting of removeNodeFromPolygon(&way, node);
    // in collectAllCloseInnerNodes();
    // is perhapse not the same as deleting above!
    collectAllCloseInnerNodes(innerNodes, *way1);

    // prevent double entries by collectAllCloseInnerNodes();. See test70
    removeDoubleEntries(innerNodes);

    /// reset level of nodes.
    // see test79 - this level reset is not in insertToEdgeLists(), because
    // removeEdge will also call insertToEdgeLists()
    // Here it is clear, that it comes to next level.
    size_t level = way1->getOrigin()->m_level;
    (&haI)->level = level;
    (&laI)->level = level;
    (&hbI)->level = level;
    (&lbI)->level = level;
    for (auto &ce : innerNodes)
    {
        ce->insertNode->level = level;
    }

    // divide into way 1 or 2
    Way::iterator wayIter(&laI);
    int wayNum = 1;
    while (!wayIter.end())
    {
        Node *fromNode = wayIter.nextChild();
        if (fromNode == &hbI)
            wayNum = 2;
        fromNode->wayNum = wayNum;
    }

    // for expSearch
    Point haP = (&haI)->point;
    Point hbP = (&hbI)->point;
    Point laP = (&laI)->point;
    Point lbP = (&lbI)->point;
    double dist = m_para.distanceFnc(haP, pCenter);
    if (dist > m_maxdistAB)
        m_maxdistAB = dist;
    dist = m_para.distanceFnc(hbP, pCenter);
    if (dist > m_maxdistAB)
        m_maxdistAB = dist;
    dist = m_para.distanceFnc(laP, pCenter);
    if (dist > m_maxdistAB)
        m_maxdistAB = dist;
    dist = m_para.distanceFnc(lbP, pCenter);
    if (dist > m_maxdistAB)
        m_maxdistAB = dist;

    /// sorting
    navelZipoIgelMerge(way1, innerNodes);
}

bool TamarAlgorithm::isClockwise(TSP::Point pCenter, Way *way)
{
    Way::iterator pI = way->begin();
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

void TamarAlgorithm::flipWay(Way *way, TSP::Point pCenter)
{
    if(!isClockwise(pCenter, way))
        way->flipDirection();
}

std::tuple<double, bool> TamarAlgorithm::triangleHeightC(const TSP::Point &ap, const TSP::Point &bp, const TSP::Point &cp)
{
    assert( ap != bp );
    double a = m_para.distanceFnc(bp, cp);
    double b = m_para.distanceFnc(ap, cp);
    double c = m_para.distanceFnc(ap, bp);
    double s = 0.5 * (a+b+c);

    double h = 2.0/c * std::sqrt(s*(s-a)*(s-b)*(s-c));

    // HINT: the exact point of c1 is not necessary, because obtuse charactization is enough.
//    double c_1 = std::sqrt( a*a - h*h );
//    Point delta = ap - bp;
//    Point c1p ( bp.x() + delta.x()*c_1/c
//               , bp.y() + delta.y()*c_1/c);

    // [https://magoosh.com/gmat/2013/gmat-math-how-do-you-find-the-height-of-a-triangle/]
    double alpha = std::acos( (c*c + b*b - a*a) / (2*b*c) );
    double beta = std::asin( b/a * std::sin(alpha) );
    // for special not necessary - special = check on height on AB - double gamma = M_PI - alpha - beta;
    double pi = 2 * std::acos(0);
    bool isObtuseSpecial = alpha > pi/2.0 || beta > pi/2.0;

    return std::make_tuple(h, isObtuseSpecial);
}

void TamarAlgorithm::_triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
                && pb == *lpbI
                && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
                && pb == *hpbI
                && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

void TamarAlgorithm::mergeWays(bsp::BspNode *origin, TamarAlgorithm::Way * &way1, const TSP::Point &pWay1Center, TamarAlgorithm::Way * &way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    origin->clearLastInserted();
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        Way::iterator w1I = way1->begin();
        Way::iterator fixI = way2->begin();
        if (isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            flipWay(way1, pCenter);
            return;
        } else {
            ++w1I;
            if (isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                flipWay(way1, pCenter);
                return;
            } else {
                ++w1I;
                if (isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    flipWay(way1, pCenter);
                    return;
                } else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    Way::iterator w1NI = w1I; ++w1NI;
                    Way::iterator w1NNI = w1NI; ++w1NNI;
                    if (isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        createTriangle3x1(way1, w1NNI, fixI);
                    } else if (isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        createTriangle3x1(way1, w1NI, fixI);
                    } else {
                        createTriangle3x1(way1, w1I, fixI);
                    }
                    flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        Way::iterator way1I = way1->begin();
        Way::iterator way2I = way2->begin();
        Way::iterator way1NI = way1I;
        ++way1NI;
        Way::iterator way2NI = way2I;
        ++way2NI;
        Way::iterator way1Nearest;
        Way::iterator way2Nearest;
        double maxDist=std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        Way::iterator way1Next = way1Nearest; ++way1Next;
        Way::iterator way2Next = way2Nearest; ++way2Next;
        if (isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next) == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            flipWay(way1, pCenter);
            return;
        } else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            } else  if (createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI ))
            {
            } else if (createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else if (createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2


    /** big Main part **/

    /// 1. beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (Way::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (Way::iterator wBI=way2->begin();
             !wBI.end(); ++wBI)
        {
            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// 9. find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    Way::iterator hpaI, hpbI, lpaI, lpbI;

    Point realCenter = calculateMeanPointOfWayCenter(way1, way2);       // TODO [seboeh] delete realCenter, due to similar to pCenter, see test73,test74,test75

    smallestDist /= 2;
    Point lastLowCrossPoint;
    Point lastHighCrossPoint;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x())-10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x())+10, pCenter.y()), &crossedPoint);
        else
            state = isCrossed(pa, pb, TSP::Point(pCenter.x(),std::min(pa.y(), pb.y())-10), TSP::Point(pCenter.x(),(std::max)(pa.y(), pb.y())+10), &crossedPoint);

        bool isLowNotHigh = isNormalVectorLeft(pb, pa, realCenter);

        double value;
        if (state != CrossedState::isOnPoint)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();
        }
        else
        {       // see test82, test76
            // see test89, test64
            Point otherP;
            if (pa == crossedPoint)
                otherP = pb;
            else
                otherP = pa;
            double otherVal;
            if (callState == MergeCallState::topDown)
                otherVal = otherP.x();
            else
                otherVal = otherP.y();


            // HINT: center of pa_pb is not working, because test76 cascading of points.
            Point descent = otherP - crossedPoint;
            descent /= m_para.distanceFnc(otherP, crossedPoint);

            if (callState == MergeCallState::topDown)
            {
                value = crossedPoint.x() + descent.x()*smallestDist;
            }
            else
            {
                value = crossedPoint.y() + descent.y()*smallestDist;
            }
        }

        bool isGood = false;
        if (isLowNotHigh)
        {
            if ((callState == MergeCallState::topDown
                    && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGood = true;
            }
            if (lastLowCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastLowCrossPoint = Point();
                }
            }
        }
        else
        {
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                    && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGood = true;
            }
            if (lastHighCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastHighCrossPoint = Point();
                }
            }
        }

        if (((state == CrossedState::isCrossed
                    || state == CrossedState::isOnPoint     // because, teset82
              )
                && isGood)          // TODO [seboeh] drop isGood here
           )
        {
            if (isLowNotHigh)
            {
                lpaI = edgeI->aNodeI;
                lpbI = edgeI->bNodeI;
            }
            else
            {
                hpaI = edgeI->aNodeI;
                hpbI = edgeI->bNodeI;
            }
        }
        // at least isOnPoint, because special case for height of triangle
        /*      omitted, due to test82
        else if (state == CrossedState::isOnPoint && isGood)
        {
            double h_c;
            bool isObtuseSpecial;
            std::tie(h_c, isObtuseSpecial) = triangleHeightC(pa, pb, realCenter);
            assert(h_c > 0);

            double maxHc;
            if (isLowNotHigh)
                maxHc = maxHcLow;
            else
                maxHc = maxHcHigh;

            if (h_c > maxHc
                    && !isObtuseSpecial)
            {
                if (isLowNotHigh)
                {
                    lpaI = edgeI->aNodeI;
                    lpbI = edgeI->bNodeI;
                    maxHcLow = h_c;
                }
                else
                {
                    hpaI = edgeI->aNodeI;
                    hpbI = edgeI->bNodeI;
                    maxHcHigh = h_c;
                }
            }
            else if (h_c > maxHc)
            {
                if (isLowNotHigh)
                {
                    lpaI = edgeI->aNodeI;
                    lpbI = edgeI->bNodeI;
                    maxHcLow = h_c;
                }
                else
                {
                    hpaI = edgeI->aNodeI;
                    hpbI = edgeI->bNodeI;
                }
            }
        }
        */

    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, hpaI, lpaI, hpbI, lpbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hpbI, lpbI, hpaI, lpaI);


    /// HINT: take respect of direction in B - way1 should be in correct direction already.
    flipWay(way1, pWay1Center);     // necessary for test-benny-1
    flipWay(way2, pWay2Center);     // necessary for test10-2

    /// 11. fit nodes of A and nodes of B according to seboehDist into new edge on top of convex hull
    mergeCascada(hpaI, lpaI, hpbI, lpbI, way1, way2, pCenter);

    flipWay(way1, pCenter);  // necessary, because code before keeps the clock-wise order.
}

TamarAlgorithm::DirectAccessState TamarAlgorithm::isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI)
{
    Way::iterator prevAI = pAI; --prevAI;
    Way::iterator nextAI = pAI; ++nextAI;
    Way::iterator prevBI = pBI; --prevBI;
    Way::iterator nextBI = pBI; ++nextBI;
    CrossedState state = isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool TamarAlgorithm::isCovered(Way * way, Way::iterator &w1I, Way::iterator &fixI)
{
    Way::iterator wI = way->begin();
    Way::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed || state == CrossedState::isCovered)
            return true;
		if (state == CrossedState::isOnPoint)
        {
            if (crossedPoint == *wI || crossedPoint == *wNI)
                return true;
        }		// else out of range
    }
    return false;
}

bool TamarAlgorithm::createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI)
{
    if (isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        Way::iterator w1NewI = way1->push_back(&way2I);
        Way::iterator w1NNewI = way1->push_back(&way2NI);

        if (dist1 < dist2 && dist1 < dist3)
        {
            way1->insertChild(w1Alt, new Node(point, &w1Alt, (&w1Alt)->next));      // TODO [seboeh] is new really necessary?
        } else if (dist2 < dist1 && dist2 < dist3)
        {
            way1->insertChild(w1NNewI, new Node(point, &w1NNewI, (&w1NNewI)->next));
        } else {
            way1->insertChild(w1NewI, new Node(point, &w1NewI, (&w1NewI)->next));
        }
        return true;
    }
    return false;
}

void TamarAlgorithm::createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    Way::iterator w1I = way1->begin();
    Way::iterator w1NI = w1I; ++w1NI;
    Way::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    if (dist1 < dist2 && dist1 < dist3)
    {
        way1->insertChild(w1I, new Node(point, &w1I, (&w1I)->next));
    } else if (dist2 < dist1 && dist2 < dist3)
    {
        way1->insertChild(w1NI, new Node(point, &w1NI, (&w1NI)->next));
    } else {
        way1->insertChild(w1NNI, new Node(point, &w1NNI, (&w1NNI)->next));
    }
}

bool TamarAlgorithm::isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    // barycentric coordinates.
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        return false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        return false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        return false;
    return true;
}

TamarAlgorithm::CrossedState TamarAlgorithm::isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint)
{
    /// Crossed between a and b?
    double x1 = pa1I.x();
    double y1 = pa1I.y();
    double x2 = pa2I.x();
    double y2 = pa2I.y();
    double x3 = pb1I.x();
    double y3 = pb1I.y();
    double x4 = pb2I.x();
    double y4 = pb2I.y();

    /// HINT: [http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection]
    double denominator = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (denominator == 0) {
        // lines are parallel not guarantied to lay on each other.
        // We want to integrate the considered node as inner node -> so it should not be covered -> parallel
        return CrossedState::isParallel;
    }
    double pX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denominator;
    double pY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denominator;
    if (crossPoint != nullptr)
        *crossPoint = TSP::Point(pX,pY);

    if ((std::max)(std::min(x1,x2),std::min(x3,x4)) <= pX && pX <= std::min((std::max)(x1,x2),(std::max)(x3,x4))
            && (std::max)(std::min(y1,y2),std::min(y3,y4)) <= pY && pY <= std::min((std::max)(y1,y2),(std::max)(y3,y4)))
    {
        if ((pX == x1 && pY == y1)          // FIX: Could be a speed-up, if we check if this crossing on exact one outer point could happen with the input.
            || (pX == x2 && pY == y2)
            || (pX == x3 && pY == y3)
            || (pX == x4 && pY == y4))
        {
            return CrossedState::isOnPoint;
        } else {
            return CrossedState::isCrossed;
        }
    } else {
        return CrossedState::isOutOfRange;
    }
}
