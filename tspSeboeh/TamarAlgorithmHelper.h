#pragma once

#include <list>

#include "dataStructures/OlavList.h"

// for further includes
#include "BaseFncs.h"       // todo: correct hack here in include-order

namespace TSP
{
typedef oko::ClosestEdges ClosestEdges;
typedef oko::OlavList WayH;
typedef oko::Node Node;

ClosestEdges closestEdge(WayH *hull, Node *innerNode);

Node* getNavelPosition(WayH *innerPointWay, WayH *hull, Node* &hullPosition);

}

namespace TSPHelper
{

typedef oko::OlavList WayH;
typedef oko::NodeChild NodeChild;
typedef oko::Node Node;
typedef oko::ClosestEdges ClosestEdges;
typedef TSP::Point Point;
typedef std::list<ClosestEdges> NavelsZippedType;

double seboehDist(const Node *p1, const Node *p2, const Node *pNew);

double seboehDist(const Point &p1, const Point &p2, const Point &pNew);

double seboehDist(ClosestEdges &ce);

double seboehHyperDist(const Point &p1, const Point &p2, const Point &pNew);

Point
    testPointsInOrder(NavelsZippedType::iterator thirdPointI, Node *secondPointN, Node *firstPointN);

std::list<ClosestEdges>::iterator
    findBestSingleCeAndAdd(Node *toNode, Node *fromNode, const ClosestEdges &element, std::list<ClosestEdges> &ceList);

double
    correctDistHelper(const std::list<ClosestEdges> &navelRoot);

std::list<ClosestEdges>::iterator
    insertBestResortedPos(Node *from, Node *to, Node *insert, std::list<ClosestEdges> &ceList, int level = 0);

std::tuple<std::list<ClosestEdges>::iterator
, std::list<ClosestEdges>::iterator>
    findFromTo(Node *insert, std::list<ClosestEdges> &ceList);

bool
    isNormalVectorLeft(Node *fromNode, Node* toNode, Node* insertNode);

std::tuple<std::list<ClosestEdges>::iterator, double>
    findBestSingleSearch(std::list<ClosestEdges> &navelRoot, const ClosestEdges &ce);

ClosestEdges
    updateBestResort(Node *fromNode, Node *toNode, Node *insert, NavelsZippedType::iterator &iter, NavelsZippedType &ceList, ClosestEdges *newCeP = nullptr);

bool
    isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);

}

