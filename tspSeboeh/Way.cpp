#include "Way.h"

#include "Parameters.h"

using namespace TSP;

double Way::length(Parameters &para)
{
   if (mState & State::ISLENGTHCALCULATED) {
       return mLength;
   }
   mLength = 0;
   WayType::iterator startPoint = mWay.begin();
   if (startPoint == mWay.end()) return 0;
   for (WayType::iterator point=++mWay.begin(); point != mWay.end(); ++point) {
       mLength += para.distanceFnc(*startPoint, *point);
       ++startPoint;
   }
   mState |= State::ISLENGTHCALCULATED;
   return mLength;
}

void Way::push_back(const Way &newWay)
{
    int iteration=0;
    for (WayType::const_iterator point=newWay.way()->begin();
         point != newWay.way()->end(); ++point, ++iteration)
    {
        if (!mWay.empty() && (mWay.back() == *point) && (iteration == 0)) {
            continue;
        }
        Point newP = *point;
        newP.number(m_number);
        mWay.push_back(newP);
    }
    mState &= ~State::ISNULL;
    mState &= ~State::ISLENGTHCALCULATED;
}


void Way::push_front(const Way &newWay)
{
    int iteration=0;
    if (newWay.way()->empty()) return;
    for (WayType::const_iterator point=newWay.way()->end();
         true; ++iteration)
    {
        if (point == newWay.way()->begin()) break;
        --point;

        if (!mWay.empty() && mWay.front() == *point && iteration == 0) {     // start/end point in two ways? (mWay, newWay)
            continue;
        }
        Point newP = *point;
        if (mState & State::ISODD) {
            newP.state( newP.state() | Point::State::ISODDWAY );
        } else {
            newP.state( newP.state() & ~Point::State::ISODDWAY );
        }
        newP.number(m_number);
        mWay.push_front(newP);
        if (point == newWay.way()->begin())
            break;
    }
    mState &= ~State::ISNULL;
    mState &= ~State::ISLENGTHCALCULATED;
}
