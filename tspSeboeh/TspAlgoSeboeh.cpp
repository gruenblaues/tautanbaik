#include "TspAlgoSeboeh.h"

#include <math.h>
#include <iostream>

#include "DTTree.h"


#define PI 3.14159265

using namespace TSP;

long TSPAlgoSeboeh::ackermannMaximum = 1000;  //  this is ackermann(2,4) - maximal possible is:ackermann(2,12);


TSPAlgoSeboeh::TSPAlgoSeboeh() : TspAlgoCommon()
{
}

TSPAlgoSeboeh::~TSPAlgoSeboeh()
{
}


Way TSPAlgoSeboeh::createWay(Point &startPoint, InputCitiesPointerType * openPoints, float wayLength)
{
    Q_ASSERT(startPoint.state() & ~Point::State::ISINFINIT);
    Q_ASSERT(startPoint.state() & ~Point::State::ISNULL);
    Q_ASSERT(openPoints != NULL);
    /// 1. walk through openPoints
    Way w;
    w.push_back(startPoint);
    Point currentP = startPoint;
    size_t wayLengthInt = static_cast<size_t>(wayLength);
    for (size_t i=0; i < wayLengthInt; ++i)
    {
        /// 2. find next point and add to way
        std::list<Point*>::iterator nextPI = m_para.findNextPointFnc( currentP, openPoints, m_para );
        if (nextPI == openPoints->end()) {
            break;      // no next point found.
        }
        w.push_back(*(*nextPI));
        currentP = *(*nextPI);
        /// 3. erase next point form open point list.
        openPoints->erase(nextPI);
    }

    /// 4. return way - the result.
    return w;
}


/// Parameter functions


std::list<Point*>::iterator TSPAlgoSeboeh::findNextPointFnc(Point &startPoint, std::list<Point*> * openPoints, Parameters &para)
{
//    std::list<Point*>::iterator nearestPIx = nearestNeighbour(startPoint, openPoints, para);
//    return nearestPIx;       // TODO SB: enable

    if (openPoints->empty()) {
        return openPoints->end();
    }
    /// 1. find nearest point and second nearest
    std::list<Point*>::iterator nearestPI = nearestNeighbour(startPoint, openPoints, para);
    if (nearestPI == openPoints->end()) {
        return openPoints->end();
    }
    /// It is not allowed to change openPoints. Therefore we need openPoints2.
    std::list<Point*> * openPoints2 = newOpenPointsStatic(openPoints, *(*nearestPI));
    std::list<Point*>::iterator nearestP2I = nearestNeighbour(*(*nearestPI), openPoints2, para);
    if (nearestP2I == openPoints2->end()) {
        return nearestPI;
    }

    /// 2. check if angle between start point and nearest point is < 90
    double angle1 = arcTangens(startPoint, *(*nearestPI), *(*nearestP2I));
    if (abs(angle1) < PI/2.0) {
        /// 2.1 is <= 90, use second nearest point as first point.
        Point nearestP2 = *(*nearestP2I);
        nearestP2I = openPoints2->erase(nearestP2I);
        if (nearestP2I == openPoints->end())
            return nearestPI;       // TODO [seboeh] check if if-case correct.
        if (*nearestP2I == nullptr)
            return nearestPI;       // TODO [seboeh] check if if-case correct.
        std::list<Point*>::iterator nearestP3I = nearestNeighbour(*(*nearestPI), openPoints2, para);
        if (nearestP3I == openPoints2->end()) {
            return nearestPI;
        /// 2.2 check if angle sum is smaller than new angel sum.
        }
        double angle2 = arcTangens(*(*nearestPI), nearestP2, *(*nearestP3I));
        double newAngle1 = arcTangens(startPoint, nearestP2, *(*nearestPI));
        double newAngle2 = arcTangens(nearestP2, *(*nearestPI), *(*nearestP3I));
        if ((angle1 + angle2) < (newAngle1 + newAngle2)) {
            /// 2.2.1 use second nearest point as return value.
            return findIter(openPoints, *(*nearestP2I));
        }
    }
    return nearestPI;

    /// 3. check crossing
    // TODO SB: further on with crossing
}

std::list<Point*>::iterator TSPAlgoSeboeh::nearestNeighbour(Point &startPoint, std::list<Point*> * openPoints, Parameters &para)
{
    std::list<Point*>::iterator nearestPI = openPoints->end();
    float nearestDistance = INFINITY;
    for (std::list<Point*>::iterator pointI = openPoints->begin();
         pointI != openPoints->end(); ++pointI)
    {
        float dist = para.distanceFnc(startPoint, *(*pointI));
        if (dist < nearestDistance) {
            nearestPI = pointI;
            nearestDistance = dist;
        }
    }
    return nearestPI;
}

std::list<Point*>::iterator TSPAlgoSeboeh::findIter(std::list<Point*> * openPoints, Point &pointToFind)
{
    Q_ASSERT(openPoints!=NULL);
    for (std::list<Point*>::iterator pI = openPoints->begin()
         ; pI != openPoints->end(); ++pI)
    {
        if (*(*pI) == pointToFind) {
            return pI;
        }
    }
    return openPoints->end();
}

std::list<Point*> * TSPAlgoSeboeh::newOpenPointsStatic(std::list<Point*> * openPoints, Point &point)
{
    std::list<Point*> * points = new std::list<Point*>();
    for (std::list<Point*>::iterator pointI= openPoints->begin();
         pointI != openPoints->end(); ++pointI)
    {
        if (point == *(*pointI)) {
            continue;
        }
        points->push_back(*pointI);
    }
    return points;
}

double TSPAlgoSeboeh::arcTangens(Point &p1, Point &p2, Point &p3)
{
    // move to null-origin
    Point vect1(p1.x()-p2.x(), p1.y()-p2.y());
    Point vect2(p3.x()-p2.x(), p3.y()-p2.y());
    return atan2(vect2.y()-vect1.y(), vect1.x()-vect2.x());
}


long TSPAlgoSeboeh::ackermann(long n, long m)
{
    while (n != 0)
    {
        if (m == 0)
        {
            m = 2;
        } else {
            m = ackermann(n, m-1 );
        }
        --n;
    }
    return m + 2;
}


int TSPAlgoSeboeh::decissionTreeChildCountFnc(float &level)
{
    float lf = static_cast<long>(level)+1;
    /// log-linear approximation is more suitable than ackermann function.
    long li = ackermannMaximum - ackermannMaximum*(log(lf)/log(6));
    if (li <= 0) {
        return 1;
    } else {
        return li;
    }

}

float TSPAlgoSeboeh::decissionTreeWeightFnc(float &level)
{
    long li = static_cast<long>(level);
    float ret = static_cast<float>( ackermann(std::min(2l,li/2),li*2) );
    if (ret <= 0) {
        return 1;
    } else {
        return ret;
    }
}

