#pragma once

#include <exception>

class StaticHelper
{
public:

    // TODO [seboeh] This is code duplication. Also used in AckermannMain GUI app. Should be moved to own lib.
    static void logAndThrow(const char * s, ...) throw (std::logic_error)
    {
        va_list args;
        va_start(args, s);
        char * outStr = new char[1000];        // This is a limit of 1000.
        vsnprintf(outStr, 1000, s, args);
        va_end(args);
        outStr[999] = '\0';
        std::string outString = outStr;
        delete [] outStr;
        std::cerr << outString << std::endl;   // logging - better to log more than too less.
        throw std::logic_error(outString.c_str());
    }

};
