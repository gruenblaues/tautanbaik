#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <list>

#include "Point.h"
#include "dll.h"

#ifndef NULL
    #define NULL 0
#endif

namespace TSP {

struct EXTERN Parameters {

    static const int MAX_COUNT_CHILDREN = 10000;
    static const int naiveRunTimeDefault = 600;

    Parameters() :
        algoType(AlgorithmType::BellmannHeldKarpInternet)
        , distanceFnc(NULL)
        , decissionTreeChildCountFnc(NULL)
        , decissionTreeWeightFnc(NULL)
        , findNextPointFnc(NULL)
    { }

    /**
     * this parameter is the algorithm strategy.
     * It is specified by creation of the Facade.
     */
    enum class AlgorithmType { Naive, Ackermann, Carisia, Pluto, Neptun, Iwan, Sarah, Tamar, TamarExp, TamarExcluding, SeboehDistAlg, VoronoiAreaSort, VoronoiClusterSort, VoronoiClusterDescent, VoronoiAreaPairGrow, VoronoiConvexAlignment, MinMaxClusterAlignment
                             , DetourClusterAlignment, BellmannHeldKarp, BellmannHeldKarpInternet, HierarchicalBreadthSplitting, HierarchicalClusteredAlignment
    };

    AlgorithmType algoType;

    /**
     * this is used in weighting and calculating the distance of two cities.
     * This is for example something like the euclidian distance.
     * @return distance/norm between two points.
     */
    typedef double (*distanceFncType)(const Point &a, const Point &b);
    distanceFncType distanceFnc;

    /**
     * different count of children per node will give different decission for the decission-tree.
     * This can be for example the |count of citys|-AckermannFnc(level).
     * It is used at building the decission tree.
     * @return the count of branches for a given depth of the tree.
     */
    typedef int (*decissionTreeChildCountFncType)(float &level);
    decissionTreeChildCountFncType decissionTreeChildCountFnc;

    /**
     * it is used at building the decission tree. It is a measure for the importance of the decission in the tree by given level.
     * This can be for example the AckermanFnc(level) for level=0..5
     * @return weight of gulty of the decission.
     */
    typedef float (*decissionTreeWeightFncType)(float &level);
    decissionTreeWeightFncType decissionTreeWeightFnc;

    /**
     * @brief In the algorithm of finding closes node, it can be that the closest node is suboptimal for the entire solution. Therefore it is better to use another node.
     * It is the distance and weighting function for the search of closest Node.
     * For example you can implement a heading to prevent crossing ways.
     * @returns interator to the new found point.
     */
    typedef std::list<Point*>::iterator (*findNextPointFncType)(Point &startPoint, std::list<Point*> * openPoints, Parameters &para);
    findNextPointFncType findNextPointFnc;

    /**
     * @brief naive version will be cancled after specified time.
     */
    int m_naiveRunMinutes = naiveRunTimeDefault;

    /**
     * for debugging
     * It is set on start of true, if using debug app 2016
     */
    bool *isRun = nullptr;

};


struct defaultFnc {

    static double defaultDistanceFnc(const Point &a, const Point &b)
    {
        return sqrt((static_cast<double>(a.x())-b.x())*(static_cast<double>(a.x())-b.x())
                    + (static_cast<double>(a.y())-b.y())*(static_cast<double>(a.y())-b.y()));
    }

    static int defaultDecissionTreeChildCountFnc(float &level)
    {
        return Parameters::MAX_COUNT_CHILDREN;
    }

    static float defaultDecissionTreeWeightFnc(float &level)
    {
        return level+1;
    }

    static std::list<Point*>::iterator defaultFindNextPointFnc(Point &startPoint, std::list<Point*> * openPoints, Parameters &)
    {
        return openPoints->begin();
    }

};

}

#endif // PARAMETERS_H
