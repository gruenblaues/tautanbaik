#ifndef DTNODE_H
#define DTNODE_H

#include <QObject>

#include "Way.h"

namespace TSP {

namespace DT {  /// DT = Decission Tree.

class DTNode;

class DTNode {
public:
    typedef DTNode* NodeType;
    typedef typename std::list<NodeType> ChildType;


    DTNode(NodeType parent, const Way &value) : mWay(value), mParent(parent) {}

    ~DTNode() {
        for (ChildType::iterator childI=mChildren.begin();
             childI != mChildren.end(); ++childI) {
            delete *childI;
            *childI = NULL;
        }
        mChildren.clear();
        }

    /**
     * @return the value of this node.
     */
    inline Way * value() { return &mWay; }

    /**
     * @brief iterates through the childs. Start with NULL as currentNode for the first child.
     * @param currentNode - have to be a child iterator from this node. Received by this "nextChild" method.
     * @return next child or Node with infinit way, if the last child was already reached.
     */
    inline ChildType * children() {
        return &mChildren;
    }

    /**
     * @brief responsible for creating a new node. Destroying and creating is inside this class.
     * @param val - is the new value for the new node, which is inserted as children from this class.
     * @return new created node.
     */
    inline ChildType::iterator addChild(const Way &val) {
        NodeType newNode = new DTNode(this, val);
        mChildren.push_back(newNode);
        return --mChildren.end();
    }

    inline NodeType parent() { return mParent; }
    inline void parent(NodeType parent) { mParent = parent; }


private:
    Way         mWay;
    ChildType   mChildren;
    NodeType    mParent;

};

}
}



#endif // DTNODE_H
