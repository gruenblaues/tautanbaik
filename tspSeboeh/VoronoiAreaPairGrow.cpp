#include "VoronoiAreaPairGrow.h"

#include <algorithm>
#include <tuple>

#include "bspTreeVersion/BspTreeSarah.h"
#include "TamarAlgorithmHelper.h"
#include "Way.h"

using TSPWay = TSP::Way;

using namespace TSP;
using namespace std;
using namespace TSPHelper;

size_t VoronoiAreaPairGrow::tspSize = 0;
double VoronoiAreaPairGrow::tspWidth = 0;
double VoronoiAreaPairGrow::tspHeight = 0;

VoronoiAreaPairGrow::VoronoiAreaPairGrow()
    : TspAlgoCommon()
{

}

VoronoiAreaPairGrow::NodeL VoronoiAreaPairGrow::createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL ret;

    double tspMaxWidth = 0;
    double tspMaxHeight = 0;
    double tspMinWidth = numeric_limits<double>::max();
    double tspMinHeight = numeric_limits<double>::max();
    for (const auto &point : *openPointsBasic)
    {
        ret.push_back(std::make_shared<Node>(*point));
        if (tspMaxWidth < point->x())
            tspMaxWidth = point->x();
        if (tspMinWidth > point->x())
            tspMinWidth = point->x();
        if (tspMaxHeight < point->y())
            tspMaxHeight = point->y();
        if (tspMinHeight > point->y())
            tspMinHeight = point->y();
    }
    tspWidth = tspMaxWidth - tspMinWidth;
    tspHeight = tspMaxHeight - tspMinHeight;

    return ret;
}

bsp::BspTreeSarah VoronoiAreaPairGrow::_createBSPTree(NodeL &input)
{
    InputCitiesPointerType cities;
    for (const auto &node : input)
        cities.push_back(new TSP::Point(node->point));
    // HINT: above. better to use the nodes from input than new points.
    // because later search of hull-node necessary - see mark MARK_hullNodeSearch

    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cities);

    return bspTree;
}

std::tuple<VoronoiAreaPairGrow::NodeL, VoronoiAreaPairGrow::Way*> VoronoiAreaPairGrow::determineHull(NodeL &input)
{
    NodeL ret;
    // * split input into quad-BSP-tree
    // * on returning from recursion merge to cyclic way (convex hull).

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = _createBSPTree(input);

    /// 2. create points order - merge
    //////////////////////////
    Way * way = nullptr;
    auto root = bspTree.root();
    if (root != nullptr) {
        way = _newConqueredWay(root);
        if (way != nullptr)
        {
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (Way::iterator valI=way->begin();
                 !valI.end(); )
            {
                Node *node = valI.nextChild();
                node->isHull = true;
                ret.push_back(std::shared_ptr<Node>(node));
            }
// TODO [seboeh] check if possible
//            way->clear();
//            delete way;
        }
    }

    // update input with new nodes from way
    Way::iterator wayI = way->begin();
    while (!wayI.end())
    {
        Node *node = wayI.nextChild();
        auto inputI = find_if(input.begin(), input.end(),
                              [node] (const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        assert(inputI != input.end());
        *inputI = shared_ptr<Node>(node, no_op_delete());
    }

    // * create resulting list

    return make_tuple(ret, way);
}


bool VoronoiAreaPairGrow::_isClockwise(TSP::Point pCenter, Way *way)
{
    Way::iterator pI = way->begin();
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

void VoronoiAreaPairGrow::_flipWay(Way *way, TSP::Point pCenter)
{
    if(!_isClockwise(pCenter, way))
        way->flipDirection();
}

VoronoiAreaPairGrow::Way *VoronoiAreaPairGrow::_newConqueredWay(bsp::BspNode *node)
{
    if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        Way *way = new Way();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
//        way->setOrigin(node);
        return way;
    }
    Way * wayUpper;
    Way * wayLower;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        Way * wayUpperLeft = _newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            Way * wayUpperRight = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x()/2.0, pUpperCenter.y()/2.0);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            _mergeHull(wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        } else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    } else {
        if (node->m_upperRight) {
            wayUpper = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        } else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        Way * wayLowerLeft = _newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            Way * wayLowerRight = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x()/2.0, pLowerCenter.y()/2.0);

            _mergeHull(wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        } else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    } else {
        if (node->m_lowerRight) {
            wayLower = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        } else {
            wayLower = nullptr;
        }
    }

    //incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {
        // HINT: test37 needs master center node->m_center.
        // HINT: test2 needs flipWay
        _flipWay(wayLower, pLowerCenter);
        _flipWay(wayUpper, pUpperCenter);
        _mergeHull(wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    } else if (wayUpper) {
        return wayUpper;
    } else if (wayLower) {
        return wayLower;
    } else {
        return nullptr;
    }
}


bool VoronoiAreaPairGrow::_isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    // barycentric coordinates.
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        return false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        return false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        return false;
    return true;
}

void VoronoiAreaPairGrow::_triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = _isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = _isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
                && pb == *lpbI
                && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
                && pb == *hpbI
                && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

VoronoiAreaPairGrow::CrossedState VoronoiAreaPairGrow::_isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint)
{
    /// Crossed between a and b?
    double x1 = pa1I.x();
    double y1 = pa1I.y();
    double x2 = pa2I.x();
    double y2 = pa2I.y();
    double x3 = pb1I.x();
    double y3 = pb1I.y();
    double x4 = pb2I.x();
    double y4 = pb2I.y();

    /// HINT: [http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection]
    double denominator = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (denominator == 0) {
        // lines are parallel not guarantied to lay on each other.
        // We want to integrate the considered node as inner node -> so it should not be covered -> parallel
        return CrossedState::isParallel;
    }
    double pX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denominator;
    double pY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denominator;
    if (crossPoint != nullptr)
        *crossPoint = TSP::Point(pX,pY);

    if ((std::max)(std::min(x1,x2),std::min(x3,x4)) <= pX && pX <= std::min((std::max)(x1,x2),(std::max)(x3,x4))
            && (std::max)(std::min(y1,y2),std::min(y3,y4)) <= pY && pY <= std::min((std::max)(y1,y2),(std::max)(y3,y4)))
    {
        if ((pX == x1 && pY == y1)          // FIX: Could be a speed-up, if we check if this crossing on exact one outer point could happen with the input.
            || (pX == x2 && pY == y2)
            || (pX == x3 && pY == y3)
            || (pX == x4 && pY == y4))
        {
            return CrossedState::isOnPoint;
        } else {
            return CrossedState::isCrossed;
        }
    } else {
        return CrossedState::isOutOfRange;
    }
}

VoronoiAreaPairGrow::DirectAccessState VoronoiAreaPairGrow::_isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI)
{
    Way::iterator prevAI = pAI; --prevAI;
    Way::iterator nextAI = pAI; ++nextAI;
    Way::iterator prevBI = pBI; --prevBI;
    Way::iterator nextBI = pBI; ++nextBI;
    CrossedState state = _isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = _isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = _isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = _isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool VoronoiAreaPairGrow::_isNormalVectorLeft(const Point &from, const Point &to, const Point &insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x())/2
                    , (from.y() + to.y())/2);

    Point normalVectLeft(meanPoint.x() - straight.y()
                         , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
                          , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}


bool VoronoiAreaPairGrow::_isCovered(Way * way, Way::iterator &w1I, Way::iterator &fixI)
{
    Way::iterator wI = way->begin();
    Way::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = _isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed || state == CrossedState::isCovered)
            return true;
        if (state == CrossedState::isOnPoint)
        {
            if (crossedPoint == *wI || crossedPoint == *wNI)
                return true;
        }		// else out of range
    }
    return false;
}

bool VoronoiAreaPairGrow::_createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI)
{
    if (_isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        Way::iterator w1NewI = way1->push_back(&way2I);
        Way::iterator w1NNewI = way1->push_back(&way2NI);

        Node *nodePoint = (&w1Inside);
        if (dist1 < dist2 && dist1 < dist3)
        {
            nodePoint->prev = &w1Alt;
            nodePoint->next = (&w1Alt)->next;
            way1->insertChild(w1Alt, nodePoint);      // TODO [seboeh] is new really necessary?
        } else if (dist2 < dist1 && dist2 < dist3)
        {
            nodePoint->prev = &w1NNewI;
            nodePoint->next = (&w1NNewI)->next;
            way1->insertChild(w1NNewI, nodePoint);
        } else {
            nodePoint->prev = &w1NewI;
            nodePoint->next = (&w1NewI)->next;
            way1->insertChild(w1NewI, nodePoint);
        }
        return true;
    }
    return false;
}

void VoronoiAreaPairGrow::_createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    Way::iterator w1I = way1->begin();
    Way::iterator w1NI = w1I; ++w1NI;
    Way::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    Node *nodePoint = (&insideI);
    if (dist1 < dist2 && dist1 < dist3)
    {
        nodePoint->prev = &w1I;
        nodePoint->next = (&w1I)->next;
        way1->insertChild(w1I, nodePoint);
    } else if (dist2 < dist1 && dist2 < dist3)
    {
        nodePoint->prev = &w1NI;
        nodePoint->next = (&w1NI)->next;
        way1->insertChild(w1NI, nodePoint);
    } else {
        nodePoint->prev = &w1NNI;
        nodePoint->next = (&w1NNI)->next;
        way1->insertChild(w1NNI, nodePoint);
    }
}

void VoronoiAreaPairGrow::_mergeHull(Way * &way1, const TSP::Point &pWay1Center, Way * &way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        _flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        Way::iterator w1I = way1->begin();
        Way::iterator fixI = way2->begin();
        if (_isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            _flipWay(way1, pCenter);
            return;
        } else {
            ++w1I;
            if (_isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                _flipWay(way1, pCenter);
                return;
            } else {
                ++w1I;
                if (_isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    _flipWay(way1, pCenter);
                    return;
                } else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    Way::iterator w1NI = w1I; ++w1NI;
                    Way::iterator w1NNI = w1NI; ++w1NNI;
                    if (_isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        _createTriangle3x1(way1, w1NNI, fixI);
                    } else if (_isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        _createTriangle3x1(way1, w1NI, fixI);
                    } else {
                        _createTriangle3x1(way1, w1I, fixI);
                    }
                    _flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        Way::iterator way1I = way1->begin();
        Way::iterator way2I = way2->begin();
        Way::iterator way1NI = way1I;
        ++way1NI;
        Way::iterator way2NI = way2I;
        ++way2NI;
        Way::iterator way1Nearest;
        Way::iterator way2Nearest;
        double maxDist=std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        Way::iterator way1Next = way1Nearest; ++way1Next;
        Way::iterator way2Next = way2Nearest; ++way2Next;
        if (_isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next) == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            _flipWay(way1, pCenter);
            return;
        } else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (_createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            } else  if (_createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI ))
            {
            } else if (_createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else if (_createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            _flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2

    /// beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (Way::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (Way::iterator wBI=way2->begin();
             !wBI.end(); ++wBI)
        {
            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (_isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    Way::iterator haI, hbI, laI, lbI;

    Point realCenter = (pWay1Center + pWay2Center)/2.0;

    smallestDist /= 2;
    Point lastLowCrossPoint;
    Point lastHighCrossPoint;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = _isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x())-10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x())+10, pCenter.y()), &crossedPoint);
        else
            state = _isCrossed(pa, pb, TSP::Point(pCenter.x(),std::min(pa.y(), pb.y())-10), TSP::Point(pCenter.x(),(std::max)(pa.y(), pb.y())+10), &crossedPoint);

        bool isLowNotHigh = _isNormalVectorLeft(pb, pa, realCenter);

        double value;
        if (state != CrossedState::isOnPoint)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();
        }
        else
        {       // see test82, test76
            // see test89, test64
            Point otherP;
            if (pa == crossedPoint)
                otherP = pb;
            else
                otherP = pa;
            double otherVal;
            if (callState == MergeCallState::topDown)
                otherVal = otherP.x();
            else
                otherVal = otherP.y();


            // HINT: center of pa_pb is not working, because test76 cascading of points.
            Point descent = otherP - crossedPoint;
            descent /= m_para.distanceFnc(otherP, crossedPoint);

            if (callState == MergeCallState::topDown)
            {
                value = crossedPoint.x() + descent.x()*smallestDist;
            }
            else
            {
                value = crossedPoint.y() + descent.y()*smallestDist;
            }
        }

        bool isGood = false;
        if (isLowNotHigh)
        {
            if ((callState == MergeCallState::topDown
                    && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGood = true;
            }
            if (lastLowCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastLowCrossPoint = Point();
                }
            }
        }
        else
        {
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                    && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGood = true;
            }
            if (lastHighCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastHighCrossPoint = Point();
                }
            }
        }

        if (((state == CrossedState::isCrossed
                    || state == CrossedState::isOnPoint     // because, teset82
              )
                && isGood)          // TODO [seboeh] drop isGood here
           )
        {
            if (isLowNotHigh)
            {
                laI = edgeI->aNodeI;
                lbI = edgeI->bNodeI;
            }
            else
            {
                haI = edgeI->aNodeI;
                hbI = edgeI->bNodeI;
            }
        }
    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, haI, laI, hbI, lbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hbI, lbI, haI, laI);

    /// correct the hull in way1
    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    /// clear childs - see test7
    (&haI)->isChildNode = false;
    (&haI)->child = nullptr;
    (&hbI)->isChildNode = false;
    (&hbI)->child = nullptr;
    (&laI)->isChildNode = false;
    (&laI)->child = nullptr;
    (&lbI)->isChildNode = false;
    (&lbI)->child = nullptr;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in _mergeHull().

    _flipWay(way1, pCenter);  // necessary, because code before does not keep the clock-wise order.
}

double VoronoiAreaPairGrow::_calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double VoronoiAreaPairGrow::_area(const PointL &polygon)
{
    double area = 0;
    for (auto pointI = polygon.begin();
         pointI != polygon.end(); ++pointI)
    {
        auto pointNextI = pointI; ++pointNextI;
        if (pointNextI == polygon.end()) pointNextI = polygon.begin();
        area += pointI->x() * pointNextI->y();
        area -= pointI->y() * pointNextI->x();
    }
    return abs(area);
}

double VoronoiAreaPairGrow::_triangleAltitudeOnC(Point A, Point B, Point C)
{
    double a = m_para.distanceFnc(A, B);
    double b = m_para.distanceFnc(B, C);
    double c = m_para.distanceFnc(C, A);
    double s = (a + b + c)/2.0;
    return 2.0*sqrt(s*(s-a)*(s-b)*(s-c)) / c;
}

bool VoronoiAreaPairGrow::_isRightSide(const Point &p1, const Point &p2)
{
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

double VoronoiAreaPairGrow::_calcDegree(Node *node, Node *nodeTested)
{
    Point pointVect = nodeTested->point - node->point;

    double degree = (pointVect.x() + pointVect.y())
            / (m_para.distanceFnc(Point(0,0), pointVect)
               * m_para.distanceFnc(Point(0,0), Point(1,1)));

    double degreeArc = acos(degree);
    // clock-wise
    if (_isRightSide(Point(1,1), pointVect))
        return  degreeArc;
    else
        return  2*acos(-1) - degreeArc;
}


double VoronoiAreaPairGrow::_correlation(Point a, Point b)
{
    return ((a.x()*b.x()) + (a.y()*b.y())) / (m_para.distanceFnc(Point(0,0), a) * m_para.distanceFnc(Point(0,0),b));
}


bool VoronoiAreaPairGrow::_checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{
    double dist1 = m_para.distanceFnc(cluPointOut, nodePoint);
    double dist2 = m_para.distanceFnc(cluPointOut2, nodePoint);
    double distC = m_para.distanceFnc(closestP, nodePoint);
    double corr1 = _correlation(cluPointOut - nodePoint, closestP - nodePoint);
    //double degree0to1For1 = (corr1 + 1) / 2.0;
    double corr2 = _correlation(cluPointOut2 - nodePoint, closestP - nodePoint);
    //double degree0to1For2 = (corr2 + 1) / 2.0;
    //double corr = degree0to1For1 + degree0to1For2;
    //double corr = corr1 + corr2;
    double corr3 = _correlation(cluPointOut - nodePoint, cluPointOut2 - nodePoint);
    // distC, dist1,2 are tradeofs not correct decisions
    return ((distC/2 < dist1 || distC/2 < dist2) && corr3 < corr1 && corr3 < corr2);
}

VoronoiAreaPairGrow::ClusterGrow VoronoiAreaPairGrow::determineClusters(NodeL &input, NodeL &hull)
{
    ClusterGrow clusters;
    double coordMax = max(tspWidth, tspHeight) * 12;
    //  TODO [seboeh] * 12 - still a problem, because the intersection below can be far away.

    /// * walk through all inputs except hull-points
    for (const auto &node : input)
    {
        Cluster clu;
        clu.point = node;
        auto fI = std::find_if(hull.begin(), hull.end(),
                               [node](const shared_ptr<Node> &val)
        {
            return val->point == node->point;
        });
        if (fI != hull.end())
            continue;

        /// * find closest three points P_1 to input x
        map<double, shared_ptr<Node>> closestPoints;
        for (const auto &node2 : input)
        {
            if (node == node2)
                continue;
            closestPoints.insert({m_para.distanceFnc(node->point, node2->point), node2});
        }
        if (closestPoints.size() < 3)
            return clusters;
        for (const auto &closE : closestPoints)
        {
            clu.closestN.push_back(closE.second);
        }

        /// * check if x is inside P_1,
        auto closestP1I = closestPoints.begin();
        auto closestP2I = closestP1I; ++closestP2I;
        auto closestP3I = closestP2I; ++closestP3I;
        list<Node*> closestTestedP;
        closestTestedP.push_back(closestP1I->second.get());
        closestTestedP.push_back(closestP2I->second.get());
        closestTestedP.push_back(closestP3I->second.get());
        Node* foundP1 = closestP1I->second.get();
        Node* foundP2 = closestP2I->second.get();
        Node* foundP3 = closestP3I->second.get();
        if ( ! _isInsideTriangle(closestP1I->second->point, closestP2I->second->point, closestP3I->second->point, node->point))
        {
            ///     * if yes, continue
            ///     * else determine next closest p_2
            ///     * check in two of P_1 and p_2, if x is inside this triangle
            ///         * if yes, continue
            ///         * else, repeat until end of inputs. -> unexpected-error, if not found
            auto closestPnextI = ++closestP3I;
            while (closestPnextI != closestPoints.end())
            {
                // 2 over n
                // TODO [seb] hint: keep closest fixed
                bool stopWhile = false;
                for (auto closestTestedP1I = closestTestedP.begin();
                     closestTestedP1I != closestTestedP.end(); ++closestTestedP1I)
                {
                    for (auto closestTestedP2I = closestTestedP.begin();
                         closestTestedP2I != closestTestedP.end()
                         && closestTestedP1I != closestTestedP2I; ++closestTestedP2I)
                    {
                        if (_isInsideTriangle((*closestTestedP1I)->point, (*closestTestedP2I)->point, closestPnextI->second->point, node->point))
                        {
                            foundP1 = *closestTestedP1I;
                            foundP2 = *closestTestedP2I;
                            foundP3 = closestPnextI->second.get();
                            closestTestedP.push_back(closestPnextI->second.get());
                            stopWhile = true;
                            break;
                        }
                    }
                    if (stopWhile) break;
                }
                if (stopWhile) break;
                closestTestedP.push_back(closestPnextI->second.get());
                ++closestPnextI;
            }
            if (closestPnextI == closestPoints.end())
                return clusters;
        }

        /// * sort closestTestedP, according to degree, because there is no order than circular around the point
        // cross product a*d - b*c, with point = (a,b) and (c,d) = (1,1)
        map<double, Node*> closestTestedPointsSorted;
        closestTestedPointsSorted.insert({_calcDegree(node.get(), foundP1), foundP1});
        closestTestedPointsSorted.insert({_calcDegree(node.get(), foundP2), foundP2});
        closestTestedPointsSorted.insert({_calcDegree(node.get(), foundP3), foundP3});


//        clu.closestN.clear();
//        clu.closestN.reserve(closestTestedPointsSorted.size());
//        for (const auto &point : closestTestedPointsSorted)
//        {
//            for (auto &closestE : closestPoints)
//            {
//                if (point.second == closestE.second->point)
//                {
//                    clu.closestN.push_back(closestE.second);
//                    break;
//                }
//            }
//        }

        /// * for all P (P_1,P_2,...):
        ///
        /// * enlarge points to get the real cluster
        clu.verts.clear();
        bool isChanged = true;
        while (isChanged)
        {
            isChanged = false;
            auto closestPI = closestPoints.begin();
            while (closestPI != closestPoints.end())
            {
                Node *nodeClosest = closestPI->second.get();
                auto findClosTesI = find_if(closestTestedPointsSorted.begin()
                                            , closestTestedPointsSorted.end()
                                            , [nodeClosest] (const map<double, Node*>::value_type &val)
                {
                    return val.second == nodeClosest;
                });
                if (findClosTesI != closestTestedPointsSorted.end())
                {
                    ++closestPI;
                    continue;
                }
                for (auto cluPointOutI = closestTestedPointsSorted.begin();
                 cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
                {
                    auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
                    if (cluPointOut2I == closestTestedPointsSorted.end())
                        cluPointOut2I = closestTestedPointsSorted.begin();
                    ///     * calculate mid of x to p_i =e_1 and mid of x to p_{i+1} =e_2
                    Point midE1 = (cluPointOutI->second->point + node->point)/2.0;
                    Point midE2 = (cluPointOut2I->second->point + node->point)/2.0;
                    ///     * calculate the orthogonals to e_1 and e_2
                    Point vec1 = (cluPointOutI->second->point - node->point) / m_para.distanceFnc(cluPointOutI->second->point, node->point);
                    Point midE1b = midE1 + Point(-vec1.y(), vec1.x()) * coordMax;           // rotate 90degree
                    Point vec2 = (cluPointOut2I->second->point - node->point) / m_para.distanceFnc(cluPointOut2I->second->point, node->point);
                    Point midE2b = midE2 + Point(vec2.y(), -vec2.x()) * coordMax;
                    ///     * calculate the intersection of orthogonals
                    Point crossMid;
                    auto intersectRes = _isCrossed(midE1, midE1b, midE2, midE2b, &crossMid);
                    if (intersectRes == CrossedState::isCrossed
                            || intersectRes == CrossedState::isOnPoint)
                    {
                        /// check if there exist an intersection closer than crossMid
                        Point midE3 = (closestPI->second->point + node->point)/2.0;
                        Point vec3 = (closestPI->second->point - node->point) / m_para.distanceFnc(closestPI->second->point, node->point);
                        Point midE3b = midE3 + Point(vec3.y(), -vec3.x()) * coordMax;
                        Point crossMid3;
                        auto intersectRes = _isCrossed(midE1, midE1b, midE3, midE3b, &crossMid3);
                        if (intersectRes == CrossedState::isCrossed
                                || intersectRes == CrossedState::isOnPoint)
                        {
                            double distMid1 = m_para.distanceFnc(node->point, crossMid);
                            double distMid2 = m_para.distanceFnc(node->point, crossMid3);
                            if (distMid2 < distMid1)
                            {       // insert
                                closestTestedPointsSorted.insert({_calcDegree(node.get(), closestPI->second.get()), closestPI->second.get()});
                                isChanged = true;
                                break;
                            }
                        }
                        else
                        {
                            midE3b = midE3 + Point(-vec3.y(), vec3.x()) * coordMax;
                            intersectRes = _isCrossed(midE2, midE2b, midE3, midE3b, &crossMid3);
                            if (intersectRes == CrossedState::isCrossed
                                    || intersectRes == CrossedState::isOnPoint)
                            {
                                double distMid1 = m_para.distanceFnc(node->point, crossMid);
                                double distMid2 = m_para.distanceFnc(node->point, crossMid3);
                                if (distMid2 < distMid1)
                                {       // insert
                                    closestTestedPointsSorted.insert({_calcDegree(node.get(), closestPI->second.get()), closestPI->second.get()});
                                    isChanged = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (_checkCorrelationAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                                {
                                    closestTestedPointsSorted.insert({_calcDegree(node.get(), closestPI->second.get()), closestPI->second.get()});
                                    isChanged = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_checkCorrelationAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                        {
                            closestTestedPointsSorted.insert({_calcDegree(node.get(), closestPI->second.get()), closestPI->second.get()});
                            isChanged = true;
                            break;
                        }
                    }
                }       // end closestTestedPointsSorted

                /// keep on running with next closest point
    //            if (!isChanged)
    // HINT: not true, because there are points which are outside but on another side.  - break;      // because, the other points are too far away
                // iterate
                ++closestPI;
            }
        }

        ///     * save this intersection as point into Cluster.verts
        for (auto cluPointOutI = closestTestedPointsSorted.begin();
         cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
        {
            auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
            if (cluPointOut2I == closestTestedPointsSorted.end())
                cluPointOut2I = closestTestedPointsSorted.begin();
            ///     * calculate mid of x to p_i =e_1 and mid of x to p_{i+1} =e_2
            Point midE1 = (cluPointOutI->second->point + node->point)/2.0;
            Point midE2 = (cluPointOut2I->second->point + node->point)/2.0;
            ///     * calculate the orthogonals to e_1 and e_2
            Point vec1 = (cluPointOutI->second->point - node->point) / m_para.distanceFnc(cluPointOutI->second->point, node->point);
            Point midE1b = midE1 + Point(-vec1.y(), vec1.x()) * coordMax;           // rotate 90degree
            Point vec2 = (cluPointOut2I->second->point - node->point) / m_para.distanceFnc(cluPointOut2I->second->point, node->point);
            Point midE2b = midE2 + Point(vec2.y(), -vec2.x()) * coordMax;
            ///     * calculate the intersection of orthogonals
            Point crossMid;
            auto intersectRes = _isCrossed(midE1, midE1b, midE2, midE2b, &crossMid);
            if (intersectRes == CrossedState::isCrossed
                    || intersectRes == CrossedState::isOnPoint)
            {
                clu.verts.push_back(crossMid);
            }
            else
            {       // because points (cuPointOut..) do not cross each other inside their range.
                     //because not inserting a point, can result in too less points for an area calculation.
                     //just adding a guessing point, to later include if necessary.

                Point crossOnLine1;
                _isCrossed(midE1, node->point, midE2, midE2b, &crossOnLine1);

                Point crossOnLine2;
                _isCrossed(midE2, node->point, midE1, midE1b, &crossOnLine2);

                if (m_para.distanceFnc(crossOnLine1, node->point)
                        < m_para.distanceFnc(crossOnLine2, node->point))
                {
                    clu.verts.push_back(crossOnLine1);
                    clu.verts.push_back(midE2);
                }
                else
                {
                    clu.verts.push_back(crossOnLine2);
                    clu.verts.push_back(midE1);
                }
            }
        }
        /// * calculate area of cluster
        assert(clu.verts.size() > 2);
        clu.area = _area(clu.verts);
        for (auto &el : closestTestedPointsSorted)
            clu.closestTestedPointsSorted.push_back(el.second);

        /// repeat from beginning, until all inputs are handled.
        clusters.push_back(shared_ptr<Cluster>(new Cluster(clu),no_op_delete()));
    }
    return clusters;
}

VoronoiAreaPairGrow::Node* VoronoiAreaPairGrow::_nextNode(Node *curr, Node * &prev)
{
    if (curr->edge1 == prev)
    {
        prev = curr;
        return curr->edge2;
    }
    else
    {
        prev = curr;
        return curr->edge1;
    }
}

bool VoronoiAreaPairGrow::_updateHullOrUpdateSimple(shared_ptr<Node> hullNodeA, shared_ptr<Node> updateNode, NodeL &hull)
{
// MARK_hullNodeSearch
    auto hullI = find_if(hull.begin(), hull.end(),
                         [hullNodeA] (const NodeL::value_type &val)
    {
        return hullNodeA->point == val->point;
    });
    shared_ptr<Node> hullNode;
    if (hullI != hull.end())
        hullNode = *hullI;
    else
        hullNode = hullNodeA;

    // check valid updateNode
    if (updateNode.get() == hullNode->edge1
            || updateNode.get() == hullNode->edge2)
        return false;
    if (hullNode->edge1 != nullptr
            && hullNode->edge2 != nullptr)
        return false;
    if (updateNode->edge1 != nullptr
            && updateNode->edge2 != nullptr)
        return true;

    // update
    if (hullNode->edge1 == nullptr)
    {
        hullNode->edge1 = updateNode.get();
        hullNodeA->edge1 = updateNode.get();
    }
    else if (hullNode->edge2 == nullptr)
    {
        hullNode->edge2 = updateNode.get();
        hullNodeA->edge2 = updateNode.get();
    }    else return true;
    if (updateNode->edge1 == nullptr)
        updateNode->edge1 = hullNode.get();
    else if (updateNode->edge2 == nullptr)
        updateNode->edge2 = hullNode.get();
    else return true;

    // check
    assert(updateNode->edge1 != updateNode.get());
    assert(updateNode->edge2 != updateNode.get());

    return true;
}

bool VoronoiAreaPairGrow::_updateAllowed(shared_ptr<Node> closest, shared_ptr<Node> node, bool isTestOnClique)
{
    // check valid nodes
    if (node.get() == closest->edge1
            || node.get() == closest->edge2)
        return false;
    if (closest->edge1 != nullptr
            && closest->edge2 != nullptr)
        return false;
    if (node->edge1 != nullptr
            && node->edge2 != nullptr)
        return true;        // because this calls intern if, which calls this criteria again, and breaks the loop.

    // check on clique - graph theory
    // walk through the edges1/2 starting from closest:
    //  if ending at node, there is a clique - edge shall not be supported
    //  only if the cound of vertices = n.
    if (isTestOnClique)
    {
        Node *nod = closest.get();
        Node *prev = nullptr;
        size_t tspSizeI = 0;
        bool isClique = false;
        while (nod != nullptr)
        {
            ++tspSizeI;
            if (nod == node.get() && tspSizeI < m_tspSize)
            {
                isClique = true;
                break;
            }
            if (nod->edge1 != nullptr && nod->edge1 != prev)
            {
                prev = nod;
                nod = nod->edge1;
            } else if (nod->edge2 != nullptr && nod->edge2 != prev)
            {
                prev = nod;
                nod = nod->edge2;
            }
            else break;
        }
        return !isClique;
    }

    return true;
}


void VoronoiAreaPairGrow::_updateNodes2(map<double, Cluster*> &clustersSorted, NodeL &hull)
{
    for (auto &cluE : clustersSorted)
    {
        Cluster *clu = cluE.second;
        Node* from;
        Node* to;
        double distBest = std::numeric_limits<double>::max();
        for (auto nodeI = clu->closestN.begin();
             nodeI != clu->closestN.end(); ++nodeI)
        {
            // update
            auto nodeNextI = nodeI; ++nodeNextI;
            if (nodeNextI == clu->closestN.end())
                nodeNextI = clu->closestN.begin();

            // case no edges
            int clearedEdgesNode1C = 0;
            int clearedEdgesNode2C = 0;
            if ((*nodeI)->edge1 != nullptr)
                ++clearedEdgesNode1C;
            if ((*nodeNextI)->edge1 != nullptr)
                ++clearedEdgesNode2C;
            if ((*nodeI)->edge2 != nullptr)
                 ++clearedEdgesNode1C;
            if ((*nodeNextI)->edge2 != nullptr)
                 ++clearedEdgesNode2C;

            double dist = TSPHelper::seboehDist((*nodeI).get(), (*nodeNextI).get(), clu->point.get());
            if (clearedEdgesNode1C == 2 && clearedEdgesNode2C == 2)
            {
                // replace
                if ((*nodeI)->edge1 == (*nodeNextI)->edge1
                        || (*nodeI)->edge2 == (*nodeNextI)->edge2
                        || (*nodeI)->edge1 == (*nodeNextI)->edge2
                        || (*nodeI)->edge2 == (*nodeNextI)->edge1)
                {
                    if (dist < distBest)
                    {
                        distBest = dist;
                        from = (*nodeI).get();
                        to = (*nodeNextI).get();
                    }
                }
            }
            else
            {
                // check
                if (dist < distBest)
                {
                    distBest = dist;
                    from = (*nodeI).get();
                    to = (*nodeNextI).get();
                }
            }
        }

        ///     * connect c.point to the closest node pn
        if (clu->point->edge1 != nullptr
                || clu->point->edge2 != nullptr)
        {
            // check if redirection is necessary.
            Node* altN = (std::max)(clu->point->edge1, clu->point->edge2);
            Node* startN = altN;
            if (startN->edge1 != clu->point.get())
                startN = startN->edge1;
            else if (startN->edge2 != clu->point.get())
                startN = startN->edge2;
            assert(startN != nullptr);
            Node* closestN;
            Node* closestOppoN;
            if (m_para.distanceFnc(from->point, altN->point) <
                    m_para.distanceFnc(to->point, altN->point))
            {
                closestN = from;
                closestOppoN = to;
            }
            else
            {
                closestN = to;
                closestOppoN = from;
            }
            double distWay1 = m_para.distanceFnc(startN->point, altN->point)
                    + m_para.distanceFnc(altN->point, clu->point->point)
                    + m_para.distanceFnc(clu->point->point, closestN->point)
                    + m_para.distanceFnc(closestN->point, closestOppoN->point);
            double distWay2 = m_para.distanceFnc(startN->point, altN->point)
                    + m_para.distanceFnc(altN->point, closestN->point)
                    + m_para.distanceFnc(closestN->point, clu->point->point)
                    + m_para.distanceFnc(clu->point->point, closestOppoN->point);
            if (distWay1 < distWay2)
            {
                from = altN;
                to = closestN;
            }
        }

        shared_ptr<Node> fromSP(from, no_op_delete());
        shared_ptr<Node> toSP(to, no_op_delete());

        _updateHullOrUpdateSimple(fromSP, clu->point, hull);
        _updateHullOrUpdateSimple(toSP, clu->point, hull);
    }
}


void VoronoiAreaPairGrow::_updateNodes(map<double, Cluster*> &clustersSorted, NodeL &hull)
{
    for (auto &cluE : clustersSorted)
    {
        Cluster *clu = cluE.second;
        ///     * connect c.point to the closest node pn, if pn does not have already two edge1, edge2 nodes.
        for (auto &clN : clu->closestN)
        {
            // check if update allowed
            if (_updateAllowed(clN, clu->point))
            {
                // update
                if (_updateHullOrUpdateSimple(clN, clu->point, hull))
                    break;
            }
        }
    }
}

void VoronoiAreaPairGrow::alignToHull(std::map<double, Cluster*> &clusters, NodeL &hull)
{
    for (const auto &cluE : clusters)
    {
        Cluster* clu = const_cast<Cluster*>(cluE.second);
        if (!clu->point->isConnectedToHull)
        {
            // search hull or end
            Node *node = clu->point.get();
            Node *prev = node->edge2;
            Node *foundNode = nullptr;
            auto foundHullI = hull.end();
            auto hullI = hull.end();
            while (true)
            {
                while (node != nullptr)
                {
                    node->isConnectedToHull = true;
                    hullI = find_if(hull.begin(), hull.end(),
                            [node] (const NodeL::value_type &val)
                    {
                        return val->point == node->point;
                    });
                    if (hullI != hull.end())
                    {
                        break;
                    }
                    node = _nextNode(node, prev);
                }
                if (node == nullptr)
                {       // end without hull node
                    if (prev != nullptr)
                    {       // because there is only one missing edge to the hull for each navel.
                        foundNode = prev;
                        if (foundHullI != hull.end())
                        {
                            // search closest hull
                            auto hullPrevI = foundHullI;
                            if (hullPrevI == hull.begin())
                                hullPrevI = hull.end();
                            --hullPrevI;
                            double distPrev = m_para.distanceFnc(foundNode->point, (*hullPrevI)->point);
                            auto hullNextI = foundHullI;
                            ++hullNextI;
                            if (hullNextI == hull.end())
                                hullNextI = hull.begin();
                            double distNext = m_para.distanceFnc(foundNode->point, (*hullNextI)->point);
                            Node *hullN = nullptr;
                            if (distPrev < distNext)
                            {
                                hullN = (*hullPrevI).get();
                            }
                            else
                            {
                                hullN = (*hullNextI).get();
                            }

                            // update
                            if (foundNode->edge1 == nullptr)
                                foundNode->edge1 = hullN;
                            else if (foundNode->edge2 == nullptr)
                                foundNode->edge2 = hullN;
                            else assert(false);

                            if (hullN->edge1 == nullptr)
                                hullN->edge1 = foundNode;
                            else if (hullN->edge2 == nullptr)
                                hullN->edge2 = foundNode;
                            else assert(false);

                            break;
                        }
                        else
                        {
                            node = clu->point.get();
                            prev = node->edge1;
                        }
                    } else assert(false);
                }
                else
                {       // end with hull node
                    // test into other direction
                    if (foundHullI != hull.end())
                        break;      // because both ends are already connected to hull.
                    foundHullI = hullI;
                    node = clu->point.get();
                    prev = node->edge1;
                }
            }
        }
    }
}

void VoronoiAreaPairGrow::_insertNewNodeToEdge(Way* way, Node *fromNode, Node *insertNode)
{
    /// HINT: prev and next nodes of xN are in nodeList.
    /// TODO [xeboeh] delete childNode - memory leak here.
    // we detect cases, where it is no child node - selectNavel insert innerNodes size 1.
//    TODO .... delete if (cx.insertNode->isChildNode)
//        cx.insertNode->child->node = cx.insertNode;     // update old node --> delete node pointer.
    insertNode->child = nullptr;     // HINT: insertChild expect child == nullptr if new. Else we assume not new!
    insertNode->isChildNode = false;            // will be re-set on insertChild().
    insertNode->isInWay = true;

    if (insertNode->point == Point(64,41)
            //&& neighClu->point->point == Point(44,61)
            //&& nextFrom0->point == Point(95,7)
            )
        int i=5;


    /// 2. Insert xN;
    if (fromNode->child == nullptr || fromNode->isChildNode == false)     // isChildNode == false, for test61
    {       // convex-hull node
        way->insertChild(Way::iterator(fromNode), insertNode);
    } else {
        way->insertChildInNext(fromNode, insertNode);
    }
    insertNode->isDeleted = false;

    /// align partners first to new edges
//    _alignPartners(way, fromNode, insertNode);
//    _alignPartners(way, insertNode, Way::nextChild(insertNode));
}


void VoronoiAreaPairGrow::_insertAsNeighbour(Node *node, Cluster *clu)
{
    Node *next = const_cast<Node*>(Way::nextChild(node));
    clu->neighbourEdges.insert({
                seboehDist(next, node, clu->point.get())
                , tuple<Node*,Node*>(next, node)
                               });
    Node *prev = const_cast<Node*>(Way::prevChild(node));
    clu->neighbourEdges.insert({
                seboehDist(prev, node, clu->point.get())
                , tuple<Node*,Node*>(prev, node)
                               });
}

void VoronoiAreaPairGrow::_runEvolutionaryGrow(map<double, Cluster*> &clustersM, Way *way)
{
    /// * run by inserting best alignment first
    size_t growCounter = 0;
    while (!clustersM.empty())
    {
        ++growCounter;
        bool success = false;
        bool neighbourInserted = false;
        for (auto cluI = clustersM.begin();
             cluI != clustersM.end(); ++cluI)
        {
            auto cluNextI = cluI; ++cluNextI;
            double distNext = 0;
            if (cluNextI == clustersM.end())
                distNext = std::numeric_limits<double>::max();
            else
                distNext = cluNextI->second->neighbourEdges.begin()->first;      // best possible align in next cluster

            Cluster *clu = cluI->second;
            Cluster *neighClu = nullptr;

            if (clu->point->point ==
                    //Point(603, 367)
                    //Point(64,41)
                    Point(44,61)
                    //&& partnerN->point == Point(44,61)
                    //&& from->point == Point(95,7)
                    )
                int i=5;


            /// * check alignment for node combination
            size_t cancelReason = 0;
            for (const auto &neighEdgeE : clu->neighbourEdges)
            {
                if (neighEdgeE.first <= distNext)
                {
                    const std::tuple<Node*, Node*> &edge = neighEdgeE.second;
                    if (std::get<0>(edge)->isInWay
                        && std::get<1>(edge)->isInWay)
                    {       // alignment possible

                        Node *nextFrom0 = const_cast<Node*>(Way::nextChild(get<0>(edge)));
                        Node *nextFrom1 = const_cast<Node*>(Way::nextChild(get<1>(edge)));
                        Node *from = nullptr;
                        Node *to = nullptr;
                        if (nextFrom0 == get<1>(edge))
                        {
                            from = get<0>(edge);
                            to = get<1>(edge);
                        }
                        else if (nextFrom1 == get<0>(edge))
                        {
                            from = get<1>(edge);
                            to = get<0>(edge);
                        }
                        if (from != nullptr)
                        {
                            /// * insert
                            bool neighFound = false;
                            auto findNeighDistI = clu->neighbourRefs.find(neighEdgeE.first);
                            if (findNeighDistI != clu->neighbourRefs.end())
                            {
                                auto &&neighRefMap = findNeighDistI->second;
                                auto findNeighFromI = std::find_if(neighRefMap.begin(), neighRefMap.end(),
                                                                [from] (std::map<Node*, NeighbourRef>::value_type &val)
                                {
                                    return from->point == val.first->point;
                                });

                                if (findNeighFromI != findNeighDistI->second.end())
                                {       // found neighbour
                                    neighClu = findNeighFromI->second.neighClu;
                                    // if neigh is already inserted, do not insert this clu
                                    if (!neighClu->point->isInWay)
                                    {
                                        neighFound = true;
                                        _insertNewNodeToEdge(way, from, clu->point.get());
                                        double dist1 = seboehDist(from, clu->point.get(), neighClu->point.get());
                                        double dist2 = seboehDist(clu->point.get(), to, neighClu->point.get());
                                        if (dist1 < dist2)
                                        {
                                            _insertNewNodeToEdge(way, from, neighClu->point.get());
                                        }
                                        else
                                        {
                                            _insertNewNodeToEdge(way, clu->point.get(), neighClu->point.get());
                                        }
                                        neighbourInserted = true;
                                    }
                                }
                            }
                            if (!neighFound)
                            {
                                _insertNewNodeToEdge(way, from, clu->point.get());
                            }
                            success = true;
                            break;
                        }       // else, not really an existing edge, because there are points inbetween
                        else
                        {
                            _insertAsNeighbour(get<0>(edge), clu);
                            _insertAsNeighbour(get<1>(edge), clu);
                        }
                    }
                    else
                    {
                        // if the grow iteration counter reaches the elemets position, it is time to take the next best.
                        if (growCounter < clu->growPosition)
                        {           // it is still possible to wait for the better alignment - not necessary to get aligned.
                            cancelReason = 1;
                            break;      // because further alignment is not good, it could be, that the best edge will come
                        }   // else, choosing best next alignment
                    }
                }
                else
                {
                    cancelReason = 2;
                    break;
                }
            }
            if (cancelReason == 0
                    && !success)
            {       // because, there is only one point which needs to align, but his neigbours are not read,
                    // it is necessary to align the point over the whole in this moment existing TSP.
                auto wayI = way->begin();
                auto wayPrevI = wayI;
                wayI.nextChild();
                Node *from = nullptr;
                double bestDist = std::numeric_limits<double>::max();
                Node *bestFrom = nullptr;
                while (!wayI.end())
                {
                    from = &wayPrevI;
                    double dist = seboehDist(wayPrevI.nextChild(), wayI.nextChild(), clu->point.get());
                    if (dist < bestDist)
                    {
                        bestDist = dist;
                        bestFrom = from;
                    }
                }
                /// * insert
                _insertNewNodeToEdge(way, bestFrom, clu->point.get());
                success = true;
            }

            // PROBLEM: Because a combination of one neighbour and the main at a less confortable edge
            // for the main can result in better alignment (or way-length), it is a Lookahead necessary.
            // This lookahead will result in high complexity of O(n!) - this is not intended.
            // So the concept of evolutionary algorithms are here failed. 2018.08.22
            // 20180909 continue this approach, by taking the lookahead previously into account.
            // The lookahead will be checked on cluster-edges and then integrated into a table-look.

            // shift for next iteration
            if (success)
            {
                clustersM.erase(cluI);
                if (neighbourInserted)
                {
                    auto findNeigI = find_if(clustersM.begin(), clustersM.end(),
                                             [neighClu] (map<double, Cluster*>::value_type &val)
                    {
                        return neighClu->point == val.second->point;
                    });
                    assert(findNeigI != clustersM.end());
                    clustersM.erase(findNeigI);     // clu will be erased later on also
                }
                break;      // restart
            }       // else, next cluster, because dist is to big in neighs or no possible edge set already.
        }       // next cluster
    }       // end while, all clusters inserted.
}

// dead code
double VoronoiAreaPairGrow::_getBestAlignEdge(Node *partnerN, Node *cluN, Cluster *clu)
{
    double bestAlign = 0;
    for (auto &edge : clu->neighbourEdges)
    {
        Node *from = get<0>(edge.second);
        Node *to = get<1>(edge.second);
        if (from != cluN && to != cluN
            && from != partnerN && to != partnerN)
        {
            bestAlign = edge.first;
            break;
        }
    }
    return bestAlign;
}

void VoronoiAreaPairGrow::_collectPairsEdgesCalc(
        std::map<double, std::tuple<Node*,Node*>> &neighbourEdges
        , Cluster *clu, Cluster *neigh)
{
    Node *cluN = clu->point.get();
    Node *partnerN = neigh->point.get();
//    double bestAlignClu = _getBestAlignEdge(partnerN, cluN, clu);
//    double bestAlignNeigh = _getBestAlignEdge(partnerN, cluN, neigh);
// check against best align does not work
// because there is always a best align - a diagonal close to the clu.
    // double bestAlignClu = clu->neighbourEdges.begin()->first;
    double bestAlignClu = clu->neighbourEdges.begin()->first;
    double bestAlignNeigh = neigh->neighbourEdges.begin()->first;
    double worstAlignClu = (--clu->neighbourEdges.end())->first;
    double worstAlignNeigh = (--neigh->neighbourEdges.end())->first;

    for (auto &edgeE : neighbourEdges)
    {
        ///         * if there is an alignment smaller than best alignment of cl + best alignment of ne,
        ///             add this edge of alignement to cl.neighbours
        Node *from = get<0>(edgeE.second);
        Node *to = get<1>(edgeE.second);
        if (from == clu->point.get()
                || to == clu->point.get()
            || from == neigh->point.get()
                || to == neigh->point.get()
                )
        continue;

        double cluDist = seboehDist(from, to, cluN);
        double partnerDist1 = seboehDist(from, cluN, partnerN);
        double partnerDist2 = seboehDist(cluN, to, partnerN);
        double partnerEffort = min(partnerDist1, partnerDist2);
       // not working if ((partnerEffort + cluDist) < (bestAlignClu + bestAlignNeigh))
        // just add every edge in combination with neighbour.
        if (
//                cluDist <= worstAlignClu       // // test12 --> =
                true
           )
        {
            // calculate portion
            double partnerDist = seboehDist(from, to, partnerN);
            double partnerPart = partnerDist / (partnerDist + cluDist);
            double cluPart = cluDist / (partnerDist + cluDist);
            double commonDist = partnerEffort + cluDist;
            // add to clu
            // HINT: not cluPart * commonDist, but old1+old2-newCommon, because of test12
            // test12
            double cluDist1 = seboehDist(from, partnerN, cluN);
            double cluDist2 = seboehDist(partnerN, to, cluN);
            //double commonDistDetour = (min(cluDist1, cluDist2) + cluDist - bestAlignClu)/2.0;
            // HINT: min(partner..1, partner..2) necessary for test5
            Point crossPoint;
//            auto isCrossed = _isCrossed(from->point, cluN->point, to->point, partnerN->point, &crossPoint);
//            if (isCrossed != CrossedState::isCrossed)
//                isCrossed = _isCrossed(from->point, partnerN->point, to->point, cluN->point, &crossPoint);
//            assert(isCrossed == CrossedState::isCrossed);

//            double commonDistDetour =
//                    min(cluDist1, cluDist2)
//                    + min(partnerDist1, partnerDist2)
//                    //- seboehDist(from->point, to->point, crossPoint)
//                    //- (cluDist+partnerDist)/2.0     // negative not possible --> total crossing chaos
//                    ;
            // thought, but failed - pow(min(..),2) because test10-2
//            double commonDistDetour =
//                    pow(1+min(cluDist1, cluDist2),2)
//                    + pow(1+min(partnerDist1, partnerDist2),2)
//                    ;
            Point center = (cluN->point + partnerN->point + from->point + to->point)/4;
//            double commonDistDetour =
//                    min(cluDist1, cluDist2)*m_para.distanceFnc(center, cluN->point)/bestAlignClu
//                    + min(partnerDist1, partnerDist2)*m_para.distanceFnc(center, partnerN->point)/bestAlignClu
//                    ;
            double areaClu = _area({from->point, to->point, cluN->point});
            double areaPart = _area({from->point, to->point, partnerN->point});

            double len = m_para.distanceFnc(from->point, to->point)/2.0;
            Point mid = (cluN->point + partnerN->point)/2.0;
            Point diff = Point(to->point - from->point)/2.0;
            Point orth = (from->point + to->point)/2.0;
            Point orth1 = orth + Point(-diff.y(), diff.x());        // for test12, but not for test10-2
            Point orth2 = orth + Point(diff.y(), -diff.x());        // for test10-2, but not for test12
//            if (m_para.distanceFnc(cluN->point, orth1) < m_para.distanceFnc(cluN->point, orth2))
                orth = orth1;
//            else
//                orth = orth2;

//            double commonDistDetour =
//                    min(seboehDist(from->point, orth, cluN->point)
//                        , seboehDist(to->point, orth, cluN->point))
//                    + min(seboehDist(from->point, orth, partnerN->point)
//                        , seboehDist(to->point, orth, partnerN->point))
//                    ;
            //double commonDistDetour = commonDist * cluPart;
            //double commonDistDetour = commonDist * partnerPart;
            //double commonDistDetour = commonDist / m_para.distanceFnc(from->point, to->point);

            if ((get<0>(clu->neighbourEdges.begin()->second) == from
                 && get<1>(clu->neighbourEdges.begin()->second) == partnerN)
                    ||
                (get<0>(clu->neighbourEdges.begin()->second) == partnerN
                 && get<1>(clu->neighbourEdges.begin()->second) == to))
            {
//                bestAlignClu = (++clu->neighbourEdges.begin())->first;
                // because pesimistic, neigh can have its bestAlign.
            }

            //double commonDistDetour = commonDist * (bestAlignClu / (bestAlignClu + bestAlignNeigh));

            double commonDistDetour = commonDist / 2;

            if (cluN->point ==
                    //Point(603, 367)
                    //Point(64,41)
                    Point(42,32)
                    //&& partnerN->point == Point(44,61)
                    //&& from->point == Point(95,7)
                    )
                int i=5;

            if (cluN->point ==
                    //Point(603, 367)
                    //Point(64,41)
                    Point(44,61)
                    //&& partnerN->point == Point(44,61)
                    //&& from->point == Point(95,7)
                    )
                int i=5;


//            if (commonDist <= (bestAlignClu + bestAlignNeigh))
//            if (false)
            {
                PartnerEl pel;
                pel.from = from;
                pel.to = to;
                pel.partner1 = clu;
                pel.partner2 = neigh;
                pel.commonDist = commonDist;
                bool isAdd = true;
                auto partnerI = m_partners.find(from);
                if (partnerI != m_partners.end())
                {
                    auto partnerToI = partnerI->second.find(to);
                    if (partnerToI != partnerI->second.end())
                    {
                        if (m_partners[from][to].commonDist <= pel.commonDist)
                        {
                            isAdd = false;
                        }
                    }
                }
                if (isAdd)
                {
                    m_partners[from][to] = pel;
                    m_partners[to][from] = pel;
                }
            }

            /// HINT: Failed here!!!! Due to missing knowledge about correct sorting of neighbourEdges and points to align.
//            if (commonDist < (bestAlignClu + bestAlignNeigh))
            {
                clu->neighbourEdges.insert({commonDistDetour, make_tuple(from, to)});
                //clu->neighbourRefs[cluPart * commonDist][from].neighClu = neigh;
                clu->neighbourRefs[commonDistDetour][from].neighClu = neigh;
            }

            // add to neigh
//            if (partnerDist1 < partnerDist2)
//            {
//                //neigh->neighbourEdges.insert({partnerPart*commonDist, make_tuple(from, cluN)});
//                neigh->neighbourEdges.insert({partnerDist1, make_tuple(from, cluN)});
//            }
//            else
//            {
//                neigh->neighbourEdges.insert({partnerDist2, make_tuple(cluN, to)});
//            }
        }
    }
}

void VoronoiAreaPairGrow::_collectPairsEdges(std::map<double, Cluster*> &clustersM)
{
    /// * for each cluster cl
    for (auto &cluE : clustersM)
    {
        Cluster *clu = cluE.second;
        ///     * for all neighbours ne
        for (Cluster * &neigh : clu->closestSortedNeighbours)
        {
            ///         * check the common alignment on all edges between neigbours of cl and all neighbours of neighbour ne
            _collectPairsEdgesCalc(clu->neighbourEdges, clu, neigh);
            _collectPairsEdgesCalc(neigh->neighbourEdges, clu, neigh);
        }
    }
}

void VoronoiAreaPairGrow::_alignPartners(Way *way, Node* from, Node *to)
{
    auto partnerFromI = m_partners.find(from);
    if (partnerFromI != m_partners.end())
    {
        auto partnerToI = partnerFromI->second.find(to);
        if (partnerToI != partnerFromI->second.end())
        {
            PartnerEl parEl = m_partners[from][to];
            _insertNewNodeToEdge(way, parEl.from, parEl.partner1->point.get());
            _insertNewNodeToEdge(way, parEl.partner1->point.get(), parEl.partner2->point.get());
        }
    }
}

void VoronoiAreaPairGrow::_addPartnersToHull(Way *way)
{
    Node *prev = &(--way->begin());
    for (auto wayI = way->begin();
         !wayI.end(); ++wayI)
    {
        _alignPartners(way, prev, &wayI);
        prev = &wayI;
    }
}

VoronoiAreaPairGrow::NodeL VoronoiAreaPairGrow::determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way)
{
    assert(way->size() > 1);

    /// * create structure
    map<double, Cluster*> clustersM;
    for (auto &cluE : clustersA)
    {
        Cluster *clu = cluE.get();

        /// * pre-calc cluster alignments at neighbours
        auto neighNextI = clu->closestTestedPointsSorted.begin();
        for (Node* &neigh : clu->closestTestedPointsSorted)
        {
            ++neighNextI;
            if (neighNextI == clu->closestTestedPointsSorted.end())
                neighNextI = clu->closestTestedPointsSorted.begin();
            double dist = seboehDist(neigh, *neighNextI, clu->point.get());
            clu->neighbourEdges.insert({dist, {neigh, *neighNextI}});
        }

        clustersM.insert({clu->area, clu});
    }
    size_t growPos = 0;
    for (auto &cluE : clustersM)
    {
        cluE.second->growPosition = ++growPos;
    }

    for (auto &cluE : clustersM)
    {
        Cluster *clu = cluE.second;
        for (const Node * node : clu->closestTestedPointsSorted)
        {
            auto cluI = find_if(clustersM.begin(), clustersM.end(),
                                [node] (const map<double, Cluster*>::value_type &val)
            {
                return node == val.second->point.get();
            });
            if (cluI != clustersM.end())
                clu->closestSortedNeighbours.push_back(cluI->second);
        }
    }

    for (auto wayI = way->begin();
         !wayI.end(); ++wayI)
        (&wayI)->isInWay = true;

    /// * find pairs - see test 10-2.png
    _collectPairsEdges(clustersM);

    /// * add pairs=partners first
    //_addPartnersToHull(way);

    /// * align to best neighbours along the hull
    _runEvolutionaryGrow(clustersM, way);


    /// * create result-struct
    NodeL ret;
    auto wayI = way->begin();
    while (!wayI.end())
        ret.push_back(make_shared<Node>(*wayI.nextChild()));

    return ret;
}

void VoronoiAreaPairGrow::buildTree(DT::DTNode::NodeType node, float level, TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    m_resultLength = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    incProgress(progressPercentage);

    // * intern Input representation
    auto input = createInputRepresentation(openPointsBasic);
    m_tspSize = input.size();

    // * determine hull
    auto res = determineHull(input);
    auto hull = get<0>(res);
    Way *way = get<1>(res);

    // * determine Voronoi cluster
    auto clusters = determineClusters(input, hull);

    // * build final path
    auto finalWay = determineWay(clusters, hull, way);

//    if (m_isCanceled)
//    {
        // set any way - like the input, because we want valid m_resultLength values at further processing.
//        m_shortestWay.clear();
//        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
//             openNodeIter != openPointsBasic->end(); ++openNodeIter)
//        {
//            m_shortestWay.push_back(**openNodeIter);
//        }
//        m_mutex.lock();
//        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
//        m_mutex.unlock();
//    }

    /// copy result to final output
    for (auto pointI = finalWay.begin();
         pointI != finalWay.end(); ++pointI)
    {
        TSPWay w;
        w.push_back((*pointI)->point);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial ways - drop this tree of ways.
        (*newChildIter)->parent(node);
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}
