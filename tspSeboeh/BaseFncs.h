#pragma once

#include "Point.h"

namespace TSPHelper {

    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };

    CrossedState isCrossed(const TSP::Point &pa1I, const TSP::Point &pa2I, const TSP::Point &pb1I, const TSP::Point &pb2I, TSP::Point *crossPoint = nullptr);


}

