#ifndef INDICATORS_H
#define INDICATORS_H

#include "Result.h"

namespace TSP {

struct Indicators {
    struct States { enum StateType { ISNULL=1, ISRUNNING=2, ISFINISHED=4, ISCANCELED=8 }; };

    Indicators() : mState(States::ISNULL) {}

    /**
     * @brief 100% is return value 1.0.
     * @return 0.0-1.0 indicate the percentage of progress.
     */
    inline float progress() { return mProgress; }
    inline void progress(float p) { mProgress = p; }
    inline int decissionTreeDepth() { return mDtDepth; }
    inline void decissionTreeDepth(int d) { mDtDepth = d; }
    inline Result intermediateResult() { return mResult; }
    inline void intermediateResult(const Result &res) { mResult = res; }
    inline float intermediateMeasure() { return mMeasure; }
    inline void intermediateMeasure(float m) { mMeasure = m; }
    inline float remainingTime() { return mRemainingTime; }
    inline void remainingTime(float t) { mRemainingTime = t; }
    inline float remainingMeasure() { return mRemainingMeasure; }
    inline void remainingMeasure(float m) { mRemainingMeasure = m; }
    inline float level() { return mLevel; }
    inline void level(float l) { mLevel = l; }
    inline int state() { return mState; }
    inline void state(int st) { mState = st; }

private:
    float   mProgress;
    int     mDtDepth;
    Result  mResult;
    float   mMeasure;
    float   mRemainingTime;
    float   mRemainingMeasure;
    float   mLevel;
    int     mState;

    // only for debug app 2016
public:
    std::list<std::list<oko::ClosestEdges>> mNavels;
};
}



#endif // INDICATORS_H
