#ifndef RESULT_H
#define RESULT_H

#include "Way.h"
#include "bspTreeVersion/BspTree.h"
#include "dll.h"

namespace TSP {

struct EXTERN Result {
    Result() : mBspTree(NULL) {}

    inline Way * way() { return &mWay; }
    inline void way(const Way &val) { mWay = val; }
    inline float time() { return mTime; }
    inline void time(float t) { mTime = t; }
    inline float measure() { return mMeasure; }
    inline void measure(float m) { mMeasure = m; }
    inline float wayLength() { return mWayLength; }
    inline void wayLength(float wl) { mWayLength = wl; }
    inline bsp::BspTree * bspTree() { return mBspTree; }
    inline void bspTree(bsp::BspTree* val) { mBspTree = val; }
    inline void wasCancled(bool val) { mWasCancled = val; }

private:
    Way     mWay;
    float   mTime;
    float   mMeasure;
    float   mWayLength;
    bsp::BspTree *  mBspTree = nullptr;
    bool    mWasCancled=false;
};

}

#endif // RESULT_H
