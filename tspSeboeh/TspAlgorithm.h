#ifndef TSPALGORITHM_H
#define TSPALGORITHM_H

#include <QThread>

#include "Point.h"
#include "Parameters.h"
#include "Result.h"
#include "Indicators.h"
#include "DTTree.h"

namespace TSP {

class TspAlgorithm : public QThread
{
public:
    typedef std::list<Point> InputCitiesType;
    typedef std::list<Point*> InputCitiesPointerType;

    TspAlgorithm() {}

    /**
     * @brief ~TspAlgorithm - this class is "pure virtual".
     */
    virtual ~TspAlgorithm() {}

    virtual void startRun(InputCitiesType *in, const Parameters &para) = 0;

    /**
     * @brief run is started by the QThread.
     */
    virtual Indicators status() = 0;

    virtual Result result() = 0;

    virtual void pause() = 0;

    virtual void stopRun() = 0;

    /** HINT: used for debugging reasons.
     * @return the decission tree of the algorithm. It is used mainly for debugging reasons.
     */
    virtual DT::DTTree * getDtTree() = 0;

	virtual void setBlockedEdges(std::list<std::pair<Point,Point>> &&edges) = 0;

protected:
    virtual void run() = 0;

};

}
#endif // TSPALGORITHM_H
