#pragma once

#include "TspAlgoCommon.h"
#include <chrono>
#include "dll.h"

namespace TSP {

class EXTERN TspAlgoNaiveIterative : public TspAlgoCommon
{
public:
    TspAlgoNaiveIterative() : TspAlgoCommon() {}

    // TspAlgoCommon interface
protected:
    void buildTree(DT::DTNode::NodeType node, float, InputCitiesPointerType *openPointsBasic);

private:
    Way     m_shortestWay;
    double  m_length=0;
    std::chrono::system_clock::time_point m_startTime;
    long    m_stackDepth=0;

    void recursiveForLoops(Way &currentWay, InputCitiesPointerType *points);
    InputCitiesPointerType * createNewWay(InputCitiesPointerType::iterator openNodeIter, TSP::TspAlgorithm::InputCitiesPointerType *openPointsBasic);
};

}
