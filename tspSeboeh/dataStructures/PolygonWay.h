/**
 * cyclic double linked list.
 */
#pragma once

#include <iterator>
#include <stack>
#include <map>
#include <memory>
#include <assert.h>
#include <iostream>

#include "Point.h"
#include "bspTreeVersion/BspNode.h"

namespace oko {

/// HINT: oko::Node is defined in BspNode.h

class PolygonWay {
private:
    class Itr {
        friend class PolygonWay;

    public:
        Itr() : m_cur(nullptr), m_end(nullptr), m_isFirstTime(true) {}
        explicit Itr(Node *val) : Itr() { m_cur = val; m_end = m_cur; }
        Itr(const Itr &val) = default;
        ~Itr() = default;
        Itr& operator++() {
            if (m_isFirstTime) m_isFirstTime = false;
            if (m_cur != nullptr) { m_cur = m_cur->next; }
            return *this;
        }        // Next element
        Itr& operator--() {
            if (m_isFirstTime) m_isFirstTime = false;
            if (m_cur != nullptr) { m_cur = m_cur->prev; }
            return *this;
        }
        TSP::Point & operator*() { return m_cur->point; }                                     // Dereference
        Node* operator&() { return m_cur; }
        bool operator==(const Itr& o) const { return m_cur == o.m_cur; }     // Comparison
        bool operator!=(const Itr& o) const { return !(*this == o); }
        bool end() { if (m_cur == nullptr) { return true; } else if (m_isFirstTime) { return false; } else { return m_cur == m_end; } }
        void reset(const Itr &val) {
            m_cur = val.m_cur;
            m_end = m_cur;
            m_isFirstTime = true;
        }

    protected:
        Node * m_cur = nullptr;
        Node * m_end = nullptr;
        bool m_isFirstTime;
    };

public:
    typedef Itr iterator;

    PolygonWay()
        : m_start(nullptr)
        , m_back(nullptr)
        , m_size(0)
    {
    }

    /**
     * @brief Not possible to use clear();, because filled with Node pointers form outside.
     */
    ~PolygonWay() {
    }

    /** HINT: we need to create a new operator of m_start, because m_start.m_end could changed outside.
     * And m_start could be changed in searchNewStart().
     * m_start have to be a convex-hull point, because we use iterator::end() to iterate over convex-hull.
     */
    iterator begin() {
        return m_start;
    }

    iterator push_back(const TSP::Point &&val) {
        return push_back(new Node(std::move(val)));
    }

    iterator push_back(Node *val) {
        if (&m_back == nullptr) {
            // if m_back == nullptr then also m_start == nullptr
            m_back = iterator(val);
            val->next = val;
            val->prev = val;
            m_start = m_back;
        } else {
            (&m_back)->next = val;
            val->prev = &m_back;
            val->next = &m_start;
            ++m_back;
            (&m_start)->prev = val;
        }
        ++m_size;
        return m_back;
    }

    /**
     * @brief clear deletes all the pointers created or inserted with push_back().
     * Use carfully. Is not used in destructor.
     */
    void clear() {
        if (empty())
            return;
        if (m_size == 1)
        {
            delete (&begin());
        } else {
            Node *n = &begin();
            n->prev->next = nullptr;        // because to stop
            while (n != nullptr)
            {
                Node *nt = n->next;
                delete n;
                n = nt;
            }
        }
        m_back = m_start = iterator(nullptr);
        m_size = 0;
    }

    size_t size() const { return m_size; }
    bool empty() const { return m_start == iterator(nullptr); }

    iterator & insert(iterator &iter, const TSP::Point &val) {
        return insert(iter, new Node(val));
    }

    /// @brief insert val before iter! Only from size > 2 allowed. Only for assigning on convex-hull.
    iterator & insert(iterator iter, Node *val) {
        assert(val);

        Node *node = &iter;
        val->prev = node->prev;
        val->next = node;
        val->isInWay = true;
        node->prev->next = val;
        node->prev = val;

        ++m_size;

        /// clear iterator position
        return --iter;
    }

    void remove(Node* n)
    {
        // TODO [seboeh] check if n == m_start , m_back
        n->prev->next = n->next;
        n->next->prev = n->prev;
        n->isInWay = false;
        --m_size;
    }

    iterator erase(const iterator &iterA)
    {
        auto iter = iterA;
        Node *n = &iter;
        if (n == &m_back)
            m_back.reset(--m_back);
        if (n == &m_start)
            m_start.reset(++m_start);
        ++iter;
        if (n == iter.m_end)
        {
            iter.m_end = iter.m_end->next;
                // next to guaranty last element checked.
            iter.m_isFirstTime = true;
                // to prevent immediatelly stopping at ++iter.
        }
        remove(n);
        return iter;
    }

private:
    iterator    m_start;
    iterator    m_back;
    size_t      m_size;
};

}
