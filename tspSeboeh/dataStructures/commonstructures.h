#pragma once

#include <list>

#include "bspTreeVersion/BspNode.h"

namespace comdat {
    using Node = oko::Node;
    struct ResultElement
    {
        Node* hullEdgeFrom = nullptr;
        std::list<Node*> innerNodes;
    };
    using Result = std::list<ResultElement>;

}

