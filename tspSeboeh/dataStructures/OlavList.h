/**
 * cyclic double linked list.
 */
#pragma once

#include <iterator>
#include <stack>
#include <map>
#include <memory>
#include <QMutex>
#include <assert.h>
#include <iostream>

#include "Point.h"
#include "bspTreeVersion/BspNode.h"
#include "BaseFncs.h"

static void debugCheckPointerWriteAccess(oko::Node * &accessAddress, oko::Node *newValue = nullptr, bool isChildUnknown = false)
{
    if (reinterpret_cast<long>(accessAddress) < 10
         ||
         !(!accessAddress->isChildNode
            && newValue->isChildNode)
         ||
         isChildUnknown
        )
    {
        if ( (std::addressof(accessAddress) == reinterpret_cast<oko::Node**>(0x7fffd0003df0)
             && newValue == reinterpret_cast<oko::Node*>(0x7fffd0005b80))
             || (accessAddress != nullptr
                 && accessAddress != reinterpret_cast<oko::Node*>(0x1)
                 && accessAddress->point.x() == 337))
            int i=5;
        accessAddress = newValue;
    }
    // else do nothing, because there is a hull point, and the next/prev are already set.
}

static void debugCheckPointerWriteAccessNext(oko::Node * &accessAddress, oko::Node *newValue, bool isChildUnknown = false)
{
    if (reinterpret_cast<long>(accessAddress) < 10
         ||
         !(!accessAddress->isChildNode
            && newValue->isChildNode)
         || isChildUnknown
        )
    {
        if ( std::addressof(accessAddress) == reinterpret_cast<oko::Node**>(0x7fffd0006f10)
             && newValue == reinterpret_cast<oko::Node*>(0x7fffd0003df0))
            int i=5;

        accessAddress = newValue;
    }
    // else do nothing, because there is a hull point, and the next/prev are already set.
}


static void debugCheckPointerWriteAccessSetNodeChild(oko::Node * &accessAddress, oko::Node *newValue, bool isChildUnknown = false)
{
    if (reinterpret_cast<long>(accessAddress) < 10
         ||
         !(!accessAddress->isChildNode
            && newValue->isChildNode)
         || isChildUnknown
        )
    {
        if ( std::addressof(accessAddress) == reinterpret_cast<oko::Node**>(0x7fffd0005b80)
             && newValue == reinterpret_cast<oko::Node*>(0x7fffd000df10))
            int i=5;

        accessAddress = newValue;
    }
    // else do nothing, because there is a hull point, and the next/prev are already set.
}


namespace oko {
    struct NodeChild;
}
static void debugCheckPointerWriteAccessNodeChild(oko::NodeChild * accessAddress, oko::Node *newValue)
{
    if ( accessAddress == reinterpret_cast<oko::NodeChild*>(0x7fffd0005740)
         && newValue == reinterpret_cast<oko::Node*>(0x7fffd0006570))
        int i=5;
}


namespace oko {

/// HINT: oko::Node is defined in BspNode.h

struct NodeChild {
    NodeChild() : prev(nullptr), next(nullptr), node(nullptr) {}
    NodeChild(Node * p, Node * prevEdgeP, Node * nextEdgeP) : NodeChild() {
        debugCheckPointerWriteAccessNodeChild(this, p);
        node = p;
        if (prevEdgeP != nullptr)
        {       // HINT: necessary for test10-2. test10-2 would run into problems with prev == nullptr in insertChild().
                // Problem probably caused by createNodeChild().
            assert(prevEdgeP != nullptr && nextEdgeP != nullptr);
            debugCheckPointerWriteAccess(node->prev , prevEdgeP, true);
            debugCheckPointerWriteAccessNext(node->next , nextEdgeP, true);
        }
        assert((node->next != nullptr && node->prev != nullptr) || (node->next == nullptr && node->prev == nullptr));
// TODO [seboeh] not acceptable, because remove and reinsert of embrio in test 54.
//        assert((prevEdgeP == nullptr && nextEdgeP == nullptr) || (prevEdgeP->next != nullptr && prevEdgeP->prev != nullptr && nextEdgeP->next != nullptr && nextEdgeP->prev != nullptr));
//        assert((prevEdgeP == nullptr && nextEdgeP == nullptr) || (prevEdgeP->next->next != nullptr && prevEdgeP->prev->prev != nullptr && nextEdgeP->next->next != nullptr && nextEdgeP->prev->prev != nullptr));
        node->debugInfo = 8;
        node->isChildNode = true;
        node->child = this;
    }

    NodeChild * prev = nullptr;       // TODO [seboeh] insert parent and replace child->node->prev.
    NodeChild * next = nullptr;
    Node * node = nullptr;
    NodeChild **parent = nullptr;     ///< pointer to prev/next NodeChild pointers. Or nullptr if child->node->prev (parent) is no NodeChild
    NodeChild *childParent = nullptr;
};




class OlavList {
private:
    class Itr {
    public:
        Itr() : m_cur(nullptr), m_end(nullptr), isForward(false), isBackward(false), m_isFirstTime(true) {}
        explicit Itr(Node *val) : Itr() { m_cur = val; m_end = m_cur; }
        Itr(const Itr &) = default;
        ~Itr() {}
        Itr& operator++() {
            if (m_isFirstTime) m_isFirstTime = false;
            if (m_cur != nullptr) { m_cur = m_cur->next; }
            return *this;
        }        // Next element
        Itr& operator--() {
            if (m_isFirstTime) m_isFirstTime = false;
            if (m_cur != nullptr) { m_cur = m_cur->prev; }
            return *this;
        }
        /// returns node and iterates to next node on complete intern polygon
        Node* nextChild() {
            assert(!isBackward);        // don't use next in a backward iterator.
            if (m_cur == nullptr) return nullptr;       // found a nullpointer on the way - TODO [seboeh] omit this case.
            if (m_cur == reinterpret_cast<Node*>(0x1))
                return nullptr;
            if (m_isFirstTime) m_isFirstTime = false;
            isForward = true;
            if (m_childStack.empty()) {
                if (m_cur->child != nullptr) {
                    if (m_cur->isChildNode) {
                        if (m_cur->child->next != nullptr) {
                            fillStackForward(m_cur->child->next);
                        } else {
                            if (m_cur == m_end) m_isFirstTime = true;       // only necessary for first run of this method. Used for end().
                            Node* n = m_cur; ++*this;
                            return n;
                        }
                    } else {
                        fillStackForward(m_cur->child);
                    }
                    if (m_cur == m_end) m_isFirstTime = true;       // only necessary for first run of this method
                    return &(*this);
                } else {
                    Node* n = &(*this); ++*this;
                    return n;
                }
            } else {
                NodeChild* p = m_childStack.top();
                if (p->next == p)
                {
                    std::cerr << "Error: cyclic polygon. The polygon contains a node, which referes to itsself." << std::endl;
                    assert(false);
                    exit(-1);
                }
                m_childStack.pop();
                if (m_cur == m_end) m_isFirstTime = true;       // only necessary for first run of this method
                if (p->next != nullptr) {
                    fillStackForward(p->next);
                } else {
                    if (m_childStack.empty()) ++*this;
                }
                return p->node;
            }
        }
        // TODO [xeboeh] drop backward iteration. It is not used.
        TSP::Point prevChild() {
            assert(!isForward);             // don't use prev in a forward iterator.
            isBackward = true;
            if (m_childStack.empty()) {
                if (m_cur->prev->child != nullptr) {
                    fillStackBackward(m_cur->prev->child);
                    if (m_cur == m_end) m_isFirstTime = true;
                    return **this;
                } else {
                    TSP::Point p = **this; --*this;
                    return p;
                }
            } else {
                NodeChild* p = m_childStack.top();
                m_childStack.pop();
                if (m_cur == m_end) m_isFirstTime = true;
                if (p->prev != nullptr) {
                    fillStackForward(p->prev);
                } else {
                    if (m_childStack.empty()) --*this;
                }
                return p->node->point;
            }
        }
        TSP::Point & operator*() { return m_cur->point; }                                     // Dereference
        Node* operator&() { return m_cur; }
        bool operator==(const Itr& o) const { return m_cur == o.m_cur; }     // Comparison
        bool operator!=(const Itr& o) const { return !(*this == o); }
        bool end() { if (m_cur == nullptr) { return true; } else if (m_isFirstTime) { return false; } else { return m_cur == m_end; } }
    private:
        void fillStackForward(NodeChild * cur) { m_childStack.push(cur); if (cur->prev != nullptr) { fillStackForward(cur->prev); } }
        void fillStackBackward(NodeChild * cur) { m_childStack.push(cur); if (cur->next != nullptr) { fillStackBackward(cur->next); } }
        Node * m_cur = nullptr;
        Node * m_end = nullptr;
        bool isForward;
        bool isBackward;
        bool m_isFirstTime;
        std::stack<NodeChild*> m_childStack;
    };

    /** Edges Tree settings **/
    struct EdgeKey {
        float pAX = 0.0f;
        float pAY = 0.0f;
        float pBX = 0.0f;
        float pBY = 0.0f;
        EdgeKey(const TSP::Point &a, const TSP::Point &b) : pAX(a.x()), pAY(a.y()), pBX(b.x()), pBY(b.y()) {}
        bool operator<(const EdgeKey &val) const { if (pAX < val.pAX) return true; if (pAX > val.pAX) return false; if (pAY < val.pAY) return true; if (pAY > val.pAY) return false; if (pBX < val.pBX) return true; if (pBX > val.pBX) return false; return pBY < val.pBY; }
    };

    typedef std::map<EdgeKey, NodeChild*> EdgesTreeType;

public:
    typedef Itr iterator;

    OlavList()
        : m_start(nullptr)
        , m_back(nullptr)
        , m_size(0)
    {
        m_edgesTree = new EdgesTreeType();
    }

	/**
	 * @brief Not possible to use clear();, because filled with Node pointers form outside.
	 */
	~OlavList() {
        // TODO [seboeh] is it possible to clear way? This means all way have to be copied!		clear();
        delete m_edgesTree;
        m_edgesTree = nullptr;
        // TODO [seboeh] does not work - test106 delete m_origin;
        m_origin = nullptr;
	}

    static Node * nextChild(const Node* node)
    {
        assert(node != nullptr);
        if (node == reinterpret_cast<Node*>(0x1))        // because node is not yet set
            return nullptr;
        // is necessary for test23.png and navelZipo and removeFromEdgeLists.
        if ((node->next == nullptr   // no hull next node
             && node->isChildNode == false
             && node->child == nullptr)   // no child next node
            || (node->isChildNode == true
                && node->child->next == nullptr
                && node->next == nullptr)
            )
            return nullptr;     // no next child exists. Node not in tree! Catched in removeFromEdgeLists().
        iterator iter(const_cast<Node*>(node));
        iter.nextChild();       // returns node first time.
        // TODO [seboeh] See MARK2
        // Because we did not hang the nodes correctly again in the tree after delete, we need to
        // check here for next/prev node.
        // isDeleted, if the node was deleted.
        Node *next = iter.nextChild();
        if (next == nullptr)
            return nullptr;
        Node *start = next;
        while (next->isDeleted && next->next != nullptr && next->next != start)
            next = next->next;
        return next;
    }

    static Node * prevChild(const Node *val)
    {
        if (val == reinterpret_cast<Node*>(0x1))
            return nullptr;
        Node *pointPrev = const_cast<Node*>(val)->prev;     // node to prev on convex-hull.
        if (pointPrev == nullptr)
            return nullptr;
        // speed-up and WORKAROUND: because go one step back and go steps further does not reach original position. see test57.
        if (val->isChildNode && val->child != nullptr)
        {
            if (val->child->next == nullptr)
                return pointPrev;
        }
        // normal calculation
        iterator pointPrevI = iterator(pointPrev);
        Node *pointPrevNext = nullptr;
        // HINT: startingPrevNode != pointPrev, to prevent loops.
        // Those loops can be initiated by nodes which has no incomming node,
        // but an outgoing on the hull. See test69
        // Node *startingPrevNode = pointPrev; - not possible due to test25
        while ((pointPrevNext = pointPrevI.nextChild()) != val
               && pointPrevNext != val->next
               && pointPrev != nullptr)
               //&& startingPrevNode != pointPrev) - not possible due to test25
            pointPrev = pointPrevNext;
        if (pointPrev == nullptr)       // TODO [seboeh] as assert. Problem in clear inner points.
            return nullptr;
        // TODO [seboeh] See MARK2
        while (pointPrev->isDeleted && pointPrev->prev != nullptr)
            pointPrev = pointPrev->prev;
        return pointPrev;
    }

    void setOrigin(bsp::BspNode *origin)
    {
        m_origin = origin;
    }

    bsp::BspNode *getOrigin()
    {
        return m_origin;
    }

    /** HINT: we need to create a new operator of m_start, because m_start.m_end could changed outside.
     * And m_start could be changed in searchNewStart().
     * m_start have to be a convex-hull point, because we use iterator::end() to iterate over convex-hull.
     */
    iterator begin() {
        if (&m_start == nullptr)
        {
            assert(false);
            searchNewStart(&m_back);
        }
        if ((&m_start) != nullptr && (&m_start)->isChildNode)
            searchNewStart(&m_start);       // m_start is inside the way and valid ChildNode, so we could start from there.
        return iterator(&m_start);          // for safety - it sets the m_start.m_end node correct to convex-hull point.
    }

    iterator lastElement() { return iterator(&m_back); }

    iterator push_back(const TSP::Point &val) { return push_back(std::move(TSP::Point(val))); }
    iterator push_back(const TSP::Point &&val) {
        return push_back(new Node(std::move(val)));
	}

    iterator push_back(Node *val) {
        if (m_origin != nullptr)
            val->level = m_origin->m_level;
		if (&m_back == nullptr) {
            // if m_back == nullptr then also m_start == nullptr
			m_back = iterator(val);
            m_start = m_back;
            debugCheckPointerWriteAccessNext((&m_back)->next , &m_start);
            debugCheckPointerWriteAccess((&m_back)->prev , &m_start);
        } else {
            insertEdgeToOrigin(&m_back, val);
            debugCheckPointerWriteAccessNext(val->next , (&m_back)->next);
            debugCheckPointerWriteAccess(val->prev , &m_back);
            debugCheckPointerWriteAccess((&m_back)->next->prev , val);
            debugCheckPointerWriteAccessNext((&m_back)->next , val);
            ++m_back;
        }
        ++m_size;
        val->isDeleted = false;     // MARK2
        return m_back;
    }

	/**
	 * @brief clear deletes all the pointers created or inserted with push_back().
	 * Use carfully. Is not used in destructor.
	 */
    void clear() {
		if (m_size == 0 && &m_start == nullptr)
			return;		// empty list
        // below, we need the first node to detect it.end().
        Node *firstNode = &begin();
        bool isFirstRun = true;
        for (iterator it = begin();
             !it.end(); )
        {
            // HINT: We have an invalid read here in nextChild(), because the next from 'end' is already deleted.
            // We ignore this risk, because this is intended.
            auto node = it.nextChild();
            if (node != nullptr && node->origin != nullptr)
                node->origin->removeFromEdgeLists(node);
            const Node *nextN = nextChild(node);
            if (nextN != nullptr)
                // TODO [seboeh] is this kind of removeFromEdgeLists() really necessary?
                node->origin->removeFromEdgeLists(node, nextN);

            getOrigin()->removeFromEdgeLists(node);

            if (!isFirstRun)
                delete node;
            else
                isFirstRun = false;
        }
        delete firstNode;

        m_size = 0;

        // clear m_edgesTree
        for (EdgesTreeType::iterator iter= m_edgesTree->begin();
             iter != m_edgesTree->end(); ++iter)
        {
            delete iter->second;
        }
        m_edgesTree->clear();       // deleted in destructor.
    }

    size_t size() const { return m_size; }
    bool empty() const { return m_start == iterator(nullptr); }
    size_t recalcSize()
    {
        iterator it = m_start;
        size_t i = 0;
        while (!it.end()) { it.nextChild(); ++i; }
        m_size = i;
        return i;
    }

    void setNewStart(Node *np) {
        m_start = iterator(np);
        if ((&m_start)->next != &m_start)
        {
            m_back = m_start;
            --m_back;
        }
    }

    /// @brief insert point after prevPoint. Call it before redirect of nodes for polygon.
    void insertEdgeToOrigin(Node *prevPoint, Node *point)
    {
        if (m_origin == nullptr) return;
        assert(prevPoint);
        assert(point);
        prevPoint->level = m_origin->m_level;
        point->level = m_origin->m_level;
        // clear edge from remove way, because way1/2 are on same edgeBSP.
        if (point->origin != nullptr && point->next != point && point->next != nullptr)
        {       // See test10.png. TODO [seboeh] deleting is too often. See test12.png.
            point->origin->removeFromEdgeLists(point);
        }
        // clear edge from insert way
        // prevPoint->next != prevPoint->prev := check m_size > 2. - See test39.
        if (prevPoint->origin != nullptr && prevPoint->next != prevPoint->prev)
        {
            prevPoint->origin->removeFromEdgeLists(prevPoint);
        }        // else no edge exists - it is a way with one single points
        m_origin->insertToEdgeLists(prevPoint, point);
        if (prevPoint->next != prevPoint)       // old: m_size > 1)
        {
            // point next is correct set in insertChildInNext() after this method.
            m_origin->insertToEdgeLists(point, const_cast<Node*>(nextChild(prevPoint)));
        }
    }

    iterator & insert(iterator &iter, const TSP::Point &val) {
		return insert(iter, new Node(val));
	}

    /// @brief insert val before iter! Only from size > 2 allowed. Only for assigning on convex-hull.
    iterator & insert(iterator iter, Node *val) {
        assert(val);
        if (m_origin != nullptr)
            val->level = m_origin->m_level;
        /// clear old values from other way or same way about val.
        if (val->prev != nullptr)
        {
            const Node *pointPrev = prevChild(val);
            const Node *pointPrevNext = nextChild(pointPrev);       // because have to be in same way1/2. See test8.png.
            // HINT: Check on pointPrevNext == val, because val could be from an different way.
            if (pointPrev->origin != nullptr && pointPrevNext == val)       // TODO [seboeh] check if necessary? point->prev->next always O point.
                pointPrev->origin->removeFromEdgeLists(pointPrev);
            // else pointPrev not in polygon yet. Happens from mergeWays().
            // And else, pointPrev is a old invalid link.
        }
        if (val->origin != nullptr)     // can be null, if fixI from mergeWays().
            val->origin->removeFromEdgeLists(val);      // inserted from previous insert() call.

        /// redirect val
        debugCheckPointerWriteAccessNext(val->next , &iter);
        debugCheckPointerWriteAccess(val->prev , (&iter)->prev);

        // TODO [seboeh] replace by insertEdgeToOrigin().

        /// clear iterator position
        //assert(m_origin);
        //assert((&iter)->prev->origin);      // use push_back instead of insert, for first nodes.
        if ((&iter)->prev->origin)
        {
            //  edge could be already removed (e.g. innerPoints), but for all other inserts, it is necessary.
            if ((&iter)->prev != nullptr)
            {       // edge can exist.
                if ((&iter)->next != (&iter)->prev)     // = m_size > 2
                {
                    if ((&iter)->prev->origin != nullptr)       // could be, that point was not yet inserted into polygon. E.g. point is way with single point.
                        (&iter)->prev->origin->removeFromEdgeLists((&iter)->prev);
                }
            }
        }

        if (m_origin)
        {
            m_origin->insertToEdgeLists(val->prev, val);
            m_origin->insertToEdgeLists(val, val->next);        // ->next ok, because it is iter. no nextchild necessary.
        }

        debugCheckPointerWriteAccessNext((&iter)->prev->next , val);
        debugCheckPointerWriteAccess((&iter)->prev , val);
        ++m_size;
        val->isDeleted = false;
        assert((val->next != nullptr && val->prev != nullptr) || (val->next == nullptr && val->prev == nullptr));
        return --iter;
    }

	void searchNewStart(Node *n) {
        iterator nI = iterator(n);
		// HINT: we walk only over the old connection of convex-hull (next-node-pointer).
		while (&nI != nullptr && (&nI)->isChildNode) ++nI;        // with ++ we walk over the Node->next pointer, which go from one Node to the next, excluding inner nodes.
		if (&nI != nullptr)
            setNewStart(&nI);                // create new iterator with empty childStack.
    }

    size_t countGeneralRuleCrossingApplied = 0;

    /// @brief: insert only for children to convex hull.
    /// Insert point direct after iter as child.
    /// copy-by-value, because useful for function values and no const operator& possible.
    void insertChild(iterator iter, Node *point) {
        assert((&iter)->isChildNode == false);
        assert((&iter) != point);
        // because we insert a new child, it have to be deleted outside.
        assert(point);
        assert(point->child == nullptr);


        /// general rule on TSP
        /// * no crossing!
        if (false) // (countGeneralRuleCrossingApplied % 2) == 0)
        {
            Node *current = &iter;
            Node *next = nextChild(current);
            next = nextChild(next);
            auto state = TSPHelper::isCrossed(next->point, current->point, current->point, point->point);
            if (state == TSPHelper::CrossedState::isCrossed
                    || state == TSPHelper::CrossedState::isOnPoint
                    || state == TSPHelper::CrossedState::isCovered)
            {
                ++countGeneralRuleCrossingApplied;
                if (next->isChildNode)
                    erase(iterator(next)); // eraseNode(next);
                else
                    erase(iterator(next));
                insertChild(iter, point);
                insertChild(iter, next);
                ++countGeneralRuleCrossingApplied;
                return;
            }
        }


        // continue with insert stuff.
        if (m_origin != nullptr)
        {
            point->level = m_origin->m_level;
            (&iter)->level = m_origin->m_level;
        }

        // HINT: point was before insert a convex-hull-node and now a childNode.
        if ((&iter)->prev == point)
        {       // assume iter is not correct yet. // FIXME: is this check necessary?
            std::cerr << "Error: At insert : (&iter)->prev == point." << std::endl;
            debugCheckPointerWriteAccess((&iter)->prev , (&iter)->prev->prev);        // better to point to an prev->prev, instead a nullptr. Assume way.size() > 3
        }
        if ((&iter)->next == point)
        {
            std::cerr << "Error: At insert : (&iter)->next == point." << std::endl;
            (&iter)->next = (&iter)->next->next;
        }
        // insert edge
        insertEdgeToOrigin(&iter, point);       // early insert, because point should not be chanegd already.

        if (&m_start == point)
            searchNewStart(&iter);
        if ((&iter)->child != nullptr)
        {
            NodeChild * prevN = (&iter)->child;
            while (prevN->prev != nullptr) prevN = prevN->prev;
            point->child = prevN->prev = new NodeChild(point, &iter, prevN->node);
            point->child->parent = &(prevN->prev);
        } else {
            point->child = (&iter)->child = new NodeChild(point, &iter, (&iter)->next);
            point->child->parent = &((&iter)->child);
        }
        // test94
        if (point->child != nullptr)
            point->child->childParent = (&iter)->child;

        // HINT: (&iter)->next != point; because iter is on convex-hull
        assert(point->child != nullptr);
		std::pair<EdgesTreeType::iterator, bool> ret = m_edgesTree->insert( EdgesTreeType::value_type(EdgeKey(point->prev->point,point->next->point), point->child) );
		if (ret.second == false)
		{
			m_edgesTree->insert( EdgesTreeType::value_type(
									 EdgeKey(
										 TSP::Point(m_edgesTree->size(),0)
										 , TSP::Point(0,0))
									 , point->child)
									 );
		}
        // because order (prev/next) of nodes is disrupted by whipeClose...
        // this assert is not possible any longer - assert((&iter)->prev != point);      // this should not happen - recursive loop.
        point->isDeleted = false;       // MAKR2
        assert((point->next != nullptr && point->prev != nullptr) || (point->next == nullptr && point->prev == nullptr));
        ++m_size;
    }

    /**
     * @brief insertChildInNext - hangs the node point on the next nodeChild branch.
     * @param node - can be nodeChild or convex-hull-node
     * @param point - must be nodeChild. isNodeChild will be set to true.
     * Insert point direct after node as Child next pointer.
     */
    void insertChildInNext(Node *node, Node *point) {
        assert(node);
        assert(point);
        assert(node->child != nullptr);     // this method should only be called, if child is not empty, else you can use insert().
        assert(node->isChildNode == true);
        assert(node != point);      // node == point, makes not sense.
        if (m_origin != nullptr)
        {
            node->level = m_origin->m_level;
            point->level = m_origin->m_level;
        }

        insertEdgeToOrigin(node, point);
        if (&m_start == point)
            searchNewStart(node);
        if (node->isChildNode)
        {
            if (node->child->next == nullptr)
            {
                point->child = node->child->next = new NodeChild(point, node, node->next);      // HINT: we really need to iterate over Node->prev/next?
                debugCheckPointerWriteAccess(point->prev , node);
                debugCheckPointerWriteAccessNext(point->next , node->next);
                point->child->parent = &(node->child->next);
            } else {
                NodeChild * prevN = node->child->next;
                while (prevN->prev != nullptr) prevN = prevN->prev;
                point->child = prevN->prev = new NodeChild(point, node, prevN->node);
                debugCheckPointerWriteAccess(point->prev , node);
                debugCheckPointerWriteAccessNext(point->next , prevN->node);
                point->child->parent = &(prevN->prev);
            }
        } else {
            if (node->child == nullptr)
            {
                point->child = node->child = new NodeChild(point, node, node->next);      // HINT: we really need to iterate over Node->prev/next?
                debugCheckPointerWriteAccess(point->prev , node);
                debugCheckPointerWriteAccessNext(point->next , node->next);
                point->child->parent = &(node->child);
            } else {
                NodeChild * prevN = node->child;
                while (prevN->prev != nullptr) prevN = prevN->prev;
                point->child = prevN->prev = new NodeChild(point, node, prevN->node);
                debugCheckPointerWriteAccess(point->prev , node);
                debugCheckPointerWriteAccessNext(point->next , prevN->node);
                point->child->parent = &(prevN->prev);
            }
        }
        point->isChildNode = true;
        assert(point->child != nullptr);
        m_edgesTree->insert( EdgesTreeType::value_type(EdgeKey(node->point, point->next->point), point->child) );
        // TODO [xeboeh] HINT: The m_size is not correct calculated.
        assert(node->prev != point);      // this should not happen - recursive loop.
        point->isDeleted = false;
        assert((point->next != nullptr && point->prev != nullptr) || (point->next == nullptr && point->prev == nullptr));
        ++m_size;
    }

    NodeChild * createNodeChild(Node *np)
    {
        np->child = new NodeChild(np, nullptr, nullptr);
        m_edgesTree->insert( EdgesTreeType::value_type(EdgeKey(
                                                           TSP::Point(
															   m_edgesTree->size()
															   ,0)
                                                           , TSP::Point(0,0))
                                                       , np->child) );
        /// HINT: No edge is inserted to BSPEdge in createNodeChild, because nodeChild pointer is managed by polygon,
        /// and this nodeChild does not have edges yet, because it is not inserted yet.
        return np->child;
    }

    /// @deprecated don't use - TODO [seboeh] dead code
    void eraseChildFromTree(const TSP::Point &pa, const TSP::Point &pb) {
        auto findI = m_edgesTree->find( EdgeKey(pa, pb) );
        if (findI != m_edgesTree->end())
            m_edgesTree->erase(findI);
    }

    void removeEdge(const Node* dn, bool isWithoutNewEdge = false)
    {
        int countFound = 0;
        // remove is every time direct possible, because removeEdge will be called from insert.
        // And insert will be called at way with length > 3.
        const Node *pointPrev = prevChild(dn);      // node to prev on convex-hull.
        // HINT: pointPrev can be nullptr, because the element before is already deleted.
        if (pointPrev != nullptr)
        {
            countFound += pointPrev->origin->removeFromEdgeLists(pointPrev);
            countFound += m_origin->removeFromEdgeLists(pointPrev);        // necessary, because collectAllCloseInnerNOdesWalk(). See test39.
        }
        assert(countFound == 2 || countFound == 0);     // it could be already deleted -> 0.

        countFound = 0;
        countFound += dn->origin->removeFromEdgeLists(dn);
        countFound += m_origin->removeFromEdgeLists(dn);      // for safety
        assert(countFound == 2 || countFound == 0);     // it could be already deleted by removeFromEdgeLists().

        /// HINT: removeEdge() have to be called before dn clearance.
        /// because we check on polygon size < 3 there is a dn->next->next, because no childs inserted.
        /// And if there are childs, we got the dn->prev with next->next also.
        /// m_size > 3, because we delete one node here, and than we will have size == 2, which ends up
        /// in only one edge, per definition!
        Node * pointNext = const_cast<Node*>(nextChild(dn));
        if (!isWithoutNewEdge && pointPrev != nullptr && dn->next != dn->prev
                // HINT: it is necessary to check on next->next->next for m_size >2, because the element is not erased already. See test15
                // HINT: it is necessary to check on childNode, because next->next could contain also m_size > 3
                && pointPrev != nextChild(pointNext) // because test59 test15 - m_size is not correct := && m_size > 2
            )
        {       // insert edge not for way with two points later.
            m_origin->insertToEdgeLists(const_cast<Node*>(pointPrev), pointNext);      // TODO [seboeh] check, if ->next could be enough - no nextChild(). Because of correct insert into tree in insertChild*()
        }
    }

    /** @brief does not really delete, because the node* are used in other ways.
     * This method is used really rare. Use eraseNode() instead.
     * iter is a copy, because it will be increased and then returned. TODO [seboeh] do we need the return?
    */
    iterator erase(iterator iter, int stateWithoutNewEdge = 0) {
        if (&iter == nullptr) return iter;      // list is empty
        // --m_size;  outdated, because use size only increasing!
        if (!(&iter)->isDeleted)
        {
            //removeEdge(&iter, stateWithoutNewEdge == 1);
            /// save child tree.
            if ((&iter)->child != nullptr)
            {
                auto prevN = (&iter)->prev;
                if (prevN->child == nullptr)
                {
                    debugCheckPointerWriteAccessSetNodeChild(prevN, (&iter)->child->node);
                    prevN->child = (&iter)->child;
                } else {        // include
                    auto nextN = prevN->child;
                    while (nextN->next != nullptr)
                        nextN = nextN->next;
                    nextN->next = (&iter)->child;
                }
                (&iter)->child = nullptr;
            }
            /// This line is not necesary : --m_size;  because, it is only growing polygon.
            /// We need the size only for estimation of the first forms of polygon.
            if ((&iter)->next)
                (&iter)->next->isChildNode = false;
            debugCheckPointerWriteAccessNext((&iter)->prev->next , (&iter)->next);
            if ((&iter)->prev)
                (&iter)->prev->isChildNode = false;
            debugCheckPointerWriteAccess((&iter)->next->prev , (&iter)->prev);
            assert((&iter)->prev->prev != nullptr);
        }

        /// process for both way1 and way2 -> call erase on way1 and way2.
        if (m_start == iter) m_start = iterator((&iter)->next);
        (&iter)->isDeleted = true;      // MAKR2
        if (m_start == iter) {                 // last element
            m_start = iterator(nullptr);
            m_back = iterator(nullptr);
            // delete &iter;
            return m_start;
        } else {
            if (m_back == iter) m_back = iterator((&iter)->prev);
            // delete &iter;
            return ++iter;
        }
    }

    NodeChild * eraseNode(Node* dn, int stateWithoutNewEdge = 0)
    {
        if (dn->isDeleted)
            return nullptr;     // already deleted.
        if (dn->next == nullptr)
            return nullptr;     // already deleted.

        // --m_size;  outdated, because use size only increasing!  see test59 test15 test58
        if (dn->isChildNode)
        {
            /// 1. delete edges from BSPEdgeTree.
            // Polygon will be bigger than 2 points, if removing a NodeChild.
            // Therefore, we can call removeEdge() on top here.
            // HINT: --m_size; is not allowed, because we often erase and insert again. We expect a growing polygon.
            // size like way1=2 and way2=2 will lead to misinterpretation - see test10-2.png.
            //removeEdge(dn, stateWithoutNewEdge == 1);

            /// 2. insert dn->child->prev.
            NodeChild * child = nullptr;
            NodeChild * * currentChildPP = dn->child->parent;
            if (currentChildPP == nullptr)
            {
                std::cerr << "Warning: could not be childNode and no child->parent is set. The node is not inserted yet. So it is not possible to delete. Perhaps, deleted already. We do nothing." << std::endl;
                assert(dn->child != nullptr);       // earseNode() is always called for node with child set.
                dn->child->next = nullptr;
                dn->child->prev = nullptr;
                dn->child->parent = nullptr;
                dn->isDeleted = true;       // MAKR2
                // dn->next = nullptr;      - MARK2
                // dn->prev = nullptr;
                return nullptr;     // point deleted already.
            }
            if (*currentChildPP != nullptr)
            {
                if (*currentChildPP != dn->child)
                {       // not pointing to itself
                    if ((*dn->child->parent)->prev == dn->child)
                    {       // was hanging on prev in relation to parent
                        // TODO [seboeh] del debugCheckPointerWriteAccessSetNodeChild(dn, dn->child->prev);
                        child = (*currentChildPP)->prev = dn->child->prev;
                    } else {
                            // was hanging on next.
                        child = (*currentChildPP)->next = dn->child->prev;
                    }
                    if (child != nullptr)
                    {
                        child->parent = currentChildPP;
                        if (dn->child->next != nullptr)
                        {       // hang it in
                            NodeChild *nextN = child;
                            while (nextN->next != nullptr)
                                nextN = nextN->next;
                            nextN->next = dn->child->next;
                            if (nextN->next != nullptr)
                            {
                                debugCheckPointerWriteAccess(nextN->next->node->prev , nextN->node);
                                debugCheckPointerWriteAccessNext(nextN->node->next , nextN->next->node);
                                dn->child->next->parent = &(nextN);
                            }
                            assert((child->prev->node->next != nullptr && child->prev->node->prev != nullptr)
                                   && (child->prev->node->next->next != nullptr && child->prev->node->prev->prev != nullptr));
                        }
                        /// change the prev,next pointer on childNodes only.
                        debugCheckPointerWriteAccessNext(child->node->next , dn->next);
                    } else {
                        // we can hang directly dn->child->next
                        if ((*dn->child->parent)->prev == dn->child)
                        {

                            child = (*currentChildPP)->prev = dn->child->next;
                        } else {
                            child = (*currentChildPP)->next = dn->child->next;
                        }
                        if (child != nullptr)
                        {
                            child->parent = currentChildPP;
                            debugCheckPointerWriteAccess(child->node->prev , dn->prev);
                        }
                    }
                    // we do not change the *currentChildPP value - parent stay still untouched.
                } else {
                    /// only one child! pointing to itself.
                    child = dn->child->prev;
                    if (child != nullptr)
                    {
                        if (dn->child->next != nullptr)
                        {
                            NodeChild *nextN = child;
                            while (nextN->next != nullptr)
                                nextN = nextN->next;
                            nextN->next = dn->child->next;
                            if (nextN->next != nullptr)
                            {
                                debugCheckPointerWriteAccess(nextN->next->node->prev , nextN->node);
                                debugCheckPointerWriteAccessNext(nextN->node->next , nextN->next->node);
                                nextN->next->parent = &(nextN);
                            }
                        }
                        child->parent = currentChildPP;
                        debugCheckPointerWriteAccessNext(child->node->next , dn->next);
                    } else {
                        // test94
                        // TODO [seb] further include iterations for prev/next branches
                        if (dn->child != nullptr
                                && dn->child->childParent != nullptr)
                        {
                            dn->child->childParent->prev = dn->child->next;
                        }

                        child = dn->child->next;
                        if (child != nullptr)
                        {
                            child->parent = currentChildPP;
                            debugCheckPointerWriteAccess(child->node->prev , dn->prev);       // node pointer - not nodeChild
                        }
                    }
                    *currentChildPP = child;        // update NodeChild pointer from convex-hull-node. Parent is chaned.
                }
            } else {
                /// HINT: dn->prev->child->prev is not taken into account, because we align always to fromNode.
                std::cerr << "Warning: parent was externally changed. It will be ignored." << std::endl;
                assert(dn->child != nullptr);       // earseNode() is always called for node with child set.
                dn->child->next = nullptr;
                dn->child->prev = nullptr;
                dn->child->parent = nullptr;
                dn->isDeleted = true;
                // dn->next = nullptr;      - MARK2
                // dn->prev = nullptr;
                return nullptr;
            }

            assert(child == nullptr || (child->node->next != nullptr && child->node->prev != nullptr) || (child->node->next == nullptr && child->node->prev == nullptr));

            /// 4.9 redirect
            // first delete edge
            // is done above mit removeEdge  - m_origin->removeFromEdgeLists(dn);
            // then redirect!
            debugCheckPointerWriteAccess(dn->next->prev , dn->prev);
            debugCheckPointerWriteAccessNext(dn->prev->next , dn->next);
            // TODO delete remove node from polygon
            // see test54 - dropped, because next,prev pointer are not common.
            // assert((dn->next->next != nullptr && dn->prev->prev != nullptr));

            /// 4.91 determine new size of way
            // necessary for general purpose calculation in mergeWays(). See test44.
            auto iter = begin();
            int count = 0;
            while (!iter.end() && count < 5)
            {
                iter.nextChild();
                ++count;
            }
            m_size = count;     // additional necessary of --m_size above, because the size is not synchronous with elements.

            /// 5. clear child->next and child->prev
            assert(dn->child->prev == nullptr || dn->child->prev != dn->child->prev->prev);
            assert(dn->child->next == nullptr || dn->child->next != dn->child->next->next);
            assert(dn->prev == nullptr || dn->prev != dn->prev->prev);
            assert(dn->next == nullptr || dn->next != dn->next->next);

            assert(dn->child != nullptr);
            dn->child->next = nullptr;
            dn->child->prev = nullptr;
            dn->child->parent = nullptr;
            dn->isDeleted = true;       // mark for delete MARK2
            // dn->next = nullptr;      - MARK2  not possible, because we need it in nextChild()
            // dn->prev = nullptr;
            return dn->child;
        } else {       // else - this is an inner node (because only called for inner nodes), which was not aligned already.
            std::cerr << "Error: used eraseNode for not childNode." << std::endl;
            /// HINT: when childNode, don't do: dn->prev->next = dn->next on nodes.
            return nullptr;
        }
    }

    /// HINT: When we flip the direction, we change the convex-hull walk only.
    /// ChildNodes are aligned accordingly. This leads to next/prev pointer is not flipped.
    /// This could get a little-bit confusing, but prev/next is only used in TamarAlgorithm::collectAllCloseInnerNodesWalk()
    void flipDirection() {
        iterator prevI = m_start;
        // to check also on end(), we need to check at start on end, at least one time.
        prevI.end();
        // seperate from following var iter, else we would not reach m_start, when prevI.end() == true.
        --prevI;
        iterator iter = m_start;
        // not necessary, because both directions as edges in tree.        bsp::BspNode * * originBefore = &((&m_start)->prev->origin);
        for (; !iter.end(); ++iter, --prevI)
        {
            // flip child-sub-tree
            if (!prevI.end())
            {
                if ((&prevI)->child != nullptr)
                {
                    flipDirectionNodeChilds((&prevI)->child);
                }
                // swap child root pointer
                std::swap((&iter)->child, (&prevI)->child);
                // parent update
                if ((&iter)->child != nullptr)
                    (&iter)->child->parent = &((&iter)->child);
            }
            // swap of tree
            std::swap((&iter)->next, (&iter)->prev);
        }
        // final flip of NodeChilds, if some are in m_start
        --iter;
        if ((&iter)->child != nullptr)
        {
            flipDirectionNodeChilds((&iter)->child);
            (&iter)->child->parent = &((&iter)->child);
        }
        // HINT: because m_start and m_end are hull-nodes allways,
        // there are no nodeChild's in stack of iterator.
        // So it is enough to correct the back.
        m_back = iterator(&iter);
    }

    // TODO [seboeh] declare as private
    void flipDirectionNodeChilds(NodeChild * child)
    {
        if (child->prev != nullptr)
            flipDirectionNodeChilds(child->prev);
        if (child->next != nullptr)
            flipDirectionNodeChilds(child->next);
        std::swap(child->prev, child->next);
        // necessary for eraseNode()
        std::swap(child->node->prev,child->node->next);
        if (child->prev != nullptr)
            child->prev->parent = &child->prev;
        if (child->next != nullptr)
            child->next->parent = &child->next;
    }

    /**
     * @brief increaseSize - used for special purpose. Necessary to avoid incorrect count of points when two sets are merged.
     * @param inc the size of increase - typically 2 - for two edges at convex-hull merge.
     */
    void increaseSize(size_t inc) {
        m_size += inc;
    }

    size_t getCountOfConvexHullEdges() { return m_countOfConvexHullEdges; }

    void calculateCountOfConvexHullEdges() {
        m_countOfConvexHullEdges = 0;
        for (iterator iter=begin(); !iter.end(); ++iter)
            ++m_countOfConvexHullEdges;
    }

private:
    iterator    m_start;
    iterator    m_back;
    size_t      m_size;
    EdgesTreeType *m_edgesTree = nullptr;
    mutable QMutex m_edgesMutex;
    size_t      m_countOfConvexHullEdges=0;
    bsp::BspNode *m_origin = nullptr;
};

}
