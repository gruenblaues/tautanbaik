#pragma once

#include <memory>
#include <tuple>
#include <stack>
#include <array>
#include <functional>
#include <vector>

#include "TspAlgoCommon.h"
#include "TamarAlgorithmHelper.h"
#include "dataStructures/OlavList.h"
#include "dataStructures/commonstructures.h"
#include "bspTreeVersion/BspTreeSarah.h"

namespace TSP {

    class HierarchicalClusteredAlignment : public TspAlgoCommon
    {
    public:
        using Node = comdat::Node;
        using NodeL = std::list<std::shared_ptr<comdat::Node>>;
        using PointL = std::list<TSP::Point>;
        using Way = oko::OlavList;
        using NodeSl = std::list<Node*>;

        struct no_op_delete
        {
            void operator()(void*) { }
        };

        enum class MergeCallState {
            leftRight, topDown
        };
        enum class DirectAccessState {
            isDirectAccess, isNoDirectAccess
        };
        typedef oko::ClosestEdges ClosestEdges;
        typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;
        struct EdgeIter {
            EdgeIter(Way::iterator& aNodeIter, Way::iterator& bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
            Way::iterator aNodeI;
            Way::iterator bNodeI;
        };

        /// stuff for HierarchicalClusteredAlignment
        struct AlignmentTreeNode {
            PointL verts;
            std::shared_ptr<Node> root;        // mean of the childs
            std::vector<std::shared_ptr<Node>> childs;
            int state = 0;      // 1 = isAligned
        };



        /// necessary standard members
        using WayL = std::list<Node*>;
        WayL m_currentWay;
        WayL m_bestWay;
        double m_currentLength = std::numeric_limits<double>::max();
        double m_bestLength = std::numeric_limits<double>::max();

        std::list<Node*> m_edgesGuarantied;

        struct CeList {
            using CeListType = std::list<ClosestEdges>;
            using CeListOrderType = std::map<double, Node*>;
            CeListOrderType ceListOrder;
            CeListType ceList;
        };

        using DirectAlignments = std::map<double, ClosestEdges>;

        bool m_isLogCollisionDetected = false;
        bool m_isLogFaultDetected = false;
        bool m_isLogOverflowDetected = false;

        // HierarchicalClusteredAlignment
        class BreadthSplittingNode {
        public:
            BreadthSplittingNode() = default;
            BreadthSplittingNode(const BreadthSplittingNode&) = default;
            BreadthSplittingNode(Node* val) : point{ val } {}

            Node* point = nullptr;
            std::list<BreadthSplittingNode*> childs;
            BreadthSplittingNode* parent = nullptr;

            // at aligning: run breadth-search to leaf, and go backtracking with alignment.
            std::list<Node*> relevantPointsBackTracking;
            std::list<BreadthSplittingNode*> relevantNodesBreadthSplitting;

            Node* hullFrom = nullptr;

            BreadthSplittingNode* prev = nullptr;
            BreadthSplittingNode* next = nullptr;

            BreadthSplittingNode* responsibleFor = nullptr;

            bool isInserted = false;
            bool isBacktrackingPassed = false;

            std::list<BreadthSplittingNode*> alternatives;
        };

        using BSTNode = BreadthSplittingNode;

        using ClosestTriangleListType = std::list<std::tuple<double, BSTNode*, BSTNode*>>;

        BSTNode* _bstRoot;
        using wayV = std::vector<std::shared_ptr<WayL>>;
        WayL _way;
        WayL _wayCommon;

        // innerOrder
        int _innerOrderVariationCounter = 0;
        std::list<int>  _innerOrderVariation;
        std::list<std::string> _aspects;

        // workaround
        using LinkBST2WayNodeTuple = std::tuple<double, WayL::iterator, BSTNode*>;      // order, pos in WayL, value in breadthList

        /** globals **/
        size_t m_tspSize = 0;
        static size_t tspSize;
        static double tspWidth;
        static double tspHeight;

        /** Ctor **/
        HierarchicalClusteredAlignment();

        using NearestType = std::list<std::tuple<double, Node*>>;
        //using NearestBSTType = std::array<std::tuple<BSTNode*, NearestType>, 3 >;
        using NearestBSTType = std::list<std::tuple<BSTNode*, NearestType>>;


    protected:
        void breadthTracking_rec_breadthFirst(WayL& way, std::list<HierarchicalClusteredAlignment::LinkBST2WayNodeTuple>& breadthList, size_t level = 0);
        /** Impl **/
        void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType* openPointsBasic);

    private:
        NodeL createInputRepresentation(TspAlgorithm::InputCitiesPointerType* openPointsBasic);
        NodeL determineWay(BSTNode* splittingTree, NodeL& hull, Way* way);
        std::tuple<NodeL, Way*> determineHull(NodeL& input);

        BSTNode* createHierarchyStructure(NodeL& hull, NodeL& innerPoints);
        void breadthTracking(WayL& way, BSTNode* root, size_t level = 0);
        void breadthTracking_rec(WayL& way, std::list<HierarchicalClusteredAlignment::LinkBST2WayNodeTuple> & breadthList, size_t level = 0);


        void _triangleRoofCorrection(std::list<EdgeIter>& directAccessEdges, Way::iterator& hpaI, Way::iterator& lpaI, Way::iterator& hpbI, Way::iterator& lpbI);
        Way* _newConqueredWay(bsp::BspNode* node);
        bsp::BspTreeSarah _createBSPTree(NodeL& input);
        void _mergeHull(Way*& way1, const TSP::Point& pWay1Center, Way*& way2, const TSP::Point& pWay2Center, const MergeCallState& callState, const TSP::Point& pCenter);
        bool _isInsideTriangle(const Point& p1, const Point& p2, const Point& p3, const Point& inside);
        DirectAccessState _isDirectAccess(const TSP::Point& pMeanA, Way::iterator& pAI, const TSP::Point& pMeanB, Way::iterator& pBI);
        bool _isNormalVectorLeft(const Point& from, const Point& to, const Point& insert);
        double _calcAreaOfTriangle(const Point& p1, const Point& p2, const Point& p3);
        double _area(const PointL& polygon);
        bool _isCovered(Way* way, Way::iterator& w1I, Way::iterator& fixI);
        void _createTriangle3x1(Way* way1, Way::iterator& insideI, Way::iterator& fixI);
        bool _createTriangle2x2(Way* way1, Way* way2, Way::iterator& w1Inside, Way::iterator& w1Alt, Way::iterator& way2I, Way::iterator& way2NI);
        void _flipWay(Way* way, TSP::Point pCenter);
        bool _isClockwise(TSP::Point pCenter, Way* way);
        bool _updateHullOrUpdateSimple(std::shared_ptr<Node> hullNodeA, std::shared_ptr<Node> updateNode, NodeL& hull);
        bool _updateAllowed(std::shared_ptr<Node> closest, std::shared_ptr<Node> node, bool isTestOnClique = true);
        double _triangleAltitudeOnC(Point c1, Point c2, Point perpendicular);
        bool _isRightSide(const Point& p1, const Point& p2);
        double _calcDegree(Node* node, Node* nodeTested);
        double _calcDegree(Point& node, Point& nodeTested);
        double _correlation(Point a, Point b);
        bool _checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);
        bool _checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);
        float sign(const Point& p1, const Point& p2, const Point& p3);
        template<typename T, typename V>
        std::pair<typename T::iterator, bool> _insertToMap(double dist, T& mapContainer, V&& value);
        CeList::CeListType::iterator _insertToCeList(CeList& ceList, Node* rootFrom, Node* rootTo, Node* insert);
        CeList::CeListType::iterator __insertToCeListAlongList(CeList& ceList, Node* rootFrom, Node* rootTo, Node* insert);
        TSP::Point _calcPerpendicularPoint(Node* from, Node* to, Node* normal);
        bool _isNotInsideTriangle(Node* point1, Node* point2, Node* point3, std::list<Node*>& pointsInside);
        Point _correctNodeCenter(Way* wayBiggerVals, Way* WayLmallerVals, bsp::BspNode* node, MergeCallState state);
        bool _isLeftSideExEqual(const Point& p1Line, const Point& p2Line, const Point& check);

        // HierarchicalClusteredAlignment
        BSTNode* _createHierachicalStructureRec(std::list<BSTNode*>& hull, std::list<BSTNode*>& innerPoints);
        std::list<std::list<BSTNode*> > _getClosestNPointsSet(std::list<BSTNode*>& input, size_t n, std::function<Node * (const BSTNode*&)> getValueFnc);
        std::tuple<Node*, Node*> _findBestAlignment(NodeL& hull, const Node* point);
        std::tuple<double, WayL::iterator> _findBestAlignment(WayL& way, const Node* point);
        void _findBestAlignmentForChild(std::map<double, std::tuple<Node*, BSTNode*> >& res, Way& way, BSTNode* child);
        void _insertAtBestAlignment(WayL& way, std::list<std::tuple<double, Node*, BSTNode*> >& bestAlignment);
        WayL::iterator _nextNodeWayL(WayL& way, WayL::iterator wayI);
        void _mergeLists(WayL& out, WayL& second);
        std::tuple<WayL::const_iterator, WayL::const_iterator> _mergeListCollision(WayL& out, WayL& secondL, const WayL::const_iterator& outI, WayL::const_iterator& secI, WayL::const_iterator& failedI, const WayL::const_iterator& foundI);
        std::tuple<double, WayL::iterator> _findBestAlignmentWithoutLast(WayL& way, const Node* point);
        BSTNode* _createNewBstNode(std::list<BSTNode*>&& closestList, TSP::Point mean);
        std::tuple<double, WayL::iterator> _findNextBestAlignment(WayL& way, WayL::iterator begin, const Node* point);
        std::list<std::tuple<double, WayL::iterator> > _findAllAlignments(WayL& way, const Node* point);
        bool _endInnerOrderOnOneLevel(BSTNode* root);
        /// @restriction maximal 64 points in points!
        std::list<LinkBST2WayNodeTuple> _insertAtBestAlignmentAllVariations(WayL& way, std::list<BSTNode*>& points);
        std::list<int> _getInnerOrderOnOneLevel(BSTNode* root);
        void _nextInnerOrderOnOneLevel(BSTNode* root);
        std::list<std::tuple<double, Node*, BSTNode*> > _checkAlignment(WayL& way, BSTNode* root);
        std::tuple<double, WayL::iterator> _checkBestAlignment(WayL& way, BSTNode* root, std::list<std::tuple<double, WayL::iterator> >::iterator alignI, std::list<BSTNode*>::iterator leafChildI);
        void _updateResponsibility(BSTNode* node);
        template<typename F>
        std::list<std::tuple<double, WayL::iterator> > _findAllAlignments_if(WayL& way, const Node* point, F f);
        void _fillParents(BSTNode* parent);
        bool _isNodeClosest(BSTNode* n1, BSTNode* n2, BSTNode* n3);
        void _setAndClearClosestsTriangle(std::list<std::list<BSTNode*> >& ret, ClosestTriangleListType& clostests, ClosestTriangleListType::iterator& cloI, ClosestTriangleListType::iterator& cloBeforeI, size_t cloITupleN);
        void _linkParents(NodeL& hull, BSTNode* root);
        void _compareLink(Node* from1, NearestBSTType::value_type& link1, NearestBSTType& link, NodeL& hull);
        void _createLink(NodeL& hull, NearestBSTType::value_type& link1, NearestBSTType::value_type& link2, Node* from1, Node* from2);
        void _connectNode2ToNode1(BSTNode* node2, BSTNode* node1);
    };

}
