#include "BspNode.h"
#include <stdio.h>

using namespace bsp;

//#define nullptr NULL

bsp::BspNode::NextChildFncType bsp::BspNode::nextChildFnc;
bsp::BspNode::PrevChildFncType bsp::BspNode::prevChildFnc;

namespace bsp {
    const double factSeboehDistToEuclidean = 1.0/(2.0-std::sqrt(2.0));
}


BspNode::BspNode()
{
    m_upperLeft = nullptr;
    m_upperRight = nullptr;
    m_lowerLeft = nullptr;
    m_lowerRight = nullptr;
    m_value = nullptr;
}

BspNode::~BspNode()
{
    m_newlyAdded.clear();
    for (auto e : m_edgesLowerLeft)
        delete e;
    m_edgesLowerLeft.clear();
    for (auto e : m_edgesLowerRight)
        delete e;
    m_edgesLowerRight.clear();
    for (auto e : m_edgesUpperLeft)
        delete e;
    m_edgesUpperLeft.clear();
    for (auto e : m_edgesUpperRight)
        delete e;
    m_edgesUpperRight.clear();

    if (m_upperLeft != nullptr)
    {
        delete m_upperLeft;
        m_upperLeft = nullptr;
    }
    if (m_upperRight != nullptr)
    {
        delete m_upperRight;
        m_upperRight = nullptr;
    }
    if (m_lowerLeft != nullptr)
    {
        delete m_lowerLeft;
        m_lowerLeft = nullptr;
    }
    if (m_lowerRight != nullptr)
    {
        delete m_lowerRight;
        m_lowerRight = nullptr;
    }
    // dont delete m_value - its a reference and not here responsible.
}


// //////////////////////////////////////////////////////////////
// Death First algorithm
// //////////////////////////////////////////////////////////////

void BspNode::_selectClosestEdgeFromSpace(bsp::Edge* &closestEdge, double &reachedDist, std::list<const bsp::Edge*> &hullEdgeList, const TSP::Point &point)
{
    if (hullEdgeList.empty())
        return;

    for (const auto &edge : hullEdgeList)
    {
        if (edge->startPoint->point == point || edge->endPoint->point == point)
            continue;       // ignore edge with same point.
        double distAlignment = seboehDist(edge->startPoint->point, edge->endPoint->point, point);
        m_bestFoundEdges.insert( BestFoundEdgesType::value_type(distAlignment, edge) );
        if ((closestEdge != nullptr && distAlignment < closestEdge->dist)
            || (closestEdge == nullptr)
           )
        {
            closestEdge = const_cast<bsp::Edge*>(edge);
            closestEdge->dist = distAlignment;
            TSP::Point distToOrigin = point - m_center;
            // HINT: due to search for alignment not distance, we have to multiply the distance by two, because two edges in triangle.
            // See SortedMerge::_calcBorderVariations()
            reachedDist = 2*(std::max)(distToOrigin.x(), distToOrigin.y());
        }
    }

    // we delete the best three iterators. Therefore a map, for sorting.
    if (m_bestFoundEdges.size() > closestEdgesBestCount)
    {
        // HINT improve instead map not possible. Because it could find the second best node.
        // And then, the strategy 'update second and third best' does not work.
        int count = 0;
        for (auto iter = m_bestFoundEdges.begin();
             iter != m_bestFoundEdges.end(); )
        {
            if (count < closestEdgesBestCount)
            {
                ++count;
                ++iter;
            } else {
                iter = m_bestFoundEdges.erase(iter);
            }
        }
    }
}

void BspNode::_findClosestEdgeDepthFirstInHull(bsp::Edge* &resultEdge, double &reachedDist, bsp::BspNode *hullNode, const TSP::Point &point)
{
    if (hullNode == nullptr)
        return;     // empty leaf -> ignore this branch.
    // *hullNode->m_value == point - not necessary, because leaf is always correct single value.
    if (hullNode->m_value != nullptr)
        return;     // leaf is reached, return depth first search. This includes also m_upper/lower/left/right == nullptr

    TSP::Point distToOrigin = point - hullNode->m_center;
    double xdiff = fabs(distToOrigin.x());
    double ydiff = fabs(distToOrigin.y());

    /// go to closest quader to point Depth-First-Search
    /// Hint: points on the coordinate-cross are in the left top quader. Therefore, we check on <=.
    if (distToOrigin.x() <= 0)
    {
        if (distToOrigin.y() <= 0)
        {
            // we have to look in the same quader like point is.
            _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperLeft, point);
            /// stop criteria for pruning: xdiff or ydiff is bigger than reachedDist.
            // we might have a result --> now recursive depth first search with pruning
            // because there is an edge, which is not already ankered in nodes below, we have to check this left upper.
            _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperLeft, point);
            if (xdiff <= reachedDist)        // <=, because edge could be on splittin-plane
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperRight, point);
                if (xdiff <= reachedDist)       // second check, because reachedDist changed in findClo...
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperRight, point);
                }
            }
            if (xdiff <= reachedDist && ydiff <= reachedDist)
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerRight, point);
                if (xdiff <= reachedDist && ydiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerRight, point);
                }
            }
            if (ydiff <= reachedDist)
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerLeft, point);
                if (ydiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerLeft, point);
                }
            }
        } else {
            // point is lower left
            _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerLeft, point);
            _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerLeft, point);
            if (ydiff <= reachedDist)       // look at upper left
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperLeft, point);
                if (ydiff <= reachedDist)       // second check, because reachedDist changed in findClo...
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperLeft, point);
                }
            }
            if (xdiff <= reachedDist && ydiff <= reachedDist)   // look upper right
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperRight, point);
                if (xdiff <= reachedDist && ydiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperRight, point);
                }
            }
            if (xdiff <= reachedDist)       // look lower right
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerRight, point);
                if (xdiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerRight, point);
                }
            }
        }
    } else {
        if (distToOrigin.y() <= 0)
        {       // point is upper right
            _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperRight, point);
            _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperRight, point);
            if (ydiff <= reachedDist)       // look at lower right
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerRight, point);
                if (ydiff <= reachedDist)       // second check, because reachedDist changed in findClo...
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerRight, point);
                }
            }
            if (xdiff <= reachedDist && ydiff <= reachedDist)   // look lower left
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerLeft, point);
                if (xdiff <= reachedDist && ydiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerLeft, point);
                }
            }
            if (xdiff <= reachedDist)       // look upper left
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperLeft, point);
                if (xdiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperLeft, point);
                }
            }
        } else {        // point is lower right
            _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerRight, point);
            _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerRight, point);
            if (xdiff <= reachedDist)       // look at lower left
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_lowerLeft, point);
                if (xdiff <= reachedDist)       // second check, because reachedDist changed in findClo...
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesLowerLeft, point);
                }
            }
            if (xdiff <= reachedDist && ydiff <= reachedDist)   // look upper left
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperLeft, point);
                if (xdiff <= reachedDist && ydiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperLeft, point);
                }
            }
            if (ydiff <= reachedDist)       // look upper right
            {
                _findClosestEdgeDepthFirstInHull(resultEdge, reachedDist, hullNode->m_upperRight, point);
                if (ydiff <= reachedDist)
                {
                    _selectClosestEdgeFromSpace(resultEdge, reachedDist, hullNode->m_edgesUpperRight, point);
                }
            }
        }
    }

}       // end findClosestEdgeDepthFirstInHull

std::tuple<bsp::Edge*, double> BspNode::selectFirstClosestEdge(const TSP::Point &point)
{
    /// search closest edge to point
    m_bestFoundEdges.clear();

    bsp::Edge * closestEdge = nullptr;
    double reachedDist = std::numeric_limits<double>::max();

    /// First search on BSPEdge-Tree, because we need a 'deep' edge close to the point.
    /// HINT: reachedDist < double_max, would cause not to look on the deepest origin.
    _findClosestEdgeDepthFirstInHull(closestEdge, reachedDist, this, point);

    // TODO [seboeh] flip findClosestEdgeDepthFirstInHull() with selectClosestEdgeFromSpace(),
    // because already found edges in highest hull will help to find other edges faster.
    _selectClosestEdgeFromSpace(closestEdge, reachedDist, m_edgesUpperLeft, point);
    _selectClosestEdgeFromSpace(closestEdge, reachedDist, m_edgesUpperRight, point);
    _selectClosestEdgeFromSpace(closestEdge, reachedDist, m_edgesLowerLeft, point);
    _selectClosestEdgeFromSpace(closestEdge, reachedDist, m_edgesLowerRight, point);

    return std::tuple<bsp::Edge*, double>(closestEdge, reachedDist);
}


// //////////////////////////////////////////////////////////////
// Death First algorithm END
// //////////////////////////////////////////////////////////////
