#include "BspTreeSarah.h"
#include <stdio.h>

using namespace bsp;

#define nullptr NULL

BspTreeSarah::BspTreeSarah()
    : m_root(nullptr)
{}

BspTreeSarah::~BspTreeSarah()
{
//    delete m_root;      // deletes the branches recursively.
}

void BspTreeSarah::buildTree(std::list<TSP::Point*> *points)
{
    m_root = newTreeNode(points, 1);
    m_root->m_level = 1;
}

TSP::Point BspTreeSarah::calculateCenter(CornerPointsType* points)
{
    float x = 0;
    float y = 0;
    for (std::list<TSP::Point*>::iterator pointIter = points->begin();
         pointIter != points->end(); ++pointIter) {
        x += (*pointIter)->x();
        y += (*pointIter)->y();
    }
    unsigned int count = points->size();
    x /= count;
    y /= count;
    return TSP::Point(x,y);
}

BspNode* BspTreeSarah::newTreeNode(CornerPointsType* _points, size_t level)
{
    if (_points->size() == 0) {
        return nullptr;
    }

    // due to test96 - there is an unknown bug
    std::map<double, double> reduceSamePointsMap;
    auto iter = reduceSamePointsMap.end();
    CornerPointsType *points = new CornerPointsType();
    for (CornerPointsType::iterator pointI=_points->begin();
         pointI != _points->end(); ++pointI)
    {
        if ((iter = reduceSamePointsMap.find((*pointI)->x())) != reduceSamePointsMap.end())
        {
            if (iter->second == (*pointI)->y())
                continue;
        }
        points->push_back(*pointI);
        reduceSamePointsMap.insert({(*pointI)->x(), (*pointI)->y()});
    }

    ++level;
    BspNode* node = new BspNode();
    if (points->size() == 1) {
        node->m_value = points->front();
        node->m_center = *node->m_value;
        return node;
    } else {
        node->m_value = nullptr;
        node->m_center = calculateCenter(points);
    }
    std::list<TSP::Point*> *newCornerPoints = newUpperLeft(points, &node->m_center);
    node->m_upperLeft = newTreeNode(newCornerPoints, level);
    if (node->m_upperLeft)
        node->m_upperLeft->m_level = level;
    newCornerPoints->clear();
//    delete newCornerPoints;
    newCornerPoints = newUpperRight(points, &node->m_center);
    node->m_upperRight = newTreeNode(newCornerPoints, level);
    if (node->m_upperRight)
        node->m_upperRight->m_level = level;
    newCornerPoints->clear();
//    delete newCornerPoints;
    newCornerPoints = newLowerRight(points, &node->m_center);
    node->m_lowerRight = newTreeNode(newCornerPoints, level);
    if (node->m_lowerRight)
        node->m_lowerRight->m_level = level;
    newCornerPoints->clear();
//    delete newCornerPoints;
    newCornerPoints = newLowerLeft(points, &node->m_center);
    node->m_lowerLeft = newTreeNode(newCornerPoints, level);
    if (node->m_lowerLeft)
        node->m_lowerLeft->m_level = level;
    newCornerPoints->clear();
//    delete newCornerPoints;

//    delete points;
    ++m_countOfNodes;
    return node;
}


BspTreeSarah::CornerPointsType *BspTreeSarah::newUpperLeft(CornerPointsType *points, TSP::Point* center)
{
    auto newPoints = new CornerPointsType();
    for(std::list<TSP::Point*>::const_iterator pointI= (*points).begin();
        pointI != points->end(); ++pointI)
    {
        if ((*pointI)->x() <= center->x()
                && (*pointI)->y() <= center->y())
        {
            newPoints->push_back(*pointI);
        }
    }
    return newPoints;
}

BspTreeSarah::CornerPointsType *BspTreeSarah::newUpperRight(CornerPointsType *points, TSP::Point* center)
{
    auto newPoints = new CornerPointsType();
    for(std::list<TSP::Point*>::const_iterator pointI= (*points).begin();
        pointI != points->end(); ++pointI)
    {
        if ((*pointI)->x() > center->x()
                && (*pointI)->y() <= center->y())
        {
            newPoints->push_back(*pointI);
        }
    }
    return newPoints;
}

BspTreeSarah::CornerPointsType *BspTreeSarah::newLowerLeft(CornerPointsType *points, TSP::Point* center)
{
    auto newPoints = new CornerPointsType();
    for(std::list<TSP::Point*>::const_iterator pointI= (*points).begin();
        pointI != points->end(); ++pointI)
    {
        if ((*pointI)->x() <= center->x()
                && (*pointI)->y() > center->y())
        {
            newPoints->push_back(*pointI);
        }
    }
    return newPoints;
}

BspTreeSarah::CornerPointsType *BspTreeSarah::newLowerRight(CornerPointsType *points, TSP::Point* center)
{
    auto newPoints = new CornerPointsType();
    for(std::list<TSP::Point*>::const_iterator pointI= (*points).begin();
        pointI != points->end(); ++pointI)
    {
        if ((*pointI)->x() > center->x()
                && (*pointI)->y() > center->y())
        {
            newPoints->push_back(*pointI);
        }
    }
    return newPoints;
}

