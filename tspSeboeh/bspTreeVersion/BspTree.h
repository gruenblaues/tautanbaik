#ifndef BSPTREE_H
#define BSPTREE_H

#include <list>

#include "Point.h"
#include "BspNode.h"

namespace bsp {

class BspTree
{
public:
    BspTree() {}

    virtual void buildTree(std::list<TSP::Point*> *)=0;
    virtual BspNode* root() { return m_root; }
    virtual std::list<TSP::Point*> * newSortedList()=0;

protected:
    typedef std::list<TSP::Point*>* CornerPointsType;
    typedef TSP::Point*             CpType;

    virtual BspNode* newTreeNode(CornerPointsType points)=0;

    virtual TSP::Point calculateCenter(CornerPointsType points)=0;

    virtual CornerPointsType newUpperLeft(CornerPointsType points, TSP::Point* center)=0;
    virtual CornerPointsType newUpperRight(CornerPointsType points, TSP::Point* center)=0;
    virtual CornerPointsType newLowerLeft(CornerPointsType points, TSP::Point* center)=0;
    virtual CornerPointsType newLowerRight(CornerPointsType points, TSP::Point* center)=0;

    virtual void sortedListSearch(CornerPointsType sortedList, BspNode* node, const TSP::Point &oldCenter)=0;

protected:
    BspNode* m_root = nullptr;
};

}

#endif // BSPTREE_H
