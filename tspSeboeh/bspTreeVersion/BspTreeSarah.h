#ifndef BSPTREESARAH_H
#define BSPTREESARAH_H

#include <list>

#include "Point.h"
#include "BspNode.h"

namespace bsp {

class BspTreeSarah
{
public:
    typedef std::list<TSP::Point*> CornerPointsType;

    BspTreeSarah();
    ~BspTreeSarah();

    virtual void buildTree(std::list<TSP::Point*> *);
    virtual BspNode* root() const { return m_root; }
    float getCountOfNodes() const { return m_countOfNodes; }

protected:
    virtual BspNode* newTreeNode(CornerPointsType *_points, size_t level);
    virtual TSP::Point calculateCenter(CornerPointsType *points);

    virtual CornerPointsType* newUpperLeft(CornerPointsType *points, TSP::Point* center);
    virtual CornerPointsType* newUpperRight(CornerPointsType *points, TSP::Point* center);
    virtual CornerPointsType* newLowerLeft(CornerPointsType *points, TSP::Point* center);
    virtual CornerPointsType* newLowerRight(CornerPointsType *points, TSP::Point* center);

protected:
    BspNode* m_root = nullptr;
    float m_countOfNodes=0;
};

}

#endif // BSPTREESARAH_H
