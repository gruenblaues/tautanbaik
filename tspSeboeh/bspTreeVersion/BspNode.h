#ifndef BSPNODE_H
#define BSPNODE_H

#include <list>
#include <cassert>
#include <memory>
#include <algorithm>
#include <functional>
#include <map>
#include <fstream>
#include <iostream>
#include <stack>

#include "Point.h"

// set xPrev and xNext to 0 to omit Debug Output 'bspdebug.output.txt'
#define xPrev 510
#define xNext 510

// define key in bspdebug.output.txt
#define BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT 419970894839184
#define BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT2 419970894838672
#define BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT3 422210049195408
#define BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT4 422210049194896
#define BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT5 422210049225648
#define BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT6 422210049179104

namespace oko {
    struct Node;
}
namespace bsp {
    class BspNode;
}
struct BspDebugEntry
{
    const bsp::BspNode *origin = nullptr;
    const oko::Node *start = nullptr;
    const oko::Node *end = nullptr;
    int count = 0;
    enum class EntryType { NONE, INSERT, REMOVE };
    EntryType type = EntryType::NONE;
    long getKey()
    {
        return reinterpret_cast<long>(origin) + reinterpret_cast<long>(start) + reinterpret_cast<long>(end);
    }
};

// first := address_origin + address_start + address_end. - Use getKey()
typedef std::map<long, BspDebugEntry> BspDebugMapType;
static BspDebugMapType bspDebugMap;
// type, start+end, iter
typedef std::list<std::tuple<int, const oko::Node*, BspDebugMapType::iterator>> BspDebugMapEntryListType;
static BspDebugMapEntryListType bspDebugMapEntryList;


namespace bsp {
  class BspNode;
}

namespace oko {

struct Node;

struct ClosestEdges {
    ClosestEdges() {}
    ClosestEdges(const ClosestEdges &) = default;
    ClosestEdges(Node * aInsertNode) : ClosestEdges() { insertNode = aInsertNode; }
//        ClosestEdges(Node* afromNode, Node* atoNode, double adist, Node* ainsertNode) :
//            fromNode(afromNode), toNode(atoNode), dist(adist), insertNode(ainsertNode) {}
    Node* fromNode=nullptr;     // FIXME: we could omit fromNode and toNode, because only the order of points in the lists is relevant.
    Node* toNode=nullptr;       // FIXME: we need only one parent fromNode pointer in OlavList::Node
    double dist=std::numeric_limits<double>::max();     ///< is the seboeh dist between (fromNode, toNode and insertNode).
    Node* insertNode=nullptr;

    bool operator==(const ClosestEdges &rhs)        // used for remove in selectRecursive. See test23.png.
    {
        return rhs.insertNode == insertNode;
    }
};

struct NodeChild;

class Cluster;

struct Node {
    Node() {
        if (point.x() == xPrev)
        {
//          breakpointSeboeh();
        }
    }
    // does not use the default copy constructor, because the node have to be integrated again into the polygon.
    Node(const Node &val) : prev(val.prev), next(val.next), point(val.point), child(val.child) {
        assert((prev == nullptr && next == nullptr) || (prev != nullptr && next != nullptr));
        if (point.x() == xPrev)
        {
//            breakpointSeboeh();
        }
        edge1 = val.edge1;
        edge2 = val.edge2;
    }
    Node(const TSP::Point &p) : point(p) {
        if (point.x() == xPrev)
        {
//            breakpointSeboeh();
        }

    }
    Node(const TSP::Point &p, Node * aPrev, Node * aNext) : prev(aPrev), next(aNext), point(p) {
        assert((prev == nullptr && next == nullptr) || (prev->next != nullptr && prev->prev != nullptr && next->next != nullptr && next->prev != nullptr));
        assert((prev == nullptr && next == nullptr) || (prev->next->next != nullptr && prev->prev->prev != nullptr && next->next->next != nullptr && next->prev->prev != nullptr));

        if (point.x() == xPrev)
//            breakpointSeboeh();
            ;
    }
    ~Node()
    {
        bspDebugMapEntryList.push_back(BspDebugMapEntryListType::value_type(2, this, bspDebugMap.end()));
    }

    Node * prev=reinterpret_cast<Node*>(0x1);       // 0x1 for debug.
    Node * next=reinterpret_cast<Node*>(0x1);
    TSP::Point point;
    // nodeChild is never deleted. This is because, it creates the tree for innerNodes.
    NodeChild * child=nullptr;              ///< represents starting pointer to childs hanging on this node.
    bool isChildNode=false;
    bool isOuterPolyonEdge=false;
    bsp::BspNode *origin=nullptr;
    bool isFixedForSelectRecursive=false;
    bool isWasFixedForSelectRecursive=false;
    Node* hullParent= nullptr;
    bool isDeleted = true;      // because see MARK2
    bool isZipoAligned = false;
    double gravityWeight = 0;
    int debugInfo = 0;
    bool gravityIsUpdated = false;
    enum class CandidateState { NONE, CANDIDATE, ELECTED, NEW_SORTED_MERGE_ELEMENT };
    CandidateState candidateState = CandidateState::ELECTED;
    bool isInserted = false;
    bool isCurrNode = false;
    ClosestEdges closestEdge;
    size_t level = 0;
    int wayNum = 0;     // 0 = not initialized, 1 = A, 2 = B
    TSP::Point hullVector;
    TSP::Point hullVectorSecond;
    TSP::Point hullVectorThird;
    Node *edge1 = nullptr;
    Node *edge2 = nullptr;
    bool isHullNodeProcessed = false;
    bool isConnectedToHull = false;
    bool isInWay = false;
    bool isHull = false;

    double weightNew = 1;
    double weightOld = 1;
    struct NeighPlane {
        Node *neigh;
        double proportionalDistributionFact = 0;
    };
    std::list<NeighPlane> neighbours;
    std::map<double, NeighPlane> neighboursSorted;            // sorted along propDistFact for neighbour of neighbour

    // MinMaxClusterAlignment
    double guarantiedDist = 0;
    Node *guarantiedFrom = nullptr;
    Node *guarantiedTo = nullptr;
    double guarantiedBasicDist = std::numeric_limits<double>::max();
    double guarantiedMaxDist = 0;
    double foundDist = std::numeric_limits<double>::max();

    // DetourClusterAlignment
    std::list<Cluster> *root = nullptr;
    Node *closestPoint = nullptr;
    enum class AlgoState { NONE, RESERVED };
    AlgoState algoState = AlgoState::NONE;

    // Bellmann, Held and Karp
    unsigned long long number = 0;
};


static void breakpointSeboeh(bsp::BspNode *origin, const oko::Node *start, const oko::Node *end, bool isInsertOrRemove)
{
#if xPrev != 0 && not NDEBUG

    BspDebugEntry entry;
    entry.origin = origin;
    entry.start = start;
    entry.end = end;
    if (entry.getKey() == BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT
            || entry.getKey() == BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT2
            || entry.getKey() == BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT3
            || entry.getKey() == BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT4
            || entry.getKey() == BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT5
            || entry.getKey() == BSP_DEBUG_ENTRY_KEY_FOR_BREAKPOINT6)
        int i=5;            // SET THE BREAKPOINT HERE to determine which methods insert/remove the edge of previously found edges in bspdebug.output.txt
    if (isInsertOrRemove)
    {
        auto iter = bspDebugMap.find(entry.getKey());
        if (iter != bspDebugMap.end())
        {       // already inserted.
            if (iter->second.type == BspDebugEntry::EntryType::REMOVE)
            {
                iter->second.type = BspDebugEntry::EntryType::INSERT;
            }
        } else {
            auto res = bspDebugMap.insert( {entry.getKey(), entry} );
            iter = std::get<0>(res);
            auto delNode = (entry.start->point.x() == xPrev) ? entry.start : entry.end;
            bspDebugMapEntryList.push_back(BspDebugMapEntryListType::value_type(0, delNode, iter));
        }
        ++iter->second.count;
    } else {
        auto iter = bspDebugMap.find(entry.getKey());
        if (iter != bspDebugMap.end())
        {
            --iter->second.count;
        } else {
            // TODO [seboeh] improve handling of edges.
//            std::cerr << "\nDebug-Warning: Removed edge, which was not inserted, but delted." << std::endl
//                      << " This Debug-Waring can be ignored. It will only appear in Debug modus." << std::endl
//                      << " start = " << std::addressof(start) << std::endl
//                      << " end = " << std::addressof(end) << std::endl;
        }
    }
#endif
}


}       // end namespace oko

namespace bsp {

class Edge
{
public:
    Edge(oko::Node * const startP, oko::Node * const endP)
    {
        startPoint = startP;
        endPoint = endP;
    }

    Edge() {}

    /// TODO [seboeh] delete - only for debugggggggging
    bool operator==(const Edge &rhs)
    {
        return startPoint == rhs.startPoint && (endPoint == rhs.endPoint || rhs.endPoint == nullptr || endPoint == nullptr);
    }

    oko::Node *startPoint = nullptr;
    oko::Node *endPoint = nullptr;
    double dist = std::numeric_limits<double>::max();
};

class BspNode
{
public:
    TSP::Point      m_center;
    BspNode*        m_upperLeft = nullptr;
    BspNode*        m_upperRight = nullptr;
    BspNode*        m_lowerLeft = nullptr;
    BspNode*        m_lowerRight = nullptr;
    TSP::Point*     m_value = nullptr;
    typedef std::list<const Edge*> BspEdgesType;
    std::list<const Edge*> m_edgesUpperLeft;        // pointer of edge, because used in OlavList at flipDirection(). - update: deprecated - shall we delete this.
    std::list<const Edge*> m_edgesUpperRight;
    std::list<const Edge*> m_edgesLowerLeft;
    std::list<const Edge*> m_edgesLowerRight;
    std::list<const Edge*> m_newlyAdded;            ///< contains only copies of pointers. Not responsible!
    size_t          m_level = 0;                    // used in find relevant fromNodes.

    typedef std::function<const oko::Node* (const oko::Node *)> NextChildFncType;
    static NextChildFncType nextChildFnc;

    typedef std::function<const oko::Node* (const oko::Node *)> PrevChildFncType;
    static PrevChildFncType prevChildFnc;

    typedef std::map<double, const bsp::Edge*> BestFoundEdgesType;

    static const size_t closestEdgesBestCount = 4;

    BspNode();
    ~BspNode();

    std::list<const Edge*>::iterator find(std::list<const Edge*>::iterator begin, std::list<const Edge*>::iterator end, const Edge &val)
    {
        for (auto iter=begin;
             iter != end; ++iter)
        {
            if ((*iter)->startPoint == val.startPoint
                    && ((*iter)->endPoint == val.endPoint
                        || val.endPoint == nullptr
                        || (*iter)->endPoint == nullptr))
                return iter;
        }
        return end;
    }

    /// TODO [seboeh] better design. Don't copy this code from TamarAlorithm and Parameters.h.
    double defaultDistanceFnc(const TSP::Point &a, const TSP::Point &b)
    {
        return sqrt((static_cast<double>(a.x())-b.x())*(static_cast<double>(a.x())-b.x())
                    + (static_cast<double>(a.y())-b.y())*(static_cast<double>(a.y())-b.y()));
    }

    double seboehDist(const TSP::Point &p1, const TSP::Point &p2, const TSP::Point &pNew)
    {
        return defaultDistanceFnc(p2, pNew)
                + defaultDistanceFnc(p1, pNew)
                - defaultDistanceFnc(p1, p2);
    }

    bool hasEdge(oko::Node *fp, oko::Node *sp)
    {
        auto iter = find(m_edgesLowerLeft.begin(), m_edgesLowerLeft.end(), Edge{fp,sp});
        if (iter != m_edgesLowerLeft.end())
            return true;
        iter = find(m_edgesUpperLeft.begin(), m_edgesUpperLeft.end(), Edge{fp,sp});
        if (iter != m_edgesUpperLeft.end())
            return true;
        iter = find(m_edgesUpperRight.begin(), m_edgesUpperRight.end(), Edge{fp,sp});
        if (iter != m_edgesUpperRight.end())
            return true;
        iter = find(m_edgesLowerRight.begin(), m_edgesLowerRight.end(), Edge{fp,sp});
        if (iter != m_edgesLowerRight.end())
            return true;
        return false;
    }

    oko::Node * pullEdgesRemoveFromList(const oko::Node *startPoint)
    {
        if ((startPoint->point.x() == xPrev// && startPoint->point.y() == 250
             )
            ||
                (startPoint->point.x() == xNext// && next->point.y() == 250
        ))
            breakpointSeboeh(this, startPoint, nullptr, false);
        oko::Node *endPoint;
        if ((endPoint = _pullEdgesRemoveFromList(m_edgesLowerLeft, startPoint)) != nullptr)
            return endPoint;
        if ((endPoint = _pullEdgesRemoveFromList(m_edgesLowerRight, startPoint)) != nullptr)
            return endPoint;
        if ((endPoint = _pullEdgesRemoveFromList(m_edgesUpperLeft, startPoint)) != nullptr)
            return endPoint;
        if ((endPoint = _pullEdgesRemoveFromList(m_edgesUpperRight, startPoint)) != nullptr)
            return endPoint;

        /// HINT: TODO [seboeh] perhaps we need a depth-first search instead of _pullHigherOrigin() here.
        return nullptr;
    }

    void insertToEdgeLists(oko::Node *firstP, oko::Node *secondP)
    {
        if ((firstP->point.x() == xPrev// && startPoint->point.y() == 250
                || secondP->point.x() == xNext// && next->point.y() == 190
             )
            ||
                (firstP->point.x() == xNext// && next->point.y() == 250
                        || secondP->point.x() == xPrev// && startPoint->point.y() == 190
        ))
            breakpointSeboeh(this, firstP, secondP, true);
//        assert(firstP->prev != nullptr);        // := valid point in polygon/way.
//        assert(firstP->next != nullptr);
//        assert(secondP->prev != nullptr);
//        assert(secondP->next != nullptr);
        // HINT: ABOVE assert LINE does not work, because insertChild will have the insert to edges before next/prev set.
        // improve by handling difference between polygon inside and innerNode in insertChild / insertNewNodeToEdge.
        // - test12-2 - in conjunction to test107 ignoring this assert failes. - assert(!hasEdge(firstP, secondP));
        assert(firstP && secondP);
        // TODO [seboeh] following assert, because of test 54 and the embrio alignment.
        // assert(!(firstP->point == secondP->point));
        if (!(firstP->point == secondP->point))
            _insertPointToEdgeLists(firstP, secondP);
        firstP->origin = this;
        secondP->origin = this;

        // TODO [seboeh] wrong code position . edge is deleted before it is inserted in this again.
//        if (firstP->origin != this)
//            firstP->origin = _pullHigherOrigin(firstP);      // parent pointer to origin.
//        if (secondP->origin != this)
//            secondP->origin = _pullHigherOrigin(secondP);
        /// HINT: secondP->origin = _pullHigherOrigin(...); setting is not necessary,
        ///  because we look only on startPoint->origin to find the correct edge.
    }

    void removeFromEdgeLists(const oko::Node * startPoint, const oko::Node * endPoint)
    {
        if ((startPoint->point.x() == xPrev// && startPoint->point.y() == 250
                || endPoint->point.x() == xNext// && next->point.y() == 190
             )
            ||
                (startPoint->point.x() == xNext// && next->point.y() == 250
                        || endPoint->point.x() == xPrev// && startPoint->point.y() == 190
        ))
            breakpointSeboeh(this, startPoint, endPoint, false);
        if (endPoint == nullptr || startPoint == nullptr)
            return;     // startpoint not in polygon.
        if (startPoint->isDeleted)
            return;     // points are deleted before already
        if (_removeEdgeFromQuader(startPoint, endPoint) < 2)
            /// second time of _remove..., because endPoint can be in another list to startPoint compaired.
            _removeEdgeFromQuader(endPoint, startPoint);
    }

    /**
     * @brief removeFromEdgeLists, removes next edge and prev edge.
     * @param firstPoint
     * @param secondPoint
     */
    int removeFromEdgeLists(const oko::Node * startPoint)
    {
        if (startPoint == nullptr)
            return 0;
        if (startPoint->isDeleted)
            return 0;     // points are deleted before already
        /// FIXME: Is it possible to distinguish between upperLeft,lowerLeft ...
        const oko::Node *next = nextChildFnc(startPoint);
        if (next == nullptr)
            return 0;     // startpoint not in polygon.
        if ((startPoint->point.x() == xPrev// && startPoint->point.y() == 250
                || next->point.x() == xNext// && next->point.y() == 190
             )
            ||
                (startPoint->point.x() == xPrev// && next->point.y() == 250
                        || next->point.x() == xNext// && startPoint->point.y() == 190
        ))
            breakpointSeboeh(this, startPoint, next, false);
        int countFound = _removeFromEdgeListsTwice(next, startPoint);

// comment out, because test44. missing edges.
//        const oko::Node *prev = prevChildFnc(startPoint);
//        if (prev == nullptr)
//            return 0;
//        countFound += _removeFromEdgeListsTwice(prev, startPoint);

        return countFound;
    }

    const Edge* searchClosestLastInserted(const oko::Node *node, double &bestDist)
    {
        const Edge *bestEdge = nullptr;
        for (auto iter=m_newlyAdded.begin();
             iter != m_newlyAdded.end(); ++iter)
        {
            if ((*iter)->startPoint == node || (*iter)->endPoint == node)
                continue;
            double dist = seboehDist((*iter)->startPoint->point, (*iter)->endPoint->point, node->point);
            if (dist < bestDist)
            {
                bestEdge = *iter;
                bestDist = dist;
            }
        }
        if (bestEdge != nullptr && nextChildFnc(bestEdge->startPoint) != bestEdge->endPoint)
        {
            std::swap(const_cast<Edge*>(bestEdge)->startPoint, const_cast<Edge*>(bestEdge)->endPoint);
        }
        return bestEdge;
    }

    void clearLastInserted()
    {
        m_newlyAdded.clear();
    }

    std::tuple<bsp::Edge *, double> selectFirstClosestEdge(const TSP::Point &point);

    BestFoundEdgesType getBestFoundEdges() { return m_bestFoundEdges; }

    void merge(BspNode *origin)
    {
        if (origin == nullptr)
            return;

        // according to center
        double xdiff = origin->m_center.x() - m_center.x();
        double ydiff = origin->m_center.y() - m_center.y();

        if (xdiff < 0)
        {
            if (ydiff < 0)
            {       // upper left
                if (m_upperLeft == nullptr)
                {
                    m_upperLeft = origin;
                } else {
                    _mergeLists(origin);
                }
            } else {
                    // lower left
                if (m_lowerLeft == nullptr)
                {
                    m_lowerLeft = origin;
                } else {
                    _mergeLists(origin);
                }
            }
        } else if (ydiff < 0)
        {       // upper right
            if (m_upperRight == nullptr)
            {
                m_upperRight = origin;
            } else {
                _mergeLists(origin);
            }
        } else {
                // lower right
            if (m_lowerRight == nullptr)
            {
                m_lowerRight = origin;
            } else {
                _mergeLists(origin);
            }
        }
    }

private:

    BestFoundEdgesType m_bestFoundEdges;


    void _removeEdgeDepthFirst(const oko::Node * startPoint, const oko::Node * endPoint, int &countFound)
    {
        if (countFound >= 2)
            return;

        countFound += _removeEdgeFromQuader(startPoint, endPoint);
        /// second time, because endPoint can be in another list to startPoint compaired.
        if (countFound < 2)
            countFound += _removeEdgeFromQuader(endPoint, startPoint);

        if (countFound < 2 && m_lowerLeft)
            m_lowerLeft->_removeEdgeDepthFirst(startPoint, endPoint, countFound);
        if (countFound < 2 && m_lowerRight)
            m_lowerRight->_removeEdgeDepthFirst(startPoint, endPoint, countFound);
        if (countFound < 2 && m_upperLeft)
            m_upperLeft->_removeEdgeDepthFirst(startPoint, endPoint, countFound);
        if (countFound < 2 && m_upperRight)
            m_upperRight->_removeEdgeDepthFirst(startPoint, endPoint, countFound);

    }

    int _removeEdgeFromQuader(const oko::Node * startPoint, const oko::Node * endPoint)
    {
        _removeEdgeFromListNewlyAdded(startPoint, endPoint);
        int countFound=0;
        if ((countFound += _removeEdgeFromList(m_edgesUpperLeft, startPoint, endPoint)) > 0)
            return countFound;
        if ((countFound += _removeEdgeFromList(m_edgesUpperRight, startPoint, endPoint)) > 0)
            return countFound;
        if ((countFound += _removeEdgeFromList(m_edgesLowerLeft, startPoint, endPoint)) > 0)
            return countFound;
        if ((countFound += _removeEdgeFromList(m_edgesLowerRight, startPoint, endPoint)) > 0)
            return countFound;
        return 0;
        /// TODO [seboeh] assert(false); check, if remove and no edge was inside.
    }

    int _removeEdgeFromList(std::list<const Edge*> &list, const oko::Node *startPoint, const oko::Node *endPoint)
    {
        int countFound = 0;
        list.remove_if([startPoint, endPoint, &countFound](const Edge * &e) {
            if ((e->startPoint == startPoint && e->endPoint == endPoint)
                    || (e->startPoint == endPoint && e->endPoint == startPoint))
            {
                ++countFound;
                delete e; e = nullptr;
                return true;
            } else {
                return false;
            }
        });
        return countFound;
    }

    void _removeEdgeFromListNewlyAdded(const oko::Node *startPoint, const oko::Node *endPoint)
    {
        m_newlyAdded.remove_if([startPoint, endPoint](const Edge * &e) {
            if ((e->startPoint == startPoint && e->endPoint == endPoint)
                    || (e->startPoint == endPoint && e->endPoint == startPoint))
            {
                return true;
            } else {
                return false;
            }
        });
    }

    void _insertPointToEdgeQuader(const oko::Node *startPoint, const oko::Node *endPoint)
    {
        double ydiff = startPoint->point.y() - m_center.y();
        double xdiff = startPoint->point.x() - m_center.x();
        Edge *edge = new Edge(const_cast<oko::Node*>(startPoint), const_cast<oko::Node*>(endPoint));

        /// Hint: points on the coordinate-cross
        if (xdiff <= 0 && ydiff <= 0)
        {        // coordinate-origin left top.
            m_edgesUpperLeft.push_back(edge);
        } else if (xdiff > 0 && ydiff <= 0)
        {
            m_edgesUpperRight.push_back(edge);
        } else if (xdiff <= 0 && ydiff > 0)
        {
            m_edgesLowerLeft.push_back(edge);
        } else {
            m_edgesLowerRight.push_back(edge);
        }

        m_newlyAdded.push_back(edge);
    }

    void _insertPointToEdgeLists(const oko::Node *startPoint, const oko::Node *endPoint)
    {
        _insertPointToEdgeQuader(startPoint, endPoint);
        _insertPointToEdgeQuader(endPoint, startPoint);
    }

    BspNode * _pullHigherOrigin(oko::Node * node)
    {
        assert(node != nullptr);
        if (node->origin == nullptr)
            return this;
        if (node->origin == this)
            return this;
        oko::Node *endPoint = node->origin->pullEdgesRemoveFromList(node);
        if (endPoint == nullptr)        // we does not found an edge with node as startPoint.
            return this;
        // This assert is used to guarantie, that the pulling mechanism is not pulled twice.
        // HINT: Should not be pulled twice, because we would lose the first pull edge.
        // But edges are only possible two at one point.
        // TODO [seboeh] check not yet implemented: assert(!node->m_isPulledToHigherOrigin);
        // node->m_isPulledToHigherOrigin = true;
        TSP::Point diffToOrigin = node->point - m_center;
        if (diffToOrigin.x() <= 0)
        {
            if (diffToOrigin.y() <= 0)
            {
                m_edgesUpperLeft.push_back(new Edge{node, endPoint});
            } else {
                m_edgesLowerLeft.push_back(new Edge{node, endPoint});
            }
        } else {
            if (diffToOrigin.y() <= 0)
            {
                m_edgesUpperRight.push_back(new Edge{node, endPoint});
            } else {
                m_edgesLowerRight.push_back(new Edge{node, endPoint});
            }
        }
        return this;
    }

    oko::Node * _pullEdgesRemoveFromList(std::list<const Edge*> &list, const oko::Node *startPoint)
    {
        for (auto edgeI = list.begin();
             edgeI != list.end(); ++edgeI)
        {
            if ((*edgeI)->startPoint == startPoint)
            {
                oko::Node * endPoint = (*edgeI)->endPoint;
                _removeEdgeFromListNewlyAdded(startPoint, endPoint);
                delete *edgeI;
                list.erase(edgeI);
                return endPoint;
            }
        }
        return nullptr;
    }


    void _selectClosestEdgeFromSpace(bsp::Edge *&closestEdge, double &reachedDist, std::list<const bsp::Edge *> &hullEdgeList, const TSP::Point &point);
    void _findClosestEdgeDepthFirstInHull(bsp::Edge *&resultEdge, double &reachedDist, bsp::BspNode *hullNode, const TSP::Point &point);

    int _removeFromEdgeListsTwice(const oko::Node *next, const oko::Node* startPoint)
    {
        int countFound = _removeEdgeFromQuader(startPoint, next);
        /// second time, because endPoint can be in another list to startPoint compaired.
        if (countFound < 2)
            countFound += _removeEdgeFromQuader(next, startPoint);
        /// HINT: TODO [seboeh] It could be, that if we increase the origin for the startPoint to a higher origin,
        /// it is possible to omit the depth-first search at find closest point in TamarAlgorithm.
        /// HINT: Because we have set the edge higher, we have to check for edges also on higher node.
        if (countFound < 2
                && next->origin != nullptr
                && next->origin != this
                && m_lowerLeft != next->origin
                && m_lowerRight != next->origin
                && m_upperLeft != next->origin
                && m_upperRight != next->origin)
        {       // next->origin is a higher origin.
            countFound += next->origin->removeFromEdgeLists(startPoint);
        }
        // TODO [seboeh] implement check : startPoint->m_isPulledToHigherOrigin = false;
        // next->m_isPulledToHigherOrigin = false;
        /// HINT: Although we pull the edge to a higher origin, we have to do a depth-first search for deleting here.
        if (countFound < 2)
            _removeEdgeDepthFirst(startPoint, next, countFound);

        return countFound;
    }

    void _mergeLists(BspNode *origin)
    {
        m_edgesUpperLeft.insert(m_edgesUpperLeft.end()
                                , origin->m_edgesUpperLeft.begin()
                                , origin->m_edgesUpperLeft.end());
        m_edgesUpperRight.insert(m_edgesUpperRight.end()
                                 , origin->m_edgesUpperRight.begin()
                                 , origin->m_edgesUpperRight.end());
        m_edgesLowerRight.insert(m_edgesLowerRight.end()
                                 , origin->m_edgesLowerRight.begin()
                                 , origin->m_edgesLowerRight.end());
        m_edgesLowerLeft.insert(m_edgesLowerLeft.end()
                                , origin->m_edgesLowerLeft.begin()
                                , origin->m_edgesLowerLeft.end());
    }

};

}



static void debugBspPrint()
{
    std::fstream file;
    file.open("bspdebug.output.txt", std::ios_base::out);
    if (!file.is_open())
        assert(false);
    file << "Key; RelevantPoint; OriginAddress; StartPoint; EndPoint; Insert/Remove" << std::endl;
    for (const auto &entry : bspDebugMapEntryList)
    {
        if (std::get<0>(entry) == 0)
        {
            auto bspEntry = std::get<2>(entry)->second;
            file << bspEntry.getKey() << "; "
                 << std::get<1>(entry) << "; "
                 << std::addressof(bspEntry.origin) << "; "
                 << bspEntry.start->point.x() << "x" << bspEntry.start->point.y() << "; "
                 << bspEntry.end->point.x() << "x" << bspEntry.end->point.y() << "; "
                 << bspEntry.count << std::endl;
        } else {
            file << "del " << std::get<1>(entry) << std::endl;
        }
    }
    file.close();
}



#endif // BSPNODE_H
