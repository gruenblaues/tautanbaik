#include "BellmannHeldKarpAlgo.h"


// *************** this classes are given up, because there are implementations already in the internet!
// ***************

#include <array>

using namespace std;

void TSP::BellmannHeldKarpAlgo::buildTree(TSP::DT::DTNode::NodeType node, float, TSP::TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL input;
    size_t num = 1;
    for (auto * &point : *openPointsBasic)
    {
        input.push_back(new Node(*point));
        input.back()->number = ++num;
    }

    calcDistanceMatrix(input);

    _input = std::move(input);

    NodeL tspRes = dynamicProgrammingWalk(1);

    assert(m_resultWay != nullptr);
    for (Node *n : tspRes)
        m_resultWay->push_back(n->point);
}

void TSP::BellmannHeldKarpAlgo::calcDistanceMatrix(TSP::BellmannHeldKarpAlgo::NodeL &input)
{
    for (Node * &n : input)
    {
        for (Node *&m : input)
        {
            if (n == m) continue;
            _calculations.distMat[n->number][m->number] = m_para.distanceFnc(n->point, m->point);
        }
    }
}

TSP::BellmannHeldKarpAlgo::NodeL TSP::BellmannHeldKarpAlgo::dynamicProgrammingWalk(size_t level)
{
    assert(!_input.empty());

    /// cancel criteria of main recursive method
    if (level > _input.size())
    {
        return NodeL();
    }

    // iterate recusive through the sub-sets, because of backtracking at getting the result.
    size_t s = _input.size();
    _bitsI.clear();
    for (size_t i = 0; i < s; ++i) _bits[i] = false;
    map<double , NodeL> dists;

    // start condition for this level, using bits mask, because easier.
    for (size_t i=0; i < level; ++i) { _bits[i] = true; _bitsI.push_back(i); }

    NodeL subset;
    std::list<Calculations::container_type::mapped_type::mapped_type::iterator> newSubSetsIter;
    bool isEnd = false;
    bool isLastRun = false;
    while (!isEnd || isLastRun)
    {
        /// prepare
        subset.clear();

        /// calc subset
        size_t i = 0;
        for (Node* n : _input)
        {
            if (_bits[i])
                subset.push_back(n);
            ++i;
        }

        /// add new subset
        auto subsetI = _calculations.addSubSet(make_shared<NodeL>(subset));

        if (level > 1)
        {
            /// calculate dist
            double distBest = numeric_limits<double>::max();
            NodeLP subSubSetBest;
            Node *pivotBest = nullptr;
            Node *subPivotBest = nullptr;
            for (Node* &pivot : subset)     // todo: improve : if {1,2} only {1},{2} is necessary, not add {2},{1}
            {
                auto subSubSet = _calculations.getSubSets(level, pivot, subset);

                double distSubBest = numeric_limits<double>::max();
                Node*  distSubPivotBest = nullptr;
                for (Node * node : *subSubSet)
                {
                    double dist = m_para.distanceFnc(node->point, pivot->point);
                    if (dist < distSubBest)
                    {
                        distSubBest = dist;
                        distSubPivotBest = node;
                    }
                }
                assert(distSubPivotBest != nullptr);

                distSubBest += _calculations.dist(level, pivot, subSubSet);
                if (distSubBest < distBest)
                {
                    distBest = distSubBest;
                    subSubSetBest = subSubSet;
                    pivotBest = pivot;
                    subPivotBest = distSubPivotBest;
                }

                newSubSetsIter.push_back(_calculations.push_back(level, distSubBest, pivot, distSubPivotBest, *subsetI));
            }

        }

        /// iterate
        // add one bit
        for (size_t i : _bitsI) _bits[i] = false;
        _nextBits();
        for (size_t i : _bitsI) _bits[i] = true;

        // iterated to the end?
        if (isLastRun)
        {
            isEnd = true;
            isLastRun = false;
        } else {
            isEnd = true;
            for (size_t i=1; isEnd && i <= level; ++i)
                isEnd &= _bits[s-i];
            if (isEnd && !isLastRun) isLastRun = true;
            else isLastRun = false;
        }
    }

    /// main algo part - recusive call and backtracking
    auto backtrackingRes = dynamicProgrammingWalk(level + 1);

    // find shortest pivot and subset, containing not backtrackingRes
    double bestBackDist = numeric_limits<double>::max();
    Node *bestBackPivot = nullptr;
    Node *bestBackNext = nullptr;
    Calculations::container_type::mapped_type::mapped_type::iterator subSetPosBest;
    for (Calculations::container_type::mapped_type::mapped_type::iterator subSetPos : newSubSetsIter)
    {
        if (
               true // get<0>(subSetPos->second) == backtrackingRes.back()
                    // || level == s
                )
        {
            bestBackPivot = get<0>(subSetPos->second);
            bestBackNext = get<1>(subSetPos->second);
            bestBackDist = get<2>(subSetPos->second);

            auto & subset = subSetPos->first;

            if (get<2>(subSetPos->second) < bestBackDist)
            {
//                if (
//                    find_if(backtrackingRes.begin(), backtrackingRes.end(),
//                         [&subset] (const NodeL::value_type &val)
//                        {
//                             return find(subset->begin(), subset->end(), val) != subset->end();
//                        }) == backtrackingRes.end()
//                    )
//                {
                    bestBackPivot = get<0>(subSetPos->second);
                    bestBackNext = get<1>(subSetPos->second);
                    bestBackDist = get<2>(subSetPos->second);
//                }
            }
        }
    }

    if (level == s)
    {
        backtrackingRes.push_back(bestBackPivot);
    }
    if (bestBackNext != nullptr)        // nullptr, because level==1
        backtrackingRes.push_front(bestBackNext);

    return backtrackingRes;
}

void TSP::BellmannHeldKarpAlgo::_nextBits()
{
    size_t s = _input.size();
    list<list<size_t>::iterator> bitsIs;
    size_t i = 0;
    for (auto bitsI = _bitsI.end();
            bitsI != _bitsI.begin(); )
    {
        --bitsI;
        ++i;
        if (*bitsI < s-i) {
            size_t b = ++(*bitsI);
            for (auto & bitsII : bitsIs)
                *bitsII = ++b;
            assert(b < s);
            break;
        } else if (*bitsI == s-i) {
            bitsIs.push_front(bitsI);
        } else assert(false);
    }
}

std::tuple<TSP::BellmannHeldKarpAlgo::NodeL, double> TSP::BellmannHeldKarpAlgo::_addPivot(TSP::BellmannHeldKarpAlgo::NodeL &subSubset, TSP::BellmannHeldKarpAlgo::Node *pivot)
{
    double d1 = m_para.distanceFnc(subSubset.front()->point, pivot->point);
    double d2 = m_para.distanceFnc(subSubset.back()->point, pivot->point);

    double currentLength = 0;
    Node *prev = nullptr;
    for (const auto &n : subSubset)
    {
        if (prev == nullptr)
        {
            prev = n;
        }
        else
        {
            currentLength += m_para.distanceFnc(prev->point, n->point);
        }
    }

    if (d1 < d2)
    {
        auto setN = subSubset;
        setN.push_front(pivot);
        return make_tuple(move(setN), currentLength + d1);
    } else {
        auto setN = subSubset;
        setN.push_back(pivot);
        return make_tuple(move(setN), currentLength + d2);
    }
}
