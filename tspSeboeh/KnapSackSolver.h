#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <tuple>
#include <set>
#include <map>
#include <list>
#include <memory>
#include <functional>

#include "dataStructures/OlavList.h"

namespace TSP {

using Node = oko::Node;

using GroupRepresentationType = std::set<Node*>;      // need find() and insert()

struct GroupFromNodeEntry
{
    Node *from = nullptr;
    double dist = std::numeric_limits<double>::max();
};

using GroupFromNodeType = std::list<GroupFromNodeEntry>;

using GroupFromNodeTypePointer = std::shared_ptr<GroupFromNodeType>;

struct GroupMapperEntry
{
    GroupRepresentationType group;
    GroupFromNodeTypePointer froms;
};

using GroupMapperType = std::list<GroupMapperEntry>;

static const size_t size_undefined = std::numeric_limits<size_t>::max();

struct IndizesEntry
{
    size_t index = 0;
    Node *from = nullptr;
};

template<typename T>
void insert_sort(T &container, const typename T::value_type &element
                 , const std::function<bool(const typename T::value_type &, const typename T::value_type &)> &lessEqual)
{
    for (auto iter = std::begin(container);
         iter != std::end(container); ++iter)
    {
        if (!lessEqual(*iter, element))
        {
            container.insert(iter, element);
            return;
        }
    }
    container.push_back(element);
}

class GroupMapperAbstract
{
public:
    virtual GroupMapperType getRepresentations() = 0;
};

// I=input O=Output e.g. Sample* G=group contains I
template <typename I, typename O, typename GI=std::list<I> >
class GroupMapper : public GroupMapperAbstract
{
public:

    void insert(GI &in, O out)
    {
        m_groupsIn.push_back(&in);
        m_groupsOut.push_back(std::make_shared<O>(out));
        for (I &inEl : in)
            m_inputCount.insert({inEl, true});
    }

    size_t elementCount() { return m_inputCount.size(); }

    std::list<std::shared_ptr<O>> getOutput(const std::list<IndizesEntry> &indizes)
    {
        std::list<std::shared_ptr<O>> groupOut;
        for (const IndizesEntry ind: indizes)
        {
            (*m_groupsOut[ind.index])->lastFrom = ind.from;
            groupOut.push_back(m_groupsOut[ind.index]);
        }
        return groupOut;
    }

    /// The user have to create indizes according to this list order.
    /// This is necessary for getOutput().
    GroupMapperType getRepresentations()
    {
        GroupMapperType out;
        auto groupOutI = m_groupsOut.begin();
        for (const GI* &gi : m_groupsIn)
        {
            GroupRepresentationType reps;
            for (I const &inEl : *gi)
                // FIXME: implementation not based on int representant, instead based on Node* representant.
                // Old code: reps.insert(m_inputToIndex[inEl]);
                reps.insert(inEl);
            // TODO [seboeh] extract ->dist func.
            out.push_back({reps, (*(*groupOutI))->froms});
            ++groupOutI;
        }
        return out;
    }


private:
    std::vector<const GI*> m_groupsIn;
    std::vector<std::shared_ptr<O>> m_groupsOut;
    std::map<Node*,bool> m_inputCount;
};

class GroupMapBuilder
{
public:
    typedef oko::OlavList Way;
//    typedef oko::NodeChild NodeChild;
    typedef oko::Node Node;
//    typedef oko::ClosestEdges ClosestEdges;

    /// input of edges and innerPoint base
    struct Edge
    {
        oko::Node *fromNode = nullptr;
        oko::Node *toNode = nullptr;
    };
    struct Sample
    {
        GroupFromNodeTypePointer froms;
        Node *lastFrom = nullptr;
        std::list<oko::Node*> innerNodes;
    };
    typedef GroupMapper<oko::Node*, Sample*> TamarGroupMapperType;

    std::shared_ptr<TamarGroupMapperType> createMapper(std::list<oko::Node*> fromNodes, Way *innerPointWay);

private:
    std::list<Sample> m_samples;
    std::shared_ptr<TamarGroupMapperType> m_mapper;
};



class KnapSackSolver
{
public:

    typedef oko::OlavList Way;

    typedef std::vector<GroupMapperEntry> InputContainerType;

    typedef std::list<IndizesEntry> IndizesType;
    void setInput(const InputContainerType &input);

    void setInput(GroupMapperAbstract * const mapper)
    {
        InputContainerType itemsKnapSack;
        auto reps = mapper->getRepresentations();
        for (const auto rep : reps)
            itemsKnapSack.push_back(rep);
        setInput(itemsKnapSack);
    }

    /**
     * @brief findBestPack
     * @param items
     * @param countOfNodes = N
     * @return
     */
    IndizesType findBestPack(const int countOfNodes);

private:
    /// for input
    struct GroupIndex
    {
        size_t index = 0;       ///< index 0 is special case - default/undefined.
        Node* from = nullptr;
        GroupFromNodeTypePointer fromsOrigin;
    };

    using GroupIndizes = std::list<GroupIndex>;
    typedef std::multimap<double, std::tuple<GroupMapperEntry, int> > InputContainerSortedType;   // first=InputEl.Repres.size/InputEl second=InputEl, third=index
    typedef std::vector<std::tuple<GroupRepresentationType , int, GroupFromNodeTypePointer> > PreparedInputType;       // repr, costs, group-from(inclunding value)
    PreparedInputType m_preparedItems;
    GroupRepresentationType _getRepresentation(const PreparedInputType::iterator &pos)
    {
        return std::get<0>(*pos);
    }

    GroupFromNodeTypePointer _getFromNodes(const PreparedInputType::iterator &pos)
    {
        return std::get<2>(*pos);
    }

    struct GroupFromOrderEntry
    {
        double dist = std::numeric_limits<double>::max();
        Node *from = nullptr;
        GroupFromNodeType::iterator fromI;
        GroupFromNodeTypePointer fromsOrigin;
        size_t groupIndex;
    };

    using GroupFromOrderContainer = std::list<GroupFromOrderEntry>;

    using GroupFromOrderSmallContainer = std::map<size_t, std::tuple<Node*, double>>;       // groupIndex, <fromNode, dist>

    struct CalcAreaElement
    {
        GroupIndizes indizes;
        double value = -std::numeric_limits<double>::max();
        GroupFromOrderContainer fromsOrdered;
        GroupFromOrderSmallContainer fromsOrderedSmall;
    };
    using CalcAreaType = std::vector<std::vector<CalcAreaElement> >;

    int _getCosts(const PreparedInputType::iterator &pos)
    {
        return std::get<1>(*pos);
    }

    /// for output

    /// for algorithm
    CalcAreaType m_calc;
    size_t m_countOfNodes;

    std::tuple<bool, KnapSackSolver::CalcAreaElement> _calcAndcompareValue(size_t index, size_t weight, size_t itemWeight);
    std::tuple<double, size_t, std::map<Node *, bool> > _getGroupAndCount(CalcAreaElement &calcEl, GroupRepresentationType &currentItem, size_t currentIndex);
    double _getOldValueReal(CalcAreaElement &calcEl, size_t index);
    double _getNewValue(size_t groupCountOld, double groupValueOld, double valueCurrent);
    void _addToOrderedFroms(CalcAreaElement &calcy, size_t currentIndex, GroupFromNodeTypePointer currentFroms);
    bool _reorderFroms(CalcAreaElement &calcy, GroupFromNodeTypePointer currentFroms, size_t currentIndex);
    std::tuple<KnapSackSolver::GroupIndex, bool> _makeStdGroupIndex(CalcAreaElement &calc, size_t index);
    std::tuple<GroupRepresentationType, bool, bool> _substract(const GroupRepresentationType &currentItems, const GroupRepresentationType &disjointItemsArg);
    double _getValue(CalcAreaElement &calcEl);
    std::tuple<KnapSackSolver::CalcAreaElement, size_t> _findNewestCoveredGroup(size_t index, size_t newIndex, size_t weight, GroupRepresentationType &currentItems);
};

}
