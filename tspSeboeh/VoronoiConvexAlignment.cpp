#include "VoronoiConvexAlignment.h"

#include <algorithm>
#include <tuple>
#include <math.h>
#ifndef M_PI
#define M_PI       3.14159265358979323846   // pi
#endif

#include "bspTreeVersion/BspTreeSarah.h"
#include "TamarAlgorithmHelper.h"
#include "Way.h"

using TSPWay = TSP::Way;

using namespace TSP;
using namespace std;
using namespace TSPHelper;

size_t VoronoiConvexAlignment::tspSize = 0;
double VoronoiConvexAlignment::tspWidth = 0;
double VoronoiConvexAlignment::tspHeight = 0;

VoronoiConvexAlignment::VoronoiConvexAlignment()
    : TspAlgoCommon()
{

}

VoronoiConvexAlignment::NodeL VoronoiConvexAlignment::createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL ret;

    double tspMaxWidth = 0;
    double tspMaxHeight = 0;
    double tspMinWidth = numeric_limits<double>::max();
    double tspMinHeight = numeric_limits<double>::max();
    for (const auto &point : *openPointsBasic)
    {
        ret.push_back(std::make_shared<Node>(*point));
        if (tspMaxWidth < point->x())
            tspMaxWidth = point->x();
        if (tspMinWidth > point->x())
            tspMinWidth = point->x();
        if (tspMaxHeight < point->y())
            tspMaxHeight = point->y();
        if (tspMinHeight > point->y())
            tspMinHeight = point->y();
    }
    tspWidth = tspMaxWidth - tspMinWidth;
    tspHeight = tspMaxHeight - tspMinHeight;

    return ret;
}

bsp::BspTreeSarah VoronoiConvexAlignment::_createBSPTree(NodeL &input)
{
    InputCitiesPointerType cities;
    for (const auto &node : input)
        cities.push_back(new TSP::Point(node->point));
    // HINT: above. better to use the nodes from input than new points.
    // because later search of hull-node necessary - see mark MARK_hullNodeSearch

    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cities);

    return bspTree;
}

std::tuple<VoronoiConvexAlignment::NodeL, VoronoiConvexAlignment::Way*> VoronoiConvexAlignment::determineHull(NodeL &input)
{
    NodeL ret;
    // * split input into quad-BSP-tree
    // * on returning from recursion merge to cyclic way (convex hull).

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = _createBSPTree(input);

    /// 2. create points order - merge
    //////////////////////////
    Way * way = nullptr;
    auto root = bspTree.root();
    if (root != nullptr) {
        way = _newConqueredWay(root);
        if (way != nullptr)
        {
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (Way::iterator valI=way->begin();
                 !valI.end(); )
            {
                Node *node = valI.nextChild();
                node->isHull = true;
                ret.push_back(std::shared_ptr<Node>(node));
            }
// TODO [seboeh] check if possible
//            way->clear();
//            delete way;
        }
    }

    // update input with new nodes from way
    Way::iterator wayI = way->begin();
    while (!wayI.end())
    {
        Node *node = wayI.nextChild();
        auto inputI = find_if(input.begin(), input.end(),
                              [node] (const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        assert(inputI != input.end());
        *inputI = shared_ptr<Node>(node, no_op_delete());
    }

    // * create resulting list

    return make_tuple(ret, way);
}


bool VoronoiConvexAlignment::_isClockwise(TSP::Point pCenter, Way *way)
{
    Way::iterator pI = way->begin();
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

void VoronoiConvexAlignment::_flipWay(Way *way, TSP::Point pCenter)
{
    if(!_isClockwise(pCenter, way))
        way->flipDirection();
}

VoronoiConvexAlignment::Way *VoronoiConvexAlignment::_newConqueredWay(bsp::BspNode *node)
{
    if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        Way *way = new Way();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
//        way->setOrigin(node);
        return way;
    }
    Way * wayUpper;
    Way * wayLower;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        Way * wayUpperLeft = _newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            Way * wayUpperRight = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x()/2.0, pUpperCenter.y()/2.0);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            _mergeHull(wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        } else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    } else {
        if (node->m_upperRight) {
            wayUpper = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        } else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        Way * wayLowerLeft = _newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            Way * wayLowerRight = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x()/2.0, pLowerCenter.y()/2.0);

            _mergeHull(wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        } else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    } else {
        if (node->m_lowerRight) {
            wayLower = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        } else {
            wayLower = nullptr;
        }
    }

    //incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {
        // HINT: test37 needs master center node->m_center.
        // HINT: test2 needs flipWay
        _flipWay(wayLower, pLowerCenter);
        _flipWay(wayUpper, pUpperCenter);
        _mergeHull(wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    } else if (wayUpper) {
        return wayUpper;
    } else if (wayLower) {
        return wayLower;
    } else {
        return nullptr;
    }
}

float sign (const Point &p1, const Point &p2, const Point &p3)
{
    return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
}

bool VoronoiConvexAlignment::_isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    if (p1 == inside || p2 == inside || p3 == inside)
        return false;

    // [https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle]
    Point pt = inside;
    Point v1 = p1;
    Point v2 = p2;
    Point v3 = p3;
    float d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(pt, v1, v2);
    d2 = sign(pt, v2, v3);
    d3 = sign(pt, v3, v1);

    has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    has_pos = (d1 >= 0) || (d2 >= 0) || (d3 >= 0);      // =0 --> on one line of triangle

    bool isInsideV1 = !(has_neg && has_pos);


    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    // barycentric coordinates.
    bool isInsideV2 = false;
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        isInsideV2 = false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        isInsideV2 = false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        isInsideV2 = false;

    // see test12 - V2 was wrong assert(isInsideV1 == isInsideV2);
    return isInsideV1;
}

void VoronoiConvexAlignment::_triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = _isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = _isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
                && pb == *lpbI
                && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
                && pb == *hpbI
                && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

VoronoiConvexAlignment::CrossedState VoronoiConvexAlignment::_isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint)
{
    /// Crossed between a and b?
    double x1 = pa1I.x();
    double y1 = pa1I.y();
    double x2 = pa2I.x();
    double y2 = pa2I.y();
    double x3 = pb1I.x();
    double y3 = pb1I.y();
    double x4 = pb2I.x();
    double y4 = pb2I.y();

    /// HINT: [http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection]
    double denominator = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (denominator == 0) {
        // lines are parallel not guarantied to lay on each other.
        // We want to integrate the considered node as inner node -> so it should not be covered -> parallel
        return CrossedState::isParallel;
    }
    double pX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denominator;
    double pY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denominator;
    if (crossPoint != nullptr)
        *crossPoint = TSP::Point(pX,pY);

    if ((std::max)(std::min(x1,x2),std::min(x3,x4)) <= pX && pX <= std::min((std::max)(x1,x2),(std::max)(x3,x4))
            && (std::max)(std::min(y1,y2),std::min(y3,y4)) <= pY && pY <= std::min((std::max)(y1,y2),(std::max)(y3,y4)))
    {
        if ((pX == x1 && pY == y1)          // FIX: Could be a speed-up, if we check if this crossing on exact one outer point could happen with the input.
            || (pX == x2 && pY == y2)
            || (pX == x3 && pY == y3)
            || (pX == x4 && pY == y4))
        {
            return CrossedState::isOnPoint;
        } else {
            return CrossedState::isCrossed;
        }
    } else {
        return CrossedState::isOutOfRange;
    }
}

VoronoiConvexAlignment::DirectAccessState VoronoiConvexAlignment::_isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI)
{
    Way::iterator prevAI = pAI; --prevAI;
    Way::iterator nextAI = pAI; ++nextAI;
    Way::iterator prevBI = pBI; --prevBI;
    Way::iterator nextBI = pBI; ++nextBI;
    CrossedState state = _isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = _isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = _isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = _isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool VoronoiConvexAlignment::_isNormalVectorLeft(const Point &from, const Point &to, const Point &insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x())/2
                    , (from.y() + to.y())/2);

    Point normalVectLeft(meanPoint.x() - straight.y()
                         , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
                          , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}


bool VoronoiConvexAlignment::_isCovered(Way * way, Way::iterator &w1I, Way::iterator &fixI)
{
    Way::iterator wI = way->begin();
    Way::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = _isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed || state == CrossedState::isCovered)
            return true;
        if (state == CrossedState::isOnPoint)
        {
            if (crossedPoint == *wI || crossedPoint == *wNI)
                return true;
        }		// else out of range
    }
    return false;
}

bool VoronoiConvexAlignment::_createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI)
{
    if (_isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        Way::iterator w1NewI = way1->push_back(&way2I);
        Way::iterator w1NNewI = way1->push_back(&way2NI);

        Node *nodePoint = (&w1Inside);
        if (dist1 < dist2 && dist1 < dist3)
        {
            nodePoint->prev = &w1Alt;
            nodePoint->next = (&w1Alt)->next;
            way1->insertChild(w1Alt, nodePoint);      // TODO [seboeh] is new really necessary?
        } else if (dist2 < dist1 && dist2 < dist3)
        {
            nodePoint->prev = &w1NNewI;
            nodePoint->next = (&w1NNewI)->next;
            way1->insertChild(w1NNewI, nodePoint);
        } else {
            nodePoint->prev = &w1NewI;
            nodePoint->next = (&w1NewI)->next;
            way1->insertChild(w1NewI, nodePoint);
        }
        return true;
    }
    return false;
}

void VoronoiConvexAlignment::_createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    Way::iterator w1I = way1->begin();
    Way::iterator w1NI = w1I; ++w1NI;
    Way::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    Node *nodePoint = (&insideI);
    if (dist1 < dist2 && dist1 < dist3)
    {
        nodePoint->prev = &w1I;
        nodePoint->next = (&w1I)->next;
        way1->insertChild(w1I, nodePoint);
    } else if (dist2 < dist1 && dist2 < dist3)
    {
        nodePoint->prev = &w1NI;
        nodePoint->next = (&w1NI)->next;
        way1->insertChild(w1NI, nodePoint);
    } else {
        nodePoint->prev = &w1NNI;
        nodePoint->next = (&w1NNI)->next;
        way1->insertChild(w1NNI, nodePoint);
    }
}

void VoronoiConvexAlignment::_mergeHull(Way * &way1, const TSP::Point &pWay1Center, Way * &way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        _flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        Way::iterator w1I = way1->begin();
        Way::iterator fixI = way2->begin();
        if (_isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            _flipWay(way1, pCenter);
            return;
        } else {
            ++w1I;
            if (_isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                _flipWay(way1, pCenter);
                return;
            } else {
                ++w1I;
                if (_isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    _flipWay(way1, pCenter);
                    return;
                } else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    Way::iterator w1NI = w1I; ++w1NI;
                    Way::iterator w1NNI = w1NI; ++w1NNI;
                    if (_isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        _createTriangle3x1(way1, w1NNI, fixI);
                    } else if (_isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        _createTriangle3x1(way1, w1NI, fixI);
                    } else {
                        _createTriangle3x1(way1, w1I, fixI);
                    }
                    _flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        Way::iterator way1I = way1->begin();
        Way::iterator way2I = way2->begin();
        Way::iterator way1NI = way1I;
        ++way1NI;
        Way::iterator way2NI = way2I;
        ++way2NI;
        Way::iterator way1Nearest;
        Way::iterator way2Nearest;
        double maxDist=std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        Way::iterator way1Next = way1Nearest; ++way1Next;
        Way::iterator way2Next = way2Nearest; ++way2Next;
        if (_isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next) == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            _flipWay(way1, pCenter);
            return;
        } else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (_createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            } else  if (_createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI ))
            {
            } else if (_createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else if (_createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            _flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2

    /// beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (Way::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (Way::iterator wBI=way2->begin();
             !wBI.end(); ++wBI)
        {
            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (_isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    Way::iterator haI, hbI, laI, lbI;

    Point realCenter = (pWay1Center + pWay2Center)/2.0;

    smallestDist /= 2;
    Point lastLowCrossPoint;
    Point lastHighCrossPoint;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = _isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x())-10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x())+10, pCenter.y()), &crossedPoint);
        else
            state = _isCrossed(pa, pb, TSP::Point(pCenter.x(),std::min(pa.y(), pb.y())-10), TSP::Point(pCenter.x(),(std::max)(pa.y(), pb.y())+10), &crossedPoint);

        bool isLowNotHigh = _isNormalVectorLeft(pb, pa, realCenter);

        double value;
        if (state != CrossedState::isOnPoint)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();
        }
        else
        {       // see test82, test76
            // see test89, test64
            Point otherP;
            if (pa == crossedPoint)
                otherP = pb;
            else
                otherP = pa;
            double otherVal;
            if (callState == MergeCallState::topDown)
                otherVal = otherP.x();
            else
                otherVal = otherP.y();


            // HINT: center of pa_pb is not working, because test76 cascading of points.
            Point descent = otherP - crossedPoint;
            descent /= m_para.distanceFnc(otherP, crossedPoint);

            if (callState == MergeCallState::topDown)
            {
                value = crossedPoint.x() + descent.x()*smallestDist;
            }
            else
            {
                value = crossedPoint.y() + descent.y()*smallestDist;
            }
        }

        bool isGood = false;
        if (isLowNotHigh)
        {
            if ((callState == MergeCallState::topDown
                    && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGood = true;
            }
            if (lastLowCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastLowCrossPoint = Point();
                }
            }
        }
        else
        {
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                    && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGood = true;
            }
            if (lastHighCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastHighCrossPoint = Point();
                }
            }
        }

        if (((state == CrossedState::isCrossed
                    || state == CrossedState::isOnPoint     // because, teset82
              )
                && isGood)          // TODO [seboeh] drop isGood here
           )
        {
            if (isLowNotHigh)
            {
                laI = edgeI->aNodeI;
                lbI = edgeI->bNodeI;
            }
            else
            {
                haI = edgeI->aNodeI;
                hbI = edgeI->bNodeI;
            }
        }
    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, haI, laI, hbI, lbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hbI, lbI, haI, laI);

    /// correct the hull in way1
    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    /// clear childs - see test7
    (&haI)->isChildNode = false;
    (&haI)->child = nullptr;
    (&hbI)->isChildNode = false;
    (&hbI)->child = nullptr;
    (&laI)->isChildNode = false;
    (&laI)->child = nullptr;
    (&lbI)->isChildNode = false;
    (&lbI)->child = nullptr;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in _mergeHull().

    _flipWay(way1, pCenter);  // necessary, because code before does not keep the clock-wise order.
}

double VoronoiConvexAlignment::_calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double VoronoiConvexAlignment::_area(const PointL &polygon)
{
    double area = 0;
    for (auto pointI = polygon.begin();
         pointI != polygon.end(); ++pointI)
    {
        auto pointNextI = pointI; ++pointNextI;
        if (pointNextI == polygon.end()) pointNextI = polygon.begin();
        area += pointI->x() * pointNextI->y();
        area -= pointI->y() * pointNextI->x();
    }
    return abs(area);
}

double VoronoiConvexAlignment::_triangleAltitudeOnC(Point A, Point B, Point C)
{
    double a = m_para.distanceFnc(A, B);
    double b = m_para.distanceFnc(B, C);
    double c = m_para.distanceFnc(C, A);
    double s = (a + b + c)/2.0;
    return 2.0*sqrt(s*(s-a)*(s-b)*(s-c)) / c;
}

bool VoronoiConvexAlignment::_isRightSide(const Point &p1, const Point &p2)
{
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

double VoronoiConvexAlignment::_calcDegree(Node *node, Node *nodeTested)
{
    return _calcDegree(nodeTested->point, node->point);
}

double VoronoiConvexAlignment::_calcDegree(Point &node, Point &nodeTested)
{
    Point pointVect = nodeTested - node;

    double degree = (pointVect.x() + pointVect.y())
            / (m_para.distanceFnc(Point(0,0), pointVect)
               * m_para.distanceFnc(Point(0,0), Point(1,1)));

    double degreeArc = acos(degree);
    // clock-wise
    if (_isRightSide(Point(1,1), pointVect))
        return  degreeArc;
    else
        return  2*acos(-1) - degreeArc;
}

bool VoronoiConvexAlignment::_checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{
    double degO1 = _calcDegree(nodePoint, cluPointOut);
    double degO2 = _calcDegree(nodePoint, cluPointOut2);
    double degC = _calcDegree(nodePoint, closestP);
    double degMin = 0;
    double degMax = 0;
    if (degO1 < degO2)
    {
        degMin = degO1;
        degMax = degO2;
    } else {
        degMin = degO2;
        degMax = degO1;
    }

    if (degMax-degMin >= M_PI)
        return degMax <= degC || degC <= degMin;
    else
        return degMin <= degC && degC <= degMax;
}

double VoronoiConvexAlignment::_correlation(Point a, Point b)
{
    return ((a.x()*b.x()) + (a.y()*b.y())) / (m_para.distanceFnc(Point(0,0), a) * m_para.distanceFnc(Point(0,0),b));
}


bool VoronoiConvexAlignment::_checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{
    double dist1 = m_para.distanceFnc(cluPointOut, nodePoint);
    double dist2 = m_para.distanceFnc(cluPointOut2, nodePoint);
    double distC = m_para.distanceFnc(closestP, nodePoint);
    double corr1 = _correlation(cluPointOut - nodePoint, closestP - nodePoint);
    //double degree0to1For1 = (corr1 + 1) / 2.0;
    double corr2 = _correlation(cluPointOut2 - nodePoint, closestP - nodePoint);
    //double degree0to1For2 = (corr2 + 1) / 2.0;
    //double corr = degree0to1For1 + degree0to1For2;
    //double corr = corr1 + corr2;
    double corr3 = _correlation(cluPointOut - nodePoint, cluPointOut2 - nodePoint);
    // distC, dist1,2 are tradeofs not correct decisions
    return ((distC/2 < dist1 || distC/2 < dist2) && corr3 < corr1 && corr3 < corr2);
}


void VoronoiConvexAlignment::_insertClosestTestedPointsSorted(std::map<double, Node*> &closestTestedPointsSorted, Node *node, Node* closest)
{
    double degree = _calcDegree(node, closest);
    auto iter = closestTestedPointsSorted.find(degree);
    if (iter != closestTestedPointsSorted.end())
    {
        ++iter;
        if (iter != closestTestedPointsSorted.end())
            degree = (degree + iter->first)/2;
        else
        {
            if (closestTestedPointsSorted.size() > 1)
            {
                --iter; --iter;
                degree = (degree + iter->first)/2;
            }
            else
                degree += 0.001;
         }
    }
    closestTestedPointsSorted.insert({degree, closest});
}

VoronoiConvexAlignment::ClusterGrow VoronoiConvexAlignment::determineClusters(NodeL &input, NodeL &hull)
{
    ClusterGrow clusters;
    double coordMax = max(tspWidth, tspHeight) * 12;
    //  TODO [seboeh] * 12 - still a problem, because the intersection below can be far away.

    /// * walk through all inputs except hull-points
    for (const auto &node : input)
    {
        Cluster clu;
        clu.point = node;
        auto fI = std::find_if(hull.begin(), hull.end(),
                               [node](const shared_ptr<Node> &val)
        {
            return val->point == node->point;
        });
        if (fI != hull.end())
            continue;

        /// * find closest three points P_1 to input x
        map<double, shared_ptr<Node>> closestPoints;
        for (const auto &node2 : input)
        {
            if (node == node2)
                continue;
            closestPoints.insert({m_para.distanceFnc(node->point, node2->point), node2});
        }
        if (closestPoints.size() < 3)
            return clusters;
        for (const auto &closE : closestPoints)
        {
            clu.closestN.push_back(closE.second);
        }

        /// * check if x is inside P_1,
        auto closestP1I = closestPoints.begin();
        auto closestP2I = closestP1I; ++closestP2I;
        auto closestP3I = closestP2I; ++closestP3I;
        list<Node*> closestTestedP;
        closestTestedP.push_back(closestP1I->second.get());
        closestTestedP.push_back(closestP2I->second.get());
        closestTestedP.push_back(closestP3I->second.get());
        Node* foundP1 = closestP1I->second.get();
        Node* foundP2 = closestP2I->second.get();
        Node* foundP3 = closestP3I->second.get();
        if ( ! _isInsideTriangle(closestP1I->second->point, closestP2I->second->point, closestP3I->second->point, node->point))
        {
            ///     * if yes, continue
            ///     * else determine next closest p_2
            ///     * check in two of P_1 and p_2, if x is inside this triangle
            ///         * if yes, continue
            ///         * else, repeat until end of inputs. -> unexpected-error, if not found
            auto closestPnextI = ++closestP3I;
            while (closestPnextI != closestPoints.end())
            {
                // 2 over n
                // TODO [seb] hint: keep closest fixed
                bool stopWhile = false;
                for (auto closestTestedP1I = closestTestedP.begin();
                     closestTestedP1I != closestTestedP.end(); ++closestTestedP1I)
                {
                    for (auto closestTestedP2I = closestTestedP.begin();
                         closestTestedP2I != closestTestedP.end()
                         && closestTestedP1I != closestTestedP2I; ++closestTestedP2I)
                    {
                        if (_isInsideTriangle((*closestTestedP1I)->point, (*closestTestedP2I)->point, closestPnextI->second->point, node->point))
                        {
                            foundP1 = *closestTestedP1I;
                            foundP2 = *closestTestedP2I;
                            foundP3 = closestPnextI->second.get();
                            closestTestedP.push_back(closestPnextI->second.get());
                            stopWhile = true;
                            break;
                        }
                    }
                    if (stopWhile) break;
                }
                if (stopWhile) break;
                closestTestedP.push_back(closestPnextI->second.get());
                ++closestPnextI;
            }
            if (closestPnextI == closestPoints.end())
                return clusters;
        }

        /// * sort closestTestedP, according to degree, because there is no order than circular around the point
        // cross product a*d - b*c, with point = (a,b) and (c,d) = (1,1)
        map<double, Node*> closestTestedPointsSorted;
        closestTestedPointsSorted.insert({_calcDegree(node.get(), foundP1), foundP1});
        closestTestedPointsSorted.insert({_calcDegree(node.get(), foundP2), foundP2});
        closestTestedPointsSorted.insert({_calcDegree(node.get(), foundP3), foundP3});


//        clu.closestN.clear();
//        clu.closestN.reserve(closestTestedPointsSorted.size());
//        for (const auto &point : closestTestedPointsSorted)
//        {
//            for (auto &closestE : closestPoints)
//            {
//                if (point.second == closestE.second->point)
//                {
//                    clu.closestN.push_back(closestE.second);
//                    break;
//                }
//            }
//        }

        /// * for all P (P_1,P_2,...):
        ///
        /// * enlarge points to get the real cluster
        clu.verts.clear();
        bool isChanged = true;
        while (isChanged)
        {
            isChanged = false;
            auto closestPI = closestPoints.begin();
            while (closestPI != closestPoints.end())
            {
                Node *nodeClosest = closestPI->second.get();
                auto findClosTesI = find_if(closestTestedPointsSorted.begin()
                                            , closestTestedPointsSorted.end()
                                            , [nodeClosest] (const map<double, Node*>::value_type &val)
                {
                    return val.second == nodeClosest;
                });
                if (findClosTesI != closestTestedPointsSorted.end())
                {
                    ++closestPI;
                    continue;
                }
                for (auto cluPointOutI = closestTestedPointsSorted.begin();
                 cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
                {
                    auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
                    if (cluPointOut2I == closestTestedPointsSorted.end())
                        cluPointOut2I = closestTestedPointsSorted.begin();
                    ///     * calculate mid of x to p_i =e_1 and mid of x to p_{i+1} =e_2
                    Point midE1 = (cluPointOutI->second->point + node->point)/2.0;
                    Point midE2 = (cluPointOut2I->second->point + node->point)/2.0;
                    ///     * calculate the orthogonals to e_1 and e_2
                    Point vec1 = (cluPointOutI->second->point - node->point) / m_para.distanceFnc(cluPointOutI->second->point, node->point);
                    Point midE1b = midE1 + Point(-vec1.y(), vec1.x()) * coordMax;           // rotate 90degree
                    Point vec2 = (cluPointOut2I->second->point - node->point) / m_para.distanceFnc(cluPointOut2I->second->point, node->point);
                    Point midE2b = midE2 + Point(vec2.y(), -vec2.x()) * coordMax;
                    ///     * calculate the intersection of orthogonals
                    Point crossMid;
                    auto intersectRes = _isCrossed(midE1, midE1b, midE2, midE2b, &crossMid);

                    // because test12 node=44x61 missing point 85x97 - but did not solve, still here, because speed-up?
                    Point midE3 = (closestPI->second->point + node->point)/2.0;

                    // vers3 - is aligning better?
                    if (_checkAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                    {
                        double alignPointOut = seboehDist(cluPointOutI->second, cluPointOut2I->second, node.get());
                        double alignPointNew1 = seboehDist(cluPointOutI->second, closestPI->second.get(), node.get());
                        double alignPointNew2 = seboehDist(cluPointOut2I->second, closestPI->second.get(), node.get());
                        // because test5 - dists - to far away principle
                        double lengthNew = m_para.distanceFnc(closestPI->second.get()->point, node.get()->point);
                        double lengthOut1 = m_para.distanceFnc(cluPointOutI->second->point, node->point);
                        double lengthOut2 = m_para.distanceFnc(cluPointOut2I->second->point, node->point);
                        double widthOut = m_para.distanceFnc(cluPointOutI->second->point, cluPointOut2I->second->point);
                        double widthNew1 = m_para.distanceFnc(cluPointOutI->second->point, closestPI->second.get()->point);
                        double widthNew2 = m_para.distanceFnc(cluPointOut2I->second->point, closestPI->second.get()->point);

                        double areaO = _calcAreaOfTriangle(node.get()->point, cluPointOutI->second->point, cluPointOut2I->second->point);

                        double areaN = 0;
                        if (widthNew1 > widthNew2)
                        {
                            areaN = _calcAreaOfTriangle(node.get()->point, closestPI->second.get()->point, cluPointOutI->second->point);
                        } else {
                            areaN = _calcAreaOfTriangle(node.get()->point, closestPI->second.get()->point, cluPointOut2I->second->point);
                        }

                        Point crossAreaO;
                        auto isCrossFound = _isCrossed(cluPointOutI->second->point
                                        , cluPointOutI->second->point +  Point(-vec1.y(), vec1.x()) * coordMax
                                   , cluPointOut2I->second->point
                                        , cluPointOut2I->second->point +  Point(vec2.y(), -vec2.x()) * coordMax
                                   , &crossAreaO);
                        double areaOsquare = 0;
                        if (isCrossFound == CrossedState::isCrossed)
                        {
                            areaOsquare = _calcAreaOfTriangle(node.get()->point, cluPointOutI->second->point, crossAreaO)
                                    + _calcAreaOfTriangle(node.get()->point, cluPointOut2I->second->point, crossAreaO);
                        }
                        else
                            areaOsquare = _calcAreaOfTriangle(node.get()->point, cluPointOutI->second->point, cluPointOut2I->second->point);

                        // Alginment-Paradox - because test12 85x97
                        if (areaN < areaOsquare         // test20
                               )
                        {       // insert
                            _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                            isChanged = true;
                            break;
                        }
                    }

                    // vers2 - check midE3 in triangle midE1, midE2, crossMid
                    if (_isInsideTriangle(midE1, midE2, crossMid, midE3))
                    {       // insert
                        _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                        isChanged = true;
                        break;
                    }

                    if (intersectRes == CrossedState::isCrossed
                            || intersectRes == CrossedState::isOnPoint)
                    {
                        /// check if there exist an intersection closer than crossMid
                        // ver1 old
                        Point vec3 = (closestPI->second->point - node->point) / m_para.distanceFnc(closestPI->second->point, node->point);
                        Point midE3b = midE3 + Point(vec3.y(), -vec3.x()) * coordMax;
                        Point crossMid3;
                        auto intersectRes = _isCrossed(midE1, midE1b, midE3, midE3b, &crossMid3);
                        if (intersectRes == CrossedState::isCrossed
                                || intersectRes == CrossedState::isOnPoint)
                        {
                            // before test12 double distMid1 = m_para.distanceFnc(node->point, crossMid);
                            double distMid1 = m_para.distanceFnc(midE1, crossMid);        // after test12 fix
                            double distMid2 = m_para.distanceFnc(midE1, crossMid3);
                            if (distMid2 < distMid1)
                            {       // insert
                                _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                                isChanged = true;
                                break;
                            }
                        }
                        else
                        {
                            midE3b = midE3 + Point(-vec3.y(), vec3.x()) * coordMax;
                            intersectRes = _isCrossed(midE2, midE2b, midE3, midE3b, &crossMid3);
                            if (intersectRes == CrossedState::isCrossed
                                    || intersectRes == CrossedState::isOnPoint)
                            {
                                double distMid1 = m_para.distanceFnc(midE2, crossMid);
                                double distMid2 = m_para.distanceFnc(midE2, crossMid3);
                                if (distMid2 < distMid1)
                                {       // insert
                                    _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                                    isChanged = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (_checkCorrelationAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                                {
                                    _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                                    isChanged = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_checkCorrelationAngelBetween(cluPointOutI->second->point, cluPointOut2I->second->point, node->point, closestPI->second->point))
                        {
                            _insertClosestTestedPointsSorted(closestTestedPointsSorted, node.get(), closestPI->second.get());
                            isChanged = true;
                            break;
                        }
                    }
                }       // end closestTestedPointsSorted

                /// keep on running with next closest point
    //            if (!isChanged)
    // HINT: not true, because there are points which are outside but on another side.  - break;      // because, the other points are too far away
                // iterate
                ++closestPI;
            }
        }

        ///     * save this intersection as point into Cluster.verts
        for (auto cluPointOutI = closestTestedPointsSorted.begin();
         cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
        {
            auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
            if (cluPointOut2I == closestTestedPointsSorted.end())
                cluPointOut2I = closestTestedPointsSorted.begin();
            ///     * calculate mid of x to p_i =e_1 and mid of x to p_{i+1} =e_2
            Point midE1 = (cluPointOutI->second->point + node->point)/2.0;
            Point midE2 = (cluPointOut2I->second->point + node->point)/2.0;
            ///     * calculate the orthogonals to e_1 and e_2
            Point vec1 = (cluPointOutI->second->point - node->point) / m_para.distanceFnc(cluPointOutI->second->point, node->point);
            Point midE1b = midE1 + Point(-vec1.y(), vec1.x()) * coordMax;           // rotate 90degree
            Point vec2 = (cluPointOut2I->second->point - node->point) / m_para.distanceFnc(cluPointOut2I->second->point, node->point);
            Point midE2b = midE2 + Point(vec2.y(), -vec2.x()) * coordMax;
            ///     * calculate the intersection of orthogonals
            Point crossMid;
            auto intersectRes = _isCrossed(midE1, midE1b, midE2, midE2b, &crossMid);
            if (intersectRes == CrossedState::isCrossed
                    || intersectRes == CrossedState::isOnPoint)
            {
                clu.verts.push_back(crossMid);
            }
            else
            {       // because points (cuPointOut..) do not cross each other inside their range.
                     //because not inserting a point, can result in too less points for an area calculation.
                     //just adding a guessing point, to later include if necessary.

                Point crossOnLine1;
                _isCrossed(midE1, node->point, midE2, midE2b, &crossOnLine1);

                Point crossOnLine2;
                _isCrossed(midE2, node->point, midE1, midE1b, &crossOnLine2);

                if (m_para.distanceFnc(crossOnLine1, node->point)
                        < m_para.distanceFnc(crossOnLine2, node->point))
                {
                    clu.verts.push_back(crossOnLine1);
                    clu.verts.push_back(midE2);
                }
                else
                {
                    clu.verts.push_back(crossOnLine2);
                    clu.verts.push_back(midE1);
                }
            }
        }
        /// * calculate area of cluster
        assert(clu.verts.size() > 2);
        clu.area = _area(clu.verts);
        for (auto &el : closestTestedPointsSorted)
            clu.closestTestedPointsSorted.push_back(el.second);

        // because test5 and test12 - areaOsquare solve the problem of test12 and test5
        // but here because of safety for test5 and similar, the closest edges including first.
        map<double, Node*> sortedByDegree;
        sortedByDegree.insert({_calcDegree(node.get(), foundP1), foundP1});
        sortedByDegree.insert({_calcDegree(node.get(), foundP2), foundP2});
        sortedByDegree.insert({_calcDegree(node.get(), foundP3), foundP3});
        auto sortedI = --sortedByDegree.end();
        auto sortedIn = sortedByDegree.begin();
        clu.neighbourEdges.insert({seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , make_tuple(sortedI->second, sortedIn->second)});
        sortedI = sortedByDegree.begin();
        ++sortedIn;
        clu.neighbourEdges.insert({seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , make_tuple(sortedI->second, sortedIn->second)});
        ++sortedI;
        ++sortedIn;
        clu.neighbourEdges.insert({seboehDist(sortedI->second, sortedIn->second, node.get())
                                   , make_tuple(sortedI->second, sortedIn->second)});

        /// repeat from beginning, until all inputs are handled.
        clusters.push_back(shared_ptr<Cluster>(new Cluster(clu),no_op_delete()));
    }

    // because test20, test100 - there can be two dots behind each other in the ray from the cluster-center
    double degreeMin = numeric_limits<double>::max();
    for (shared_ptr<Cluster> &clu : clusters)
    {
        Point cluP = clu->point.get()->point;
        Node *cluN = clu->point.get();
        auto iterPcurr = clu->closestTestedPointsSorted.begin();
        auto iterPprev = --clu->closestTestedPointsSorted.end();
        for (;iterPcurr != clu->closestTestedPointsSorted.end(); ++iterPcurr)
        {
            double x = fabs(_calcDegree(cluN, *iterPcurr) - _calcDegree(cluN, *iterPprev));
            double degree = fabs(std::fmod(x, 3.14158) - static_cast<int>(x/3.14158)*3.14158);
            if (degree < degreeMin)
                degreeMin = degree;
            if (degree < 0.001)     // because there can be three points in a line - TODO improve the hard-coded number by variable number
            {
                auto iterPrevPrev = iterPprev;
                if (iterPrevPrev == clu->closestTestedPointsSorted.begin())
                    iterPrevPrev = --clu->closestTestedPointsSorted.end();
                else
                    --iterPrevPrev;
                auto iterNext = iterPcurr;
                ++iterNext;
                if (iterNext == clu->closestTestedPointsSorted.end())
                    iterNext = clu->closestTestedPointsSorted.begin();

                double distPrev1 = seboehDist((*iterPrevPrev)->point, (*iterPprev)->point, cluP);
                double distPrev2 = seboehDist((*iterPrevPrev)->point, (*iterPcurr)->point, cluP);
                double distNext1 = seboehDist((*iterNext)->point, (*iterPprev)->point, cluP);
                double distNext2 = seboehDist((*iterNext)->point, (*iterPcurr)->point, cluP);

                if (min(distPrev1, distPrev2) < min(distNext1, distNext2))
                {
                    if (distPrev1 > distPrev2)
                        swap(*iterPcurr, *iterPprev);
                       // else nothing, because all already correct
                } else {
                    if (distNext1 < distNext2)
                        swap(*iterPcurr, *iterPprev);
                }
            }

            iterPprev = iterPcurr;
        }
    }

    return clusters;
}

VoronoiConvexAlignment::Node* VoronoiConvexAlignment::_nextNode(Node *curr, Node * &prev)
{
    if (curr->edge1 == prev)
    {
        prev = curr;
        return curr->edge2;
    }
    else
    {
        prev = curr;
        return curr->edge1;
    }
}

bool VoronoiConvexAlignment::_updateHullOrUpdateSimple(shared_ptr<Node> hullNodeA, shared_ptr<Node> updateNode, NodeL &hull)
{
// MARK_hullNodeSearch
    auto hullI = find_if(hull.begin(), hull.end(),
                         [hullNodeA] (const NodeL::value_type &val)
    {
        return hullNodeA->point == val->point;
    });
    shared_ptr<Node> hullNode;
    if (hullI != hull.end())
        hullNode = *hullI;
    else
        hullNode = hullNodeA;

    // check valid updateNode
    if (updateNode.get() == hullNode->edge1
            || updateNode.get() == hullNode->edge2)
        return false;
    if (hullNode->edge1 != nullptr
            && hullNode->edge2 != nullptr)
        return false;
    if (updateNode->edge1 != nullptr
            && updateNode->edge2 != nullptr)
        return true;

    // update
    if (hullNode->edge1 == nullptr)
    {
        hullNode->edge1 = updateNode.get();
        hullNodeA->edge1 = updateNode.get();
    }
    else if (hullNode->edge2 == nullptr)
    {
        hullNode->edge2 = updateNode.get();
        hullNodeA->edge2 = updateNode.get();
    }    else return true;
    if (updateNode->edge1 == nullptr)
        updateNode->edge1 = hullNode.get();
    else if (updateNode->edge2 == nullptr)
        updateNode->edge2 = hullNode.get();
    else return true;

    // check
    assert(updateNode->edge1 != updateNode.get());
    assert(updateNode->edge2 != updateNode.get());

    return true;
}

bool VoronoiConvexAlignment::_updateAllowed(shared_ptr<Node> closest, shared_ptr<Node> node, bool isTestOnClique)
{
    // check valid nodes
    if (node.get() == closest->edge1
            || node.get() == closest->edge2)
        return false;
    if (closest->edge1 != nullptr
            && closest->edge2 != nullptr)
        return false;
    if (node->edge1 != nullptr
            && node->edge2 != nullptr)
        return true;        // because this calls intern if, which calls this criteria again, and breaks the loop.

    // check on clique - graph theory
    // walk through the edges1/2 starting from closest:
    //  if ending at node, there is a clique - edge shall not be supported
    //  only if the cound of vertices = n.
    if (isTestOnClique)
    {
        Node *nod = closest.get();
        Node *prev = nullptr;
        size_t tspSizeI = 0;
        bool isClique = false;
        while (nod != nullptr)
        {
            ++tspSizeI;
            if (nod == node.get() && tspSizeI < m_tspSize)
            {
                isClique = true;
                break;
            }
            if (nod->edge1 != nullptr && nod->edge1 != prev)
            {
                prev = nod;
                nod = nod->edge1;
            } else if (nod->edge2 != nullptr && nod->edge2 != prev)
            {
                prev = nod;
                nod = nod->edge2;
            }
            else break;
        }
        return !isClique;
    }

    return true;
}


void VoronoiConvexAlignment::_updateNodes2(map<double, Cluster*> &clustersSorted, NodeL &hull)
{
    for (auto &cluE : clustersSorted)
    {
        Cluster *clu = cluE.second;
        Node* from;
        Node* to;
        double distBest = std::numeric_limits<double>::max();
        for (auto nodeI = clu->closestN.begin();
             nodeI != clu->closestN.end(); ++nodeI)
        {
            // update
            auto nodeNextI = nodeI; ++nodeNextI;
            if (nodeNextI == clu->closestN.end())
                nodeNextI = clu->closestN.begin();

            // case no edges
            int clearedEdgesNode1C = 0;
            int clearedEdgesNode2C = 0;
            if ((*nodeI)->edge1 != nullptr)
                ++clearedEdgesNode1C;
            if ((*nodeNextI)->edge1 != nullptr)
                ++clearedEdgesNode2C;
            if ((*nodeI)->edge2 != nullptr)
                 ++clearedEdgesNode1C;
            if ((*nodeNextI)->edge2 != nullptr)
                 ++clearedEdgesNode2C;

            double dist = TSPHelper::seboehDist((*nodeI).get(), (*nodeNextI).get(), clu->point.get());
            if (clearedEdgesNode1C == 2 && clearedEdgesNode2C == 2)
            {
                // replace
                if ((*nodeI)->edge1 == (*nodeNextI)->edge1
                        || (*nodeI)->edge2 == (*nodeNextI)->edge2
                        || (*nodeI)->edge1 == (*nodeNextI)->edge2
                        || (*nodeI)->edge2 == (*nodeNextI)->edge1)
                {
                    if (dist < distBest)
                    {
                        distBest = dist;
                        from = (*nodeI).get();
                        to = (*nodeNextI).get();
                    }
                }
            }
            else
            {
                // check
                if (dist < distBest)
                {
                    distBest = dist;
                    from = (*nodeI).get();
                    to = (*nodeNextI).get();
                }
            }
        }

        ///     * connect c.point to the closest node pn
        if (clu->point->edge1 != nullptr
                || clu->point->edge2 != nullptr)
        {
            // check if redirection is necessary.
            Node* altN = (std::max)(clu->point->edge1, clu->point->edge2);
            Node* startN = altN;
            if (startN->edge1 != clu->point.get())
                startN = startN->edge1;
            else if (startN->edge2 != clu->point.get())
                startN = startN->edge2;
            assert(startN != nullptr);
            Node* closestN;
            Node* closestOppoN;
            if (m_para.distanceFnc(from->point, altN->point) <
                    m_para.distanceFnc(to->point, altN->point))
            {
                closestN = from;
                closestOppoN = to;
            }
            else
            {
                closestN = to;
                closestOppoN = from;
            }
            double distWay1 = m_para.distanceFnc(startN->point, altN->point)
                    + m_para.distanceFnc(altN->point, clu->point->point)
                    + m_para.distanceFnc(clu->point->point, closestN->point)
                    + m_para.distanceFnc(closestN->point, closestOppoN->point);
            double distWay2 = m_para.distanceFnc(startN->point, altN->point)
                    + m_para.distanceFnc(altN->point, closestN->point)
                    + m_para.distanceFnc(closestN->point, clu->point->point)
                    + m_para.distanceFnc(clu->point->point, closestOppoN->point);
            if (distWay1 < distWay2)
            {
                from = altN;
                to = closestN;
            }
        }

        shared_ptr<Node> fromSP(from, no_op_delete());
        shared_ptr<Node> toSP(to, no_op_delete());

        _updateHullOrUpdateSimple(fromSP, clu->point, hull);
        _updateHullOrUpdateSimple(toSP, clu->point, hull);
    }
}


void VoronoiConvexAlignment::_updateNodes(map<double, Cluster*> &clustersSorted, NodeL &hull)
{
    for (auto &cluE : clustersSorted)
    {
        Cluster *clu = cluE.second;
        ///     * connect c.point to the closest node pn, if pn does not have already two edge1, edge2 nodes.
        for (auto &clN : clu->closestN)
        {
            // check if update allowed
            if (_updateAllowed(clN, clu->point))
            {
                // update
                if (_updateHullOrUpdateSimple(clN, clu->point, hull))
                    break;
            }
        }
    }
}

void VoronoiConvexAlignment::alignToHull(std::map<double, Cluster*> &clusters, NodeL &hull)
{
    for (const auto &cluE : clusters)
    {
        Cluster* clu = const_cast<Cluster*>(cluE.second);
        if (!clu->point->isConnectedToHull)
        {
            // search hull or end
            Node *node = clu->point.get();
            Node *prev = node->edge2;
            Node *foundNode = nullptr;
            auto foundHullI = hull.end();
            auto hullI = hull.end();
            while (true)
            {
                while (node != nullptr)
                {
                    node->isConnectedToHull = true;
                    hullI = find_if(hull.begin(), hull.end(),
                            [node] (const NodeL::value_type &val)
                    {
                        return val->point == node->point;
                    });
                    if (hullI != hull.end())
                    {
                        break;
                    }
                    node = _nextNode(node, prev);
                }
                if (node == nullptr)
                {       // end without hull node
                    if (prev != nullptr)
                    {       // because there is only one missing edge to the hull for each navel.
                        foundNode = prev;
                        if (foundHullI != hull.end())
                        {
                            // search closest hull
                            auto hullPrevI = foundHullI;
                            if (hullPrevI == hull.begin())
                                hullPrevI = hull.end();
                            --hullPrevI;
                            double distPrev = m_para.distanceFnc(foundNode->point, (*hullPrevI)->point);
                            auto hullNextI = foundHullI;
                            ++hullNextI;
                            if (hullNextI == hull.end())
                                hullNextI = hull.begin();
                            double distNext = m_para.distanceFnc(foundNode->point, (*hullNextI)->point);
                            Node *hullN = nullptr;
                            if (distPrev < distNext)
                            {
                                hullN = (*hullPrevI).get();
                            }
                            else
                            {
                                hullN = (*hullNextI).get();
                            }

                            // update
                            if (foundNode->edge1 == nullptr)
                                foundNode->edge1 = hullN;
                            else if (foundNode->edge2 == nullptr)
                                foundNode->edge2 = hullN;
                            else assert(false);

                            if (hullN->edge1 == nullptr)
                                hullN->edge1 = foundNode;
                            else if (hullN->edge2 == nullptr)
                                hullN->edge2 = foundNode;
                            else assert(false);

                            break;
                        }
                        else
                        {
                            node = clu->point.get();
                            prev = node->edge1;
                        }
                    } else assert(false);
                }
                else
                {       // end with hull node
                    // test into other direction
                    if (foundHullI != hull.end())
                        break;      // because both ends are already connected to hull.
                    foundHullI = hullI;
                    node = clu->point.get();
                    prev = node->edge1;
                }
            }
        }
    }
}

void VoronoiConvexAlignment::_insertNewNodeToEdge(Way* way, Node *fromNode, Node *insertNode)
{
    /// HINT: prev and next nodes of xN are in nodeList.
    /// TODO [xeboeh] delete childNode - memory leak here.
    // we detect cases, where it is no child node - selectNavel insert innerNodes size 1.
//    TODO .... delete if (cx.insertNode->isChildNode)
//        cx.insertNode->child->node = cx.insertNode;     // update old node --> delete node pointer.
    insertNode->child = nullptr;     // HINT: insertChild expect child == nullptr if new. Else we assume not new!
    insertNode->isChildNode = false;            // will be re-set on insertChild().
    insertNode->isInWay = true;

    if (insertNode->point == Point(64,41)
            //&& neighClu->point->point == Point(44,61)
            //&& nextFrom0->point == Point(95,7)
            )
        int i=5;


    /// 2. Insert xN;
    if (fromNode->child == nullptr || fromNode->isChildNode == false)     // isChildNode == false, for test61
    {       // convex-hull node
        way->insertChild(Way::iterator(fromNode), insertNode);
    } else {
        way->insertChildInNext(fromNode, insertNode);
    }
    insertNode->isDeleted = false;

    /// align partners first to new edges
//    _alignPartners(way, fromNode, insertNode);
//    _alignPartners(way, insertNode, Way::nextChild(insertNode));
}


void VoronoiConvexAlignment::_insertAsNeighbour(Node *node, Cluster *clu)
{
    Node *next = const_cast<Node*>(Way::nextChild(node));
    clu->neighbourEdges.insert({
                seboehDist(next, node, clu->point.get())
                , tuple<Node*,Node*>(next, node)
                               });
    Node *prev = const_cast<Node*>(Way::prevChild(node));
    clu->neighbourEdges.insert({
                seboehDist(prev, node, clu->point.get())
                , tuple<Node*,Node*>(prev, node)
                               });
}


// dead code
double VoronoiConvexAlignment::_getBestAlignEdge(Node *partnerN, Node *cluN, Cluster *clu)
{
    double bestAlign = 0;
    for (auto &edge : clu->neighbourEdges)
    {
        Node *from = get<0>(edge.second);
        Node *to = get<1>(edge.second);
        if (from != cluN && to != cluN
            && from != partnerN && to != partnerN)
        {
            bestAlign = edge.first;
            break;
        }
    }
    return bestAlign;
}


// VoronoiConvexAlignment
void VoronoiConvexAlignment::_collectClusterEdges(map<double, Cluster*> &clustersM)
{
//    // add hull
//    Node *prev = hull.back().get();
//    for (auto &sp : hull)
//    {
//        Node *next = sp.get();
//        double bestDist = numeric_limits<double>::max();
//        Cluster *bestClu = nullptr;
//        for (auto &cluEl : clustersM)
//        {
//            Cluster *clu = cluEl.second;
//            double dist = seboehDist(prev, next, clu->point.get());
//            if (dist < bestDist)
//            {
//                bestDist = dist;
//                bestClu = clu;
//            }
//        }
//        m_edgeToClusters[prev][next].clu1 = bestClu;
//        m_edgeToClusters[prev][next].dist1 = bestDist;
//        m_edgeToClusters[prev][next].edge.from = prev;
//        m_edgeToClusters[prev][next].edge.to = next;
//        prev = next;
//    }


    // add clusters
    for (auto &cluEl : clustersM)
    {
        Cluster* clu = cluEl.second;
        Node *prev = clu->closestTestedPointsSorted.back();
        for (Node *next : clu->closestTestedPointsSorted)
        {
            double dist = seboehDist(prev, next, clu->point.get());
            if (dist < m_edgeToClusters[prev][next].dist1)
            {
                m_edgeToClusters[prev][next].clu1 = clu;
                if (m_edgeToClusters[prev][next].dist1 == std::numeric_limits<double>::max())
                {       // init
                    m_edgeToClusters[prev][next].edge.from = prev;
                    m_edgeToClusters[prev][next].edge.to = next;
                }
                m_edgeToClusters[prev][next].dist1 = dist;
            }
            else if (dist < m_edgeToClusters[prev][next].dist2)
            {
                m_edgeToClusters[prev][next].clu2 = clu;
                if (m_edgeToClusters[prev][next].dist2 == std::numeric_limits<double>::max())
                {       // init
                    m_edgeToClusters[prev][next].edge.from = prev;
                    m_edgeToClusters[prev][next].edge.to = next;
                }
                m_edgeToClusters[prev][next].dist2 = dist;
            }

            prev = next;
        }
    }
}


VoronoiConvexAlignment::ClusterEdge * VoronoiConvexAlignment::_findClusterToEdge(Node *from, Node *to)
{
    int i=0;
    for (; i < 2; ++i)
    {
        auto iterEdge = m_edgeToClusters.find(from);
        if (iterEdge != m_edgeToClusters.end())
        {
            auto iterEdge2 = iterEdge->second.find(to);
            if (iterEdge2 != iterEdge->second.end())
            {
                return &iterEdge2->second;
            }
        }
        swap(from,to);
    }
    return nullptr;
}



// VoronoiConvexAlignment
void VoronoiConvexAlignment::_insertCluEIfPossible(ClusterEdge *cluE, OpenClustersAtEdges &openN)
{
    if (cluE != nullptr
            && cluE->clu1 != nullptr
            && (cluE->clu1->state != 1
                || (cluE->clu2 != nullptr
                    && cluE->clu2->state != 1)))
    {
        ///         * if not visited, insert to openN, each
        double dist;
        if (cluE->clu1->state != 1)
        {       // dist1 is always smaller than dist2
            dist = cluE->dist1;
        } else {
            dist = cluE->dist2;
        }
        auto iter = openN.find(dist);
        if (iter != openN.end())
            dist = (dist + (++iter)->first)/2.0;       // M20190111

       openN.insert({dist, cluE});
    }
}

bool VoronoiConvexAlignment::_searchBestPathByConvexHullAlignment(OpenClustersAtEdges &open)
{
    /// * cancel criteria
    if (open.empty())
    {
        ///     * add p to way
        ///     * check way-length and update way
        double length = 0;
        Node *prev = m_currentWay.back();
        for (Node *next : m_currentWay)
        {
            length += m_para.distanceFnc(prev->point, next->point);
            prev = next;
        }
        if (length < m_bestLength)
        {
            m_bestLength = length;
            m_bestWay = m_currentWay;
        }
        return true;
    }

    // Version 2
    bool isLast = false;
    ///     * copy map to openN and delete the element p
    OpenClustersAtEdges openN = open;
    auto openEI = openN.begin();
    Cluster *p = nullptr;
    ClusterEdge *cluEdge = nullptr;
    while (p == nullptr)
    {
        cluEdge = openEI->second;

        if (cluEdge->clu1->state != 1)
            p = cluEdge->clu1;
        else if (cluEdge->clu2 != nullptr && cluEdge->clu2->state != 1)
            p = cluEdge->clu2;
        // TODO [seb] what is with n-closest point?

        openEI = openN.erase(openEI);       // first is unique because M20190111
        if (openEI == openN.end())
            break;
    }
    if (p == nullptr)
        _searchBestPathByConvexHullAlignment(openN);        // because, final cancel-criteria

    /// * go through all elements p of open
    // version 1
//    for (auto openEI = open.begin();
//         openEI != open.end(); ++openEI)
    // version 2
    if (p != nullptr)
    {
        ///     * mark p as visited
        p->state = 1;
        for (auto neighbourEl : p->neighbourEdges)
        {
            Node *from = std::get<0>(neighbourEl.second);
            Node *to = std::get<1>(neighbourEl.second);

            auto wayInsertedI = m_currentWay.end();
            ///     * add p to way
            if (p->point->point.x() == 64)
                int i=5;
            if (from->point.x() == 176 || from->point.x() == 126)
                int i=5;

            auto wayI = --m_currentWay.end();       // trick because edge from 'last' to 'first' node.
            if (*wayI != from)
            {
                wayI = m_currentWay.begin();
                for (; wayI != m_currentWay.end(); ++wayI)
                {
                        // both edges form/to below, because safety.
                    if (*wayI == from)
                    {
                        ++wayI;
                        if (wayI == m_currentWay.end())
                            wayI = m_currentWay.begin();
                        if (*wayI == to)
                            break;
                    }
                }
            }
            else
                wayI = m_currentWay.begin();
            if (wayI != m_currentWay.end())
            {
                wayInsertedI = m_currentWay.insert(wayI, p->point.get());
            } else
                continue;

            ///     * find clu1 clu2 of the two edges between cluEdge.edge and p.point
            ///     * check if clu1 or clu2 ar not visited already
            _insertCluEIfPossible(
                        _findClusterToEdge(
                            from, p->point.get())
                        , openN);
            _insertCluEIfPossible(
                        _findClusterToEdge(
                            p->point.get(), to)
                        , openN);

            ///     * start recursion again.
            isLast = _searchBestPathByConvexHullAlignment(openN);

            ///     * clear for recursion
            if (wayInsertedI != m_currentWay.end())
                m_currentWay.erase(wayInsertedI);
            if (isLast)      // last is always aligned at best edge
                break;
        }
        p->state = 0;
    }

    return false; //openEI == openN.end();
}


void VoronoiConvexAlignment::_runEvolutionaryGrow(std::map<double, Cluster*> clustersM, NodeL &hull)
{
    m_bestLength = std::numeric_limits<double>::max();
    m_bestWay.clear();
    m_currentLength = std::numeric_limits<double>::max();
    m_currentWay.clear();

    // * create openEdges list with alignments to hull
    OpenClustersAtEdges open;

    Node *prev = hull.back().get();
    for (auto sp : hull)
    {
        Node *next = sp.get();
        m_currentWay.push_back(next);

        ClusterEdge *cluE = _findClusterToEdge(prev, next);
        if (cluE != nullptr)
        {
            double dist = cluE->dist1;
            auto iter = open.find(dist);
            if (iter != open.end())
                dist = (dist + (++iter)->first)/2.0;       // M20190111
            open.insert({dist, cluE});
        }
        prev = next;
    }

    // * start recursive search (similar to A*-search)
    _searchBestPathByConvexHullAlignment(open);
}



VoronoiConvexAlignment::NodeL VoronoiConvexAlignment::determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way)
{
    assert(way->size() > 1);

    /// * create structure
    map<double, Cluster*> clustersM;
    for (auto &cluE : clustersA)
    {
        Cluster *clu = cluE.get();

        /// * pre-calc cluster alignments at neighbours
        auto neighNextI = clu->closestTestedPointsSorted.begin();
        for (Node* &neigh : clu->closestTestedPointsSorted)
        {
            ++neighNextI;
            if (neighNextI == clu->closestTestedPointsSorted.end())
                neighNextI = clu->closestTestedPointsSorted.begin();
            double dist = seboehDist(neigh, *neighNextI, clu->point.get());
            clu->neighbourEdges.insert({dist, {neigh, *neighNextI}});
        }

        clustersM.insert({clu->area, clu});
    }
    size_t growPos = 0;
    for (auto &cluE : clustersM)
    {
        cluE.second->growPosition = ++growPos;
    }

    for (auto &cluE : clustersM)
    {
        Cluster *clu = cluE.second;
        for (const Node * node : clu->closestTestedPointsSorted)
        {
            auto cluI = find_if(clustersM.begin(), clustersM.end(),
                                [node] (const map<double, Cluster*>::value_type &val)
            {
                return node == val.second->point.get();
            });
            if (cluI != clustersM.end())
                clu->closestSortedNeighbours.push_back(cluI->second);
        }
    }

    /// * create structure
    _collectClusterEdges(clustersM);

    /// * align to best edges starting from hull
    _runEvolutionaryGrow(clustersM, hull);

    /// * create result-struct
    NodeL ret;
    for (Node *node : m_bestWay)
        ret.push_back(make_shared<Node>(*node));

    return ret;
}

void VoronoiConvexAlignment::buildTree(DT::DTNode::NodeType node, float level, TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    m_resultLength = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    incProgress(progressPercentage);

    // * intern Input representation
    auto input = createInputRepresentation(openPointsBasic);
    m_tspSize = input.size();

    // * determine hull
    auto res = determineHull(input);
    auto hull = get<0>(res);
    Way *way = get<1>(res);

    // * determine Voronoi cluster
    auto clusters = determineClusters(input, hull);

    // * build final path
    auto finalWay = determineWay(clusters, hull, way);

//    if (m_isCanceled)
//    {
        // set any way - like the input, because we want valid m_resultLength values at further processing.
//        m_shortestWay.clear();
//        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
//             openNodeIter != openPointsBasic->end(); ++openNodeIter)
//        {
//            m_shortestWay.push_back(**openNodeIter);
//        }
//        m_mutex.lock();
//        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
//        m_mutex.unlock();
//    }

    /// copy result to final output
    for (auto pointI = finalWay.begin();
         pointI != finalWay.end(); ++pointI)
    {
        TSPWay w;
        w.push_back((*pointI)->point);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial ways - drop this tree of ways.
        (*newChildIter)->parent(node);
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}
