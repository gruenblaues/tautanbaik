#include "TamarDebugging.h"

// methods used for debugging in TamarAlgorithm
using namespace TSP;

std::ofstream TamarDebugging::m_debugFile;

#define pointAX1 0
#define pointAX2 0

#define pointCX 567

#define pointDX 471


static void breakpoint()
{
    int i=5;
}

TamarDebugging *TamarDebugging::m_this = nullptr;

TamarDebugging::TamarDebugging()
{
#ifndef NDEBUG
    // for some debugging
    m_debugFile.open("debugOutput.txt", std::ios_base::out);
#endif
}

void TamarDebugging::printDebugFile(TamarAlgorithm::Way *hull, TamarAlgorithm::Way *innerPoints, Point &outerHullNavel, Point &innerPointsNavel)
{
#ifndef NDEBUG
    if (!m_debugFile.is_open())
        return;

    m_debugFile << "<entry>" << std::endl;
    m_debugFile << "\t<hull>" << std::endl;
    m_debugFile << "\t\t";
    auto startNode = &(hull->begin());
    const TamarAlgorithm::Node* node = startNode;
    if (startNode == nullptr)
        return;
    m_debugFile << node->point.x() << "x" << node->point.y();
    while ((node = TamarAlgorithm::Way::nextChild(node)) != startNode)
    {
        m_debugFile << ";" << node->point.x() << "x" << node->point.y();
    }
    m_debugFile << std::endl << "\t</hull>" << std::endl;
    if (innerPoints != nullptr)
    {
        m_debugFile << "\t<innerPoints>" << std::endl;
        m_debugFile << "\t\t";
        node = startNode = &(innerPoints->begin());
        if (startNode == nullptr)
            return;
        m_debugFile << node->point.x() << "x" << node->point.y();
        while ((node = TamarAlgorithm::Way::nextChild(node)) != startNode)
        {
            m_debugFile << ";" << node->point.x() << "x" << node->point.y();
        }
        m_debugFile << std::endl << "\t</innerPoints>" << std::endl;
    }
    m_debugFile << "\t<hullPos> " << outerHullNavel.x() << "x" << outerHullNavel.y() << " </hullPos>" << std::endl;
    m_debugFile << "\t<innerPos> " << innerPointsNavel.x() << "x" << innerPointsNavel.y() << " </innerPos>" << std::endl;
    m_debugFile << "</entry>" << std::endl;
#endif
}

void TamarDebugging::debugApp2016_print(TamarAlgorithm::NavelsMapType &navels, Parameters &para, Indicators &indicators)
{
#ifndef NDEBUG
    if (para.isRun != nullptr && *para.isRun)
    {
        std::list<std::list<TamarAlgorithm::ClosestEdges>> navelExtracted;
        for (const auto &navel : navels)
            navelExtracted.push_back(navel.second);
        indicators.mNavels = navelExtracted;
        *para.isRun = false;
        while (!*para.isRun) ;
    }       // else, it is not the debug app 2016
#endif
}

void TamarDebugging::debugApp2016_print2(TamarAlgorithm::NavelsType &navelsCopy, Parameters &para, Indicators &indicators)
{
#ifndef NDEBUG
    TamarAlgorithm::NavelsMapType navels;
    for (const auto &nav : navelsCopy)
    {
        if (!nav.navelCopy.empty())
            navels.insert({nav.navelCopy.front().fromNode, nav.navelCopy});
    }
    if (para.isRun != nullptr && *para.isRun)
    {
        std::list<std::list<TamarAlgorithm::ClosestEdges>> navelExtracted;
        for (const auto &navel : navels)
            navelExtracted.push_back(navel.second);
        indicators.mNavels = navelExtracted;
        *para.isRun = false;
        while (!*para.isRun) ;
    }       // else, it is not the debug app 2016
#endif
}

void TamarDebugging::debugApp2016_printWait(TamarAlgorithm::NavelsMapType &navels, Parameters &para, Indicators &indicators)
{
#ifndef NDEBUG
    if (para.isRun != nullptr && *para.isRun)
    {
        std::list<TamarAlgorithm::NavelsZippedType> zip;
        for (auto &root : navels)
        {
            zip.push_back(root.second);
        }
        indicators.mNavels = zip;
        *para.isRun = false;
        while (!*para.isRun) ;
    }
#endif
}


void TamarDebugging::debugIsInNavels(TamarAlgorithm::NavelsMapType &navels)
{
#ifndef NDEBUG
    for (const auto &navelRoot : navels)
    {
        for (const auto &ce : navelRoot.second)
        {
            if (ce.fromNode->point.x() == 360)
                breakpoint();
        }
    }
#endif
}

void TamarDebugging::debugIsInNavels(TamarAlgorithm::NavelsMapType::mapped_type &navels)
{
#ifndef NDEBUG
    for (const auto &ce : navels)
    {
        if (ce.fromNode->point.x() == 473)
            breakpoint();
    }
#endif
}

void TamarDebugging::debugFindWrongOrder(TamarAlgorithm::NavelsType &navels)
{
#ifndef NDEBUG
    for (const auto &navel : navels)
    {
        // E.g.
        //        if (navel.navelCopy.size() == 2
        //                && navel.navelCopy.back().insertNode == reinterpret_cast<Node*>(0x7fffd0008f50)
        //                && navel.navelCopy.back().fromNode == reinterpret_cast<Node*>(0x7fffd0003680))
        //            breakpoint();
        if (navel.navelCopy.front().fromNode == navel.navelCopy.front().insertNode)
            breakpoint();
    }
#endif
}
