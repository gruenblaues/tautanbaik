#-------------------------------------------------
#
# Project created by QtCreator 2015-06-23T23:18:38
#
#-------------------------------------------------

QT       -= gui

TARGET = TspSeboeh
TEMPLATE = lib

CCFLAG += --std=c++11
CONFIG += c++11

CCFLAG += --stack,1073741824

#linux qt include path
#INCLUDEPATH += /opt/Qt/5.9/gcc_64/include/
#windows qt include path
#INCLUDEPATH +=
INCLUDEPATH += /opt/Qt/currentVersion/gcc_64/include/

INCLUDEPATH += ../lib/boost_1_58_0/

unix {
INCLUDEPATH += $(libpng-config --cflags)
}
windows {
INCLUDEPATH += ../lib/libpng
}

unix {
LIBS += -L/usr/lib/x86_64-linux-gnu/ `libpng-config --ldflags`
}
windows {
#LIBS += -LC:\MinGW\lib\ -lpng -lz -lm # does not work
LIBS += -L../trunk/lib/libpng -lwin_png -lz -lm
}

unix {
#LIBS += -L/opt/Qt/5.9/gcc_64/lib
LIBS += -L/opt/Qt/currentVersion/gcc_64/lib
}

SOURCES += Parameters.cpp \
    BaseFncs.cpp \
    BellmannHeldKarpAlgo.cpp \
    BellmannHeldKarpInternet.cpp \
    HierarchicalBreadthSplitting.cpp \
            TspFacade.cpp \
            PngImage.cpp \
            TamarAlgorithm.cpp \
            TspAlgoCommon.cpp \
            TspAlgoNaiveIterative.cpp \
            TspAlgoSeboeh.cpp \
            Way.cpp \
            bspTreeVersion/BspNode.cpp \
            bspTreeVersion/BspTreeSarah.cpp \
            dataStructures/OlavList.cpp \
			TamarDebugging.cpp \
    ClosestSeboehDist.cpp \
    KnapSackSolver.cpp \
    TamarAlgorithmHelper.cpp \
    SortedMerge.cpp \
    exponentsearch.cpp \
    ExcludingMerge.cpp \
    VoronoiSort.cpp \
    VoronoiClusterSort.cpp \
    dataStructures/PolygonWay.cpp \
    VoronoiClusterDescent.cpp \
    VoronoiAreaPairGrow.cpp \
    VoronoiConvexAlignment.cpp \
    MinMaxClusterAlignment.cpp \
    DetourClusterAlignment.cpp

HEADERS += dll.h \
    BaseFncs.h \
    BellmannHeldKarpAlgo.h \
    BellmannHeldKarpInternet.h \
    HierarchicalBreadthSplitting.h \
            Parameters.h \
            Point.h \
            Way.h \
            Indicators.h \
            PngImage.h \
            Result.h \
            TspAlgoCommon.h \
            TspAlgoNaiveIterative.h \
            TspAlgoSeboeh.h \
            TamarAlgorithm.h \
            DTNode.h \
            DTTree.h \
            bspTreeVersion/BspNode.h \
            bspTreeVersion/BspTree.h \
            bspTreeVersion/BspTreeSarah.h \
            dataStructures/OlavList.h \
			TamarDebugging.h \
    ClosestSeboehDist.h \
    KnapSackSolver.h \
    TamarAlgorithmHelper.h \
    SortedMerge.h \
    dataStructures/commonstructures.h \
    exponentsearch.h \
    ExcludingMerge.h \
    VoronoiSort.h \
    VoronoiClusterSort.h \
    dataStructures/PolygonWay.h \
    TspFacade.h \
    VoronoiClusterDescent.h \
    VoronoiAreaPairGrow.h \
    VoronoiConvexAlignment.h \
    MinMaxClusterAlignment.h \
    DetourClusterAlignment.h



unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    code-snipets.txt \
    debugging.inc \
    VoronoiAreaPairGrow_info.txt
