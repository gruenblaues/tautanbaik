#pragma once

#include <vector>
#include <chrono>

#include "dataStructures/OlavList.h"
#include "bspTreeVersion/BspNode.h"
#include "dataStructures/commonstructures.h"

namespace TSP {

class SortedMerge
{
public:
    SortedMerge();
    ~SortedMerge();

    /**
      * Types
      */
    using Node = oko::Node;
    using Way = oko::OlavList;
    using ClosestEdges = oko::ClosestEdges;
    using input_value = std::list<std::shared_ptr<ClosestEdges>>;
    using Result = comdat::Result;
    typedef Result output_value;
    struct Edge
    {
        oko::Node *fromNode = nullptr;
        oko::Node *toNode = nullptr;
    };
    using InputCitiesPointerType = std::list<Point*>;

    struct Variations
    {
        struct Entry
        {
            Entry(Node* n, size_t p) : node(n), altPos(p) {}
            enum class State { UNCHANGED = 0, ALIGNED = 1 };
            State state = State::UNCHANGED;
            Node *node = nullptr;
            size_t altPos = 0;
        };

        std::list<Node*> next();

        std::list<Entry> variations;

        bool             m_stateFinished = false;
    };

    /// fim
    struct FimVariations
    {
        using FimVariationContainerType = std::list<std::list<Node*>>;
        FimVariationContainerType variations;
        FimVariationContainerType::iterator *m_fimVariationsI = nullptr;

        std::list<Node*> next();

        void _calcBorderVariations_fastImperfect(std::list<oko::Node*> &innerPointsNodes);
        void _fimRecursiveVariationCreation(std::list<Node*> &used, std::list<Node*> open, std::list<Node*>::iterator hullCurrentI, size_t crossedCount);
    };
    static std::list<Node*> m_fimHullNodes;
    static size_t m_fimCrossedMax;
    static double m_fimMaxInnerLength;
    bool m_fimSimpleAlignment = false;

    /**
     * public methods
     */

    std::tuple<output_value, double> search(const std::list<Node *> &fromNodes, input_value &innerPoints);

    std::tuple<SortedMerge::output_value, double> search_V2(const std::list<Node *> &fromNodes, Way &innerPointWay);

    /**
      * attributes
      */
    bool m_isCanceled = false;
    std::chrono::system_clock::time_point m_startTime;

private:
    struct GroupFromNodeEntry
    {
        Node *from = nullptr;
        double dist = std::numeric_limits<double>::max();
        std::list<Node*> nodeOrder;
    };

    using GroupFromNodeType = std::map<double, GroupFromNodeEntry>;

    using GroupFromNodeTypePointer = std::shared_ptr<GroupFromNodeType>;

    struct Sample
    {
        GroupFromNodeTypePointer froms;
        Node *lastFrom = nullptr;
        std::list<Node*> innerNodes;
        Node *firstNode = nullptr;
    };
    typedef std::list<std::shared_ptr<Sample>> TamarGroupMapperType;

    Result m_result;
    size_t m_countOfNodes = 0;
    using SampleType = std::multimap<Node*,Sample>;
    SampleType m_samples;      ///< key=first-node in innerNodes, value=container for innerNodes and froms.
    InputCitiesPointerType m_points;

    std::vector<size_t> m_varity;

    // search tree
    struct SimpleSample
    {
        std::list<Node*> innerNodes;
        std::list<Node*> nodeOrder;
        Node *bestFrom = nullptr;
        double bestDist = std::numeric_limits<double>::max();
        Node *firstNode = nullptr;
    };

    using SearchTreeType = std::list<SimpleSample>;                 /// list of tuples/set of nodes
    using OpenTreeNodeType = std::list<Node*>;

    SearchTreeType m_bestResTotal;
    double m_bestDistTotal = std::numeric_limits<double>::max();

    /**
      * private methods
      */
private:
    /**
     * @brief SortedMerge::_createContent used internally to calculate all distances to the hull for each inner-Node.
     * @param fromNodes [in] are the from Nodes according to the outer hull.
     * @param innerPointWay [in] are the inner Nodes, which shall be aligned.
     * @return true on success, else the input was not ok.
     */
    std::list<Node*> _createContent(const std::list<oko::Node *> &fromNodes, std::list<oko::Node *> &innerPointsNodes);

    SearchTreeType &_setBestAndCalcDist(SearchTreeType tree);

    SampleType::iterator _findSample(SimpleSample &el);

    double _calcDistAccordingToTree(SearchTreeType &tree);

    SortedMerge::SearchTreeType _searchRecursive(SearchTreeType tree, OpenTreeNodeType openNodes, size_t level);

    void _resetVarity(size_t elCount);

    std::tuple<std::vector<size_t>, bool> _createNextVarity();

    std::tuple<Node *, double, std::list<Node *> > _findBestFrom(std::list<Node *> froms, std::list<Node *> set);

    bool _checkPropertyVaryFit(const std::vector<size_t> &vary, Way &innerPointWay);

    std::tuple<double, std::list<Node *> > _findBestFrom(Node *fromNode, Node *toNode, std::list<Node *> set);

    void _insertToCeList(const Node *node, std::list<ClosestEdges> &ceList, const Edge &edge);

    void _updateValue(const Edge &edge, const std::list<Node *> &newOrder, GroupFromNodeType::mapped_type &value);

    std::list<oko::Node *> _prepareHeuristicalOrder(input_value &innerPoints);
    std::list<oko::Node *> _prepareHeuristicalOrder_old2(input_value &innerPoints);
    std::list<oko::Node *> _prepareHeuristicalOrder_old(input_value &innerPoints);

    std::list<oko::Node *>::const_iterator _prepareCalcGravity(const Node *elected, const std::list<Node *> &newNodes);

    std::list<oko::Node *> _prepareWithGravityRecalculations(std::list<Node *> &newNodes);

    void _insertToNodeList(std::list<Node*> nodeListB, std::list<Node*> insertAtoB);

    void _prepareGetBestNode(double &distBest, const Node* electedNode, std::list<Node *> &nodeListA, std::list<Node*>::iterator &bestNode);

    void _prepareInsertToNewList(std::list<Node *> &nodeListAlt, std::list<Node *> &nodeListNew, std::list<Node*>::iterator bestNodeNewI, Node *electedNode);

    Variations _calcBorderVariations(std::list<oko::Node *> &innerPointsNodes);

    std::map<double, std::list<Node *> > _calcSpecialOtherBorder(Node *from, Node *to, std::list<Node *> nodes);

    double _calcTriLength(const Point &pA, const Point &pB, const Point &center, const Point &current);

    std::tuple<bool, size_t, double, double> _featureClosestAlign(std::list<oko::Node *> &innerNodes, oko::Node *nodeCurr, oko::Node *nodePrev, oko::Node *nodeNext);

    void _gapsCalculate(std::vector<size_t> &currentGapsVect);

    void _gapsNextRecursion(std::list<Node*> &nodes, SearchTreeType &tree, SearchTreeType::iterator &treeBiggestI, std::vector<size_t> &in);

    void _gapsCalculate(std::list<Node *> &nodes, SearchTreeType &tree, SearchTreeType::iterator &treeBiggestI, std::vector<size_t> &currentGapsVect);

    SearchTreeType::iterator _gapsFindBiggestList(SearchTreeType &tree);

    void _gaps(SearchTreeType &tree);

    bool _gapsCheckOnValid(std::vector<size_t> &in, SearchTreeType &tree, std::list<Node*> &nodes, SearchTreeType::iterator &treeBiggestI);

};

}
