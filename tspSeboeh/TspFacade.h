#pragma once

#include <list>
#include <map>

#include "Parameters.h"
#include "Indicators.h"
#include "Result.h"
#include "TspAlgorithm.h"
#include "TspAlgoSeboeh.h"
#include "TspAlgoNaiveIterative.h"
#include "TamarAlgorithm.h"
#include "VoronoiSort.h"
#include "VoronoiClusterSort.h"
#include "VoronoiClusterDescent.h"
#include "ClosestSeboehDist.h"
#include "VoronoiAreaPairGrow.h"
#include "VoronoiConvexAlignment.h"
#include "MinMaxClusterAlignment.h"
#include "DetourClusterAlignment.h"
#include "BellmannHeldKarpAlgo.h"
#include "BellmannHeldKarpInternet.h"
#include "HierarchicalBreadthSplitting.h"
#include "HierarchicalClusteredAlignment.h"
#include "DTTree.h"

// TODO [seboeh]: This include and all depending (m_resultWay) will corrupt the principle of a facade.
#include "Way.h"

#include <QStringList>

#include "dll.h"

namespace TSP {

class EXTERN TSPFacade
{
public:

    typedef std::pair<float, float> PointListValueType;
    typedef std::list<PointListValueType > PointListType;

	/// To guaranty a correct new thread, this facade have to be recreated with new after finishing.
	TSPFacade(Parameters::AlgorithmType algoType) {
        switch (algoType) {
        case Parameters::AlgorithmType::Ackermann :
            algo = new TSPAlgoSeboeh();
            break;
        case Parameters::AlgorithmType::Naive :
            algo = new TspAlgoNaiveIterative();
            break;
        case Parameters::AlgorithmType::Tamar :
            algo = new TamarAlgorithm();
            break;
        case Parameters::AlgorithmType::TamarExp :
            algo = new TamarAlgorithm();
            break;
        case Parameters::AlgorithmType::SeboehDistAlg :
            algo = new ClosestSeboehDistAlgorithm();
            break;
        case Parameters::AlgorithmType::VoronoiAreaSort :
            algo = new VoronoiSort();
            break;
        case Parameters::AlgorithmType::VoronoiClusterSort :
            algo = new VoronoiClusterSort();
            break;
        case Parameters::AlgorithmType::VoronoiClusterDescent :
            algo = new VoronoiClusterDescent();
            break;
        case Parameters::AlgorithmType::VoronoiAreaPairGrow :
            algo = new VoronoiAreaPairGrow();
            break;
        case Parameters::AlgorithmType::VoronoiConvexAlignment :
            algo = new VoronoiConvexAlignment();
            break;
        case Parameters::AlgorithmType::MinMaxClusterAlignment :
            algo = new MinMaxClusterAlignment();
            break;
        case Parameters::AlgorithmType::DetourClusterAlignment :
            algo = new DetourClusterAlignment();
            break;
        case Parameters::AlgorithmType::BellmannHeldKarp :
            algo = new BellmannHeldKarpAlgo();
            break;
        case Parameters::AlgorithmType::BellmannHeldKarpInternet :
            algo = new BellmannHeldKarpInternet();
            break;
        case Parameters::AlgorithmType::HierarchicalBreadthSplitting :
            algo = new HierarchicalBreadthSplitting();
            break;
        case Parameters::AlgorithmType::HierarchicalClusteredAlignment:
            algo = new HierarchicalClusteredAlignment();
            break;
        default:
            algo = new TSPAlgoSeboeh();
        }
        m_algoType = algoType;
    }

    ~TSPFacade() {
        delete algo;
		delete m_results;
    }

    /**
     * @brief used to parse simple input object to the algorithm.
     * @param points - cities in TSP. Not assumed to by negative, but would work. Can be infinite and 0.
     */
    void addInput(const PointListType points);

    /**
     * @brief getResultTypeWay has no conversion to a standard low-interfaced type.
     * @return the \see TSP::Way directly from the result.
     */
    TSP::Way    * getResultTypeWay();

    /**
     * @brief getResults - this is the high interface output of the algorithms.
     * @return all stored results in the algorithm.
     */
    TSP::Result * getResults();

    /**
     * @brief the main execution method. Starts the process in a parallel thread.
     * @param para
     */
    void startRun(Parameters &para);

    void stopRun();

    void pause();

    /**
     * @brief wait till process ends. Called to terminate the process-thread.
     */
	void wait(unsigned long milliseconds = ULONG_MAX);

	/**
	 * @brief used to determine if finished, when wait() is used.
	 */
	bool isFinished();

    /**
     * @brief Used to initialize the input parameter with valid values. This method helps the programmer to find a configuration.
     * @param inputPara - resulting parameter
     */
    void setDefaultParameters(Parameters * const inputPara);

    /**
     * @brief Gives the user the possibility to read the current status of the progress.
     * @return container of status information. Contains also a copy of an intermediate result.
     * When the status.progress is to 1, the algorithm has finished!
     */
    Indicators status();

    DT::DTTree * getDtTree() { return algo->getDtTree(); }

	void setBlockedEdges(std::list<std::pair<Point,Point>> &&edges)
	{
		assert(algo);
		algo->setBlockedEdges(std::move(edges));
	}

private:
	TspAlgorithm                   *algo = nullptr;
        TSPAlgoSeboeh::InputCitiesType  mCities;
        TSP::Way                        m_resultWay;
	Parameters::AlgorithmType       m_algoType;
	TSP::Result                     *m_results = nullptr;

};

}
