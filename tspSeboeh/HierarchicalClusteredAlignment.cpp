#include "HierarchicalClusteredAlignment.h"

#include <algorithm>
#include <tuple>
#include <math.h>
#ifndef M_PI
#define M_PI       3.14159265358979323846   // pi
#endif

#include <vector>

#include "bspTreeVersion/BspTreeSarah.h"
#include "Way.h"

#include <bspTreeVersion/BspNode.h>

#define SMALL_DOUBLE_STEP_MAX 0.0001
#define SMALL_DOUBLE_STEP_MIN 0.000001

using TSPWay = TSP::Way;

using namespace TSP;
using namespace std;
using namespace TSPHelper;

size_t HierarchicalClusteredAlignment::tspSize = 0;
double HierarchicalClusteredAlignment::tspWidth = 0;
double HierarchicalClusteredAlignment::tspHeight = 0;

HierarchicalClusteredAlignment::HierarchicalClusteredAlignment()
    : TspAlgoCommon()
{

}

HierarchicalClusteredAlignment::NodeL HierarchicalClusteredAlignment::createInputRepresentation(TspAlgorithm::InputCitiesPointerType* openPointsBasic)
{
    NodeL ret;

    double tspMaxWidth = 0;
    double tspMaxHeight = 0;
    double tspMinWidth = numeric_limits<double>::max();
    double tspMinHeight = numeric_limits<double>::max();
    for (const auto& point : *openPointsBasic)
    {
        ret.push_back(std::make_shared<Node>(*point));
        if (tspMaxWidth < point->x())
            tspMaxWidth = point->x();
        if (tspMinWidth > point->x())
            tspMinWidth = point->x();
        if (tspMaxHeight < point->y())
            tspMaxHeight = point->y();
        if (tspMinHeight > point->y())
            tspMinHeight = point->y();
    }
    tspWidth = tspMaxWidth - tspMinWidth;
    tspHeight = tspMaxHeight - tspMinHeight;

    return ret;
}

bsp::BspTreeSarah HierarchicalClusteredAlignment::_createBSPTree(NodeL& input)
{
    InputCitiesPointerType cities;
    for (const auto& node : input)
        cities.push_back(new TSP::Point(node->point));
    // HINT: above. better to use the nodes from input than new points.
    // because later search of hull-node necessary - see mark MARK_hullNodeSearch

    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cities);

    return bspTree;
}

std::tuple<HierarchicalClusteredAlignment::NodeL, HierarchicalClusteredAlignment::Way*> HierarchicalClusteredAlignment::determineHull(NodeL& input)
{
    NodeL ret;
    // * split input into quad-BSP-tree
    // * on returning from recursion merge to cyclic way (convex hull).

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = _createBSPTree(input);

    /// 2. create points order - merge
    //////////////////////////
    Way* way = nullptr;
    auto root = bspTree.root();
    if (root != nullptr) {
        way = _newConqueredWay(root);
        if (way != nullptr)
        {
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (Way::iterator valI = way->begin();
                !valI.end(); )
            {
                Node* node = valI.nextChild();
                node->isHull = true;
                ret.push_back(std::shared_ptr<Node>(node));
            }
            // TODO [seboeh] check if possible
            //            way->clear();
            //            delete way;
        }
    }

    // update input with new nodes from way
    Way::iterator wayI = way->begin();
    while (!wayI.end())
    {
        Node* node = wayI.nextChild();
        auto inputI = find_if(input.begin(), input.end(),
            [node](const NodeL::value_type& val)
            {
                return val->point == node->point;
            });
        assert(inputI != input.end());
        *inputI = shared_ptr<Node>(node, no_op_delete());
    }

    // * create resulting list
    return make_tuple(ret, way);
}


bool HierarchicalClusteredAlignment::_isClockwise(TSP::Point pCenter, Way* way)
{
    Way::iterator pI = way->begin();
    Way::iterator pbackupI = pI;
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((static_cast<double>(p1.x())* p2.y()) + ((-1.0 * p1.y()) * p2.x()));
    return dot >= 0 && ((dot / 90) != m_para.distanceFnc(*pbackupI, *pI));
}

void HierarchicalClusteredAlignment::_flipWay(Way* way, TSP::Point pCenter)
{
    if (!_isClockwise(pCenter, way))
        way->flipDirection();
}

Point HierarchicalClusteredAlignment::_correctNodeCenter(Way* wayBiggerVals, Way* WayLmallerVals, bsp::BspNode* node, MergeCallState state)
{
    Point::value_type valA = 0;
    auto wI = WayLmallerVals->begin();
    while (!wI.end())
    {
        valA = max(valA, state == MergeCallState::leftRight ? (*wI).x() : (*wI).y());
        ++wI;
    }
    float valB = numeric_limits<double>::max();
    wI = wayBiggerVals->begin();
    while (!wI.end())
    {
        valB = min(valB, state == MergeCallState::leftRight ? (*wI).x() : (*wI).y());
        ++wI;
    }
    Point mainCenter = node->m_center;
    if (state == MergeCallState::leftRight)
        mainCenter.set(mainCenter.x() + ((valB - valA) / 2.0), mainCenter.y());
    else
        mainCenter.set(mainCenter.x(), mainCenter.y() + ((valB - valA) / 2.0));

    return mainCenter;
}

HierarchicalClusteredAlignment::Way* HierarchicalClusteredAlignment::_newConqueredWay(bsp::BspNode* node)
{
    if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        Way* way = new Way();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
        //        way->setOrigin(node);
        return way;
    }
    Way* wayUpper;
    Way* wayLower;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        Way* wayUpperLeft = _newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            Way* wayUpperRight = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x() / 2.0, pUpperCenter.y() / 2.0);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            // see argument below Point mainCenter = _correctNodeCenter(wayUpperRight, wayUpperLeft, node, MergeCallState::leftRight);
            _mergeHull(wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        }
        else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    }
    else {
        if (node->m_upperRight) {
            wayUpper = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        }
        else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        Way* wayLowerLeft = _newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            Way* wayLowerRight = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x() / 2.0, pLowerCenter.y() / 2.0);

            // HINT: it is faster not to do this: Point mainCenter = _correctNodeCenter(wayLowerRight, wayLowerLeft, node, MergeCallState::leftRight);
            // instead using node->m_center
            _mergeHull(wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        }
        else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    }
    else {
        if (node->m_lowerRight) {
            wayLower = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        }
        else {
            wayLower = nullptr;
        }
    }

    //incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {
        // HINT: test37 needs master center node->m_center.
        // HINT: test2 needs flipWay
        _flipWay(wayLower, pLowerCenter);
        _flipWay(wayUpper, pUpperCenter);
        // Point mainCenter = _correctNodeCenter(wayLower, wayUpper, node, MergeCallState::topDown);
        _mergeHull(wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    }
    else if (wayUpper) {
        return wayUpper;
    }
    else if (wayLower) {
        return wayLower;
    }
    else {
        return nullptr;
    }
}

float HierarchicalClusteredAlignment::sign(const Point& p1, const Point& p2, const Point& p3)
{
    return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
}

bool HierarchicalClusteredAlignment::_isInsideTriangle(const Point& p1, const Point& p2, const Point& p3, const Point& inside)
{
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    if (p1 == inside || p2 == inside || p3 == inside)
        return false;

    // [https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle]
    Point pt = inside;
    Point v1 = p1;
    Point v2 = p2;
    Point v3 = p3;
    float d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(pt, v1, v2);
    d2 = sign(pt, v2, v3);
    d3 = sign(pt, v3, v1);

    has_neg = (d1 <= 0) || (d2 <= 0) || (d3 <= 0);      // also =0 --> point on line --> not inside (false)
    has_pos = (d1 >= 0) || (d2 >= 0) || (d3 >= 0);      // =0 --> on one line of triangle

    bool isInsideV1 = !(has_neg && has_pos);


    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    // barycentric coordinates.
    bool isInsideV2 = false;
    double determinant = ((p2.y() - p3.y()) * (p1.x() - p3.x()) + (p3.x() - p2.x()) * (p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y()) * (inside.x() - p3.x()) + (p3.x() - p2.x()) * (inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        isInsideV2 = false;
    double beta = ((p3.y() - p1.y()) * (inside.x() - p3.x()) + (p1.x() - p3.x()) * (inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        isInsideV2 = false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        isInsideV2 = false;

    // see test12 - V2 was wrong assert(isInsideV1 == isInsideV2);
    return isInsideV1;
}

void HierarchicalClusteredAlignment::_triangleRoofCorrection(std::list<EdgeIter>& directAccessEdges, Way::iterator& hpaI, Way::iterator& lpaI, Way::iterator& hpbI, Way::iterator& lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = _isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = _isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI = directAccessEdges.begin();
        edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
            && pb == *lpbI
            && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
            && pb == *hpbI
            && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

HierarchicalClusteredAlignment::DirectAccessState HierarchicalClusteredAlignment::_isDirectAccess(const TSP::Point& pMeanA, Way::iterator& pAI, const TSP::Point& pMeanB, Way::iterator& pBI)
{
    Way::iterator prevAI = pAI; --prevAI;
    Way::iterator nextAI = pAI; ++nextAI;
    Way::iterator prevBI = pBI; --prevBI;
    Way::iterator nextBI = pBI; ++nextBI;
    CrossedState state = isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if (state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if (state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if (state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if (state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool HierarchicalClusteredAlignment::_isNormalVectorLeft(const Point& from, const Point& to, const Point& insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
        , to.y() - from.y());
    //    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x()) / 2
        , (from.y() + to.y()) / 2);

    Point normalVectLeft(meanPoint.x() - straight.y()
        , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
        , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}


bool HierarchicalClusteredAlignment::_isCovered(Way* way, Way::iterator& w1I, Way::iterator& fixI)
{
    Way::iterator wI = way->begin();
    Way::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed
            || state == CrossedState::isCovered
            || state == CrossedState::isOnPoint)
            return true;
    }
    return false;
}

bool HierarchicalClusteredAlignment::_createTriangle2x2(Way* way1, Way* way2, Way::iterator& w1Inside, Way::iterator& w1Alt, Way::iterator& way2I, Way::iterator& way2NI)
{
    if (_isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        Way::iterator w1NewI = way1->push_back(&way2I);
        Way::iterator w1NNewI = way1->push_back(&way2NI);

        Node* nodePoint = (&w1Inside);
        if (dist1 < dist2 && dist1 < dist3)
        {
            nodePoint->prev = &w1Alt;
            nodePoint->next = (&w1Alt)->next;
            way1->insertChild(w1Alt, nodePoint);      // TODO [seboeh] is new really necessary?
        }
        else if (dist2 < dist1 && dist2 < dist3)
        {
            nodePoint->prev = &w1NNewI;
            nodePoint->next = (&w1NNewI)->next;
            way1->insertChild(w1NNewI, nodePoint);
        }
        else {
            nodePoint->prev = &w1NewI;
            nodePoint->next = (&w1NewI)->next;
            way1->insertChild(w1NewI, nodePoint);
        }
        return true;
    }
    return false;
}

void HierarchicalClusteredAlignment::_createTriangle3x1(Way* way1, Way::iterator& insideI, Way::iterator& fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    Way::iterator w1I = way1->begin();
    Way::iterator w1NI = w1I; ++w1NI;
    Way::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    Node* nodePoint = (&insideI);
    if (dist1 < dist2 && dist1 < dist3)
    {
        nodePoint->prev = &w1I;
        nodePoint->next = (&w1I)->next;
        way1->insertChild(w1I, nodePoint);
    }
    else if (dist2 < dist1 && dist2 < dist3)
    {
        nodePoint->prev = &w1NI;
        nodePoint->next = (&w1NI)->next;
        way1->insertChild(w1NI, nodePoint);
    }
    else {
        nodePoint->prev = &w1NNI;
        nodePoint->next = (&w1NNI)->next;
        way1->insertChild(w1NNI, nodePoint);
    }
}

void HierarchicalClusteredAlignment::_mergeHull(Way*& way1, const TSP::Point& pWay1Center, Way*& way2, const TSP::Point& pWay2Center, const MergeCallState& callState, const TSP::Point& pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        _flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        Way::iterator w1I = way1->begin();
        Way::iterator fixI = way2->begin();
        if (_isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            _flipWay(way1, pCenter);
            return;
        }
        else {
            ++w1I;
            if (_isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                _flipWay(way1, pCenter);
                return;
            }
            else {
                ++w1I;
                if (_isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    _flipWay(way1, pCenter);
                    return;
                }
                else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    Way::iterator w1NI = w1I; ++w1NI;
                    Way::iterator w1NNI = w1NI; ++w1NNI;
                    if (_isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        _createTriangle3x1(way1, w1NNI, fixI);
                    }
                    else if (_isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        _createTriangle3x1(way1, w1NI, fixI);
                    }
                    else {
                        _createTriangle3x1(way1, w1I, fixI);
                    }
                    _flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        Way::iterator way1I = way1->begin();
        Way::iterator way2I = way2->begin();
        Way::iterator way1NI = way1I;
        ++way1NI;
        Way::iterator way2NI = way2I;
        ++way2NI;
        Way::iterator way1Nearest;
        Way::iterator way2Nearest;
        double maxDist = std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        Way::iterator way1Next = way1Nearest; ++way1Next;
        Way::iterator way2Next = way2Nearest; ++way2Next;
        Point crossedPoint;
        auto crossedVal = isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next, &crossedPoint);
        if (crossedVal == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            _flipWay(way1, pCenter);
            return;
        }
        else if (crossedVal == CrossedState::isOnPoint)
        {       // triangle with inside point exactly on one triangle-side
            /// - determine way1 or 2 = wayX, contains crossedPoint
            auto wayI = way1->begin();
            Way* wayX = nullptr;
            Way* wayY = nullptr;
            if (*wayI == crossedPoint || *(++wayI) == crossedPoint)
            {
                wayX = way1;
                wayY = way2;
            }
            else
            {
                wayI = way2->begin();
                if (*wayI == crossedPoint || *(++wayI) == crossedPoint)
                {
                    wayX = way2;
                    wayY = way1;
                }
            }
            assert(wayX != nullptr && wayY != nullptr);
            auto wayCrossedI = wayI;
            auto wayNotCrossedI = ++wayI;
            /// - determine p_i from wayY is on one line with crossedPoint and other point (=xa) of wayX
            wayI = wayY->begin();
            auto wayNI = wayI; ++wayNI;
            Point meanP = (*wayI + *wayNI) / 2.0;
            Way::iterator wayPI;
            if (isCrossed(*wayNotCrossedI, *wayI, *wayCrossedI, meanP) == CrossedState::isOnPoint)
            {       // pi = wayI
                wayPI = wayI;
            }
            else
            {       // pi = ++wayI
                wayPI = ++wayI;
            }
            auto wayPNI = wayPI; ++wayPNI;
            /// - insert wayY before xa and after crossedPoint.
            (&wayNotCrossedI)->next = &wayCrossedI;
            (&wayNotCrossedI)->prev = &wayPNI;
            (&wayPNI)->next = &wayNotCrossedI;
            (&wayPNI)->prev = &wayPI;
            (&wayPI)->next = &wayPNI;
            (&wayPI)->prev = &wayCrossedI;
            (&wayCrossedI)->next = &wayPI;
            (&wayCrossedI)->prev = &wayNotCrossedI;

            if (way1 != wayX)
                swap(way1, way2);
            _flipWay(way1, pCenter);
            return;
        }
        else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (_createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            }
            else  if (_createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI))
            {
            }
            else if (_createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            }
            else if (_createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            }
            else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            _flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2

    /// beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (Way::iterator wAI = way1->begin();
        !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (Way::iterator wBI = way2->begin();
            !wBI.end(); ++wBI)
        {
            if ((*wAI).x() == 470)
                int i = 5;
            if ((*wBI).x() == 470)
                int i = 5;

            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (_isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    Way::iterator haI, hbI, laI, lbI;

    Point realCenter = (pWay1Center + pWay2Center) / 2.0;

    smallestDist /= 2;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    CrossedState oldState = CrossedState::isOutOfRange;
    Point oldCrossedPoint;
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI = directAccessEdges.begin();
        edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x()) - 10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x()) + 10, pCenter.y()), &crossedPoint);
        else
            state = isCrossed(pa, pb, TSP::Point(pCenter.x(), std::min(pa.y(), pb.y()) - 10), TSP::Point(pCenter.x(), (std::max)(pa.y(), pb.y()) + 10), &crossedPoint);

        //bool isLowNotHigh = _isNormalVectorLeft(pb, pa, realCenter);

        double value;

        if (pa.x() == 270)
            if (pb.x() == 550)
                int i = 5;
        if (pb.x() == 270)
            int i = 5;

        bool isGoodLow = false;
        bool isGoodHigh = false;
        if (state == CrossedState::isOnPoint)
        {       // see test82, test76
            // see test89, test64

            // test if all points are on one side.
            int countSides = 0;
            Point vect = (pb - pa) / m_para.distanceFnc(pa, pb);
            double l = (max(tspWidth, tspHeight) / 2);
            Point startP = pa - vect * l;
            Point endP = pb + vect * l;

            for (Way::iterator wAI = way1->begin();
                !wAI.end(); ++wAI)         // iterate over convex hull
            {
                try {
                    if (_isLeftSideExEqual(startP, endP, *wAI))
                    {
                        if (countSides == 0)
                        {
                            countSides = 1;
                        }
                        else if (countSides == 2)
                        {
                            countSides = 3;
                            break;
                        }
                    }
                    else {
                        if (countSides == 0)
                        {
                            countSides = 2;
                        }
                        else if (countSides == 1)
                        {
                            countSides = 3;
                            break;
                        }
                    }
                }
                catch (const exception&)
                {
                    // ignore, all points on the line are not disrupting the tangent
                }
            }
            if (countSides != 3)
            {
                for (Way::iterator wBI = way2->begin();
                    !wBI.end(); ++wBI)
                {
                    try {
                        if (_isLeftSideExEqual(startP, endP, *wBI))
                        {
                            if (countSides == 2)
                            {
                                countSides = 3;
                                break;
                            }
                        }
                        else {
                            if (countSides == 1)
                            {
                                countSides = 3;
                                break;
                            }
                        }
                    }
                    catch (const exception&)
                    {
                        // ignore, because tangente still valid.
                    }
                }
            }
            if (countSides != 3)
            {
                state = CrossedState::isCrossed;
            }
        }

        if (state == CrossedState::isCrossed)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();

            if ((callState == MergeCallState::topDown
                && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGoodLow = true;
            }
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGoodHigh = true;
            }

            // running both if-s parallel, because to guaranty below the h.. and l.. valid
            if (isGoodLow)
            {
                laI = edgeI->aNodeI;
                lbI = edgeI->bNodeI;
            }
            if (isGoodHigh)
            {
                haI = edgeI->aNodeI;
                hbI = edgeI->bNodeI;
            }
        }
    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, haI, laI, hbI, lbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hbI, lbI, haI, laI);

    /// correct the hull in way1
    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    /// clear childs - see test7
    (&haI)->isChildNode = false;
    (&haI)->child = nullptr;
    (&hbI)->isChildNode = false;
    (&hbI)->child = nullptr;
    (&laI)->isChildNode = false;
    (&laI)->child = nullptr;
    (&lbI)->isChildNode = false;
    (&lbI)->child = nullptr;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in _mergeHull().

    _flipWay(way1, pCenter);  // necessary, because code before does not keep the clock-wise order.
}

double HierarchicalClusteredAlignment::_calcAreaOfTriangle(const Point& p1, const Point& p2, const Point& p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
        (a + b - c)
        * (a - b + c)
        * (-a + b + c)
        * (a + b + c)
    );
}

double HierarchicalClusteredAlignment::_area(const PointL& polygon)
{
    double area = 0;
    for (auto pointI = polygon.begin();
        pointI != polygon.end(); ++pointI)
    {
        auto pointNextI = pointI; ++pointNextI;
        if (pointNextI == polygon.end()) pointNextI = polygon.begin();
        area += pointI->x() * pointNextI->y();
        area -= pointI->y() * pointNextI->x();
    }
    return abs(area);
}

double HierarchicalClusteredAlignment::_triangleAltitudeOnC(Point c1, Point c2, Point perpendicular)
{
    double c = m_para.distanceFnc(c1, c2);
    double a = m_para.distanceFnc(c2, perpendicular);
    double b = m_para.distanceFnc(perpendicular, c1);
    double s = (a + b + c) / 2.0;
    return 2.0 * sqrt(s * (s - a) * (s - b) * (s - c)) / c;
}

bool HierarchicalClusteredAlignment::_isLeftSideExEqual(const Point& p1Line, const Point& p2Line, const Point& check)
{
    Point p1 = p2Line - p1Line;
    Point p2 = check - p1Line;

    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x() * p2.y()) + ((-1 * p1.y()) * p2.x()));
    dot = static_cast<int>(dot * 100) / 100;
    double degree90strait = m_para.distanceFnc(p2Line, check);
    if (dot == 0 || ((dot / 90 - 0.01) < degree90strait && degree90strait < (dot / 90 + 0.01)))
        throw logic_error("Point is on line, and not left or right.");
    return dot < 0;
}


bool HierarchicalClusteredAlignment::_isRightSide(const Point& p1, const Point& p2)
{
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x() * p2.y()) + ((-1 * p1.y()) * p2.x()));
    return dot >= 0;
}

double HierarchicalClusteredAlignment::_calcDegree(Node* node, Node* nodeTested)
{
    return _calcDegree(nodeTested->point, node->point);
}

double HierarchicalClusteredAlignment::_calcDegree(Point& node, Point& nodeTested)
{
    Point pointVect = nodeTested - node;

    double degree = (pointVect.x() + pointVect.y())
        / (m_para.distanceFnc(Point(0, 0), pointVect)
            * m_para.distanceFnc(Point(0, 0), Point(1, 1)));

    double degreeArc = acos(degree);
    // clock-wise
    if (_isRightSide(Point(1, 1), pointVect))
        return  degreeArc;
    else
        return  2 * acos(-1) - degreeArc;
}

bool HierarchicalClusteredAlignment::_checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{       // calc clock-wise
    double degO1 = _calcDegree(nodePoint, cluPointOut);     // typical smaller
    double degO2 = _calcDegree(nodePoint, cluPointOut2);
    double degC = _calcDegree(nodePoint, closestP);
    if (degO1 <= degO2)
    {
        if (degO2 - degO1 == M_PI)
            return true;                // see test25
        else if (degO2 - degO1 > M_PI)
            return degO2 <= degC || degC <= degO1;
        else
            return degO1 <= degC && degC <= degO2;
    }
    else {
        if (degO2 - degO1 == M_PI)
            return true;               // see test25
        return degO1 <= degC || degC <= degO2;      // shall include points although at the outer winkel! see test25
    }
}

double HierarchicalClusteredAlignment::_correlation(Point a, Point b)
{
    return ((a.x() * b.x()) + (a.y() * b.y())) / (m_para.distanceFnc(Point(0, 0), a) * m_para.distanceFnc(Point(0, 0), b));
}


bool HierarchicalClusteredAlignment::_checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP)
{
    double dist1 = m_para.distanceFnc(cluPointOut, nodePoint);
    double dist2 = m_para.distanceFnc(cluPointOut2, nodePoint);
    double distC = m_para.distanceFnc(closestP, nodePoint);
    double corr1 = _correlation(cluPointOut - nodePoint, closestP - nodePoint);
    //double degree0to1For1 = (corr1 + 1) / 2.0;
    double corr2 = _correlation(cluPointOut2 - nodePoint, closestP - nodePoint);
    //double degree0to1For2 = (corr2 + 1) / 2.0;
    //double corr = degree0to1For1 + degree0to1For2;
    //double corr = corr1 + corr2;
    double corr3 = _correlation(cluPointOut - nodePoint, cluPointOut2 - nodePoint);
    // distC, dist1,2 are tradeofs not correct decisions
    return ((distC / 2 < dist1 || distC / 2 < dist2) && corr3 < corr1 && corr3 < corr2);
}


HierarchicalClusteredAlignment::CeList::CeListType::iterator  HierarchicalClusteredAlignment::_insertToCeList(CeList& ceList, Node* rootFrom, Node* rootTo, Node* insert)
{
    auto ceListResI = ceList.ceList.end();

    if (!ceList.ceList.empty())
    {
        double distDirect = seboehDist(ceList.ceList.front().fromNode, ceList.ceList.back().toNode, insert);
        auto insertPosI = ceList.ceListOrder.insert({ distDirect, insert }).first;
        if (++insertPosI != ceList.ceListOrder.end())
        {
            ceList.ceList.clear();
            for (auto& orderEl : ceList.ceListOrder)
            {
                auto iter = __insertToCeListAlongList(ceList, rootFrom, rootTo, orderEl.second);
                if (orderEl.second == insert)
                    ceListResI = iter;
            }
        }
        else {
            ceListResI = __insertToCeListAlongList(ceList, rootFrom, rootTo, insert);
        }
    }
    else
    {
        ceListResI = __insertToCeListAlongList(ceList, rootFrom, rootTo, insert);
        double distDirect = seboehDist(ceList.ceList.front().fromNode, ceList.ceList.back().toNode, insert);
        ceList.ceListOrder.insert({ distDirect, insert });
    }

    return ceListResI;
}

HierarchicalClusteredAlignment::CeList::CeListType::iterator  HierarchicalClusteredAlignment::__insertToCeListAlongList(CeList& ceList, Node* rootFrom, Node* rootTo, Node* insert)
{
    ClosestEdges ce(insert);
    CeList::CeListType::iterator ceI = ceList.ceList.end();

    if (ceList.ceList.empty())
    {
        ce.fromNode = rootFrom;
        ce.toNode = rootTo;
    }
    else
    {
        double bestDist = numeric_limits<double>::max();
        auto bestI = ceList.ceList.end();
        Node* bestPrev = nullptr;
        Node* prev = ceList.ceList.front().fromNode;
        for (auto foundCeI = ceList.ceList.begin();
            foundCeI != ceList.ceList.end(); ++foundCeI)
        {
            double dist = seboehDist(prev, foundCeI->insertNode, ce.insertNode);
            if (dist < bestDist)
            {
                bestDist = dist;
                bestI = foundCeI;
                bestPrev = prev;
            }
            prev = foundCeI->insertNode;
        }
        // check end
        double dist = seboehDist(ceList.ceList.back().insertNode, ceList.ceList.back().toNode, ce.insertNode);
        if (dist < bestDist)
        {
            bestDist = dist;
            ceI = ceList.ceList.end();
        }
        else {
            ceI = bestI;
        }

        // set from to
        if (ceI != ceList.ceList.end())
        {
            ce.fromNode = bestPrev;
            ce.toNode = ceI->insertNode;
        }
        else {
            ce.fromNode = ceList.ceList.back().insertNode;
            ce.toNode = ceList.ceList.back().toNode;
        }
    }

    ce.dist = seboehDist(ce.fromNode, ce.toNode, ce.insertNode);
    return ceList.ceList.insert(ceI, ce);
}


TSP::Point HierarchicalClusteredAlignment::_calcPerpendicularPoint(Node* from, Node* to, Node* normal)
{
    double alt = _triangleAltitudeOnC(from->point, to->point, normal->point);
    double c = m_para.distanceFnc(from->point, normal->point);
    double b = sqrt(c * c - alt * alt);

    return from->point + ((to->point - from->point) * (b / m_para.distanceFnc(from->point, to->point)));
}


// because test38 -> HINT: in the whole algorithm it is not allowed to have map.insert with collision.
template <typename T, typename V>
std::pair<typename T::iterator, bool> HierarchicalClusteredAlignment::_insertToMap(double dist, T& mapContainer, V&& value)
{
    assert(dist > -SMALL_DOUBLE_STEP_MIN);
    auto mI = mapContainer.end();
    if ((mI = mapContainer.find(dist)) != mapContainer.end())
    {
        if (mI->second == value) return make_pair(mI, false);

        // HINT: can happen at neighboursEdge - MARK:collision1
        if (!m_isLogCollisionDetected)
        {
            cerr << "\nCollision detected - is handled!" << endl;
            m_isLogCollisionDetected = true;
        }

        auto mInext = mI; ++mInext;
        double diffNext = 0;
        if (mInext == mapContainer.end())
        {
            diffNext = SMALL_DOUBLE_STEP_MIN;
        }
        else {
            diffNext = (mInext->first - mI->first) / 2.0;
        }

        // overflow
        if ((mI->first + SMALL_DOUBLE_STEP_MIN) < mI->first)
        {
            if (!m_isLogOverflowDetected)
            {
                cerr << "\nOverflow detected - please shrink your inputs!" << endl;
                m_isLogOverflowDetected = true;
            }
            dist = std::numeric_limits<double>::min();
        }
        else {
            dist = max(min(diffNext, SMALL_DOUBLE_STEP_MAX), SMALL_DOUBLE_STEP_MIN) + mI->first;
        }
    }
    return mapContainer.insert({ dist, value });
}


bool HierarchicalClusteredAlignment::_isNotInsideTriangle(Node* point1, Node* point2, Node* point3, std::list<Node*>& pointsInside)
{
    for (auto& insideNode : pointsInside)
    {
        if (insideNode != point1 && insideNode != point2 && insideNode != point3)
        {
            if (_isInsideTriangle(point1->point, point2->point, point3->point, insideNode->point))
            {
                return false;
            }
        }
    }
    return true;
}


bool HierarchicalClusteredAlignment::_updateHullOrUpdateSimple(shared_ptr<Node> hullNodeA, shared_ptr<Node> updateNode, NodeL& hull)
{
    // MARK_hullNodeSearch
    auto hullI = find_if(hull.begin(), hull.end(),
        [hullNodeA](const NodeL::value_type& val)
        {
            return hullNodeA->point == val->point;
        });
    shared_ptr<Node> hullNode;
    if (hullI != hull.end())
        hullNode = *hullI;
    else
        hullNode = hullNodeA;

    // check valid updateNode
    if (updateNode.get() == hullNode->edge1
        || updateNode.get() == hullNode->edge2)
        return false;
    if (hullNode->edge1 != nullptr
        && hullNode->edge2 != nullptr)
        return false;
    if (updateNode->edge1 != nullptr
        && updateNode->edge2 != nullptr)
        return true;

    // update
    if (hullNode->edge1 == nullptr)
    {
        hullNode->edge1 = updateNode.get();
        hullNodeA->edge1 = updateNode.get();
    }
    else if (hullNode->edge2 == nullptr)
    {
        hullNode->edge2 = updateNode.get();
        hullNodeA->edge2 = updateNode.get();
    }
    else return true;
    if (updateNode->edge1 == nullptr)
        updateNode->edge1 = hullNode.get();
    else if (updateNode->edge2 == nullptr)
        updateNode->edge2 = hullNode.get();
    else return true;

    // check
    assert(updateNode->edge1 != updateNode.get());
    assert(updateNode->edge2 != updateNode.get());

    return true;
}

bool HierarchicalClusteredAlignment::_updateAllowed(shared_ptr<Node> closest, shared_ptr<Node> node, bool isTestOnClique)
{
    // check valid nodes
    if (node.get() == closest->edge1
        || node.get() == closest->edge2)
        return false;
    if (closest->edge1 != nullptr
        && closest->edge2 != nullptr)
        return false;
    if (node->edge1 != nullptr
        && node->edge2 != nullptr)
        return true;        // because this calls intern if, which calls this criteria again, and breaks the loop.

    // check on clique - graph theory
    // walk through the edges1/2 starting from closest:
    //  if ending at node, there is a clique - edge shall not be supported
    //  only if the cound of vertices = n.
    if (isTestOnClique)
    {
        Node* nod = closest.get();
        Node* prev = nullptr;
        size_t tspSizeI = 0;
        bool isClique = false;
        while (nod != nullptr)
        {
            ++tspSizeI;
            if (nod == node.get() && tspSizeI < m_tspSize)
            {
                isClique = true;
                break;
            }
            if (nod->edge1 != nullptr && nod->edge1 != prev)
            {
                prev = nod;
                nod = nod->edge1;
            }
            else if (nod->edge2 != nullptr && nod->edge2 != prev)
            {
                prev = nod;
                nod = nod->edge2;
            }
            else break;
        }
        return !isClique;
    }

    return true;
}



HierarchicalClusteredAlignment::NodeL 
HierarchicalClusteredAlignment::determineWay(BSTNode* splittingTree, NodeL& hull, Way* way)
{
    assert(way->size() > 1);

    auto wayI = way->begin();
    while (!wayI.end())
        m_bestWay.push_back(wayI.nextChild());

    _wayCommon = m_bestWay;     // precond
    if (splittingTree != nullptr)
        breadthTracking(m_bestWay, splittingTree);

    /// * create result-struct
    NodeL ret;
    for (Node* node : _wayCommon)
        ret.push_back(make_shared<Node>(*node));

    return ret;
}

/* can be deleted
std::list<std::list<HierarchicalClusteredAlignment::BSTNode*>> HierarchicalClusteredAlignment::_getClosestNPointsSet(std::list<BSTNode*> &input, size_t n, std::function<Node*(const BSTNode*&)> getValueFnc)
{
    std::list<std::list<BSTNode*>> ret;
    while (!input.empty())
    {
        const BSTNode* node = input.front();
        double *closest = new double[n];
        const BSTNode* *best = new const BSTNode*[n];
        for (size_t i=0; i < n; ++i) closest[i] = numeric_limits<double>::max();
        for (size_t i=0; i < n; ++i) best[i] = nullptr;
        for (const BSTNode* nodeTest : input)
        {
            if (nodeTest == node) continue;
            double d = m_para.distanceFnc(getValueFnc(node)->point, getValueFnc(nodeTest)->point);
            for (size_t i=0; i < n; ++i)
            {
                if (closest[i] > d)
                {
                    closest[i] = d;
                    best[i] = nodeTest;
                    break;
                }
            }
        }

        // creat res
        list<BSTNode*> result;
        result.push_back(const_cast<BSTNode*>(node));
        for (size_t i=0; i < n; ++i) if (best) result.push_back(const_cast<BSTNode*>(best[n]));
        ret.push_back(std::move(result));

        // erase
        for (size_t i=0; i < n; ++i) if (best) { input.remove(const_cast<BSTNode*>(best[n]));
        }
        delete [] best;
        delete [] closest;
    }

    return ret;
}
*/

bool HierarchicalClusteredAlignment::_isNodeClosest(BSTNode* n1, BSTNode* n2, BSTNode* n3)
{
    Point m(0, 0);
    m += n1->point->point;
    m += n2->point->point;
    m += n3->point->point;
    m /= 3;

    double minDist = min(min(m_para.distanceFnc(n1->point->point, n2->point->point)
        , m_para.distanceFnc(n2->point->point, n3->point->point))
        , m_para.distanceFnc(n1->point->point, n3->point->point));

    if (m_para.distanceFnc(n1->point->point, m) > minDist)
    {
        return false;
    }
    else if (m_para.distanceFnc(n2->point->point, m) > minDist)
    {
        return false;
    }
    else if (m_para.distanceFnc(n3->point->point, m) > minDist)
    {
        return false;
    }

    return true;
}

void HierarchicalClusteredAlignment::_setAndClearClosestsTriangle(std::list<std::list<BSTNode*>>& ret, ClosestTriangleListType& closests, ClosestTriangleListType::iterator& cloI, ClosestTriangleListType::iterator& cloBeforeI, size_t cloITupleN)
{
    auto node1 = get<1>(*cloBeforeI);
    auto node2 = get<2>(*cloBeforeI);
    auto node3 = (cloITupleN == 1) ? get<1>(*cloI) : get<2>(*cloI);
    ret.push_back({ node1, node2, node3 });
    
    // * clear unesseccary points
    cloBeforeI = closests.erase(cloBeforeI);
    auto nextI = cloI; ++nextI;
    for (auto cloAfterI = nextI; cloAfterI != closests.end(); )
    {
        auto n1 = get<1>(*cloAfterI);
        auto n2 = get<2>(*cloAfterI);
        if (n1 == node1 || n2 == node1)
        {
            node1->alternatives.push_back(((n1 == node1) ? n2 : n1));   // dead-code, cause prob. logic-error in the past
            cloAfterI = closests.erase(cloAfterI);
        }
        else
            if (n1 == node2 || n2 == node2)
            {
                node2->alternatives.push_back(((n1 == node2) ? n2 : n1));
                cloAfterI = closests.erase(cloAfterI);
            }
            else
                if (n1 == node3 || n2 == node3)
                {
                    node3->alternatives.push_back(((n1 == node3) ? n2 : n1));
                    cloAfterI = closests.erase(cloAfterI);
                }
                else
                    ++cloAfterI;
    }
    cloI = closests.erase(cloI);        // makes a difference, if deleted here and not above
}

std::list<std::list<HierarchicalClusteredAlignment::BSTNode*>>
HierarchicalClusteredAlignment::_getClosestNPointsSet(std::list<BSTNode*>& input, size_t n, std::function<Node * (const BSTNode*&)> getValueFnc)
{
    if (n > 0) --n;

    // * get list of closest pairs
    ClosestTriangleListType closests;
    for (const BSTNode* nodeTest : input)
    {
        for (const BSTNode* nodeOther : input)
        {
            if (nodeTest == nodeOther) continue;
            double d = m_para.distanceFnc(getValueFnc(nodeTest)->point, getValueFnc(nodeOther)->point);
            closests.push_back({ d, const_cast<BSTNode*>(nodeTest), const_cast<BSTNode*>(nodeOther) });
        }
    }
    closests.sort(
        [](const tuple<double, BSTNode*, BSTNode*>& val1
            , const tuple<double, BSTNode*, BSTNode*>& val2)
        {
            return get<0>(val1) < get<0>(val2);
        });

    // * combine two pairs with one similar node to a pair of three
    std::list<std::list<BSTNode*>> ret;
    auto cloI = closests.begin();
    while (cloI != closests.end())
    {
        bool isFound = false;
        for (auto cloBeforeI = closests.begin();
            cloBeforeI != cloI; ++cloBeforeI)
        {
            if (get<1>(*cloI) == get<1>(*cloBeforeI))
            {
                if (_isNodeClosest(get<2>(*cloBeforeI), get<1>(*cloI), get<2>(*cloI)))
                {
                    _setAndClearClosestsTriangle(ret, closests, cloI, cloBeforeI, 2);
                    isFound = true;
                    break;
                }
            }
            else if (get<2>(*cloI) == get<2>(*cloBeforeI))
            {
                if (_isNodeClosest(get<1>(*cloBeforeI), get<1>(*cloI), get<2>(*cloI)))
                {
                    _setAndClearClosestsTriangle(ret, closests, cloI, cloBeforeI, 1);
                    isFound = true;
                    break;
                }
            }
        }
        if (!isFound)
        {
            ++cloI;
        }
    }

    size_t countOfNodes = 3 * ret.size();

    // * collect the rest pairs - no singles
    if (!closests.empty())
    {
        ClosestTriangleListType eraseList;
                // iterate forward, cause then it is still smallest dist first.
        for (auto missing : closests)
        {
            bool isInsideRet1 = false;      // TODO: checking this is not necessary, if deleted correctly on _setAndClear...()
            bool isInsideRet2 = false;
            for (const auto& nodeList : ret)
            {
                for (const auto& node : nodeList)
                {
                    if (node->point == get<1>(missing)->point)
                    {
                        isInsideRet1 = true;
                        break;
                    }
                    if (node->point == get<2>(missing)->point)
                    {
                        isInsideRet2 = true;
                        break;
                    }
                }
            }
            if (!isInsideRet1 && !isInsideRet2)
            {
                ret.push_back({ get<1>(missing), get<2>(missing) });
                countOfNodes += 2;
                eraseList.push_back(missing);
            }
        }

        for (const auto& delEl : eraseList)
        {
            closests.remove(delEl);
        }
    }

    // * insert single points
    if (countOfNodes < input.size())
    {
        for (auto& nodeLooking : input)
        {
            bool found = false;
            for (auto& nodeL : ret)
            {
                for (auto& nodeExisting : nodeL)
                {
                    if (nodeLooking == nodeExisting) { found = true; break; }
                }
                if (found) break;
            }

            // ** insert as single
            if (!found)
            {
                ret.push_back({ nodeLooking });
                /// HINT: no delete/erase from closests necessary here, because below not used again.
            }
        }
    }

    return ret;
}


void HierarchicalClusteredAlignment::_updateResponsibility(BSTNode* node)
{
    for (auto& parent : node->childs)
    {
        for (auto& parChi : parent->childs)
        {
            /// * determine closest dist (ds) to sibling
            BSTNode* bestOncle = nullptr;
            double doncle = numeric_limits<double>::max();

            for (auto& oncle : node->childs)
            {
                if (oncle == parent) continue;
                double dist = m_para.distanceFnc(parChi->point->point, oncle->point->point);
                if (dist < doncle)
                {
                    doncle = dist;
                    bestOncle = oncle;
                }
            }
            if (doncle == numeric_limits<double>::max()) continue;

            /// * determine closest dist (dc) to child
            BSTNode* bestCh = nullptr;
            double dchild = numeric_limits<double>::max();
            for (auto& ch : parent->childs)
            {
                if (ch == parChi) continue;
                double dist = m_para.distanceFnc(ch->point->point, parChi->point->point);
                if (dist < dchild)
                {
                    dchild = dist;
                    bestCh = ch;
                }
            }
            if (dchild == numeric_limits<double>::max()) continue;

            /// * if .. set responsible
            if (doncle < dchild)
            {
                assert(bestOncle != nullptr);
                auto d = parChi->point->point;
                auto d1 = bestOncle->point->point;

                bool isOtherBroResponsible = false;
                for (const auto& bro : parent->childs)
                {
                    if (doncle > m_para.distanceFnc(bro->point->point, bestOncle->point->point))
                    {
                        isOtherBroResponsible = true;
                        break;
                    }
                }

                if (!isOtherBroResponsible)
                {
                    parChi->responsibleFor = bestOncle;
                }
            }
        }
    }
}

HierarchicalClusteredAlignment::BSTNode*
HierarchicalClusteredAlignment::_createNewBstNode(std::list<BSTNode*>&& closestList, TSP::Point nodePoint)
{
    BSTNode* node = new BSTNode(new Node(nodePoint));
    node->childs = move(closestList);

    _updateResponsibility(node);

    return node;
}

HierarchicalClusteredAlignment::BSTNode*
HierarchicalClusteredAlignment::_createHierachicalStructureRec(std::list<BSTNode*>& hull, std::list<BSTNode*>& innerPoints)
{
    assert(!innerPoints.empty());
    if (innerPoints.size() == 1)
    {
        return innerPoints.front();
    }
    else if (innerPoints.size() == 2)
    {
        return _createNewBstNode(move(innerPoints), Point(0, 0));
    }

    list<BSTNode*> input;
    for (auto node : innerPoints)
        input.push_back(node);

    /// * extract the closest three points
    auto closests = _getClosestNPointsSet(input, 3, [](const BSTNode*& node) -> Node* { return node->point; });

    list<BSTNode*> newCenters;
    for (auto closestList : closests)
    {
        Point mean(0, 0);
        if (!closestList.empty())     // !empty - cause, changed cause test114
        {
            for (const auto& bstn : closestList)
            {
                mean += bstn->point->point;
            }
            mean /= closestList.size();
            newCenters.push_back(move(_createNewBstNode(move(closestList), mean)));
        }
    }

    return _createHierachicalStructureRec(hull, newCenters);
}

void HierarchicalClusteredAlignment::_fillParents(BSTNode* parent)
{
    for (auto& child : parent->childs)
    {
        child->parent = parent;
        _fillParents(child);
    }
}

// test109
void HierarchicalClusteredAlignment::_connectNode2ToNode1(BSTNode* node2, BSTNode* node1)
{
    // cause first assignment of prev-next-pair rules - see test107
    bool isValid = true;
    for (auto node = node2->prev; node != nullptr; node = node->prev)
        if (node == node1) isValid = false;
    for (auto node = node1->next; node != nullptr; node = node->next)
        if (node == node2) isValid = false;

    if (isValid)
    {
        node2->next = node1;
        node1->prev = node2;
    }
}

void HierarchicalClusteredAlignment::_createLink(NodeL& hull
    , NearestBSTType::value_type& link1, NearestBSTType::value_type& link2
    , Node* from1, Node* from2)
{
    auto toI = std::find_if(hull.begin(), hull.end(), [from1](NodeL::value_type& val) { return val.get() == from1; });
    if (toI != hull.end())
    {
        ++toI; if (toI == hull.end()) toI = hull.begin();
        auto node1 = get<0>(link1);
        auto node2 = get<0>(link2);
        double dist1 = seboehDist(from1, (*toI).get(), node1->point);
        double dist2 = seboehDist(from1, node1->point, node2->point);
        double dist3 = seboehDist(node1->point, (*toI).get(), node2->point);
        double dist5 = get<0>(get<1>(link1).front());
        double dist6 = get<0>(get<1>(link2).front());
        double dComp1 = min(dist2, dist3) + dist1;
        double dComp2;
        {
            double dist1 = seboehDist(from1, (*toI).get(), node2->point);
            double dist2 = seboehDist(from1, node2->point, node1->point);
            double dist3 = seboehDist(node2->point, (*toI).get(), node1->point);
            dComp2 = min(dist2, dist3) + dist1;
        }

        if (dComp1 < dComp2)                //(min(dist2, dist3)+dist1) < (dist5 + dist6) )
        {
            if (dist2 < dist3)
            {
                if (node2->next == nullptr && node1->prev == nullptr)
                {
                    if (node2->prev != node1 && node1->next != node2)
                    {           // cause first assignment of prev-next-pair rules - see test107
                        _connectNode2ToNode1(node2, node1);
                    }
                }
            }
            else {
                if (node1->next == nullptr && node2->prev == nullptr)
                {
                    if (node1->prev != node2 && node2->next != node1)
                    {           // cause first assignment of prev-next-pair rules - see test107
                        _connectNode2ToNode1(node1, node2);
                    }
                }
            }
        }
        else {
            double dist1 = seboehDist(from1, (*toI).get(), node2->point);
            double dist2 = seboehDist(from1, node2->point, node1->point);
            double dist3 = seboehDist(node2->point, (*toI).get(), node1->point);
            //if ((min(dist2, dist3)+dist1) < (dist5 + dist6) )
            if (dist2 < dist3)
            {
                if (node1->next == nullptr && node2->prev == nullptr)
                {
                    _connectNode2ToNode1(node1, node2);
                }
            }
            else {
                if (node2->next == nullptr && node1->prev == nullptr)
                {
                    _connectNode2ToNode1(node2, node1);
                }
            }
        }
    }
}

void HierarchicalClusteredAlignment::_compareLink(Node* from1, NearestBSTType::value_type& link1, NearestBSTType& link, NodeL& hull)
{
    for (auto& link2 : link)
    {
        if (link1 == link2) continue;
        Node* from2 = get<1>(get<1>(link2).front());

        if (from1 == from2)
        {
            _createLink(hull, link1, link2, from1, from2);
        }
        else {
            from2 = get<1>(*(++get<1>(link2).begin()));

            if (from1 == from2)
            {
                _createLink(hull, link1, link2, from1, from2);
            }
        }
    }
}

void HierarchicalClusteredAlignment::_linkParents(NodeL& hull, BSTNode* root)
{
    NearestBSTType link;

    for (auto& node : root->childs)
    {
        NearestType nearest;
        auto prev = *(--hull.end());
        for (auto next : hull)
        {
            nearest.push_back(make_tuple(seboehDist(prev.get(), next.get(), node->point), prev.get()));
            prev = next;
        }
        nearest.sort([](NearestType::value_type& val1, NearestType::value_type& val2) {
            return get<0>(val1) < get<0>(val2);
            });

        link.push_back(make_tuple(node, nearest));
    }

    // find similar from-nodes as first,second-best only!
    for (auto& link1 : link)
    {
        Node* from1 = get<1>(get<1>(link1).front());
        _compareLink(from1, link1, link, hull);

        from1 = get<1>(*(++get<1>(link1).begin()));
        _compareLink(from1, link1, link, hull);
    }

}

HierarchicalClusteredAlignment::BSTNode*
HierarchicalClusteredAlignment::createHierarchyStructure(NodeL& hull, NodeL& innerPoints)
{
    // leafs
    list<BSTNode*> input;
    for (auto node : innerPoints)
        input.push_back(new BSTNode(node.get()));
    list<BSTNode*> hullInput;
    for (auto node : hull)
        hullInput.push_back(new BSTNode(node.get()));

    auto root = _createHierachicalStructureRec(hullInput, input);

    _fillParents(root);

    _linkParents(hull, root);

    return root;
}

std::tuple<Node*, Node*> 
HierarchicalClusteredAlignment::_findBestAlignment(NodeL& hull, const Node* point)
{
    double bestD = numeric_limits<double>::max();
    Node* bestNfrom = nullptr;
    Node* bestNto = nullptr;
    Node* from = nullptr;
    Node* to = hull.back().get();
    for (const auto& node : hull)
    {
        from = to;
        to = node.get();
        double d = seboehDist(from, to, point);
        if (d < bestD)
        {
            bestD = d;
            bestNfrom = from;
            bestNto = to;
        }
    }
    return make_tuple(bestNfrom, bestNto);
}

std::tuple<double, HierarchicalClusteredAlignment::WayL::iterator> 
HierarchicalClusteredAlignment::_findBestAlignmentWithoutLast(WayL& way, const Node* point)
{
    auto bestWayI = way.end();
    double bestD = numeric_limits<double>::max();
    auto wayPrevI = way.begin();
    for (auto wayI = ++way.begin(); wayI != way.end(); ++wayI)
    {
        double d = seboehDist(*wayPrevI, *wayI, point);
        if (d < bestD)
        {
            bestD = d;
            bestWayI = wayPrevI;
        }
        wayPrevI = wayI;
    }

    return make_tuple(bestD, bestWayI);
}

/// @returns list of prev-iterators in way
std::list<std::tuple<double, HierarchicalClusteredAlignment::WayL::iterator>> 
HierarchicalClusteredAlignment::_findAllAlignments(WayL& way, const Node* point)
{
    std::list<std::tuple<double, WayL::iterator>> ret;
    auto wayPrevI = --way.end();
    for (auto wayI = way.begin(); wayI != way.end(); ++wayI)
    {
        double d = seboehDist(*wayPrevI, *wayI, point);
        ret.push_back({ d, wayPrevI });

        if (++wayPrevI == way.end()) wayPrevI = way.begin();
    }

    using type = std::tuple<double, WayL::iterator>;
    ret.sort([](type& val1, type& val2) { return get<0>(val1) < get<0>(val2); });

    auto d = (*get<1>(ret.front()))->point.x();
    return ret;
}


std::tuple<double, HierarchicalClusteredAlignment::WayL::iterator> HierarchicalClusteredAlignment::_findBestAlignment(WayL& way, const Node* point)
{
    auto bestWayI = way.end();
    double bestD = numeric_limits<double>::max();
    auto wayPrevI = --way.end();
    for (auto wayI = way.begin(); wayI != way.end(); ++wayI)
    {
        double d = seboehDist(*wayPrevI, *wayI, point);
        if (d < bestD)
        {
            bestD = d;
            bestWayI = wayPrevI;
        }

        if (++wayPrevI == way.end()) wayPrevI = way.begin();
    }

    return make_tuple(bestD, bestWayI);
}


void HierarchicalClusteredAlignment::_insertAtBestAlignment(WayL& way, std::list<std::tuple<double, Node*, BSTNode*>>& bestAlignment)
{
    for (const auto& align : bestAlignment)
    {
        auto fromI = find(way.begin(), way.end(), get<1>(align));
        auto node = get<2>(align);
        node->hullFrom = *fromI;
        node->isInserted = true;
        way.insert(++fromI, node->point);
    }
}

HierarchicalClusteredAlignment::WayL::iterator HierarchicalClusteredAlignment::_nextNodeWayL(WayL& way, WayL::iterator wayI)
{
    auto nextI = wayI; ++nextI;
    if (nextI == way.end()) nextI = way.begin();
    return nextI;
}


std::tuple<HierarchicalClusteredAlignment::WayL::const_iterator
    , HierarchicalClusteredAlignment::WayL::const_iterator>
    HierarchicalClusteredAlignment::_mergeListCollision(WayL& out, WayL& secondL, const WayL::const_iterator& outI, WayL::const_iterator& secI, WayL::const_iterator& failedI, const WayL::const_iterator& foundI)
{
    auto fromI = failedI;
    if (fromI != out.begin())
    {
        --fromI;
    }
    else {
        assert(false);      // todo: dead-code?
        out.push_front(out.back());
        out.pop_back();
        fromI = out.begin();
    }

    WayL part;
    Node* from = *fromI;
    part.push_back(from);
    part.insert(part.end(), failedI, outI);
    auto toI = outI;
    Node* to;
    if (outI != out.cend())
    {
        to = *outI;
    }
    else {
        to = *(out.begin());
        --toI;
    }
    part.push_back(to);

    auto secII = secI;
    for (; secII != foundI; ++secII)
    {
        if (secII == secondL.end()) break;       // HINT: safety only - not correct here
        if (find(part.begin(), part.end(), *secII) == part.end())       // check cause, maybe the out is the part containing change
        {
            auto iter = get<1>(_findBestAlignmentWithoutLast(part, *secII));
            part.insert(++iter, *secII);
        }
    }

    auto replaceI = out.erase(fromI, ++toI);
    auto ret = out.end();
    if (outI == out.cend())
    {
        part.pop_back();
    }
    ret = out.insert(replaceI, part.begin(), part.end());
    for (size_t i = 0; i < part.size() - 1; ++i) ++ret;

    return make_tuple(ret, secII);
}

void HierarchicalClusteredAlignment::_mergeLists(WayL& out, WayL& second)
{
    assert(out.front() == second.front());      // only for check

    auto secI = second.cbegin();
    auto failedI = out.cend();
    auto outI = out.cbegin();
    for (; outI != out.cend(); ++outI)
    {
        if (secI == second.end())
            int i = 5; // todo: is not happening?
        auto fI = find(second.cbegin(), second.cend(), *outI);
        if (fI != second.end())
        {
            if (failedI == out.end())
            {
                if (secI != fI)
                {
                    outI = out.insert(outI, secI, fI);
                    for (; secI != fI; ++secI) ++outI;      // cause buggy list?
                    // instead  secI = fI;
                }
                ++secI;
            }
            else
            {       // collision
                if (secI != fI)
                {
                    tie(outI, secI) = _mergeListCollision(out, second, outI, secI, failedI, fI);
                }
                failedI = out.end();
                ++secI;
            }
        }
        else
        {
            if (failedI == out.end())
                failedI = outI;
        }
    }

    if (failedI != out.end())
    {
        auto res = _mergeListCollision(out, second, out.cend(), secI, failedI, second.cend());
        secI = get<1>(res);
    }

    if (secI != second.end()
        || outI != out.end())
    {
        auto res = _mergeListCollision(out, second, out.cend(), secI, outI, second.cend());
        //out.insert(out.end(), secI, second.cend());
    }
}


/** recursive depth-first walk with closest-alignment-first
 * at return so called 'backtracking' the alignment happens
 */
std::tuple<double, HierarchicalClusteredAlignment::WayL::iterator>
HierarchicalClusteredAlignment::_checkBestAlignment(
    WayL& way
    , BSTNode* root
    , std::list<std::tuple<double, WayL::iterator>>::iterator alignI
    , std::list<BSTNode*>::iterator leafChildI)
{
    auto bestAlignRes = *alignI;

    /// * check if is one of three points inside this triangle - dueto: test12
    size_t count = 0;
    bool isFailed = false;
    do {
        auto fromI = get<1>(bestAlignRes);
        if (fromI != way.end())
        {
            auto nextI = fromI; ++nextI; if (nextI == way.end()) nextI = way.begin();
            isFailed = false;
            for (const auto& child : root->childs)
            {
                if (child == *leafChildI) continue;
                if (_isInsideTriangle((*fromI)->point, (*nextI)->point, (*leafChildI)->point->point, child->point->point))
                {
                    isFailed = true;
                    ++count;
                    break;
                }
            }
        }
        if (isFailed)
            bestAlignRes = *(++alignI);
    } while (isFailed && count < 3 && get<1>(bestAlignRes) != way.end());
    if (get<1>(bestAlignRes) == way.end())
    {       // it was not possible to improve for test12 - so all edges around contain one dot at least
        // this is not possible at 3 points
        assert(false);
        bestAlignRes = _findBestAlignment(way, (*leafChildI)->point);
    }

    return bestAlignRes;
}

template<typename F>
std::list<std::tuple<double, HierarchicalClusteredAlignment::WayL::iterator>> 
HierarchicalClusteredAlignment::_findAllAlignments_if(WayL& way, const Node* point, F f)
{
    std::list<std::tuple<double, WayL::iterator>> ret;
    auto wayPrevI = --way.end();
    for (auto wayI = way.begin(); wayI != way.end(); ++wayI)
    {
        double d = f(*wayPrevI, *wayI, point);
        ret.push_back({ d, wayPrevI });

        if (++wayPrevI == way.end()) wayPrevI = way.begin();
    }

    using type = std::tuple<double, WayL::iterator>;
    ret.sort([](type& val1, type& val2) { return get<0>(val1) < get<0>(val2); });

    return ret;
}

bool HierarchicalClusteredAlignment::_endInnerOrderOnOneLevel(BSTNode* root)
{
    return (_innerOrderVariationCounter >= 5);
}


std::list<HierarchicalClusteredAlignment::LinkBST2WayNodeTuple>  
HierarchicalClusteredAlignment::_insertAtBestAlignmentAllVariations(WayL& way, std::list<BSTNode*>& points)
{

    // * init maxVar Var bestD bestV and insertions
    double bestD = std::numeric_limits<double>::max();
    list<list<BSTNode*>::iterator> bestV;

    // c++-standard impl
    auto permutation = points;

    // * go through all combinations
    do {
        list<list<BSTNode*>::iterator> pointsPos;
        double varDist = 0;
        list<list<Node*>::iterator> insertions;
        //   * do for all elements
        for (auto &perEl : permutation) {
            double dist = 0;
            WayL::iterator wayI;
            tie(dist, wayI) = _findBestAlignment(way, perEl->point);
            insertions.push_back(
                way.insert(++wayI, perEl->point)
            );
            pointsPos.push_back(find(points.begin(), points.end(), perEl));
            varDist += dist;
        }

        //   * if dist < bestD  update bestV
        if (varDist < bestD)
        {
            bestD = varDist;
            bestV = pointsPos;
        }
        //   * clear insertions
        for (const auto& wayI : insertions)
        {
            way.erase(wayI);
        }
    } while (next_permutation(permutation.begin(), permutation.end()));

    // * insert bestV
    list<LinkBST2WayNodeTuple> ret;
    for (const auto& pointsI : bestV)
    {
        auto res = _findBestAlignment(way, (*pointsI)->point);
        ret.push_back({ 
            get<0>(res), 
            way.insert(++get<1>(res), (*pointsI)->point), 
            *pointsI 
            });
    }

    return ret;
}


std::list<int> HierarchicalClusteredAlignment::_getInnerOrderOnOneLevel(BSTNode* root)
{

    if (std::find(_aspects.begin(), _aspects.end(), "n^2-hierarchical") != _aspects.end())
    {
        int variationCounter = _innerOrderVariationCounter % 6;
        if (variationCounter == 0)
        {
            return list<int>({ 1, 2, 3 });
        }
        else if (variationCounter == 1)
        {
            return list<int>({ 2, 1, 3 });
        }
        else if (variationCounter == 2)
        {
            return list<int>({ 1, 3, 2 });
        }
        else if (variationCounter == 3)
        {
            return list<int>({ 3, 1, 2 });
        }
        else if (variationCounter == 4)
        {
            return list<int>({ 3, 2, 1 });
        }
        else
        {
            return list<int>({ 2, 3, 1 });
        }
    }
    else if (std::find(_aspects.begin(), _aspects.end(), "n-farestDotFirst") != _aspects.end())
    {
        // .... todo: root->childs


    }
}

void HierarchicalClusteredAlignment::_nextInnerOrderOnOneLevel(BSTNode* root)
{
    ++_innerOrderVariationCounter;
}

std::list<std::tuple<double, HierarchicalClusteredAlignment::Node*, HierarchicalClusteredAlignment::BSTNode*>>
HierarchicalClusteredAlignment::_checkAlignment(WayL& way, BSTNode* root)
{
    list<tuple<double, Node*, BSTNode*>> bestCombi;
    double bestAlignmentOfGroup = numeric_limits<double>::max();
    for (auto childStartI = root->childs.begin();
        childStartI != root->childs.end();
        ++childStartI)
    {
        /// * generate alignment-combination-result
        list<tuple<double, Node*, BSTNode*>> bestAlignmentForChild;
        auto leafChildI = childStartI;

        auto d = (*leafChildI)->point->point.x();


        for (size_t i = 0; i < root->childs.size(); ++i)
        {
            bool isInserted = false;
            if ( // (false) &&  /* DISABLES CODE */ 
                (*leafChildI)->responsibleFor != nullptr
                // && !(*leafChildI)->isInserted) - when was it inserted before?
                )
            {       /// HINT: because test102 - closer tree-nodes than partners in the triangle, is the current node responsible for!
                auto resp = (*leafChildI)->responsibleFor;
                auto node = (*leafChildI)->point;

                /// * determine closest way-vertex (wv) to responsible sibling (rs)
                auto closestVertex = _findAllAlignments_if(way, resp->point,
                    [this](const Node* prev, const Node* next, const Node* el) {
                        return m_para.distanceFnc(prev->point, el->point);
                    });

                ///     * find wv with p<->rs and p<->wv have a correlation higher than 0.5 and wv is closest to p
                Node* bestAlignN = nullptr;
                for (const auto& alignEl : closestVertex)
                {
                    Node* alignNo = *get<1>(alignEl);
                    if (_correlation(alignNo->point - node->point, resp->point->point - node->point) > 0.5)
                    {       // found 
                        bestAlignN = alignNo;
                        break;
                    }
                }
                if (bestAlignN != nullptr)
                {
                    /// * choose the second vertex (vv) as the one of before wv or after wv, which is closer to rs
                    auto bestAlignI = find(way.begin(), way.end(), bestAlignN);
                    auto prevI = bestAlignI; if (prevI == way.begin()) prevI = way.end(); --prevI;
                    auto nextI = bestAlignI; ++nextI; if (nextI == way.end()) nextI = way.begin();

                    auto d = resp->point->point;
                    auto d1 = (*prevI)->point;
                    auto d2 = (*nextI)->point;
                    auto d3 = (*leafChildI)->point->point;

                    // todo: prevent that in test102 prevI or nextI are on resp->childs
                    bool isValid = true;
                    for (const auto& bstn : resp->childs)
                    {
                        if ((*prevI)->point == bstn->point->point
                            || (*nextI)->point == bstn->point->point)
                        {
                            isValid = false;
                            break;
                        }
                    }

                    if (isValid)
                    {
                        /// * align p to vv and wv - insert it accordingly

                        bool isSiblingPrev = false;
                        bool isSiblingNext = false;
                        for (const auto& bsnSibl : root->childs)
                        {
                            if (bsnSibl->point == *prevI)
                                isSiblingPrev = true;
                            else if (bsnSibl->point == *nextI)
                                isSiblingNext = true;
                        }

                        // following check failed in test102
                        //                        if (m_para.distanceFnc(resp->point->point, (*prevI)->point)
                        //                                < m_para.distanceFnc(resp->point->point, (*nextI)->point))
                        if (isSiblingNext || isSiblingPrev)
                        {
                            if (isSiblingNext && isSiblingPrev)
                            {
                                if (m_para.distanceFnc(resp->point->point, (*prevI)->point)
                                    < m_para.distanceFnc(resp->point->point, (*nextI)->point))
                                    isSiblingNext = false;
                                else
                                    isSiblingPrev = false;
                            }
                            // it is only node-bestAlignN-resp dist, because at least, this dist has to be smaller than best align for node.
                            double dist = seboehDist(node, bestAlignN, resp->point);
                            if (isSiblingPrev)
                            {
                                bestAlignmentForChild.push_back(make_tuple(dist
                                    , *prevI, *leafChildI));
                                (*leafChildI)->hullFrom = *prevI;
                                way.insert(bestAlignI, (*leafChildI)->point);
                            }
                            else {
                                bestAlignmentForChild.push_back(make_tuple(dist
                                    , *bestAlignI, *leafChildI));
                                (*leafChildI)->hullFrom = *bestAlignI;
                                way.insert(nextI, (*leafChildI)->point);
                            }
                            // (*leafChildI)->isInserted = true; - when was it inserted before?
                            // resp->isInserted = true; - not doing, because resp is parent, no brother
                            isInserted = true;
                        }
                    }
                }
            }

            if (!isInserted)
            {
                //auto bestAlignRes = _findBestAlignment(hull, child->point);
                //auto bestAlignRes = _findBestAlignment(_way, (*leafChildI)->point);
                auto allAligns = _findAllAlignments(way, (*leafChildI)->point);
                auto alignI = allAligns.begin();
                auto bestAlignRes = _checkBestAlignment(way, root, alignI, leafChildI);

                auto d3 = (*get<1>(bestAlignRes))->point.x();

                bestAlignmentForChild.push_back(make_tuple(get<0>(bestAlignRes), *get<1>(bestAlignRes), *leafChildI));
                (*leafChildI)->hullFrom = *get<1>(bestAlignRes);
                // (*leafChildI)->isInserted = true; - when was it inserted before?
                way.insert(++get<1>(bestAlignRes), (*leafChildI)->point);
            }

            // iter
            ++leafChildI;
            if (leafChildI == root->childs.end())
                leafChildI = root->childs.begin();

            auto d1 = (*leafChildI)->point->point.x();
            {int i = 5; }
        }
        /// * sum up the new way-length
        double sum = 0;
        for (const auto& alignEl : bestAlignmentForChild)
        {
            sum += get<0>(alignEl);
            Node* node = get<2>(alignEl)->point;
            way.remove(node);
            *node = Node(node->point);       // refresh node
        }

        if (sum < bestAlignmentOfGroup)
        {
            bestCombi = bestAlignmentForChild;
            bestAlignmentOfGroup = sum;
        }
    }

    //    BSTNode *prev = get<2>(bestCombi.back());
    //    for (auto bstCI = bestCombi.begin();
    //         bstCI != bestCombi.end(); ++bstCI)
    //    {
    //        BSTNode *bstn = get<2>(*bstCI);

    //        auto d = bstn->point->point.x();
    //        auto d1 = bstn->hullFrom->point.x();
    //        auto d2 = bstn->childs.size();
    //        auto d3 = prev->point->point.x();

    //        if (bstn->hullFrom->point == prev->point->point)
    //        {
    //            bstn->prev = prev;
    //            prev->next = bstn;
    //        }

    //        prev = bstn;
    //    }

    return bestCombi;
}


void HierarchicalClusteredAlignment::breadthTracking(WayL& way, BSTNode* root, size_t level)
{
    m_bestLength = numeric_limits<double>::max();

    _wayCommon.clear();

    list<LinkBST2WayNodeTuple> breadthList;
    //breadthList.push_back({0, way.insert(way.end(), root->point), root});  -- breadth-first
    breadthList.push_back({ 0, way.end(), root });      // depth-first

    breadthTracking_rec(way, breadthList);
    _wayCommon = way;

    auto wayCI = _wayCommon.begin();
    double length = 0;
    for (Node* node : _wayCommon)
    {
        if (wayCI == _wayCommon.begin()) continue;
        length += m_para.distanceFnc((*wayCI)->point, node->point);
        ++wayCI;
    }
    length += m_para.distanceFnc(_wayCommon.front()->point, _wayCommon.back()->point);

    m_currentLength = length;
}



void HierarchicalClusteredAlignment::breadthTracking_rec(WayL& way
    , std::list<HierarchicalClusteredAlignment::LinkBST2WayNodeTuple>& breadthList
    , size_t level)
{
    if (breadthList.empty()) return;

    /*
    HINT: depth-first-search means:
    - rec algo goes directly to the leafs
    - so call rec again on sorted list of best-criteria for intermideate nodes
    - cancel criteria = no childs in bstNodes
    - then insert the leafs into way, else no insert to way.
    */

    /// depth-first-search go deep first
    {
        /// -- cancel criteria of recusive fnc
        if (get<2>(breadthList.front())->childs.empty())
        {
            list<BSTNode*> newAlignmets;
            for (auto& breadthEl : breadthList)
            {
                newAlignmets.push_back(get<2>(breadthEl));
            }
            _insertAtBestAlignmentAllVariations(way, newAlignmets);
            return;
        }

        /// -- go into depth
        map<BSTNode*, double> dists;
        
        /// VARIATION: either all only at hull, or all at closest dist to other three points.
        /// -- all at closest dist to each other - according to middle of triangle, because faster calc.
        if (breadthList.size() == 3)
        {
            Point m = get<2>(breadthList.front())->point->point;
            m += get<2>(*(++breadthList.begin()))->point->point;
            m += get<2>(breadthList.back())->point->point;
            m /= 3;

            for (auto& breadthEl : breadthList)
            {
                dists.insert({ get<2>(breadthEl) , m_para.distanceFnc(m, get<2>(breadthEl)->point->point) });
            }

        }
        else if (breadthList.size() == 2)
        {
            Point m = get<2>(breadthList.front())->point->point;
            m += get<2>(breadthList.back())->point->point;
            m /= 2;

            double minDist = numeric_limits<double>::max();
            Node* closestHull = nullptr;
            for (const auto& node : way)
            {
                double d = m_para.distanceFnc(m, node->point);
                if (d < minDist)
                {
                    minDist = d;
                    closestHull = node;
                }
            }
            closestHull->isHullNodeProcessed = true;

            breadthList.push_back({ 0, way.end(), new BSTNode(closestHull) });

            m *= 2;
            m += closestHull->point;
            m /= 3;

            for (auto& breadthEl : breadthList)
            {
                dists.insert({ get<2>(breadthEl) , m_para.distanceFnc(m, get<2>(breadthEl)->point->point) });
            }

        } else if (breadthList.size() == 1)
        {
            Point m = get<2>(breadthList.front())->point->point;

            double minDist1 = numeric_limits<double>::max();
            double minDist2 = numeric_limits<double>::max();
            Node* closestHull1 = nullptr;
            Node* closestHull2 = nullptr;
            for (const auto& node : way)
            {
                double d = m_para.distanceFnc(m, node->point);
                if (d < minDist1)
                {
                    minDist2 = minDist1;
                    minDist1 = d;
                    closestHull2 = closestHull1;
                    closestHull1 = node;
                }
                else if (d < minDist2)
                {
                    minDist2 = d;
                    closestHull2 = node;
                }
            }
            closestHull1->isHullNodeProcessed = true;
            breadthList.push_back({ 0, way.end(), new BSTNode(closestHull1) });
            closestHull2->isHullNodeProcessed = true;
            breadthList.push_back({ 0, way.end(), new BSTNode(closestHull2) });

            m += closestHull1->point;
            m += closestHull2->point;
            m /= 3;

            for (auto& breadthEl : breadthList)
            {
                dists.insert({ get<2>(breadthEl) , m_para.distanceFnc(m, get<2>(breadthEl)->point->point) });
            }

        }
        else assert(false);

        breadthList.sort([dists](const LinkBST2WayNodeTuple& lhs, const LinkBST2WayNodeTuple& rhs)
            {
                return dists.at(get<2>(lhs)) < dists.at(get<2>(rhs));
            });

        breadthList.reverse();      // max dist to center first


        for (auto& breadthEl : breadthList)
        {
            if (!get<2>(breadthEl)->point->isHullNodeProcessed)
            {
                list<LinkBST2WayNodeTuple> newBreadthList;
                for (auto& bstNode : get<2>(breadthEl)->childs)
                {
                    newBreadthList.push_back({ 0, way.end(), bstNode });
                }
                breadthTracking_rec(way, newBreadthList);
            }
        }

    }

}




void HierarchicalClusteredAlignment::breadthTracking_rec_breadthFirst(WayL& way
    , std::list<HierarchicalClusteredAlignment::LinkBST2WayNodeTuple> & breadthList
    , size_t level)
{
    if (breadthList.empty()) return;

    /*
    HINT: breadth-search means:
    - rec algo goes only on breadthList
    - so call rec again on bigger breadthList
    - cancel criteria = no childs in bstNodes
    */

    list<LinkBST2WayNodeTuple> newBreadthList;

    /// - handle all elements in this level = breadth-search
    for (auto& breadthEl : breadthList)
    {
        auto currI = get<1>(breadthEl);
        auto* bstCurr = get<2>(breadthEl);

        // ** remove the parent
        assert(currI != way.end());
        currI = way.erase(currI);

        // -- cancel criteria of recusive fnc
        if (bstCurr->childs.empty())
        {
            double dist; auto insertI = way.end();
            tie(dist, insertI) = _findBestAlignment(way, bstCurr->point);
            if (++insertI == way.end()) insertI = way.begin();
            way.insert(insertI, bstCurr->point);
            continue;
        }

        // -- create links according to align-criteria
        // align-criteria = shortest distour
        auto links = _insertAtBestAlignmentAllVariations(way, bstCurr->childs);

        // -- order links according to bundle-criteria
        // bundle-criteria = closer to center first


// allready done in _insertAtBestAlign...()
        // * search the prev and next hull-node in way for each el in links
        //list<tuple<double, BSTNode*>> linksAtHull;
        //for (const auto& pointEl : links)
        //{
        //    auto* bstN = get<2>(pointEl);
        //    auto fromI = get<1>(pointEl);
        //    auto currI = fromI; 
        //    assert(currI != way.end());
        //    if (++currI == way.end()) currI = way.begin();

        //    auto nextI = fromI;
        //    auto endI = nextI;
        //    if (++nextI == way.end()) nextI = way.begin();
        //    while (nextI != endI && !(*nextI)->isHull)
        //        if (++nextI == way.end()) nextI = way.begin();

        //    auto prevI = fromI;
        //    endI = prevI; ++endI;
        //    while (prevI != endI && !(*prevI)->isHull)
        //        if (prevI == way.begin()) prevI = --way.end(); else --prevI;

        //    // ** calc distour to hull and update el-dist (first el)
        //    linksAtHull.push_back({
        //        seboehDist(*prevI, *nextI, bstN->point),
        //        bstN });
        //}

        //// * sort according distour the links
        //linksAtHull.sort([](const auto& val1, const auto& val2)
        //    {
        //        return get<0>(val1) > get<0>(val2);
        //    });

        //// * start with the biggest distour to the lowest distour to align
        //for (const auto& pointEl : linksAtHull)
        //{
        //    auto* bstN = get<1>(pointEl);

        //    // ** insert curr to way
        //    auto allAligns = _findAllAlignments(way, bstN->point);
        //    auto alignI = allAligns.begin();
        //    auto insertI = get<1>(*alignI);
        //    if (insertI != way.end()) ++insertI; else insertI = way.begin();
        //    insertI = way.insert(insertI, bstN->point);

        //    // -- add the inserted-childs links also to new breadthList
        //    newBreadthList.push_back({
        //        get<0>(pointEl),
        //        insertI,        // new currI
        //        bstN
        //        });

        //}

        for (auto &el : links)
            newBreadthList.emplace_back(move(el));
    }

    // -- call recursive
    breadthTracking_rec_breadthFirst(way, newBreadthList);

}



void HierarchicalClusteredAlignment::buildTree(DT::DTNode::NodeType node, float level, TspAlgorithm::InputCitiesPointerType* openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    m_resultLength = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    incProgress(progressPercentage);

    /// * intern Input representation
    auto input = createInputRepresentation(openPointsBasic);
    m_tspSize = input.size();

    /// * determine hull
    auto res = determineHull(input);
    auto hull = get<0>(res);
    Way* way = get<1>(res);
    NodeL innerPoints;
    for (auto node : input)
    {
        if (!node->isHull)
            innerPoints.push_back(node);
    }

    /// * create structure
    int data;
    BSTNode* _bstRoot = nullptr;
    if (!innerPoints.empty())
        _bstRoot = createHierarchyStructure(hull, innerPoints);


    /// * build final path
    auto finalWay = determineWay(_bstRoot, hull, way);

    //    if (m_isCanceled)
    //    {
            // set any way - like the input, because we want valid m_resultLength values at further processing.
    //        m_shortestWay.clear();
    //        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
    //             openNodeIter != openPointsBasic->end(); ++openNodeIter)
    //        {
    //            m_shortestWay.push_back(**openNodeIter);
    //        }
    //        m_mutex.lock();
    //        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
    //        m_mutex.unlock();
    //    }

        /// copy result to final output
    for (auto pointI = finalWay.begin();
        pointI != finalWay.end(); ++pointI)
    {
        TSPWay w;
        w.push_back((*pointI)->point);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial WayL - drop this tree of WayL.
        (*newChildIter)->parent(node);      // backward linked
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}
