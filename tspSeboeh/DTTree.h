#ifndef DTTREE_H
#define DTTREE_H

#include "DTNode.h"

namespace TSP {

namespace DT {

class DTTree {
public:
    typedef DTNode NodeType;
    DTTree() { mRoot = new DTNode(NULL, Way()); }
    ~DTTree() { if (mRoot) { delete mRoot; mRoot = NULL; } }
    inline DTNode* root() { return mRoot; }

private:
    DTNode * mRoot = nullptr;
};

}
}



#endif // DTTREE_H
