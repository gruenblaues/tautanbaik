#ifndef TSPALGOCOMMON_H
#define TSPALGOCOMMON_H

#include <QMutex>
#include <chrono>

#include "TspAlgorithm.h"
#include "Parameters.h"
#include "Indicators.h"
#include "DTNode.h"
#include "DTTree.h"

namespace TSP {

class TspAlgoCommon : public TspAlgorithm
{
public:
    TspAlgoCommon();

    virtual ~TspAlgoCommon();


    /** following methods are not necessary to implement **/

    virtual void startRun(InputCitiesType *in, const Parameters &para);

    virtual Indicators status();

    virtual Result result();

    virtual void pause();

    virtual void stopRun();

    virtual void setBlockedEdges(std::list<std::pair<Point,Point>> &&edges);

protected:
    virtual void run();

    /**
     * This method have to be implemented!
     * The FINAL RESULT have to be in this variable node!
     * The result is set in the tree by order of 'parents'.
     * @brief buildTree builds the decission tree on which we search later the best route.
     * This procedure is called recursivly. It can be initialized with an empty \a root.
     * @param root the start of the tree.
     * @param level is the starting level (normaly 0).
     */
    virtual void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic);

    virtual void calculateResult(DT::DTNode::NodeType node);

    /**
     * @brief createWay is called by buildTree. It needs following parameters.
     * If it does not find any way, it returns an Invalid Way of type Way::State::ISNULL.
     * This indicates the end of building the Decission-Tree.
     * This method calculates the way for the children in the Decission-Tree.
     * @param startPoint is the last point from the DTNode where the children are added.
     * @param openPoints list of open points used for the way. This list is reduced by the used points in the new (returned) way.
     * @param number is the number of an part of the whole track. This counts from 0 to n. The part is of type way in the decission tree.
     * So it can directly used to iterate over the openPoints again.
     * @param wayLength
     * @return a route from \a startPoint including up to \a wayLength points.
     */
    virtual Way createWay(Point & startPoint, InputCitiesPointerType * openPoints, float wayLength) { return Way(); }

    /**
     * @brief newOpenPoints - only copies the points.
     * @param openPointsBasic - input points, which are copied.
     * @return new list of points, created with new operator.
     */
    virtual InputCitiesPointerType * newOpenPoints(InputCitiesPointerType * openPointsBasic, Point &startPoint);

    virtual void incProgress(float percent);

    virtual DT::DTTree *getDtTree();

protected:
    InputCitiesType     *m_cities = nullptr;
    Parameters          m_para;
    Indicators          m_indicators;
    DT::DTTree          m_tree;
    Way                 *m_resultWay = nullptr;
    double              m_resultLength;
    QMutex       m_mutex;
    bool    m_isCanceled=false;
    std::chrono::system_clock::time_point m_startTime;


};

}

#endif // TSPALGOCOMMON_H
