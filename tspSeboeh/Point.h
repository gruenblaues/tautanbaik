#ifndef POINT_H
#define POINT_H

#include <cmath>
#include <tuple>

#include "dll.h"

namespace TSP {

struct EXTERN Point {
    struct State {
        enum StateType { ISVALID=1, ISNULL=2, ISINFINIT=4, ISODDWAY=8 };
    };

    typedef float value_type;

    Point() : mX(0), mY(0), mState(State::ISNULL), m_number(0) {}
    Point(value_type x, value_type y) {
        mState = State::ISNULL;
        mX = x; mY = y;
        if (std::isinf(x) || std::isinf(y)) { mState = State::ISINFINIT; }
        else { mState = State::ISVALID; }
        m_number = 0;
    }
    Point(const Point &val) {
        *this = val;
    }
    Point &operator=(const Point& val) {
        mX = val.x();
        mY = val.y();
        mState = val.state();
        m_number = val.number();
        hullVector = val.hullVector;
        hullVectorSecond = val.hullVectorSecond;
        hullVectorThird = val.hullVectorThird;
        return *this;
    }
    ~Point() {}

//    Point(Point&&) = default;
//    Point &operator=(Point&&) = default;

    inline float x() const { return mX; }
    inline void set(const float aX, const float aY) { mState = State::ISVALID; mX = aX; mY = aY; }
    inline float y() const { return mY; }
    inline int state() const { return mState; }
    inline void state(int st) { mState = st; }
    inline int number() const { return m_number; }
    inline void number(int num) { m_number = num; }

    bool operator==(const Point &rhs) const {
        if (rhs.mState != mState) {
            return false;
        }
        return (((rhs.mX == mX) && (rhs.mY == mY))
                || (rhs.mState & mState & State::ISINFINIT));
    }

    bool operator==(const std::pair<float,float> &rhs) const {
        return *this == Point(rhs.first, rhs.second);
    }

    Point operator-(const Point &rhs) const {
        return Point( x()-rhs.x(), y()-rhs.y() );
    }

    Point &operator+=(const Point &rhs) {
        this->mX += rhs.mX;
        this->mY += rhs.mY;
        return *this;
    }

    Point operator+(const Point &rhs) const {
        return Point( x()+rhs.x(), y()+rhs.y() );
    }

    Point operator/=(const double rhs) {
        this->mX /= rhs;
        this->mY /= rhs;
        return *this;
    }

    bool operator!=(const Point &rhs) const {
        return !(*this == rhs);
    }

    Point operator*=(const double rhs) {
        mX *= rhs;
        mY *= rhs;
        return *this;
    }

    Point operator/(const double rhs) {
        Point tmp = *this;
        tmp /= rhs;
        return tmp;
    }

    Point operator*(const double rhs) {
        Point tmp = *this;
        tmp *= rhs;
        return tmp;
    }

    Point operator*(const Point rhs) {
        Point tmp = *this;
        tmp.set(tmp.x() * rhs.x(), tmp.y() * rhs.y());
        return tmp;
    }

    Point *hullVector = nullptr;
    Point *hullVectorSecond = nullptr;
    Point *hullVectorThird = nullptr;

private:
    float mX;
    float mY;
    int mState;
    int m_number;
};

}

#endif // POINT_H
