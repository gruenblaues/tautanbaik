#include "TspFacade.h"

#include "TspAlgoSeboeh.h"


using namespace TSP;


void TSPFacade::addInput(const PointListType points)
{
    mCities.clear();
    for (std::list<std::pair<float, float> >::const_iterator pI=points.begin()
         ; pI != points.end(); ++pI)
    {
        mCities.push_back(Point( pI->first, pI->second ));
    }
}

void TSPFacade::setDefaultParameters(Parameters * const inputPara)
{
    inputPara->decissionTreeChildCountFnc = defaultFnc::defaultDecissionTreeChildCountFnc;
    inputPara->decissionTreeWeightFnc = defaultFnc::defaultDecissionTreeWeightFnc;
    inputPara->distanceFnc =  defaultFnc::defaultDistanceFnc;
    inputPara->findNextPointFnc = defaultFnc::defaultFindNextPointFnc;
    inputPara->algoType = m_algoType;
    // default naiveRunMinutes is set in constructor of Parameters.
}

void TSPFacade::startRun(Parameters &para)
{
    algo->startRun(&mCities, para);
	if (m_results) delete m_results;
    m_results = nullptr;
}

void TSPFacade::stopRun()
{
    algo->stopRun();
}

void TSPFacade::pause()
{
    algo->pause();
}

void TSPFacade::wait(unsigned long milliseconds)
{
    algo->wait(milliseconds);
}

bool TSPFacade::isFinished()
{
    return algo->isFinished();
}

TSP::Result * TSPFacade::getResults()
{
    if (m_results) {
        return m_results;
	} else {
		m_results = new Result();
		*m_results = algo->result();
		return m_results;
	}
}

Indicators TSPFacade::status()
{
    return algo->status();
}

TSP::Way * TSPFacade::getResultTypeWay()
{
    m_resultWay.clear();
    Result result = algo->result();
    m_resultWay = *result.way();
    // HINT: mCities.clear(); is done at new run @see addInput.
    return &m_resultWay;
}
