#pragma once

#include "TspAlgoCommon.h"
#include <chrono>
#include <math.h>
#include <list>
#include <array>

#include "dll.h"

#include <bspTreeVersion/BspNode.h>

namespace TSP {

class EXTERN BellmannHeldKarpAlgo : public TspAlgoCommon
{
public:
    using Node = oko::Node;
    using NodeL = std::list<Node*>;
    using NodeLP = std::shared_ptr<NodeL>;

private:
    class Calculations {
    public:
        using container_type = std::map<size_t, std::map<const Node*, std::map<NodeLP, std::tuple<Node*, Node*, double>> > >;

        std::list<NodeLP>::iterator addSubSet(const NodeLP &np)
        {
            _subsets.push_back(np);
            return --_subsets.end();
        }

        double dist(size_t level, const Node* pivot, NodeLP &restSet)
        {
            if (restSet->size() == 1)
            {
                return 0;
            }
            else {
                return std::get<2>(_costs[level][pivot][restSet]);
            }
        }

        NodeLP getSubSets(size_t currentLevel, Node* missingPivot, NodeL &elementsFromSubSet)
        {
            auto nodeLPBest = _subsets.front();
            for (auto &sub : _subsets)
            {           // todo improve : set the subsets in map of size
                if (sub->size() == currentLevel-1
                        && std::find_if(sub->begin(), sub->end(),
                                        [elementsFromSubSet, missingPivot] (const Node* const &val)
                                    {
                                        if (val == missingPivot
                                            || std::find(elementsFromSubSet.begin(), elementsFromSubSet.end()
                                                         , val) == elementsFromSubSet.end()
                                            )
                                            return true;
                                        else return false;
                                    }
                            ) == sub->end())
                {
                   nodeLPBest = sub;
                   break;
                }
            }
            return nodeLPBest;
        }

        container_type::mapped_type::mapped_type::iterator push_back(size_t level, double dist, const Node* pivotN, const Node* subPivotN, NodeLP &subSet)
        {
            auto ret = _costs[level][pivotN].insert({subSet
                                             , std::make_tuple(const_cast<Node*>(pivotN), const_cast<Node*>(subPivotN), dist)});
            if (!std::get<1>(ret))
            {
                if (dist < std::get<2>(std::get<0>(ret)->second))
                    std::get<0>(ret)->second = std::make_tuple(const_cast<Node*>(pivotN), const_cast<Node*>(subPivotN), dist);
            }
            return std::get<0>(ret);
        }

        std::map<size_t, std::map<size_t, double>> distMat;

    private:
        container_type _costs;
        std::list<NodeLP> _subsets;
    };


public:
    BellmannHeldKarpAlgo() : TspAlgoCommon() { _bitsI.clear(); }

    // TspAlgoCommon interface
protected:
    void buildTree(DT::DTNode::NodeType node, float, InputCitiesPointerType *openPointsBasic);

private:
    Way     m_shortestWay;
    double  m_length=0;
    std::chrono::system_clock::time_point m_startTime;
    long    m_stackDepth=0;
    Calculations _calculations;
    NodeL   _input;
    std::array<bool, 10000> _bits;
    std::list<size_t>       _bitsI;

    void calcDistanceMatrix(NodeL &input);
    NodeL dynamicProgrammingWalk(size_t level);

    // no interface fncs
    std::tuple<NodeL, double> _addPivot(NodeL &subSubset, Node *pivot);
    void _nextBits();
};

}
