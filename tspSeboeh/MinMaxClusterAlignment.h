#pragma once

#include <memory>
#include <tuple>
#include <stack>
#include <array>

#include "TspAlgoCommon.h"
#include "TamarAlgorithmHelper.h"
#include "dataStructures/OlavList.h"
#include "dataStructures/commonstructures.h"
#include "bspTreeVersion/BspTreeSarah.h"


#ifndef _GLIBCXX_NOEXCEPT
#define _GLIBCXX_NOEXCEPT
#endif


namespace TSP {

class MinMaxClusterAlignment : public TspAlgoCommon
{
public:
    using Node = comdat::Node;
    using NodeL = std::list<std::shared_ptr<comdat::Node>>;
    using PointL = std::list<TSP::Point>;
    using Way = oko::OlavList;

    struct no_op_delete
    {
        void operator()(void*) { }
    };

    enum class MergeCallState {
        leftRight, topDown
    };
    enum class DirectAccessState {
        isDirectAccess, isNoDirectAccess
    };
    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };
    typedef oko::ClosestEdges ClosestEdges;
    typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;
    struct EdgeIter {
        EdgeIter(Way::iterator &aNodeIter, Way::iterator &bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
        Way::iterator aNodeI;
        Way::iterator bNodeI;
    };
    struct Cluster;
    struct NeighbourRef {
        Cluster *neighClu = nullptr;
    };
    struct Cluster {
        PointL verts;
        std::shared_ptr<Node> point;
        double area = 0.0;
        std::vector<std::shared_ptr<Node>> closestN;
        std::list<Node*> closestTestedPointsSorted;
        std::list<Cluster*> closestSortedNeighbours;
        std::map<double, std::tuple<Node*,Node*>> neighbourEdges;
        int state = 0;      // 1 = isAligned
        int iterationC = 0;
        size_t growPosition = 0;
        std::map<double, std::map<Node*, NeighbourRef>> neighbourRefs;       // first index = dist for common edge pairEdge, second index = from-node
        std::map<double, bool> currentRelativeMeasure;
        std::tuple<Node*, Node*> lastAssignedEdges = std::make_tuple(nullptr, nullptr);
    };
    typedef std::list<std::shared_ptr<Cluster>> ClusterGrow;

    class ClusterAlignmentException : public std::logic_error
    {
    public:
        ClusterAlignmentException(const std::string &msg, const std::list<double> &ret) : std::logic_error(msg.c_str()) { _ret = ret; }
        std::list<double> _ret;
    };

    size_t m_lastIterN = 0;
    size_t m_lastLevel = 0;


    struct PartnerEl
    {
        Node* from = nullptr;
        Node* to = nullptr;
        Cluster* partner1 = nullptr;
        Cluster* partner2 = nullptr;
        double commonDist = std::numeric_limits<double>::max();
    };
    using PartnerM = std::map<Node*, std::map<Node*, PartnerEl>>;
    PartnerM m_partners;

    PartnerM m_commonDist;      // for fnc _commonDistCalc()

    // VoronoiClusterAlignment
    struct Edge
    {
        Node *from = nullptr;
        Node *to = nullptr;
    };

    struct ClusterEdge
    {
        std::map<double, Cluster *> attachments;        // more than one clu, because of test27
        Edge edge;
    };

    using ClusterEdgeM = std::map<Node *, std::map< Node *, ClusterEdge> >;     // from, to, clusterEdge - and also - to, from, clusterEdge
    ClusterEdgeM m_edgeToClusters;

    struct OpenClusterEdge
    {
        bool operator==(const OpenClusterEdge &val) { return clu == val.clu && edge.from == val.edge.from && edge.to == val.edge.to; }
        Edge edge;
        Cluster *clu;
    };


    // fault in std::map --> own impl
    class TspMap {
#define TSPMAP_BORDER 1000
    public:
        TspMap() = default;
        TspMap(const TspMap &) = default;

        typedef OpenClusterEdge value_type;
        typedef double key_type;
        typedef std::list<std::tuple<key_type, value_type>> allocater_type;
        typedef allocater_type::iterator iterator;


        iterator
        find(const key_type& __x) {
            auto h = hash(__x);
            for (iterator r = _container[h].begin();
                   r != _container[h].end(); ++r)
            {
                if (std::get<0>(*r) == __x)
                    return r;
            }
            return _container[h].end();
        }

        std::pair<iterator, bool>
        insert(std::pair<double, OpenClusterEdge> && __x) {
            bool ret = false;
            iterator iter = find(__x.first);
            auto && container = _container[hash(__x.first)];
            if (iter != container.end())
            {
                container.push_back(std::make_tuple(__x.first, __x.second));
                iter = --container.end();
                ret = true;
            }
            return std::make_pair(iter, ret);
        }

        iterator
        erase(iterator __position) {
            auto && container = _container[hash(std::get<0>(*__position))];
            return container.erase(__position);
        }

        size_t
        begin() _GLIBCXX_NOEXCEPT {
            _i = 0;
            return 0;
        }

        size_t
        end() const _GLIBCXX_NOEXCEPT {
            return TSPMAP_BORDER - 1;
        }


        // todo own iterator class
        bool operator++()
        {

        }


    protected:
        size_t _i = 0;
        size_t hash(const key_type &__x) const {
            return static_cast<size_t>(__x) & TSPMAP_BORDER;
        }


    private:
        using _container_type = std::array< allocater_type , TSPMAP_BORDER>;
        _container_type _container;

    };

    // using OpenClustersAtEdges = TspMap;
    // HINT: does not work, because fault in std::map impl - = std::map<double, OpenClusterEdge>;
    using OpenClustersAtEdges = std::map<double, OpenClusterEdge>;

    using WayL = std::list<Node*>;
    WayL m_currentWay;
    WayL m_bestWay;
    double m_currentLength = std::numeric_limits<double>::max();
    double m_bestLength = std::numeric_limits<double>::max();

    // MinMaxClusterAlignment
    std::list<Node*> m_edgesGuarantied;

    struct CeList {
        using CeListType = std::list<ClosestEdges>;
        using CeListOrderType = std::map<double, Node*>;
        CeListOrderType ceListOrder;
        CeListType ceList;
    };

    using DirectAlignments = std::map<double, ClosestEdges>;

    bool m_isLogCollisionDetected = false;
    bool m_isLogFaultDetected = false;
    bool m_isLogOverflowDetected = false;



    size_t m_tspSize = 0;

    /** globals **/
    static size_t tspSize;
    static double tspWidth;
    static double tspHeight;

    /** Ctor **/
    MinMaxClusterAlignment();

protected:
    /** Impl **/
    void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic);

private:
    NodeL createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic);
    std::tuple<MinMaxClusterAlignment::NodeL, MinMaxClusterAlignment::Way *> determineHull(NodeL &input);
    void determineGuarantiedAlignment(NodeL &input, NodeL &hull);
    ClusterGrow determineClusters(NodeL &input, NodeL &hull);
    void alignToHull(std::map<double, Cluster *> &clusters, NodeL &hull);
    MinMaxClusterAlignment::NodeL determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way);
    void determineGroupAlignment(std::list<Node *> &_openNodes);

    void _triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI);
    MinMaxClusterAlignment::Way *_newConqueredWay(bsp::BspNode *node);
    bsp::BspTreeSarah _createBSPTree(NodeL &input);
    void _mergeHull(Way *&way1, const TSP::Point &pWay1Center, Way *&way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter);
    bool _isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);
    DirectAccessState _isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI);
    CrossedState _isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint = nullptr);
    bool _isNormalVectorLeft(const Point &from, const Point &to, const Point &insert);
    double _calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3);
    double _area(const PointL &polygon);
    bool _isCovered(Way *way, Way::iterator &w1I, Way::iterator &fixI);
    void _createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI);
    bool _createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI);
    void _flipWay(Way *way, TSP::Point pCenter);
    bool _isClockwise(TSP::Point pCenter, Way *way);
    MinMaxClusterAlignment::Node *_nextNode(Node *curr, Node * &prev);
    bool _updateHullOrUpdateSimple(std::shared_ptr<Node> hullNodeA, std::shared_ptr<Node> updateNode, NodeL &hull);
    bool _updateAllowed(std::shared_ptr<Node> closest, std::shared_ptr<Node> node, bool isTestOnClique = true);
    void _updateNodes(std::map<double, Cluster *> &clustersSorted, NodeL &hull);
    void _updateNodes2(std::map<double, Cluster *> &clustersSorted, NodeL &hull);
    double _triangleAltitudeOnC(Point c1, Point c2, Point perpendicular);
    bool _isRightSide(const Point &p1, const Point &p2);
    double _calcDegree(Node *node, Node *nodeTested);
    double _calcDegree(Point &node, Point &nodeTested);
    void _insertAsNeighbour(Node *node, Cluster *clu);
    double _correlation(Point a, Point b);
    bool _checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);
    double _getBestAlignEdge(Node *partnerN, Node *cluN, Cluster *clu);

    // MinMaxClusterAlignment
    void _collectClusterEdges(std::map<double, Cluster*> &clustersM);
    bool _searchBestPathByConvexHullAlignment(OpenClustersAtEdges &open, size_t level);
    void _runEvolutionaryGrow(NodeL &hull);
    ClusterEdge *_findClusterToEdge(Node *from, Node *to);
    std::list<double> _insertCluEIfPossible(ClusterEdge *cluE, OpenClustersAtEdges &openN, Node *from, Node *to, Node *middle);
    void _insertClosestTestedPointsSorted(std::map<double, Node *> &closestTestedPointsSorted, Node *node, Node *closest);
    bool _checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);
    float sign(const Point &p1, const Point &p2, const Point &p3);
    template<typename T, typename V>
    std::pair<typename T::iterator, bool> _insertToMap(double dist, T &mapContainer, V &&value);
    CeList::CeListType::iterator _insertToCeList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert);
    void _determineGroupAlignmentRec(std::list<Node *> selectedNodes, std::list<Node *> openNodes);
    double _calcCeListDist(CeList &ceList);
    bool _isAtEdge(std::list<Node*> &openNodes, Node *from, Node *to, std::list<Node *> &selectedNodes);
    CeList::CeListType::iterator __insertToCeListAlongList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert);
    TSP::Point _calcPerpendicularPoint(Node *from, Node *to, Node *normal);
    bool _isNotInsideTriangle(Node *point1, Node *point2, Node *point3, std::list<Node*> &pointsInside);
    Point _correctNodeCenter(Way* wayBiggerVals, Way* waySmallerVals, bsp::BspNode *node, MergeCallState state);
    bool _isLeftSideExEqual(const Point &p1Line, const Point &p2Line, const Point &check);
    void _clearOpenClustersAtEdgesList(std::list<double> &ret, OpenClustersAtEdges &open, size_t s);
    void _finalize();
    void _clearEvolutionaryGrowRecursion(std::list<double> &newClu1AtEdgeIters, std::list<double> &newClu2AtEdgeIters, OpenClustersAtEdges &open, WayL::iterator &wayInsertedI, Cluster *p, double &lengthChange, size_t level, size_t iterN, size_t s);
};

}
