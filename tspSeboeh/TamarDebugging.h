#ifndef TAMARDEBUGGING_H
#define TAMARDEBUGGING_H

#include <ostream>

#include "TamarAlgorithm.h"
//#include <bspTreeVersion/BspNode.h>
//#include <dataStructures/OlavList.h>

namespace TSP {

class TamarDebugging
{
public:
    TamarDebugging();

    static TamarDebugging *getInstance()
    {
        if (m_this == nullptr)
            m_this = new TamarDebugging;
        return m_this;
    }

    void printDebugFile(TamarAlgorithm::Way *hull, TamarAlgorithm::Way *innerPoints, Point &outerHullNavel, Point &innerPointsNavel);

    static void debugApp2016_print(TamarAlgorithm::NavelsMapType &navels, Parameters &para, Indicators &indicators);
    static void debugIsInNavels(TamarAlgorithm::NavelsMapType &navels);
    static void debugIsInNavels(TamarAlgorithm::NavelsMapType::mapped_type &navels);
    static void debugApp2016_printWait(TamarAlgorithm::NavelsMapType &navels, Parameters &para, Indicators &indicators);
    static void debugApp2016_print2(TamarAlgorithm::NavelsType &navelsCopy, Parameters &para, Indicators &indicators);
    static void debugFindWrongOrder(TamarAlgorithm::NavelsType &navels);
protected:
    static std::ofstream m_debugFile;

    static TamarDebugging *m_this;

};

}

#endif // TAMARDEBUGGING_H
