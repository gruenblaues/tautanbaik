/*
 * Copyright 2002-2010 Guillaume Cottenceau.
 *
 * This software may be freely redistributed under the terms
 * of the X11 license.
 *
 */
/*
 * Edited by Sebastian Bohmer under the licence of LGPL V1. 2014
 * The change is the object oriented programming style.
 */

#include "PngImage.h"

//#ifndef __WIN32__
//    #include <unistd.h>
//#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <string>
#include <iostream>
#include <cassert>

#include "StaticHelperTSP.h"

PngImage::PngImage(size_t _width, size_t _height) :
    width(_width), height(_height),
    color_type(PNG_COLOR_TYPE_RGBA),
    bit_depth(8),
    png_ptr(0),
    info_ptr(0),
    number_of_passes(0),
    row_pointers(0),
    m_state(State::ISNULL)
{
    /* initialize stuff */
// TODO [seboeh] fix png_destroy_read/write - clear png_ptr before using new png_ptr.
//    if (png_ptr)
//        png_destroy_write_struct(&png_ptr, &info_ptr);
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
            StaticHelper::logAndThrow<std::invalid_argument>("[ctor] png_create_write_struct failed");

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
            png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
            png_ptr = nullptr;
            StaticHelper::logAndThrow<std::invalid_argument>("[ctor] png_create_info_struct failed");
    }
    png_info_init(info_ptr);

    if (setjmp(png_jmpbuf(png_ptr)))
    {
            png_destroy_write_struct(&png_ptr, &info_ptr);
            png_ptr = nullptr; info_ptr = nullptr;
            StaticHelper::logAndThrow<std::invalid_argument>("[ctor] Error during info_init");
    } else {        // else, necessary, because loggAndThrow is not detected by clang.
        number_of_passes = png_set_interlace_handling(png_ptr);

        info_ptr->color_type = color_type;
        info_ptr->bit_depth = bit_depth;
        info_ptr->color_type = color_type;
        info_ptr->width = width;
        info_ptr->height = height;
        png_read_update_info(png_ptr, info_ptr);        /// png_read_update_info - is necessary to allocate the memory later with png_get_rowbytes().

        if (setjmp(png_jmpbuf(png_ptr)))
                StaticHelper::logAndThrow<std::invalid_argument>("[ctor] Error during png_read_update_info");

        allocRowPointers();

        m_textChunk = new png_text[10];         /// HINT: We assume a maximum of 10 text chunks.
        for (int i=0; i < 10; ++i)
        {
            m_textChunk[i].key = nullptr;
            m_textChunk[i].text = nullptr;
            m_textChunk[i].text_length = 0;
            m_textChunk[i].compression = 0;
        }
    }
}

PngImage::~PngImage()
{
    if (png_ptr != nullptr && info_ptr != nullptr && m_state | State::ISWRITEPTR ) {
        // TODO SB: does not work : png_destroy_write_struct(&png_ptr, &info_ptr);
        //png_destroy_write_struct(&png_ptr, &info_ptr);
    } else if (png_ptr != nullptr && info_ptr != nullptr && m_state | State::ISREADPTR ) {
        // HINT: not used "end_info_ptr" png_destroy_read_struct(&png_ptr, &info_ptr, &end_info_ptr);
    }
    if (png_ptr != nullptr && info_ptr != nullptr && ((m_state | State::ISWRITEPTR) || (m_state | State::ISREADPTR))) {
        // TODO SB: does not work: png_destroy_info_struct(png_ptr, &info_ptr);
		png_destroy_info_struct(png_ptr, &info_ptr);
    }
    if (row_pointers) {
        freeRowPointers();
    }

	/* HINT: does not work.
	for (int i=0; i < 10; ++i)
	{
		if (m_textChunk[i].key != nullptr) {
			delete m_textChunk[i].key;
			delete m_textChunk[i].text;
		}
	}
	*/
	// Valgrind sad mismath delete [] - TODO [seboeh] differ between png creation and new here. delete [] m_textChunk;
}

void PngImage::freeRowPointers()
{
    if (row_pointers) {
        for (size_t y=0; y<height; ++y) {
            delete [] row_pointers[y];
        }
        delete [] row_pointers;
    }
    row_pointers = NULL;
}

void PngImage::allocRowPointers()
{
    row_pointers = new png_bytep[height];
    for (size_t y=0; y<height; y++) {
        size_t rowbytes = png_get_rowbytes(png_ptr, info_ptr);
        row_pointers[y] = new png_byte[rowbytes];
    }
}

png_byte* PngImage::row_pointers_at(size_t y) {
    assert(y < height);
    return row_pointers[y];
}


void PngImage::read_png_file(std::string file_name)
{
        m_state &= ~State::ISNULL;
        if (m_state | State::ISWRITEPTR) {
            // TODO SB: does not work right now: png_destroy_write_struct(&png_ptr, &info_ptr);
            // TODO SB: does not work right now: png_destroy_info_struct(png_ptr, &info_ptr);
            freeRowPointers();
            m_state &= ~State::ISWRITEPTR;
        }

        png_byte header[8];    // 8 is the maximum size that can be checked

        /* open file and test for it being a png */
        FILE *fp = fopen(file_name.c_str(), "rb");
        if (!fp)
                StaticHelper::logAndThrow<std::invalid_argument>("[read_png_file] File %s could not be opened for reading", file_name.c_str());
		size_t readCount = fread(header, 1, 8, fp);
		if (png_sig_cmp(header, 0, readCount))
                StaticHelper::logAndThrow<std::invalid_argument>("[read_png_file] File %s is not recognized as a PNG file", file_name.c_str());


        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
                StaticHelper::logAndThrow<std::invalid_argument>("[read_png_file] png_create_read_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
        {
                png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
                png_ptr = nullptr;
                StaticHelper::logAndThrow<std::invalid_argument>("[read_png_file] png_create_info_struct failed");
        }

        png_init_io(png_ptr, fp);

        if (setjmp(png_jmpbuf(png_ptr)))
                StaticHelper::logAndThrow<std::invalid_argument>("[read_png_file] Error during init_io");

        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        width = png_get_image_width(png_ptr, info_ptr);
        height = png_get_image_height(png_ptr, info_ptr);
        color_type = png_get_color_type(png_ptr, info_ptr);
        bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        allocRowPointers();
        m_state |= State::ISREADPTR;

        png_read_image(png_ptr, row_pointers);

        if (setjmp(png_jmpbuf(png_ptr)))
        {
                png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
                png_ptr = nullptr;
                info_ptr = nullptr;
                StaticHelper::logAndThrow<std::invalid_argument>("[read_png_file] Error during read_image");
        }

        fclose(fp);
}


void PngImage::write_png_file(std::string file_name)
{
        m_state &= ~State::ISNULL;
        if (m_state | State::ISREADPTR) {
            /// No destroy of png_ptr and info_ptr here, because they are not new created. They are created at constructor.
            /// Using row_pointers, which are allocation by row_pointers_at().
            m_state &= ~State::ISREADPTR;
        }

        /* create file */
        FILE *fp = fopen(file_name.c_str(), "wb");
        if (!fp)
                StaticHelper::logAndThrow<std::invalid_argument>("[write_png_file] File %s could not be opened for writing", file_name.c_str());

        png_init_io(png_ptr, fp);

        if (setjmp(png_jmpbuf(png_ptr)))
                StaticHelper::logAndThrow<std::invalid_argument>("[write_png_file] Error during init_io");

        /* write header */
        png_set_IHDR(png_ptr, info_ptr, width, height,
                     bit_depth, color_type, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

        png_write_info(png_ptr, info_ptr);

        if (setjmp(png_jmpbuf(png_ptr)))
                StaticHelper::logAndThrow<std::invalid_argument>("[write_png_file] Error during writing header");

        /* write bytes */
        png_write_image(png_ptr, row_pointers);

        if (setjmp(png_jmpbuf(png_ptr)))
                StaticHelper::logAndThrow<std::invalid_argument>("[write_png_file] Error during writing bytes");

        /* end write */
        png_write_end(png_ptr, NULL);

        if (setjmp(png_jmpbuf(png_ptr)))
                StaticHelper::logAndThrow<std::invalid_argument>("[write_png_file] Error during end of write");

        fclose(fp);
}

void PngImage::checkPixelFormat() throw (std::invalid_argument)
{
    if (png_get_color_type(png_ptr, info_ptr) == PNG_COLOR_TYPE_RGB)
            StaticHelper::logAndThrow<std::invalid_argument>("[check pixel format] input file is PNG_COLOR_TYPE_RGB but must be PNG_COLOR_TYPE_RGBA "
                   "(lacks the alpha channel)");

    if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_RGBA)
            StaticHelper::logAndThrow<std::invalid_argument>("[check pixel format] color_type of input file must be PNG_COLOR_TYPE_RGBA (%d) (is %d)",
                   PNG_COLOR_TYPE_RGBA, png_get_color_type(png_ptr, info_ptr));

}

void PngImage::set_text_chunk(PngText * txtArray)
{
    png_set_text(png_ptr, info_ptr, txtArray, 1);
}

PngImage::PngText * PngImage::get_text_chunk(char *key, int * num) throw (std::invalid_argument)
{
    assert(key!=NULL);
    assert(num!=NULL);
    *num=0;
    png_get_text(png_ptr, info_ptr, &m_textChunk, num);
    if (setjmp(png_jmpbuf(png_ptr)))
            StaticHelper::logAndThrow<std::invalid_argument>("[get_text_chunk] Error reading text chunks.");
    return m_textChunk;
}
