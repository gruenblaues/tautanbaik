#pragma once

#include <list>
#include <tuple>
#include <map>

#include "dataStructures/commonstructures.h"
#include "dataStructures/OlavList.h"
#include "bspTreeVersion/BspNode.h"
#include "TamarAlgorithmHelper.h"
#include "Parameters.h"

namespace TSP {
namespace exclmerg {

class ExcludingMerge
{
public:

    using Node = comdat::Node;
    using Result = comdat::Result;
    using Way = oko::OlavList;
    using PointsSet = std::list<Node*>;

    struct Anchor
    {
        Node* point = nullptr;
        std::list<PointsSet>::iterator edgeI;
        double distAlign = std::numeric_limits<double>::max();
    };
    using InnerPointsAnchorType = std::list<Anchor>;
    using AnchorMap = std::map<double, Anchor>;

    PointsSet findHullEdges(Way &hull, const PointsSet &innerPoints, double maxDist)
    {
        std::map<double, Node*> ret;

        Point center(0,0);
        for (const auto &p : innerPoints)
        {
            center += p->point;
        }
        center /= innerPoints.size();

        auto hullI = hull.begin();
        Node *prev = const_cast<Node*>(Way::prevChild(&hullI));
        while (!hullI.end())
        {
            Node *hullP = hullI.nextChild();
            Point mid = (prev->point + hullP->point) / 2;
            double dist = defaultFnc::defaultDistanceFnc(mid, center);
            double length = defaultFnc::defaultDistanceFnc(prev->point, hullP->point);
            ret.insert({dist/std::sqrt(length), hullP});
            prev = hullP;
        }

        std::list<Node*> retList;
        for (const auto &el : ret)
            retList.push_back(el.second);
        return retList;
    }

    std::tuple<Result, double> search(const PointsSet &hullNodes, const PointsSet &innerPoints)
    {
        Result retResult;
        if (hullNodes.empty())
            return std::make_tuple(retResult, std::numeric_limits<double>::max());

        // preparation
        m_currentFroms = hullNodes;
        m_currentPoints = innerPoints;


        while (!m_currentFroms.empty() && !m_currentPoints.empty())
        {
            Node *from = m_currentFroms.front();
            m_currentFroms.pop_front();
            std::list<ClosestEdges> navel;
            ClosestEdges ce;
            ce.fromNode = from;
            ce.toNode = const_cast<Node*>(Way::nextChild(from));

            // first align all points
            for (const auto &node : m_currentPoints)
            {
                ce.insertNode = node;
                if (navel.empty())
                    navel.push_back(ce);
                else
                    TSPHelper::findBestSingleSearch(navel, ce);
            }

            // filter
            for (auto ceI = navel.begin();
                 ceI != navel.end(); ++ceI)
            {
                double distBefore;
                if (ceI == navel.begin())
                {
                    distBefore = defaultFnc::defaultDistanceFnc(ceI->fromNode->point, ceI->insertNode->point);
                } else {
                    auto nextI = ceI; ++nextI;
                    if (nextI != navel.end())
                    {
                        distBefore = defaultFnc::defaultDistanceFnc(ceI->insertNode->point, nextI->insertNode->point);
                    } else {
                        distBefore = defaultFnc::defaultDistanceFnc(ceI->insertNode->point, ceI->toNode->point);
                    }
                }
                double distAlt = findAlternativeDist(ceI->insertNode, distBefore);
                if (distAlt < distBefore)
                {
                    ceI = navel.erase(ceI);
                }
            }

            if (!navel.empty())
            {
                // write result
                comdat::ResultElement res;
                res.hullEdgeFrom = navel.front().fromNode;
                for (const auto ce : navel)
                    res.innerNodes.push_back(ce.insertNode);
                retResult.push_back(res);
            }

            // clear
            for (const auto &ce : navel)
                m_currentPoints.remove(ce.insertNode);
        }

        double dist = 0;
        for (const comdat::ResultElement &res : retResult)
            dist += recalculateSeboehDist(res.hullEdgeFrom, res.innerNodes);

        return std::make_tuple(retResult, dist);
    }

private:

    double findAlternativeDist(Node *el, double distBefore)
    {
        for (const auto &from : m_currentFroms)
        {
            Node *to = const_cast<Node*>(Way::nextChild(from));
            double dist = TSPHelper::seboehDist(from, to, el);
            if (dist < distBefore)
                return dist;
        }
        return std::numeric_limits<double>::max();
    }

    double recalculateSeboehDist(Node *from, const PointsSet &navel)
    {
        Node *to = const_cast<Node*>(Way::nextChild(from));

        double distTotal = 0;
        for (const auto &node : navel)
        {
            distTotal += TSPHelper::seboehDist(from, to, node);
        }

        return distTotal;
    }

    /**
      Attributes
      */

    std::list<Node*> m_currentFroms;
    std::list<Node*> m_currentPoints;


};


}
}
