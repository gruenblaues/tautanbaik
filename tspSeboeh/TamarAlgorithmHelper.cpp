#include "TamarAlgorithmHelper.h"

#include "Parameters.h"


using namespace TSP;
using namespace TSPHelper;

double TSPHelper::seboehDist(const Node *p1, const Node *p2, const Node *pNew)
{
    return seboehDist(p1->point, p2->point, pNew->point);
}

double TSPHelper::seboehDist(ClosestEdges &ce)
{
    return seboehDist(ce.fromNode, ce.toNode, ce.insertNode);
}

double TSPHelper::seboehDist(const Point &p1, const Point &p2, const Point &pNew)
{
    // TODO use m_para instead of defaultFnc.
    double detour = defaultFnc::defaultDistanceFnc(p2, pNew)
            + defaultFnc::defaultDistanceFnc(p1, pNew)
            - defaultFnc::defaultDistanceFnc(p1, p2);

    //return detour;
//    Point center((p1.x()+p2.x())/2.0, (p1.y()+p2.y())/2.0);
//    double m = Parameters::distanceFnc(center, pNew);
//    return localDecision(calcAreaOfTriangle(p1, p2, pNew), detour, m);
//    return deviation * m;
    return detour;
}

double TSPHelper::seboehHyperDist(const Point &p1, const Point &p2, const Point &pNew)
{
    // TODO use m_para instead of defaultFnc.
    double height = defaultFnc::defaultDistanceFnc(p2, pNew)
            + defaultFnc::defaultDistanceFnc(p1, pNew);

    double detour =
            sqrt(std::pow((p1.x()-pNew.x()),2) + std::pow((p1.y()-pNew.y()),2) + height*height)
            - defaultFnc::defaultDistanceFnc(p1, p2);

    return detour;
}


double TSPHelper::correctDistHelper(const std::list<ClosestEdges> &navelRoot)
{
    if (navelRoot.empty())
        return 0;
    double dist = 0;
    Node *prevNode = navelRoot.front().fromNode;
    for (const auto &ce : navelRoot)
    {
        // HINT: it is using candidate here also.
        dist += seboehDist(prevNode->point, ce.toNode->point, ce.insertNode->point);
        prevNode = ce.insertNode;
    }
    return dist;
}

Point
TSPHelper::testPointsInOrder(NavelsZippedType::iterator thirdPointI, Node *secondPointN, Node *firstPointN)
{
    auto pointBase = firstPointN->point;
    auto pointNext = secondPointN->point;
    auto pointNNext = thirdPointI->insertNode->point;
    auto vect1 = pointNext - pointBase;
    auto vect2 = pointNNext - pointBase;
    auto vectRes = vect2 - vect1;

    return vectRes;
}


std::tuple<std::list<ClosestEdges>::iterator
, std::list<ClosestEdges>::iterator>
TSPHelper::findFromTo(Node *insert, std::list<ClosestEdges> &ceList)
{
    if (ceList.empty())
        return std::make_tuple(ceList.end(), ceList.end());

    Node *rootFrom = ceList.front().fromNode;
    Node *rootTo = ceList.back().toNode;
    double startDist = seboehDist(rootFrom, rootTo, insert);

    // find to
    std::list<ClosestEdges>::iterator toI = ceList.end();
    double bestDist = startDist;
    for (auto ceI = ceList.begin();
         ceI != ceList.end(); ++ceI)
    {
        double dist = seboehDist(rootFrom, ceI->insertNode, insert);
        if (dist < bestDist)
        {
            bestDist = dist;
            toI = ceI;
        }
    }

    // find from
    bool isCrossed = false;
    bool bestIsCrossed = false;
    std::list<ClosestEdges>::iterator fromI = ceList.end();
    bestDist = startDist;
    for (auto ceI = ceList.begin();
         ceI != ceList.end(); ++ceI)
    {
        if (ceI == toI)
            isCrossed = true;
        double dist = seboehDist(ceI->insertNode, rootTo, insert);
        if (dist < bestDist)
        {
            bestDist = dist;
            fromI = ceI;
            bestIsCrossed = isCrossed;
        }
    }

    /// secure check on point before toI. See test63.
    if (toI != ceList.end())
    {
        auto nextI = toI; ++nextI;
        Node *nextN;
        if (nextI == ceList.end())
        {
            nextN = toI->toNode;
        } else {
            nextN = nextI->insertNode;
        }

        // check if toI is before
        auto vectRes = testPointsInOrder(toI, insert, rootFrom);
        if (
                vectRes.x() < 0
                ||
                vectRes.y() < 0
            )
        {
            if (fromI != ceList.end())
            {
                auto vectResFrom = testPointsInOrder(fromI, insert, rootFrom);
                if (
                        vectResFrom.x() > 0
                        ||
                        vectResFrom.y() > 0
                    )
                {
                    bestIsCrossed = true;
                } else {
                    auto vectResFromTo = testPointsInOrder(fromI, toI->insertNode, rootFrom);
                    if (
                            vectResFromTo.x() < 0
                            ||
                            vectResFromTo.y() < 0
                        )
                    {
                        toI = nextI;
                    } else {
                        bestIsCrossed = true;       // because, fromI is after toI.
                    }
                }
            } else {
                fromI = toI;
                toI = ceList.end();
            }
        }
    }
    // TODO [seboeh] same as above could be helpful for fromI too.

    if (bestIsCrossed)
    {
        std::swap(fromI, toI);
    }

    /// check if points are in correct order according to ceList.
    if (fromI != ceList.end() && toI != ceList.end())
    {
        bool isFromReached = false;
        for (const auto &ce : ceList)
        {
            if (ce.insertNode == fromI->insertNode)
                isFromReached = true;
            if (ce.insertNode == toI->insertNode)
                break;
        }
        if (!isFromReached)
            std::swap(fromI, toI);
    }

    return std::make_tuple(fromI, toI);
}

bool TSPHelper::isNormalVectorLeft(Node *fromNode, Node* toNode, Node* insertNode)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());
    Point to = fromNode->point;
    Point from = toNode->point;

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point normalVectLeft(from.x() - straight.y()
                         , from.y() + straight.x());
    normalVectLeft = Point(-normalVectLeft.y(), normalVectLeft.x());
    // TODO [seboeh] use m_para.
    double distLeft = defaultFnc::defaultDistanceFnc(normalVectLeft, insertNode->point);

    Point normalVectRight(from.x() + straight.y()
                          , from.y() - straight.x());
    normalVectRight = Point(-normalVectRight.y(), normalVectRight.x());
    double distRight = defaultFnc::defaultDistanceFnc(normalVectRight, insertNode->point);

    return distLeft < distRight;
}


std::tuple<std::list<ClosestEdges>::iterator, double>
TSPHelper::findBestSingleSearch(std::list<ClosestEdges> &navelRoot, const ClosestEdges &ce)
{
    if (navelRoot.empty())
        return std::tuple<std::list<ClosestEdges>::iterator, double>(navelRoot.end(), std::numeric_limits<double>::max());

    double bestDist = std::numeric_limits<double>::max();
    Node *prevNode = navelRoot.front().fromNode;
    // HINT: One edge (last prevNode to navelRoot.front().toNode) is missing
    // because we do not need to check, it is on the opposite site of ce.insertNode.
    std::list<ClosestEdges>::iterator bestI = navelRoot.end();
    for (auto nextCeI = navelRoot.begin();
         nextCeI != navelRoot.end(); ++nextCeI)
    {
        assert(nextCeI->insertNode != ce.insertNode);

        /// HINT: Due to test63, it is necessary to calculate the area of triangular also into seboehDist.
        double distNext = seboehDist(prevNode, nextCeI->insertNode, ce.insertNode);
        if (distNext < bestDist)
        {
            bestDist = distNext;
            bestI = nextCeI;
        }
        prevNode = nextCeI->insertNode;
    }
    double dist = seboehDist(prevNode, navelRoot.back().toNode, ce.insertNode);
    if (dist < bestDist)
    {
        return std::tuple<std::list<ClosestEdges>::iterator, double>(navelRoot.end(), dist);
    } else {
        return std::tuple<std::list<ClosestEdges>::iterator, double>(bestI, bestDist);
    }
}


TSPHelper::ClosestEdges TSPHelper::updateBestResort(Node *fromNode, Node *toNode, Node *insert, NavelsZippedType::iterator &iter, NavelsZippedType &ceList, ClosestEdges *newCeP)
{
    ClosestEdges ce(insert);
    if (iter == ceList.end())
    {
        if (ceList.empty())
        {
            ce.fromNode = fromNode;
            ce.toNode = toNode;		// TODO [seboeh] replace toNode with getNextChild().
        } else {
            ce.fromNode = ceList.back().insertNode;
            ce.toNode = toNode;
        }
    } else {		// normal
        ce.fromNode = iter->fromNode;
        ce.toNode = iter->toNode;        // toNode is Root-Anchor-toNode everytime.
        if (newCeP == nullptr
                && insert->candidateState == Node::CandidateState::ELECTED)
            iter->fromNode = ce.insertNode;      // insert Candidate in correct list, but does not set the fromNode.
        if (newCeP == nullptr
                && insert->candidateState == Node::CandidateState::ELECTED)
            iter->dist = seboehDist(*iter);
    }
    ce.dist = seboehDist(ce);
    if (newCeP != nullptr)
        *newCeP = ce;
    return ce;
}


// HINT: This algorithm insertBestResortedPos() works only clock-wise, i.e. not in opposit direction to from-to. See test19.
std::list<ClosestEdges>::iterator TSPHelper::insertBestResortedPos(Node *from, Node *to, Node *insert, std::list<ClosestEdges> &ceList, int level)
{
    /// preparation
    if (level == 0)
    {
        for (auto &ce : ceList)
        {
            ce.insertNode->isInserted = false;
        }
    }
    /// recursive algorithm
    auto posRes = findFromTo(insert, ceList);       // TODO [seboeh] if from-to is edge on new added insert in level 0, we have no resorting. We can terminate and correct level 0 insert to standard insert.
    auto fromI = std::get<0>(posRes);
    auto toI = std::get<1>(posRes);
    // HINT: if below, expect that there could be a to node, which is far away from fromNode, and is not better than hull-edge.
    // Other way around with to / from Node also. This is indicated by end(). Additionally, if fromI == toI, it is not clear which
    // position of best edge, therefore we need an findBestSingleSearch() also.
    if (fromI == toI
            || (fromI != ceList.end() && toI != ceList.end()        // TODO ....improve fromI == end && toI != end -> isNormalVectorLeft
                 && isNormalVectorLeft(fromI->insertNode, toI->insertNode, insert)
                ))
    {       /// simple insert
        ClosestEdges ce(insert);
        auto bestRes = findBestSingleSearch(ceList, ce);
        auto iter = std::get<0>(bestRes);
        return ceList.insert(iter, updateBestResort(from, to, insert, iter, ceList));
    } else {
        /// resort the result
        std::list<Node*> reInserts;
        auto reinsertI = fromI;
        if (fromI == ceList.end())
        {
            reinsertI = ceList.begin();
        } else {
            ++reinsertI;
        }
        for (; reinsertI != toI; ++reinsertI)
        {
            if (reinsertI->insertNode->isInserted)
            {       // already resorted - it is enough to directly insert at current point.
                // align at simple mode. Happens only for resorted elements. This is no happen when level == 0.
                ClosestEdges ce(insert);
                auto bestRes = findBestSingleSearch(ceList, ce);
                auto iter = std::get<0>(bestRes);
                return ceList.insert(iter, updateBestResort(from, to, insert, iter, ceList));
            } else {
                reInserts.push_back(reinsertI->insertNode);
            }
        }

        if (reInserts.empty())
        {
            return ceList.insert(toI, updateBestResort(from, to, insert, toI, ceList));
        } else {
            /// prepare insert node
            ClosestEdges ce(insert);
            if (fromI == ceList.end())
            {
                ce.fromNode = ceList.front().fromNode;
            } else {
                ce.fromNode = fromI->insertNode;
            }
            if (toI == ceList.end())
            {
                ce.toNode = ceList.back().toNode;
            } else {
                ce.toNode = toI->toNode;
                toI->fromNode = insert;
                toI->dist = seboehDist(*toI);
            }
            ce.dist = seboehDist(ce);
            ce.insertNode->isInserted = true;

            /// clear points between
            reinsertI = fromI;
            if (fromI == ceList.end())
            {
                reinsertI = ceList.begin();
            } else {
                ++reinsertI;
            }
            for (;reinsertI != toI;)
            {
                reinsertI = ceList.erase(reinsertI);
            }

            /// insert current element
            ceList.insert(toI, ce);

            /// recursion
            for (auto newEl : reInserts)
            {
                if (!newEl->isInserted)
                    insertBestResortedPos(from, to, newEl, ceList, level+1);
            }

            /// back from recursion
            auto findI = std::find_if(ceList.begin(), ceList.end()
                                      , [insert] (const ClosestEdges &val)
            {
                return insert == val.insertNode;
            });
            assert(findI != ceList.end());
            return findI;
        }
    }
}



std::list<ClosestEdges>::iterator
TSPHelper::findBestSingleCeAndAdd(Node *toNode, Node *fromNode, const ClosestEdges &element, std::list<ClosestEdges> &ceList)
{
    NavelsZippedType::iterator iter = insertBestResortedPos(fromNode, toNode, element.insertNode, ceList);

    return iter;
}



ClosestEdges TSP::closestEdge(WayH *hull, Node *innerNode)
{
    auto closest = hull->getOrigin()->selectFirstClosestEdge(innerNode->point);
    const bsp::Edge* edge = std::get<0>(closest);
    // we have to check start/end, because there are not clear which direction from edge quad-tree.
    ClosestEdges ce(innerNode);
    if (WayH::nextChild(edge->startPoint) != edge->endPoint)
    {
        // we could detect the opposite direction in BSP, because there are two edges for each polygon-edge in BSP tree.
        ce.fromNode = edge->endPoint;
        assert(ce.fromNode != ce.insertNode);
        ce.toNode = edge->startPoint;
    } else {
        ce.fromNode = edge->startPoint;
        assert(ce.fromNode != ce.insertNode);
        ce.toNode = edge->endPoint;
    }
    ce.dist = seboehDist(ce.fromNode->point, ce.toNode->point, ce.insertNode->point);
    return ce;
}


Node* TSP::getNavelPosition(WayH *innerPointWay, WayH *hull, Node* &hullPosition)
{
    double minNavelGravity = std::numeric_limits<double>::max();
    Node * hullBestNode = nullptr;
    Node * innerPointBestNode = nullptr;

    // TODO HINT: there is an heuristic on getNavelPosition in repository.
    for (WayH::iterator innerPointI = innerPointWay->begin();
         !innerPointI.end(); )
    {
        /// calculate
        auto closestCe = closestEdge(hull, &innerPointI);

        Node *firstP = closestCe.fromNode;
        Node *secondP = closestCe.toNode;

        /// iterate
        const Node * innerPointNode = innerPointI.nextChild();
        const Node * innerPointNextNode = WayH::nextChild(innerPointNode);

        // There is no search along the hull necessary, if we have only one inner point.
        // In this case, the closest edge is the best edge to align.
        // ovule takes the next closest area/edge.
        if (innerPointWay->size() == 1)
        {
            hullPosition = const_cast<Node*>(firstP);
            return const_cast<Node*>(innerPointNode);
        }

        Node* totalEndP = secondP;
        size_t i=0;
        do
        {
            ++i;
            if (i > innerPointWay->size()) break;
            if (secondP == nullptr) break;
            // HINT: Calculation is here not hullPfirst to innerPnext, because here, innerPoints are clockwise, because result of newConqueredWay().
            double navelGravity =
                    defaultFnc::defaultDistanceFnc(firstP->point, innerPointNode->point)
                    + defaultFnc::defaultDistanceFnc(secondP->point, innerPointNextNode->point)
                    - defaultFnc::defaultDistanceFnc(firstP->point, secondP->point)
                    - defaultFnc::defaultDistanceFnc(innerPointNode->point, innerPointNextNode->point);

            if (navelGravity < minNavelGravity)
            {
                hullBestNode = const_cast<Node*>(firstP);
                innerPointBestNode = const_cast<Node*>(innerPointNode);
                minNavelGravity = navelGravity;
            }

            // Go one step further.
            firstP = secondP;
        } while ((secondP = const_cast<Node*>(WayH::nextChild(secondP))) != totalEndP);

    }       // for all points in innerPoints list.

    hullPosition = hullBestNode;
    return innerPointBestNode;
}

bool TSPHelper::isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    // barycentric coordinates.
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        return false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        return false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        return false;
    return true;
}
