#include "TspAlgoNaiveIterative.h"

// TODO [seboeh] increase stack depth.
#define NATIVE_ITERATIVE_STACK_DEPTH 80000000

TSP::TspAlgorithm::InputCitiesPointerType * TSP::TspAlgoNaiveIterative::createNewWay(InputCitiesPointerType::iterator openNodeIter, InputCitiesPointerType *openPointsBasic)
{
    InputCitiesPointerType * points = new InputCitiesPointerType();
    for (InputCitiesPointerType::iterator cityIter = openPointsBasic->begin();
         cityIter != openPointsBasic->end(); ++cityIter)
    {
        if (*(*cityIter) == **openNodeIter) {
            continue;
        }
        points->push_back(*cityIter);
    }

    return points;
}

void TSP::TspAlgoNaiveIterative::buildTree(TSP::DT::DTNode::NodeType node, float /*level*/, TSP::TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    Way currentWay;
    m_length = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    currentWay.clear();
    currentWay.push_back(*openPointsBasic->front());
    InputCitiesPointerType* points = createNewWay(openPointsBasic->begin(), openPointsBasic);
    recursiveForLoops(currentWay, points);
    incProgress(progressPercentage);
    points->clear();
    delete points;

    if (m_isCanceled)
    {
        // set any way - like the input, because we want valid m_resultLength values at further processing.
        m_shortestWay.clear();
        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
             openNodeIter != openPointsBasic->end(); ++openNodeIter)
        {
            m_shortestWay.push_back(**openNodeIter);
        }
        m_mutex.lock();
        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
        m_mutex.unlock();
    }
    for (TSP::Way::WayType::const_iterator pointI = m_shortestWay.way()->begin();
         pointI != m_shortestWay.way()->end(); ++pointI)
    {
        Way w;
        w.push_back(*pointI);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial ways - drop this tree of ways.
        (*newChildIter)->parent(node);
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}

void TSP::TspAlgoNaiveIterative::recursiveForLoops(TSP::Way &currentWay, InputCitiesPointerType * points)
{
    if ((std::chrono::system_clock::now() - m_startTime) > std::chrono::minutes(m_para.m_naiveRunMinutes)
            || (m_stackDepth > NATIVE_ITERATIVE_STACK_DEPTH))
    {
        m_isCanceled = true;
        return;
    }
    if (points->empty())
    {
        double length = currentWay.length(m_para);
        length += m_para.distanceFnc(*(currentWay.front()), *(currentWay.back()));
        if (length < m_length)
        {
            m_shortestWay.clear();
            m_shortestWay = currentWay;
            m_length = length;
        }
    } else {
        double length = currentWay.length(m_para);
        Point lastPoint = *currentWay.back();
        ++m_stackDepth;
        for (InputCitiesPointerType::iterator openNodeIter = points->begin();
             openNodeIter != points->end(); ++openNodeIter)
        {
            double dist = m_para.distanceFnc(lastPoint, **openNodeIter);
            dist += m_para.distanceFnc(**openNodeIter, *(currentWay.front()));
            if ((dist + length) > m_length)
                continue;
            currentWay.push_back(**openNodeIter);
            InputCitiesPointerType* newPoints = createNewWay(openNodeIter, points);
            recursiveForLoops(currentWay, newPoints);
            newPoints->clear();
            delete newPoints;
            currentWay.pop_back();
        }
        --m_stackDepth;
    }
}
