#pragma once

#include <list>
#include <tuple>
#include <map>

#include "dataStructures/commonstructures.h"
#include "dataStructures/OlavList.h"
#include "bspTreeVersion/BspNode.h"
#include "TamarAlgorithmHelper.h"
#include "Parameters.h"

namespace TSP {
namespace expsea {

class ExponentSearch
{
public:

    using Node = comdat::Node;
    using Result = comdat::Result;
    using Way = oko::OlavList;
    using PointsSet = std::list<Node*>;

    struct Anchor
    {
        Node* point = nullptr;
        std::list<PointsSet>::iterator edgeI;
        double distAlign = std::numeric_limits<double>::max();
    };
    using InnerPointsAnchorType = std::list<Anchor>;
    using AnchorMap = std::map<double, Anchor>;

    PointsSet findHullEdges(Way &hull, const PointsSet &innerPoints, double maxDist)
    {
        std::list<Node*> ret;

        auto hullI = hull.begin();
        Node *prev = const_cast<Node*>(Way::prevChild(&hullI));
        Node *lastInserted = nullptr;
        while (!hullI.end())
        {
            Node *hullP = hullI.nextChild();
            for (const auto &innerP : innerPoints)
            {
                double dist = defaultFnc::defaultDistanceFnc(hullP->point, innerP->point);
                if (dist < maxDist)
                {
                    if (lastInserted != prev)
                        ret.push_back(prev);
                    ret.push_back(hullP);
                    lastInserted = hullP;
                    break;
                }
            }
            prev = hullP;
        }
        if (ret.front() == ret.back())      // because of prev, --hull.begin() can be inside list twice.
            ret.pop_front();

        return ret;
    }

    std::tuple<Result, double> searchV1(const PointsSet &hullNodes, const PointsSet &innerPoints)
    {
        Result retResult;
        if (hullNodes.empty())
            return std::make_tuple(retResult, std::numeric_limits<double>::max());

        // pre sort, because to prevent of FIR (finite input response) also called start/init-problem.
        std::map<double, std::tuple<Node*, Node*>> firPresortHulldist;
        for (const Node* point : innerPoints)
        {
            Node *bestHull = nullptr;
            double bestDist = std::numeric_limits<double>::max();
            for (const Node* hullp : hullNodes)
            {
                const Node *hullNext = Way::nextChild(hullp);
                double dist = TSPHelper::seboehDist(hullp, hullNext, point);
                if (dist < bestDist)
                {
                    bestHull = const_cast<Node*>(hullp);
                    bestDist = dist;
                }
            }
            firPresortHulldist.insert({bestDist, std::tuple<Node*,Node*>(const_cast<Node*>(point), bestHull)});
        }

        // recursion
        m_currentAlignments.clear();
        for (const auto &point : hullNodes)
        {
            PointsSet alignment;
            alignment.push_back(point);
            m_currentAlignments.push_back(alignment);
        }
        auto begin = innerPoints.cbegin();
        auto end = innerPoints.cend();
        recursiveExpSearch(begin, end, 0);


        for (const auto &points : m_bestAlignments)
        {
            if (points.size() > 1)
            {
                comdat::ResultElement resEl;
                auto innerI = points.begin();
                resEl.hullEdgeFrom = *innerI;
                ++innerI;

                for (; innerI != points.end(); ++innerI)
                {
                    resEl.innerNodes.push_back(*innerI);
                }

                retResult.push_back(resEl);
            }
        }

        return std::make_tuple(retResult, m_bestDist);
    }


    std::tuple<Result, double> searchV2(const PointsSet &hullNodes, const PointsSet &innerPoints)
    {
        // pre-condition
        Result retResult;
        if (hullNodes.empty())
            return std::make_tuple(retResult, std::numeric_limits<double>::max());

        // preparation
        m_currentAlignments.clear();
        for (const auto &point : hullNodes)
        {
            PointsSet alignment;
            alignment.push_back(point);
            m_currentAlignments.push_back(alignment);
        }


        // run
        for (auto &alignment : m_currentAlignments)
        {
            auto sortedInner = preSort(innerPoints, alignment.front());
            for (const auto &inner : sortedInner)
            {
                double dist = std::get<0>(findBestAlignment(alignment, inner.second));
                auto findI = m_hullAlignment.find(inner.second);
                if (findI == m_hullAlignment.end())
                {
                    m_hullAlignment.insert({inner.second, std::tuple<Node*,double>(alignment.front(), dist)});
                }
                else
                {
                    if (dist < std::get<1>(findI->second))
                    {
                        findI->second = std::tuple<Node*, double>(alignment.front(), dist);
                    }
                }
            }
        }

        // write result
        for (auto &alignment : m_currentAlignments)
        {
            PointsSet newAlignInner;
            Node *hullFrom = alignment.front();
            for (const auto &innerAligned : m_hullAlignment)
            {
                if (std::get<0>(innerAligned.second) == hullFrom)
                    newAlignInner.push_back(innerAligned.first);
            }
            if (!newAlignInner.empty())
            {
                comdat::ResultElement resEl;
                resEl.hullEdgeFrom = hullFrom;
                auto sortedAlignInner = preSort(newAlignInner, hullFrom);
                for (const auto &inner : sortedAlignInner)
                {
                    resEl.innerNodes.push_back(const_cast<Node*>(inner.second));
                }
                retResult.push_back(resEl);
            }
        }

        return std::make_tuple(retResult, m_bestDist);
    }

    std::tuple<Result, double> search_froAnchorsV1(const PointsSet &hullNodes, const PointsSet &innerPoints)
    {
        Result retResult;
        if (hullNodes.empty())
            return std::make_tuple(retResult, std::numeric_limits<double>::max());

        // preparation
        m_currentAlignments.clear();
        for (const auto &point : hullNodes)
        {
            PointsSet alignment;
            alignment.push_back(point);
            m_currentAlignments.push_back(alignment);
        }
        m_anchorSorted.clear();
        double i = 0.0;
        for (const auto &point : innerPoints)
        {
            Anchor an;
            an.point = point;
            an.edgeI = m_currentAlignments.begin();
            m_anchorSorted.insert({++i,an});
            m_currentAlignments.front().push_back(point);
        }

        // recursion
        for (size_t i=0; i < m_currentAlignments.size(); ++i)
        {
            double bestDistBefore = m_bestDist;
            for (auto edgeI = m_currentAlignments.begin();
                 edgeI != m_currentAlignments.end(); ++edgeI)
            {
                double bestDistBefore2 = m_bestDist;
                m_anchorSorted = preSort2(edgeI);
                auto begin = m_anchorSorted.begin();
                auto end = m_anchorSorted.end();
                recursiveExpSearchForAnchorsV1(edgeI, begin, end);
                if (bestDistBefore2 != m_bestDist)
                {
                    m_anchorSorted = m_bestAnchorIndizes;
                    m_currentAlignments = m_bestAlignments;
                }
            }
            if (bestDistBefore == m_bestDist)
                break;      // pruning, no change in current alignment
        }

        // post-condition
        for (auto &alignment : m_bestAlignments)
        {
            if (alignment.size() > 1)
            {
                comdat::ResultElement resEl;
                auto alignI = alignment.begin();
                resEl.hullEdgeFrom = *alignI;
                ++alignI;
                for (; alignI != alignment.end(); ++alignI)
                {
                    resEl.innerNodes.push_back(const_cast<Node*>(*alignI));
                }
                retResult.push_back(resEl);
            }
        }

        return std::make_tuple(retResult, m_bestDist);
    }

    std::tuple<Result, double> search(const PointsSet &hullNodes, const PointsSet &innerPoints)
    {
        Result retResult;
        if (hullNodes.empty())
            return std::make_tuple(retResult, std::numeric_limits<double>::max());

        // preparation
        m_currentAlignments.clear();
        for (const auto &point : hullNodes)
        {
            PointsSet alignment;
            alignment.push_back(point);
            m_currentAlignments.push_back(alignment);
        }
        m_anchorSorted.clear();
        double i = 0.0;
        for (const auto &point : innerPoints)
        {
            Anchor an;
            an.point = point;
            an.edgeI = m_currentAlignments.begin();
            m_anchorSorted.insert({++i,an});
            //m_currentAlignments.front().push_back(point);
        }

        // recursion
        auto begin = m_anchorSorted.begin();
        auto end = m_anchorSorted.end();
        recursiveExpSearchForAnchors(begin, end, 0.0);

        // post-condition
        for (auto &alignment : m_bestAlignments)
        {
            if (alignment.size() > 1)
            {
                comdat::ResultElement resEl;
                auto alignI = alignment.begin();
                resEl.hullEdgeFrom = *alignI;
                ++alignI;
                for (; alignI != alignment.end(); ++alignI)
                {
                    resEl.innerNodes.push_back(const_cast<Node*>(*alignI));
                }
                retResult.push_back(resEl);
            }
        }

        return std::make_tuple(retResult, m_bestDist);
    }

private:


    void recursiveExpSearchForAnchors(AnchorMap::iterator &innerPointsI, AnchorMap::iterator &end, double currentDist)
    {
        if (innerPointsI == end)
        {
            // calc total dist
            double dist = 0;
            for (const auto &edge : m_currentAlignments)
            {
                Node *hullNext = const_cast<Node*>(Way::nextChild(edge.front()));
                auto pointI = edge.begin();
                Node *from = *pointI;
                ++pointI;
                for (; pointI != edge.end(); ++pointI)
                {
                    dist += TSPHelper::seboehDist(from, hullNext, *pointI);
                    from = *pointI;
                }
            }
            if (dist < m_bestDist)
            {
                m_bestDist = dist;
                m_bestAlignments = m_currentAlignments;
                m_bestAnchorIndizes = m_anchorSorted;
            }

            return;
        }

        auto &anchor = innerPointsI->second;
        ++innerPointsI;

        for (auto edgeI = m_currentAlignments.begin();
             edgeI != m_currentAlignments.end(); ++edgeI)
        {
            // backup
            auto edgeBefore = *edgeI;

            // new align
            double currentDistOfAlignment = recalculateSeboehDist(*edgeI);
            auto preSortedPoints = shortestWay(*edgeI);
            preSortedPoints.pop_front();

            Node *from = edgeI->front();
            edgeI->clear();
            edgeI->push_back(from);

            double currentDistOfNewAlignment = 0;
            PointsSet::iterator alignI;
            double distAlign;
            double distAlignement1 = 0;
            PointsSet alignment1;
            alignment1.push_back(from);
            for (const auto &node : preSortedPoints)
            {
                std::tie(distAlign, alignI) = findBestAlignment(alignment1, node);
                //currentDistOfNewAlignment += distAlign;
                distAlignement1 += distAlign;
            }
            std::tie(distAlign, alignI) = findBestAlignment(alignment1, anchor.point);
            distAlignement1 += distAlign;

            double distAlignement2 = 0;
            PointsSet alignment2;
            alignment2.push_back(from);
            if (!preSortedPoints.empty())
            {
                for (auto nodeI = --preSortedPoints.end();
                     true; --nodeI)
                {
                    std::tie(distAlign, alignI) = findBestAlignment(alignment2, *nodeI);
                    distAlignement2 += distAlign;
                    if (nodeI == preSortedPoints.begin())
                        break;
                }
            }
            std::tie(distAlign, alignI) = findBestAlignment(alignment2, anchor.point);
            distAlignement2 += distAlign;

            double distAlignement3 = 0;
            PointsSet alignment3;
            alignment3.push_back(from);
            std::tie(distAlign, alignI) = findBestAlignment(alignment3, anchor.point);
            distAlignement3 += distAlign;
            for (const auto &node : preSortedPoints)
            {
                std::tie(distAlign, alignI) = findBestAlignment(alignment3, node);
                //currentDistOfNewAlignment += distAlign;
                distAlignement3 += distAlign;
            }

            double distAlignement4 = 0;
            PointsSet alignment4;
            alignment4.push_back(from);
            std::tie(distAlign, alignI) = findBestAlignment(alignment4, anchor.point);
            distAlignement4 += distAlign;
            preSortedPoints.reverse();
            for (const auto &node : preSortedPoints)
            {
                std::tie(distAlign, alignI) = findBestAlignment(alignment4, node);
                //currentDistOfNewAlignment += distAlign;
                distAlignement4 += distAlign;
            }

            if (distAlignement1 <= distAlignement2)
            {
                if (distAlignement1 <= distAlignement3)
                {
                    if (distAlignement1 <= distAlignement4)
                        *edgeI = alignment1;
                    else
                        *edgeI = alignment4;
                }
                else if (distAlignement3 <= distAlignement4)
                    *edgeI = alignment3;
                else
                    *edgeI = alignment4;
            }
            else
            {
                if (distAlignement2 <= distAlignement3)
                {
                    if (distAlignement2 <= distAlignement4)
                        *edgeI = alignment2;
                    else
                        *edgeI = alignment4;
                }
                else if (distAlignement3 <= distAlignement4)
                    *edgeI = alignment3;
                else
                    *edgeI = alignment4;
            }

            // correct current dist
            currentDist += (currentDistOfNewAlignment - currentDistOfAlignment);

            // pruning
            // TODO [seboeh] Pruning - not possible
            /// Way-length as alternative to seboehDist can be longer than seboehDist,
            /// and will result in to early pruning, because the global criteria is seboehDist.
//            if (currentDist + distAlign > m_bestDist)
//            {
//                edgeI->erase(alignI);
//                continue;
//            }

            // update  - TODO [seboeh] delete all anchor here
            anchor.edgeI = edgeI;
            anchor.distAlign = distAlign;

            // one recursion/decision with
            recursiveExpSearchForAnchors(innerPointsI, end, currentDist + distAlign);

            // clear new align
            // is not enough - see test25 - edgeI->remove(anchor.point);
            // remove, because in deeper recursion, there is an preSortedPoints alignment
            *edgeI = edgeBefore;
        }

        --innerPointsI;
    }

    double recalculateSeboehDist(PointsSet &edge)
    {
        auto edgeI = edge.begin();
        Node *from = *edgeI;
        Node *to = const_cast<Node*>(Way::nextChild(from));

        ++edgeI;
        double distTotal = 0;
        for (; edgeI != edge.end(); ++edgeI)
        {
            distTotal += TSPHelper::seboehDist(from, to, *edgeI);
        }

        return distTotal;
    }

    PointsSet shortestWay(const PointsSet &edge)
    {
        PointsSet shortWay;
        PointsSet rest = edge;
        Node *from = edge.front();
        Node *to = const_cast<Node*>(Way::nextChild(from));
        Point center = (from->point + to->point) / 2.0;
        shortWay.push_back(from);
        rest.pop_front();
        for (size_t i = 0; i < edge.size()-1; ++i)
        {
            double bestDist = std::numeric_limits<double>::max();
            auto bestNodeI = rest.end();
            for (auto nodeI = rest.begin();
                 nodeI != rest.end(); ++nodeI)
            {
                double dist = defaultFnc::defaultDistanceFnc(from->point, (*nodeI)->point);
                // does not work test90 - dist += defaultFnc::defaultDistanceFnc(center, (*nodeI)->point);
                if (dist < bestDist)
                {
                    bestDist = dist;
                    bestNodeI = nodeI;
                }
            }
            shortWay.push_back(*bestNodeI);
            from = *bestNodeI;
            rest.erase(bestNodeI);
        }
        return shortWay;
    }

    void recursiveExpSearchForAnchorsV1(std::list<PointsSet>::iterator edgeI, AnchorMap::iterator &innerPointsI, AnchorMap::iterator &end)
    {
        if (innerPointsI == end)
        {
            // calc total dist
            double dist = 0;
            for (const auto &edge : m_currentAlignments)
            {
                Node *hullNext = const_cast<Node*>(Way::nextChild(edge.front()));
                auto pointI = edge.begin();
                Node *from = *pointI;
                ++pointI;
                for (; pointI != edge.end(); ++pointI)
                {
                    dist += TSPHelper::seboehDist(from, hullNext, *pointI);
                    from = *pointI;
                }
            }
            if (dist < m_bestDist)
            {
                m_bestDist = dist;
                m_bestAlignments = m_currentAlignments;
                m_bestAnchorIndizes = m_anchorSorted;
            }

            return;
        }

        auto &anchor = innerPointsI->second;
        ++innerPointsI;

        // erase old align
        auto pointOnOldEdgeI = std::find(anchor.edgeI->begin(), anchor.edgeI->end(), anchor.point);
        if (pointOnOldEdgeI != anchor.edgeI->end())
            anchor.edgeI->erase(pointOnOldEdgeI);

        // new align
        PointsSet::iterator alignI;
        double distAlign;
        std::tie(distAlign, alignI) = findBestAlignment(*edgeI, anchor.point);

        // update
        auto oldEdgeI = anchor.edgeI;
        auto oldDistAlign = anchor.distAlign;
        anchor.edgeI = edgeI;
        anchor.distAlign = distAlign;

        // one recursion/decision with
        recursiveExpSearchForAnchorsV1(edgeI, innerPointsI, end);

        // clear new align
        oldEdgeI->push_back(anchor.point);
        edgeI->erase(alignI);
        anchor.edgeI = oldEdgeI;
        anchor.distAlign = oldDistAlign;

        if (oldDistAlign <= distAlign)     // old < new
        {
            // second decision - without insertion (in findBestAlignment())
            recursiveExpSearchForAnchorsV1(edgeI, innerPointsI, end);
        }

        --innerPointsI;
    }

    std::tuple<double, PointsSet::iterator> findBestAlignment(PointsSet &listIncFrom, const Node *valueToInsert)
    {
        assert(!listIncFrom.empty());
        auto listI = listIncFrom.begin();
        Node *from = *listI;
        ++listI;
        double bestDist = std::numeric_limits<double>::max();
        auto bestI = listIncFrom.end();
        for (; listI != listIncFrom.end(); ++listI)
        {
            double dist = TSPHelper::seboehDist(from, *listI, valueToInsert);
            if (dist < bestDist)
            {
                bestDist = dist;
                bestI = listI;
            }
            from = *listI;
        }
        Node *nextHull = const_cast<Node*>(Way::nextChild(listIncFrom.front()));
        double dist = TSPHelper::seboehDist(listIncFrom.back(), nextHull, valueToInsert);
        if (dist < bestDist)
        {
            bestDist = dist;
            bestI = listIncFrom.end();
        }

        bestI = listIncFrom.insert(bestI, const_cast<Node*>(valueToInsert));

        return std::make_tuple(bestDist, bestI);
    }

    std::map<double, Node*> preSort(const PointsSet &innerPoints, const Node *hullFrom)
    {
        const Node *hullNext = Way::nextChild(hullFrom);

        std::map<double, Node*> firstAlign;
        for (const Node* point : innerPoints)
        {
            double dist = TSPHelper::seboehDist(hullFrom, hullNext, point);
            firstAlign.insert({dist, const_cast<Node*>(point)});
        }

        return firstAlign;
    }

    AnchorMap preSort2(std::list<PointsSet>::iterator edgeI)
    {
        const Node *hullFrom = edgeI->front();
        const Node *hullNext = Way::nextChild(hullFrom);

        AnchorMap alignmentsSorted;
        for (const auto &pointS : m_anchorSorted)
        {
            Anchor an;
            an.point = pointS.second.point;
            double dist = TSPHelper::seboehDist(hullFrom, hullNext, an.point);
            if (pointS.second.distAlign == std::numeric_limits<double>::max())
            {
                an.edgeI = edgeI;       // at first time - else keep old one
                an.distAlign = dist;
            }
            else
            {
                an.edgeI = pointS.second.edgeI;
                an.distAlign = pointS.second.distAlign;
            }
            alignmentsSorted.insert({dist, an});
        }

        return alignmentsSorted;
    }

    std::map<double, Node*> preSort3(std::list<PointsSet>::iterator edgeI)
    {
        auto edgePointsI = edgeI->begin();
        const Node *hullFrom = *edgePointsI;
        const Node *hullNext = Way::nextChild(hullFrom);

        ++edgePointsI;
        std::map<double, Node*>  alignmentsSorted;
        for (; edgePointsI != edgeI->end(); ++edgePointsI)
        {
            alignmentsSorted.insert({
                                        TSPHelper::seboehDist(hullFrom, hullNext, *edgePointsI)
                                        , *edgePointsI});
        }

        return alignmentsSorted;
    }

    void recursiveExpSearch(PointsSet::const_iterator &openInnerPointsI, PointsSet::const_iterator &end, double dist)
    {
        assert(!m_currentAlignments.empty());
        // cancel criteria
        if (openInnerPointsI == end)
        {
            if (dist < m_bestDist)
            {
                m_bestDist = dist;
                m_bestAlignments = m_currentAlignments;
            }
            return;
        }
        // pruning
        if (dist > m_bestDist)
            return;

        auto point = *openInnerPointsI;
        ++openInnerPointsI;

        // find best, second best alginment
        PointsSet::iterator alignI;
        double distAlign;
        std::tie(distAlign, alignI) = findBestAlignment(*m_currentAlignmentsI, point);

        // recursion
        recursiveExpSearch(openInnerPointsI, end, dist + distAlign);

        m_currentAlignmentsI->erase(alignI);

        --openInnerPointsI;
    }



    void recursiveExpSearchV1(PointsSet::const_iterator &openInnerPointsI, PointsSet::const_iterator &end, double dist)
    {
        assert(!m_currentAlignments.empty());
        // cancel criteria
        if (openInnerPointsI == end)
        {
            if (dist < m_bestDist)
            {
                m_bestDist = dist;
                m_bestAlignments = m_currentAlignments;
            }
            return;
        }
        // pruning
        if (dist > m_bestDist)
            return;

        auto point = *openInnerPointsI;
        ++openInnerPointsI;

        // find best, second best alginment
        double best = std::numeric_limits<double>::max();
        auto bestI = m_currentAlignments.front().end();
        auto bestAlignI = m_currentAlignments.end();
        double secondBest = std::numeric_limits<double>::max();
        auto secondBestI = m_currentAlignments.front().end();
        auto secondBestAlignI = m_currentAlignments.end();
        for (auto pointsI = m_currentAlignments.begin();
             pointsI != m_currentAlignments.end(); ++pointsI)
        {
            for (auto fromI = pointsI->begin();
                 fromI != pointsI->end(); ++fromI)
            {
                Node *next = nullptr;
                auto nextI = fromI; ++nextI;
                if (nextI == pointsI->end())
                    next = const_cast<Node*>(Way::nextChild(pointsI->front()));
                else
                    next = *nextI;

                double dist = TSPHelper::seboehDist(*fromI, next, point);
                if (dist < best)
                {
                    secondBest = best;
                    secondBestI = bestI;
                    secondBestAlignI = bestAlignI;
                    best = dist;
                    bestI = fromI;
                    bestAlignI = pointsI;
                }
                else
                if (dist < secondBest)
                {
                    secondBest = dist;
                    secondBestI = fromI;
                    secondBestAlignI = pointsI;
                }
            }
        }

        // recursion 1
        assert(bestI != m_currentAlignments.front().end());
        ++bestI;        // because insert after fromI
        bestI = bestAlignI->insert(bestI, point);
        recursiveExpSearch(openInnerPointsI, end, dist + best);
        bestAlignI->erase(bestI);

        // recurion 2
        if (secondBestI != m_currentAlignments.front().end())
        {
            ++secondBestI;
            secondBestI = secondBestAlignI->insert(secondBestI, point);
            recursiveExpSearch(openInnerPointsI, end, dist + secondBest);
            secondBestAlignI->erase(secondBestI);
        }

        --openInnerPointsI;
    }



    /**
      Attributes
      */
    double m_bestDist = std::numeric_limits<double>::max();
    std::list<PointsSet> m_bestAlignments;          ///< in the order of hullFromNodes, including fromNodes
    std::list<PointsSet> m_currentAlignments;       ///< in the order of hullFromNodes, including fromNodes
    std::list<PointsSet>::iterator m_currentAlignmentsI;
    std::map<Node*, std::tuple<Node*, double>> m_hullAlignment;     ///< innerPoint, bestFromHull, bestAlignedDist
    InnerPointsAnchorType m_innerPointsAnchor;       ///< innerPoint, iter to m_currentAlignments.
    AnchorMap m_anchorSorted;
    AnchorMap m_bestAnchorIndizes;

};


}
}
