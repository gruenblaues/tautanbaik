#pragma once

#include <memory>
#include <tuple>

#include "TspAlgoCommon.h"
#include "TamarAlgorithmHelper.h"
#include "dataStructures/OlavList.h"
#include "dataStructures/commonstructures.h"
#include "bspTreeVersion/BspTreeSarah.h"


// have to be out of namespace, because BSPNode.h
using Node = comdat::Node;
using PointL = std::list<TSP::Point>;

struct Cluster;
struct NeighbourRef {
    Cluster *neighClu = nullptr;
};
struct Cluster {
    PointL verts;
    std::shared_ptr<Node> point;
    double area = std::numeric_limits<double>::max();
    std::vector<std::shared_ptr<Node>> closestN;
    std::list<Node*> closestTestedPointsSorted;
    std::list<Cluster*> closestSortedNeighbours;
    std::map<double, std::tuple<Node*,Node*>> neighbourEdges;
    int state = 0;      // 1 = isAligned
    int iterationC = 0;
    size_t growPosition = 0;
    std::map<double, std::map<Node*, NeighbourRef>> neighbourRefs;       // first index = dist for common edge pairEdge, second index = from-node
    // DetourClusterAlignment
    std::shared_ptr<Cluster> parent;
    std::list<std::shared_ptr<Cluster>> childs;
};


namespace TSP {

class DetourClusterAlignment : public TspAlgoCommon
{
public:
    using Node = comdat::Node;
    using NodeL = std::list<std::shared_ptr<comdat::Node>>;
    using PointL = std::list<TSP::Point>;
    using Way = oko::OlavList;

    struct no_op_delete
    {
        void operator()(void*) { }
    };

    enum class MergeCallState {
        leftRight, topDown
    };
    enum class DirectAccessState {
        isDirectAccess, isNoDirectAccess
    };
    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };
    typedef oko::ClosestEdges ClosestEdges;
    typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;
    struct EdgeIter {
        EdgeIter(Way::iterator &aNodeIter, Way::iterator &bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
        Way::iterator aNodeI;
        Way::iterator bNodeI;
    };

    typedef std::list<std::shared_ptr<Cluster>> ClusterGrow;


    // VoronoiClusterAlignment
    struct Edge
    {
        Node *from = nullptr;
        Node *to = nullptr;
    };

    struct ClusterEdge
    {
        std::map<double, Cluster *> attachments;        // more than one clu, because of test27
        Edge edge;
    };

    using ClusterEdgeM = std::map<Node *, std::map< Node *, ClusterEdge> >;     // from, to, clusterEdge - and also - to, from, clusterEdge
    ClusterEdgeM m_edgeToClusters;

    struct OpenClusterEdge
    {
        bool operator==(const OpenClusterEdge &val) { return clu == val.clu && edge.from == val.edge.from && edge.to == val.edge.to; }
        Edge edge;
        Cluster *clu;
    };

    using OpenClustersAtEdges = std::map<double, OpenClusterEdge>;

    using WayL = std::list<Node*>;
    WayL m_currentWay;
    WayL m_bestWay;
    double m_currentLength = std::numeric_limits<double>::max();
    double m_bestLength = std::numeric_limits<double>::max();

    // MinMaxClusterAlignment
    std::list<Node*> m_edgesGuarantied;

    struct CeList {
        using CeListType = std::list<ClosestEdges>;
        using CeListOrderType = std::map<double, Node*>;
        CeListOrderType ceListOrder;
        CeListType ceList;
    };

    using DirectAlignments = std::map<double, ClosestEdges>;

    // DetourClusterAlignment


    size_t m_tspSize = 0;

    /** globals **/
    static size_t tspSize;
    static double tspWidth;
    static double tspHeight;

    /** Ctor **/
    DetourClusterAlignment();

protected:
    /** Impl **/
    void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic);

private:
    NodeL createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic);
    std::tuple<DetourClusterAlignment::NodeL, DetourClusterAlignment::Way *> determineHull(NodeL &input);
    void determineGuarantiedAlignment(NodeL &input, NodeL &hull);
    ClusterGrow determineClusters(NodeL &input, NodeL &hull);
    DetourClusterAlignment::NodeL determineWay(ClusterGrow &clustersA, NodeL &hull, Way *way);
    void determineGroupAlignment(std::list<Node *> &_openNodes);

    void _triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, Way::iterator &hpaI, Way::iterator &lpaI, Way::iterator &hpbI, Way::iterator &lpbI);
    DetourClusterAlignment::Way *_newConqueredWay(bsp::BspNode *node);
    bsp::BspTreeSarah _createBSPTree(NodeL &input);
    void _mergeHull(Way *&way1, const TSP::Point &pWay1Center, Way *&way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter);
    bool _isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);
    DirectAccessState _isDirectAccess(const TSP::Point &pMeanA, Way::iterator &pAI, const TSP::Point &pMeanB, Way::iterator &pBI);
    CrossedState _isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint = nullptr);
    bool _isNormalVectorLeft(const Point &from, const Point &to, const Point &insert);
    double _calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3);
    double _area(const PointL &polygon);
    bool _isCovered(Way *way, Way::iterator &w1I, Way::iterator &fixI);
    void _createTriangle3x1(Way *way1, Way::iterator &insideI, Way::iterator &fixI);
    bool _createTriangle2x2(Way *way1, Way *way2, Way::iterator &w1Inside, Way::iterator &w1Alt, Way::iterator &way2I, Way::iterator &way2NI);
    void _flipWay(Way *way, TSP::Point pCenter);
    bool _isClockwise(TSP::Point pCenter, Way *way);
    double _triangleAltitudeOnC(Point c1, Point c2, Point perpendicular);
    bool _isRightSide(const Point &p1, const Point &p2);
    double _calcDegree(Node *node, Node *nodeTested);
    double _calcDegree(Point &node, Point &nodeTested);
    double _correlation(Point a, Point b);
    bool _checkCorrelationAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);

    // MinMaxClusterAlignment
    void _collectClusterEdges(std::map<double, Cluster*> &clustersM);
    bool _searchBestPathByConvexHullAlignment(OpenClustersAtEdges &open);
    void _runEvolutionaryGrow(NodeL &hull);
    ClusterEdge *_findClusterToEdge(Node *from, Node *to);
    std::list<OpenClustersAtEdges::iterator> _insertCluEIfPossible(ClusterEdge *cluE, OpenClustersAtEdges &openN);
    void _insertClosestTestedPointsSorted(std::map<double, Node *> &closestTestedPointsSorted, Node *node, Node *closest);
    bool _checkAngelBetween(Point cluPointOut, Point cluPointOut2, Point nodePoint, Point closestP);
    float sign(const Point &p1, const Point &p2, const Point &p3);
    template<typename T, typename V>
    std::pair<typename T::iterator, bool> _insertToMap(double dist, T &mapContainer, V &&value);
    CeList::CeListType::iterator _insertToCeList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert);
    void _determineGroupAlignmentRec(std::list<Node *> selectedNodes, std::list<Node *> openNodes);
    double _calcCeListDist(CeList &ceList);
    bool _isAtEdge(std::list<Node*> &openNodes, Node *from, Node *to, std::list<Node *> &selectedNodes);
    CeList::CeListType::iterator __insertToCeListAlongList(CeList &ceList, Node *rootFrom, Node *rootTo, Node *insert);
    bool _isNotInsideTriangle(Node *point1, Node *point2, Node *point3, std::list<Node*> &pointsInside);
    Point _correctNodeCenter(Way* wayBiggerVals, Way* waySmallerVals, bsp::BspNode *node, MergeCallState state);
    bool _isLeftSideExEqual(const Point &p1Line, const Point &p2Line, const Point &check);

    // DetourClusterAlignment
    std::tuple<std::map<double, Cluster *>, Cluster *> determineStructure(Way *way, NodeL &hull, ClusterGrow &clustersA);
    void _recursiveWayCreationByHierarchy(Way *way, NodeL &hull, Cluster *currentClu);
    void _insertNodes(Way *way, std::list<Node *> &nodes);
};

}
