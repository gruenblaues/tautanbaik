#include "SortedMerge.h"

#include "TamarAlgorithmHelper.h"
#include "TspFacade.h"
#include <thread>

using namespace TSP;

// TODO [seboeh] rename SortedMerge -> SearchingMerge.

template<typename T>
void insert_sort(T &container, const typename T::value_type &element
                 , const std::function<bool(const typename T::value_type &, const typename T::value_type &)> &lessEqual)
{
    for (auto iter = std::begin(container);
         iter != std::end(container); ++iter)
    {
        if (!lessEqual(*iter, element))
        {
            container.insert(iter, element);
            return;
        }
    }
    container.push_back(element);
}



SortedMerge::SortedMerge()
{

}

SortedMerge::~SortedMerge()
{
// TODO [seboeh] delete enable
//    for (auto el : m_points)
//        delete el;
}


void SortedMerge::_resetVarity(size_t elCount)
{
    m_varity.clear();
    m_varity.resize(elCount, 1);
    m_countOfNodes = elCount;       // safety, because input was countOfNodes probably
}

std::tuple<std::vector<size_t>, bool> SortedMerge::_createNextVarity()
{
    if (m_varity.empty())
        return std::make_tuple(m_varity, true);

    size_t max = 0;
    for (const auto &el : m_varity)
    {
        if (el > 1)
            break;
        ++max;
    }
    max = m_countOfNodes - max + 1;

    bool isEnd = false;
    for (auto varyIter = --m_varity.end(); max > 1; --varyIter, --max)
    {
        if (*varyIter >= max)
        {
            if (varyIter == m_varity.begin())
            {
                isEnd = true;
                break;
            }
            *varyIter = 1;
            auto varyIterCarry = varyIter; --varyIterCarry;
            ++(*varyIterCarry);
        }
        else
        {
            ++(*varyIter);
            break;
        }

        if (varyIter == m_varity.begin())
            break;
    }
    if (isEnd)
        return std::make_tuple(m_varity, true);

    // check validity
    std::vector<size_t> mask;
    mask.resize(m_countOfNodes, 0);
    for (const auto &el : m_varity)
    {
        mask[(1 << (el-1))-1] = 1;
    }
    size_t pos = m_countOfNodes;
    while (pos > 0 && mask[pos-1] == 0) --pos;
    while (pos > 0 && mask[pos-1] == 1) --pos;

    if (pos > 0)
        return _createNextVarity();
    else
        return std::make_tuple(m_varity, false);
}

std::tuple<double, std::list<SortedMerge::Node *> > SortedMerge::_findBestFrom(Node *fromNode, Node *toNode, std::list<Node*> set)
{

    if (set.empty())
        return std::make_tuple(std::numeric_limits<double>::max(), std::list<Node*>());

    bool isNodesWithNewCreated = false;

    /// create best way at hull edge
    std::list<Node*> hullOrderNodes;
    if (set.size() == 1)
    {
        hullOrderNodes.push_back(set.front());
    }
    else if (set.size() == 2)
    {
        auto iter = set.begin();
        Node *a = *iter;
        ++iter;
        Node *b = *iter;
        if (defaultFnc::defaultDistanceFnc(fromNode->point, a->point)
                <
            defaultFnc::defaultDistanceFnc(fromNode->point, b->point))
        {
            hullOrderNodes.push_back(a);
            hullOrderNodes.push_back(b);
        }
        else
        {
            hullOrderNodes.push_back(b);
            hullOrderNodes.push_back(a);
        }
    }
    else
    {
        TSP::TSPFacade::PointListType cities;
        std::pair<float, float> tspPoint = { fromNode->point.x(), fromNode->point.y() };
        cities.push_back(tspPoint);
        tspPoint = { toNode->point.x(), toNode->point.y() };
        cities.push_back(tspPoint);
        for (Node *node : set)
        {
            tspPoint.first = node->point.x();
            tspPoint.second = node->point.y();
            cities.push_back(tspPoint);
        }
        TSP::TSPFacade *mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::Tamar);
        mTsp->addInput(cities);
        TSP::Parameters     para;
        mTsp->setDefaultParameters(&para);

        std::list<std::pair<Point,Point>> blockedEdges;
        blockedEdges.push_back({fromNode->point, toNode->point});
        blockedEdges.push_back({toNode->point, fromNode->point});
        mTsp->setBlockedEdges(std::move(blockedEdges));

        mTsp->startRun(para);

        TSP::Indicators indi;
        while (0 == (indi.state()
               & (TSP::Indicators::States::ISFINISHED | TSP::Indicators::States::ISCANCELED))) {
            indi = mTsp->status();
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }
        mTsp->wait();

        const TSP::Way::WayType *hullWay = mTsp->getResultTypeWay()->way();
        // HINT: have to walk counter-clock-wise, due to merging from-to hull edge.
        auto startIter = hullWay->begin();
        while (!(*startIter == fromNode->point) && startIter != hullWay->end()) ++startIter;
        if (startIter == hullWay->end())
        {
            std::cerr << "Wrong hull-edge alignment in sortedMerge. Result not valid." << std::endl;
            return std::make_tuple(std::numeric_limits<double>::max(), std::list<Node*>());
        }

        auto iter = startIter;
        ++iter;       // omit the hull edge from point.
        ++iter;       // omit the hull edge to point.
        while (iter != startIter)
        {
            // find set equivalent
            auto setIter = std::find_if(set.begin(), set.end(),
                                        [iter](const Node *val)
            {
                return *iter == val->point;
            });

            hullOrderNodes.push_front(*setIter);        // push_front(), because counter-clock-wise.

            ++iter;
            if (iter == hullWay->end())
                iter = hullWay->begin();
        }

        delete mTsp;
    }

    /// calc dist
    double dist = 0;
    Node *lastNode = fromNode;
    for (Node *node : hullOrderNodes)
    {
        dist += TSPHelper::seboehDist(lastNode, toNode, node);
        lastNode = node;
    }

    return std::make_tuple(dist, hullOrderNodes);
}

std::tuple<Node*, double, std::list<Node*>> SortedMerge::_findBestFrom(std::list<Node*> froms, std::list<Node*> set)
{
    if (set.empty())
        return std::make_tuple(nullptr, std::numeric_limits<double>::max(), std::list<Node*>());

    bool isNodesWithNewCreated;
    bool isNodesWithNewCreatedBefore = false;
    Node *bestFrom = nullptr;
    double bestDist = std::numeric_limits<double>::max();
    std::list<Node*> bestNodes;
    for (Node* fromNode : froms)
    {
        isNodesWithNewCreated = false;

        /// hull edge
        Node *toNode = const_cast<Node*>(Way::nextChild(fromNode));

        /// create best way at hull edge
        std::list<Node*> hullOrderNodes;
        if (set.size() == 1)
        {
            hullOrderNodes.push_back(set.front());
        }
        else if (set.size() == 2)
        {
            auto iter = set.begin();
            Node *a = *iter;
            ++iter;
            Node *b = *iter;
            if (defaultFnc::defaultDistanceFnc(fromNode->point, a->point)
                    <
                defaultFnc::defaultDistanceFnc(fromNode->point, b->point))
            {
                hullOrderNodes.push_back(a);
                hullOrderNodes.push_back(b);
            }
            else
            {
                hullOrderNodes.push_back(b);
                hullOrderNodes.push_back(a);
            }
        }
        else
        {
            TSP::TSPFacade::PointListType cities;
            std::pair<float, float> tspPoint = std::pair<float,float>( fromNode->point.x(), fromNode->point.y() );
            cities.push_back(tspPoint);
            tspPoint = std::pair<float,float>( toNode->point.x(), toNode->point.y() );
            cities.push_back(tspPoint);
            for (Node *node : set)
            {
                tspPoint.first = node->point.x();
                tspPoint.second = node->point.y();
                cities.push_back(tspPoint);
            }
            TSP::TSPFacade *mTsp = new TSP::TSPFacade(TSP::Parameters::AlgorithmType::Tamar);
            mTsp->addInput(cities);
            TSP::Parameters     para;
            mTsp->setDefaultParameters(&para);

            std::list<std::pair<Point,Point>> blockedEdges;
            blockedEdges.push_back({fromNode->point, toNode->point});
            blockedEdges.push_back({toNode->point, fromNode->point});
            mTsp->setBlockedEdges(std::move(blockedEdges));

            mTsp->startRun(para);

            TSP::Indicators indi;
            while (0 == (indi.state()
                   & (TSP::Indicators::States::ISFINISHED | TSP::Indicators::States::ISCANCELED))) {
                indi = mTsp->status();
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
            }
            mTsp->wait();

            const TSP::Way::WayType *hullWay = mTsp->getResultTypeWay()->way();
            // HINT: have to walk counter-clock-wise, due to merging from-to hull edge.
            auto startIter = hullWay->begin();
            while (!(*startIter == fromNode->point) && startIter != hullWay->end()) ++startIter;
            if (startIter == hullWay->end())
            {
                std::cerr << "Wrong hull-edge alignment in sortedMerge. Result not valid." << std::endl;
                return std::make_tuple(*froms.begin(), std::numeric_limits<double>::max(), std::list<Node*>());
            }

            auto iter = startIter;
            ++iter;       // omit the hull edge from point.
            ++iter;       // omit the hull edge to point.
            while (iter != startIter)
            {
                // TODO [seboeh]: move this code to TamarAlgorithm to use the already existing nodes and not the facade.
                hullOrderNodes.push_front(new Node(*iter));        // push_front(), because counter-clock-wise.
                isNodesWithNewCreated = true;
                ++iter;
                if (iter == hullWay->end())
                    iter = hullWay->begin();
            }

            delete mTsp;
        }

        /// calc dist
        double dist = 0;
        Node *lastNode = fromNode;
        for (Node *node : hullOrderNodes)
        {
            dist += TSPHelper::seboehDist(lastNode, toNode, node);
            lastNode = node;
        }

        if (dist < bestDist)
        {
            bestDist = dist;
            bestFrom = fromNode;
            if (isNodesWithNewCreatedBefore)
            {
                for (auto &node : bestNodes)
                    delete node;
            }
            if (isNodesWithNewCreated)
                isNodesWithNewCreatedBefore = true;
            else
                isNodesWithNewCreatedBefore = false;
            bestNodes.clear();
            bestNodes = hullOrderNodes;
        }
        else
        {
            if (isNodesWithNewCreated)
            {
                for (auto &node : hullOrderNodes)
                    delete node;
            }
            hullOrderNodes.clear();
        }

    }       // end - for each fromNode

    return std::make_tuple(bestFrom, bestDist, bestNodes);
}

bool SortedMerge::_checkPropertyVaryFit(const std::vector<size_t> &vary, Way &innerPointWay)
{
    for (size_t vi = 1; vi <= vary.size(); ++vi)
    {
        /// calculate max(min)
        std::list<Node*> group;
        // do this find, because faster than next loop, because nextChild() in this loop.
        if (std::find(vary.begin(), vary.end(), vi) == vary.end())
            continue;
        auto wayIter = innerPointWay.begin();
        for (size_t setIndex : vary)
        {
            if (setIndex == vi)
            {
                group.push_back(&wayIter);
            }
            wayIter.nextChild();
        }

        /// find dist max(min)
        double max = 0;
        for (const auto &node : group)
        {
            double closestMinDist = std::numeric_limits<double>::max();
            for (const auto &couple : group)
            {
                if (node == couple) continue;
                double dist = defaultFnc::defaultDistanceFnc(node->point, couple->point);
                if (dist < closestMinDist)
                {
                    closestMinDist = dist;
                }
            }
            if (max < closestMinDist)
                max = closestMinDist;
        }

        /// check property vary fit
        bool isVaryFit = false;
        for (const auto &node : group)
        {

            // TODO [seboeh] this mehtod does not work
            // comparison between seboeh dist and euclidean dist.
            if (node->closestEdge.dist > max)
            {
                isVaryFit = true;
                break;
            }
        }
        if (!isVaryFit)
            return false;
    }

    return true;
}

std::tuple<SortedMerge::output_value, double> SortedMerge::search_V2(const std::list<Node*> &fromNodes, Way &innerPointWay)
{
    _resetVarity(innerPointWay.recalcSize());

    auto restFroms = fromNodes;

    double bestDist = std::numeric_limits<double>::max();
    std::map<int, std::list<Node*>> bestSets;
    std::map<int, Node*> bestFroms;
    std::map<int, std::list<Node*>> bestNodeOrders;

    bool isEnd = false;
    std::vector<size_t> vary;
    while (!isEnd)
    {
        bool isVaryFit = false;
        double maxMinDist;
        while (!isEnd && !isVaryFit)
        {
            std::tie(vary, isEnd) = _createNextVarity();
//            std::tie(maxMinDist, isVaryFit) = _checkPropertyVaryFit(vary, innerPointWay);
            isVaryFit = true;
        }
        if (isEnd) break;

        std::map<int, std::list<Node*>> sets;
        auto wayIter = innerPointWay.begin();
        for (int setIndex : vary)
        {
            sets[setIndex-1].push_back(wayIter.nextChild());
        }

        double totalDist = 0;
        std::map<int, Node*> froms;
        std::map<int, std::list<Node*>> nodeOrders;
        for (size_t i = 0; i <= m_countOfNodes; ++i)
        {
            Node *from;
            double dist;
            std::list<Node*> nodeOrder;
            std::tie(from, dist, nodeOrder) = _findBestFrom(restFroms, sets[i]);
            totalDist += dist;
            froms[i] = from;
            nodeOrders[i] = nodeOrder;
        }

        if (totalDist < bestDist)
        {
            bestDist = totalDist;
            bestSets = sets;
            bestFroms = froms;
            bestNodeOrders = nodeOrders;
        }
    }       // end - while of all variations

    // create result
    m_result.clear();
    output_value::value_type outEl;
    for (size_t i = 0; i < bestSets.size(); ++i)
    {
        outEl.innerNodes = bestSets[i];
        outEl.hullEdgeFrom = bestFroms[i];
        m_result.push_back(outEl);
    }

    return std::make_tuple(std::ref(m_result), bestDist);
}

std::map<double, std::list<Node*> > SortedMerge::_calcSpecialOtherBorder(Node *from, Node *to, std::list<Node*> nodes)
{
    assert(from != nullptr);
    assert(to != nullptr);
    std::map<double, std::list<Node*> > ret;
    if (nodes.empty())
        return ret;

    for (size_t s = 0; s < nodes.size(); ++s)
    {
        std::list<Node*> ceEles;
        auto iter = nodes.begin();
        for (size_t i = 0; i < s; ++i, ++iter)
            ceEles.push_back(*iter);

        for (size_t shift = 0; shift < nodes.size()-s; ++shift)
        {
            Node *fromCe = from;
            std::list<ClosestEdges> ceList;
            for (const auto &node : ceEles)
            {
                ClosestEdges ce(node);
                TSPHelper::findBestSingleCeAndAdd(to, fromCe, ce, ceList);
            }
            double dist = TSPHelper::correctDistHelper(ceList);
            ret.insert({dist, ceEles});
            if (iter != nodes.end())
            {
                ceEles.push_back(*iter);
                ++iter;
                ceEles.pop_front();
            }
        }
    }

    return ret;
}

double SortedMerge::_calcTriLength(const Point &pA, const Point &pB, const Point &center, const Point &current)
{
    double lengthInner1 = defaultFnc::defaultDistanceFnc(current, pA);
    double lengthInner2 = defaultFnc::defaultDistanceFnc(current, pB);
    double lengthInner = lengthInner1 + lengthInner2;
    double lengthOuter1 = defaultFnc::defaultDistanceFnc(center, pA);
    double lengthOuter2 = defaultFnc::defaultDistanceFnc(center, pB);
    double lengthOuter = lengthOuter1 + lengthOuter2;
    double triangleRate = 0;
    if (lengthOuter != 0)
        triangleRate = lengthInner / lengthOuter;
    else
        triangleRate = lengthInner / 0.0001;
    double lengthHypo = defaultFnc::defaultDistanceFnc(pA, pB);
    if (lengthHypo == 0) lengthHypo = 1;
    triangleRate /= lengthHypo;

    return triangleRate;
}

std::tuple<bool, size_t, double, double> SortedMerge::_featureClosestAlign(std::list<oko::Node*> &innerNodes
                                                           , oko::Node *nodeCurr, oko::Node *nodePrev, oko::Node *nodeNext)
{
    std::map<double, size_t> alignmentsPrev;
    std::map<double, size_t> alignmentsNext;

    size_t pos = 0;
    for (Node *node : innerNodes)
    {
        ++pos;
        if (node == nodeCurr
                || node == nodePrev
                || node == nodeNext)
            continue;
        double dist = TSPHelper::seboehDist(nodePrev, node, nodeCurr);
        alignmentsPrev.insert({dist, pos});
        dist = TSPHelper::seboehDist(nodeNext, node, nodeCurr);
        alignmentsNext.insert({dist, pos});
    }

    // decide for one next or prev
    if (alignmentsNext.size() < 2
            || alignmentsPrev.size() < 2)
        return std::make_tuple(false, 0, 0, 0);

    double distNext = alignmentsNext.begin()->first + (++alignmentsNext.begin())->first;
    double distPrev = alignmentsPrev.begin()->first + (++alignmentsPrev.begin())->first;

    auto alignmentIter = alignmentsNext.begin();
    Node *nodeRoot = nodeNext;
    if (distPrev < distNext)
    {
        alignmentIter = alignmentsPrev.begin();
        nodeRoot = nodePrev;
    }

    size_t pos1 = alignmentIter->second;
    ++alignmentIter;
    size_t pos2 = alignmentIter->second;

    if (pos1+1 != pos2
            && pos2+1 != pos1)
        return std::make_tuple(false, 0, 0, 0);

    // decide if valid
    double distLineCurr = defaultFnc::defaultDistanceFnc(nodePrev->point, nodeCurr->point)
            + defaultFnc::defaultDistanceFnc(nodeCurr->point, nodeNext->point);

    if (pos1 > pos2)
        std::swap(pos1,pos2);

    Node *nodeNextAlign = nullptr;
    Node *nodePrevAlign = nullptr;
    size_t i = 0;
    for (Node* node : innerNodes)
    {
        ++i;
        if (i == pos1)
            nodePrevAlign = node;
        if (i == pos2)
            nodeNextAlign = node;
    }

    double distLineNew = defaultFnc::defaultDistanceFnc(nodePrevAlign->point, nodeCurr->point)
            + defaultFnc::defaultDistanceFnc(nodeCurr->point, nodeNextAlign->point);

    if (distLineCurr < distLineNew)
        return std::make_tuple(false, 0, 0, 0);

    if (!TSPHelper::isInsideTriangle(nodeRoot->point, nodePrevAlign->point, nodeNextAlign->point, nodeCurr->point))
        return std::make_tuple(false, 0, 0, 0);

    double feature = std::min(distPrev, distNext);
    double criteria = TSPHelper::seboehDist(nodePrevAlign, nodeNextAlign, nodeCurr)
            - feature;

    return std::make_tuple(true, pos1, feature, criteria);
}

std::list<Node*> SortedMerge::m_fimHullNodes;
size_t SortedMerge::m_fimCrossedMax = 0;
double SortedMerge::m_fimMaxInnerLength = 0;

void SortedMerge::FimVariations::_fimRecursiveVariationCreation(std::list<Node*> &used, std::list<Node*> open, std::list<Node*>::iterator hullCurrentI, size_t crossedCount)
{
    if (open.empty())
    {
        variations.push_back(used);
        return;
    }

    for (auto iter = open.begin();
         iter != open.end(); ++iter)
    {
        auto node = *iter;

        /// check validity
        bool isInvalid = false;
        auto lastI = iter;
        if (lastI != open.begin())
            --lastI;
        Node *last = *lastI;

        // no jumping over prev/next hull
        auto hullLastI = hullCurrentI;
        auto iterF = std::find(m_fimHullNodes.begin(), m_fimHullNodes.end(), node);
        if (iterF != m_fimHullNodes.end())
        {
            auto nextI = hullCurrentI;
            ++nextI;
            if (nextI == m_fimHullNodes.end())
                nextI = m_fimHullNodes.begin();
            auto next2I = nextI;
            ++next2I;
            if (next2I == m_fimHullNodes.end())
                next2I = m_fimHullNodes.begin();
            if (*iterF != *nextI
               && *iterF != *next2I     // test107
               /// HINT: NO *iterF != *prevI, because not necessary in recursive-tree-variation
               )
            {
                isInvalid = true;
            }
            else
            {
                hullCurrentI = iterF;
            }
        }
        if (isInvalid)
            continue;

        // no crossing of previous lines
        size_t crossedCountLast = crossedCount;
        if (!used.empty()
            && last != node
            && hullLastI != hullCurrentI)
        {
            Node* prev = used.front();
            for (const auto &nodeUsed : used)
            {
                if (nodeUsed == prev)
                    continue;
                auto crossed = TSPHelper::isCrossed(prev->point, nodeUsed->point, last->point, node->point);
                if (crossed == TSPHelper::CrossedState::isCrossed
                    || crossed == TSPHelper::CrossedState::isCovered)       // onPoint is often given, because each after another edge.
                {       // cancel option
                    ++crossedCount;     // not necessary for test107
                    if (true) //crossedCount > m_fimCrossedMax)
                    {
                        isInvalid = true;
                        break;
                    }
                }
                prev = node;
            }
        }

        /// insert node
        if (!isInvalid)
        {
            used.push_back(node);
            auto insertI = open.erase(iter);
            _fimRecursiveVariationCreation(used, open, hullCurrentI, crossedCount);
            iter = open.insert(insertI, node);
            used.remove(node);
        }
        // not necessary, because of copy in fnc-call - hullCurrentI = lastHullI;
        crossedCount = crossedCountLast;
    }
}

std::list<Node*> SortedMerge::FimVariations::next()
{
    if (variations.empty())
        return std::list<Node*>();
    if (m_fimVariationsI == nullptr)
    {
        m_fimVariationsI = new SortedMerge::FimVariations::FimVariationContainerType::iterator();
        *m_fimVariationsI = variations.begin();
        return **m_fimVariationsI;
    }
    else
    {
        ++(*m_fimVariationsI);
        if (*m_fimVariationsI == variations.end())
            return std::list<Node*>();
        else
            return **m_fimVariationsI;
    }
}

void SortedMerge::FimVariations::_calcBorderVariations_fastImperfect(std::list<oko::Node*> &innerPointsNodes)
{
    std::list<Node*> used;
    used.push_back(m_fimHullNodes.front());
    auto innerPointsRun = innerPointsNodes;
    auto findI = std::find(innerPointsRun.begin(), innerPointsRun.end(), m_fimHullNodes.front());
    innerPointsRun.erase(findI);
    _fimRecursiveVariationCreation(used, innerPointsRun, m_fimHullNodes.begin(), 0);
}

SortedMerge::Variations SortedMerge::_calcBorderVariations(std::list<oko::Node*> &innerPointsNodes)
{
    assert(innerPointsNodes.size() > 1);
    Variations vars;

    Point center(0,0);
    for (auto const &node : innerPointsNodes)
    {
        center += node->point;
    }
    size_t n = innerPointsNodes.size();
    center = Point(center.x()/n, center.y()/n);

    Node * nodePrev = innerPointsNodes.back();
    size_t currPos = 0;
    auto nodeNextI = innerPointsNodes.begin();
    for (auto nodeCurrI = innerPointsNodes.begin();
         nodeCurrI != innerPointsNodes.end(); ++nodeCurrI)
    {
        ++currPos;
        ++nodeNextI;
        if (nodeNextI == innerPointsNodes.end())
            nodeNextI = innerPointsNodes.begin();

        double distCurrPrev = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, nodePrev->point);
        double distCurrNext = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, (*nodeNextI)->point);
        double possibleDistOfCurrentNode = std::min(distCurrPrev, distCurrNext);
        // because test95
        //double possibleDistOfCurrentNode = (distCurrPrev + distCurrNext)/2.0;
        double distCurrAligned = TSPHelper::seboehDist(nodePrev, *nodeNextI, *nodeCurrI);
        double lengthTriCurrent= _calcTriLength(nodePrev->point, (*nodeNextI)->point, center, (*nodeCurrI)->point);
        // HINT: There was an idea - Due to test95 here is a new measurement in 3D
        double distHyperCurrent = TSPHelper::seboehHyperDist(nodePrev->point, (*nodeNextI)->point, (*nodeCurrI)->point);
        Point hullVectorCrossedPoint(std::numeric_limits<float>::max()
                                     , std::numeric_limits<float>::max());
        Point hullVectorCrossedPointSecond(std::numeric_limits<float>::max()
                                     , std::numeric_limits<float>::max());
        Point hullVectorCrossedPointThird(std::numeric_limits<float>::max()
                                     , std::numeric_limits<float>::max());

        // standard algorithm for standard features
        size_t posClosest = 0;
        size_t pos = 0;
        bool isDistByDistance = false;
        bool isDistByTriLength = false;
        bool isDistByAlignment = false;
        bool isDistByClosestAlignment = false;
        bool isDistByHull = false;
        bool isDistByHullSecond = false;
        bool isDistByHullThird = false;

        // TODO program as single step
        // feature closest align
        // see test87
        // use convexSorting instead
        double distClosestAlign = std::numeric_limits<double>::max();
        size_t posClosestAlign = 0;
        double criteriaClosestAlign2Distance = std::numeric_limits<double>::max();
//        std::tie(isDistByClosestAlignment, posClosestAlign, distClosestAlign, criteriaClosestAlign2Distance)
//                = _featureClosestAlign(
//                    innerPointsNodes, *nodeCurrI, nodePrev, *nodeNextI);

//        posClosest = posClosestAlign;

        // HINT: other methodu would be:
        // - distance between mid-point of triangel center and mid-point of triangle currentNode divided by "hypothenuse"
        // - area triangel of currentNode diveded by area triangle of center
        auto nextI = innerPointsNodes.begin();
        Node *nodePrev2 = innerPointsNodes.back();
        for (const auto &node : innerPointsNodes)
        {
            ++pos;
            ++nextI;
            if (nextI == innerPointsNodes.end())
                nextI = innerPointsNodes.begin();
            if (node != *nodeCurrI
//                    && node != *nodeNextI           // TODO [seb] perhapse include next node? no?
                    && node != nodePrev
               )
            {

                // HINT: dist calculation is the weakest criterium, due to missing information in the meaturment
                double distPrev = defaultFnc::defaultDistanceFnc(node->point, (*nodeCurrI)->point);
                double distAligned = TSPHelper::seboehDist(node, *nextI, *nodeCurrI);
                double distNext = defaultFnc::defaultDistanceFnc((*nextI)->point, (*nodeCurrI)->point);
                //double dist = (distPrev + distNext)/2.0;
                double dist = distPrev;

                auto nextNextI = nextI;
                ++nextNextI;
                if (nextNextI == innerPointsNodes.end())
                    nextNextI = innerPointsNodes.begin();
                if (*nextNextI == *nodeCurrI)
                {
                    ++nextNextI;
                    if (nextNextI == innerPointsNodes.end())
                        nextNextI = innerPointsNodes.begin();
                }
                double distAlignedNext = TSPHelper::seboehDist(*nextI, *nextNextI, *nodeCurrI);
                double distAlignedPrev = TSPHelper::seboehDist(nodePrev2, node, *nodeCurrI);

                if (dist < possibleDistOfCurrentNode        // necessary test74; not =, because else both dist can be the same edge (-> length)
                        // does not work in test85           && distAligned < 2*dist       // because of test71 270x210
                        // for test85, test81, test74 and test86
                        // does not work because test86 && (distAligned < distAlignedNext && distAligned < distAlignedPrev)
                        && distAligned < distAlignedNext
                        && !isDistByAlignment
                        && !isDistByTriLength
                        && criteriaClosestAlign2Distance > distAligned
                    )
                {       // =, because test67, dist can be equal, but due to variation later this does not distroy descent degree.
                    possibleDistOfCurrentNode = dist;
                    if (!(isDistByClosestAlignment
                            && std::labs(static_cast<long>(posClosestAlign) - pos) < 2))     // see test87, test81
                    {
                        posClosest = pos;
                        double distBefore = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, nodePrev2->point);
                        double distAfter = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, (*nextI)->point);
                        double alignBefore = TSPHelper::seboehDist(nodePrev2, node, *nodeCurrI);
                        double alignAfter = TSPHelper::seboehDist(node, *nextI, *nodeCurrI);
                        if (alignBefore < alignAfter     // test86, 240x290
                            )
                        {
                            if (posClosest > 1)     // count about pos starting with 1
                                --posClosest;
                            else
                                posClosest = innerPointsNodes.size();
                        }
                        isDistByDistance = true;
                    }
                }

                // HINT: triangle length is the best measurement, due to many informations
                // for test71 it should align 270x210 at 440x110
                double triangleRate = _calcTriLength(node->point, (*nextI)->point, center, (*nodeCurrI)->point);

                if (false // off, because test93 - triangleRate < lengthTriCurrent
                    )
                {
                    // other distance measurements with highest priority
                    // it shall do something like to check, if already specified point is in new triangle
                    // TODO [seboeh] check if there are dots between the triangle
                    lengthTriCurrent = triangleRate;
                    posClosest = pos;
                    isDistByTriLength = true;
                }

                // because of test95
                Point hullFarestInnerEdgePoint;
                if (distPrev < distNext)
                {
                    hullFarestInnerEdgePoint = (*nextI)->point;
                }
                else
                {
                    hullFarestInnerEdgePoint = node->point;
                }
                Point hullVector = (*nodeCurrI)->hullVector;
                if (hullVector.x() != 0 || hullVector.y() != 0)
                {
                    hullVector /= std::sqrt(hullVector.x()*hullVector.x() + hullVector.y()*hullVector.y());
                    double hullInnerEdgeDist = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, hullFarestInnerEdgePoint);
                    Point hullExtendedInnerNode = (*nodeCurrI)->point + hullVector * hullInnerEdgeDist;
                    Point crossedPoint;
                    auto crossedOnHull = TSPHelper::isCrossed((*nodeCurrI)->point, hullExtendedInnerNode
                                                              , node->point, (*nextI)->point, &crossedPoint);
                    if ((crossedOnHull == TSPHelper::CrossedState::isCrossed
                            || crossedOnHull == TSPHelper::CrossedState::isOnPoint)
                        // due to test97 - && !isDistByDistance
                        && !isDistByAlignment)
                    {
                        if (defaultFnc::defaultDistanceFnc(hullVectorCrossedPoint, (*nodeCurrI)->point)
                                > defaultFnc::defaultDistanceFnc(crossedPoint, (*nodeCurrI)->point))
                        {
                            posClosest = pos;
                            isDistByHull = true;
                            hullVectorCrossedPoint = crossedPoint;  // necessary for test98
                        }
                    }
                }

                // for test98
                hullVector = (*nodeCurrI)->hullVectorSecond;
                if (hullVector.x() != 0 || hullVector.y() != 0)
                {
                    hullVector /= std::sqrt(hullVector.x()*hullVector.x() + hullVector.y()*hullVector.y());
                    double hullInnerEdgeDist = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, hullFarestInnerEdgePoint);
                    Point hullExtendedInnerNode = (*nodeCurrI)->point + hullVector * hullInnerEdgeDist;
                    Point crossedPoint;
                    auto crossedOnHull = TSPHelper::isCrossed((*nodeCurrI)->point, hullExtendedInnerNode
                                                              , node->point, (*nextI)->point, &crossedPoint);
                    if ((crossedOnHull == TSPHelper::CrossedState::isCrossed
                            || crossedOnHull == TSPHelper::CrossedState::isOnPoint)
                        && !isDistByAlignment
                        && !isDistByHull
                        )
                    {
                        if (defaultFnc::defaultDistanceFnc(hullVectorCrossedPointSecond, (*nodeCurrI)->point)
                                > defaultFnc::defaultDistanceFnc(crossedPoint, (*nodeCurrI)->point))
                        {
                            posClosest = pos;
                            isDistByHullSecond = true;
                            hullVectorCrossedPointSecond = crossedPoint;  // necessary for test98
                        }
                    }
                }

                // for test102
                hullVector = (*nodeCurrI)->hullVectorThird;
                if (hullVector.x() != 0 || hullVector.y() != 0)
                {
                    hullVector /= std::sqrt(hullVector.x()*hullVector.x() + hullVector.y()*hullVector.y());
                    double hullInnerEdgeDist = defaultFnc::defaultDistanceFnc((*nodeCurrI)->point, hullFarestInnerEdgePoint);
                    Point hullExtendedInnerNode = (*nodeCurrI)->point + hullVector * hullInnerEdgeDist;
                    Point crossedPoint;
                    auto crossedOnHull = TSPHelper::isCrossed((*nodeCurrI)->point, hullExtendedInnerNode
                                                              , node->point, (*nextI)->point, &crossedPoint);
                    if ((crossedOnHull == TSPHelper::CrossedState::isCrossed
                            || crossedOnHull == TSPHelper::CrossedState::isOnPoint)
                        && !isDistByAlignment
                        && !isDistByHull
                        && !isDistByHullSecond
                        )
                    {
                        if (defaultFnc::defaultDistanceFnc(hullVectorCrossedPointThird, (*nodeCurrI)->point)
                                > defaultFnc::defaultDistanceFnc(crossedPoint, (*nodeCurrI)->point))
                        {
                            posClosest = pos;
                            isDistByHullThird = true;
                            hullVectorCrossedPointThird = crossedPoint;  // necessary for test98
                        }
                    }
                }

                // HINT: alignment has more priority than distance and less than lengthTriangle.
                // This is the second best measurement, because closest to gravity.
                if (distAligned < distCurrAligned
                        && !isDistByTriLength
                   )
                {
                    distCurrAligned = distAligned;
                    posClosest = pos;
                    isDistByAlignment = true;
                }
            }
            nodePrev2 = node;
        }

        // insert pos
        if (posClosest == currPos)
            posClosest = 0;
        vars.variations.push_back({*nodeCurrI,
                                    posClosest});
                                   //0});

        auto altPosI = vars.variations.begin();
        if (posClosest > 0)
        {
            for (size_t i = 0; i < posClosest-1; ++i)
                ++altPosI;
        }

        // because of reduction of side-by-side efects.
        // omit on test98 - not usefull
/*
        if (    //false)
                posClosest > 0
                && posClosest <= vars.variations.size()
                )
        {
            // because of TEST61, the if.
            if (altPosI->altPos == currPos)
            {       // because side-by-side collision
                auto nodeAltI = std::find(innerPointsNodes.begin(), innerPointsNodes.end(), altPosI->node);
                assert(nodeAltI != innerPointsNodes.end());

                auto nodePrevI = nodeAltI;
                if (nodePrevI == innerPointsNodes.begin())
                    nodePrevI = --innerPointsNodes.end();
                else
                    --nodePrevI;
                auto nodeAltNextI = nodeAltI;
                ++nodeAltNextI;
                if (nodeAltNextI == innerPointsNodes.end())
                    nodeAltNextI = innerPointsNodes.begin();

                // not using this distances, because of test74 - using new minus old seboehDist.
//                double maxDistOfCurrentNode = (std::max)(distCurrPrev, distCurrNext);
//                double maxDistOfAltNode = (std::max)(
//                            defaultFnc::defaultDistanceFnc((*nodeAltI)->point, (*nodeNextI)->point)
//                            , defaultFnc::defaultDistanceFnc((*nodeAltI)->point, (*nodePrevI)->point));
                // maxDistOfCurrentNode > maxDistOfAltNode
                double distAltOld = TSPHelper::seboehDist(*nodePrevI, *nodeAltNextI, *nodeAltI);
                double distAltNew1 = TSPHelper::seboehDist(nodePrev, *nodeCurrI, *nodeAltI);
                double distAltNew2 = TSPHelper::seboehDist(*nodeCurrI, *nodeNextI, *nodeAltI);
                double distAltNew = std::min(distAltNew1, distAltNew2);
                double distCurrOld = TSPHelper::seboehDist(nodePrev, *nodeNextI, *nodeCurrI);
                double distCurrNew1 = TSPHelper::seboehDist(*nodePrevI, *nodeAltI, *nodeCurrI);
                double distCurrNew2 = TSPHelper::seboehDist(*nodeAltI, *nodeAltNextI, *nodeCurrI);
                double distCurrNew = std::min(distCurrNew1, distCurrNew2);

                if ((distCurrNew - distCurrOld) < (distAltNew - distAltOld)     // the logical test
                    && distCurrNew < distAltNew     // test, because test74
                   )
                {
                    altPosI->altPos = 0;
                }
                else
                {
                    vars.variations.back().altPos = 0;
                }
            }
        }
*/

        nodePrev = *nodeCurrI;
    }

    return vars;
}

std::list<oko::Node*> SortedMerge::Variations::next()
{
    std::list<Node*> nodes;
    if (m_stateFinished)
        return nodes;           // cancel criteria.

    for (const Entry &entry : variations)
        nodes.push_back(entry.node);

    // create new variation
    std::vector<Node*> alreadyInserted;
    std::vector<size_t> alreadyInsertedPos;
    alreadyInserted.resize(nodes.size(), nullptr);
    alreadyInsertedPos.resize(nodes.size(), 0);
    size_t pos = 0;
    for (const Entry &entry : variations)
    {
        ++pos;      // pos starting with 1
        if (entry.altPos == 0
            || entry.state == Entry::State::UNCHANGED
        )
            continue;
        auto iterOrig = std::find(nodes.begin(), nodes.end(), entry.node);
        assert(nodes.size() >= entry.altPos);
        auto iterNewPos = nodes.begin();
        for (size_t i=0; i < entry.altPos; ++i)
            ++iterNewPos;
        iterNewPos = nodes.insert(iterNewPos, entry.node);
        nodes.erase(iterOrig);

        // correct the alreadyInserted - see test71
        alreadyInsertedPos[pos-1] = entry.altPos;     // altPos starting with 1
        alreadyInserted[pos-1] = entry.node;
        for (size_t alreadyPos = 0; alreadyPos < pos; ++alreadyPos)
        {
            if (alreadyInsertedPos[alreadyPos] == pos)
            {
                auto alreadyNode = alreadyInserted[alreadyPos];
                auto alreadyIterOrig = std::find(nodes.begin(), nodes.end(), alreadyNode);
                nodes.insert(iterNewPos, alreadyNode);
                nodes.erase(alreadyIterOrig);
            }
        }
    }

    // iter variation
    auto lastEntryI = variations.end();
    for (auto entryI = variations.begin();
         entryI != variations.end(); ++entryI)
    {
        if (entryI->altPos > 0)
            lastEntryI = entryI;
    }
    if (lastEntryI == variations.end())
    {       // return because no iteration necessary, only the first default variation.
        m_stateFinished = true;
        return nodes;
    }
    Entry::State lastEntryState = lastEntryI->state;

    for (Entry &entry : variations)
    {
        if (entry.altPos > 0)
        {
            if (entry.state == Entry::State::UNCHANGED)
            {
                entry.state = Entry::State::ALIGNED;
                break;
            }
            else if (entry.state == Entry::State::ALIGNED)
            {
                entry.state = Entry::State::UNCHANGED;
            }
        }
    }

    // test71, test37, test81
    // all same altPos shall be handled in same step
    // switched off, due to test71
    // switched on, due to test88 (too slow)?
//    for (const auto &var : variations)
//    {
//        for (auto &var2 : variations)
//        {
//            if (var.node == var2.node)
//                continue;
//            if (var.altPos == var2.altPos)
//                var2.state = var.state;
//        }
//    }

    // TODO [seboeh] why is there multiple times the same combinations?

    if (lastEntryState == Entry::State::ALIGNED && lastEntryI->state == Entry::State::UNCHANGED)
        m_stateFinished = true;

    return nodes;
}

std::tuple<SortedMerge::output_value, double> SortedMerge::search(const std::list<Node*> &fromNodes, input_value &innerPoints)
{
    m_isCanceled = false;
    //auto innerPointsNodes = _prepareHeuristicalOrder(innerPoints);
    std::list<Node*> innerPointsNodes;
    for (const auto &ce : innerPoints)
        innerPointsNodes.push_back((*ce).insertNode);

    FimVariations variationsBig;
    Variations variationsSmall;
    if (!m_fimSimpleAlignment)
    {
        // fast old style
        variationsSmall = _calcBorderVariations(innerPointsNodes);
    }
    else
    {
        // new style - better at big blobs, because more correct
        variationsBig._calcBorderVariations_fastImperfect(innerPointsNodes);
    }

    std::list<Node*> newVariation;
    double bestResTotalOverVariationsDist = std::numeric_limits<double>::max();
    while (true)
    {
        if (!m_fimSimpleAlignment)
        {
            newVariation = variationsSmall.next();
        }
        else
        {
            newVariation = variationsBig.next();
        }
        if (newVariation.empty())
            break;


        std::list<Node*> nodeOrderByVariations = _createContent(fromNodes, newVariation);
        if (nodeOrderByVariations.empty())
            throw "invalid input for sorted Merge.";

        input_value newInnerPoints;
        for (const auto &node : nodeOrderByVariations)
        {
            auto iter = std::find_if(innerPoints.begin(), innerPoints.end(),
                                     [node](input_value::value_type &val)
            {
                    return val->insertNode == node;
            });
            if (iter != innerPoints.end())
            {
                newInnerPoints.push_back(*iter);
            }
        }
        innerPoints = std::move(newInnerPoints);

        m_countOfNodes = innerPoints.size();
        m_bestDistTotal = std::numeric_limits<double>::max();

        for (size_t startElCount = 0; startElCount < m_countOfNodes; ++startElCount)
        {
            std::list<Node*> startingNodes;
            // precondition innerPoints.begin() is the first/closest node.
            input_value::iterator innerLastI = innerPoints.begin();
            size_t nodesCount = 0;
            while (innerLastI != innerPoints.end() && nodesCount <= startElCount)
            {
                startingNodes.push_back((*innerLastI)->insertNode);
                ++innerLastI;
                ++nodesCount;
            }

            OpenTreeNodeType openNodes;     // all other nodes, expect startingNodes.
            while (innerLastI != innerPoints.end() && nodesCount < m_countOfNodes)
            {
                openNodes.push_back((*innerLastI)->insertNode);
                ++innerLastI;
                ++nodesCount;
            }

            SearchTreeType tree;
            SimpleSample samp;
            samp.innerNodes = startingNodes;
            // because the innerNodes change by nodeOrder
            samp.firstNode = startingNodes.front();
            tree.push_back(samp);

            _searchRecursive(tree, openNodes, 0);
            if (m_isCanceled)
                break;

            if (!m_fimSimpleAlignment)
            {
                // shifting - Finite Input Response problem (FIR)
                for (size_t shiftNum = 1; shiftNum <= startElCount && startElCount < (m_countOfNodes-1); ++shiftNum)
                {

                    std::list<Node*> startingNodes;
                    // precondition innerPoints.begin() is the first/closest node.
                    input_value::iterator innerLastI = innerPoints.begin();
                    size_t nodesCount = 0;
                    while (innerLastI != innerPoints.end() && nodesCount <= (startElCount - shiftNum))
                    {
                        startingNodes.push_back((*innerLastI)->insertNode);
                        ++innerLastI;
                        ++nodesCount;
                    }

                    OpenTreeNodeType openNodes;     // all other nodes, expect startingNodes.
                    while (innerLastI != innerPoints.end() && nodesCount < (m_countOfNodes - shiftNum))
                    {
                        openNodes.push_back((*innerLastI)->insertNode);
                        ++innerLastI;
                        ++nodesCount;
                    }

                    auto insertIter = startingNodes.begin();
                    while (innerLastI != innerPoints.end() && nodesCount < m_countOfNodes)
                    {
                        startingNodes.insert(insertIter, (*innerLastI)->insertNode);
                        ++innerLastI;
                        ++nodesCount;
                    }

                    SearchTreeType tree;
                    SimpleSample samp;
                    samp.innerNodes = startingNodes;
                    // because the innerNode change by nodeOrder.
                    samp.firstNode = samp.innerNodes.front();
                    tree.push_back(samp);

                    _searchRecursive(tree, openNodes, 0);
                    if (m_isCanceled)
                        break;
                }
                if (m_isCanceled)
                    break;
            }
        }

        // update best res
        double bestResTotalDist = 0;
        for (const auto &el : m_bestResTotal)
        {
            bestResTotalDist += el.bestDist;
        }
        if (bestResTotalDist < bestResTotalOverVariationsDist)
        {
            // read result
            bestResTotalOverVariationsDist = bestResTotalDist;
            m_result.clear();
            output_value::value_type outEl;
            for (const auto &el : m_bestResTotal)
            {
                outEl.innerNodes = el.nodeOrder;
                outEl.hullEdgeFrom = el.bestFrom;
                m_result.push_back(outEl);
            }
        }

        if (m_isCanceled)
            break;

    }       // while over variations

    return std::make_tuple(std::ref(m_result), std::ref(m_bestDistTotal));
}

void SortedMerge::_insertToCeList(const Node *node, std::list<ClosestEdges> &ceList, const Edge &edge)
{
    ClosestEdges ce(const_cast<Node*>(node));
    if (!ceList.empty())
    {
        double dist;
        std::list<ClosestEdges>::iterator iter;

        // alignment on closest seboehDist in list.
        //std::tie(iter, dist) = TSPHelper::findBestSingleSearch(ceList, ce);

        // simple push_back
        iter = ceList.end();
        if (iter != ceList.end())
        {
            ce.fromNode = iter->insertNode;
            ce.toNode = iter->toNode;
            iter->toNode = ce.insertNode;
        }
        else
        {
            ce.fromNode = ceList.back().insertNode;
            ce.toNode = ceList.back().toNode;
        }
        ceList.emplace(iter, std::move(ce));
    }
    else
    {
        ce.fromNode = edge.fromNode;
        ce.toNode = edge.toNode;
        ceList.emplace_back(std::move(ce));
    }
}


void SortedMerge::_updateValue(const Edge &edge, const std::list<Node*> &newOrder, GroupFromNodeType::mapped_type &value)
{
    std::list<ClosestEdges> ceList;
    for (const Node *node : newOrder)
    {
        _insertToCeList(node, ceList, edge);
    }
    double dist = TSPHelper::correctDistHelper(ceList);
    if (dist < value.dist)
    {
        value.dist = dist;
        value.nodeOrder = newOrder;
    }

    ceList.clear();
    for (auto iter = --newOrder.end(); true; --iter)
    {
        Node *node = *iter;
        _insertToCeList(node, ceList, edge);
        if (iter == newOrder.begin())
            break;
    }
    dist = TSPHelper::correctDistHelper(ceList);
    if (dist < value.dist)
    {
        value.dist = dist;
        value.nodeOrder = newOrder;
        value.nodeOrder.reverse();
    }
}

void SortedMerge::_insertToNodeList(std::list<Node*> nodeListB, std::list<Node*> insertAtoB)
{
    for (Node* node : insertAtoB)
    {
        double distClosest = std::numeric_limits<double>::max();
        auto bestI = nodeListB.end();
        for (auto nI = nodeListB.begin();
             nI != nodeListB.end(); ++nI)
        {
            double dist = defaultFnc::defaultDistanceFnc((*nI)->point, node->point);
            if (distClosest > dist)
            {
                bestI = nI;
                distClosest = dist;
            }
        }
        auto prevI = bestI; --prevI;
        double distPrev = defaultFnc::defaultDistanceFnc((*prevI)->point, node->point);
        auto nextI = bestI; ++nextI;
        double distNext = defaultFnc::defaultDistanceFnc((*nextI)->point, node->point);
        if (distPrev < distNext)
        {
            nodeListB.insert(bestI, node);
        } else {
            nodeListB.insert(nextI, node);
        }
    }
}

std::list<Node*>::const_iterator
SortedMerge::_prepareCalcGravity(const Node* elected, const std::list<Node*> &newNodes)
{
    size_t pos = 0;
    double gravSum = 0;
    double gravDivisorSum = 0;
    double globalDist = defaultFnc::defaultDistanceFnc(
                newNodes.front()->point
                , newNodes.back()->point);
    double gravPrecisionHack = std::trunc(globalDist*globalDist);
    for (auto pointI = newNodes.begin();
         pointI != newNodes.end(); ++pointI)
    {
        const Node* pointN = *pointI;
        if (pointN == elected)
            continue;
        ++pos;
        auto nextPointI = pointI; ++nextPointI;
        if (nextPointI == newNodes.end())
            nextPointI = newNodes.begin();
        if (*nextPointI == elected)
            ++nextPointI;
        const Node *nextPointN = *nextPointI;
        double gravDivisor = std::pow(
                    TSPHelper::seboehDist(pointN, nextPointN, elected)
                    , 2);
        gravDivisorSum += gravPrecisionHack / gravDivisor;
        gravSum += (pos * gravPrecisionHack) / gravDivisor;
        // HINT: we assume mass m=1 and pos is a x_i of the first momentum m_1 (or gravity center point - Schwerpunkt)
        // over the position from the start to the end of 1D-node-list
        // HINT: seboehDist() is just an approximation to the gravity of the whole edge.
    }
    // resulting in position
    size_t newPos = static_cast<size_t>(gravSum / gravDivisorSum);
    auto newPosI = newNodes.begin();
    for (size_t i=0; i < newPos-1; ++i, ++newPosI)
    {
        if (*newPosI == elected)
        {
            --i;
            continue;
        }
    }
    if (newPosI == newNodes.begin())
        return --newNodes.end();        // because we want return the prev node in circle.
    else
        return newPosI;
}

std::list<oko::Node*>
SortedMerge::_prepareWithGravityRecalculations(std::list<Node*> &newNodes)
{
    bool isFinished = false;
    size_t iterations = 0;
    size_t n = newNodes.size();
    while (!isFinished && iterations < n*n)
    {       // because, start from begining again, if new position
        ++iterations;
        Node *oldPos = newNodes.back();
        isFinished = true;
        for (auto electedI = newNodes.begin();
             electedI != newNodes.end(); ++electedI)
        {
            // get gravity-pos
            const Node* const elected = *electedI;
            auto newPosI = _prepareCalcGravity(elected, newNodes);
            if (oldPos != *newPosI)
            {
                isFinished = false;
                ++newPosI;
                if (newPosI == newNodes.end())
                    newPosI = newNodes.begin();
                newNodes.insert(newPosI, *electedI);
                newNodes.erase(electedI);
                break;
            }
        }
    }
    if (iterations >= n*n)
        std::cerr << "WARNING: _prepareWithGravityRecalculations()"\
                     " have had too many iterations. Result can be incorrect." << std::endl;
    return newNodes;
}

void SortedMerge::_prepareGetBestNode(double &distBest, const Node* electedNode, std::list<Node*> &nodeListA, std::list<Node*>::iterator &bestNode)
{
    if (electedNode == nodeListA.front()
        || electedNode == nodeListA.back())
        return;
    for (auto nodeI = nodeListA.begin();
         nodeI != nodeListA.end(); ++nodeI)
    {
        if (*nodeI == electedNode)
            continue;
        auto nextNodeI = nodeI; ++nextNodeI;
        if (nextNodeI == nodeListA.end())
            break;
        if (*nextNodeI == electedNode)
            ++nextNodeI;
        double dist = TSPHelper::seboehDist(*nodeI, *nextNodeI, electedNode);
        if (dist < distBest)
        {
            distBest = dist;
            bestNode = nodeI;
        }
    }
}

void SortedMerge::_prepareInsertToNewList(std::list<Node*> &nodeListAlt, std::list<Node*> &nodeListNew, std::list<Node*>::iterator bestNodeNewI, Node* electedNode)
{
    auto nextBestI = bestNodeNewI; ++nextBestI;
    if (*nextBestI != electedNode)
    {
        nodeListAlt.remove(electedNode);
        nodeListNew.insert(nextBestI, electedNode);
    }
}

std::list<oko::Node*>
SortedMerge::_prepareHeuristicalOrder(input_value &innerPoints)
{
    std::list<Node*> nodeListA, nodeListB;
    for (const auto &el : innerPoints)
    {
        if (el->insertNode->wayNum == 1)
            nodeListA.push_back(el->insertNode);
        else
            nodeListB.push_back(el->insertNode);
    }

    for (auto nodeI = innerPoints.begin();
         nodeI != innerPoints.end(); ++nodeI)
    {
        const Node* hullNode = (*nodeI)->fromNode;
        const Node* nextHullNode = Way::nextChild(hullNode);
        Node* electedNode = (*nodeI)->insertNode;
        double distHull = TSPHelper::seboehDist(hullNode, nextHullNode, electedNode);

        double distBestA = distHull;
        auto bestNodeAI = nodeListA.end();
        _prepareGetBestNode(distBestA, electedNode, nodeListA, bestNodeAI);
        double distBestB = distHull;
        auto bestNodeBI = nodeListB.end();
        _prepareGetBestNode(distBestB, electedNode, nodeListB, bestNodeBI);

        double distNothingToDo = std::numeric_limits<double>::max();
        if (nodeListA.size() > 1 && electedNode == nodeListA.front())
        {
            distNothingToDo = TSPHelper::seboehDist(nodeListB.back(), *(++nodeListA.begin()), electedNode);
        } else if (nodeListB.size() > 1 && electedNode == nodeListB.back())
        {
            distNothingToDo = TSPHelper::seboehDist(*(--(--nodeListB.end())), nodeListA.front(), electedNode);
        } else if (nodeListA.size() > 1 && electedNode == nodeListA.back())
        {
            distNothingToDo = TSPHelper::seboehDist(*(--(--nodeListA.end())), nodeListB.front(), electedNode);
        } else if (nodeListB.size() > 1 && electedNode == nodeListB.front())
        {
            distNothingToDo = TSPHelper::seboehDist(*(++nodeListB.begin()), nodeListA.back(), electedNode);
        }

        if (distBestA < distHull && distBestA < distBestB && distBestA < distNothingToDo)
        {
            _prepareInsertToNewList(nodeListB, nodeListA, bestNodeAI, electedNode);
        }
        else if (distBestB < distHull && distBestB < distBestA && distBestB < distNothingToDo)
        {
            _prepareInsertToNewList(nodeListA, nodeListB, bestNodeBI, electedNode);
        }
        else if (distHull < distNothingToDo)
        {
            if (electedNode->wayNum == 1)
            {
                if (electedNode == nodeListA.front()
                        || electedNode == nodeListA.back())
                    continue;       // because move to list-b will not help for TSP.
            }
            else
            {
                if (electedNode == nodeListB.front()
                        || electedNode == nodeListB.back())
                    continue;
            }
            if (electedNode->wayNum == 1)
                nodeListA.remove(electedNode);
            else
                nodeListB.remove(electedNode);

            if (TSPHelper::seboehDist(nodeListA.front(), nodeListB.back(), electedNode)
               < TSPHelper::seboehDist(nodeListA.back(), nodeListB.front(), electedNode))
            {
                if (electedNode->wayNum == 1)
                    nodeListB.push_back(electedNode);
                else
                    nodeListA.push_front(electedNode);
            }
            else
            {
                if (electedNode->wayNum == 1)
                    nodeListB.push_front(electedNode);
                else
                    nodeListA.push_back(electedNode);
            }
        }
    }

    std::list<Node*> newNodes;
    for (const auto &nodeP : nodeListA)
        newNodes.push_back(nodeP);
    for (const auto &nodeP : nodeListB)
        newNodes.push_back(nodeP);
    return newNodes;
}

std::list<oko::Node*>
SortedMerge::_prepareHeuristicalOrder_old2(input_value &innerPoints)
{
    std::list<Node*> newNodes;
    for (const auto &ce : innerPoints)
        newNodes.push_back(ce->insertNode);

    newNodes = _prepareWithGravityRecalculations(newNodes);

    return newNodes;
}

std::list<oko::Node*>
SortedMerge::_prepareHeuristicalOrder_old(input_value &innerPoints)
{
    // HINT: in fromNode is the closest edge inside, due to preparation in TamarAlgorithm::mergeCascada().
    std::list<Node*> insertAtoB;
    std::list<Node*> insertBtoA;
    for (std::shared_ptr<ClosestEdges> ce : innerPoints)
    {
        if (ce->fromNode->wayNum == 1
                && ce->insertNode->wayNum == 2)
        {
            insertBtoA.push_back(ce->insertNode);
        }
        if (ce->fromNode->wayNum == 2
                && ce->insertNode->wayNum == 1)
        {
            insertAtoB.push_back(ce->insertNode);
        }
    }

    std::list<Node*> nodeListA;
    std::list<Node*> nodeListB;
    for (std::shared_ptr<ClosestEdges> ce : innerPoints)
    {
        if (std::find(insertAtoB.begin(), insertAtoB.end(), ce->insertNode) == insertAtoB.end()
                || std::find(insertBtoA.begin(), insertBtoA.end(), ce->insertNode) == insertBtoA.end())
            continue;
        if (ce->insertNode->wayNum == 1)
            nodeListA.push_back(ce->insertNode);
        else
            nodeListB.push_back(ce->insertNode);
    }

    _insertToNodeList(nodeListB, insertAtoB);
    _insertToNodeList(nodeListA, insertBtoA);

    for (auto &node : nodeListA)
        node->wayNum = 1;

    for (auto &node : nodeListB)
    {
        node->wayNum = 2;
        nodeListA.push_back(node);
    }
    return nodeListA;
}


std::list<Node*> SortedMerge::_createContent(const std::list<oko::Node*> &fromNodes, std::list<oko::Node*> &innerPointsNodes)
{
    /// for size of n, the group size
    // TODO [seboeh] remove on refactoring - guaranty that it will be
    size_t pointsCount = innerPointsNodes.size();
    if (pointsCount == 0)
        return std::list<Node*>();

    std::list<Edge> edges;
    for (const Node* el : fromNodes)
    {
        Edge edge;
        edge.fromNode = const_cast<Node*>(el);
        edge.toNode = const_cast<Node*>(Way::nextChild(el));
        edges.push_back(edge);
    }

    for (size_t n=1; n <= pointsCount; ++n)
    {
        /// set initial iterators
        Sample s;
        auto innerLastI = innerPointsNodes.begin();
        for (size_t i=0; innerLastI != innerPointsNodes.end() && i < n; ++i)
        {
            s.innerNodes.push_back(*innerLastI);
            ++innerLastI;
        }
        s.firstNode = s.innerNodes.front();     // used later as fromNode index

        /// walk through nodes
        Node *innerPoint = nullptr;
        Node *startPoint = s.innerNodes.back();
        do {
            /// walk through hull edges
            s.froms = std::make_shared<GroupFromNodeType>();
            for (const Edge &edge : edges)
            {

// HINT: calculating a small TSP again, is to much work.
//                double dist;
//                std::list<Node*> nodeOrder;
//                std::tie(dist, nodeOrder) = _findBestFrom(edge.fromNode, edge.toNode, s.innerNodes);

// alternative
                std::list<ClosestEdges> ceList;
                for (const Node *node : s.innerNodes)
                {
                    _insertToCeList(node, ceList, edge);
                }
                double dist = TSPHelper::correctDistHelper(ceList);

                GroupFromNodeType::mapped_type value;
                if (!m_fimSimpleAlignment)
                {
                    ceList.clear();
                    for (auto iter = --s.innerNodes.end(); true; --iter)
                    {
                        Node *node = *iter;
                        _insertToCeList(node, ceList, edge);
                        if (iter == s.innerNodes.begin())
                            break;
                    }
                    double dist2 = TSPHelper::correctDistHelper(ceList);

                    value.from = edge.fromNode;
                    value.nodeOrder = s.innerNodes;         // if using new TSP, here nodeOrdered, instead of innernNodes
                    if (dist2 < dist)
                    {
                        value.nodeOrder.reverse();
                        value.dist = dist2;
                    }
                    else
                    {
                        value.dist = dist;
                    }
                }
                else
                {
                    value.from = edge.fromNode;
                    value.nodeOrder = s.innerNodes;         // if using new TSP, here nodeOrdered, instead of innernNodes
                    value.dist = dist;
                }

                if (!m_fimSimpleAlignment)
                {
                    // flip
                    if (s.innerNodes.size() > 2)
                    {
                        for (auto changeI = ++s.innerNodes.begin();
                             changeI != s.innerNodes.end(); ++changeI)
                        {
                            // from change -> begin-backward -> end-backward -> change
                            // = flip right halfe only
                            std::list<Node*> newOrder;
                            for (auto iter = changeI;
                                 true; --iter)
                            {
                                newOrder.push_back(*iter);
                                if (iter == s.innerNodes.begin())
                                    break;
                            }
                            for (auto iter = --s.innerNodes.end();
                                 iter != changeI; --iter)
                            {
                                newOrder.push_back(*iter);
                            }

                            _updateValue(edge, newOrder, value);

                            // from begin-forward -> change -> end-backward -> change
                            // = flip right and left half
                            newOrder.clear();
                            for (auto iter = s.innerNodes.begin();
                                 iter != changeI; ++iter)
                            {
                                newOrder.push_back(*iter);
                            }
                            for (auto iter = --s.innerNodes.end();
                                 iter != changeI; --iter)
                            {
                                newOrder.push_back(*iter);
                                if (iter == s.innerNodes.begin())
                                    break;
                            }
                            newOrder.push_back(*changeI);

                            _updateValue(edge, newOrder, value);

                            // for test29
    //                        newOrder.clear();
    //                        for (auto iter = s.innerNodes.begin();
    //                             iter != changeI; ++iter )
                        }
                    }
                }

                s.froms->insert({value.dist, value});
            }
            /// insert
            m_samples.insert({s.firstNode, s});

            /// iterate
            if (innerLastI == innerPointsNodes.end())
                innerLastI = innerPointsNodes.begin();
            innerPoint = *innerLastI;
            ++innerLastI;
            s.innerNodes.pop_front();
            s.innerNodes.push_back(innerPoint);
            s.firstNode = s.innerNodes.front();     // used later as fromNode index
        } while (innerPoint != startPoint);

    }
    return innerPointsNodes;
}


SortedMerge::SearchTreeType & SortedMerge::_setBestAndCalcDist(SearchTreeType tree)
{
    // HINT: vary order of groups - see test19
    // shift the constellation, due to test29 - FIR (finite input response) problem.
    size_t shiftN = tree.front().innerNodes.size();
    if (m_fimSimpleAlignment)
        shiftN = 1;
    for (size_t shiftC = 0; shiftC < shiftN; ++shiftC)
    {
        // flip the order - still necessary due to test54 test63 test19
        for (size_t orderC = 0; orderC < tree.size(); ++orderC)
        {
            // stop if time exceeded
            if (m_startTime.time_since_epoch().count() > 0 &&
                    (std::chrono::system_clock::now() - m_startTime) > std::chrono::minutes(6))
            {
                m_isCanceled = true;
                return m_bestResTotal;
            }

            // calc dist
            double dist = _calcDistAccordingToTree(tree);
            if (dist == std::numeric_limits<double>::max())
                return m_bestResTotal;        // because, return is not relevant in this function currently.

            // set best
            if (dist < m_bestDistTotal)
            {
                m_bestDistTotal = dist;
                m_bestResTotal = tree;
            }
            tree.push_back(std::move(tree.front()));
            tree.pop_front();

        }

        if (!m_fimSimpleAlignment)
        {
            // shift
            auto nextI = tree.begin();
            for (auto currentI = tree.begin();
                 currentI != tree.end(); ++currentI)
            {
                bool isReverseCurrent = currentI->innerNodes.front() != currentI->firstNode;
                ++nextI;
                if (nextI == tree.end())
                    nextI = tree.begin();
                bool isReverseNext = nextI->innerNodes.front() != nextI->firstNode;

                Node* insertEl;
                if (isReverseCurrent)
                    insertEl = std::move(currentI->innerNodes.front());
                else
                    insertEl = std::move(currentI->innerNodes.back());
                if (isReverseNext)
                    nextI->innerNodes.push_back(insertEl);
                else
                    nextI->innerNodes.push_front(insertEl);
                if (isReverseCurrent)
                    currentI->innerNodes.pop_front();
                else
                    currentI->innerNodes.pop_back();
                if (isReverseNext)
                    nextI->firstNode = nextI->innerNodes.back();
                else
                    nextI->firstNode = nextI->innerNodes.front();
            }
        }
    }
    return m_bestResTotal;
}


SortedMerge::SampleType::iterator SortedMerge::_findSample(SimpleSample &el)
{
    auto sampleIterRange = m_samples.equal_range(el.firstNode);
    if (sampleIterRange.first == m_samples.end())
        return m_samples.end();     // no iterator found
    SampleType::iterator sampleIter;
    bool isGlobalFailed = true;
    for (sampleIter = sampleIterRange.first;
          sampleIter != sampleIterRange.second; ++sampleIter)
    {
        bool isFailed = false;
        if (sampleIter->second.innerNodes.size() != el.innerNodes.size())
            continue;
        auto sampleIterNodeI = sampleIter->second.innerNodes.begin();
        for (const auto &node : el.innerNodes)
        {
// HINT:  code without respect order of innerNodes.
//            if (std::find(sampleIter->second.innerNodes.begin()
//                          , sampleIter->second.innerNodes.end(), node)
//                    == sampleIter->second.innerNodes.end())
//            {
//                isFailed = true;
//                break;
//            }
            // exact match is possible, due to fixed order of innerNodes.
            if (*sampleIterNodeI != node)
            {
                isFailed = true;
                break;
            }
            else
                ++sampleIterNodeI;
        }
        if (!isFailed)
        {
            isGlobalFailed = false;
            break;
        }
    }
    if (isGlobalFailed)
        return m_samples.end();
    else
        return sampleIter;
}

double SortedMerge::_calcDistAccordingToTree(SearchTreeType &tree)
{
    double dist = 0;
    std::map<Node*, bool> occupiedFromNodes;
    for (SimpleSample &el : tree)
    {
        auto sampleIter = _findSample(el);
        if (sampleIter == m_samples.end())
        {
            std::cerr << "Error: a sample was not found. Part of nodes ignored." << std::endl;
            return std::numeric_limits<double>::max();
        }
        // find from, which is not occupied yet
        el.bestFrom = nullptr;
        el.bestDist = std::numeric_limits<double>::max();
        for (auto fromIter = sampleIter->second.froms->begin();
             fromIter != sampleIter->second.froms->end(); ++fromIter)
        {
            if (occupiedFromNodes.find(fromIter->second.from) == occupiedFromNodes.end())
            {
                occupiedFromNodes.insert({fromIter->second.from, true});
                el.bestDist = fromIter->second.dist;
                el.bestFrom = fromIter->second.from;
                el.nodeOrder = fromIter->second.nodeOrder;
                break;
            }
        }
        if (el.bestFrom == nullptr)
        {       // to few edges for too many separate nodes.
            return std::numeric_limits<double>::max();
        }
        dist += el.bestDist;

        /// pruning
        if (dist > m_bestDistTotal)
            return dist;
    }
    return dist;
}

SortedMerge::SearchTreeType::iterator SortedMerge::_gapsFindBiggestList(SearchTreeType &tree)
{
    auto treeBiggestI = tree.end();
    size_t treeBiggestSize = 0;
    for (auto treeI = tree.begin(); treeI != tree.end(); ++treeI)
    {
        if (treeI->innerNodes.size() > treeBiggestSize)
        {
            treeBiggestI = treeI;
            treeBiggestSize = treeI->innerNodes.size();
        }
    }

    return treeBiggestI;
}

void SortedMerge::_gaps(SearchTreeType &tree)
{
    if (tree.empty())
        return;

    // concentrate only on biggest innerNodes list
    auto treeCopy = tree;
    auto treeBiggestI = _gapsFindBiggestList(treeCopy);
    auto biggestNodeList = treeBiggestI->innerNodes;
    // reduce the size of tree.
    if (treeBiggestI->innerNodes.size() < 3)
        return;
    treeBiggestI = tree.erase(treeBiggestI);

    // start recursion to generate mask and then start the calculation and setting of best tree.
    std::vector<size_t> mask(biggestNodeList.size()-2);
    _gapsNextRecursion(biggestNodeList, treeCopy, treeBiggestI, mask);
}

bool SortedMerge::_gapsCheckOnValid(std::vector<size_t> &in, SearchTreeType &tree, std::list<Node*> &nodes, SearchTreeType::iterator &treeBiggestI)
{
    // test on valid - is increasing except zero
    /*
     * 100203004
     * 001102003
     * ...
     */
    int lastVal = 0;
    bool isIncreased = false;
    for (int val : in)
    {
        if (val >= lastVal)
        {
            if (val > lastVal+1)
                return false;             // only one by one of mask numbers are ok
            if (val > lastVal)
                ++lastVal;
            isIncreased = false;
        }
        else if (val == 0)
        {
            if (!isIncreased)
                ++lastVal;
            isIncreased = true;
        }
        else
            return false;
    }
    return true;
}

void SortedMerge::_gapsNextRecursion(std::list<Node*> &nodes, SearchTreeType &tree, SearchTreeType::iterator &treeBiggestI, std::vector<size_t> &in)
{
    size_t s = in.size();
    bool isContinue = true;
    bool isCanceled = false;
    while (!isCanceled)
    {
        isContinue = false;
        for (size_t i = 0; i < s; ++i)
        {
            if (in[i] == s)
            {
                in[i] = 0;
                continue;
            }
            ++in[i];

            if (!_gapsCheckOnValid(in, tree, nodes, treeBiggestI))
            {
                isContinue = true;
                break;
            }

            _gapsCalculate(nodes, tree, treeBiggestI, in);
        }
        if (!isContinue)
            isCanceled = true;
    }
}

void SortedMerge::_gapsCalculate(std::list<Node*> &nodes, SearchTreeType &tree, SearchTreeType::iterator &treeBiggestI, std::vector<size_t> &currentGapsVect)
{
    size_t s = currentGapsVect.size();
    std::vector<std::list<Node*>> newTree(s+1);
    size_t i = 0;       // first is always in base tree
    newTree[0].push_back(nodes.front());
    for (const auto &node : nodes)
    {
        if (i == 0)     // due to first node push_back above
        {
            ++i;
            continue;
        }
        if (i == s+1)
            break;
        newTree[currentGapsVect[i-1]].push_back(node);
        ++i;
    }
    newTree[0].push_back(nodes.back());     // last is always in base tree

    auto treeInsertStartI = tree.end();
    for (const auto &anchor : newTree)
        if (!anchor.empty())
        {
            SimpleSample ss;
            ss.innerNodes = anchor;
            ss.firstNode = anchor.front();
            auto nextI = tree.insert(treeBiggestI, ss);
            if (treeInsertStartI == tree.end())
                treeInsertStartI = nextI;
        }

    _setBestAndCalcDist(tree);

    treeBiggestI = tree.erase(treeInsertStartI, treeBiggestI);       // for next iteration
}

// main rec fnc
/**
  program-run of search

    1. recursive (Rec) through the tree of combinations
       combinations are:
         1 1 1 1 1
         1 1 1 2 2
         1 1 2 2 1
         1 1 3 3 3
         ...
         1 2 2 1 1
         ...
         1 4 4 4 4
         5 5 5 5 5      // the set is an example. It is in build in SearchTreeType. This is a parameter of recursive fnc.

    2. on comming back from recursion,
        enlarge the set for next depth search
    3. on reaching final combination,
        check if lower limit (best) is over-stepped. And abort then.
    4. Is final combination reached, and lower then best, set best to this combination.
  */
SortedMerge::SearchTreeType SortedMerge::_searchRecursive(SearchTreeType tree, OpenTreeNodeType openNodes, size_t level)
{
    // 1.
    // cancel criteria
    if (openNodes.empty())
    {
        return _setBestAndCalcDist(tree);
        // TODO [seb] remove return type from this function, due to unused.
     }

    // recursion
    SearchTreeType::value_type nodesTuple;
    OpenTreeNodeType restOpenNodes = openNodes;
    for (const auto &openNode : openNodes)
    {
        // creating tree
        nodesTuple.innerNodes.push_back(openNode);
        nodesTuple.firstNode = nodesTuple.innerNodes.front();
        tree.push_back(nodesTuple);

        // recursive call
        restOpenNodes.pop_front();

        ++level;

        // 2.
        _searchRecursive(tree
                          , restOpenNodes
                          , level);

        if (m_isCanceled)
            break;

        // iterate
        tree.pop_back();        // because next other element with higher count.
    }

    return m_bestResTotal;
}
