#ifndef WAY_H
#define WAY_H

#include "Point.h"
#include "Parameters.h"
#include "dll.h"

#include <list>

namespace TSP {

class EXTERN Way
{
public:
    typedef std::list<Point> WayType;
    struct State { enum StateType { ISNULL=1, ISINFINITE=2, ISDAMAGED=4, ISLENGTHCALCULATED=8, ISODD=16}; };

    Way() : mWay(), mLength(0), mState(State::ISNULL), m_number(0) {}

    /**
     * @brief Makes a depth copy of the way.
     */
    Way(const Way &val)
    {
        *this = val;
    }

    Way(const Point &start, const Point &end) {
        Way();
        push_back(start);
        push_back(end);
    }

    Way &operator=(const Way &val)
    {               /// operator= necessary because move optimization in std::list
        mLength = val.mLength;
        mState = val.state();
        m_number = val.number();
        mWay.clear();
        for (WayType::const_iterator valI=val.way()->begin();
             valI != val.way()->end(); ++valI)
        {
            mWay.push_back(*valI);
        }
        return *this;
    }

    inline const WayType * way() const { return &mWay; }
    inline WayType * wayVolatile() { return &mWay; }
    inline void push_back(const Point &val) { mWay.push_back(val); mState &= ~State::ISNULL; mState &= ~State::ISLENGTHCALCULATED; }
    void push_back(const Way &);
    void push_front(const Way &);
    inline Point * back() { return &(mWay.back()); }
    inline Point * front() { return &(mWay.front()); }
    double length(Parameters &para);
    inline int state() const { return mState; }
    inline void state(int st) { mState = st; }
    inline void clear() { mWay.clear(); mState = State::ISNULL; mLength = 0; m_number=0; }
    inline int number() const { return m_number; }
    inline void number(int num) { m_number = num; }
    inline void pop_back() { mWay.pop_back(); mState &= ~State::ISNULL; mState &= ~State::ISLENGTHCALCULATED; }

private:
    WayType mWay;
    double mLength;
    int mState;
    int m_number;

};

}

#endif // WAY_H
