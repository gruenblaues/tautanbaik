#include "TspAlgoCommon.h"

using namespace TSP;

TspAlgoCommon::TspAlgoCommon() : TspAlgorithm()
{
    m_resultWay = new Way();     // way is initialized with ISNULL.
}

TspAlgoCommon::~TspAlgoCommon()
{
    // TODO [seboeh] have to be a shared ptr, because used outside of algorithm. - delete m_resultWay;
}

void TspAlgoCommon::startRun(InputCitiesType *in, const Parameters &para)
{
    m_cities = in;
    m_para = para;
    m_mutex.lock();
    m_indicators.progress(0);
    m_mutex.unlock();
    // start works only if client creates this thread new,
    // because old thread is dead.
    this->start();
    //run();
}

void TspAlgoCommon::run()
{
    m_mutex.lock();
    int state = m_indicators.state();
    state &= ~Indicators::States::ISNULL;
    state |= Indicators::States::ISRUNNING;
    m_indicators.state(state);
    m_mutex.unlock();

    m_resultWay->clear();
    m_resultLength = INFINITY;

    // it is not forseen to have multiple same points
    std::map<double, std::map<double, bool>> reduceSamePointsMap;
    auto iter = reduceSamePointsMap.end();

    InputCitiesPointerType * cityPoints = new InputCitiesPointerType();
    for (InputCitiesType::iterator cityIter=m_cities->begin();
         cityIter != m_cities->end(); ++cityIter)
    {
        if (! reduceSamePointsMap[cityIter->x()].insert({cityIter->y(), true}).second)
            continue;
        cityPoints->push_back(&(*cityIter));        // HINT: for safety we create the points in the heap. They are used to build the result.
    }
    buildTree(m_tree.root(), 0, cityPoints);

    calculateResult(m_tree.root());

    cityPoints->clear();
    delete cityPoints;

    m_mutex.lock();
    state = m_indicators.state();
    state &= ~Indicators::States::ISRUNNING;
    state |= Indicators::States::ISFINISHED;
    m_indicators.state(state);
    m_mutex.unlock();
}

void TspAlgoCommon::buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    int k = m_para.decissionTreeChildCountFnc(level);
    float l = m_para.decissionTreeWeightFnc(level);
    float progressPercentage = 0.25 / openPointsBasic->size() / (level+1);
        /// 0.25 percentage increment, because two threads increase the percentage simultanious.
        /// 0.5 would fit for one thread. Leads to 0.5/2 = 0.25.

    /// fill with children.
    int i=0;
    for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
         i < k && openNodeIter != openPointsBasic->end(); ++i, ++openNodeIter)
    {
        InputCitiesPointerType * openPoints = newOpenPoints( openPointsBasic, *(*openNodeIter) );
        DT::DTNode::ChildType::iterator newChildIter
                = node->addChild(
                    createWay(*(*openNodeIter)
                              , openPoints
                              , l));
        (*newChildIter)->parent(node);
        incProgress(progressPercentage);
        buildTree(*newChildIter, level + 1, openPoints );
        openPoints->clear();
        delete openPoints;
        if ( (level == 0) && (m_para.algoType == Parameters::AlgorithmType::BellmannHeldKarpInternet) ) {
            break;      /// for Naive version it is enough to calculate all possibilities for one start-node.
        }
    }
}

TspAlgorithm::InputCitiesPointerType * TspAlgoCommon::newOpenPoints(InputCitiesPointerType * openPointsBasic, Point &startPoint)
{
    Q_ASSERT(openPointsBasic != NULL);
    InputCitiesPointerType * points = new InputCitiesPointerType();
    for (InputCitiesPointerType::iterator cityIter = openPointsBasic->begin();
         cityIter != openPointsBasic->end(); ++cityIter)
    {
        if (*(*cityIter) == startPoint) {
            continue;
        }
        points->push_back(*cityIter);
    }
    return points;
}


void TspAlgoCommon::calculateResult(DT::DTNode::NodeType node)
{
    Q_ASSERT(node != NULL);
    if (node->children()->empty()) {
        m_indicators.progress(m_indicators.progress()+0.01);
        /// 1. reached leaf - create way.
        Way *way = new TSP::Way();
        DT::DTNode::NodeType prev = node->parent();
        int number=0;
        while (prev != NULL) {
            way->number(++number);
            way->push_front(*(node->value()));
            node = prev;
            prev = node->parent();
        }
        /// calculate the length
        double length = way->length(m_para);
        if (length > 0)
        {
            length += m_para.distanceFnc(*(way->front()), *(way->back())); /// way-length + length from start to end. Because of length in a circle
            if (length < m_resultLength) {
                m_resultLength = length;
                *m_resultWay = *way;
            }
        }
        delete way;
        return;
    } else {
        for (DT::DTNode::ChildType::iterator childI = node->children()->begin();
             childI != node->children()->end(); ++childI)
        {
            calculateResult(*childI);
        }
    }
}

Indicators TspAlgoCommon::status()
{
    m_mutex.lock();
    Indicators indi = m_indicators;
    m_mutex.unlock();
    return indi;
}

Result TspAlgoCommon::result()
{
    Result res;
    res.way(*m_resultWay);
    m_resultLength = m_resultWay->length(m_para);
    if (m_resultLength > 0)     // else, empty way.
        m_resultLength += m_para.distanceFnc(*(m_resultWay->front()), *(m_resultWay->back()));      /// way-length + length from start to end. Because of length in a circle
    res.wayLength(m_resultLength);
    res.wasCancled(m_isCanceled);

    return res;
}

void TspAlgoCommon::pause()
{
    std::cerr << "PROGRAMM-ERROR: pause() not implemented." << std::endl;

}

void TspAlgoCommon::stopRun()
{
    if (m_para.isRun != nullptr)
        *m_para.isRun = false;
}

void TspAlgoCommon::incProgress(float percent)
{
    m_mutex.lock();
    m_indicators.progress(m_indicators.progress() + percent);
    m_mutex.unlock();
}

DT::DTTree * TspAlgoCommon::getDtTree()
{
    return &m_tree;
}

void TspAlgoCommon::setBlockedEdges(std::list<std::pair<Point,Point>> &&edges)
{
}
