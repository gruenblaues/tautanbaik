#pragma once

#include <memory>
#include <tuple>

#include "TspAlgoCommon.h"
#include "dataStructures/OlavList.h"
#include "dataStructures/PolygonWay.h"
#include "dataStructures/commonstructures.h"
#include "bspTreeVersion/BspTreeSarah.h"

namespace TSP {

class VoronoiClusterSort : public TspAlgoCommon
{
public:
    using Node = comdat::Node;
    using NodeL = std::list<std::shared_ptr<comdat::Node>>;
    using PointL = std::list<TSP::Point>;
    using WayOlav = oko::OlavList;
    using Poly = oko::PolygonWay;
    enum class MergeCallState {
        leftRight, topDown
    };
    enum class DirectAccessState {
        isDirectAccess, isNoDirectAccess
    };
    enum class CrossedState {
        isCrossed, isParallel, isCovered, isOnPoint, isOutOfRange
    };
    typedef oko::ClosestEdges ClosestEdges;
    typedef std::list<std::shared_ptr<ClosestEdges>> ClosestEdgesListInputType;
    struct EdgeIter {
        EdgeIter(WayOlav::iterator &aNodeIter, WayOlav::iterator &bNodeIter) : aNodeI(aNodeIter), bNodeI(bNodeIter) {}
        WayOlav::iterator aNodeI;
        WayOlav::iterator bNodeI;
    };
    struct Cluster {
        Cluster(Node*n) { point = std::shared_ptr<Node>(n, no_op_delete()); }
        Cluster() = default;
        PointL verts;
        std::shared_ptr<Node> point;
        double area = 0.0;
        std::vector<std::shared_ptr<Node>> closestN;
        std::list<Node*> closestTestedPointsSorted;
        std::list<Cluster*> closestSortedNeighbours;
        std::map<double, std::tuple<Node*,Node*>> neighbourEdges;
        int state = 0;      // 1 = isAligned
        int iterationC = 0;
    };
    typedef std::list<Cluster> ClusterM;
    typedef std::list<Cluster*> ClusterPL;

    struct Yet {
        Cluster *clu;
        Point yetPrev;
        Point yetNext;
    };

    struct no_op_delete
    {
        void operator()(void*) { }
    };

    /** globals **/
    static size_t tspSize;
    static double tspWidth;
    static double tspHeight;

    /** Ctor **/
    VoronoiClusterSort();

protected:
    /** Impl **/
    void buildTree(DT::DTNode::NodeType node, float level, InputCitiesPointerType *openPointsBasic);

private:
    NodeL createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic);
    std::tuple<NodeL, WayOlav *> determineHull(NodeL &input);
    ClusterM determineClusters(NodeL &input, NodeL &hull);
    void alignToHull(std::map<double, Cluster *> &clusters, NodeL &hull);
    NodeL determineWay(ClusterM &clusters, NodeL &hull);

    std::shared_ptr<Poly> _devideConquer(ClusterPL openClusters);
    std::shared_ptr<Poly> _merge(std::shared_ptr<Poly> wayPrev, std::shared_ptr<Poly> way, Cluster *cluPivot);
    void _triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, WayOlav::iterator &hpaI, WayOlav::iterator &lpaI, WayOlav::iterator &hpbI, WayOlav::iterator &lpbI);
    WayOlav *_newConqueredWay(bsp::BspNode *node);
    bsp::BspTreeSarah _createBSPTree(NodeL &input);
    void _mergeHull(WayOlav *&way1, const TSP::Point &pWay1Center, WayOlav *&way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter);
    bool _isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside);
    DirectAccessState _isDirectAccess(const TSP::Point &pMeanA, WayOlav::iterator &pAI, const TSP::Point &pMeanB, WayOlav::iterator &pBI);
    CrossedState _isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint = nullptr);
    bool _isNormalVectorLeft(const Point &from, const Point &to, const Point &insert);
    double _calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3);
    double _area(const PointL &polygon);
    bool _isCovered(WayOlav *way, WayOlav::iterator &w1I, WayOlav::iterator &fixI);
    void _createTriangle3x1(WayOlav *way1, WayOlav::iterator &insideI, WayOlav::iterator &fixI);
    bool _createTriangle2x2(WayOlav *way1, WayOlav *way2, WayOlav::iterator &w1Inside, WayOlav::iterator &w1Alt, WayOlav::iterator &way2I, WayOlav::iterator &way2NI);
    void _flipWay(WayOlav *way, TSP::Point pCenter);
    bool _isClockwise(TSP::Point pCenter, WayOlav *way);
    Node *_nextNode(Node *curr, Node * &prev);
    double _triangleAltitudeOnC(Point A, Point B, Point C);
    void _insertNewNodeToEdge(WayOlav *way1, Node *fromNode, Node *insertNode);
    bool _isRightSide(const Point &p1, const Point &p2);
    double _calcDegree(Node *node, Node *nodeTested);
    Cluster *_getPivotCluster(ClusterPL &clusters);
    void _insertNewNodeToPolyOnBestAlign(std::shared_ptr<Poly> way, Node *insertNode);
    std::list<Node *> _removePointsFromPoly(std::shared_ptr<Poly> way, Cluster *cluPivot, Cluster *cluCloseNeighPrev, Cluster *cluCloseNeigh);
    double _tradeof(const Point &pEdgeA1, const Point &pEdgeA2, const Point &pEdgeB1, const Point &pEdgeB2);
    std::tuple<double, Node *, Node *, Node *, Node *> _bestTradeof(std::list<Node *> way1Froms, std::list<Node *> way1Tos, std::list<Node *> way2Froms, std::list<Node *> way2Tos);        ///< bestDist, bestWay1P, bestWay2N, bestWay1N, bestWay2P);
    std::tuple<Node *, Node *, Node *, Node *> _findBestTradeof(std::shared_ptr<Poly> way1, std::shared_ptr<Poly> way2);
    std::list<Node *> _removeCommonPoints(std::shared_ptr<Poly> way1, std::shared_ptr<Poly> way2);
    void _removeNode(std::list<Node *> &container, const Node *val);
    void _insertIfNotExist(std::list<Node *> &container, Node *val);
    bool _isCrossedInWay(std::shared_ptr<Poly> way1, Node *way1P, Node *way2N);
};

}
