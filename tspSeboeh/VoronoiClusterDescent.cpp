#include "VoronoiClusterDescent.h"

#include <algorithm>
#include <tuple>

#include "bspTreeVersion/BspTreeSarah.h"
#include "TamarAlgorithmHelper.h"
#include "dataStructures/OlavList.h"

using TSPWay = TSP::Way;

using namespace TSP;
using namespace std;
using namespace TSPHelper;

size_t VoronoiClusterDescent::tspSize = 0;
double VoronoiClusterDescent::tspWidth = 0;
double VoronoiClusterDescent::tspHeight = 0;

VoronoiClusterDescent::VoronoiClusterDescent()
    : TspAlgoCommon()
{

}

VoronoiClusterDescent::NodeL VoronoiClusterDescent::createInputRepresentation(TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    NodeL ret;

    double tspMaxWidth = 0;
    double tspMaxHeight = 0;
    double tspMinWidth = numeric_limits<double>::max();
    double tspMinHeight = numeric_limits<double>::max();
    for (const auto &point : *openPointsBasic)
    {
        ret.push_back(std::make_shared<Node>(*point));
        if (tspMaxWidth < point->x())
            tspMaxWidth = point->x();
        if (tspMinWidth > point->x())
            tspMinWidth = point->x();
        if (tspMaxHeight < point->y())
            tspMaxHeight = point->y();
        if (tspMinHeight > point->y())
            tspMinHeight = point->y();
    }
    tspWidth = tspMaxWidth - tspMinWidth;
    tspHeight = tspMaxHeight - tspMinHeight;

    return ret;
}

bsp::BspTreeSarah VoronoiClusterDescent::_createBSPTree(NodeL &input)
{
    InputCitiesPointerType cities;
    for (const auto &node : input)
        cities.push_back(new TSP::Point(node->point));
    // HINT: above. better to use the nodes from input than new points.
    // because later search of hull-node necessary - see mark MARK_hullNodeSearch

    bsp::BspTreeSarah bspTree;
    bspTree.buildTree(&cities);

    return bspTree;
}

std::tuple<VoronoiClusterDescent::NodeL, VoronoiClusterDescent::WayOlav*> VoronoiClusterDescent::determineHull(NodeL &input)
{
    NodeL ret;
    // * split input into quad-BSP-tree
    // * on returning from recursion merge to cyclic WayOlav (convex hull).

    /// 1. create bsp tree - divide
    //////////////////////////
    bsp::BspTreeSarah bspTree = _createBSPTree(input);

    /// 2. create points order - merge
    //////////////////////////
    WayOlav * way = nullptr;
    auto root = bspTree.root();
    if (root != nullptr) {
        way = _newConqueredWay(root);
        if (way != nullptr)
        {
            // HINT: A way with a cycle not reaching the valI.end(), will lead to crash this app by a zombie.
            for (WayOlav::iterator valI=way->begin();
                 !valI.end(); )
            {
                Node *node = valI.nextChild();
                node->isHull = true;
                ret.push_back(std::shared_ptr<Node>(node));
            }
// TODO [seboeh] check if possible
//            way->clear();
//            delete way;
        }
    }

    // update input with new nodes from way
    WayOlav::iterator wayI = way->begin();
    while (!wayI.end())
    {
        Node *node = wayI.nextChild();
        auto inputI = find_if(input.begin(), input.end(),
                              [node] (const NodeL::value_type &val)
        {
            return val->point == node->point;
        });
        assert(inputI != input.end());
        *inputI = shared_ptr<Node>(node, no_op_delete());
    }

    // * create resulting list

    return make_tuple(ret, way);
}


bool VoronoiClusterDescent::_isClockwise(TSP::Point pCenter, WayOlav *way)
{
    WayOlav::iterator pI = way->begin();
    TSP::Point p1 = *pI - pCenter;
    ++pI;       // need convex hull points, else it could rotate counter-clock-wise in a banana shape.
    TSP::Point p2 = *pI - pCenter;
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

void VoronoiClusterDescent::_flipWay(WayOlav *way, TSP::Point pCenter)
{
    if(!_isClockwise(pCenter, way))
        way->flipDirection();
}

VoronoiClusterDescent::WayOlav *VoronoiClusterDescent::_newConqueredWay(bsp::BspNode *node)
{
    if (node->m_value != nullptr)
    {
        // HINT: the way is deleted after run, after moving result to m_resultWay.
        WayOlav *way = new WayOlav();
        way->push_back(*(node->m_value));
        node->m_center = *(node->m_value);
//        way->setOrigin(node);
        return way;
    }
    WayOlav * wayUpper;
    WayOlav * wayLower;
    TSP::Point pLowerCenter;
    TSP::Point pUpperCenter;
    if (node->m_upperLeft) {
        WayOlav * wayUpperLeft = _newConqueredWay(node->m_upperLeft);
        if (node->m_upperRight) {
            WayOlav * wayUpperRight = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperLeft->m_center + node->m_upperRight->m_center;
            pUpperCenter = Point(pUpperCenter.x()/2.0, pUpperCenter.y()/2.0);

            /// HINT: test37 needs master middle "node->m_center", instead of upperLeft upperRight middle.
            _mergeHull(wayUpperLeft, node->m_upperLeft->m_center, wayUpperRight, node->m_upperRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayUpperRight;
        } else {
            pUpperCenter = node->m_upperLeft->m_center;
        }
        wayUpper = wayUpperLeft;
    } else {
        if (node->m_upperRight) {
            wayUpper = _newConqueredWay(node->m_upperRight);
            pUpperCenter = node->m_upperRight->m_center;
        } else {
            wayUpper = nullptr;
        }
    }
    if (node->m_lowerLeft) {
        WayOlav * wayLowerLeft = _newConqueredWay(node->m_lowerLeft);
        if (node->m_lowerRight) {
            WayOlav * wayLowerRight = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerLeft->m_center + node->m_lowerRight->m_center;
            pLowerCenter = Point(pLowerCenter.x()/2.0, pLowerCenter.y()/2.0);

            _mergeHull(wayLowerLeft, node->m_lowerLeft->m_center, wayLowerRight, node->m_lowerRight->m_center, MergeCallState::leftRight, node->m_center);
            delete wayLowerRight;
        } else {
            pLowerCenter = node->m_lowerLeft->m_center;
        }
        wayLower = wayLowerLeft;
    } else {
        if (node->m_lowerRight) {
            wayLower = _newConqueredWay(node->m_lowerRight);
            pLowerCenter = node->m_lowerRight->m_center;
        } else {
            wayLower = nullptr;
        }
    }

    //incProgress(1/m_progressMax);

    if (wayUpper && wayLower) {
        // HINT: test37 needs master center node->m_center.
        // HINT: test2 needs flipWay
        _flipWay(wayLower, pLowerCenter);
        _flipWay(wayUpper, pUpperCenter);
        _mergeHull(wayUpper, pUpperCenter, wayLower, pLowerCenter, MergeCallState::topDown, node->m_center);
        delete wayLower;
        return wayUpper;
    } else if (wayUpper) {
        return wayUpper;
    } else if (wayLower) {
        return wayLower;
    } else {
        return nullptr;
    }
}


bool VoronoiClusterDescent::_isInsideTriangle(const Point &p1, const Point &p2, const Point &p3, const Point &inside)
{
    // [http://en.wikipedia.org/wiki/Barycentric_coordinate_system]
    if (p1.y() < inside.y() && p2.y() < inside.y() && p3.y() < inside.y())
        return false;
    if (p1.y() > inside.y() && p2.y() > inside.y() && p3.y() > inside.y())
        return false;
    if (p1.x() < inside.x() && p2.x() < inside.x() && p3.x() < inside.x())
        return false;
    if (p1.x() > inside.x() && p2.x() > inside.x() && p3.x() > inside.x())
        return false;
    // barycentric coordinates.
    double determinant = ((p2.y() - p3.y())*(p1.x() - p3.x()) + (p3.x() - p2.x())*(p1.y() - p3.y()));
    double alpha = ((p2.y() - p3.y())*(inside.x() - p3.x()) + (p3.x() - p2.x())*(inside.y() - p3.y())) / determinant;
    if (alpha < 0 || alpha > 1)
        return false;
    double beta = ((p3.y() - p1.y())*(inside.x() - p3.x()) + (p1.x() - p3.x())*(inside.y() - p3.y())) / determinant;
    if (beta < 0 || beta > 1)
        return false;
    double gamma = 1.0 - alpha - beta;
    if (gamma < 0)
        return false;
    return true;
}

void VoronoiClusterDescent::_triangleRoofCorrection(std::list<EdgeIter> &directAccessEdges, WayOlav::iterator &hpaI, WayOlav::iterator &lpaI, WayOlav::iterator &hpbI, WayOlav::iterator &lpbI)
{
    double trianglePointDistLow = m_para.distanceFnc(*lpaI, *lpbI);
    double trianglePointDistHigh = m_para.distanceFnc(*hpaI, *hpbI);

    bool isALowInside = _isInsideTriangle(*lpbI, *hpbI, *hpaI, *lpaI);
    bool isAHighInside = _isInsideTriangle(*lpbI, *hpbI, *lpaI, *hpaI);

    if (!isALowInside && !isAHighInside)
        return;

    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);
        double dist = m_para.distanceFnc(pa, pb);

        if (pa == *hpaI
                && pb == *lpbI
                && isALowInside
            )
        {
            if (dist > trianglePointDistLow)
            {
                lpaI = hpaI;
                trianglePointDistLow = dist;
            }
        }
        if (pa == *lpaI
                && pb == *hpbI
                && isAHighInside
            )
        {
            if (dist > trianglePointDistHigh)
            {
                hpaI = lpaI;
                trianglePointDistHigh = dist;
            }
        }
    }
}

VoronoiClusterDescent::CrossedState VoronoiClusterDescent::_isCrossed(const Point &pa1I, const Point &pa2I, const Point &pb1I, const Point &pb2I, Point *crossPoint)
{
    /// Crossed between a and b?
    double x1 = pa1I.x();
    double y1 = pa1I.y();
    double x2 = pa2I.x();
    double y2 = pa2I.y();
    double x3 = pb1I.x();
    double y3 = pb1I.y();
    double x4 = pb2I.x();
    double y4 = pb2I.y();

    /// HINT: [http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection]
    double denominator = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (denominator == 0) {
        // lines are parallel not guarantied to lay on each other.
        // We want to integrate the considered node as inner node -> so it should not be covered -> parallel
        return CrossedState::isParallel;
    }
    double pX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denominator;
    double pY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denominator;
    if (crossPoint != nullptr)
        *crossPoint = TSP::Point(pX,pY);

    if ((std::max)(std::min(x1,x2),std::min(x3,x4)) <= pX && pX <= std::min((std::max)(x1,x2),(std::max)(x3,x4))
            && (std::max)(std::min(y1,y2),std::min(y3,y4)) <= pY && pY <= std::min((std::max)(y1,y2),(std::max)(y3,y4)))
    {
        if ((pX == x1 && pY == y1)          // FIX: Could be a speed-up, if we check if this crossing on exact one outer point could happen with the input.
            || (pX == x2 && pY == y2)
            || (pX == x3 && pY == y3)
            || (pX == x4 && pY == y4))
        {
            return CrossedState::isOnPoint;
        } else {
            return CrossedState::isCrossed;
        }
    } else {
        return CrossedState::isOutOfRange;
    }
}

VoronoiClusterDescent::DirectAccessState VoronoiClusterDescent::_isDirectAccess(const TSP::Point &pMeanA, WayOlav::iterator &pAI, const TSP::Point &pMeanB, WayOlav::iterator &pBI)
{
    WayOlav::iterator prevAI = pAI; --prevAI;
    WayOlav::iterator nextAI = pAI; ++nextAI;
    WayOlav::iterator prevBI = pBI; --prevBI;
    WayOlav::iterator nextBI = pBI; ++nextBI;
    CrossedState state = _isCrossed(*prevAI, pMeanA, *pAI, *pBI);
    if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
    {
        state = _isCrossed(*nextAI, pMeanA, *pAI, *pBI);
        if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
        {
            state = _isCrossed(*prevBI, pMeanB, *pAI, *pBI);
            if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
            {
                state = _isCrossed(*nextBI, pMeanB, *pAI, *pBI);
                if ( state == CrossedState::isParallel || state == CrossedState::isOnPoint || state == CrossedState::isOutOfRange)
                {
                    return DirectAccessState::isDirectAccess;
                }
            }
        }
    }

    return DirectAccessState::isNoDirectAccess;
}

bool VoronoiClusterDescent::_isNormalVectorLeft(const Point &from, const Point &to, const Point &insert)
{
    // [https://de.wikipedia.org/wiki/Normalenvektor]
    // because coordinate system has left,top null-point, we turn the left/right
//    Point to = Point(-fromNode->point.y(), fromNode->point.x());
//    Point from = Point(-toNode->point.y(), toNode->point.x());

    Point straight(to.x() - from.x()
                   , to.y() - from.y());
//    straight = Point(-straight.y(), straight.x());

    Point meanPoint((from.x() + to.x())/2
                    , (from.y() + to.y())/2);

    Point normalVectLeft(meanPoint.x() - straight.y()
                         , meanPoint.y() + straight.x());
    double distLeft = m_para.distanceFnc(normalVectLeft, insert);

    Point normalVectRight(meanPoint.x() + straight.y()
                          , meanPoint.y() - straight.x());
    double distRight = m_para.distanceFnc(normalVectRight, insert);

    return distLeft < distRight;
}


bool VoronoiClusterDescent::_isCovered(WayOlav * way, WayOlav::iterator &w1I, WayOlav::iterator &fixI)
{
    WayOlav::iterator wI = way->begin();
    WayOlav::iterator wNI = wI; ++wNI;
    for (; !wI.end(); ++wI, ++wNI)
    {
        if (*wI == *w1I || *wNI == *w1I)
            continue;
        Point crossedPoint;
        CrossedState state = _isCrossed(*wI, *wNI, *w1I, *fixI, &crossedPoint);
        if (state == CrossedState::isCrossed || state == CrossedState::isCovered)
            return true;
        if (state == CrossedState::isOnPoint)
        {
            if (crossedPoint == *wI || crossedPoint == *wNI)
                return true;
        }		// else out of range
    }
    return false;
}

bool VoronoiClusterDescent::_createTriangle2x2(WayOlav *way1, WayOlav *way2, WayOlav::iterator &w1Inside, WayOlav::iterator &w1Alt, WayOlav::iterator &way2I, WayOlav::iterator &way2NI)
{
    if (_isInsideTriangle(*w1Alt, *way2I, *way2NI, *w1Inside)) {
        double dist1 = seboehDist(*w1Alt, *way2I, *w1Inside);
        double dist2 = seboehDist(*w1Alt, *way2NI, *w1Inside);
        double dist3 = seboehDist(*way2I, *way2NI, *w1Inside);
        TSP::Point point = *w1Inside;
        /// HINT: This method is only called when way1 and way2 have both size = 2.
        /// So we can use erase and push_back, instead of erase and insert, with no problem.
        // Examples are test33, test36
        way1->erase(w1Inside);
        // TODO [seboeh] differ with sign between way1->erase or way2->erase.
        way2->erase(w1Inside);
        WayOlav::iterator w1NewI = way1->push_back(&way2I);
        WayOlav::iterator w1NNewI = way1->push_back(&way2NI);

        Node *nodePoint = (&w1Inside);
        if (dist1 < dist2 && dist1 < dist3)
        {
            nodePoint->prev = &w1Alt;
            nodePoint->next = (&w1Alt)->next;
            way1->insertChild(w1Alt, nodePoint);      // TODO [seboeh] is new really necessary?
        } else if (dist2 < dist1 && dist2 < dist3)
        {
            nodePoint->prev = &w1NNewI;
            nodePoint->next = (&w1NNewI)->next;
            way1->insertChild(w1NNewI, nodePoint);
        } else {
            nodePoint->prev = &w1NewI;
            nodePoint->next = (&w1NewI)->next;
            way1->insertChild(w1NewI, nodePoint);
        }
        return true;
    }
    return false;
}

void VoronoiClusterDescent::_createTriangle3x1(WayOlav *way1, WayOlav::iterator &insideI, WayOlav::iterator &fixI)
{
    TSP::Point point = *insideI;
    /// By iter = ... AND no push_back, because test49.
    auto iter = way1->erase(insideI);
    /// HINT: only on convex-hull the List.m_size have to be increased, because every node was one time a convex-hull node.
    way1->insert(iter, &fixI);
    WayOlav::iterator w1I = way1->begin();
    WayOlav::iterator w1NI = w1I; ++w1NI;
    WayOlav::iterator w1NNI = w1NI; ++w1NNI;
    double dist1 = seboehDist(*w1I, *w1NI, point);
    double dist2 = seboehDist(*w1NI, *w1NNI, point);
    double dist3 = seboehDist(*w1I, *w1NNI, point);
    Node *nodePoint = (&insideI);
    if (dist1 < dist2 && dist1 < dist3)
    {
        nodePoint->prev = &w1I;
        nodePoint->next = (&w1I)->next;
        way1->insertChild(w1I, nodePoint);
    } else if (dist2 < dist1 && dist2 < dist3)
    {
        nodePoint->prev = &w1NI;
        nodePoint->next = (&w1NI)->next;
        way1->insertChild(w1NI, nodePoint);
    } else {
        nodePoint->prev = &w1NNI;
        nodePoint->next = (&w1NNI)->next;
        way1->insertChild(w1NNI, nodePoint);
    }
}

void VoronoiClusterDescent::_mergeHull(WayOlav * &way1, const TSP::Point &pWay1Center, WayOlav * &way2, const TSP::Point &pWay2Center, const MergeCallState &callState, const TSP::Point &pCenter)
{
    // last inserted are new assigned in insert/push_back/insertChild - used in sortRob().
    if (way1->size() == 1 && way2->size() == 1)
    {
        way1->push_back(&(way2->begin()));
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 2 || way2->size() == 2))
    {
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        way1->push_back(&(way2->begin()));
        _flipWay(way1, pCenter);
        return;
    }
    if ((way1->size() == 1 || way2->size() == 1)
        && (way1->size() == 3 || way2->size() == 3))
    {
        // swap way1 with way2 to way1 be the bigger one.
        if (way1->size() == 1)
        {
            std::swap(way1, way2);
        }
        // handle 3 to 1
        WayOlav::iterator w1I = way1->begin();
        WayOlav::iterator fixI = way2->begin();
        if (_isCovered(way1, w1I, fixI)) {
            // raute or trapetz
            ++w1I; ++w1I;
            way1->insert(w1I, &fixI);
            _flipWay(way1, pCenter);
            return;
        } else {
            ++w1I;
            if (_isCovered(way1, w1I, fixI)) {
                ++w1I; ++w1I;
                way1->insert(w1I, &fixI);
                _flipWay(way1, pCenter);
                return;
            } else {
                ++w1I;
                if (_isCovered(way1, w1I, fixI)) {
                    ++w1I; ++w1I;
                    way1->insert(w1I, &fixI);
                    _flipWay(way1, pCenter);
                    return;
                } else {
                    // triangle with one point inside
                    w1I = way1->begin();
                    WayOlav::iterator w1NI = w1I; ++w1NI;
                    WayOlav::iterator w1NNI = w1NI; ++w1NNI;
                    if (_isInsideTriangle(*fixI, *w1I, *w1NI, *w1NNI))
                    {
                        _createTriangle3x1(way1, w1NNI, fixI);
                    } else if (_isInsideTriangle(*fixI, *w1NNI, *w1I, *w1NI))
                    {
                        _createTriangle3x1(way1, w1NI, fixI);
                    } else {
                        _createTriangle3x1(way1, w1I, fixI);
                    }
                    _flipWay(way1, pCenter);
                    return;
                }
            }
        }
    }           // end 3x1
    /// HINT: 1xn is handled by the general part.
    if (way1->size() == 2 && way2->size() == 2)
    {
        WayOlav::iterator way1I = way1->begin();
        WayOlav::iterator way2I = way2->begin();
        WayOlav::iterator way1NI = way1I;
        ++way1NI;
        WayOlav::iterator way2NI = way2I;
        ++way2NI;
        WayOlav::iterator way1Nearest;
        WayOlav::iterator way2Nearest;
        double maxDist=std::numeric_limits<double>::max();
        double dist = m_para.distanceFnc(*way1I, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1I, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1I; way2Nearest = way2NI;
        }
        dist = m_para.distanceFnc(*way1NI, *way2I);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2I;
        }
        dist = m_para.distanceFnc(*way1NI, *way2NI);
        if (dist < maxDist) {
            maxDist = dist;
            way1Nearest = way1NI; way2Nearest = way2NI;
        }
        WayOlav::iterator way1Next = way1Nearest; ++way1Next;
        WayOlav::iterator way2Next = way2Nearest; ++way2Next;
        if (_isCrossed(*way1Nearest, *way2Nearest, *way1Next, *way2Next) == CrossedState::isCrossed) {
            // it is a hash / raute
            auto iter = way1->insert(way1Next, &way2Nearest);
            way1->insert(iter, &way2Next);
            _flipWay(way1, pCenter);
            return;
        } else {
            // it is a trapetz or triangle. To detect triangle check if one point is inside a triangle.
            // HINT: We could alternativly check cos-similarity, but this would not help for the question, which node is inside the triangle.
            if (_createTriangle2x2(way1, way2, way1I, way1NI, way2I, way2NI))
            {
            } else  if (_createTriangle2x2(way1, way2, way1NI, way1I, way2I, way2NI ))
            {
            } else if (_createTriangle2x2(way2, way1, way2I, way2NI, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else if (_createTriangle2x2(way2, way1, way2NI, way2I, way1I, way1NI)) {
                std::swap(way1, way2);     // you have to switch way1 and way2, because way2 will be deleted outside.
            } else {
                // else it is a trapetz
                auto iter = way1->insert(way1Nearest, &way2Nearest);
                way1->insert(iter, &way2Next);
            }
            _flipWay(way1, pCenter);
            return;
        }
    }       // end way1.size == 2 && way2.size == 2

    /// beginning at start of polyon A and polyon B - iterate first over A, then over B
    /// HINT: iterate only over convex hull
    double smallestDist = std::numeric_limits<double>::max();
    std::list<EdgeIter> directAccessEdges;
    for (WayOlav::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        for (WayOlav::iterator wBI=way2->begin();
             !wBI.end(); ++wBI)
        {
            /// 1.1 check if node a1 in polyon A has direct access to node b1 in polygon B
            /// HINT: check the line from next/previous node to te middle of A and B
            /// HINT: one crossing of those both lines is enough to indicate a coverage (no direct access)
            if (_isDirectAccess(pWay1Center, wAI, pWay2Center, wBI) == DirectAccessState::isDirectAccess)
            {
                directAccessEdges.push_back(EdgeIter(wAI, wBI));
                double dist = m_para.distanceFnc(*wAI, *wBI);
                if (dist < smallestDist)
                    smallestDist = dist;
            }
        }
    }

    /// find highest point in directAccessEdges as HPAI and HPBI, find also lowest point as LPAI and LPBI
    WayOlav::iterator haI, hbI, laI, lbI;

    Point realCenter = (pWay1Center + pWay2Center)/2.0;

    smallestDist /= 2;
    Point lastLowCrossPoint;
    Point lastHighCrossPoint;
    double highCrossPoint = 0;
    if (callState == MergeCallState::leftRight)
        highCrossPoint = std::numeric_limits<double>::max();     // not 0, because top left null-point.
    double lowCrossPoint = 0;
    if (callState == MergeCallState::topDown)
        lowCrossPoint = std::numeric_limits<double>::max();
    /// HINT: we expect no negative coordinates on the cities.
    for (auto edgeI=directAccessEdges.begin();
         edgeI != directAccessEdges.end(); ++edgeI)
    {
        TSP::Point pa = *(edgeI->aNodeI);
        TSP::Point pb = *(edgeI->bNodeI);

        Point crossedPoint;
        CrossedState state;
        if (callState == MergeCallState::topDown)
            state = _isCrossed(pa, pb, TSP::Point(std::min(pa.x(), pb.x())-10, pCenter.y()), TSP::Point((std::max)(pa.x(), pb.x())+10, pCenter.y()), &crossedPoint);
        else
            state = _isCrossed(pa, pb, TSP::Point(pCenter.x(),std::min(pa.y(), pb.y())-10), TSP::Point(pCenter.x(),(std::max)(pa.y(), pb.y())+10), &crossedPoint);

        bool isLowNotHigh = _isNormalVectorLeft(pb, pa, realCenter);

        double value;
        if (state != CrossedState::isOnPoint)
        {
            if (callState == MergeCallState::topDown)
                value = crossedPoint.x();
            else
                value = crossedPoint.y();
        }
        else
        {       // see test82, test76
            // see test89, test64
            Point otherP;
            if (pa == crossedPoint)
                otherP = pb;
            else
                otherP = pa;
            double otherVal;
            if (callState == MergeCallState::topDown)
                otherVal = otherP.x();
            else
                otherVal = otherP.y();


            // HINT: center of pa_pb is not working, because test76 cascading of points.
            Point descent = otherP - crossedPoint;
            descent /= m_para.distanceFnc(otherP, crossedPoint);

            if (callState == MergeCallState::topDown)
            {
                value = crossedPoint.x() + descent.x()*smallestDist;
            }
            else
            {
                value = crossedPoint.y() + descent.y()*smallestDist;
            }
        }

        bool isGood = false;
        if (isLowNotHigh)
        {
            if ((callState == MergeCallState::topDown
                    && value <= lowCrossPoint)      // =, because of test76
                || (callState == MergeCallState::leftRight
                    && value >= lowCrossPoint))     // =, because of test76
            {
                lowCrossPoint = value;
                isGood = true;
            }
            if (lastLowCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastLowCrossPoint = Point();
                }
            }
        }
        else
        {
            // <= and not >=, because left top null-point.
            if ((callState == MergeCallState::topDown
                    && value >= highCrossPoint)
                || (callState == MergeCallState::leftRight
                    && value <= highCrossPoint))
            {
                highCrossPoint = value;
                isGood = true;
            }
            if (lastHighCrossPoint != Point())
            {
                if (state != CrossedState::isOnPoint)
                {
                    lowCrossPoint = value;
                    isGood = true;
                    lastHighCrossPoint = Point();
                }
            }
        }

        if (((state == CrossedState::isCrossed
                    || state == CrossedState::isOnPoint     // because, teset82
              )
                && isGood)          // TODO [seboeh] drop isGood here
           )
        {
            if (isLowNotHigh)
            {
                laI = edgeI->aNodeI;
                lbI = edgeI->bNodeI;
            }
            else
            {
                haI = edgeI->aNodeI;
                hbI = edgeI->bNodeI;
            }
        }
    }

    // due to test88, find high triangular points
    _triangleRoofCorrection(directAccessEdges, haI, laI, hbI, lbI);
    // left-right instead of upside-down
    _triangleRoofCorrection(directAccessEdges, hbI, lbI, haI, laI);

    /// correct the hull in way1
    /// connect polygon A and B. Redirect
    (&haI)->next = &hbI;
    assert(!(&haI)->isChildNode);
    (&haI)->child = nullptr;        // TODO [seboeh] free memory here?
    (&hbI)->prev = &haI;

    (&lbI)->next = &laI;
    assert(!(&lbI)->isChildNode);
    (&lbI)->child = nullptr;
    (&laI)->prev = &lbI;

    way1->setNewStart(&haI);
    way1->increaseSize(way2->size());      // avoid that the way1 stay on low size - prevent using of standard merge in _mergeHull().

    _flipWay(way1, pCenter);  // necessary, because code before does not keep the clock-wise order.
}

double VoronoiClusterDescent::_calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = m_para.distanceFnc(p1, p2);
    double b = m_para.distanceFnc(p2, p3);
    double c = m_para.distanceFnc(p3, p1);
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double VoronoiClusterDescent::_area(const PointL &polygon)
{
    double area = 0;
    for (auto pointI = polygon.begin();
         pointI != polygon.end(); ++pointI)
    {
        auto pointNextI = pointI; ++pointNextI;
        if (pointNextI == polygon.end()) pointNextI = polygon.begin();
        area += pointI->x() * pointNextI->y();
        area -= pointI->y() * pointNextI->x();
    }
    return abs(area);
}

double VoronoiClusterDescent::_triangleAltitudeOnC(Point A, Point B, Point C)
{
    double a = m_para.distanceFnc(A, B);
    double b = m_para.distanceFnc(B, C);
    double c = m_para.distanceFnc(C, A);
    double s = (a + b + c)/2.0;
    return 2.0*sqrt(s*(s-a)*(s-b)*(s-c)) / c;
}

bool VoronoiClusterDescent::_isRightSide(const Point &p1, const Point &p2)
{
    /// HINT: because the coordinate system is with origin on left-top, we have to switch the y sign.
    /// Therefore we get from scalar product : ax*-by + ay*bx  -> ax*by + -ay*bx
    /// [http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another]
    double dot = ((p1.x()*p2.y()) + ((-1*p1.y())*p2.x()));
    return dot >= 0;
}

double VoronoiClusterDescent::_calcDegree(Node *node, Node *nodeTested)
{
    Point pointVect = nodeTested->point - node->point;

    double degree = (pointVect.x() + pointVect.y())
            / (m_para.distanceFnc(Point(0,0), pointVect)
               * m_para.distanceFnc(Point(0,0), Point(1,1)));

    double degreeArc = acos(degree);
    // clock-wise
    if (_isRightSide(Point(1,1), pointVect))
        return  degreeArc;
    else
        return  2*acos(-1) - degreeArc;
}

VoronoiClusterDescent::ClusterM VoronoiClusterDescent::determineClusters(NodeL &input, NodeL &hull)
{
    ClusterM clusters;
    double coordMax = max(tspWidth, tspHeight) * 12;
    //  TODO [seboeh] * 12 - still a problem, because the intersection below can be far away.

    /// * walk through all inputs except hull-points
    for (const auto &node : input)
    {
        Cluster clu;
        clu.point = node;
        auto fI = std::find_if(hull.begin(), hull.end(),
                               [node](const shared_ptr<Node> &val)
        {
            return val->point == node->point;
        });
        if (fI != hull.end())
            continue;

        /// * find closest three points P_1 to input x
        map<double, shared_ptr<Node>> closestPoints;
        for (const auto &node2 : input)
        {
            if (node == node2)
                continue;
            closestPoints.insert({m_para.distanceFnc(node->point, node2->point), node2});
        }
        if (closestPoints.size() < 3)
            return clusters;
        for (const auto &closE : closestPoints)
        {
            clu.closestN.push_back(closE.second);
        }

        /// * check if x is inside P_1,
        auto closestP1I = closestPoints.begin();
        auto closestP2I = closestP1I; ++closestP2I;
        auto closestP3I = closestP2I; ++closestP3I;
        list<Node*> closestTestedP;
        closestTestedP.push_back(closestP1I->second.get());
        closestTestedP.push_back(closestP2I->second.get());
        closestTestedP.push_back(closestP3I->second.get());
        if ( ! _isInsideTriangle(closestP1I->second->point, closestP2I->second->point, closestP3I->second->point, node->point))
        {
            ///     * if yes, continue
            ///     * else determine next closest p_2
            ///     * check in two of P_1 and p_2, if x is inside this triangle
            ///         * if yes, continue
            ///         * else, repeat until end of inputs. -> unexpected-error, if not found
            auto closestPnextI = ++closestP3I;
            while (closestPnextI != closestPoints.end())
            {
                // 2 over n
                // TODO [seb] hint: keep closest fixed
                bool stopWhile = false;
                for (auto closestTestedP1I = closestTestedP.begin();
                     closestTestedP1I != closestTestedP.end(); ++closestTestedP1I)
                {
                    for (auto closestTestedP2I = closestTestedP.begin();
                         closestTestedP2I != closestTestedP.end()
                         && closestTestedP1I != closestTestedP2I; ++closestTestedP2I)
                    {
                        if (_isInsideTriangle((*closestTestedP1I)->point, (*closestTestedP2I)->point, closestPnextI->second->point, node->point))
                        {
                            closestTestedP.push_back(closestPnextI->second.get());
                            stopWhile = true;
                            break;
                        }
                    }
                    if (stopWhile) break;
                }
                if (stopWhile) break;
                closestTestedP.push_back(closestPnextI->second.get());
                ++closestPnextI;
            }
            if (closestPnextI == closestPoints.end())
                return clusters;
        }

        /// * sort closestTestedP, according to degree, because there is no order than circular around the point
        // cross product a*d - b*c, with point = (a,b) and (c,d) = (1,1)
        map<double, Node*> closestTestedPointsSorted;
        for (const auto &nodeTested : closestTestedP)
        {
            closestTestedPointsSorted.insert({_calcDegree(node.get(), nodeTested), const_cast<Node*>(nodeTested)});
        }

//        clu.closestN.clear();
//        clu.closestN.reserve(closestTestedPointsSorted.size());
//        for (const auto &point : closestTestedPointsSorted)
//        {
//            for (auto &closestE : closestPoints)
//            {
//                if (point.second == closestE.second->point)
//                {
//                    clu.closestN.push_back(closestE.second);
//                    break;
//                }
//            }
//        }

        /// * for all P (P_1,P_2,...):
        ///
        /// * enlarge points to get the real cluster
        clu.verts.clear();
        auto closestPI = closestPoints.begin();
        // because start of next not already checked points
        for (size_t i=0; i < closestTestedP.size(); ++i)
            if (closestPI != closestPoints.end())
                ++closestPI;
            else break;
        while (closestPI != closestPoints.end())
        {
            bool isChanged = false;
            for (auto cluPointOutI = closestTestedPointsSorted.begin();
             cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
            {
                auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
                if (cluPointOut2I == closestTestedPointsSorted.end())
                    cluPointOut2I = closestTestedPointsSorted.begin();
                ///     * calculate mid of x to p_i =e_1 and mid of x to p_{i+1} =e_2
                Point midE1 = (cluPointOutI->second->point + node->point)/2.0;
                Point midE2 = (cluPointOut2I->second->point + node->point)/2.0;
                ///     * calculate the orthogonals to e_1 and e_2
                Point vec1 = (cluPointOutI->second->point - node->point) / m_para.distanceFnc(cluPointOutI->second->point, node->point);
                Point midE1b = midE1 + Point(-vec1.y(), vec1.x()) * coordMax;           // rotate 90degree
                Point vec2 = (cluPointOut2I->second->point - node->point) / m_para.distanceFnc(cluPointOut2I->second->point, node->point);
                Point midE2b = midE2 + Point(vec2.y(), -vec2.x()) * coordMax;
                ///     * calculate the intersection of orthogonals
                Point crossMid;
                auto intersectRes = _isCrossed(midE1, midE1b, midE2, midE2b, &crossMid);
                if (intersectRes == CrossedState::isCrossed
                        || intersectRes == CrossedState::isOnPoint)
                {

//                    /// check on point inside triangle between midE1-midE2-2h*crossMid
//                    //double height = _triangleAltitudeOnC(midE1, midE2, crossMid);
//                    Point base = (midE2 + midE1)/2.0;
//                    Point C = base + (crossMid - base)*2.0;
//                    Point A = midE1;
//                    Point B = midE2;
//                    if (_isInsideTriangle(A, B, C, closestPI->second->point))
//                    {

                    /// check if there exist an intersection closer than crossMid
                    Point midE3 = (closestPI->second->point + node->point)/2.0;
                    Point vec3 = (closestPI->second->point - node->point) / m_para.distanceFnc(closestPI->second->point, node->point);
                    Point midE3b = midE3 + Point(vec3.y(), -vec3.x()) * coordMax;
                    Point crossMid3;
                    auto intersectRes = _isCrossed(midE1, midE1b, midE3, midE3b, &crossMid3);
                    if (intersectRes == CrossedState::isCrossed
                            || intersectRes == CrossedState::isOnPoint)
                    {
                        double distMid1 = m_para.distanceFnc(node->point, crossMid);
                        double distMid2 = m_para.distanceFnc(node->point, crossMid3);
                        if (distMid2 < distMid1)
                        {       // insert
                            closestTestedPointsSorted.insert({_calcDegree(node.get(), closestPI->second.get()), closestPI->second.get()});
                            isChanged = true;
                            break;
                        }
                    }
                    midE3b = midE3 + Point(-vec3.y(), vec3.x()) * coordMax;
                    intersectRes = _isCrossed(midE2, midE2b, midE3, midE3b, &crossMid3);
                    if (intersectRes == CrossedState::isCrossed
                            || intersectRes == CrossedState::isOnPoint)
                    {
                        double distMid1 = m_para.distanceFnc(node->point, crossMid);
                        double distMid2 = m_para.distanceFnc(node->point, crossMid3);
                        if (distMid2 < distMid1)
                        {       // insert
                            closestTestedPointsSorted.insert({_calcDegree(node.get(), closestPI->second.get()), closestPI->second.get()});
                            isChanged = true;
                            break;
                        }
                    }
                }
            }       // end closestTestedPointsSorted

            /// keep on running with next closest point
            if (!isChanged)
                break;      // because, the other points are too far away
            // iterate
            ++closestPI;
        }

        ///     * save this intersection as point into Cluster.verts
        for (auto cluPointOutI = closestTestedPointsSorted.begin();
         cluPointOutI != closestTestedPointsSorted.end(); ++cluPointOutI)
        {
            auto cluPointOut2I = cluPointOutI; ++cluPointOut2I;
            if (cluPointOut2I == closestTestedPointsSorted.end())
                cluPointOut2I = closestTestedPointsSorted.begin();
            ///     * calculate mid of x to p_i =e_1 and mid of x to p_{i+1} =e_2
            Point midE1 = (cluPointOutI->second->point + node->point)/2.0;
            Point midE2 = (cluPointOut2I->second->point + node->point)/2.0;
            ///     * calculate the orthogonals to e_1 and e_2
            Point vec1 = (cluPointOutI->second->point - node->point) / m_para.distanceFnc(cluPointOutI->second->point, node->point);
            Point midE1b = midE1 + Point(-vec1.y(), vec1.x()) * coordMax;           // rotate 90degree
            Point vec2 = (cluPointOut2I->second->point - node->point) / m_para.distanceFnc(cluPointOut2I->second->point, node->point);
            Point midE2b = midE2 + Point(vec2.y(), -vec2.x()) * coordMax;
            ///     * calculate the intersection of orthogonals
            Point crossMid;
            auto intersectRes = _isCrossed(midE1, midE1b, midE2, midE2b, &crossMid);
            if (intersectRes == CrossedState::isCrossed
                    || intersectRes == CrossedState::isOnPoint)
            {
                clu.verts.push_back(crossMid);
            }
        }
        /// * calculate area of cluster
        clu.area = _area(clu.verts);
        for (auto &el : closestTestedPointsSorted)
            clu.closestTestedPointsSorted.push_back(el.second);

        /// repeat from beginning, until all inputs are handled.
        clusters.push_back(clu);
    }
    return clusters;
}

VoronoiClusterDescent::Node* VoronoiClusterDescent::_nextNode(Node *curr, Node * &prev)
{
    if (curr->edge1 == prev)
    {
        prev = curr;
        return curr->edge2;
    }
    else
    {
        prev = curr;
        return curr->edge1;
    }
}

VoronoiClusterDescent::Cluster* VoronoiClusterDescent::_getPivotCluster(ClusterPL &clusters)
{
    Point center(0,0);
    for (Cluster * &clu : clusters)
    {
         center += clu->point->point;
    }
    center /= clusters.size();

    /// * get closest cluster to center
    Cluster *cluClose;
    double distBest = numeric_limits<double>::max();
    for (Cluster * &clu : clusters)
    {
        double dist = m_para.distanceFnc(clu->point->point, center);
        if (dist < distBest)
        {
            distBest = dist;
            cluClose = clu;
        }
    }

    return cluClose;
}

std::list<Node*> VoronoiClusterDescent::_removePointsFromPoly(
        shared_ptr<Poly> way
        , Cluster *cluPivot, Cluster *cluCloseNeighPrev, Cluster *cluCloseNeigh)
{
    list<Node*> openPoints;
    for (Poly::iterator valI=way->begin();
         !valI.end(); )
    {
        Node *node = &valI;
        if (node->point == cluPivot->point->point
                || (cluCloseNeighPrev != nullptr && node->point == cluCloseNeighPrev->point->point)
                || (cluCloseNeigh != nullptr && node->point == cluCloseNeigh->point->point))
        {
            /// * erase node from way, if neighbour or pivot cluster
            openPoints.push_back(&valI);
            valI = way->erase(valI);
        }
        else
        {
            ++valI;
        }
    }
    return openPoints;
}

void VoronoiClusterDescent::_removeNode(std::list<Node*> &container, const Node *val)
{
    auto iter = find_if(container.begin(), container.end(),
                           [val] (Node *&val2)
    {
        return val->point == val2->point;
    });
    if (iter != container.end())
        container.erase(iter);
}

void VoronoiClusterDescent::_insertIfNotExist(std::list<Node*> &container, Node *val)
{
    auto iter = find_if(container.begin(), container.end(),
                         [val] (Node *&val2)
    {
        return val->point == val2->point;
    });
    if (iter == container.end())
        container.push_back(val);
}

std::list<Node*> VoronoiClusterDescent::_removeCommonPoints(
        shared_ptr<Poly> way1, shared_ptr<Poly> way2)
{
    list<Node*> openPoints;
    Poly::iterator val2I = way2->begin();
    for (Poly::iterator val1I=way1->begin();
         !val1I.end(); )
    {
        // HINT: have to loop every time again through way2,
        // because the direction of way1 and way2 can change while looking for common points.
        // Improvement: reset() instead of = ...begin()
        bool isFound = false;
        for (val2I.reset(val2I); !val2I.end(); --val2I)
        {
            if ((&val1I)->point == (&val2I)->point)
            {
                openPoints.push_back(&val1I);
                ++val1I; //val1I = way1->erase(val1I);, because only remove from second way
                val2I = way2->erase(val2I);
                --val2I;
                isFound = true;
                break;
            }
        }
        if (!isFound)
        {
            ++val1I;
        }
    }

    return openPoints;
}


double VoronoiClusterDescent::_tradeof(const Point &pEdgeA1, const Point &pEdgeA2, const Point &pEdgeB1, const Point &pEdgeB2)
{
    return m_para.distanceFnc(pEdgeA1, pEdgeB2)
            + m_para.distanceFnc(pEdgeA2, pEdgeB1)
            - m_para.distanceFnc(pEdgeA1, pEdgeA2)
            - m_para.distanceFnc(pEdgeB1, pEdgeB2);
}

std::tuple<double, Node*, Node*, Node*, Node*> VoronoiClusterDescent::_bestTradeof(
        list<Node*> way1Froms, list<Node*> way1Tos
        , list<Node*> way2Froms, list<Node*> way2Tos)
{
    assert(!way1Froms.empty());
    assert(!way1Tos.empty());
    assert(!way2Froms.empty());
    assert(!way2Tos.empty());
    double bestDist = numeric_limits<double>::max();
    Node *bestWay1P = nullptr, *bestWay1N, *bestWay2P, *bestWay2N;
    auto way1ToI = way1Tos.begin();
    for (Node *&way1From : way1Froms)
    {
        auto way2ToI = way2Tos.begin();
        for (Node *&way2From : way2Froms)
        {
            double dist = _tradeof(way1From->point, (*way1ToI)->point, way2From->point, (*way2ToI)->point);
            if (dist < bestDist)
            {
                bestDist = dist;
                bestWay1P = way1From;
                bestWay1N = *way1ToI;
                bestWay2P = way2From;
                bestWay2N = *way2ToI;
            }
            ++way2ToI;
            if (way2ToI == way2Tos.end())
                break;
        }
        ++way1ToI;
        if (way1ToI == way1Tos.end())
            break;
    }
    assert(bestWay1P != nullptr);

    return make_tuple(bestDist, bestWay1P, bestWay2N, bestWay1N, bestWay2P);
}

bool VoronoiClusterDescent::_isCrossedInWay(std::shared_ptr<Poly> way1, Node *way1P, Node *way2N)
{
    Node *prevW1 = &(--way1->begin());
    for (Poly::iterator wAI=way1->begin();
         !wAI.end(); ++wAI)         // iterate over convex hull
    {
        if (_isCrossed(way1P->point, way2N->point, prevW1->point, *wAI) == CrossedState::isCrossed)
        {
            return true;
        }
        prevW1 = &wAI;
    }
    return false;
}

std::tuple<Node*,Node*, Node*,Node*> VoronoiClusterDescent::_findBestTradeof(
        std::shared_ptr<Poly> way1, std::shared_ptr<Poly> way2)
{
    Node *bestWay1P = nullptr, *bestWay1N, *bestWay2P, *bestWay2N;
    double bestDist = numeric_limits<double>::max();
    Poly::iterator from1I; from1I.reset(--way1->begin());
    for (auto to1I = way1->begin();
         !to1I.end(); ++to1I)
    {
        Poly::iterator from2I; from2I.reset(--way2->begin());
        for (auto to2I = way2->begin();
             !to2I.end(); ++to2I)
        {
            double dist = _tradeof((&from1I)->point, (&to1I)->point, (&from2I)->point, (&to2I)->point);
            if (dist < bestDist)
            {
                // here check on cross, because tradeof calc is faster than following code
                Node *way1P = &from1I;
                Node *way1N = &to1I;
                Node *way2P = &from2I;
                Node *way2N = &to2I;
                if (_isCrossed(way1P->point, way2N->point, way1N->point, way2P->point)
                        == CrossedState::isCrossed)
                {
                    ++from2I;
                    continue;       // there have to be a unwanted crossing
                }

                if (!_isCrossedInWay(way1, way1P, way2N)
                        && !_isCrossedInWay(way1, way1N, way2P)
                        && !_isCrossedInWay(way2, way1P, way2N)
                        && !_isCrossedInWay(way2, way1N, way2P))
                {
                    bestDist = dist;
                    bestWay1P = way1P;
                    bestWay1N = way1N;
                    bestWay2P = way2P;
                    bestWay2N = way2N;
                }
            }
            // iterate
            ++from2I;
        }
        // iterate
        ++from1I;
    }
    assert(bestWay1P != nullptr);

    return make_tuple(bestWay1P, bestWay1N, bestWay2P, bestWay2N);
}

std::shared_ptr<VoronoiClusterDescent::Poly> VoronoiClusterDescent::_merge(
        shared_ptr<Poly> wayPrev, shared_ptr<Poly> way, Cluster *cluPivot)
{
    /// There are three 'sides': two clusters, which should be merged and one pivotCenter
    /// According to 1D-Sort, it is necessary to link the closest to pivot first.
    /// Then, the second closest can link to pivot or to cluster.
    /// This depends on the distance to cluster or pivot. The closer the better.
    /// So the second cluster is aligned to pivot if it is closer to pivot than to cluster.
    /// Or it is aligned to cluster, if it is closer to cluster than pivot.
    /// HINT: TODO: in 2D it is not clear now, what this mean.

    auto wayI = wayPrev->begin();
    for (; !wayI.end(); ++wayI) if (*wayI == cluPivot->point->point) break;
    if (wayI.end())
    {
        _insertNewNodeToPolyOnBestAlign(wayPrev, cluPivot->point.get());
    }

    /// * Then, all common points from the second cluster are removed.
    /// * find common points and remove them
    _removeCommonPoints(wayPrev, way);

    /// * Then, the best tradeof between the two ways is determined.
    if (!way->empty() && !wayPrev->empty())
    {
        Node *outerFromWay1, *outerToWay1, *outerFromWay2, *outerToWay2;
        tie(outerFromWay1, outerToWay1, outerFromWay2, outerToWay2)
                = _findBestTradeof(wayPrev, way);

        /// * After that the ways are connected according to the tradeof and the result should be correct.
        // link best trade together
        outerFromWay1->next = outerToWay2;
        outerToWay2->prev = outerFromWay1;
        outerFromWay2->next = outerToWay1;
        outerToWay1->prev = outerFromWay2;
    }
    else
    {
        if (wayPrev->empty())
            wayPrev = way;
    }

    return wayPrev;
}

std::shared_ptr<VoronoiClusterDescent::Poly> VoronoiClusterDescent::_devideConquer(ClusterPL openClusters)
{
    /// * calculate center and closest cluster
    Cluster *cluPivot = _getPivotCluster(openClusters);
    Point pivot = cluPivot->point->point;

    if (openClusters.size() > 1)
    {
        /// * start to devide with this cluster
        shared_ptr<Poly> way(nullptr, no_op_delete());
        shared_ptr<Poly> wayPrev(nullptr, no_op_delete());

        // prepare for ordered align according to closest first
        map<double, Yet> yets;
        std::list<Cluster*>::iterator cluCloseNeighI = cluPivot->closestSortedNeighbours.begin();
        list<Point>::iterator cluYetPointNextI = cluPivot->verts.begin();
        for (list<Point>::iterator cluYetPointI = cluPivot->verts.begin();
             cluYetPointI != cluPivot->verts.end(); ++cluYetPointI)
        {
            ++cluYetPointNextI;
            if (cluYetPointNextI == cluPivot->verts.end())
                cluYetPointNextI = cluPivot->verts.begin();
            ++cluCloseNeighI;        // start at second el
            if (cluCloseNeighI == cluPivot->closestSortedNeighbours.end())
                cluCloseNeighI = cluPivot->closestSortedNeighbours.begin();

            Point yetFarPoint = *cluYetPointI
                    + ((*cluYetPointI - pivot)/m_para.distanceFnc(*cluYetPointI, pivot)
                        * Point(tspWidth, tspHeight));

            Point yetFarPointNext = *cluYetPointNextI
                    + ((*cluYetPointNextI - pivot)/m_para.distanceFnc(*cluYetPointNextI, pivot)
                        * Point(tspWidth, tspHeight));

            Yet y;
            y.clu = *cluCloseNeighI;
            y.yetPrev = yetFarPoint;
            y.yetNext = yetFarPointNext;
            double dist = m_para.distanceFnc(y.clu->point->point, pivot);
            yets.insert({dist, y});
        }

        Cluster* cluCloseNeighPrev = nullptr;
        for (const auto &yetEl : yets)
        {
            const Yet yet = yetEl.second;

            // preconditino for devideAndConquer
            if (wayPrev != nullptr)
            {       // only not already merged points are important
                bool isFound = false;
                for (auto wayI = wayPrev->begin(); !wayI.end(); ++wayI) { if (*wayI == yet.clu->point->point) { isFound = true; break; } }
                if (isFound)
                {
                    continue;
                }
            }

            /// * separate for DEVIDING
            ClusterPL newClusters;
            for (auto cluI = openClusters.begin();
                 cluI != openClusters.end(); ++cluI)
            {
                if (*cluI == cluPivot)
                    continue;
                if (_isInsideTriangle(pivot, yet.yetPrev, yet.yetNext, (*cluI)->point->point))
                {
                    newClusters.push_back(*cluI);
                    cluI = openClusters.erase(cluI);
                    --cluI;
                }
            }

            /// * align simple points, if no cluster between the yets.
            if (newClusters.empty())
            {
                // if not yet in wayPrev add the one hull-node from between yets.
                Point pToFind = yet.clu->point->point;
                if (wayPrev != nullptr)
                {
                    bool isFound = false;
                    for (auto wayI = wayPrev->begin(); !wayI.end(); ++wayI) { if (*wayI == pToFind) { isFound = true; break; } }
                    if (!isFound)
                        _insertNewNodeToPolyOnBestAlign(wayPrev, yet.clu->point.get());
                }
                else
                {
                    wayPrev = shared_ptr<Poly>(new Poly(), no_op_delete());
                    wayPrev->push_back(yet.clu->point.get());
                }
                // iterate
                continue;
            }

            /// * recursive call of next 'side'/cluster-section
            ///  In 1D this cluster-sections are related to the sides of one element (in 1D two sides, before and after point)
            way = _devideConquer(newClusters);

            /// * MERGE
            if (wayPrev != nullptr)
                way = _merge(wayPrev, way, cluPivot);

            // iterate
            wayPrev = way;
            cluCloseNeighPrev = yet.clu;
        }
        return way;
    }
    else
    {
        /// * merge - with one cluster
        shared_ptr<Poly> way(new Poly(), no_op_delete());
        for (Node *&node : cluPivot->closestTestedPointsSorted)
            way->push_back(new Node(node->point));
        _insertNewNodeToPolyOnBestAlign(way, new Node(pivot));
        return way;
    }
}

VoronoiClusterDescent::NodeL VoronoiClusterDescent::determineWay(ClusterM &clusters, NodeL &hull)
{
    if (!clusters.empty())
    {
        NodeL ret;

        /// * fill input with neighbours
        list<Node*> input;
        Node *prev = hull.back().get();
        NodeL::iterator nodeNextI = hull.begin();
        NeighPlane neighPl;
        for (NodeL::iterator nodeI = hull.begin();
              nodeI != hull.end(); ++nodeI)
        {
            ++nodeNextI;
            if (nodeNextI == hull.end())
                nodeNextI = hull.begin();
            neighPl.neigh = prev;
            (*nodeI)->neighbours.push_back(neighPl);
            neighPl.neigh = nodeNextI->get();
            (*nodeI)->neighbours.push_back(neighPl);
            prev = nodeI->get();
            input.push_back(nodeI->get());
        }
        for (Cluster &clu : clusters)
        {
            for (Node *&neighbour : clu.closestTestedPointsSorted)
            {
                if (neighbour->isHull)
                {
                    neighPl.neigh = clu.point.get();
                    neighbour->neighbours.push_back(neighPl);
                }
                neighPl.neigh = neighbour;
                clu.point->neighbours.push_back(neighPl);
            }
            input.push_back(clu.point.get());
        }

        /// * add proportional distribution factors
        for (Node *&node : input)
        {
            double distrTotal = 0;
            for (Node::NeighPlane &neighPlane : node->neighbours)
            {
                distrTotal += 1.0 / m_para.distanceFnc(node->point, neighPlane.neigh->point);
            }
            // without distrTotal --> no proportional
            distrTotal = 1.0;
            for (Node::NeighPlane &neighPlane : node->neighbours)
            {
                neighPlane.proportionalDistributionFact =
                        (1.0 / m_para.distanceFnc(node->point, neighPlane.neigh->point))
                        / distrTotal;
            }
        }

        /// * repeat n times
        for (size_t i=0; i < input.size(); ++i)
        {
            ///   * for each input
            for (Node *&node : input)
            {
                ///       * for each neighbour distribute its neighbours-weight to all other neighbours
                for (NeighPlane &neigh : node->neighbours)
                {
                    double neighWeight = neigh.neigh->weightOld;
                    for (NeighPlane &neighOther : node->neighbours)
                    {
                        if (neighOther.neigh == neigh.neigh)
                            continue;
                        neighOther.neigh->weightNew += neighOther.proportionalDistributionFact * neighWeight;
                    }
                    neigh.neigh->weightOld = neigh.neigh->weightNew;
                }
            }
        }

        /// * prepare structure for walk
        for (Node *&node : input)
        {
            for (NeighPlane &neigh : node->neighbours)
            {
                node->neighboursSorted.insert({neigh.neigh->weightNew, neigh});
            }
        }

        /// * start descent at highest weigth of input-node
        // walk
        Node *start = nullptr;
        double bestWeight = std::numeric_limits<double>::max();
        for (Node *&node : input)
        {
            if (node->weightNew < bestWeight)
            {
                bestWeight = node->weightNew;
                start = node;
            }
        }

        Node *current = start;
        size_t countVisitedPoints = 0;
        while (true)
        {
            ///   * insert next node to result
            ret.push_back(shared_ptr<Node>(current, no_op_delete()));
            ++countVisitedPoints;
            if (countVisitedPoints == input.size())
                break;

            ///   * go weight down by selecting node with highest weight lower than own.
            Node *nextBest = nullptr;
            double bestDist = std::numeric_limits<double>::max();
            for (auto &neighE : current->neighboursSorted)
            {
                NeighPlane &neighPl = neighE.second;

                // because FIR
                if (neighPl.neigh->isInserted == false
                    && neighPl.neigh != start
                    && countVisitedPoints == (input.size()-1))
                {
                    nextBest = neighPl.neigh;
                    bestDist = neighPl.neigh->weightNew;
                    break;
                }

                // default criteria
                if (neighPl.neigh->isInserted == false
                    && neighPl.neigh != start
                    && neighPl.neigh->weightNew >= current->weightNew)
                {
                    if (neighPl.neigh->weightNew < bestDist)
                    {
                        nextBest = neighPl.neigh;
                        bestDist = neighPl.neigh->weightNew;
                    }
                }

                /// * pair criteria
                if (neighPl.neigh->isInserted == false && neighPl.neigh != start)
                {
                    ///   * check if neighbour is detour duty
                    bool hasGoodNeighbour = false;
                    bool isOneNeighbourFree = false;
                    for (NeighPlane &neighNeighPl : neighPl.neigh->neighbours)
                    {
                        if (neighNeighPl.neigh == current)
                            continue;
                        if (!neighNeighPl.neigh->isInserted)
                            isOneNeighbourFree = true;
                        if (!neighNeighPl.neigh->isInserted
                            && neighNeighPl.neigh->weightNew <= neighPl.neigh->weightNew        // because there have to be a candidate for the neigh
                            )
                        {
                            hasGoodNeighbour = true;
                            break;
                        }
                    }
                    if (!hasGoodNeighbour && isOneNeighbourFree)
                    {       // it's a duty
                        nextBest = neighPl.neigh;
                        bestDist = neighPl.neigh->weightNew;
                        break;
                    }
                }

            }

            if (nextBest == nullptr)
                break;      // end reached, because we started with highest node

            current = nextBest;
            current->isInserted = true;     // not the first shall be set - because FIR and circle
        }

        // sort
//        map<double, Node*> weightSorted;
//        for (Node *&node : input)
//        {
//            weightSorted.insert({-node->weightNew, node});
//        }

//        Node *current = start;
//        while (true)
//        {
//            ///   * insert next node to result
//            ret.push_back(shared_ptr<Node>(current, no_op_delete()));
//            if (ret.size() == input.size())
//                break;      // end reached

//            ///   * go weight down by selecting node with highest weight lower than own.
//            map<double, Node*> smallestNeigh;
//            for (NeighPlane &neighPl : current->neighbours)
//            {
//                smallestNeigh.insert({-neighPl.neigh->weight, neighPl.neigh});
//            }

//            for (auto &smallNeighE: smallestNeigh)
//            {
//                Node *neighSmall = smallNeighE.second;
//                current = neighSmall;
//                // lookahead
//                double neighMax = 0;
//                for (NeighPlane &neighNeigh : neighSmall->neighbours)
//                {
//                    if (neighNeigh.neigh->weight > neighSmall->weight)
//                    {
//                        neighMax = neighNeigh.neigh->weight;
//                        break;
//                    }
//                }
//                if (neighMax == 0)
//                {
//                    break;
//                }
//            }
//        }

        return ret;
    }
    else
    {
        return hull;
    }
}

void VoronoiClusterDescent::_insertNewNodeToPolyOnBestAlign(std::shared_ptr<Poly> way, Node *insertNode)
{
    Poly::iterator bestI;
    double bestDist = std::numeric_limits<double>::max();
    Poly::iterator prevI; prevI.reset(--way->begin());
    for (Poly::iterator valI=way->begin();
         !valI.end(); ++valI, ++prevI)
    {
        double dist = seboehDist(&prevI, &valI, insertNode);
        if (dist < bestDist)
        {
            bestDist = dist;
            bestI = valI;
        }
    }

    way->insert(bestI, insertNode);
}

void VoronoiClusterDescent::_insertNewNodeToEdge(WayOlav* way, Node *fromNode, Node *insertNode)
{
    /// HINT: prev and next nodes of xN are in nodeList.
    /// TODO [xeboeh] delete childNode - memory leak here.
    // we detect cases, where it is no child node - selectNavel insert innerNodes size 1.
//    TODO .... delete if (cx.insertNode->isChildNode)
//        cx.insertNode->child->node = cx.insertNode;     // update old node --> delete node pointer.
    insertNode->child = nullptr;     // HINT: insertChild expect child == nullptr if new. Else we assume not new!
    insertNode->isChildNode = false;            // will be re-set on insertChild().
    insertNode->isInWay = true;

    /// 2. Insert xN;
    if (fromNode->child == nullptr || fromNode->isChildNode == false)     // isChildNode == false, for test61
    {       // convex-hull node
        way->insertChild(WayOlav::iterator(fromNode), insertNode);
    } else {
        way->insertChildInNext(fromNode, insertNode);
    }
    insertNode->isDeleted = false;
}

void VoronoiClusterDescent::buildTree(DT::DTNode::NodeType node, float level, TspAlgorithm::InputCitiesPointerType *openPointsBasic)
{
    Q_ASSERT(node != NULL);
    Q_ASSERT(openPointsBasic != NULL);
    if (openPointsBasic->empty()) {
        return;
    }

    m_startTime = std::chrono::system_clock::now();
    m_isCanceled = false;
    m_resultLength = INFINITY;
    float progressPercentage = 0.75 / openPointsBasic->size();

    incProgress(progressPercentage);

    // * intern Input representation
    auto input = createInputRepresentation(openPointsBasic);
    tspSize = input.size();

    // * determine hull
    auto res = determineHull(input);
    auto hull = get<0>(res);

    // * determine Voronoi cluster
    auto clusters = determineClusters(input, hull);

    // * build final path
    auto finalWay = determineWay(clusters, hull);

//    if (m_isCanceled)
//    {
        // set any way - like the input, because we want valid m_resultLength values at further processing.
//        m_shortestWay.clear();
//        for (InputCitiesPointerType::iterator openNodeIter = openPointsBasic->begin();
//             openNodeIter != openPointsBasic->end(); ++openNodeIter)
//        {
//            m_shortestWay.push_back(**openNodeIter);
//        }
//        m_mutex.lock();
//        m_indicators.state(m_indicators.state() | TSP::Indicators::States::ISCANCELED);
//        m_mutex.unlock();
//    }

    /// copy result to final output
    for (auto pointI = finalWay.begin();
         pointI != finalWay.end(); ++pointI)
    {
        TSPWay w;
        w.push_back((*pointI)->point);
        DT::DTNode::ChildType::iterator newChildIter = node->addChild(w);          // FIXME: used sturcture is caused by old ideas of partial ways - drop this tree of ways.
        (*newChildIter)->parent(node);
        node = *newChildIter;
    }
    m_mutex.lock();
    m_indicators.progress(1.0f);
    m_mutex.unlock();
}
