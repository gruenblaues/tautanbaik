#include "ClosestSeboehDist.h"

#include <limits>
#include <algorithm>

using namespace TSP;


ClosestSeboehDistAlgorithm::ClosestSeboehDistAlgorithm() : TspAlgoCommon()
{
}

ClosestSeboehDistAlgorithm::~ClosestSeboehDistAlgorithm()
{
}

void ClosestSeboehDistAlgorithm::run()
{
    /// precondition
    if (m_cities == nullptr || m_resultWay == nullptr)
        return;     // HINT: method init() should be called before run().
    m_mutex.lock();
    int state = m_indicators.state();
    state &= ~Indicators::States::ISNULL;
    state |= Indicators::States::ISRUNNING;
    m_indicators.state(state);
    m_mutex.unlock();

     /// the algorithm
    auto closestSeboehEdges = determineClosestNeighbours(*m_cities);

    /// write result
    writeResult(closestSeboehEdges);

    /// finish
    m_mutex.lock();
    state = m_indicators.state();
    state &= ~Indicators::States::ISRUNNING;
    state |= Indicators::States::ISFINISHED;
    m_indicators.state(state);
    m_indicators.progress(1.0f);
    m_mutex.unlock();

}

ClosestSeboehDistAlgorithm::ClosestSeboehDistsType ClosestSeboehDistAlgorithm::determineClosestNeighbours(const InputCitiesType &citiesInput)
{
    ClosestSeboehDistsType closestSeboehEdges;
    std::list<Point> insertedPoints;
    /// sort all points
    for (const auto &cityIn : citiesInput)
    {
        insertedPoints.push_back(cityIn);

        /// find closest edge
        ClosestEdges bestEdge;
        bestEdge.insert = cityIn;
        double bestDist = std::numeric_limits<double>::max();
        // select from point
        for (const auto &cityFrom : citiesInput)
        {
            if (cityFrom == cityIn)
                continue;
            // select to point
            for (const auto &cityTo : citiesInput)
            {
                /// insert only, if toPoint not already inserted.
                auto insertedI = std::find_if(insertedPoints.begin(), insertedPoints.end()
                                              , [cityTo] (const Point &val)
                {
                    return cityTo == val;
                });
//  TODO [boehmer] ... reinsert               if (insertedI != insertedPoints.end())
//                    continue;
                if (cityTo == cityIn || cityTo == cityFrom)
                    continue;
                 double distEdge = seboehDist(cityFrom, cityTo, cityIn);
                 if (distEdge < bestDist)
                 {
                     bestDist = distEdge;
                     bestEdge.from = cityFrom;
                     bestEdge.to = cityTo;
                 }
            }
            // if no fitting further to point found - expect all points inserted?
            if (bestDist == std::numeric_limits<double>::max())
                return closestSeboehEdges;      // FIXME: program has to cancel here!
        }
        closestSeboehEdges.push_back(bestEdge);
    }
    return closestSeboehEdges;
}

void ClosestSeboehDistAlgorithm::writeResult(ClosestSeboehDistsType &closestDistEdges)
{
    /// precondition
    if (closestDistEdges.empty())
        return;

    /// start
    Point startPoint = closestDistEdges.front().insert;
    auto currentEdgeI = closestDistEdges.begin();

    /// insert first point
    m_resultWay->push_back(startPoint);

    /// walk through other points
    std::list<Point> reviedPoints;
    while (!(currentEdgeI->to == startPoint))
    {
        reviedPoints.push_back(currentEdgeI->insert);
        // find next point
        auto nextEdgeI = std::find_if(closestDistEdges.begin(), closestDistEdges.end()
                                      , [currentEdgeI] (const ClosestEdges &val)
        {
            return currentEdgeI->to == val.insert;
        });
        // insert next point
        if (nextEdgeI != closestDistEdges.end())
        {
            m_resultWay->push_back(nextEdgeI->insert);
            // iterate
            currentEdgeI = nextEdgeI;
        } else {
            ++currentEdgeI;      // TODO [boehmer] iterate to next not possible. Currently we leave this point outside.
        }

        /// DEBUG WORKAROUND: cancel the while-loop, because of shorter ways than taking all points.
        auto workaroundI = std::find_if(m_resultWay->way()->begin(), m_resultWay->way()->end()
                                        ,[currentEdgeI] (const Point &val)
        {
            return currentEdgeI->to == val;
        });
        if (workaroundI != m_resultWay->way()->end())
            break;      // we found a circle, but probably does not reache startPoint.
        if (reviedPoints.size() == closestDistEdges.size())
            break;      // all points inserted, although not startPoint visited. TODO [boehmer] this is standard case.
    }
}

double ClosestSeboehDistAlgorithm::seboehDist(const Point &p1, const Point &p2, const Point &pNew)
{
    return m_para.distanceFnc(p2, pNew)
            + m_para.distanceFnc(p1, pNew)
            - m_para.distanceFnc(p1, p2);
}
