#pragma once

#include "TspAlgoCommon.h"
#include <chrono>
#include <math.h>
#include <list>

#include "dll.h"

#include <bspTreeVersion/BspNode.h>

#define BELLMANN_HELD_KARP_MAX_CITIES 32

namespace TSP {

class EXTERN BellmannHeldKarpInternet : public TspAlgoCommon
{
public:
    using Node = oko::Node;
    using NodeL = std::list<Node*>;
    using NodeLP = std::shared_ptr<NodeL>;


public:
    BellmannHeldKarpInternet() : TspAlgoCommon() { }

    // TspAlgoCommon interface
protected:
    void buildTree(DT::DTNode::NodeType node, float, InputCitiesPointerType *openPointsBasic);

private:
    Way     m_shortestWay;
    double  m_length=0;
    std::chrono::system_clock::time_point m_startTime;
    long    m_stackDepth=0;
    NodeL   _input;

    BellmannHeldKarpInternet::NodeL bellmannHeldKarpAlgo(NodeL &input);
    BellmannHeldKarpInternet::Node *getNodeAt(NodeL &liste, size_t pos);
};

}
