#include <iostream>
#include <list>
#include <cmath>
#include <string>
#include <cstdlib>
#include <sstream>

using namespace std;

class Point
{
public:
    Point() : x(0), y(0) {}
    Point(float aX, float aY) : x(aX), y(aY) {}
    Point(const string &val) { parseFromString(val); }

    void parseFromString(const string &val)
    {
        size_t pos = val.find("x");
        if (pos != string::npos)
        {
            x = atof(val.substr(0,pos).c_str());
            y = atof(val.substr(pos+1).c_str());
        }
    }

    double operator |(const Point &rhs) const
    {
        double dx = x - rhs.x;
        double dy = y - rhs.y;
        return std::sqrt(dx*dx + dy*dy);
    }

    const Point & operator+=(const Point &rhs)
    {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    float x;
    float y;
};



Point calcCenter(const std::list<Point> &points)
{
    /* new
    auto start = ceList.front().fromNode->point;
    auto end = ceList.front().toNode->point;
    Point center((start.x()+end.x())/2, (start.y()+end.y())/2);

    return center;
*/
// /* old
    Point center;
    for (const auto &point : points)
    {
        center += point;
    }
    size_t n = points.size();
    return Point(center.x/n, center.y/n);
 // */
}

double calcAreaOfTriangle(const Point &p1, const Point &p2, const Point &p3)
{
    // [http://www.wolframalpha.com/input/?i=area+of+triangle]
    double a = p1 | p2;
    double b = p2 | p3;
    double c = p3 | p1;
    return 0.25 * std::sqrt(
                (a+b-c)
                * (a-b+c)
                * (-a+b+c)
                * (a+b+c)
                );
}

double localDecision(double area, double detour, double distCenter)
{
//    return (std::pow(area, 0.5)  * std::pow(detour,2) * std::pow(distCenter,1))
//        +  (std::pow(detour,0) * std::pow(distCenter,0))
//        +  std::pow(detour,2);
    return detour * std::pow(distCenter,1);
}


double seboehDistWithGravity(Point from, Point to, Point insert, Point center)
{
    double m = insert | center;
    double detour = (from | insert)
            + (insert | to)
            - (from | to);


    return localDecision(calcAreaOfTriangle(from, to, insert)
                         , detour, m);
}



int main()
{
    std::list<Point> points;

    /// extract points from stream
    string line, word;
    istringstream istream;
    while (std::getline(cin, line)) // initialize line
    {       // move data from line into istream (so it's no longer in line):
        if (line == "--help")
        {
            std::cout << "USAGE: 120x130 140x150 ..." << std::endl;
            return -1;
        }
        istream.str(std::move(line));
        while(istream >> word)
        {
            points.push_back(std::move(Point(word)));
        }
    }

    /// calculate length
    if (points.empty())
    {
        cout << "0" << endl;
        return 0;
    }
    double length = 0;
    Point lastPos = points.front();
    for (const auto &point : points)
    {
        length += point | lastPos;
        lastPos = point;
    }

    cout.precision(5);
    cout << fixed << length << endl;


    return 0;
    /*

    /// calc gravity dist
    lastPos = points.front();
    Point toPos = points.back();
    Point center = calcCenter(points);
    double distGrav = 0;
    for (const auto &point : points)
    {
        if (point.x == points.front().x && point.y == points.front().y)
            continue;       // if first element.
        if (point.x == toPos.x && point.y == toPos.y)
            continue;       // if last element.
        distGrav += seboehDistWithGravity(lastPos, toPos, point, center);
        lastPos = point;
    }

    cout << "Gravity: ";
    cout << fixed << distGrav << endl;

    return 0;
    */
}

